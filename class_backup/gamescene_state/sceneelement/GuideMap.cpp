#include "GuideMap.h"

#include "../GameSceneState.h"
#include "GameView.h"
#include "../role/MyPlayer.h"
#include "../../ui/mapscene/MapScene.h"
#include "../../ui/mapscene/AreaMap.h"
#include "../../ui/FivePersonInstance/InstanceDetailUI.h"
#include "../../ui/FivePersonInstance/FivePersonInstance.h"
#include "../../ui/FivePersonInstance/InstanceFunctionPanel.h"
#include "../../ui/robot_ui/RobotMainUI.h"
#include "../MainScene.h"
#include "../../messageclient/element/CMapInfo.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/UISceneTest.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/Script.h"
#include "../role/MyPlayerAIConfig.h"
#include "../role/MyPlayerAI.h"
#include "../role/MyPlayerSimpleAI.h"
#include "../../ui/shop_ui/ShopUI.h"
#include "../../ui/goldstore_ui/GoldStoreUI.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../messageclient/element/CFunctionOpenLevel.h"
#include "../../messageclient/element/CFivePersonInstance.h"
#include "AppMacros.h"
#include "../../ui/offlinearena_ui/OffLineArenaUI.h"
#include "../../ui/OnlineReward_ui/OnlineGiftIconWidget.h"
#include "../../ui/Active_ui/ActiveUI.h"
#include "../../ui/OnlineReward_ui/OnlineGiftData.h"
#include "../../ui/Active_ui/ActiveShopData.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../ui/Rank_ui/RankUI.h"
#include "../../ui/offlineExp_ui/OffLineExpUI.h"
#include "../../ui/vip_ui/FirstBuyVipUI.h"
#include "../../ui/Question_ui/QuestionUI.h"
#include "../../ui/Question_ui/QuestionData.h"
#include "../../messageclient/protobuf/PlayerMessage.pb.h"
#include "../../utils/GameConfig.h"
#include "../../ui/Mail_ui/MailFriend.h"
#include "../../ui/Chat_ui/AddPrivateUi.h"
#include "../../ui/family_ui/familyFight_ui/FamilyFightData.h"
#include "../../ui/challengeRound/SingCopyInstance.h"
#include "../../utils/GameUtils.h"
#include "../../ui/challengeRound/SingCopyCountDownUI.h"
#include "../../ui/family_ui/familyFight_ui/BattleSituationUI.h"
#include "../../ui/Active_ui/ActiveIconWidget.h"
#include "../../ui/FivePersonInstance/InstanceMapUI.h"
#include "../../ui/Reward_ui/RewardUi.h"
#include "GeneralsModeUI.h"
#include "../../legend_script/CCTeachingGuide.h"
#include "MissionAndTeam.h"

#define kTagActionLayer 951
#define KTagGeneralsModeUI 952

GuideMap::GuideMap():
m_generalMode(1),
m_autoFight(2),
m_nRemainTime(600),
isExistUIForMainScene(false),
isExistUIForFiveInstanceScene(false),
isExistUIForArenaScene(false),
isExistUIForFamilyFightScene(false),
isExistUIForSingCopyScene(false)
{

	if (MyPlayerAIConfig::getAutomaticSkill() == 1)
	{
		isOpenSkillMonster = true;
	}
	

	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	//2行5列
	for (int m = 1;m>=0;m--)
	{
		for (int n = 4;n>=0;n--)
		{
			int _m = 1-m;
			int _n = 4-n;

			GMCoordinate temp ;
			temp.x = winSize.width-70-_n*53;
			temp.y = winSize.height-70-_m*53;
			CoordinateVector.push_back(temp);
		}
	}
}


GuideMap::~GuideMap()
{
	std::vector<CFunctionOpenLevel*>::iterator _iter_1;
	for (_iter_1 = GMOpendedFunctionVector_1.begin(); _iter_1 != GMOpendedFunctionVector_1.end(); ++_iter_1)
	{
		delete *_iter_1;
	}
	GMOpendedFunctionVector_1.clear();

	std::vector<CFunctionOpenLevel*>::iterator _iter;
	for (_iter = GMOpendedFunctionVector.begin(); _iter != GMOpendedFunctionVector.end(); ++_iter)
	{
		delete *_iter;
	}
	GMOpendedFunctionVector.clear();
}

bool GMSortByPositionIndex(CFunctionOpenLevel * fcOpenLevel1 , CFunctionOpenLevel *fcOpenLevel2)
{
	return (fcOpenLevel1->get_positionIndex() < fcOpenLevel2->get_positionIndex());
}


void GuideMap::onEnter()
{
	UIScene::onEnter();
}

void GuideMap::onExit()
{
	UIScene::onExit();
}


GuideMap * GuideMap::create()
{
	GuideMap * guideMap = new GuideMap();
	if (guideMap && guideMap->init())
	{
		guideMap->autorelease();
		return guideMap;
	}
	CC_SAFE_DELETE(guideMap);
	return NULL;
}

bool GuideMap::init()
{
	if (UIScene::init())
	{

		//setSceneType(GameView::getInstance()->getMapInfo()->maptype());

		u_layer_MainScene = UILayer::create();
		addChild(u_layer_MainScene);
		u_layer_FiveInstanceScene = UILayer::create();
		addChild(u_layer_FiveInstanceScene);
		u_layer_ArenaScene = UILayer::create();
		addChild(u_layer_ArenaScene);
		u_layer_FamilyFightScene = UILayer::create();
		addChild(u_layer_FamilyFightScene);
		u_layer_SingCopyScene = UILayer::create();
		addChild(u_layer_SingCopyScene);

// 		createUIForMainScene();
// 		createUIForFiveInstanceScene();
// 		createUIForArenaScene();
// 		createUIForFamilyFightScene();

		changeUIBySceneType(GameView::getInstance()->getMapInfo()->maptype());

		this->schedule(schedule_selector(GuideMap::update),1.0f);
		this->setAnchorPoint(ccp(0,0));
		this->setPosition(ccp(0,0));
		this->setContentSize(winSize);
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(winSize);
		
		return true;
	}
	return false;
}

bool GuideMap::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
		return false;
}
void GuideMap::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
{

}
void GuideMap::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
{

}
void GuideMap::ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent)
{

}

void GuideMap::InstanceEvent( CCObject *pSender )
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	MainScene* mainUILayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
	CCAssert(mainUILayer != NULL, "should not be nil");

	//add by yangjun 2014.10.13
	if (mainUILayer)
	{
		if (mainUILayer->isHeadMenuOn)
		{
			mainUILayer->ButtonHeadEvent(NULL);
		}
	}

	// the player has already been in the instance map
	if(FivePersonInstance::getStatus() == FivePersonInstance::status_in_instance)
	{
// 		if(mainUILayer->getChildByTag(kTagInstanceFunctionPanel) != NULL)
// 		{
// 			mainUILayer->getChildByTag(kTagInstanceFunctionPanel)->removeFromParent();
// 		}
// 		else
// 		{
// 			InstanceFunctionPanel* _ui=InstanceFunctionPanel::create();
// 			_ui->ignoreAnchorPointForPosition(false);
// 			_ui->setAnchorPoint(ccp(1.0f,1.0f));
// 			_ui->setPosition(ccp(btn_instance->getPosition().x-20, btn_instance->getPosition().y+20));
// 			_ui->setTag(kTagInstanceFunctionPanel);
// 			mainUILayer->addChild(_ui);
// 		}
	}
	else
	{
// 		if(mainUILayer->getChildByTag(kTagInstanceMainUI) == NULL)
// 		{
// 			InstanceDetailUI * _ui=InstanceDetailUI::create();
// 			_ui->ignoreAnchorPointForPosition(false);
// 			_ui->setAnchorPoint(ccp(0.5f,0.5f));
// 			_ui->setPosition(ccp(winSize.width/2,winSize.height/2));
// 			_ui->setTag(kTagInstanceMainUI);
// 			mainUILayer->addChild(_ui);
// 		}

		if(mainUILayer->getChildByTag(kTagInstanceMapUI) == NULL)
		{
			InstanceMapUI * _ui=InstanceMapUI::create();
			_ui->ignoreAnchorPointForPosition(false);
			_ui->setAnchorPoint(ccp(0.5f,0.5f));
			_ui->setPosition(ccp(winSize.width/2,winSize.height/2));
			_ui->setTag(kTagInstanceMapUI);
			mainUILayer->addChild(_ui);
		}
	}

	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(pSender);
}


void GuideMap::CallFriendsEvent( CCObject *pSender )
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	MainScene* mainUILayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
	CCAssert(mainUILayer != NULL, "should not be nil");

	//add by yangjun 2014.10.13
	if (mainUILayer)
	{
		if (mainUILayer->isHeadMenuOn)
		{
			mainUILayer->ButtonHeadEvent(NULL);
		}
	}

	// the player has already been in the instance map
	if(FivePersonInstance::getStatus() == FivePersonInstance::status_in_instance)
	{
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagPopFriendListUI) == NULL)
		{
			CCSize winsize=CCDirector::sharedDirector()->getVisibleSize();
			MailFriend * mailFriend =MailFriend::create(kTagInstanceDetailUI);
			mailFriend->ignoreAnchorPointForPosition(false);
			mailFriend->setAnchorPoint(ccp(0.5f,0.5f));
			mailFriend->setPosition(ccp(winsize.width/2,winsize.height/2));
			GameView::getInstance()->getMainUIScene()->addChild(mailFriend,0,kTagPopFriendListUI);
			AddPrivateUi::FriendList friend1={0,20,0,1};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2202,&friend1);
		}
	}
}

void GuideMap::ExitEvent( CCObject *pSender )
{
	//add by yangjun 2014.10.13
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		if (mainScene->isHeadMenuOn)
		{
			mainScene->ButtonHeadEvent(NULL);
		}
	}

	GameView::getInstance()->showPopupWindow(StringDataManager::getString("fivePerson_areYouSureToExit"),2,this,coco_selectselector(GuideMap::sureToExit),NULL,PopupWindow::KTypeRemoveByTime,10);
}

void GuideMap::sureToExit( CCObject *pSender )
{
	//add by yangjun 2014.10.13
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		if (mainScene->isHeadMenuOn)
		{
			mainScene->ButtonHeadEvent(NULL);
		}
	}

	GameMessageProcessor::sharedMsgProcessor()->sendReq(1924);
}

void GuideMap::ExitSingCopy( CCObject * obj )
{
	//add by yangjun 2014.10.13
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		if (mainScene->isHeadMenuOn)
		{
			mainScene->ButtonHeadEvent(NULL);
		}
	}

	GameView::getInstance()->showPopupWindow(StringDataManager::getString("singCopy_areYouSureToExit"),2,this,coco_selectselector(GuideMap::surToExitSingCopy),NULL,PopupWindow::KTypeRemoveByTime,10);
}

void GuideMap::surToExitSingCopy( CCObject * obj )
{
	MissionAndTeam * missionAndTeam = (MissionAndTeam *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMissionAndTeam);
	if (missionAndTeam)
	{
		missionAndTeam->setStateIsRobot(false);
		missionAndTeam->unschedule(schedule_selector(MissionAndTeam::updateSingCopyCheckMonst));
	}

	int m_clazz = SingCopyInstance::getCurSingCopyClazz();
	int m_level = SingCopyInstance::getCurSingCopyLevel();
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1902,(void *)m_clazz,(void *)m_level);
}

void GuideMap::HangUpEvent( CCObject *pSender )
{
	//add by yangjun 2014.10.13
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		if (mainScene->isHeadMenuOn)
		{
			mainScene->ButtonHeadEvent(NULL);
		}
	}

	for(int i = 0;i<FunctionOpenLevelConfigData::s_functionOpenLevelMsg.size();++i)
	{
		if (strcmp(((UIButton*)pSender)->getName(),FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionName().c_str()) == 0)
		{
			//headMenu->addNewFunction(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i));
			if (GameView::getInstance()->myplayer->getActiveRole()->level() < FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel())
			{
				GameView::getInstance()->showAlertDialog(StringDataManager::getString("skillui_nothavefuction"));
				return;
			}
		}
	}

	MainScene* mainUILayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
	CCAssert(mainUILayer != NULL, "should not be nil");
	//if (isOpenSkillMonster==false)
	if (MyPlayerAIConfig::getAutomaticSkill() == 0) 
	{
// 		if (getBtnHangUp())
// 		{
// 			getBtnHangUp()->setTextures("gamescene_state/zhujiemian3/tubiao/off_hook.png","gamescene_state/zhujiemian3/tubiao/off_hook.png","");
// 		}
		setAllBtnHangUpTextures("gamescene_state/zhujiemian3/tubiao/off_hook.png");
		
		const char *strings_starSkill = StringDataManager::getString("robotui_StarSkillMonster");
		GameView::getInstance()->showAlertDialog(strings_starSkill);
		//set automatic skill
		MyPlayerAIConfig::setAutomaticSkill(1);
		//set star robot
		MyPlayerAIConfig::enableRobot(true);
		GameView::getInstance()->myplayer->getMyPlayerAI()->start();

		isOpenSkillMonster =true;
	}else
	{
// 		if (getBtnHangUp())
// 		{
// 			getBtnHangUp()->setTextures("gamescene_state/zhujiemian3/tubiao/on_hook.png","gamescene_state/zhujiemian3/tubiao/on_hook.png","");
// 		}
		setAllBtnHangUpTextures("gamescene_state/zhujiemian3/tubiao/on_hook.png");

		const char *strings_stopSkill = StringDataManager::getString("robotui_StopSkillMonster");
		GameView::getInstance()->showAlertDialog(strings_stopSkill);
		// 停止自动攻击，但挂机并不停止，可以继续吃药等挂机行为
		MyPlayerAIConfig::setAutomaticSkill(0);
		
		// 停止自动攻击，但会将玩家锁定的一个危险目标打死为止
		MyPlayer* me = GameView::getInstance()->myplayer;
		GameActor* pActor = me->getLockedActor();
		if(pActor != NULL)
		{
			BaseFighter* lockedFighter = dynamic_cast<BaseFighter*>(pActor);
			if(lockedFighter != NULL && lockedFighter->getChildByTag(GameActor::kTagDangerCircle) != NULL)
			{
				GameView::getInstance()->myplayer->getSimpleAI()->startKill(lockedFighter->getRoleId());
			}
		}
		
		isOpenSkillMonster=false;
	}

	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(pSender);
}

void GuideMap::ShopEvent( CCObject *pSender )
{
	//add by yangjun 2014.10.13
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		if (mainScene->isHeadMenuOn)
		{
			mainScene->ButtonHeadEvent(NULL);
		}
	}

 	for(int i = 0;i<FunctionOpenLevelConfigData::s_functionOpenLevelMsg.size();++i)
 	{
 		if (strcmp(((UIButton*)pSender)->getName(),FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionName().c_str()) == 0)
 		{
 			//headMenu->addNewFunction(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i));
 			if (GameView::getInstance()->myplayer->getActiveRole()->level() < FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel())
 			{
 				GameView::getInstance()->showAlertDialog(StringDataManager::getString("skillui_nothavefuction"));
 				return;
 			}
 		}
 	}
 
 	GoldStoreUI *goldStoreUI = (GoldStoreUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoldStoreUI);
 	if(goldStoreUI == NULL)
 	{
 		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
 		goldStoreUI = GoldStoreUI::create();
 		GameView::getInstance()->getMainUIScene()->addChild(goldStoreUI,0,kTagGoldStoreUI);
 
 		goldStoreUI->ignoreAnchorPointForPosition(false);
 		goldStoreUI->setAnchorPoint(ccp(0.5f, 0.5f));
 		goldStoreUI->setPosition(ccp(winSize.width/2, winSize.height/2));
 	}
//  	
// 	if (QuestionData::instance()->get_hasAnswerQuestion() >= QuestionData::instance()->get_allQuestion())
// 	{
// 		GameView::getInstance()->showAlertDialog(StringDataManager::getString("QuestionUI_hasNoNumber"));
// 	}
// 	else
// 	{
// 		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
// 		QuestionUI * pTmpQuestionUI = (QuestionUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagQuestionUI);
// 		if (NULL == pTmpQuestionUI)
// 		{
// 			QuestionUI * pQuestionUI = QuestionUI::create();
// 			pQuestionUI->ignoreAnchorPointForPosition(false);
// 			pQuestionUI->setAnchorPoint(ccp(0.5f, 0.5f));
// 			pQuestionUI->setPosition(ccp(winSize.width / 2, winSize.height / 2));
// 			pQuestionUI->setTag(kTagQuestionUI);
// 			pQuestionUI->requestQuestion();				// 向服务器请求题目
// 
// 			GameView::getInstance()->getMainUIScene()->addChild(pQuestionUI);
// 		}
// 	}
}

void GuideMap::ActivityEvent( CCObject *pSender )
{
	GameView::getInstance()->showAlertDialog(StringDataManager::getString("feature_will_be_open"));
}

void GuideMap::GiftEvent( CCObject *pSender )
{
	GameView::getInstance()->showAlertDialog(StringDataManager::getString("feature_will_be_open"));
}

void GuideMap::ActiveDegreeEvent( CCObject *pSender )
{
	//GameView::getInstance()->showAlertDialog(StringDataManager::getString("feature_will_be_open"));

	//ActiveUI* pTmpActiveUI = (ActiveUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveUI);
	//if (NULL == pTmpActiveUI)
	//{
	//	ActiveUI * pActiveUI = ActiveUI::create();
	//	pActiveUI->ignoreAnchorPointForPosition(false);
	//	pActiveUI->setAnchorPoint(ccp(0.5f, 0.5f));
	//	pActiveUI->setPosition(ccp(winSize.width / 2, winSize.height / 2));
	//	pActiveUI->setTag(kTagActiveUI);
	//	
	//	GameView::getInstance()->getMainUIScene()->addChild(pActiveUI);

	//	pActiveUI->initActiveUIData();

	//	// 请求 活跃度商店数据（当活跃度商店数据不为空时，说明已经请求过服务器数据，那么不再发送请求）
	//	if (ActiveShopData::instance()->m_vector_activeShopSource.empty())
	//	{
	//		// 请求服务器领取奖品
	//		GameMessageProcessor::sharedMsgProcessor()->sendReq(5114, NULL);
	//	}
	//}
}

void GuideMap::EveryDayGiftEvent( CCObject *pSender )
{
	GameView::getInstance()->showAlertDialog(StringDataManager::getString("feature_will_be_open"));
}

void GuideMap::NewServiceEvent( CCObject *pSender )
{
	GameView::getInstance()->showAlertDialog(StringDataManager::getString("feature_will_be_open"));
}

void GuideMap::update(float dt)
{
// 	if (u_layer_ArenaScene->isVisible())
// 	{
// 		if (u_layer_ArenaScene->getWidgetByName("l_remainTimeValue"))
// 		{
// 			m_nRemainTime--;
// 			l_remainTimeValue->setText(this->timeFormatToString(m_nRemainTime).c_str());
// 
// 			if (m_nRemainTime <= 0)
// 			{
// 				//退出竞技场
// 				GameMessageProcessor::sharedMsgProcessor()->sendReq(3021);
// 			}
// 		}
// 	}
}

void GuideMap::setSceneType(int mapType)
{
	//m_curSceneType = sceneType;
	changeUIBySceneType(mapType);
}

//teach learn
void GuideMap::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void GuideMap::addCCTutorialIndicator( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,true);
// 	tutorialIndicator->setPosition(ccp(pos.x-60,pos.y-80));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);

	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,54,54,true);
	tutorialIndicator->setDrawNodePos(ccp(pos.x,pos.y));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void GuideMap::addCCTutorialIndicator1( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
	if (this->getActionLayer())
	{
// 		CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,true);
// 		tutorialIndicator->setPosition(ccp(pos.x-60,pos.y-80));
// 		tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 		this->getActionLayer()->addChild(tutorialIndicator);
		CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,54,54,true);
		tutorialIndicator->setDrawNodePos(ccp(pos.x,pos.y));
		tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
		MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
		if (mainScene)
		{
			mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
		}
	}
}

void GuideMap::removeCCTutorialIndicator()
{
	CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	if (this->getActionLayer())
	{
		CCTutorialIndicator* tutorialIndicator1 = dynamic_cast<CCTutorialIndicator*>(this->getActionLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(tutorialIndicator1 != NULL)
			tutorialIndicator1->removeFromParent();
	}

	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		CCTeachingGuide* teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}

}

void GuideMap::GoToMapScene( CCObject * pSender )
{
	//add by yangjun 2014.10.13
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		if (mainScene->isHeadMenuOn)
		{
			mainScene->ButtonHeadEvent(NULL);
		}
	}

	//地图界面入口
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMap) == NULL)
	{
		CCSize s = CCDirector::sharedDirector()->getVisibleSize();
		MapScene * mapLayer = MapScene::create();
		GameView::getInstance()->getMainUIScene()->addChild(mapLayer,0,kTagMap);
		mapLayer->ignoreAnchorPointForPosition(false);
		mapLayer->setAnchorPoint(ccp(0.5f, 0.5f));
		mapLayer->setPosition(ccp(s.width/2, s.height/2));
	}
}

void GuideMap::addNewFunctionForArea1( CCNode * pNode,void * functionOpenLevel)
{
// 	if (GameView::getInstance()->getMapInfo()->maptype() == coliseum||GameView::getInstance()->getMapInfo()->maptype() == 6)
// 		return;

	//地图1号区域
	if (u_layer_MainScene->getWidgetByName(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId().c_str()))
	{
		(u_layer_MainScene->getWidgetByName(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId().c_str()))->setVisible(true);
	}
}

void GuideMap::addNewFunctionForArea2( CCNode * pNode,void * functionOpenLevel)
{
// 	if (GameView::getInstance()->getMapInfo()->maptype() == coliseum||GameView::getInstance()->getMapInfo()->maptype() == 6)
// 		return;

	if (strcmp(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId().c_str(),"btn_offLineArena") == 0)  //竞技场开启了
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(3000);
	}

	//地图2号区域
	UILayer* l_al = (UILayer *)u_layer_MainScene->getChildByTag(kTagActionLayer);
	int lastIndex = -5;
	for (int i = 0;i<GMOpendedFunctionVector.size();++i)
	{
		if (GMOpendedFunctionVector.at(i)->get_positionIndex() > ((CFunctionOpenLevel * )functionOpenLevel)->get_positionIndex())
		{
			if (lastIndex == -5)
				lastIndex = i-1;

			
			if (l_al->getWidgetByName(GMOpendedFunctionVector.at(i)->get_functionId().c_str()))
			{
				if (i == 4)
				{
					CCFiniteTimeAction*  action = CCSequence::create(
						CCMoveTo::create(0.15f,ccp(CoordinateVector.at(5).x,CoordinateVector.at(5).y)),
						CCMoveTo::create(0.35f,ccp(CoordinateVector.at(i+1).x,CoordinateVector.at(i+1).y)),
						NULL);
					(l_al->getWidgetByName(GMOpendedFunctionVector.at(i)->get_functionId().c_str()))->runAction(action);
				}
				else
				{
					CCFiniteTimeAction*  action = CCSequence::create(
						CCMoveTo::create(0.5f,ccp(CoordinateVector.at(i+1).x,CoordinateVector.at(i+1).y)),
						NULL);
					(l_al->getWidgetByName(GMOpendedFunctionVector.at(i)->get_functionId().c_str()))->runAction(action);
				}
			}
		}
	}

	if(lastIndex == -5)
	{
		if (l_al->getWidgetByName(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId().c_str()))
		{
			(l_al->getWidgetByName(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId().c_str()))->setVisible(true);
			(l_al->getWidgetByName(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId().c_str()))->setPosition(ccp(CoordinateVector.at(GMOpendedFunctionVector.size()).x,CoordinateVector.at(GMOpendedFunctionVector.size()).y));
		}
	}
	else
	{
		if (l_al->getWidgetByName(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId().c_str()))
		{
			(l_al->getWidgetByName(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId().c_str()))->setVisible(true);
			(l_al->getWidgetByName(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId().c_str()))->setPosition(ccp(CoordinateVector.at(lastIndex + 1).x,CoordinateVector.at(lastIndex + 1).y));
		}
	}

	CFunctionOpenLevel * temp = new CFunctionOpenLevel();
	temp->set_functionId(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId());
	temp->set_functionName(((CFunctionOpenLevel * )functionOpenLevel)->get_functionName());
	temp->set_functionType(((CFunctionOpenLevel * )functionOpenLevel)->get_functionType());
	temp->set_requiredLevel(((CFunctionOpenLevel * )functionOpenLevel)->get_requiredLevel());
	temp->set_positionIndex(((CFunctionOpenLevel * )functionOpenLevel)->get_positionIndex());
	GMOpendedFunctionVector.push_back(temp);
	sort(GMOpendedFunctionVector.begin(),GMOpendedFunctionVector.end(),GMSortByPositionIndex);
}

void GuideMap::OnLineGiftEvent( CCObject *pSender )
{

}

void GuideMap::showRemindContent( CCObject * obj )
{
	//add by yangjun 2014.10.13
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		if (mainScene->isHeadMenuOn)
		{
			mainScene->ButtonHeadEvent(NULL);
		}
	}

	UIButton * button_ =(UIButton *)obj;
	RemindUi * remindui_ =RemindUi::create();
	remindui_->ignoreAnchorPointForPosition(false);
	remindui_->setAnchorPoint(ccp(0.5f,1));
	remindui_->setPosition(ccp(button_->getPosition().x,winSize.height - button_->getContentSize().height));
	remindui_->setTag(kTagRemindUi);
	GameView::getInstance()->getMainUIScene()->addChild(remindui_);
}
void GuideMap::ArenaEvent( CCObject *pSender )
{
	//add by yangjun 2014.10.13
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		if (mainScene->isHeadMenuOn)
		{
			mainScene->ButtonHeadEvent(NULL);
		}
	}

	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOffLineArenaUI) == NULL)
	{
		CCSize winSize=CCDirector::sharedDirector()->getVisibleSize();
		OffLineArenaUI * offLineArenaUI =OffLineArenaUI::create();
		offLineArenaUI->ignoreAnchorPointForPosition(false);
		offLineArenaUI->setAnchorPoint(ccp(0.5f,0.5f));
		offLineArenaUI->setPosition(ccp(winSize.width/2,winSize.height/2));
		offLineArenaUI->setTag(kTagOffLineArenaUI);
		GameView::getInstance()->getMainUIScene()->addChild(offLineArenaUI);

		Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if(sc != NULL)
			sc->endCommand(pSender);
	}
}

void GuideMap::changeUIBySceneType( int mapType )
{
	switch(mapType)
	{
	case com::future::threekingdoms::server::transport::protocol::coliseum:           //竞技场                                                                     //竞技场
		{
			if (!isExistUIForArenaScene)
			{
				createUIForArenaScene();
			}

			m_nRemainTime = 600;

			u_layer_MainScene->setVisible(false);
			u_layer_FiveInstanceScene->setVisible(false);
			u_layer_ArenaScene->setVisible(true);
			u_layer_FamilyFightScene->setVisible(false);
			u_layer_SingCopyScene->setVisible(false);
		}
		break;
	case com::future::threekingdoms::server::transport::protocol::copy:        //副本
		{
			if (!isExistUIForFiveInstanceScene)
			{
				createUIForFiveInstanceScene();
			}

			u_layer_MainScene->setVisible(false);
			u_layer_FiveInstanceScene->setVisible(true);
			u_layer_ArenaScene->setVisible(false);
			u_layer_FamilyFightScene->setVisible(false);
			u_layer_SingCopyScene->setVisible(false);

			if (MyPlayerAIConfig::getAutomaticSkill() == 1)
			{
				setAllBtnHangUpTextures("gamescene_state/zhujiemian3/tubiao/off_hook.png");
			}
			else
			{
				setAllBtnHangUpTextures("gamescene_state/zhujiemian3/tubiao/on_hook.png");
			}

			//如果副本教学正在执行，则打断
			Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
			if(sc != NULL)
			{
				if(sc->isScriptCMD("TutorialFiveInstance2"))
				{
					sc->endCommand(this->btn_instance);
				}
			}
		}
		break;
	case com::future::threekingdoms::server::transport::protocol::tower:        //singCopy
		{
			/*
			//sing copy countdown star
			SingCopyInstance::setStartTime(GameUtils::millisecondNow());
			SingCopyCountDownUI * singCopyCountDownUi = SingCopyCountDownUI::create();
			singCopyCountDownUi->ignoreAnchorPointForPosition(false);
			singCopyCountDownUi->setAnchorPoint(ccp(0.5f,0.5f));
			singCopyCountDownUi->setPosition(ccp(winSize.width/2,winSize.height/2));
			singCopyCountDownUi->setTag(kTagSingCopyCountDownUi);
			GameView::getInstance()->getMainUIScene()->addChild(singCopyCountDownUi);
			*/
			MainScene * mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();	
			MissionAndTeam * missionAndTeam = (MissionAndTeam *)mainscene->getChildByTag(kTagMissionAndTeam);
			if (missionAndTeam)
			{
				missionAndTeam->starRunUpdate();
			}
			
			if (!isExistUIForSingCopyScene)
			{
				createUIForSingCopyScene();
			}

			u_layer_MainScene->setVisible(false);
			u_layer_FiveInstanceScene->setVisible(false);
			u_layer_ArenaScene->setVisible(false);
			u_layer_FamilyFightScene->setVisible(false);
			u_layer_SingCopyScene->setVisible(true);

			if (MyPlayerAIConfig::getAutomaticSkill() == 1)
			{
				setAllBtnHangUpTextures("gamescene_state/zhujiemian3/tubiao/off_hook.png");
			}
			else
			{
				setAllBtnHangUpTextures("gamescene_state/zhujiemian3/tubiao/on_hook.png");
			}
		}break;
	case com::future::threekingdoms::server::transport::protocol::guildFight:        //家族战
		{
			if (!isExistUIForFamilyFightScene)
			{
				createUIForFamilyFightScene();
			}

			u_layer_MainScene->setVisible(false);
			u_layer_FiveInstanceScene->setVisible(false);
			u_layer_ArenaScene->setVisible(false);
			u_layer_FamilyFightScene->setVisible(true);
			u_layer_SingCopyScene->setVisible(false);

			if (MyPlayerAIConfig::getAutomaticSkill() == 1)
			{
				setAllBtnHangUpTextures("gamescene_state/zhujiemian3/tubiao/off_hook.png");
			}
			else
			{
				setAllBtnHangUpTextures("gamescene_state/zhujiemian3/tubiao/on_hook.png");
			}
		}
		break;
	default:
		{
			if (!isExistUIForMainScene)
			{
				createUIForMainScene();
			}

			u_layer_MainScene->setVisible(true);
			u_layer_FiveInstanceScene->setVisible(false);
			u_layer_ArenaScene->setVisible(false);
			u_layer_FamilyFightScene->setVisible(false);
			u_layer_SingCopyScene->setVisible(false);

			if (MyPlayerAIConfig::getAutomaticSkill() == 1)
			{
				setAllBtnHangUpTextures("gamescene_state/zhujiemian3/tubiao/off_hook.png");
			}
			else
			{
				setAllBtnHangUpTextures("gamescene_state/zhujiemian3/tubiao/on_hook.png");
			}
		}
	}

	changeBtnVisibleByType(mapType);
}

void GuideMap::createUIForMainScene()
{
	isExistUIForMainScene = true;
	winSize = CCDirector::sharedDirector()->getVisibleSize();
	CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

	UIButton * map_diBg= UIButton::create();
	map_diBg->setTextures("gamescene_state/zhujiemian3/ditu/ditu_di.png","gamescene_state/zhujiemian3/ditu/ditu_di.png","");
	map_diBg->setAnchorPoint(ccp(0.5f,0.5f));
	map_diBg->setScale9Enable(true);
	map_diBg->setCapInsets(CCRectMake(21,18,1,1));
	map_diBg->setScale9Size(CCSizeMake(153,44));
	map_diBg->setName("map_diBg");
	u_layer_MainScene->addWidget(map_diBg);
	map_diBg->setTouchEnable(true);
	map_diBg->addReleaseEvent(this,coco_releaseselector(GuideMap::GoToMapScene));
	map_diBg->setPressedActionEnabled(true,0.85f,1.2f);
	map_diBg->setPosition(ccp(winSize.width-map_diBg->getSize().width/2+origin.x,winSize.height-map_diBg->getSize().height/2+origin.y));

	//地图名称
	//UILabelBMFont * mapName = UILabelBMFont::create();
	//mapName->setFntFile("res_ui/font/ziti_3.fnt");
	UILabel * mapName = UILabel::create();
	mapName->setFontName(APP_FONT_NAME);
	mapName->setFontSize(15);
	mapName->setColor(ccc3(255,216,61));  //(247,151,30) // (218,105,6)
	mapName->setText(GameView::getInstance()->getMapInfo()->mapname().c_str());
	mapName->setAnchorPoint(ccp(0.5f,0.5f));
	mapName->setPosition(ccp(0,-3));
	mapName->setName("mapName");
	map_diBg->addChild(mapName);

	int countryId = GameView::getInstance()->getMapInfo()->country();
	if (countryId > 0 && countryId <6)
	{
		std::string countryIdName = "country_";
		char id[2];
		sprintf(id, "%d", countryId);
		countryIdName.append(id);
		std::string iconPathName = "res_ui/country_icon/";
		iconPathName.append(countryIdName);
		iconPathName.append(".png");
		UIImageView * countrySp_ = UIImageView::create();
		countrySp_->setTexture(iconPathName.c_str());
		countrySp_->setAnchorPoint(ccp(1.0,0.5f));
		countrySp_->setPosition(ccp(mapName->getPosition().x - mapName->getContentSize().width/2-1,mapName->getPosition().y));
		countrySp_->setScale(0.7f);
		countrySp_->setName("countrySp_");
		map_diBg->addChild(countrySp_);
	}
	else
	{
		mapName->setPosition(ccp(0,-3));
	}

	std::string str_channelId = "(";
	char s_channelId[5];
	sprintf(s_channelId,"%d",GameView::getInstance()->getMapInfo()->channel()+1);
	str_channelId.append(s_channelId);
	str_channelId.append(StringDataManager::getString("GuideMap_Channel_Name"));
	str_channelId.append(")");
	UILabel * l_channel = UILabel::create();
	l_channel->setFontName(APP_FONT_NAME);
	l_channel->setFontSize(15);
	l_channel->setColor(ccc3(255,216,61));  //(247,151,30) // (218,105,6)
	l_channel->setText(str_channelId.c_str());
	l_channel->setAnchorPoint(ccp(0.f,0.5f));
	l_channel->setPosition(ccp(mapName->getPosition().x + mapName->getContentSize().width/2-1,mapName->getPosition().y));
	l_channel->setName("l_channel");
	map_diBg->addChild(l_channel);

	//各个副本按钮等
	//first line
	//商城
	UIButton *btn_shop = UIButton::create();
	btn_shop->setTextures("gamescene_state/zhujiemian3/tubiao/rmbshop.png","gamescene_state/zhujiemian3/tubiao/rmbshop.png","");
	btn_shop->setAnchorPoint(ccp(0.5f,0.5f));
	btn_shop->setPressedActionEnabled(true);
	btn_shop->setTouchEnable(true);
	btn_shop->setPosition(ccp(winSize.width-160+origin.x,winSize.height-btn_shop->getContentSize().height/2+origin.y));
	btn_shop->addReleaseEvent(this,coco_releaseselector(GuideMap::ShopEvent));
	btn_shop->setName("btn_shop");
	u_layer_MainScene->addWidget(btn_shop);
	btn_shop->setVisible(false);
	//挂机
	bool aaa = MyPlayerAIConfig::isEnableRobot();
	int hookIndex = MyPlayerAIConfig::getAutomaticSkill();
	UIButton* btn_hangUp = UIButton::create();
	if (hookIndex == 1)
	{
		btn_hangUp->setTextures("gamescene_state/zhujiemian3/tubiao/off_hook.png","gamescene_state/zhujiemian3/tubiao/off_hook.png","");
	}else
	{
		btn_hangUp->setTextures("gamescene_state/zhujiemian3/tubiao/on_hook.png","gamescene_state/zhujiemian3/tubiao/on_hook.png","");
	}
	btn_hangUp->setAnchorPoint(ccp(0.5f,0.5f));
	btn_hangUp->setPressedActionEnabled(true);
	btn_hangUp->setTouchEnable(true);
	btn_hangUp->setPosition(ccp(btn_shop->getPosition().x-50+origin.x,winSize.height-btn_hangUp->getContentSize().height/2+origin.y));
	btn_hangUp->addReleaseEvent(this,coco_releaseselector(GuideMap::HangUpEvent));
	btn_hangUp->setName("btn_hangUp_1");
	u_layer_MainScene->addWidget(btn_hangUp);
	btn_hangUp->setVisible(false);

	//武将模式
	UIButton *btn_generalMode = UIButton::create();
	btn_generalMode->setTextures("gamescene_state/zhujiemian3/tubiao/driving.png","gamescene_state/zhujiemian3/tubiao/driving.png","");
	btn_generalMode->setAnchorPoint(ccp(0.5f,0.5f));
	btn_generalMode->setPressedActionEnabled(true);
	btn_generalMode->setTouchEnable(true);
	btn_generalMode->setPosition(ccp(btn_hangUp->getPosition().x-50+origin.x,winSize.height-btn_hangUp->getContentSize().height/2+origin.y));
	btn_generalMode->addReleaseEvent(this,coco_releaseselector(GuideMap::GeneralModeEvent));
	btn_generalMode->setName("btn_generalMode");
	u_layer_MainScene->addWidget(btn_generalMode);
	btn_generalMode->setVisible(false);

	setDefaultGeneralMode(CCUserDefault::sharedUserDefault()->getIntegerForKey("general_mode"));

	//remind
	buttonRemind = UIButton::create();
	buttonRemind->setAnchorPoint(ccp(0.5f,0.5f));
	buttonRemind->setPosition(ccp(btn_generalMode->getPosition().x-50+origin.x,winSize.height-btn_generalMode->getContentSize().height/2+origin.y));
	buttonRemind->setTextures("gamescene_state/zhujiemian3/tubiao/upup.png","gamescene_state/zhujiemian3/tubiao/upup.png","");
	buttonRemind->setTouchEnable(true);
	buttonRemind->setPressedActionEnabled(true);
	buttonRemind->addReleaseEvent(this,coco_releaseselector(GuideMap::showRemindContent));
	buttonRemind->setName("btn_remind");
	u_layer_MainScene->addWidget(buttonRemind);
	buttonRemind->setVisible(false);
	 
	//可缩回/弹出的层
	layer_action = UILayer::create();
	u_layer_MainScene->addChild(layer_action);
	layer_action->setTag(kTagActionLayer);

	//副本
	btn_instance= UIButton::create();
	btn_instance->setTextures("gamescene_state/zhujiemian3/tubiao/instance.png","gamescene_state/zhujiemian3/tubiao/instance.png","");
	btn_instance->setAnchorPoint(ccp(0.5f,0.5f));
	btn_instance->setPressedActionEnabled(true);
	btn_instance->setTouchEnable(true);
	btn_instance->setPosition(ccp(CoordinateVector.at(0).x,CoordinateVector.at(0).y));
	btn_instance->addReleaseEvent(this,coco_releaseselector(GuideMap::InstanceEvent));
	btn_instance->setName("btn_instance");
	layer_action->addWidget(btn_instance);
	btn_instance->setVisible(false);

	//竞技场
	btn_offLineArena= UIButton::create();
	btn_offLineArena->setTextures("gamescene_state/zhujiemian3/tubiao/arena.png","gamescene_state/zhujiemian3/tubiao/arena.png","");
	btn_offLineArena->setAnchorPoint(ccp(0.5f,0.5f));
	btn_offLineArena->setPressedActionEnabled(true);
	btn_offLineArena->setTouchEnable(true);
	btn_offLineArena->setPosition(ccp(CoordinateVector.at(0).x,CoordinateVector.at(0).y));
	btn_offLineArena->addReleaseEvent(this,coco_releaseselector(GuideMap::ArenaEvent));
	btn_offLineArena->setName("btn_offLineArena");
	layer_action->addWidget(btn_offLineArena);
	btn_offLineArena->setVisible(false);

	btn_Reward = UIButton::create();
	btn_Reward->setTextures("gamescene_state/zhujiemian3/tubiao/OnlineAwards.png","gamescene_state/zhujiemian3/tubiao/OnlineAwards.png","");
	btn_Reward->setAnchorPoint(ccp(0.5f,0.5f));
	btn_Reward->setPressedActionEnabled(true);
	btn_Reward->setTouchEnable(true);
	btn_Reward->setPosition(ccp(CoordinateVector.at(3).x,CoordinateVector.at(3).y));
	btn_Reward->addReleaseEvent(this,coco_releaseselector(GuideMap::callBackGetReward));
	btn_Reward->setName("btn_onLineGift");
	btn_Reward->setVisible(false);
	layer_action->addWidget(btn_Reward);

	btn_RewardParticle = CCTutorialParticle::create("tuowei0.plist",30,46);
	btn_RewardParticle->setPosition(ccp(0,-btn_Reward->getContentSize().height/2));
	btn_RewardParticle->setVisible(false);
	btn_Reward->addCCNode(btn_RewardParticle);


	/*
	//在线奖励
	if (!OnlineGiftData::instance()->isAllGiftHasGet())
	{
		OnlineGiftIconWidget *  btn_onLineGift = OnlineGiftIconWidget::create();
		btn_onLineGift->setAnchorPoint(ccp(0.5f,0.5f));
		btn_onLineGift->setTouchEnable(true);
		btn_onLineGift->setPosition(ccp(CoordinateVector.at(3).x,CoordinateVector.at(3).y));
		btn_onLineGift->addReleaseEvent(this,coco_releaseselector(GuideMap::OnLineGiftEvent));
		btn_onLineGift->setName("btn_onLineGift");
		btn_onLineGift->setTag(kTagOnLineGiftIcon);
		btn_onLineGift->setVisible(false);
		btn_onLineGift->initDataFromIntent();
		layer_action->addWidget(btn_onLineGift);
	}
	*/
	// 活跃度icon
	ActiveIconWidget * btn_activeIcon = ActiveIconWidget::create();
	btn_activeIcon->setAnchorPoint(ccp(0.5f, 0.5f));
	btn_activeIcon->setTouchEnable(true);
	btn_activeIcon->setPosition(ccp(CoordinateVector.at(1).x,CoordinateVector.at(1).y));
	btn_activeIcon->addReleaseEvent(this, coco_releaseselector(GuideMap::ActiveDegreeEvent));
	btn_activeIcon->setName("btn_activeDegree");
	btn_activeIcon->setTag(kTagActiveIcon);
	btn_activeIcon->setVisible(false);
	layer_action->addWidget(btn_activeIcon);

	//首冲
	ActiveRole * activeRole = GameView::getInstance()->myplayer->getActiveRole();
	if (activeRole->mutable_playerbaseinfo()->rechargeflag() == 0)
	{
		// vip开关控制
		bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
		if (vipEnabled)
		{
			UIButton * btn_firstBuyVip = UIButton::create();
			btn_firstBuyVip->setTextures("gamescene_state/zhujiemian3/tubiao/firstbuy.png", "gamescene_state/zhujiemian3/tubiao/firstbuy.png", "");
			btn_firstBuyVip->setAnchorPoint(ccp(0.5f, 0.5f));
			btn_firstBuyVip->setTouchEnable(true);
			btn_firstBuyVip->setPressedActionEnabled(true);
			btn_firstBuyVip->setPosition(ccp(CoordinateVector.at(1).x,CoordinateVector.at(2).y));
			btn_firstBuyVip->addReleaseEvent(this, coco_releaseselector(GuideMap::FirstBuyVipEvent));
			btn_firstBuyVip->setName("btn_firstBuyVip");
			btn_firstBuyVip->setVisible(false);
			layer_action->addWidget(btn_firstBuyVip);
		}
	}
	//礼包
// 		UIButton *  gift = UIButton::create();
// 		gift->setTextures("gamescene_state/tubiao/libao.png","gamescene_state/tubiao/libao.png","");
// 		gift->setAnchorPoint(ccp(0.5f,0.5f));
// 		gift->setTouchEnable(true);
// 		gift->setPressedActionEnabled(true);
// 		gift->setPosition(ccp(CoordinateVector.at(2).x,CoordinateVector.at(2).y));
// 		gift->addReleaseEvent(this,coco_releaseselector(GuideMap::GiftEvent));
// 		gift->setName("btn_gift");
// 		layer_action->addWidget(gift);
// 		gift->setVisible(false);
	//活动
// 		UIButton * activity = UIButton::create();
// 		activity->setTextures("gamescene_state/tubiao/huodong.png","gamescene_state/tubiao/huodong.png","");
// 		activity->setAnchorPoint(ccp(0.5f,0.5f));
// 		activity->setTouchEnable(true);
// 		activity->setPressedActionEnabled(true);
// 		activity->setPosition(ccp(CoordinateVector.at(3).x,CoordinateVector.at(3).y));
// 		activity->addReleaseEvent(this,coco_releaseselector(GuideMap::ActiveDegreeEvent));
// 		activity->setName("btn_activity");
// 		layer_action->addWidget(activity);
// 		activity->setVisible(false);
	//third line
	//kai fu huo dong 
// 		UIButton *   newService = UIButton::create();
// 		newService->setTextures("gamescene_state/tubiao/kaifuhuodong.png","gamescene_state/tubiao/kaifuhuodong.png","");
// 		newService->setAnchorPoint(ccp(0.5f,0.5f));
// 		newService->setTouchEnable(true);
// 		newService->setPressedActionEnabled(true);
// 		newService->setPosition(ccp(CoordinateVector.at(4).x,CoordinateVector.at(4).y));
// 		newService->addReleaseEvent(this,coco_releaseselector(GuideMap::NewServiceEvent));
// 		newService->setName("btn_newService");
// 		layer_action->addWidget(newService);
// 		newService->setVisible(false);
	//每日领取
// 		UIButton *  everyDayGift = UIButton::create();
// 		everyDayGift->setTextures("gamescene_state/tubiao/meirilingqu.png","gamescene_state/tubiao/meirilingqu.png","");
// 		everyDayGift->setAnchorPoint(ccp(0.5f,0.5f));
// 		everyDayGift->setTouchEnable(true);
// 		everyDayGift->setPressedActionEnabled(true);
// 		everyDayGift->setPosition(ccp(CoordinateVector.at(5).x,CoordinateVector.at(5).y));
// 		everyDayGift->addReleaseEvent(this,coco_releaseselector(GuideMap::EveryDayGiftEvent));
// 		everyDayGift->setName("btn_everyDayGift");
// 		layer_action->addWidget(everyDayGift);
// 		everyDayGift->setVisible(false);

	int roleLevel = GameView::getInstance()->myplayer->getActiveRole()->level();
	for (int i = 0;i<FunctionOpenLevelConfigData::s_functionOpenLevelMsg.size();i++)
	{
		if (FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionType() == 2)//地图区域（1号区域）
		{
			if (FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel()<= roleLevel)     //可以显示
			{
				CFunctionOpenLevel * tempItem = new CFunctionOpenLevel();
				tempItem->set_functionId(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionId());
				tempItem->set_functionName(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionName());
				tempItem->set_functionType(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionType());
				tempItem->set_requiredLevel(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel());
				tempItem->set_positionIndex(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_positionIndex());
				GMOpendedFunctionVector_1.push_back(tempItem);
			}
		}
		else if (FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionType() == 3)       //地图区域（2号区域）
		{
			if (FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel()<= roleLevel)     //可以显示
			{
				if (layer_action->getWidgetByName(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionId().c_str()))
				{
					CFunctionOpenLevel * tempItem = new CFunctionOpenLevel();
					tempItem->set_functionId(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionId());
					tempItem->set_functionName(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionName());
					tempItem->set_functionType(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionType());
					tempItem->set_requiredLevel(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel());
					tempItem->set_positionIndex(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_positionIndex());
					GMOpendedFunctionVector.push_back(tempItem);
				}
			}
		}
   	}
	sort(GMOpendedFunctionVector.begin(),GMOpendedFunctionVector.end(),	GMSortByPositionIndex);
	int m_index = 0;
	for (int i = 0;i<GMOpendedFunctionVector.size();i++)
	{
		if (layer_action->getWidgetByName(GMOpendedFunctionVector.at(i)->get_functionId().c_str()))
		{
			(layer_action->getWidgetByName(GMOpendedFunctionVector.at(i)->get_functionId().c_str()))->setVisible(true);
			(layer_action->getWidgetByName(GMOpendedFunctionVector.at(i)->get_functionId().c_str()))->setPosition(ccp(CoordinateVector.at(m_index).x,CoordinateVector.at(m_index).y));
			m_index++;
		}
	}


	for (int i = 0;i<GMOpendedFunctionVector_1.size();i++)
	{
		if (u_layer_MainScene->getWidgetByName(GMOpendedFunctionVector_1.at(i)->get_functionId().c_str()))
		{
			(u_layer_MainScene->getWidgetByName(GMOpendedFunctionVector_1.at(i)->get_functionId().c_str()))->setVisible(true);
		}
	}
}

void GuideMap::createUIForFiveInstanceScene()
{
	isExistUIForFiveInstanceScene = true;
	winSize = CCDirector::sharedDirector()->getVisibleSize();
	CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

	//各个副本按钮等
	//first line
	//退出
	UIButton *btn_exit = UIButton::create();
	btn_exit->setTextures("gamescene_state/zhujiemian3/tubiao/quit.png","gamescene_state/zhujiemian3/tubiao/quit.png","");
	btn_exit->setAnchorPoint(ccp(0.5f,0.5f));
	btn_exit->setPressedActionEnabled(true);
	btn_exit->setTouchEnable(true);
	btn_exit->setPosition(ccp(winSize.width-30+origin.x,winSize.height-btn_exit->getContentSize().height/2+origin.y));
	btn_exit->addReleaseEvent(this,coco_releaseselector(GuideMap::ExitEvent));
	btn_exit->setName("btn_exit");
	u_layer_FiveInstanceScene->addWidget(btn_exit);
	btn_exit->setVisible(true);
	//商城
	UIButton *btn_shop = UIButton::create();
	btn_shop->setTextures("gamescene_state/zhujiemian3/tubiao/rmbshop.png","gamescene_state/zhujiemian3/tubiao/rmbshop.png","");
	btn_shop->setAnchorPoint(ccp(0.5f,0.5f));
	btn_shop->setPressedActionEnabled(true);
	btn_shop->setTouchEnable(true);
	btn_shop->setPosition(ccp(btn_exit->getPosition().x-50+origin.x,winSize.height-btn_exit->getContentSize().height/2+origin.y));
	btn_shop->addReleaseEvent(this,coco_releaseselector(GuideMap::ShopEvent));
	btn_shop->setName("btn_shop");
	u_layer_FiveInstanceScene->addWidget(btn_shop);
	btn_shop->setVisible(true);
	//挂机
	bool aaa = MyPlayerAIConfig::isEnableRobot();
	int hookIndex = MyPlayerAIConfig::getAutomaticSkill();
	UIButton* btn_hangUp = UIButton::create();
	if (hookIndex == 1)
	{
		btn_hangUp->setTextures("gamescene_state/zhujiemian3/tubiao/off_hook.png","gamescene_state/zhujiemian3/tubiao/off_hook.png","");
	}else
	{
		btn_hangUp->setTextures("gamescene_state/zhujiemian3/tubiao/on_hook.png","gamescene_state/zhujiemian3/tubiao/on_hook.png","");
	}
	btn_hangUp->setAnchorPoint(ccp(0.5f,0.5f));
	btn_hangUp->setPressedActionEnabled(true);
	btn_hangUp->setTouchEnable(true);
	btn_hangUp->setPosition(ccp(btn_shop->getPosition().x-50+origin.x,winSize.height-btn_exit->getContentSize().height/2+origin.y));
	btn_hangUp->addReleaseEvent(this,coco_releaseselector(GuideMap::HangUpEvent));
	btn_hangUp->setName("btn_hangUp_1");
	u_layer_FiveInstanceScene->addWidget(btn_hangUp);
	btn_hangUp->setVisible(true);

	//武将模式
	UIButton *btn_generalMode = UIButton::create();
	btn_generalMode->setTextures("gamescene_state/zhujiemian3/tubiao/driving.png","gamescene_state/zhujiemian3/tubiao/driving.png","");
	btn_generalMode->setAnchorPoint(ccp(0.5f,0.5f));
	btn_generalMode->setPressedActionEnabled(true);
	btn_generalMode->setTouchEnable(true);
	btn_generalMode->setPosition(ccp(btn_hangUp->getPosition().x-50+origin.x,winSize.height-btn_exit->getContentSize().height/2+origin.y));
	btn_generalMode->addReleaseEvent(this,coco_releaseselector(GuideMap::GeneralModeEvent));
	btn_generalMode->setName("btn_generalMode");
	u_layer_FiveInstanceScene->addWidget(btn_generalMode);

	setDefaultGeneralMode(CCUserDefault::sharedUserDefault()->getIntegerForKey("general_mode"));

	//remind
// 	buttonRemind = UIButton::create();
// 	buttonRemind->setAnchorPoint(ccp(0.5f,0.5f));
// 	buttonRemind->setPosition(ccp(btn_generalMode->getPosition().x-50+origin.x,winSize.height-btn_exit->getContentSize().height/2+origin.y));
// 	buttonRemind->setTextures("gamescene_state/zhujiemian3/tubiao/upup.png","gamescene_state/zhujiemian3/tubiao/upup.png","");
// 	buttonRemind->setTouchEnable(true);
// 	buttonRemind->setPressedActionEnabled(true);
// 	buttonRemind->addReleaseEvent(this,coco_releaseselector(GuideMap::showRemindContent));
// 	buttonRemind->setName("btn_remind");
// 	u_layer_FiveInstanceScene->addWidget(buttonRemind);
// 	buttonRemind->setVisible(true);

	//可缩回/弹出的层
	layer_action = UILayer::create();
	u_layer_FiveInstanceScene->addChild(layer_action);
	layer_action->setTag(kTagActionLayer);

	//召唤
	UIButton *btn_callFriend= UIButton::create();
	btn_callFriend->setTextures("gamescene_state/zhujiemian3/tubiao/callUP.png","gamescene_state/zhujiemian3/tubiao/callUP.png","");
	btn_callFriend->setAnchorPoint(ccp(0.5f,0.5f));
	btn_callFriend->setPressedActionEnabled(true);
	btn_callFriend->setTouchEnable(true);
	btn_callFriend->setPosition(ccp(winSize.width-30+origin.x,winSize.height-btn_generalMode->getContentSize().height-30+origin.x));
	btn_callFriend->addReleaseEvent(this,coco_releaseselector(GuideMap::CallFriendsEvent));
	btn_callFriend->setName("btn_callFriend");
	layer_action->addWidget(btn_callFriend);
	btn_callFriend->setVisible(true);
}

void GuideMap::createUIForSingCopyScene()
{
	isExistUIForSingCopyScene = true;
	winSize = CCDirector::sharedDirector()->getVisibleSize();
	CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

	UIButton *btn_exit = UIButton::create();
	btn_exit->setTextures("gamescene_state/zhujiemian3/tubiao/quit.png","gamescene_state/zhujiemian3/tubiao/quit.png","");
	btn_exit->setAnchorPoint(ccp(0.5f,0.5f));
	btn_exit->setPressedActionEnabled(true);
	btn_exit->setTouchEnable(true);
	btn_exit->setPosition(ccp(winSize.width-30+origin.x,winSize.height-btn_exit->getContentSize().height/2+origin.y));
	btn_exit->addReleaseEvent(this,coco_releaseselector(GuideMap::ExitSingCopy));
	btn_exit->setName("btn_exit");
	u_layer_SingCopyScene->addWidget(btn_exit);
	btn_exit->setVisible(true);

	UIButton *btn_shop = UIButton::create();
	btn_shop->setTextures("gamescene_state/zhujiemian3/tubiao/rmbshop.png","gamescene_state/zhujiemian3/tubiao/rmbshop.png","");
	btn_shop->setAnchorPoint(ccp(0.5f,0.5f));
	btn_shop->setPressedActionEnabled(true);
	btn_shop->setTouchEnable(true);
	btn_shop->setPosition(ccp(btn_exit->getPosition().x-50+origin.x,winSize.height-btn_exit->getContentSize().height/2+origin.y));
	btn_shop->addReleaseEvent(this,coco_releaseselector(GuideMap::ShopEvent));
	btn_shop->setName("btn_shop");
	u_layer_SingCopyScene->addWidget(btn_shop);
	btn_shop->setVisible(true);

	int hookIndex = MyPlayerAIConfig::getAutomaticSkill();
	UIButton* btn_hangUp = UIButton::create();
	if (hookIndex == 1)
	{
		btn_hangUp->setTextures("gamescene_state/zhujiemian3/tubiao/off_hook.png","gamescene_state/zhujiemian3/tubiao/off_hook.png","");
	}else
	{
		btn_hangUp->setTextures("gamescene_state/zhujiemian3/tubiao/on_hook.png","gamescene_state/zhujiemian3/tubiao/on_hook.png","");
	}
	btn_hangUp->setAnchorPoint(ccp(0.5f,0.5f));
	btn_hangUp->setPressedActionEnabled(true);
	btn_hangUp->setTouchEnable(true);
	btn_hangUp->setPosition(ccp(btn_shop->getPosition().x-50+origin.x,winSize.height-btn_exit->getContentSize().height/2+origin.y));
	btn_hangUp->addReleaseEvent(this,coco_releaseselector(GuideMap::HangUpEvent));
	btn_hangUp->setName("btn_hangUp_1");
	u_layer_SingCopyScene->addWidget(btn_hangUp);
	btn_hangUp->setVisible(true);
	//general model
	UIButton *btn_generalMode = UIButton::create();
	btn_generalMode->setTextures("gamescene_state/zhujiemian3/tubiao/driving.png","gamescene_state/zhujiemian3/tubiao/driving.png","");
	btn_generalMode->setAnchorPoint(ccp(0.5f,0.5f));
	btn_generalMode->setPressedActionEnabled(true);
	btn_generalMode->setTouchEnable(true);
	btn_generalMode->setPosition(ccp(btn_hangUp->getPosition().x-50+origin.x,winSize.height-btn_exit->getContentSize().height/2+origin.y));
	btn_generalMode->addReleaseEvent(this,coco_releaseselector(GuideMap::GeneralModeEvent));
	btn_generalMode->setName("btn_generalMode");
	u_layer_SingCopyScene->addWidget(btn_generalMode);

	setDefaultGeneralMode(CCUserDefault::sharedUserDefault()->getIntegerForKey("general_mode"));

	//remind
// 	buttonRemind = UIButton::create();
// 	buttonRemind->setAnchorPoint(ccp(0.5f,0.5f));
// 	buttonRemind->setPosition(ccp(btn_generalMode->getPosition().x-50+origin.x,winSize.height-btn_exit->getContentSize().height/2+origin.y));
// 	buttonRemind->setTextures("gamescene_state/zhujiemian3/tubiao/upup.png","gamescene_state/zhujiemian3/tubiao/upup.png","");
// 	buttonRemind->setTouchEnable(true);
// 	buttonRemind->setPressedActionEnabled(true);
// 	buttonRemind->addReleaseEvent(this,coco_releaseselector(GuideMap::showRemindContent));
// 	buttonRemind->setName("btn_remind");
// 	u_layer_SingCopyScene->addWidget(buttonRemind);
// 	buttonRemind->setVisible(true);
}


void GuideMap::createUIForArenaScene()
{
	isExistUIForArenaScene = true;
	winSize = CCDirector::sharedDirector()->getVisibleSize();
	CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

	//first line
	//退出
	UIButton *btn_quit = UIButton::create();
	btn_quit->setTextures("gamescene_state/zhujiemian3/tubiao/quit.png","gamescene_state/zhujiemian3/tubiao/quit.png","");
	btn_quit->setAnchorPoint(ccp(0.5f,0.5f));
	btn_quit->setPressedActionEnabled(true);
	btn_quit->setTouchEnable(true);
	btn_quit->setPosition(ccp(winSize.width-30+origin.x,winSize.height-btn_quit->getContentSize().height/2+origin.y));
	btn_quit->addReleaseEvent(this,coco_releaseselector(GuideMap::QuitArenaEvent));
	btn_quit->setName("btn_quit");
	u_layer_ArenaScene->addWidget(btn_quit);
	//挂机
	int hookIndex = MyPlayerAIConfig::getAutomaticSkill();
	UIButton* btn_hangUp = UIButton::create();
	if (hookIndex == 1)
	{
		btn_hangUp->setTextures("gamescene_state/zhujiemian3/tubiao/off_hook.png","gamescene_state/zhujiemian3/tubiao/off_hook.png","");
	}
	else
	{
		btn_hangUp->setTextures("gamescene_state/zhujiemian3/tubiao/on_hook.png","gamescene_state/zhujiemian3/tubiao/on_hook.png","");
	}
	btn_hangUp->setAnchorPoint(ccp(0.5f,0.5f));
	btn_hangUp->setPressedActionEnabled(true);
	btn_hangUp->setTouchEnable(true);
	btn_hangUp->setPosition(ccp(btn_quit->getPosition().x-60+origin.x,winSize.height-btn_quit->getContentSize().height/2+origin.y));
	btn_hangUp->addReleaseEvent(this,coco_releaseselector(GuideMap::HangUpEvent));
	btn_hangUp->setName("btn_hangUp_1");
	u_layer_ArenaScene->addWidget(btn_hangUp);
	btn_hangUp->setVisible(true);
	//武将模式
	UIButton *btn_generalMode = UIButton::create();
	btn_generalMode->setTextures("gamescene_state/zhujiemian3/tubiao/driving.png","gamescene_state/zhujiemian3/tubiao/driving.png","");
	btn_generalMode->setAnchorPoint(ccp(0.5f,0.5f));
	btn_generalMode->setPressedActionEnabled(true);
	btn_generalMode->setTouchEnable(true);
	btn_generalMode->setPosition(ccp(btn_hangUp->getPosition().x-60+origin.x,winSize.height-btn_hangUp->getContentSize().height/2+origin.y));
	btn_generalMode->addReleaseEvent(this,coco_releaseselector(GuideMap::GeneralModeEvent));
	btn_generalMode->setName("btn_generalMode");
	u_layer_ArenaScene->addWidget(btn_generalMode);

	setDefaultGeneralMode(CCUserDefault::sharedUserDefault()->getIntegerForKey("general_mode"));
}

void GuideMap::createUIForFamilyFightScene()
{
	isExistUIForFamilyFightScene = true;
	winSize = CCDirector::sharedDirector()->getVisibleSize();
	CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

	UIButton * map_diBg= UIButton::create();
	map_diBg->setTextures("gamescene_state/zhujiemian3/ditu/ditu_di.png","gamescene_state/zhujiemian3/ditu/ditu_di.png","");
	map_diBg->setAnchorPoint(ccp(0.5f,0.5f));
	map_diBg->setScale9Enable(true);
	map_diBg->setCapInsets(CCRectMake(21,18,1,1));
	map_diBg->setScale9Size(CCSizeMake(153,44));
	map_diBg->setName("map_diBg");
	u_layer_FamilyFightScene->addWidget(map_diBg);
	map_diBg->setTouchEnable(true);
	map_diBg->addReleaseEvent(this,coco_releaseselector(GuideMap::GoToMapScene));
	map_diBg->setPressedActionEnabled(true,0.85f,1.2f);
	map_diBg->setPosition(ccp(winSize.width-map_diBg->getSize().width/2+origin.x,winSize.height-map_diBg->getSize().height/2+origin.y));

	//地图名称
	//UILabelBMFont * mapName = UILabelBMFont::create();
	//mapName->setFntFile("res_ui/font/ziti_3.fnt");
	UILabel * mapName = UILabel::create();
	mapName->setFontName(APP_FONT_NAME);
	mapName->setFontSize(15);
	mapName->setColor(ccc3(255,216,61));  //(247,151,30) // (218,105,6)
	mapName->setText(GameView::getInstance()->getMapInfo()->mapname().c_str());
	mapName->setAnchorPoint(ccp(0.5f,0.5f));
	mapName->setPosition(ccp(0,-3));
	mapName->setName("mapName");
	map_diBg->addChild(mapName);

	int countryId = GameView::getInstance()->getMapInfo()->country();
	if (countryId > 0 && countryId <6)
	{
		std::string countryIdName = "country_";
		char id[2];
		sprintf(id, "%d", countryId);
		countryIdName.append(id);
		std::string iconPathName = "res_ui/country_icon/";
		iconPathName.append(countryIdName);
		iconPathName.append(".png");
		UIImageView * countrySp_ = UIImageView::create();
		countrySp_->setTexture(iconPathName.c_str());
		countrySp_->setAnchorPoint(ccp(1.0,0.5f));
		countrySp_->setPosition(ccp(mapName->getPosition().x - mapName->getContentSize().width/2-1,mapName->getPosition().y));
		countrySp_->setScale(0.7f);
		countrySp_->setName("countrySp_");
		map_diBg->addChild(countrySp_);
	}
	else
	{
		mapName->setPosition(ccp(-10,-3));
	}

	std::string str_channelId = "(";
	char s_channelId[5];
	sprintf(s_channelId,"%d",GameView::getInstance()->getMapInfo()->channel()+1);
	str_channelId.append(s_channelId);
	str_channelId.append(StringDataManager::getString("GuideMap_Channel_Name"));
	str_channelId.append(")");
	UILabel * l_channel = UILabel::create();
	l_channel->setFontName(APP_FONT_NAME);
	l_channel->setFontSize(15);
	l_channel->setColor(ccc3(255,216,61));  //(247,151,30) // (218,105,6)
	l_channel->setText(str_channelId.c_str());
	l_channel->setAnchorPoint(ccp(0.f,0.5f));
	l_channel->setPosition(ccp(mapName->getPosition().x + mapName->getContentSize().width/2-1,mapName->getPosition().y));
	l_channel->setName("l_channel");
	map_diBg->addChild(l_channel);

	//各个副本按钮等
	//first line
	//商城
	UIButton *btn_shop = UIButton::create();
	btn_shop->setTextures("gamescene_state/zhujiemian3/tubiao/rmbshop.png","gamescene_state/zhujiemian3/tubiao/rmbshop.png","");
	btn_shop->setAnchorPoint(ccp(0.5f,0.5f));
	btn_shop->setPressedActionEnabled(true);
	btn_shop->setTouchEnable(true);
	btn_shop->setPosition(ccp(winSize.width-160+origin.x,winSize.height-btn_shop->getContentSize().height/2+origin.y));
	btn_shop->addReleaseEvent(this,coco_releaseselector(GuideMap::ShopEvent));
	btn_shop->setName("btn_shop");
	u_layer_FamilyFightScene->addWidget(btn_shop);
	//挂机
	bool aaa = MyPlayerAIConfig::isEnableRobot();
	int hookIndex = MyPlayerAIConfig::getAutomaticSkill();
	UIButton* btn_hangUp = UIButton::create();
	if (hookIndex == 1)
	{
		btn_hangUp->setTextures("gamescene_state/zhujiemian3/tubiao/off_hook.png","gamescene_state/zhujiemian3/tubiao/off_hook.png","");
	}else
	{
		btn_hangUp->setTextures("gamescene_state/zhujiemian3/tubiao/on_hook.png","gamescene_state/zhujiemian3/tubiao/on_hook.png","");
	}
	btn_hangUp->setAnchorPoint(ccp(0.5f,0.5f));
	btn_hangUp->setPressedActionEnabled(true);
	btn_hangUp->setTouchEnable(true);
	btn_hangUp->setPosition(ccp(btn_shop->getPosition().x-50+origin.x,winSize.height-btn_hangUp->getContentSize().height/2+origin.y));
	btn_hangUp->addReleaseEvent(this,coco_releaseselector(GuideMap::HangUpEvent));
	btn_hangUp->setName("btn_hangUp_1");
	u_layer_FamilyFightScene->addWidget(btn_hangUp);

	//武将模式
	UIButton *btn_generalMode = UIButton::create();
	btn_generalMode->setTextures("gamescene_state/zhujiemian3/tubiao/driving.png","gamescene_state/zhujiemian3/tubiao/driving.png","");
	btn_generalMode->setAnchorPoint(ccp(0.5f,0.5f));
	btn_generalMode->setPressedActionEnabled(true);
	btn_generalMode->setTouchEnable(true);
	btn_generalMode->setPosition(ccp(btn_hangUp->getPosition().x-50+origin.x,winSize.height-btn_hangUp->getContentSize().height/2+origin.y));
	btn_generalMode->addReleaseEvent(this,coco_releaseselector(GuideMap::GeneralModeEvent));
	btn_generalMode->setName("btn_generalMode");
	u_layer_FamilyFightScene->addWidget(btn_generalMode);

	setDefaultGeneralMode(CCUserDefault::sharedUserDefault()->getIntegerForKey("general_mode"));

	//退出
	UIButton *btn_quit = UIButton::create();
	btn_quit->setTextures("gamescene_state/zhujiemian3/tubiao/quit.png","gamescene_state/zhujiemian3/tubiao/quit.png","");
	btn_quit->setAnchorPoint(ccp(0.5f,0.5f));
	btn_quit->setPressedActionEnabled(true);
	btn_quit->setTouchEnable(true);
	btn_quit->setPosition(ccp(winSize.width-30+origin.x,winSize.height- map_diBg->getSize().height -btn_quit->getContentSize().height/2-2+origin.y));
	btn_quit->addReleaseEvent(this,coco_releaseselector(GuideMap::QuitFamilyFightEvent));
	btn_quit->setName("btn_quit");
	u_layer_FamilyFightScene->addWidget(btn_quit);
	//战况
	UIButton *btn_battleSituation = UIButton::create();
	btn_battleSituation->setTextures("gamescene_state/zhujiemian3/tubiao/zhankuang.png","gamescene_state/zhujiemian3/tubiao/zhankuang.png","");
	btn_battleSituation->setAnchorPoint(ccp(0.5f,0.5f));
	btn_battleSituation->setPressedActionEnabled(true);
	btn_battleSituation->setTouchEnable(true);
	btn_battleSituation->setPosition(ccp(btn_quit->getPosition().x-50+origin.x,btn_quit->getPosition().y));
	btn_battleSituation->addReleaseEvent(this,coco_releaseselector(GuideMap::BattleSituationEvent));
	btn_battleSituation->setName("btn_battleSituation");
	u_layer_FamilyFightScene->addWidget(btn_battleSituation);
}

UIButton * GuideMap::getBtnRemind()
{
	if (u_layer_MainScene->isVisible())
	{
		UIButton * btn_temp = (UIButton *)u_layer_MainScene->getWidgetByName("btn_remind");
		if (btn_temp)
			return btn_temp;
	}

	if (u_layer_FiveInstanceScene->isVisible())
	{
		UIButton * btn_temp = (UIButton *)u_layer_FiveInstanceScene->getWidgetByName("btn_remind");
		if (btn_temp)
			return btn_temp;
	}

	if (u_layer_ArenaScene->isVisible())
	{
		UIButton * btn_temp = (UIButton *)u_layer_ArenaScene->getWidgetByName("btn_remind");
		if (btn_temp)
			return btn_temp;
	}

	if (u_layer_SingCopyScene->isVisible())
	{
		UIButton * btn_temp = (UIButton *)u_layer_SingCopyScene->getWidgetByName("btn_remind");
		if (btn_temp)
			return btn_temp;
	}

	return NULL;
}

void GuideMap::QuitArenaEvent( CCObject *pSender )
{
	//add by yangjun 2014.10.13
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		if (mainScene->isHeadMenuOn)
		{
			mainScene->ButtonHeadEvent(NULL);
		}
	}

	GameView::getInstance()->showPopupWindow(StringDataManager::getString("OffLineArenaUI_quiteWillFail"),2,this,coco_selectselector(GuideMap::SureToQuitArena),NULL,PopupWindow::KTypeRemoveByTime,10);
}

void GuideMap::SureToQuitArena( CCObject *pSender )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(3021);
}


void GuideMap::QuitFamilyFightEvent( CCObject *pSender )
{
	//add by yangjun 2014.10.13
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		if (mainScene->isHeadMenuOn)
		{
			mainScene->ButtonHeadEvent(NULL);
		}
	}

	GameView::getInstance()->showPopupWindow(StringDataManager::getString("FamilyFightUI_areYouSureToQuit"),2,this,coco_selectselector(GuideMap::SureToQuitFamilyFight),NULL,PopupWindow::KTypeRemoveByTime,10);
}

void GuideMap::SureToQuitFamilyFight( CCObject *pSender )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1541,(void *)FamilyFightData::getCurrentBattleId());
}


void GuideMap::GeneralModeEvent( CCObject *pSender )
{
// 	if (m_generalMode == 1)
// 	{
// 		GameMessageProcessor::sharedMsgProcessor()->sendReq(1206,(void *)2);
// 	}
// 	else
// 	{
// 		GameMessageProcessor::sharedMsgProcessor()->sendReq(1206,(void *)1);
// 	}
// 
	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(pSender);

	//add by yangjun 2014.10.13
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		if (mainScene->isHeadMenuOn)
		{
			mainScene->ButtonHeadEvent(NULL);
		}
	}

	if (this->getChildByTag(KTagGeneralsModeUI) == NULL)
	{
		int mode = CCUserDefault::sharedUserDefault()->getIntegerForKey("general_mode");
		int autofight = CCUserDefault::sharedUserDefault()->getIntegerForKey("general_autoFight");
		GeneralsModeUI* temp = GeneralsModeUI::create(mode,autofight);
		temp->ignoreAnchorPointForPosition(false);
		temp->setAnchorPoint(ccp(.5f,1.0f));
		temp->setPosition(ccp(this->getBtnGeneralMode()->getPosition().x,this->getBtnGeneralMode()->getPosition().y - this->getBtnGeneralMode()->getContentSize().height/2));
		temp->setTag(KTagGeneralsModeUI);
		this->addChild(temp);
	}
}
void GuideMap::applyGeneralMode( int idx,int autoFight )
{
	m_generalMode = idx;
	m_autoFight = autoFight;

// 	std::string str_texture_path = "gamescene_state/zhujiemian3/tubiao/";
// 	switch(idx)
// 	{
// 	case 1:   //跟随
// 		{
// 			str_texture_path.append("assistance.png");
// 			GameView::getInstance()->showAlertDialog(StringDataManager::getString("generals_mode_1"));
// 		}
// 		break;
// 	case 2:   //攻击
// 		{
// 			str_texture_path.append("driving.png");
// 			GameView::getInstance()->showAlertDialog(StringDataManager::getString("generals_mode_2"));
// 		}
// 		break;
// 	case 3:  //休息
// 		{
// 			str_texture_path.append("assistance.png");
// 			GameView::getInstance()->showAlertDialog(StringDataManager::getString("generals_mode_1"));
// 		}
// 		break;
// 	default:
// 		str_texture_path.append("assistance.png");
// 	}
// 
// 	setAllBtnGeneralModeTextures(str_texture_path.c_str());

	GeneralsModeUI* temp = (GeneralsModeUI*)this->getChildByTag(KTagGeneralsModeUI);
	if (temp)
	{
		temp->applyGeneralMode(m_generalMode,m_autoFight);
	}
}


void GuideMap::setDefaultGeneralMode( int idx )
{
	m_generalMode = idx;

// 	std::string str_texture_path = "gamescene_state/zhujiemian3/tubiao/";
// 	switch(idx)
// 	{
// 	case 1:   //跟随
// 		{
// 			str_texture_path.append("assistance.png");
// 		}
// 		break;
// 	case 2:   //攻击
// 		{
// 			str_texture_path.append("driving.png");
// 		}
// 		break;
// 	case 3:  //休息
// 		{
// 			str_texture_path.append("assistance.png");
// 		}
// 		break;
// 	default:
// 		str_texture_path.append("assistance.png");
// 	}

//	setAllBtnGeneralModeTextures(str_texture_path.c_str());
}

std::string GuideMap::timeFormatToString( int t )
{
	int int_h = t/60/60;
	int int_m = (t%3600)/60;
	int int_s = (t%3600)%60;

	std::string timeString = "";
	char str_h[10];
	sprintf(str_h,"%d",int_h);
	if (int_h <= 0)
	{

	}
	else if (int_h > 0 && int_h < 10)
	{
		timeString.append("0");
		timeString.append(str_h);
		timeString.append(":");
	}
	else
	{
		timeString.append(str_h);
		timeString.append(":");
	}

	char str_m[10];
	sprintf(str_m,"%d",int_m);
	if (int_m >= 0 && int_m < 10)
	{
		timeString.append("0");
		timeString.append(str_m);
	}
	else
	{
		timeString.append(str_m);
	}
	timeString.append(":");

	char str_s[10];
	sprintf(str_s,"%d",int_s);
	if (int_s >= 0 && int_s < 10)
	{
		timeString.append("0");
		timeString.append(str_s);
	}
	else
	{
		timeString.append(str_s);
	}

	return timeString;
}


void GuideMap::FirstBuyVipEvent(CCObject *pSender)
{
	//add by yangjun 2014.10.13
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		if (mainScene->isHeadMenuOn)
		{
			mainScene->ButtonHeadEvent(NULL);
		}
	}

	// 初始化 UI WINDOW数据
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

	FirstBuyVipUI* firstBuyVipUI = (FirstBuyVipUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFirstBuyVipUI);
	if (firstBuyVipUI == NULL)
	{
		FirstBuyVipUI * temp = FirstBuyVipUI::create();
		temp->ignoreAnchorPointForPosition(false);
		temp->setAnchorPoint(ccp(0.5f, 0.5f));
		temp->setPosition(ccp(winSize.width / 2, winSize.height / 2));
		temp->setTag(kTagFirstBuyVipUI);
		GameView::getInstance()->getMainUIScene()->addChild(temp);
	}
}

int GuideMap::getGeneralMode()
{
	return m_generalMode;
}

int GuideMap::getGeneralAutoFight()
{
	return m_autoFight;
}

UILayer * GuideMap::getCurActiveLayer()
{
	if (u_layer_MainScene->isVisible())
	{
		return u_layer_MainScene;
	}

	if (u_layer_FiveInstanceScene->isVisible())
	{
		return u_layer_FiveInstanceScene;
	}

	if (u_layer_ArenaScene->isVisible())
	{
		return u_layer_ArenaScene;
	}

	if (u_layer_FamilyFightScene->isVisible())
	{
		return u_layer_FamilyFightScene;
	}

	if (u_layer_SingCopyScene->isVisible())
	{
		return u_layer_SingCopyScene;
	}
	return NULL;
}

UILayer * GuideMap::getActionLayer()
{
	if (u_layer_MainScene->isVisible())
	{
		UILayer * tempLayer = (UILayer *)u_layer_MainScene->getChildByTag(kTagActionLayer);
		if (tempLayer)
			return tempLayer;
	}
	
	if (u_layer_FiveInstanceScene->isVisible())
	{
		UILayer * tempLayer = (UILayer *)u_layer_FiveInstanceScene->getChildByTag(kTagActionLayer);
		if (tempLayer)
			return tempLayer;
	}


	return NULL;
}

UIButton * GuideMap::getBtnHangUp()
{
	if (u_layer_MainScene->isVisible())
	{
		UIButton * btn_temp = (UIButton *)u_layer_MainScene->getWidgetByName("btn_hangUp_1");
		if (btn_temp)
			return btn_temp;
	}

	if (u_layer_FiveInstanceScene->isVisible())
	{
		UIButton * btn_temp = (UIButton *)u_layer_FiveInstanceScene->getWidgetByName("btn_hangUp_1");
		if (btn_temp)
			return btn_temp;
	}

	if (u_layer_ArenaScene->isVisible())
	{
		UIButton * btn_temp = (UIButton *)u_layer_ArenaScene->getWidgetByName("btn_hangUp_1");
		if (btn_temp)
			return btn_temp;
	}

	if (u_layer_FamilyFightScene->isVisible())
	{
		UIButton * btn_temp = (UIButton *)u_layer_FamilyFightScene->getWidgetByName("btn_hangUp_1");
		if (btn_temp)
			return btn_temp;
	}

	if (u_layer_SingCopyScene->isVisible())
	{
		UIButton * btn_temp = (UIButton *)u_layer_SingCopyScene->getWidgetByName("btn_hangUp_1");
		if (btn_temp)
			return btn_temp;
	}

	return NULL;
}

UIButton * GuideMap::getBtnGeneralMode()
{
	if (u_layer_MainScene->isVisible())
	{
		UIButton * btn_temp = (UIButton *)u_layer_MainScene->getWidgetByName("btn_generalMode");
		if (btn_temp)
			return btn_temp;
	}

	if (u_layer_FiveInstanceScene->isVisible())
	{
		UIButton * btn_temp = (UIButton *)u_layer_FiveInstanceScene->getWidgetByName("btn_generalMode");
		if (btn_temp)
			return btn_temp;
	}

	if (u_layer_ArenaScene->isVisible())
	{
		UIButton * btn_temp = (UIButton *)u_layer_ArenaScene->getWidgetByName("btn_generalMode");
		if (btn_temp)
			return btn_temp;
	}

	if (u_layer_FamilyFightScene->isVisible())
	{
		UIButton * btn_temp = (UIButton *)u_layer_FamilyFightScene->getWidgetByName("btn_generalMode");
		if (btn_temp)
			return btn_temp;
	}

	if (u_layer_SingCopyScene->isVisible())
	{
		UIButton * btn_temp = (UIButton *)u_layer_SingCopyScene->getWidgetByName("btn_generalMode");
		if (btn_temp)
			return btn_temp;
	}
	return NULL;
}

void GuideMap::setAllBtnHangUpTextures(const char * str )
{
	UIButton * btn_temp = (UIButton *)u_layer_MainScene->getWidgetByName("btn_hangUp_1");
	if (btn_temp)
		btn_temp->setTextures(str,str,"");

	btn_temp = (UIButton *)u_layer_FiveInstanceScene->getWidgetByName("btn_hangUp_1");
	if (btn_temp)
		btn_temp->setTextures(str,str,"");

	btn_temp = (UIButton *)u_layer_ArenaScene->getWidgetByName("btn_hangUp_1");
	if (btn_temp)
		btn_temp->setTextures(str,str,"");

	btn_temp = (UIButton *)u_layer_FamilyFightScene->getWidgetByName("btn_hangUp_1");
	if (btn_temp)
		btn_temp->setTextures(str,str,"");

	btn_temp = (UIButton *)u_layer_SingCopyScene->getWidgetByName("btn_hangUp_1");
	if (btn_temp)
		btn_temp->setTextures(str,str,"");

	// 判断是否 去掉 自动战斗 的anm
	


	MyPlayer* pMyPlayer = GameView::getInstance()->myplayer;

}

void GuideMap::setAllBtnGeneralModeTextures(const char * str )
{
	UIButton * btn_temp = (UIButton *)u_layer_MainScene->getWidgetByName("btn_generalMode");
	if (btn_temp)
		btn_temp->setTextures(str,str,"");

	btn_temp = (UIButton *)u_layer_FiveInstanceScene->getWidgetByName("btn_generalMode");
	if (btn_temp)
		btn_temp->setTextures(str,str,"");

	btn_temp = (UIButton *)u_layer_ArenaScene->getWidgetByName("btn_generalMode");
	if (btn_temp)
		btn_temp->setTextures(str,str,"");

	btn_temp = (UIButton *)u_layer_FamilyFightScene->getWidgetByName("btn_generalMode");
	if (btn_temp)
		btn_temp->setTextures(str,str,"");

	btn_temp = (UIButton *)u_layer_SingCopyScene->getWidgetByName("btn_generalMode");
	if (btn_temp)
		btn_temp->setTextures(str,str,"");
}

void GuideMap::RefreshMapName()
{
	if (!u_layer_MainScene->isVisible())
		return;

	if (u_layer_MainScene->getWidgetByName("map_diBg"))
	{
		UILabel * mapName = (UILabel*)u_layer_MainScene->getWidgetByName("map_diBg")->getChildByName("mapName");
		if (!mapName)
			return;

		mapName->setText(GameView::getInstance()->getMapInfo()->mapname().c_str());

		UIImageView * countrySp_ = (UIImageView*)u_layer_MainScene->getWidgetByName("map_diBg")->getChildByName("countrySp_");
		if (countrySp_)
		{
			int countryId = GameView::getInstance()->getMapInfo()->country();
			if (countryId > 0 && countryId <6)
			{
				std::string countryIdName = "country_";
				char id[2];
				sprintf(id, "%d", countryId);
				countryIdName.append(id);
				std::string iconPathName = "res_ui/country_icon/";
				iconPathName.append(countryIdName);
				iconPathName.append(".png");
				countrySp_->setTexture(iconPathName.c_str());
				countrySp_->setPosition(ccp(mapName->getPosition().x - mapName->getContentSize().width/2-1,mapName->getPosition().y));
			}
			else
			{
				countrySp_->removeFromParent();
				mapName->setPosition(ccp(0,-3));
			}
		}
		else
		{
			int countryId = GameView::getInstance()->getMapInfo()->country();
			if (countryId > 0 && countryId <6)
			{
				std::string countryIdName = "country_";
				char id[2];
				sprintf(id, "%d", countryId);
				countryIdName.append(id);
				std::string iconPathName = "res_ui/country_icon/";
				iconPathName.append(countryIdName);
				iconPathName.append(".png");
				UIImageView * countrySp_ = UIImageView::create();
				countrySp_->setTexture(iconPathName.c_str());
				countrySp_->setAnchorPoint(ccp(1.0,0.5f));
				countrySp_->setPosition(ccp(mapName->getPosition().x - mapName->getContentSize().width/2-1,mapName->getPosition().y));
				countrySp_->setScale(0.7f);
				countrySp_->setName("countrySp_");
				u_layer_MainScene->getWidgetByName("map_diBg")->addChild(countrySp_);
			}
			else
			{
				mapName->setPosition(ccp(0,-3));
			}
		}

		UILabel * l_channel = (UILabel*)u_layer_MainScene->getWidgetByName("map_diBg")->getChildByName("l_channel");
		if (l_channel)
		{
			std::string str_channelId = "(";
			char s_channelId[5];
			sprintf(s_channelId,"%d",GameView::getInstance()->getMapInfo()->channel()+1);
			str_channelId.append(s_channelId);
			str_channelId.append(StringDataManager::getString("GuideMap_Channel_Name"));
			str_channelId.append(")");
			l_channel->setText(str_channelId.c_str());
			l_channel->setPosition(ccp(mapName->getPosition().x + mapName->getContentSize().width/2-1,mapName->getPosition().y));
		}
	}
}

void GuideMap::BattleSituationEvent( CCObject *pSender )
{
	//add by yangjun 2014.10.13
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		if (mainScene->isHeadMenuOn)
		{
			mainScene->ButtonHeadEvent(NULL);
		}
	}

	// 初始化 UI WINDOW数据
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

	BattleSituationUI* battleSituationUI = (BattleSituationUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBattleSituationUI);
	if (battleSituationUI == NULL)
	{
		battleSituationUI = BattleSituationUI::create();
		battleSituationUI->ignoreAnchorPointForPosition(false);
		battleSituationUI->setAnchorPoint(ccp(0.5f, 0.5f));
		battleSituationUI->setPosition(ccp(winSize.width / 2, winSize.height / 2));
		battleSituationUI->setTag(kTagBattleSituationUI);
		GameView::getInstance()->getMainUIScene()->addChild(battleSituationUI);
	}
}

void GuideMap::callBackGetReward( CCObject * pSender )
{
	//add by yangjun 2014.10.13
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		if (mainScene->isHeadMenuOn)
		{
			mainScene->ButtonHeadEvent(NULL);
		}
	}

	RewardUi * reward_ui = (RewardUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRewardUI);
	if (reward_ui == NULL)
	{
		RewardUi * reward_ui = RewardUi::create();
		reward_ui->ignoreAnchorPointForPosition(false);
		reward_ui->setAnchorPoint(ccp(0.5f, 0.5f));
		reward_ui->setPosition(ccp(winSize.width / 2, winSize.height / 2));
		reward_ui->setTag(kTagRewardUI);
		GameView::getInstance()->getMainUIScene()->addChild(reward_ui);
	}
}

void GuideMap::changeBtnVisibleByType( int mapType )
{
	switch(mapType)
	{
	case com::future::threekingdoms::server::transport::protocol::coliseum:           //竞技场
		{
			//退出
			UIButton *btn_quit = (UIButton*)u_layer_ArenaScene->getWidgetByName("btn_quit");
			if (btn_quit)
			{
				btn_quit->setVisible(true);
			}
			
			//挂机
			UIButton* btn_hangUp = (UIButton*)u_layer_ArenaScene->getWidgetByName("btn_hangUp_1");
			if (btn_hangUp)
			{
				btn_hangUp->setVisible(true);
				UIButton * btn_temp = (UIButton*)u_layer_MainScene->getWidgetByName("btn_hangUp_1");
				if (btn_temp)
				{
					if (!btn_temp->isVisible())
					{
						btn_hangUp->setVisible(false);
					}
				}
			}

			//武将模式
			UIButton *btn_generalMode = (UIButton*)u_layer_ArenaScene->getWidgetByName("btn_generalMode");
			if (btn_generalMode)
			{
				btn_generalMode->setVisible(true);
				UIButton * btn_temp = (UIButton*)u_layer_MainScene->getWidgetByName("btn_generalMode");
				if (btn_temp)
				{
					if (!btn_temp->isVisible())
					{
						btn_generalMode->setVisible(false);
					}
				}
			}
		}
		break;
	case com::future::threekingdoms::server::transport::protocol::copy:        //副本
		{
			//各个副本按钮等
			//first line
			//退出
			UIButton *btn_quit = (UIButton*)u_layer_FiveInstanceScene->getWidgetByName("btn_exit");
			if (btn_quit)
			{
				btn_quit->setVisible(true);
			}
			//商城
			UIButton* btn_shop = (UIButton*)u_layer_FiveInstanceScene->getWidgetByName("btn_shop");
			if (btn_shop)
			{
				btn_shop->setVisible(true);
				UIButton * btn_temp = (UIButton*)u_layer_MainScene->getWidgetByName("btn_shop");
				if (btn_temp)
				{
					if (!btn_temp->isVisible())
					{
						btn_shop->setVisible(false);
					}
				}
			}

			//挂机
			UIButton* btn_hangUp = (UIButton*)u_layer_FiveInstanceScene->getWidgetByName("btn_hangUp_1");
			if (btn_hangUp)
			{
				btn_hangUp->setVisible(true);
				UIButton * btn_temp = (UIButton*)u_layer_MainScene->getWidgetByName("btn_hangUp_1");
				if (btn_temp)
				{
					if (!btn_temp->isVisible())
					{
						btn_hangUp->setVisible(false);
					}
				}
			}

			//武将模式
			UIButton *btn_generalMode = (UIButton*)u_layer_FiveInstanceScene->getWidgetByName("btn_generalMode");
			if (btn_generalMode)
			{
				btn_generalMode->setVisible(true);
				UIButton * btn_temp = (UIButton*)u_layer_MainScene->getWidgetByName("btn_generalMode");
				if (btn_temp)
				{
					if (!btn_temp->isVisible())
					{
						btn_generalMode->setVisible(false);
					}
				}
			}

			//remind
			UIButton *btn_remind = (UIButton*)u_layer_FiveInstanceScene->getWidgetByName("btn_remind");
			if (btn_remind)
			{
				btn_remind->setVisible(true);
				UIButton * btn_temp = (UIButton*)u_layer_MainScene->getWidgetByName("btn_remind");
				if (btn_temp)
				{
					if (!btn_temp->isVisible())
					{
						btn_remind->setVisible(false);
					}
				}
			}

			//可缩回/弹出的层
			UILayer * layer_action_temp = (UILayer*)u_layer_FiveInstanceScene->getChildByTag(kTagActionLayer);
			if (layer_action_temp)
			{
				//召唤
				UIButton *btn_callFriend = (UIButton*)layer_action_temp->getWidgetByName("btn_callFriend");
				btn_callFriend->setVisible(true);
			}
		}
		break;
	case com::future::threekingdoms::server::transport::protocol::tower:        //singCopy
		{
			//退出
			UIButton *btn_exit = (UIButton*)u_layer_SingCopyScene->getWidgetByName("btn_exit");
			if (btn_exit)
			{
				btn_exit->setVisible(true);
			}
			//
			UIButton* btn_shop = (UIButton*)u_layer_SingCopyScene->getWidgetByName("btn_shop");
			if (btn_shop)
			{
				btn_shop->setVisible(true);
				UIButton * btn_temp = (UIButton*)u_layer_MainScene->getWidgetByName("btn_shop");
				if (btn_temp)
				{
					if (!btn_temp->isVisible())
					{
						btn_shop->setVisible(false);
					}
				}
			}
			//挂机
			UIButton* btn_hangUp = (UIButton*)u_layer_SingCopyScene->getWidgetByName("btn_hangUp_1");
			if (btn_hangUp)
			{
				btn_hangUp->setVisible(true);
				UIButton * btn_temp = (UIButton*)u_layer_MainScene->getWidgetByName("btn_hangUp_1");
				if (btn_temp)
				{
					if (!btn_temp->isVisible())
					{
						btn_hangUp->setVisible(false);
					}
				}
			}
			//武将模式
			UIButton *btn_generalMode = (UIButton*)u_layer_SingCopyScene->getWidgetByName("btn_generalMode");
			if (btn_generalMode)
			{
				btn_generalMode->setVisible(true);
				UIButton * btn_temp = (UIButton*)u_layer_MainScene->getWidgetByName("btn_generalMode");
				if (btn_temp)
				{
					if (!btn_temp->isVisible())
					{
						btn_generalMode->setVisible(false);
					}
				}
			}
			//remind
			UIButton *btn_remind = (UIButton*)u_layer_SingCopyScene->getWidgetByName("btn_remind");
			if (btn_remind)
			{
				btn_remind->setVisible(true);
				UIButton * btn_temp = (UIButton*)u_layer_MainScene->getWidgetByName("btn_remind");
				if (btn_temp)
				{
					if (!btn_temp->isVisible())
					{
						btn_remind->setVisible(false);
					}
				}
			}
		}break;
	case com::future::threekingdoms::server::transport::protocol::guildFight:        //家族战
		{
			//first line
			//商城
			UIButton *btn_shop = (UIButton*)u_layer_FamilyFightScene->getWidgetByName("btn_shop");
			if (btn_shop)
			{
				btn_shop->setVisible(true);
				UIButton * btn_temp = (UIButton*)u_layer_MainScene->getWidgetByName("btn_shop");
				if (btn_temp)
				{
					if (!btn_temp->isVisible())
					{
						btn_shop->setVisible(false);
					}
				}
			}

			//挂机
			UIButton* btn_hangUp = (UIButton*)u_layer_FamilyFightScene->getWidgetByName("btn_hangUp_1");
			if (btn_hangUp)
			{
				btn_hangUp->setVisible(true);
				UIButton * btn_temp = (UIButton*)u_layer_MainScene->getWidgetByName("btn_hangUp_1");
				if (btn_temp)
				{
					if (!btn_temp->isVisible())
					{
						btn_hangUp->setVisible(false);
					}
				}
			}

			//武将模式
			UIButton *btn_generalMode = (UIButton*)u_layer_FamilyFightScene->getWidgetByName("btn_generalMode");
			if (btn_generalMode)
			{
				btn_generalMode->setVisible(true);
				UIButton * btn_temp = (UIButton*)u_layer_MainScene->getWidgetByName("btn_generalMode");
				if (btn_temp)
				{
					if (!btn_temp->isVisible())
					{
						btn_generalMode->setVisible(false);
					}
				}
			}

			//退出
			UIButton *btn_quit = (UIButton*)u_layer_FamilyFightScene->getWidgetByName("btn_quit");
			if (btn_quit)
			{
				btn_quit->setVisible(true);
			}
			//战况
			UIButton *btn_battleSituation = (UIButton*)u_layer_FamilyFightScene->getWidgetByName("btn_battleSituation");
			if (btn_battleSituation)
			{
				btn_battleSituation->setVisible(true);
			}	
		}
		break;
	default:
		{
		}
	}
}

void GuideMap::addCCTutorialIndicatorSingCopy( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,54,54,true);
	tutorialIndicator->setDrawNodePos(pos);
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

UIButton * GuideMap::getBtnFirstReCharge()
{
	if (u_layer_MainScene)
	{
		UILayer * tempLayer = (UILayer *)u_layer_MainScene->getChildByTag(kTagActionLayer);
		if (tempLayer)
		{
			UIButton * btn_temp = (UIButton *)tempLayer->getWidgetByName("btn_firstBuyVip");
			if (btn_temp)
				return btn_temp;
		}
	}
	
	return NULL;
}
