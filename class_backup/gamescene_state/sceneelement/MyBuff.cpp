#include "MyBuff.h"
#include "BuffCell.h"
#include "../../utils/StaticDataManager.h"
#include "../exstatus/ExStatus.h"
#include "GameView.h"
#include "../role/MyPlayer.h"
#include "BaseBuff.h"
#include "AppMacros.h"

MyBuff::MyBuff()
{
}


MyBuff::~MyBuff()
{
}


MyBuff * MyBuff::create()
{
	MyBuff * myBuff = new MyBuff();
	if (myBuff && myBuff->init())
	{
		myBuff->autorelease();
		return myBuff;
	}
	CC_SAFE_DELETE(myBuff);
	return NULL;
}

bool MyBuff::init()
{
	if (UIScene::init())
	{
		CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
		CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

		UIImageView * bg = UIImageView::create();
		bg->setTexture("gamescene_state/zhujiemian3/renwuduiwu/renwu_di.png");
		bg->setAnchorPoint(CCPointZero);
		bg->setPosition(CCPointZero);
		bg->setScale9Enable(true);
		bg->setCapInsets(CCRectMake(12,12,1,1));
		bg->setScale9Size(CCSizeMake(217,220));
		m_pUiLayer->addWidget(bg);

		l_dialog = UILabel::create();
		l_dialog->setFontName(APP_FONT_NAME);
		l_dialog->setFontSize(20);
		l_dialog->setText(StringDataManager::getString("myBuff_noEx"));
		l_dialog->setAnchorPoint(ccp(0.5f,0.5f));
		l_dialog->setPosition(ccp(108,110));
		bg->addChild(l_dialog);
		l_dialog->setVisible(false);

		tableView = CCTableView::create(this,CCSizeMake(217,212));
		tableView->setDirection(kCCScrollViewDirectionVertical);
		tableView->setAnchorPoint(ccp(0,0));
		tableView->setPosition(ccp(0,4));
		tableView->setDelegate(this);
		tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		this->addChild(tableView);

		this->ignoreAnchorPointForPosition(false);
		this->setAnchorPoint(ccp(0,0));
		this->setPosition(ccp(0,0));
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(CCSizeMake(217,220));
		
		return true;
	}
	return false;
}



void MyBuff::onEnter()
{
	UIScene::onEnter();
}
void MyBuff::onExit()
{
	UIScene::onExit();
}

bool MyBuff::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
    return this->resignFirstResponder(pTouch,this,false);
}
void MyBuff::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
{

}
void MyBuff::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
{

}
void MyBuff::ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent)
{

}


CCSize MyBuff::tableCellSizeForIndex(CCTableView *table, unsigned int idx)
{
	return CCSizeMake(210,71);
}

CCTableViewCell* MyBuff::tableCellAtIndex(CCTableView *table, unsigned int idx)
{
	CCString *string = CCString::createWithFormat("%d", idx);
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	ExStatus * exStatus = GameView::getInstance()->myplayer->getExStatusVector().at(idx);
	cell = BuffCell::create(exStatus);

	return cell;

}

unsigned int MyBuff::numberOfCellsInTableView(CCTableView *table)
{
	if (GameView::getInstance()->myplayer->getExStatusVector().size()>0)
	{
		l_dialog->setVisible(false);
		return GameView::getInstance()->myplayer->getExStatusVector().size();
	}
	else
	{
		l_dialog->setVisible(true);
		return 0;
	}
}

void MyBuff::tableCellTouched(CCTableView* table, CCTableViewCell* cell)
{
}

void MyBuff::scrollViewDidScroll(CCScrollView* view )
{
}

void MyBuff::scrollViewDidZoom(CCScrollView* view )
{
}








