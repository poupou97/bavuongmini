
#ifndef _GAMESCENESTATE_TARGETINFOMINI_H_
#define _GAMESCENESTATE_TARGETINFOMINI_H_

#include "cocos-ext.h"
#include "../../ui/extensions/uiscene.h"
#include "cocos2d.h"

USING_NS_CC;
USING_NS_CC_EXT;

class BaseFighter;

/**
   * the blood bar on the enemy's head
   */

class TargetInfoMini : public UIScene
{
public:
	TargetInfoMini();
	~TargetInfoMini();

	static TargetInfoMini* create(BaseFighter *baseFighter);
	bool init(BaseFighter *baseFighter);

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

	virtual void update(float delta);

	UIImageView * ImageView_targetBlood;
	UIImageView * ImageView_targetBloodGray;

	void ReloadTargetData( BaseFighter * baseFighter, bool bInitNewTarget = false);
	
	long long baseFighterId;

private:
	float m_preScale;
	float m_nowScale;

	float m_timeDelay;
};

#endif;

