
#ifndef _GAMESCENESTATE_MISSIONANDTEAM_H_
#define _GAMESCENESTATE_MISSIONANDTEAM_H_

#include "cocos-ext.h"
#include "../../ui/extensions/uiscene.h"
#include "cocos2d.h"
#include "../../legend_script/CCTutorialIndicator.h"

USING_NS_CC;
USING_NS_CC_EXT;

class UITab;
class MissionCell;
class CTeamMember;
class MissionAndTeam : public UIScene, public CCTableViewDataSource, public CCTableViewDelegate
{
public:

	enum PanelTypes
	{
		type_MissionAndTeam = 0,
		type_FiveInstanceAndTeam,
		type_FamilyFightAndTeam,
		type_SingCopyChanllengeOnly,
		type_OfflineArenaOnly,
	};

	enum CurPresentPanel
	{
		p_mission = 0,
		p_team,
		p_fiveInstance,
		p_familyFight,
		p_singCopyChallenge,
		p_offLineArena,
	};

	struct GeneralInfoInArena{
		long long generalId;
		std::string name;
		int level;
		int max_hp;
		int cur_hp;
		bool isFinishedFight;
		int star;
	};

	MissionAndTeam();
	~MissionAndTeam();
	static MissionAndTeam* create();
	bool init();
	
	//总的
	UILayer * layer_leader;

	UILayer * MissionLayer ;
	UILayer * TeamLayer;
	UILayer * FiveInstanceLayer;
	UILayer * FamilyFightLayer;
	UILayer * SingCopyLayer;
	UILayer * OffLineArenaLayer;

	bool isExistMissionLayer;
	bool isExistTeamLayer;
	bool isExistFiveInstanceLayer;
	bool isExistFamilyFightLayer;
	bool isExistSingCopylayer;
	bool isExistOffLineArenaLayer;
	bool isOn ;

	UIButton * Button_mission;
	UIButton * Button_team;
	UIButton * Button_fiveInstance;
	UIButton * Button_familyFight;
	UIButton * Button_singCopy;
	UIButton * Button_offLineArena;

	void ButtonMissionEvent(CCObject *pSender);
	void ButtonTeamEvent(CCObject *pSender);
	void ButtonFiveInstanceEvent(CCObject *pSender);
	void ButtonFamilyFightEvent(CCObject *pSender);
	void ButtonSingCopyChallengeEvent(CCObject *pSender);
	void ButtonOffLineArenaEvent(CCObject *pSender);

	void initMissionLayer();
	void initTeamLayer();
	void initFiveInstanceLayer();
	void initFamilyFightLayer();
	void initSingCopyLayer();
	void initOffLineArenaLayer();

	//teamInvite
	UIButton * teamInvite_btn;
	void callBackTeamInvite(CCObject * obj);

	std::vector<CTeamMember *>selfTeamPlayerVector;
	void refreshTeamPlayer();
	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

	virtual void update(float dt);

    virtual void tableCellHighlight(CCTableView* table, CCTableViewCell* cell);
    virtual void tableCellUnhighlight(CCTableView* table, CCTableViewCell* cell);

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);


	//教学
	virtual void registerScriptCommand(int scriptId);
	//第一个
	void addCCTutorialIndicator1(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction  );
	//第二个
	void addCCTutorialIndicator2(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction  );
	//副本面板
	void addCCTutorialIndicator3(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction  );
	void removeCCTutorialIndicator();

	int m_nTutorialIndicatorIndex;

	void reloadMissionData();
	void reloadMissionDataWithoutChangeOffSet(CCPoint p_offset,CCSize p_contentSize);

	//15级前指引任务的笔
	void addGuidePen();
	void removeGuidePen();

	/** 在主界面上是否存在指引（如果存在则指引任务的笔不出现） */
	bool isExistTutorialInMainScene();

	CCTableView * getLeftOffLineArenaTableView();
	CCTableView * getRightOffLineArenaTableView();

private:
	UIButton * missionAndTeam_on_off;
	UIImageView * ImageView_missionAndTeam_on_off;

	CCPoint touchBeganPoint;
	int contentSizeHeight;

	UIButton * Button_flag;

	UILabelBMFont* l_boss_Value;
	UILabelBMFont* l_monster_Value;

	void MissionAndTeamOnOff(CCObject *pSender);

	void popUpMission(CCObject *pSender);
	void takeBackMission(CCObject *pSender);

	//教学
	int mTutorialScriptInstanceId;

	//上次点击的时刻
	long long m_nLastTouchTime;
public:
	CCTableView * missionTableView ;
	CCTableView * teamTableView ;
	CCTableView * fiveInstanceTableView;
	CCTableView * familyFightTableView;
	CCTableView * singCopyTableview;

	long long leaderId;

private:
	UILabelBMFont * curLevel_BmFont;
	UILabelAtlas * m_curLevelShowValue;
	UILabelAtlas * label_monsterValue;
	UILabel * m_useTimeValue;
	UILabel * allTimeShowValue;

	int m_singCopy_curLevel;
	int m_singCopy_curMonsterNumber;
	int m_singCopy_monsterNumbers;
	int m_singCopy_remainMonster;
	int m_singCopy_useTime;
	int m_singCopy_timeCount;
public:
	//点击过关斩将面板的响应事件
	void callBackSingCopy(CCObject * pSender);
	void updateSingCopyCheckMonst(float delta);

	void setSingCopy_useTime(int time_);
	int getSingCopy_useTime();

	void setStateIsRobot(bool value_);
	bool getStateIsRobot();

private:
	static int s_panelType;
	static int s_curPresentPanel;

	//副本中的怪数量、boss数量
	int cur_boss_count;
	int cur_monster_count;
	int max_boss_count;
	int max_monster_count;

	//
	bool m_state_start;

public:
	static void setPanelType(int stype);
	static int getPanelType();

	static void setCurPresentPanel(int cpPanel);
	static int getCurPresentPanel();

	void refreshFunctionBtn();

	//任务面板向下的小箭头
	void refreshDownArrowStatus();

	//点击副本面板的响应事件
	void FiveInstanceInfoEvent(CCObject * pSender);
	//寻路完成后回调函数
	void DoThingWhenFinishedMove(CCPoint targetPos);
	CCPoint m_Target_pos_inInstance;
	//update 检测距离(寻路完成后自动开启挂机)
	void CheckDistance(float dt);

	//副本进度更新
	void reloadToDefaultConfig(int instanceId);
	void updateInstanceSchedule(int bossCount,int monsterCount);
	void updateFamilyFightRemainTime(float delta );

	//家族战进度更新
	void updateFamilyFightSchedult();
	//update 检测距离(寻路完成后自动开启挂机)
	CCPoint m_Target_pos_inFamilyFight;
	void CheckDistanceInFamilyFight(float dt);

	//singCopy schedule 
	void updateSingCopyCellInfo(int level,int curMon,int allMon,int remMon,int useTime,int timeCount);
	void starRunUpdate();
	void updateSingCopyCurUseTime(float delta );

	void refreshTutorial();

	//data for offLineArena//
public:
	GeneralInfoInArena* p_enemyInfoInArena;
	std::vector<GeneralInfoInArena*> generalsInArena_mine;
	std::vector<GeneralInfoInArena*> generalsInArena_other;

	long long m_nRemainTime_Arena;

	//竞技场剩余时间更新
	void updateOffLineArenaRemainTime(float delta );

	void setArenaDataToDefault();

	//data for team
public:
	void ManageTeamEvent(CCObject *pSender);
	void ExitTeamEvent(CCObject *pSender);
};
#endif;

