#ifndef _GAMESCENESTATE_NEWFUNCTIONREMINDUI_H_
#define _GAMESCENESTATE_NEWFUNCTIONREMINDUI_H_

#include "../../ui/extensions/uiscene.h"
#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class NewFunctionRemindUI : public UIScene
{
public:
	NewFunctionRemindUI();
	~NewFunctionRemindUI();

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

	static NewFunctionRemindUI* create(int level);
	bool init(int level);

	virtual void update(float dt);

	void CloseEvent(CCObject *pSender);
	void BtnEvent(CCObject *pSender);

protected:
	std::map<int,std::string>newFunctionList;
	int time;
};

#endif

