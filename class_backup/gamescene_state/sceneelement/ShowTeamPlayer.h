#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../../ui/extensions/UIScene.h"
USING_NS_CC;
USING_NS_CC_EXT;
class CTeamMember;
class ShowTeamPlayer:public UIScene
{
public:
	ShowTeamPlayer(void);
	~ShowTeamPlayer(void);

	static ShowTeamPlayer *create(CTeamMember * members,long long leader);
	bool init(CTeamMember * members,long long leader);
	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

	void playerInfo(CCObject * obj);

};

