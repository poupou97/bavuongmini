#include "ShortcutLayer.h"
#include "ShortcutSlot.h"
#include "ShortcutUIConstant.h"
#include "../../../messageclient/element/CShortCut.h"
#include "../../../messageclient/element/FolderInfo.h"
#include "GameView.h"
#include "../../../messageclient/protobuf/ModelMessage.pb.h"
#include "../../../messageclient/GameMessageProcessor.h"
#include "../../GameSceneState.h"
#include "../../../ui/skillscene/SkillScene.h"
#include "../../MainScene.h"
#include "../..//skill/GameFightSkill.h"
#include "../../exstatus/ExStatus.h"
#include "../../role/MyPlayer.h"
#include "../../exstatus/ExStatusType.h"
#include "../../../legend_script/UITutorialIndicator.h"
#include "../../../legend_script/Script.h"
#include "../../../legend_script/ScriptManager.h"
#include "../../../legend_script/CCTutorialIndicator.h"
#include "../../../gamescene_state/sceneelement/HeadMenu.h"
#include "../../../legend_script/CCTutorialParticle.h"
#include "../../../utils/StaticDataManager.h"
#include "../TriangleButton.h"
#include "../../../legend_script/CCTeachingGuide.h"

ShortcutLayer::ShortcutLayer():
shortcutSlotNum(5)
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

	//                  (4)
	//                (1)
	//           (2)  
	//        (3)     (0)
	const int EXPERIENCE_BAR_HEIGHT = 10;   // 经验条的高度
	const int OFFSET_X = -10; 
	const int OFFSET_Y = 10;
	slotPos[0].x = winSize.width - 37 + origin.x + OFFSET_X;
	slotPos[0].y = 37 + origin.y + OFFSET_Y + EXPERIENCE_BAR_HEIGHT;
	slotPos[1].x = winSize.width - 32 + origin.x + OFFSET_X;
	slotPos[1].y = 119 + origin.y + OFFSET_Y + EXPERIENCE_BAR_HEIGHT;   // 114
	slotPos[2].x = winSize.width - 99 + origin.x + OFFSET_X;   // 94
	slotPos[2].y = 99 + origin.y + OFFSET_Y + EXPERIENCE_BAR_HEIGHT;   // 94
	slotPos[3].x = winSize.width - 119 + origin.x + OFFSET_X;   // 114
	slotPos[3].y = 32 + origin.y + OFFSET_Y + EXPERIENCE_BAR_HEIGHT;
	slotPos[4].x = winSize.width - 32 + origin.x + OFFSET_X;
	slotPos[4].y = 197 + origin.y + OFFSET_Y + EXPERIENCE_BAR_HEIGHT;   // 187
	

	for (int i = 0;i<shortcutSlotNum;++i)
	{
		shortcurTag[i] = i;
	}
}


ShortcutLayer::~ShortcutLayer()
{
}

ShortcutLayer* ShortcutLayer::create()
{
	ShortcutLayer * shortcutLayer = new ShortcutLayer();
	if (shortcutLayer && shortcutLayer->init())
	{
		shortcutLayer->autorelease();
		return shortcutLayer;
	}
	CC_SAFE_DELETE(shortcutLayer);
	return NULL;
}

bool ShortcutLayer::init()
{
	if (UIScene::init())
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

		u_layer = UILayer::create();
		addChild(u_layer);

		u_teachLayer = UILayer::create();
		addChild(u_teachLayer);
		u_teachLayer->setZOrder(100);

		int musouSkillOpenLevel = ShortcutSlotOpenLevelConfigData::s_shortcutSlotOpenLevelMsg[4];
		for (int i = 0; i<shortcutSlotNum; ++i)
		{
			if (i == shortcutSlotNum-1)
			{
				if (GameView::getInstance()->myplayer->getActiveRole()->level() >= musouSkillOpenLevel)
				{
					ShortcutSlot * shortcutSlot = ShortcutSlot::createPeerless();
					shortcutSlot->setAnchorPoint(CCPointZero);
					shortcutSlot->setPosition(slotPos[i]);
					shortcutSlot->setTag(shortcurTag[i]+100);
					shortcutSlot->setZOrder(10-i);
					shortcutSlot->setScale(1.1f);
					addChild(shortcutSlot);
				}
				else
				{
					UIButton * btn_peerless = UIButton::create();
					btn_peerless->setTextures("gamescene_state/zhujiemian3/jinengqu/wushuang_off.png","gamescene_state/zhujiemian3/jinengqu/wushuang_off.png","");
					btn_peerless->setTouchEnable(true);
					btn_peerless->setAnchorPoint(CCPointZero);
					btn_peerless->setPosition(ccp(slotPos[i].x-btn_peerless->getContentSize().width/2,slotPos[i].y-btn_peerless->getContentSize().height/2+8));
					btn_peerless->setName("btn_peerless");
					btn_peerless->addReleaseEvent(this,coco_releaseselector(ShortcutLayer::PeerlessEvent));
					u_layer->addWidget(btn_peerless);
				}
			}
			else
			{
				ShortcutSlot * shortcutSlot = ShortcutSlot::create(i);
				shortcutSlot->setAnchorPoint(CCPointZero);
				shortcutSlot->setPosition(slotPos[i]);
				shortcutSlot->setTag(shortcurTag[i]+100);
				shortcutSlot->setZOrder(10-i);
				int index = shortcurTag[i];
				if (DEFAULTATTACKSKILL_INDEX == index)
				{
					shortcutSlot->setScale(SCALE_SHORTCUTSLOT_DEFAULTATTACKSKILL);
				}
				else
				{
					shortcutSlot->setScale(SCALE_SHORTCUTSLOT);
				}
				
				addChild(shortcutSlot);
				shortcutSlot->setVisible(false);

				int openlevel = ShortcutSlotOpenLevelConfigData::s_shortcutSlotOpenLevelMsg[i];
				if (GameView::getInstance()->myplayer->getActiveRole()->level()>=openlevel)
				{
					shortcutSlot->setVisible(true);
					shortcutSlot->setPosition(slotPos[i]);
				}
			}
		}

		//角色创建完上线后默认快捷栏中有普通攻击技能
		if (GameView::getInstance()->myplayer->getActiveRole()->level() == 1)
		{
			ShortcutSlot * temp = dynamic_cast<ShortcutSlot*>(this->getChildByTag(100));
			if(temp != NULL)
			{
				std::string defaultAtkSkill = "";
				if(GameView::getInstance()->myplayer->getActiveRole()->profession() == PROFESSION_MJ_NAME)
				{
					defaultAtkSkill = "WarriorDefaultAttack";
				}
				else if (GameView::getInstance()->myplayer->getActiveRole()->profession() == PROFESSION_GM_NAME)
				{
					defaultAtkSkill = "MagicDefaultAttack";
				}
				else if (GameView::getInstance()->myplayer->getActiveRole()->profession() == PROFESSION_HJ_NAME)
				{
					defaultAtkSkill = "WarlockDefaultAttack";
				}
				else if (GameView::getInstance()->myplayer->getActiveRole()->profession() == PROFESSION_SS_NAME)
				{
					defaultAtkSkill = "RangerDefaultAttack";
				}

				CShortCut * tempShortCut = new CShortCut();
				tempShortCut->set_index(100);
				tempShortCut->set_storedtype(1);
				tempShortCut->set_skillpropid(defaultAtkSkill);
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1122, tempShortCut);
				delete tempShortCut;
			}
		}

		for(int i = 0;i<(int)GameView::getInstance()->shortCutList.size();++i)
		{
			CShortCut * shortcut = new CShortCut();
			shortcut->CopyFrom(*(GameView::getInstance()->shortCutList.at(i)));
			//CCLOG("%s",shortcut->skillpropid().c_str());
			if (shortcut->complexflag() == 1)
			{
				if (GameView::getInstance()->myplayer->getActiveRole()->level() >= musouSkillOpenLevel)
				{
					GameView::getInstance()->myplayer->setMusouSkill(shortcut->skillpropid().c_str());
					//change by yangjun 2014.9.28
// 					ShortcutSlot * temp = dynamic_cast<ShortcutSlot*>(this->getChildByTag(104));
// 					if(temp != NULL)
// 						temp->setPeerlessInfo(shortcut);
				}
			}
			else
			{
				if (shortcut->index() < MUSOUSKILL_INDEX)
				{
					ShortcutSlot * temp = dynamic_cast<ShortcutSlot*>(this->getChildByTag(shortcut->index()+100));
					if(temp != NULL)
						temp->setInfo(shortcut);
				}
			}
		
			delete shortcut;
		}

		
		this->scheduleUpdate();

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(winSize);
		this->setTouchPriority(1);
		return true;
	}
	return false;
}

void ShortcutLayer::onEnter()
{
	UIScene::onEnter();
	//this->openAnim();
}
void ShortcutLayer::onExit()
{
	UIScene::onExit();
}	

bool ShortcutLayer::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
	// 如果被移出了屏幕，右下角区域不拦截触控
// 	if (this->isVisible())
// 		return false;

	//右下角三角按钮
	TriangleButton * triangleBtn = (TriangleButton*)GameView::getInstance()->getMainUIScene()->getChildByTag(78);
	if (triangleBtn)
	{
		if (!triangleBtn->isOpen)
			return false;
	}

	// convert the touch point to OpenGL coordinates
	CCPoint location = pTouch->getLocation();
	touchBeganPoint = pTouch->getLocation();

	CCPoint pos = this->getPosition();
	CCSize size = this->getContentSize();
	CCPoint anchorPoint = this->getAnchorPointInPoints();
	pos = ccpSub(pos, anchorPoint);
	CCRect rect(pos.x, pos.y, size.width, size.height);

// 	int changeY = 0;
// 	if (GameView::getInstance()->getMainUIScene())
// 	{
// 		MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
// 		if (mainScene->isHeadMenuOn)
// 		{
// 			changeY = mainScene->headMenu->getContentSize().height;
// 		}
// 		else
// 		{
// 			changeY = 0;
// 		}
// 	}
	
	if (location.y <= slotPos[3].y+27)
	{
		if (location.x > slotPos[3].x-27)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else if (location.y <= slotPos[2].y+27)
	{
		if (location.x > slotPos[2].x-27)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else if (location.y <= slotPos[1].y+27)
	{
		if (location.x > slotPos[1].x-27)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else if (location.y <= slotPos[4].y+27)
	{
		if (location.x > slotPos[4].x-27)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}
void ShortcutLayer::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
{

}
void ShortcutLayer::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
{

}
void ShortcutLayer::ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent)
{

}

void ShortcutLayer::RefreshAll()
{
	for(int i = 0;i<(int)GameView::getInstance()->AllPacItem.size();++i)
	{
			FolderInfo * fi = GameView::getInstance()->AllPacItem.at(i);
			if (fi->has_goods())
			{
				RefreshOne(fi);
			}
	}
}

void ShortcutLayer::RefreshOne(FolderInfo* folderInfo)
{
	for(int i = 0;i<8;i++)
	{
		if (i < GameView::getInstance()->shortCutList.size())
		{
		 	CShortCut * c_shortcut = GameView::getInstance()->shortCutList.at(i);
		 	if (c_shortcut->storedtype() == 2)    //快捷栏中是物品
		 	{
		 		if (folderInfo->has_goods())
		 		{
		 			if (folderInfo->goods().canshortcut())   //背包中的物品可以装配
		 			{
		 				if (strcmp(folderInfo->goods().id().c_str(),c_shortcut->skillpropid().c_str()) == 0)   //如果该物品装配到了快捷栏
		 				{
							if (c_shortcut->index() < MUSOUSKILL_INDEX)
							{
								if (this->getChildByTag(shortcurTag[c_shortcut->index()]+100))
								{
									ShortcutSlot * shortcutSlot = (ShortcutSlot*)this->getChildByTag(shortcurTag[c_shortcut->index()]+100);
									shortcutSlot->refreshRetainNum(folderInfo);
								}
							}
		 				}
		 			}
				}
		 	}
		}
	}
}
void ShortcutLayer::CloseEvent( CCObject * pSender )
{
	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(pSender);


/*	this->closeAnim();*/
}

void ShortcutLayer::RefreshOneShortcutSlot( CShortCut * shortcut )
{
	int index = shortcut->index();
	if (!(ShortcutSlot*)this->getChildByTag(index+100))
		return;

	ShortcutSlot* shortcutslot = (ShortcutSlot*)this->getChildByTag(index+100);
	shortcutslot->removeFromParent();

	if (index == MUSOUSKILL_INDEX)
	{
		ShortcutSlot * shortcutSlot = ShortcutSlot::createPeerless();
		shortcutSlot->setAnchorPoint(CCPointZero);
		shortcutSlot->setPosition(slotPos[index]);
		shortcutSlot->setTag(shortcurTag[index]+100);
		shortcutSlot->setScale(SCALE_SHORTCUTSLOT_MUSOUSKILL);
		addChild(shortcutSlot);
		//change by yangjun 2014.9.28
		//shortcutSlot->setPeerlessInfo(shortcut);
	}
	else
	{
		ShortcutSlot * shortcutSlot = ShortcutSlot::create(index);
		shortcutSlot->setAnchorPoint(CCPointZero);
		shortcutSlot->setPosition(slotPos[index]);
		shortcutSlot->setTag(shortcurTag[index]+100);
		shortcutSlot->setZOrder(10-index);
		int m_index = shortcurTag[index];
		if (m_index == DEFAULTATTACKSKILL_INDEX)
		{
			shortcutSlot->setScale(SCALE_SHORTCUTSLOT_DEFAULTATTACKSKILL);
		}
		else
		{
			shortcutSlot->setScale(SCALE_SHORTCUTSLOT);
		}
		addChild(shortcutSlot);
		shortcutSlot->setInfo(shortcut);
	}
}

void ShortcutLayer::RefreshMusouEffect( float percentage )
{
	ShortcutSlot* shortcutslot = (ShortcutSlot*)this->getChildByTag(104);
	if (shortcutslot)
		shortcutslot->setPeerlessAngerEffect(percentage);
}

void ShortcutLayer::update(float dt)
{
	for (int i = 0;i<(int)GameView::getInstance()->shortCutList.size();++i)
	{
		CShortCut * tempshortcut = GameView::getInstance()->shortCutList.at(i);
		if (tempshortcut->complexflag() != 1 && tempshortcut->storedtype() == 1)///判断是否有足够的魔法水来释放该技能
		{
			if (tempshortcut->has_skillpropid())
			{
				if (isEnoughMagic(tempshortcut))
				{
					setSlotUseType(tempshortcut->index(),true);
				}
				else
				{
					setSlotUseType(tempshortcut->index(),false);
				}
			}
		}
		else if (tempshortcut->complexflag() != 1 && tempshortcut->storedtype() == 2)//判断药品是否已用尽
		{
			if (tempshortcut->has_skillpropid())
			{
				if (isEnoughGoods(tempshortcut))
				{
					setSlotUseType(tempshortcut->index(),true);
				}
				else
				{
					setSlotUseType(tempshortcut->index(),false);
				}
			}
		}
	}

	///判断角色是否处于眩晕,定身等状态
	if (this->isImmobilize())
	{
		for (int i = 0;i<(int)GameView::getInstance()->shortCutList.size();++i)
		{
			CShortCut * tempshortcut = GameView::getInstance()->shortCutList.at(i);
			if (tempshortcut->has_skillpropid())
			{
				setSlotUseType(i,false);
			}
		}
	}
}

void ShortcutLayer::setSlotUseType( int index,bool canUse )
{
	if (index != MUSOUSKILL_INDEX)
	{
		if(this->getChildByTag(index+100))
		{
			ShortcutSlot * tempSlot = (ShortcutSlot *)this->getChildByTag(index+100);
			if (tempSlot->slotType != ShortcutSlot::T_Void)
			{
				tempSlot->setUseType(index,canUse);
			}
		}
	}
}

bool ShortcutLayer::isEnoughMagic( CShortCut * shortcut )
{
	//test
	if (shortcut->skillpropid() == FightSkill_SKILL_ID_PUTONGGONGJI || 
		shortcut->skillpropid() == FightSkill_SKILL_ID_WARRIORDEFAULTATTACK ||
		shortcut->skillpropid() == FightSkill_SKILL_ID_MAGICDEFAULTATTACK ||
		shortcut->skillpropid() == FightSkill_SKILL_ID_WARLOCKDEFAULTATTACK ||
		shortcut->skillpropid() == FightSkill_SKILL_ID_RANGERDEFAULTATTACK)
	{
		return true;
	}

	if (GameView::getInstance()->GameFightSkillList.find(shortcut->skillpropid()) != GameView::getInstance()->GameFightSkillList.end())
	{
		GameFightSkill * skill = (GameView::getInstance()->GameFightSkillList.find(shortcut->skillpropid()))->second;
		ActiveRole * activeRole = GameView::getInstance()->myplayer->getActiveRole();
		if (skill->getCFightSkill()->mp() > activeRole->mp())
		{
			return false;
		}
		else
		{
			return true;
			}
	}
	else 
	{
		return false;
	}
}

bool ShortcutLayer::isImmobilize()
{
	if(GameView::getInstance()->myplayer->hasExStatus(ExStatusType::immobilize))
	{
		return true;
	}

	return false;
}

bool ShortcutLayer::isEnoughGoods( CShortCut * shortcut )
{
	int goodsTotalNum = 0;
	if (shortcut->storedtype() == 2)
	{
		for (int i = 0;i<(int)GameView::getInstance()->AllPacItem.size();++i)
		{
			if (GameView::getInstance()->AllPacItem.at(i)->has_goods())
			{
				if (strcmp(shortcut->skillpropid().c_str(),GameView::getInstance()->AllPacItem.at(i)->goods().id().c_str()) == 0)
				{
					goodsTotalNum += GameView::getInstance()->AllPacItem.at(i)->quantity();
				}
			}
		}
	}

	if (goodsTotalNum > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}


void ShortcutLayer::RefreshCD( std::string skillId )
{
	for(int i = 0;i<(int)GameView::getInstance()->shortCutList.size();++i)
	{
		CShortCut * shortcut = new CShortCut();
		shortcut->CopyFrom(*(GameView::getInstance()->shortCutList.at(i)));
		if (shortcut->index() < MUSOUSKILL_INDEX)
		{
			if (shortcut->complexflag() != 1)
			{
				if (shortcut->has_skillpropid())
				{
					if (strcmp(shortcut->skillpropid().c_str(),skillId.c_str()) == 0)
					{
						ShortcutSlot * temp = (ShortcutSlot *)this->getChildByTag(shortcut->index()+100);
						temp->BeganToRunCD();
					};
				}
			}
		}

		delete shortcut;
	}
}


void ShortcutLayer::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void ShortcutLayer::addCCTutorialIndicator( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,true);
// 	tutorialIndicator->setPosition(ccp(pos.x-45,pos.y+95));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	u_teachLayer->addChild(tutorialIndicator);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene->isHeadMenuOn)
	{
		mainScene->ButtonHeadEvent(NULL);
	}

	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height-480)/2;
	CCTeachingGuide::CloseUIForTutorial();
	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,70,70,true,true,true);
	tutorialIndicator->setDrawNodePos(ccp(pos.x,pos.y));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void ShortcutLayer::addCCTutorialIndicator1( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,true);
// 	tutorialIndicator->setPosition(ccp(pos.x,pos.y));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	tutorialIndicator->addHighLightFrameAction(70,70);
// 	u_teachLayer->addChild(tutorialIndicator);

	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height-480)/2;
	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,70,70,true,true,true);
	tutorialIndicator->setDrawNodePos(ccp(pos.x,pos.y));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void ShortcutLayer::removeCCTutorialIndicator()
{
	CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(u_teachLayer->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		CCTeachingGuide* teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}

bool SLSortByComplexOrder(GameFightSkill * gameFightSkill1 , GameFightSkill *gameFightSkill2)
{
	return (gameFightSkill1->getCBaseSkill()->get_complex_order() < gameFightSkill2->getCBaseSkill()->get_complex_order());
}

void ShortcutLayer::addNewShortcutSlot()
{
	//在这个等级会开启的下标
	std::vector<int>openingVector;
	int musouSkillOpenLevel = ShortcutSlotOpenLevelConfigData::s_shortcutSlotOpenLevelMsg[MUSOUSKILL_INDEX];
	for (int i = 0; i<shortcutSlotNum; ++i)
	{
		if (i == shortcutSlotNum-1)
		{
			if (GameView::getInstance()->myplayer->getActiveRole()->level() == musouSkillOpenLevel)
			{
				if (u_layer->getWidgetByName("btn_peerless"))
				{
					u_layer->getWidgetByName("btn_peerless")->removeFromParent();
				}

				ShortcutSlot * shortcutSlot = ShortcutSlot::createPeerless();
				shortcutSlot->setAnchorPoint(CCPointZero);
				shortcutSlot->setPosition(slotPos[i]);
				shortcutSlot->setTag(shortcurTag[i]+100);
				shortcutSlot->setZOrder(10-i);
				shortcutSlot->setScale(SCALE_SHORTCUTSLOT_MUSOUSKILL);
				addChild(shortcutSlot);
			}
		}
		else
		{
			int openlevel = ShortcutSlotOpenLevelConfigData::s_shortcutSlotOpenLevelMsg[i];
			if (GameView::getInstance()->myplayer->getActiveRole()->level() == openlevel)
			{
				ShortcutSlot * shortcutSlot = (ShortcutSlot*)this->getChildByTag(shortcurTag[i]+100);
				if (shortcutSlot)
				{
					shortcutSlot->setVisible(true);
					openingVector.push_back(i);
					shortcutSlot->setPosition(slotPos[i]);
				}
			}
		}
	}
	for(int i = 0;i<(int)GameView::getInstance()->shortCutList.size();++i)
	{
		CShortCut * shortcut = new CShortCut();
		shortcut->CopyFrom(*(GameView::getInstance()->shortCutList.at(i)));
		//CCLOG("%s",shortcut->skillpropid().c_str());
		if (shortcut->complexflag() == 1)
		{
			if (GameView::getInstance()->myplayer->getActiveRole()->level() == musouSkillOpenLevel)
			{
				GameView::getInstance()->myplayer->setMusouSkill(shortcut->skillpropid().c_str());
				//change by yangjun 2014.9.28
// 				ShortcutSlot * temp = dynamic_cast<ShortcutSlot*>(this->getChildByTag(104));
// 				if(temp != NULL)
// 					temp->setPeerlessInfo(shortcut);
			}
		}
		else
		{
			if (shortcut->index() < MUSOUSKILL_INDEX)
			{
				ShortcutSlot * temp = dynamic_cast<ShortcutSlot*>(this->getChildByTag(shortcut->index()+100));
				if(temp != NULL)
					temp->setInfo(shortcut);
			}
		}

		delete shortcut;
	}

	//把未装配的技能拿出来，按照龙魂优先级排序，优先级高的先设置到快捷栏
	std::vector<GameFightSkill*>notEquipedList;
	std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GameFightSkillList.begin();
	for ( ; it != GameView::getInstance()->GameFightSkillList.end(); ++ it )
	{
		GameFightSkill * tempSkill = it->second;
		if (tempSkill->getCBaseSkill()->usemodel() != 0)
			continue;

		bool isEquiped = false;
		for(int i =0;i<GameView::getInstance()->shortCutList.size();++i)
		{
			CShortCut * tempShortCut = GameView::getInstance()->shortCutList.at(i);
			if (!tempShortCut->has_skillpropid())
				continue;

			if (tempShortCut->storedtype() != 1)
				continue;

			if (strcmp(tempSkill->getId().c_str(),tempShortCut->skillpropid().c_str()) == 0)
			{
				isEquiped = true;
			}
		}

		if (isEquiped)
			continue;

		GameFightSkill * newGameFightSkill = new GameFightSkill();
		newGameFightSkill->initSkill(tempSkill->getId(),tempSkill->getId(),tempSkill->getLevel(),GameActor::type_player);
		notEquipedList.push_back(newGameFightSkill);
	}
	//按龙魂优先级来排序
	sort(notEquipedList.begin(),notEquipedList.end(),SLSortByComplexOrder);

	//把未装配的技能装配上
	for(int i = 0;i<openingVector.size();++i)
	{
		ShortcutSlot * shortcutSlot = dynamic_cast<ShortcutSlot*>(this->getChildByTag(shortcurTag[openingVector.at(i)]+100));
		if (shortcutSlot)
		{
			if (shortcutSlot->slotType != ShortcutSlot::T_Void)
				continue;

			if (!shortcutSlot->isVisible())
				continue;

			if(i<notEquipedList.size())
			{
				CShortCut * tempShortCut = new CShortCut();
				tempShortCut->set_index(openingVector.at(i)+100);
				tempShortCut->set_storedtype(1);
				tempShortCut->set_skillpropid(notEquipedList.at(i)->getId());
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1122, tempShortCut);
				delete tempShortCut;
			}

		}
	}
	//内存回收
	//chat
	std::vector<GameFightSkill *>::iterator iter_gameFightSkill;
	for (iter_gameFightSkill=notEquipedList.begin();iter_gameFightSkill !=notEquipedList.end();++iter_gameFightSkill)
	{
		delete *iter_gameFightSkill;
	}
	notEquipedList.clear();
}

void ShortcutLayer::PeerlessEvent( CCObject* pSender )
{
	GameView::getInstance()->showAlertDialog(StringDataManager::getString("musouskill_will_be_open"));
}

void ShortcutLayer::setMusouShortCutSlotPresent( bool isPresent )
{
	if (u_layer->getWidgetByName("btn_peerless"))
	{
		u_layer->getWidgetByName("btn_peerless")->setVisible(isPresent);
	}
	else
	{
		ShortcutSlot * musouSlot = (ShortcutSlot*)this->getChildByTag(100+4);
		if (musouSlot)
		{
			musouSlot->setVisible(isPresent);
		}

		float sl = GameView::getInstance()->myplayer->getAngerValue()*1.0f/GameView::getInstance()->myplayer->getAngerCapacity();
		if (isPresent)
		{
			this->RefreshMusouEffect(sl);
		}
	}
}

void ShortcutLayer::createMusouShortCutSlotForce()
{
	if (u_layer->getWidgetByName("btn_peerless"))
	{
		u_layer->getWidgetByName("btn_peerless")->removeFromParent();

		ShortcutSlot * shortcutSlot = ShortcutSlot::createPeerless();
		shortcutSlot->setAnchorPoint(CCPointZero);
		shortcutSlot->setPosition(slotPos[shortcutSlotNum-1]);
		shortcutSlot->setTag(shortcurTag[shortcutSlotNum-1]+100);
		shortcutSlot->setZOrder(10-(shortcutSlotNum-1));
		shortcutSlot->setScale(SCALE_SHORTCUTSLOT_MUSOUSKILL);
		addChild(shortcutSlot);

		for(int i = 0;i<(int)GameView::getInstance()->shortCutList.size();++i)
		{
			CShortCut * shortcut = new CShortCut();
			shortcut->CopyFrom(*(GameView::getInstance()->shortCutList.at(i)));
			//CCLOG("%s",shortcut->skillpropid().c_str());
			if (shortcut->complexflag() == 1)
			{
				GameView::getInstance()->myplayer->setMusouSkill(shortcut->skillpropid().c_str());
				//change by yangjun 2014.9.28
// 				ShortcutSlot * temp = dynamic_cast<ShortcutSlot*>(this->getChildByTag(104));
// 				if(temp != NULL)
// 					temp->setPeerlessInfo(shortcut);
			}

			delete shortcut;
		}
	}
}

void ShortcutLayer::recoverMusouShortCutSlot()
{
	if (u_layer->getWidgetByName("btn_peerless"))
	{
	}
	else
	{
		ShortcutSlot * temp = dynamic_cast<ShortcutSlot*>(this->getChildByTag(104));
		if(temp != NULL)
			temp->removeFromParent();

		UIButton * btn_peerless = UIButton::create();
		btn_peerless->setTextures("gamescene_state/zhujiemian3/jinengqu/wushuang_off.png","gamescene_state/zhujiemian3/jinengqu/wushuang_off.png","");
		btn_peerless->setTouchEnable(true);
		btn_peerless->setAnchorPoint(CCPointZero);
		btn_peerless->setPosition(ccp(slotPos[shortcutSlotNum-1].x-btn_peerless->getContentSize().width/2,slotPos[shortcutSlotNum-1].y-btn_peerless->getContentSize().height/2+8));
		btn_peerless->setName("btn_peerless");
		btn_peerless->addReleaseEvent(this,coco_releaseselector(ShortcutLayer::PeerlessEvent));
		u_layer->addWidget(btn_peerless);
	}
}

void ShortcutLayer::presentShortCutSlot( int num )
{
	for(int i = 1;i<(int)GameView::getInstance()->shortCutList.size();++i)
	{
		ShortcutSlot * temp = dynamic_cast<ShortcutSlot*>(this->getChildByTag(100+i));
		if(temp != NULL)
			temp->setVisible(false);
	}

	if(num > 3)
		num = 3;

	for (int i = 0;i<num;i++)
	{
		int temp_tag = 3-i;
		ShortcutSlot * temp = dynamic_cast<ShortcutSlot*>(this->getChildByTag(100+temp_tag));
		if(temp != NULL)
			temp->setVisible(true);
	}
}

