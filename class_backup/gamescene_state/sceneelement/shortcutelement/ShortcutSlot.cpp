#include "ShortcutSlot.h"
#include "ShortcutLayer.h"
#include "ShortcutUIConstant.h"
#include "../../GameSceneState.h"
#include "../../MainScene.h"
#include "../../role/MyPlayer.h"
#include "../../role/MyPlayerSimpleAI.h"
#include "GameView.h"
#include "../../role/MyPlayerOwnedCommand.h"
#include "../../../messageclient/GameMessageProcessor.h"
#include "../../../ui/backpackscene/PacPageView.h"
#include "../../../messageclient/element/CShortCut.h"
#include "../../skill/GameFightSkill.h"
#include "../../../messageclient/element/FolderInfo.h"
#include "../../exstatus/ExStatusType.h"
#include "../../../utils/StaticDataManager.h"
#include "../../../legend_script/Script.h"
#include "../../../legend_script/ScriptManager.h"
#include "../../../messageclient/element/CBaseSkill.h"
#include "../../../gamescene_state/GameSceneEffects.h"
#include "../../skill/GameFightSkill.h"
#include "../../../messageclient/element/CDrug.h"
#include "AppMacros.h"
#include "../../../ui/backpackscene/GeneralsListForTakeDrug.h"
#include "../../../ui/backpackscene/PackageScene.h"
#include "../../../messageclient/element/GoodsInfo.h"
#include "../../../newcomerstory/NewCommerStoryManager.h"
#include "../../../ui/skillscene/SkillScene.h"
#include "../../../utils/GameUtils.h"
#include "../../../GameAudio.h"

#define ParticleEffect_Musou_Tag 258
#define kTag_CBSkill 555
#define kTag_HightLight 556
#define kTag_ProgressTimer_skill 557
#define kTag_adorn 558
#define kTag_skillVariety 559
#define kTag_FireAnm 560

ShortcutSlot::ShortcutSlot():
slotType(T_Void),
slotUseType(T_CanUse),
curFolderIndex(0.0f),
num(-1),
m_fNormalScale(1.0f)
{
}


ShortcutSlot::~ShortcutSlot()
{
	CC_SAFE_DELETE(curShortCut);
}


ShortcutSlot* ShortcutSlot::create(int index)
{
	ShortcutSlot * shortcutSlot = new ShortcutSlot();
	if (shortcutSlot && shortcutSlot->init(index))
	{
		shortcutSlot->autorelease();
		return shortcutSlot;
	}
	CC_SAFE_DELETE(shortcutSlot);
	return NULL;
}

ShortcutSlot* ShortcutSlot::createPeerless()
{
	ShortcutSlot * shortcutSlot = new ShortcutSlot();
	if (shortcutSlot && shortcutSlot->initPeerless())
	{
		shortcutSlot->autorelease();
		return shortcutSlot;
	}
	CC_SAFE_DELETE(shortcutSlot);
	return NULL;
}
bool ShortcutSlot::init(int index)
{
	if (UIScene::init())
	{
		curIndex = index;
		// Add the button
		/*************************SKILL****************************/  
		CCRect fullRect = CCRectMake(0,0, 0, 0);
		CCRect insetRect = CCRectMake(0,0,0,0);

		layer_icon =  UILayer::create();
		layer_num =  UILayer::create();
		addChild(layer_icon);
		addChild(layer_num);

		btn_bgFrame = UIButton::create();
		btn_bgFrame->setTextures(SKILLEMPTYPATH,SKILLEMPTYPATH,"");
		btn_bgFrame->setTouchEnable(true);
		btn_bgFrame->setAnchorPoint(ccp(0.5f, 0.5f));
		btn_bgFrame->setPosition(ccp(0,0));
		layer_icon->addWidget(btn_bgFrame);

		UIImageView * imageView_hightLight = UIImageView::create();
		imageView_hightLight->setTexture(HIGHLIGHT3);
		imageView_hightLight->setAnchorPoint(ccp(0.5f, 0.5f));
		imageView_hightLight->setPosition(ccp(0,3));
		imageView_hightLight->setName("gaoguang");
		btn_bgFrame->addChild(imageView_hightLight);

// 		UIImageView * imageView_dressUp = UIImageView::create();
// 		imageView_dressUp->setTexture("gamescene_state/zhujiemian3/jinengqu/lolo.png");
// 		imageView_dressUp->setAnchorPoint(ccp(0.5f, 0.5f));
// 		imageView_dressUp->setPosition(ccp(0,0));
// 		imageView_dressUp->setName("imageView_dressUp");
// 		btn_bgFrame->addChild(imageView_dressUp);

		curShortCut = new CShortCut();

// 		if (index == 0)
// 		{
// 			btn_bgFrame->setScale(1.0f);
// 		}
// 		else
// 		{
// 			btn_bgFrame->setScale(0.85f);
// 		}

		this->setContentSize(ccp(btn_bgFrame->getSize().width*btn_bgFrame->getScale(),btn_bgFrame->getSize().height*btn_bgFrame->getScale()));
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		return true;
	}
	return false;
}


bool ShortcutSlot::initPeerless()
{
	if (UIScene::init())
	{
		//龙魂特效
		CCParticleSystem* particleEffect_musou = CCParticleSystemQuad::create("animation/texiao/particledesigner/longhundi.plist");
		particleEffect_musou->setPositionType(kCCPositionTypeGrouped);
		particleEffect_musou->setPosition(ccp(0,0));
		particleEffect_musou->setScale(0.8f);
		particleEffect_musou->setTag(ParticleEffect_Musou_Tag);
		addChild(particleEffect_musou);
		particleEffect_musou->setVisible(false);
		// Add the button
		/*************************PeerlessSkill****************************/  
		CCRect fullRect = CCRectMake(0,0, 0, 0);
		CCRect insetRect = CCRectMake(0,0,0,0);

		layer_icon =  UILayer::create();
		layer_num =  UILayer::create();
		addChild(layer_icon);
		addChild(layer_num);

		btn_preelessBgFrame = UIButton::create();
		btn_preelessBgFrame->setTextures(MUSOUSKILL_DI,MUSOUSKILL_DI,"");
		btn_preelessBgFrame->setTouchEnable(true);
		btn_preelessBgFrame->setAnchorPoint(ccp(0.5f, 0.5f));
		btn_preelessBgFrame->setPosition(ccp(0,0));
		btn_preelessBgFrame->addReleaseEvent(this,coco_releaseselector(ShortcutSlot::PreelessEvent));
		layer_icon->addWidget(btn_preelessBgFrame);

		imageView_time = UIImageView::create();
		imageView_time->setTexture(MUSOUSKILL_TIME);
		imageView_time->setAnchorPoint(ccp(0,0));
		imageView_time->setPosition(ccp(-29,-29));
		imageView_time->setTextureRect(CCRectMake(0,58,0,29));
		layer_icon->addWidget(imageView_time);

		imageView_prellIcon = UIImageView::create();
		imageView_prellIcon->setTexture("gamescene_state/zhujiemian3/jinengqu/huoqiuchendi.png");
		imageView_prellIcon->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_prellIcon->setPosition(ccp(0,0));
		layer_icon->addWidget(imageView_prellIcon);

		CCLegendAnimation * lam_fire = CCLegendAnimation::create("animation/texiao/changjingtexiao/longhunhuoqiu1/longhunhuoqiu1.anm");
		if (lam_fire)
		{
			lam_fire->setPlayLoop(true);
			lam_fire->setReleaseWhenStop(true);
			lam_fire->setPosition(ccp(0,-2));
			lam_fire->setScale(0.5f);
			lam_fire->setTag(kTag_FireAnm);
			layer_icon->addChild(lam_fire);
		}

		UIImageView * imageView_bg = UIImageView::create();
		imageView_bg->setTexture(MUSOUSKILL);
		imageView_bg->setAnchorPoint(ccp(0.5,0.5));
		imageView_bg->setPosition(ccp(0,8));
		layer_icon->addWidget(imageView_bg);

		//高光
// 		UIImageView * highLight = UIImageView::create();
// 		highLight->setTexture(HIGHLIGHT1);
// 		highLight->setAnchorPoint(ccp(0.5f, 0.5f));
// 		highLight->setPosition(ccp(0,1));
// 		m_pUiLayer->addWidget(highLight);

		//float per = (GameView::getInstance()->myplayer->getAngerValue()*1.0f)/(GameView::getInstance()->myplayer->getAngerCapacity()*1.0f);
		//setPeerlessAngerEffect(per);

		curShortCut = new CShortCut();
		this->setContentSize(CCSizeMake(64,64));
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		return true;
	}
	return false;
}



void ShortcutSlot::touchDownAction(CCObject *senderz, CCControlEvent controlEvent)
{
	CCControlButton* btn = (CCControlButton*)senderz;

	curIndex = curShortCut->index();

	if (slotType == T_Void)
	{

	}
	else if (slotType == T_Skill)
	{
		if (this->slotUseType == T_CanNotUse)
		{
// 			const char *str1 = StringDataManager::getString("skillSlot_canNotUse");
// 			char* p1 =const_cast<char*>(str1);
// 			GameView::getInstance()->showAlertDialog(p1);
			ShortcutLayer * shortcutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
			if (shortcutLayer)
			{
				if (shortcutLayer->isImmobilize())
				{
					GameView::getInstance()->showAlertDialog(StringDataManager::getString("skillSlot_isImmobilize"));
				}
				else if (!shortcutLayer->isEnoughMagic(curShortCut))
				{
					GameView::getInstance()->showAlertDialog(StringDataManager::getString("skillSlot_magicNotEnough"));
				}
			}
	
		}
		else if (this->slotUseType == T_CanUse)
		{
			if (mProgressTimer_skill->isVisible())
			{
				//GameView::getInstance()->showAlertDialog("can not do it");
			}
			else
			{
// 				switch(slotType)
// 				{
// 				case T_Void :
// 					break;
// 				case T_Skill :
// 					{
						useSkill();
// 					}
// 					break;
// 				case T_Goods :
// 					{
// 						useGoods();
// 					}
// 					break;
// 				case T_InstanceGoods :
// 					break;
// 				}

				
			}
		}
		else{}
	}
	else if (slotType == T_Goods)
	{
		ShortcutLayer * shortcutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
		if (shortcutLayer)
		{
			if (!shortcutLayer->isEnoughGoods(curShortCut))
			{
				GameView::getInstance()->showAlertDialog(StringDataManager::getString("skillSlot_goodsNotEnough"));
			}
			else
			{
				useGoods();
			}
		}
	}
}
	

void ShortcutSlot::touchDragInsideAction(CCObject *sender, CCControlEvent controlEvent)
{

}

void ShortcutSlot::touchDragOutsideAction(CCObject *sender, CCControlEvent controlEvent)
{

}

void ShortcutSlot::touchDragEnterAction(CCObject *sender, CCControlEvent controlEvent)
{

}

void ShortcutSlot::touchDragExitAction(CCObject *sender, CCControlEvent controlEvent)
{

}

void ShortcutSlot::touchUpInsideAction(CCObject *sender, CCControlEvent controlEvent)
{
}

void ShortcutSlot::touchUpOutsideAction(CCObject *sender, CCControlEvent controlEvent)
{
}

void ShortcutSlot::touchCancelAction(CCObject *sender, CCControlEvent controlEvent)
{

}
void ShortcutSlot::skillCoolDownCallBack( CCNode* node )
{
	mProgressTimer_skill->setVisible(false);
	CBSkill->setEnabled(true);

	CCSequence * sequence = CCSequence::create(
		CCEaseBackIn::create(CCScaleTo::create(0.15f,1.3f*m_fNormalScale)),
		CCScaleTo::create(0.15f,1.0f*m_fNormalScale),
		NULL);
	this->runAction(sequence);
}
void ShortcutSlot::initType( int _typeid)
{
	switch(_typeid)
	{
	case 0 :
		slotType = T_Void;
		break;
	case 1 :
		slotType = T_Skill;
		break;
	case 2 :
		slotType = T_Goods;
		break;
	case 3 :
		slotType = T_InstanceGoods;
		break;
	}
}

void ShortcutSlot::useSkill()
{
// 	if (curIndex == 1 || curIndex == 0)
// 	{
		Script* sc = ScriptManager::getInstance()->getScriptById(((ShortcutLayer*)this->getParent())->mTutorialScriptInstanceId);
		if(sc != NULL)
			sc->endCommand(this);
//	}

	//add by yangjun 2014.9.12
	ShortcutLayer * shortcutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
	sc = ScriptManager::getInstance()->getScriptById(shortcutLayer->mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(shortcutLayer);

 	std::string skillId = curShortCut->skillpropid();
	//std::string skillId = FightSkill_SKILL_ID_PUTONGGONGJI;
	if (skillId.size()>0)
	{
// 		float mCDTime_skill;
// 		if (GameView::getInstance()->GameFightSkillList.find(skillId) != GameView::getInstance()->GameFightSkillList.end())
// 		{
// 			if (GameView::getInstance()->GameFightSkillList.find(skillId)->second->getCFightSkill()->has_intervaltime())
// 			{
// 				mCDTime_skill = GameView::getInstance()->GameFightSkillList.find(skillId)->second->getCFightSkill()->intervaltime()/1000.0f;
// 			}
// 			else
// 			{
// 				mCDTime_skill = 1.0f;
// 			}
// 		}
// 		else
// 		{
// 			mCDTime_skill = 1.0f;
// 		}

		MyPlayer* myplayer = GameView::getInstance()->myplayer;
		myplayer->useSkill(skillId, true);

	}
}

void ShortcutSlot::useGoods()
{
	if(CDrug::isDrug(curShortCut->skillpropid()) )
	{
		if (!DrugManager::getInstance()->getDrugById(curShortCut->skillpropid()))
			return;

		CDrug* pDrug = DrugManager::getInstance()->getDrugById(curShortCut->skillpropid());
		if (pDrug->isInCD())
			return;

		FolderInfo * folderInfo = new FolderInfo();
		for (int i = 0;i<(int)GameView::getInstance()->AllPacItem.size();++i)
		{
			if (GameView::getInstance()->AllPacItem.at(i)->has_goods())
			{
				if (strcmp(curShortCut->skillpropid().c_str(),GameView::getInstance()->AllPacItem.at(i)->goods().id().c_str()) == 0)
				{
					folderInfo->CopyFrom(*GameView::getInstance()->AllPacItem.at(i));
					break;
				}
			}
		}

		if(!folderInfo->has_goods())
			return;

		if (folderInfo->goods().clazz() == GOODS_CLASS_HUIHUNDAN)//武将用
		{
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralListForTakeDrug) == NULL)
			{
				CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
				GeneralsListForTakeDrug * generalListForTakeDrug = GeneralsListForTakeDrug::create(folderInfo);
				generalListForTakeDrug->ignoreAnchorPointForPosition(false);
				generalListForTakeDrug->setAnchorPoint(ccp(0.5f,0.5f));
				generalListForTakeDrug->setPosition(ccp(winSize.width/2,winSize.height/2));
				GameView::getInstance()->getMainUIScene()->addChild(generalListForTakeDrug,0,kTagGeneralListForTakeDrug);
			}
		}
		else if (folderInfo->goods().clazz() == GOODS_CLASS_YAOSHUI)
		{
			std::map<std::string,CDrug*>::const_iterator cIter;
			cIter = CDrugMsgConfigData::s_drugBaseMsg.find(folderInfo->goods().id());
			if (cIter != CDrugMsgConfigData::s_drugBaseMsg.end()) // 没找到就是指向END了  
			{
				CDrug * temp =  CDrugMsgConfigData::s_drugBaseMsg.find(folderInfo->goods().id())->second;
				if (temp->get_type_flag() == 2)//武将用
				{
					if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralListForTakeDrug) == NULL)
					{
						CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
						GeneralsListForTakeDrug * generalListForTakeDrug = GeneralsListForTakeDrug::create(folderInfo);
						generalListForTakeDrug->ignoreAnchorPointForPosition(false);
						generalListForTakeDrug->setAnchorPoint(ccp(0.5f,0.5f));
						generalListForTakeDrug->setPosition(ccp(winSize.width/2,winSize.height/2));
						GameView::getInstance()->getMainUIScene()->addChild(generalListForTakeDrug,0,kTagGeneralListForTakeDrug);
					}
				}
				else  //角色用
				{
					int index = this->getCurFonderId();
					if(index != -1)
					{
						PackageScene::IdxAndNum * temp = new PackageScene::IdxAndNum();
						temp->index = index;
						temp->num = 1;
						GameMessageProcessor::sharedMsgProcessor()->sendReq(1302, (void *)0,temp);
						delete temp;
					}
					else
					{
						//GameView::getInstance()->showAlertDialog(StringDataManager::getString("potion_not_enough"));
					}
				}
			}
		}
		else
		{
			CDrug* pDrug = DrugManager::getInstance()->getDrugById(curShortCut->skillpropid());
			if (pDrug->isInCD())
				return;

			int index = this->getCurFonderId();
			if(index != -1)
			{
				PackageScene::IdxAndNum * temp = new PackageScene::IdxAndNum();
				temp->index = index;
				temp->num = 1;
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1302, (void *)0,temp);
				delete temp;
			}
			else
			{
				//GameView::getInstance()->showAlertDialog(StringDataManager::getString("potion_not_enough"));
			}
		}

		delete folderInfo;
	}
	else
	{
		int index = this->getCurFonderId();
		if(index != -1)
		{
			PackageScene::IdxAndNum * temp = new PackageScene::IdxAndNum();
			temp->index = index;
			temp->num = 1;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1302, (void *)0,temp);
			delete temp;
		}
		else
		{
			//GameView::getInstance()->showAlertDialog(StringDataManager::getString("potion_not_enough"));
		}
	}
}

int ShortcutSlot::getCurFonderId()
{
	for (int i = 0;i<(int)GameView::getInstance()->AllPacItem.size();++i)
	{
		if (GameView::getInstance()->AllPacItem.at(i)->has_goods())
		{
			if (strcmp(curShortCut->skillpropid().c_str(),GameView::getInstance()->AllPacItem.at(i)->goods().id().c_str()) == 0)
			{
				return GameView::getInstance()->AllPacItem.at(i)->id();
			}
		}
	}

	//CCAssert(false, "please check it, should not run to here");
	return -1;
}

void ShortcutSlot::setInfo( CShortCut *shortCut )
{
	if(btn_bgFrame->getChildByName("gaoguang"))
	{
		btn_bgFrame->getChildByName("gaoguang")->removeFromParent();
	}

// 	if(btn_bgFrame->getChildByName("imageView_dressUp"))
// 	{
// 		btn_bgFrame->getChildByName("imageView_dressUp")->removeFromParent();
// 	}

	if (layer_icon->getChildByTag(kTag_CBSkill))
	{
		layer_icon->getChildByTag(kTag_CBSkill)->removeFromParent();
	}

	if (layer_icon->getChildByTag(kTag_HightLight))
	{
		layer_icon->getChildByTag(kTag_HightLight)->removeFromParent();
	}

	if (layer_icon->getChildByTag(kTag_ProgressTimer_skill))
	{
		layer_icon->getChildByTag(kTag_ProgressTimer_skill)->removeFromParent();
	}

	btn_bgFrame->setTouchEnable(false);
	initType((int)shortCut->storedtype());
	
	curShortCut->CopyFrom(*shortCut);
	curIndex = curShortCut->index();

	CCSize screenSize = CCDirector::sharedDirector()->getVisibleSize();
	CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();
	//根据类型不同显示样式不同
	if (slotType == T_Void)
	{
		// 调用CCScale9Sprite
		CCScale9Sprite *sprite = CCScale9Sprite::create("gamescene_state/zhujiemian3/jinengqu/suibian.png"); 
		CBSkill = CCControlButton::create(sprite);
		CBSkill->setPreferredSize(CCSize(46, 46));  
		CBSkill->setAnchorPoint(ccp(0.5f, 0.5f));
		CBSkill->setPosition(ccp(0,3));
		CBSkill->setTouchEnabled(true);
	}
	else if (slotType == T_Skill)
	{
		CCSprite * sprite_skillIcondi = CCSprite::create("gamescene_state/zhujiemian3/jinengqu/mengban_round.png");
		sprite_skillIcondi->setAnchorPoint(ccp(0.5f,0.5f));
		sprite_skillIcondi->setPosition(ccp(0,0));
// 		if (curIndex == 0)
// 		{
// 			sprite_skillIcondi->setScale(1.0f);
// 		}
// 		else
// 		{
// 			sprite_skillIcondi->setScale(0.85f);
// 		}
		layer_icon->addChild(sprite_skillIcondi);
		// 调用CCScale9Sprite
		std::string skillIconPath = "res_ui/jineng_icon/";

		if (shortCut->has_skillpropid())
		{
			CBaseSkill * baseSkill = StaticDataBaseSkill::s_baseSkillData[shortCut->skillpropid()];
			if (baseSkill)
			{
				skillIconPath.append(baseSkill->icon());
			}
			else
			{
				skillIconPath.append("jineng_2");
			}

			skillIconPath.append(".png");
		}
		else
 		{
			skillIconPath = "gamescene_state/zhujiemian3/jinengqu/suibian.png";
		}

		CCScale9Sprite *sprite = CCScale9Sprite::create(skillIconPath.c_str()); 
		sprite->setPreferredSize(CCSizeMake(46,46));
		CBSkill = CCControlButton::create(sprite);
		CBSkill->setPreferredSize(CCSize(46, 46));  
		CBSkill->setAnchorPoint(ccp(0.5f, 0.5f));
		CBSkill->setPosition(ccp(0,0));
		CBSkill->setTouchEnabled(true);
	}
	else if (slotType == T_Goods)
	{
		// 调用CCScale9Sprite
		std::string skillIconPath = "res_ui/props_icon/";
		skillIconPath.append(shortCut->icon());
		//skillIconPath.append("yaoshui");
		skillIconPath.append(".png");

		CCScale9Sprite *sprite = CCScale9Sprite::create(skillIconPath.c_str()); 
		sprite->setPreferredSize(CCSizeMake(46,46));
		CBSkill = CCControlButton::create(sprite);
		CBSkill->setPreferredSize(CCSize(46, 46));  
		CBSkill->setAnchorPoint(ccp(0.5f, 0.5f));
		CBSkill->setPosition(ccp(0,0));
		CBSkill->setTouchEnabled(true);

// 		CCScale9Sprite * sprite_numFrame = CCScale9Sprite::create("");
// 		sprite_numFrame->setCapInsets(CCRectMake());
// 		sprite_numFrame->setPreferredSize(); 
// 		sprite_numFrame->setAnchorPoint(0.5f,0);
// 		sprite_numFrame->setPosition(ccp(0,-27));
// 		layer_num->addChild(sprite_numFrame);
		CCSprite * sprite_numFrame = CCSprite::create("gamescene_state/zhujiemian3/jinengqu/zhaozhao00.png");
		sprite_numFrame->setAnchorPoint(ccp(0.5f,0));
		sprite_numFrame->setPosition(ccp(0,-26));
		layer_num->addChild(sprite_numFrame);

		num = 0;
		for (int i = 0;i<(int)GameView::getInstance()->AllPacItem.size();++i)
		{
			if (GameView::getInstance()->AllPacItem.at(i)->has_goods())
			{
				if (strcmp(curShortCut->skillpropid().c_str(),GameView::getInstance()->AllPacItem.at(i)->goods().id().c_str()) == 0)
				{
					num += GameView::getInstance()->AllPacItem.at(i)->quantity();
				}
			}
		}
		char s_num[20];
		sprintf(s_num,"%d",num);
		label_goodsNum = CCLabelTTF::create(s_num,APP_FONT_NAME,12);
		label_goodsNum->setAnchorPoint(ccp(0.5f,0));
		label_goodsNum->setPosition(ccp(0,-30));
		ccColor3B shadowColor = ccc3(0,0,0);   // black
		label_goodsNum->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		layer_num->addChild(label_goodsNum);
	}

	//高光
	CCSprite * highLight = CCSprite::create(HIGHLIGHT3);
	highLight->ignoreAnchorPointForPosition(false);
	highLight->setAnchorPoint(ccp(0.5f, 0.5f));
	highLight->setPosition(ccp(0,3));
	//CBSkill->addChild(highLight);

	// 添加旋转进度条精灵---半阴影 上层
	progress_skill = CCSprite::create(PROGRESS_SKILL);
	mProgressTimer_skill = CCProgressTimer::create(progress_skill);
	mProgressTimer_skill->setOpacity(200);
	mProgressTimer_skill->setAnchorPoint(ccp(0.5f, 0.5f));
	mProgressTimer_skill->setPosition(ccp(0,1));
	mProgressTimer_skill->setVisible(false);
	//CBSkill->addChild(mProgressTimer_skill, 600);  
	CBSkill->setTouchEnabled(true);
	layer_icon->addChild(CBSkill, 1000,kTag_CBSkill);   
	layer_icon->addChild(highLight,1200,kTag_HightLight);   
	layer_icon->addChild(mProgressTimer_skill,1500,kTag_ProgressTimer_skill);   
 	CBSkill->setScale(1.2f);
 	mProgressTimer_skill->setScale(1.2f);
 	highLight->setScale(1.0f);

// 	CCSprite * sprite_dressUp = CCSprite::create("gamescene_state/zhujiemian3/jinengqu/lolo.png");
// 	sprite_dressUp->setAnchorPoint(ccp(0.5f, 0.5f));
// 	sprite_dressUp->setPosition(ccp(0,-23));
// 	layer_icon->addChild(sprite_dressUp,2000,kTag_adorn);

	//skillVariety
	if (slotType == T_Skill)
	{
		if (shortCut->has_skillpropid())
		{
			CCNode * sprite_skillVariety = SkillScene::GetSkillVarirtySprite(shortCut->skillpropid().c_str());
			if (sprite_skillVariety)
			{
				sprite_skillVariety->setAnchorPoint(ccp(0.5f,0.5f));
				sprite_skillVariety->setPosition(ccp(15,-17));
				sprite_skillVariety->setScale(1.15f);
				layer_icon->addChild(sprite_skillVariety,2000,kTag_skillVariety);
			}
		}
	}

// 	if (curIndex == 0)
// 	{
// 		CBSkill->setScale(1.15f);
// 		mProgressTimer_skill->setScale(1.15f);
// 		highLight->setScale(1.0f);
// // 		sprite_dressUp->setScale(1.0f);
// // 		sprite_dressUp->setPosition(ccp(0,-23));
// 	}
// 	else
// 	{
// 		CBSkill->setScale(1.0f);
// 		mProgressTimer_skill->setScale(1.0f);
// 		highLight->setScale(0.85f);
// // 		sprite_dressUp->setScale(0.85f);
// // 		sprite_dressUp->setPosition(ccp(0,-20));
// 	}

	// Sets up event handlers
	CBSkill->addTargetWithActionForControlEvents(this, cccontrol_selector(ShortcutSlot::touchDownAction), CCControlEventTouchDown);
	CBSkill->addTargetWithActionForControlEvents(this, cccontrol_selector(ShortcutSlot::touchDragInsideAction), CCControlEventTouchDragInside);
	CBSkill->addTargetWithActionForControlEvents(this, cccontrol_selector(ShortcutSlot::touchDragOutsideAction), CCControlEventTouchDragOutside);
	CBSkill->addTargetWithActionForControlEvents(this, cccontrol_selector(ShortcutSlot::touchDragEnterAction), CCControlEventTouchDragEnter);
	CBSkill->addTargetWithActionForControlEvents(this, cccontrol_selector(ShortcutSlot::touchDragExitAction), CCControlEventTouchDragExit);
	CBSkill->addTargetWithActionForControlEvents(this, cccontrol_selector(ShortcutSlot::touchUpInsideAction), CCControlEventTouchUpInside);
	CBSkill->addTargetWithActionForControlEvents(this, cccontrol_selector(ShortcutSlot::touchUpOutsideAction), CCControlEventTouchUpOutside);
	CBSkill->addTargetWithActionForControlEvents(this, cccontrol_selector(ShortcutSlot::touchCancelAction), CCControlEventTouchCancel);

	RefreshCD();
}

// void ShortcutSlot::setPeerlessInfo( CShortCut *shortCut )
// {
// 	btn_preelessBgFrame->addReleaseEvent(this,coco_releaseselector(ShortcutSlot::PreelessEvent));
// 	initType((int)shortCut->storedtype());
// 
// 	curShortCut->CopyFrom(*shortCut);
// 
// 	CCSize screenSize = CCDirector::sharedDirector()->getVisibleSize();
// 	CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();
// 	// 调用CCScale9Sprite
// 	std::string skillIconPath = "res_ui/jineng_icon/";
// 
// 	if (shortCut->has_skillpropid())
// 	{
// 		CBaseSkill * baseSkill = StaticDataBaseSkill::s_baseSkillData[shortCut->skillpropid()];
// 		if (baseSkill)
// 		{
// 			skillIconPath.append(baseSkill->icon());
// 		}
// 		else
// 		{
// 			skillIconPath.append("jineng_2");
// 		}
// 
// 		skillIconPath.append(".png");
// 	}
// 	else
// 	{
// 		skillIconPath = "gamescene_state/zhujiemian3/jinengqu/suibian.png";
// 	}
// 
// 	imageView_prellIcon->setTexture(skillIconPath.c_str());
// 
// 
// 	// the musou skill is not open, so ignore
// 	int musouSkillOpenLevel = ShortcutSlotOpenLevelConfigData::s_shortcutSlotOpenLevelMsg[7];
// 	if (GameView::getInstance()->myplayer->getActiveRole()->level() < musouSkillOpenLevel)
// 		return;
// 
// 	float per = (GameView::getInstance()->myplayer->getAngerValue()*1.0f)/(GameView::getInstance()->myplayer->getAngerCapacity()*1.0f);
// 	setPeerlessAngerEffect(per);
// }

void ShortcutSlot::setUseType(int index,bool canUse)
{
	if (this->getTag()-100 == index)
	{
		if (canUse)
		{
			this->slotUseType = T_CanUse;
			if (this->getChildByTag(3210) != NULL)
			{
				CCSprite * image_jinyong = (CCSprite*)this->getChildByTag(3210);
				image_jinyong->removeFromParent();
			}
		}
		else
		{
			this->slotUseType = T_CanNotUse;
			//create image jinyong
			if (this->getChildByTag(3210) == NULL)
			{
				CCSprite * image_jinyong = CCSprite::create("res_ui/jineng/disable.png");
				image_jinyong->ignoreAnchorPointForPosition(false);
				image_jinyong->setAnchorPoint(ccp(0.5f, 0.5f));
				image_jinyong->setPosition(ccp(0,0));
				image_jinyong->setZOrder(1000);
				image_jinyong->setTag(3210);
				image_jinyong->setScale(1.35f);
				addChild(image_jinyong);

// 				if (curIndex == 0)
// 				{
// 					image_jinyong->setScale(1.1f*1.15f);
// 				}
// 				else
// 				{
// 					image_jinyong->setScale(1.1f*1.0f);
// 				}
			}
		}
	}
}

void ShortcutSlot::onEnter()
{
	UIScene::onEnter();
}
void ShortcutSlot::onExit()
{
	UIScene::onExit();
}	

bool ShortcutSlot::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
	return false;	
}
void ShortcutSlot::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
{

}
void ShortcutSlot::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
{

}
void ShortcutSlot::ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent)
{
}

float ShortcutSlot::getCurProgress()
{
	return mProgressTimer_skill->getPercentage();
}

void ShortcutSlot::BeganToRunCD()
{
	mProgressTimer_skill->setVisible(true);
	CBSkill->setTouchEnabled(true);
	mProgressTimer_skill->setType(kCCProgressTimerTypeRadial);
	mProgressTimer_skill->setReverseProgress(true); // 设置进度条为逆时针

// 	std::string skillId = curShortCut->skillpropid();
// 	float coolTime = (float)(GameView::getInstance()->myplayer->getGameFightSkill(skillId.c_str())->getCoolTime()/1000);
	float mCDTime_skill;
	if (curShortCut->storedtype() == 1)
	{
		std::string skillId = curShortCut->skillpropid();
		if (skillId.size()>0)
		{

			if (GameView::getInstance()->GameFightSkillList.find(skillId) != GameView::getInstance()->GameFightSkillList.end())
			{
				if (GameView::getInstance()->GameFightSkillList.find(skillId)->second->getCFightSkill()->has_intervaltime())
				{
					mCDTime_skill = GameView::getInstance()->GameFightSkillList.find(skillId)->second->getCFightSkill()->intervaltime()/1000.0f;
				}
				else
				{
					mCDTime_skill = 1.0f;
				}
			}
			else
			{
				mCDTime_skill = 1.0f;
			}
		}
		else
		{
			mCDTime_skill = 1.0f;
		}
	}
	else if (curShortCut->storedtype() == 2)
	{
		if (CDrug::isDrug(curShortCut->skillpropid()))
		{
			if (DrugManager::getInstance()->getDrugById(curShortCut->skillpropid()))
			{
				CDrug* pDrug = DrugManager::getInstance()->getDrugById(curShortCut->skillpropid());
				mCDTime_skill = pDrug->get_cool_time()/1000.0f;
			}
		}
	}
	
	//设置一个最短时间（2014.4.10），目前主要用于各职业普攻的默认cd时间
	if(mCDTime_skill <= 0)
		mCDTime_skill = 0.3f;

	action_progress_from_to = CCProgressFromTo::create(mCDTime_skill , 100, 0);     
	action_callback = CCCallFuncN::create(this, callfuncN_selector(ShortcutSlot::skillCoolDownCallBack));
	mProgressTimer_skill->runAction(CCSequence::create(action_progress_from_to, action_callback, NULL));
}

void ShortcutSlot::DoNothing( CCObject *pSender )
{
	//CCLOG("donothing !!!!!!!!!");
}

void ShortcutSlot::useMusouSkill()
{
	GameView::getInstance()->myplayer->useMusouSkill();

	if (NewCommerStoryManager::getInstance()->IsNewComer())
	{
		NewCommerStoryManager::getInstance()->setIsReqForUseMusouSkill(true);
	}
}

#define MUSOUSKILL_BUTTON_ACTION_TAG 634
void ShortcutSlot::PreelessEvent( CCObject *pSender )
{
	Script* sc = ScriptManager::getInstance()->getScriptById(((ShortcutLayer*)this->getParent())->mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(this);

	if (GameView::getInstance()->myplayer->hasExStatus(ExStatusType::musou))
	{
		//CCLOG("you can use peerlessSkill now !!! ");
		// 龙魂按钮已经被点击
		if(this->getActionByTag(MUSOUSKILL_BUTTON_ACTION_TAG) != NULL)
			return;

		GameSceneLayer* scene = GameView::getInstance()->getGameScene();

		BaseFighter* myplayer = GameView::getInstance()->myplayer;
		// Effect 1
		//myplayer->onSkillReleased(FIGHTSKILL_ID_MUSOU_EXTRA, NULL, 0, 0);
		//useMusouSkill();
		//return;
		//add sound effect
		GameUtils::playGameSound(EFFECT_MUSOU, 2, false);
		// Effect 2
		CCNode* sceneLayer = scene->getChildByTag(GameSceneLayer::kTagSceneLayer);
		CCPoint screenPos = myplayer->getPosition() + sceneLayer->getPosition();
		MusouSpecialEffect* effectNode = MusouSpecialEffect::create(screenPos);
		scene->addChild(effectNode, SCENE_STORY_LAYER_BASE_ZORDER);

		CCActionInterval *action = (CCActionInterval*)CCSequence::create(
				CCDelayTime::create(effectNode->getDuration()),
				CCCallFunc::create(this, callfunc_selector(ShortcutSlot::useMusouSkill)), 
				CCDelayTime::create(1.0f),   // extra delay to avoid the player click the musou skill button quickly
				NULL);
		action->setTag(MUSOUSKILL_BUTTON_ACTION_TAG);
		runAction(action);
	}
	else
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("musou_not_enough"));
		//CCLOG("not enought power to use peerlessSkill ... ");
	}
}

void ShortcutSlot::setPeerlessAngerEffect( float percentage )
{
	imageView_time->setTextureRect(CCRectMake(0,58*(1-percentage),58,58*percentage));

	if (percentage >= 1)
	{
		if (this->getChildByTag(ParticleEffect_Musou_Tag))
		{
			(this->getChildByTag(ParticleEffect_Musou_Tag))->setVisible(true);
		}
	}
	else
	{
		if (this->getChildByTag(ParticleEffect_Musou_Tag))
		{
			(this->getChildByTag(ParticleEffect_Musou_Tag))->setVisible(false);
		}
	}

	CCLegendAnimation * lam_fire = (CCLegendAnimation*)layer_icon->getChildByTag(kTag_FireAnm);
	if (lam_fire)
	{
		lam_fire->setScale(0.5f*(1.0f+percentage));
	}
}

void ShortcutSlot::refreshRetainNum( FolderInfo * folderInfo )
{
	num = 0;
	if (folderInfo->has_goods())
	{
		for (int i = 0;i<(int)GameView::getInstance()->AllPacItem.size();++i)
		{
			if (GameView::getInstance()->AllPacItem.at(i)->has_goods())
			{
				if (strcmp(folderInfo->goods().id().c_str(),GameView::getInstance()->AllPacItem.at(i)->goods().id().c_str()) == 0)
				{
					num += GameView::getInstance()->AllPacItem.at(i)->quantity();
				}
			}
		}
	}
	else
	{
		num = 0;
	}
	char s_num[20];
	sprintf(s_num,"%d",num);
	label_goodsNum->setString(s_num);
}

void ShortcutSlot::RefreshCD()
{
	// 	std::string skillId = curShortCut->skillpropid();
	// 	float coolTime = (float)(GameView::getInstance()->myplayer->getGameFightSkill(skillId.c_str())->getCoolTime()/1000);
	float mCDTime_skill = 0.0f;
	float remaintimes = 0.0f;
	if (curShortCut->storedtype() == 1)
	{
		std::string skillId = curShortCut->skillpropid();
		if (skillId.size()>0)
		{

			if (GameView::getInstance()->GameFightSkillList.find(skillId) != GameView::getInstance()->GameFightSkillList.end())
			{
				if (GameView::getInstance()->GameFightSkillList.find(skillId)->second->getCFightSkill()->has_intervaltime())
				{
					mCDTime_skill = GameView::getInstance()->GameFightSkillList.find(skillId)->second->getCFightSkill()->intervaltime()/1000.0f;
				}
				else
				{
					mCDTime_skill = 1.0f;
				}
			}
			else
			{
				mCDTime_skill = 1.0f;
			}
		}
		else
		{
			mCDTime_skill = 1.0f;
		}

		remaintimes = (GameView::getInstance()->myplayer->getGameFightSkill(skillId.c_str(), skillId.c_str()))->getRemainCD()/1000.0f;
	}
	else if (curShortCut->storedtype() == 2)
	{
		if (CDrug::isDrug(curShortCut->skillpropid()))
		{
			if (DrugManager::getInstance()->getDrugById(curShortCut->skillpropid()))
			{
				CDrug* pDrug = DrugManager::getInstance()->getDrugById(curShortCut->skillpropid());
				mCDTime_skill = pDrug->get_cool_time()/1000.0f;
				remaintimes = pDrug->getRemainTime()/1000.0f;
			}
		}
	}

	//设置一个最短时间（2014.4.10），目前主要用于各职业普攻的默认cd时间
	if(mCDTime_skill <= 0)
		mCDTime_skill = 0.3f;	

	if (remaintimes <= 0)
	{
		remaintimes = 0.0f;
		return;
	}

	mProgressTimer_skill->setVisible(true);
	CBSkill->setTouchEnabled(true);
	mProgressTimer_skill->setType(kCCProgressTimerTypeRadial);
	mProgressTimer_skill->setReverseProgress(true); // 设置进度条为逆时针

	action_progress_from_to = CCProgressFromTo::create(remaintimes , (remaintimes/mCDTime_skill)*100, 0);     
	action_callback = CCCallFuncN::create(this, callfuncN_selector(ShortcutSlot::skillCoolDownCallBack));
	mProgressTimer_skill->runAction(CCSequence::create(action_progress_from_to, action_callback, NULL));
}

void ShortcutSlot::setScale( float scale )
{
	UIScene::setScale(scale);
	m_fNormalScale = scale;
}
