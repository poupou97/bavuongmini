
#ifndef _GAMESCENESTATE_SHORTCUTCONFIGURE_H_
#define _GAMESCENESTATE_SHORTCUTCONFIGURE_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "ShortcutLayer.h"
#include "../../../legend_script/CCTutorialIndicator.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CShortCut;

class ShortcutConfigure : public ShortcutLayer
{
public:
	ShortcutConfigure();
	~ShortcutConfigure();

	static ShortcutConfigure * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

	void CloseEvent(CCObject *pSender);
	void ButtonSkillEvent(CCObject *pSender);
	void sendReqEquipSkill(int index);

	void RefreshOneShortcutConfigure(CShortCut * shortcut);

	//��ѧ
	UIButton * btn_close;
	UIButton * btn_first;
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator1(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction );
	void addCCTutorialIndicator2(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction );
	void removeCCTutorialIndicator();

private:
	//��ѧ
	int mTutorialScriptInstanceId;

};
#endif;

