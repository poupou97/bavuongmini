

#ifndef _GAMESCENESTATE_SHORTCUTSLOT_H_
#define _GAMESCENESTATE_SHORTCUTSLOT_H_

#include "cocos-ext.h"
#include "../../../ui/extensions/uiscene.h"
#include "cocos2d.h"
#include "../../../messageclient/protobuf/ModelMessage.pb.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CShortCut;
class FolderInfo;

class ShortcutSlot : public UIScene 
{
public:
	ShortcutSlot();
	~ShortcutSlot();

	static ShortcutSlot* create(int index);
	bool init(int index);
	static ShortcutSlot* createPeerless();
	bool initPeerless();

public:
	enum shortcurSlotType
	{
		T_Void = 0,
		T_Skill = 1,
		T_Goods = 2,
		T_InstanceGoods = 3
	};

	enum useType
	{
		T_CanUse,
		T_CanNotUse
	};
	
	/*下标*/
	int curIndex;
	/*道具数量*/
	int num;
	/*坐标*/
	CCPoint curpos;
	//data
	CShortCut * curShortCut;

	UILayer * layer_icon;
	UILayer * layer_num;

	//当技能为空时（除了无双技能）
	UIButton * btn_bgFrame ;

	UIButton * btn_preelessBgFrame;
	UIImageView * imageView_prellIcon;
	UIImageView * imageView_time;

	useType slotUseType;
	shortcurSlotType slotType;
	std::string slotId;
	std::string slotIcon;
	CCControlButton* CBSkill;
	CCSprite * bgFrame;
	//CCScale9Sprite* backgroundButton;
	CCSprite* progress_skill;
	CCProgressTimer * mProgressTimer_skill;
	CCLabelTTF * label_goodsNum;

	CCActionInterval* action_progress_from_to;
	CCCallFunc* action_callback;

	//物品位置索引
	int curFolderIndex;

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

	void BeganToRunCD();
	void RefreshCD();

	void PreelessEvent(CCObject *pSender);

	void skillCoolDownCallBack(CCNode* node);

	virtual void setScale(float scale);
private:
	float m_fNormalScale;

	void touchDownAction(CCObject *sender, CCControlEvent controlEvent);
	void touchDragInsideAction(CCObject *sender, CCControlEvent controlEvent);
	void touchDragOutsideAction(CCObject *sender, CCControlEvent controlEvent);
	void touchDragEnterAction(CCObject *sender, CCControlEvent controlEvent);
	void touchDragExitAction(CCObject *sender, CCControlEvent controlEvent);
	void touchUpInsideAction(CCObject *sender, CCControlEvent controlEvent);
	void touchUpOutsideAction(CCObject *sender, CCControlEvent controlEvent);
	void touchCancelAction(CCObject *sender, CCControlEvent controlEvent);

	void initType(int _typeid);

	void useSkill();
	void useGoods();

	void DoNothing(CCObject *pSender);

	int getCurFonderId();

public: 
	//设置技能详细信息
	void setInfo(CShortCut *shortCut);
	void setUseType(int index,bool canUse);
	//设置无双技能详细信息
	//void setPeerlessInfo(CShortCut *shortCut);

	//设置无双技能怒气显示
	void setPeerlessAngerEffect(float percentage);

	//刷新剩余数量
	void refreshRetainNum(FolderInfo * folderInfo);

	//获取当前CD进度变化
	float getCurProgress();

private:
	void useMusouSkill();
};
#endif;

