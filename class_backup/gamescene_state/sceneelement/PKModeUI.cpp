#include "PKModeUI.h"
#include "GameView.h"
#include "../role/MyPlayer.h"
#include "AppMacros.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../utils/StaticDataManager.h"

PKModeUI::PKModeUI()
{
}


PKModeUI::~PKModeUI()
{
}


PKModeUI * PKModeUI::create()
{
	PKModeUI * mode = new PKModeUI();
	if (mode && mode->init())
	{
		mode->autorelease();
		return mode;
	}
	CC_SAFE_DELETE(mode);
	return NULL;
}

bool PKModeUI::init()
{
	if (UIScene::init())
	{
		CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
		CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

		UIPanel * mainPanel = (UIPanel*)GUIReader::shareReader()->widgetFromJsonFile("res_ui/pkMode_1.json");
		mainPanel->setAnchorPoint(ccp(0.f,0.f));
		mainPanel->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mainPanel);

		UIButton * button= (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_heping");
		CCAssert(button != NULL, "should not be nil");
		button->setTouchEnable(true);
		button->setPressedActionEnabled(true,0.9f,1.2f);
		button->addReleaseEvent(this,coco_releaseselector(PKModeUI::PeaceEvent));

		UILabel * l_des = (UILabel*)UIHelper::seekWidgetByName(button,"Label_des");
		l_des->setTextAreaSize(CCSizeMake(135,0));

		button= (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_shalu");
		CCAssert(button != NULL, "should not be nil");
		button->setTouchEnable(true);
		button->setPressedActionEnabled(true,0.9f,1.2f);
		button->addReleaseEvent(this,coco_releaseselector(PKModeUI::KillEvent));

		l_des = (UILabel*)UIHelper::seekWidgetByName(button,"Label_des");
		l_des->setTextAreaSize(CCSizeMake(135,0));

		button= (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_jiazu");
		CCAssert(button != NULL, "should not be nil");
		button->setTouchEnable(true);
		button->setPressedActionEnabled(true,0.9f,1.2f);
		button->addReleaseEvent(this,coco_releaseselector(PKModeUI::FamilyEvent));

		l_des = (UILabel*)UIHelper::seekWidgetByName(button,"Label_des");
		l_des->setTextAreaSize(CCSizeMake(135,0));

		button= (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_guojia");
		CCAssert(button != NULL, "should not be nil");
		button->setTouchEnable(true);
		button->setPressedActionEnabled(true,0.9f,1.2f);
		button->addReleaseEvent(this,coco_releaseselector(PKModeUI::CountryEvent));

		l_des = (UILabel*)UIHelper::seekWidgetByName(button,"Label_des");
		l_des->setTextAreaSize(CCSizeMake(135,0));

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(mainPanel->getContentSize());

		return true;
	}
	return false;
}

void PKModeUI::onEnter()
{
	UIScene::onEnter();
}
void PKModeUI::onExit()
{
	UIScene::onExit();
}

bool PKModeUI::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
	return this->resignFirstResponder(pTouch,this,false);
}
void PKModeUI::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
{

}
void PKModeUI::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
{

}
void PKModeUI::ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent)
{

}

void PKModeUI::PeaceEvent( CCObject *pSender )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1207, (void*)0);
	this->closeAnim();
}

void PKModeUI::KillEvent( CCObject *pSender )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1207, (void*)1);
	this->closeAnim();
}

void PKModeUI::FamilyEvent( CCObject *pSender )
{
	if (GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().factionid() == 0)
	{
		const char *dialog_family = StringDataManager::getString("PKMode_family_dialog");
		char* pkMode_dialog_family =const_cast<char*>(dialog_family);
		GameView::getInstance()->showAlertDialog(pkMode_dialog_family);
		return;
	}

	GameMessageProcessor::sharedMsgProcessor()->sendReq(1207, (void*)2);
	this->closeAnim();
}

void PKModeUI::CountryEvent( CCObject *pSender )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1207, (void*)3);
	this->closeAnim();
}
