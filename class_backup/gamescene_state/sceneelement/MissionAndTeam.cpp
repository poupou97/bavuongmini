#include "MissionAndTeam.h"
#include "../GameSceneState.h"
#include "../role/MyPlayer.h"
#include "GameView.h"
#include "MissionCell.h"
#include "../../ui/missionscene/MissionManager.h"
#include "../role/ActorCommand.h"
#include "../role/MyPlayerOwnedCommand.h"
#include "../../ui/extensions/UITab.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "ShowTeamPlayer.h"
#include "../../messageclient/element/CTeamMember.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../messageclient/element/MissionInfo.h"
#include "../../messageclient/element/CMapInfo.h"
#include "../../messageclient/element/CFivePersonInstance.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/ScriptManager.h"
#include "MyHeadInfo.h"
#include "HeadMenu.h"
#include "GuideMap.h"
#include "../MainScene.h"
#include "shortcutelement/ShortcutLayer.h"
#include "../../ui/Friend_ui/TeamMemberList.h"
#include "../role/MyPlayerAIConfig.h"
#include "../role/MyPlayerAI.h"
#include "AppMacros.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/family_ui/familyFight_ui/FamilyFightData.h"
#include "../../ui/generals_ui/RecuriteActionItem.h"
#include "../../utils/GameUtils.h"
#include "../../utils/StrUtils.h"
#include "../../ui/challengeRound/SingCopyCountDownUI.h"
#include "../../ui/Friend_ui/FriendUi.h"
#include "../../legend_script/CCTeachingGuide.h"

#define MISSION_TABALEVIEW_HIGHLIGHT_TAG 21
#define KTag_MoveAction 56
#define KTag_GuidePen 65

#define KTag_FamilyFight_Label_RemainTime 75
#define KTag_OffLineArena_Label_RemainTime 76

#define kTag_offLineArena_tableView_left 90
#define kTag_offLineArena_tableView_right 91

#define LAYER_SIZE_WIDTH 170
#define LAYER_SIZE_HEIGHT 184

#define MISSIONLAYER_LAYER_HASNEXT_TAG 210

int MissionAndTeam::s_panelType = MissionAndTeam::type_FiveInstanceAndTeam;
int MissionAndTeam::s_curPresentPanel = MissionAndTeam::p_mission;

MissionAndTeam::MissionAndTeam():
isOn(true),
m_nTutorialIndicatorIndex(-1),
cur_boss_count(0),
cur_monster_count(0),
max_boss_count(0),
max_monster_count(0),
m_Target_pos_inInstance(ccp(0,0)),
isExistMissionLayer(false),
isExistTeamLayer(false),
isExistFiveInstanceLayer(false),
isExistFamilyFightLayer(false),
isExistSingCopylayer(false),
isExistOffLineArenaLayer(false),
m_singCopy_curLevel(0),
m_singCopy_curMonsterNumber(0),
m_singCopy_monsterNumbers(0),
m_singCopy_remainMonster(0),
m_singCopy_useTime(0),
m_singCopy_timeCount(0),
m_Target_pos_inFamilyFight(ccp(0,0)),
m_nRemainTime_Arena(600),
missionTableView(NULL),
teamTableView(NULL),
fiveInstanceTableView(NULL),
familyFightTableView(NULL),
singCopyTableview(NULL),
m_state_start(false)
{
}


MissionAndTeam::~MissionAndTeam()
{
	delete p_enemyInfoInArena;
	std::vector<GeneralInfoInArena*>::iterator iter_mine;
	for (iter_mine = generalsInArena_mine.begin(); iter_mine != generalsInArena_mine.end(); ++iter_mine)
	{
		delete *iter_mine;
	}
	generalsInArena_mine.clear();

	std::vector<GeneralInfoInArena*>::iterator iter_other;
	for (iter_other = generalsInArena_other.begin(); iter_other != generalsInArena_other.end(); ++iter_other)
	{
		delete *iter_other;
	}
	generalsInArena_other.clear();
}

MissionAndTeam * MissionAndTeam::create()
{
	MissionAndTeam * missionAndTeam = new MissionAndTeam();
	if (missionAndTeam && missionAndTeam->init())
	{
		missionAndTeam->autorelease();
		return missionAndTeam;
	}
	CC_SAFE_DELETE(missionAndTeam);
	return NULL;
}

bool MissionAndTeam::init()
{
	if (UIScene::init())
	{
		CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
		CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

		m_nLastTouchTime = GameUtils::millisecondNow();

		p_enemyInfoInArena = new GeneralInfoInArena();
// 		UIImageView * ImageView_background = UIImageView::create();
// 		ImageView_background->setTouchEnable(true);
// 		ImageView_background->setScale9Enable(true);
// 		ImageView_background->setTexture("gamescene_state/renwuduiwu/di.png");
// 		ImageView_background->setScale9Size(CCSizeMake(183, 150));
// 		ImageView_background->setAnchorPoint(CCPointZero);
// 		ImageView_background->setPosition(ccp(0,0));
// 		m_pUiLayer->addWidget(ImageView_background);

		CCSize tableViewSize = CCSizeMake(LAYER_SIZE_WIDTH,LAYER_SIZE_HEIGHT);

		layer_leader = UILayer::create();
		layer_leader->setContentSize(tableViewSize);
		layer_leader->setTouchEnabled(true);
		layer_leader->setAnchorPoint(CCPointZero);
		layer_leader->setPosition(ccp(0,0));
		addChild(layer_leader);

		//任务面板
		MissionLayer = UILayer::create();
		MissionLayer->setContentSize(tableViewSize);
		MissionLayer->setTouchEnabled(true);
		MissionLayer->setAnchorPoint(CCPointZero);
		MissionLayer->setPosition(ccp(0,0));
		layer_leader->addChild(MissionLayer);
		MissionLayer->setVisible(true);


		this->setPanelType(type_FiveInstanceAndTeam);
		this->setCurPresentPanel(p_mission);
		//默认创建人物面板
		initMissionLayer();//(size:170,184)

		//队伍面板
		TeamLayer = UILayer::create();
		TeamLayer->setContentSize(tableViewSize);
		TeamLayer->setTouchEnabled(true);
		TeamLayer->setAnchorPoint(CCPointZero);
		TeamLayer->setPosition(ccp(0,0));
		layer_leader->addChild(TeamLayer);
		TeamLayer->setVisible(false);
		//initTeamLayer();

		//副本面板
		FiveInstanceLayer = UILayer::create();
		FiveInstanceLayer->setContentSize(tableViewSize);
		FiveInstanceLayer->setTouchEnabled(true);
		FiveInstanceLayer->setAnchorPoint(CCPointZero);
		FiveInstanceLayer->setPosition(ccp(0,0));
		layer_leader->addChild(FiveInstanceLayer);
		FiveInstanceLayer->setVisible(false);
		//initFiveInstanceLayer();

		//家族战面板
		FamilyFightLayer = UILayer::create();
		FamilyFightLayer->setContentSize(tableViewSize);
		FamilyFightLayer->setTouchEnabled(true);
		FamilyFightLayer->setAnchorPoint(CCPointZero);
		FamilyFightLayer->setPosition(ccp(0,0));
		layer_leader->addChild(FamilyFightLayer);
		FamilyFightLayer->setVisible(false);
		//initFamilyFightLayer();

		//singCopy panel
		SingCopyLayer = UILayer::create();
		SingCopyLayer->setContentSize(tableViewSize);
		SingCopyLayer->setTouchEnabled(true);
		SingCopyLayer->setAnchorPoint(CCPointZero);
		SingCopyLayer->setPosition(ccp(0,0));
		layer_leader->addChild(SingCopyLayer);
		SingCopyLayer->setVisible(false);
		//initSingCopyLayer();

		//竞技场面板
		OffLineArenaLayer = UILayer::create();
		OffLineArenaLayer->setContentSize(tableViewSize);
		OffLineArenaLayer->setTouchEnabled(true);
		OffLineArenaLayer->setAnchorPoint(CCPointZero);
		OffLineArenaLayer->setPosition(ccp(0,0));
		layer_leader->addChild(OffLineArenaLayer);
		OffLineArenaLayer->setVisible(false);

		UILayer * uilayer = UILayer::create();
		addChild(uilayer);
		//弹出/收回
		missionAndTeam_on_off = UIButton::create();
		missionAndTeam_on_off->setTouchEnable(true);
		missionAndTeam_on_off->setPressedActionEnabled(true);
		missionAndTeam_on_off->setTextures("gamescene_state/zhujiemian3/renwuduiwu/buttons.png", "gamescene_state/zhujiemian3/renwuduiwu/buttons.png", "");
		missionAndTeam_on_off->setAnchorPoint(ccp(0.5f,0.5f));
		missionAndTeam_on_off->setPosition(ccp(missionAndTeam_on_off->getContentSize().width/2,LAYER_SIZE_HEIGHT+missionAndTeam_on_off->getContentSize().height/2+1));
		missionAndTeam_on_off->addReleaseEvent(this, coco_releaseselector(MissionAndTeam::MissionAndTeamOnOff));
		uilayer->addWidget(missionAndTeam_on_off);
		ImageView_missionAndTeam_on_off = UIImageView::create();
		ImageView_missionAndTeam_on_off->setTexture("gamescene_state/zhujiemian3/renwuduiwu/arrow.png");
		ImageView_missionAndTeam_on_off->setAnchorPoint(ccp(0.5f,0.5f));
		ImageView_missionAndTeam_on_off->setPosition(ccp(0,0));
		missionAndTeam_on_off->addChild(ImageView_missionAndTeam_on_off);
		ImageView_missionAndTeam_on_off->setRotation(180);
		ImageView_missionAndTeam_on_off->setScale(0.8f);

		Button_mission = UIButton::create();
		Button_mission->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
		Button_mission->setTouchEnable(true);
		Button_mission->setAnchorPoint(ccp(0.5f,0.5f));
		Button_mission->setPosition(ccp(missionAndTeam_on_off->getContentSize().width+Button_mission->getContentSize().width/2,LAYER_SIZE_HEIGHT+Button_mission->getContentSize().height/2+1));
		Button_mission->addReleaseEvent(this,coco_releaseselector(MissionAndTeam::ButtonMissionEvent));
		Button_mission->setPressedActionEnabled(true);
		layer_leader->addWidget(Button_mission);
		UIImageView * ImageView_mission = UIImageView::create();
		ImageView_mission->setTexture("gamescene_state/zhujiemian3/renwuduiwu/task.png");
		ImageView_mission->setAnchorPoint(ccp(0.5f,0.5f));
		Button_mission->addChild(ImageView_mission);

		Button_fiveInstance = UIButton::create();
		Button_fiveInstance->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
		Button_fiveInstance->setTouchEnable(true);
		Button_fiveInstance->setAnchorPoint(ccp(0.5f,0.5f));
		Button_fiveInstance->setPosition(ccp(missionAndTeam_on_off->getContentSize().width+Button_fiveInstance->getContentSize().width/2,LAYER_SIZE_HEIGHT+Button_fiveInstance->getContentSize().height/2+1));
		Button_fiveInstance->addReleaseEvent(this,coco_releaseselector(MissionAndTeam::ButtonFiveInstanceEvent));
		Button_fiveInstance->setPressedActionEnabled(true);
		layer_leader->addWidget(Button_fiveInstance);
		UIImageView * ImageView_fiveInstance = UIImageView::create();
		ImageView_fiveInstance->setTexture("gamescene_state/zhujiemian3/renwuduiwu/information.png");
		ImageView_fiveInstance->setAnchorPoint(ccp(0.5f,0.5f));
		Button_fiveInstance->addChild(ImageView_fiveInstance);

		Button_team = UIButton::create();
		Button_team->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
		Button_team->setTouchEnable(true);
		Button_team->setAnchorPoint(ccp(0.5f,0.5f));
		Button_team->setPosition(ccp(Button_mission->getPosition().x+Button_mission->getContentSize().width,LAYER_SIZE_HEIGHT+Button_team->getContentSize().height/2+1));
		Button_team->addReleaseEvent(this,coco_releaseselector(MissionAndTeam::ButtonTeamEvent));
		Button_team->setPressedActionEnabled(true);
		layer_leader->addWidget(Button_team);
		UIImageView * ImageView_team = UIImageView::create();
		ImageView_team->setTexture("gamescene_state/zhujiemian3/renwuduiwu/team.png");
		ImageView_team->setAnchorPoint(ccp(0.5f,0.5f));
		Button_team->addChild(ImageView_team);

		Button_familyFight = UIButton::create();
		Button_familyFight->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
		Button_familyFight->setTouchEnable(true);
		Button_familyFight->setAnchorPoint(ccp(0.5f,0.5f));
		Button_familyFight->setPosition(ccp(missionAndTeam_on_off->getContentSize().width+Button_familyFight->getContentSize().width/2,LAYER_SIZE_HEIGHT+Button_familyFight->getContentSize().height/2+1));
		Button_familyFight->addReleaseEvent(this,coco_releaseselector(MissionAndTeam::ButtonFamilyFightEvent));
		Button_familyFight->setPressedActionEnabled(true);
		layer_leader->addWidget(Button_familyFight);
		UIImageView * ImageView_familyFight = UIImageView::create();
		ImageView_familyFight->setTexture("gamescene_state/zhujiemian3/renwuduiwu/information.png");
		ImageView_familyFight->setAnchorPoint(ccp(0.5f,0.5f));
		Button_familyFight->addChild(ImageView_familyFight);
		//add by liuzhenxing
		Button_singCopy = UIButton::create();
		Button_singCopy->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
		Button_singCopy->setTouchEnable(true);
		Button_singCopy->setAnchorPoint(ccp(0.5f,0.5f));
		Button_singCopy->setPosition(ccp(missionAndTeam_on_off->getContentSize().width+Button_singCopy->getContentSize().width/2,LAYER_SIZE_HEIGHT+Button_singCopy->getContentSize().height/2+1));
		Button_singCopy->addReleaseEvent(this,coco_releaseselector(MissionAndTeam::ButtonSingCopyChallengeEvent));
		Button_singCopy->setPressedActionEnabled(true);
		layer_leader->addWidget(Button_singCopy);
		UIImageView * ImageView_SingCopyCheallenge = UIImageView::create();
		ImageView_SingCopyCheallenge->setTexture("gamescene_state/zhujiemian3/renwuduiwu/information.png");
		ImageView_SingCopyCheallenge->setAnchorPoint(ccp(0.5f,0.5f));
		Button_singCopy->addChild(ImageView_SingCopyCheallenge);

		Button_offLineArena = UIButton::create();
		Button_offLineArena->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
		Button_offLineArena->setTouchEnable(true);
		Button_offLineArena->setAnchorPoint(ccp(0.5f,0.5f));
		Button_offLineArena->setPosition(ccp(missionAndTeam_on_off->getContentSize().width+Button_offLineArena->getContentSize().width/2,LAYER_SIZE_HEIGHT+Button_offLineArena->getContentSize().height/2+1));
		Button_offLineArena->addReleaseEvent(this,coco_releaseselector(MissionAndTeam::ButtonOffLineArenaEvent));
		Button_offLineArena->setPressedActionEnabled(true);
		layer_leader->addWidget(Button_offLineArena);
		UIImageView * ImageView_offLineArena = UIImageView::create();
		ImageView_offLineArena->setTexture("gamescene_state/zhujiemian3/renwuduiwu/information.png");
		ImageView_offLineArena->setAnchorPoint(ccp(0.5f,0.5f));
		Button_offLineArena->addChild(ImageView_offLineArena);

		refreshFunctionBtn();

		this->ignoreAnchorPointForPosition(false);
		this->setAnchorPoint(ccp(0,0));
		this->setPosition(ccp(0,0));
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		contentSizeHeight = tableViewSize.height + Button_team->getContentSize().height +5;
		this->setContentSize(ccp(tableViewSize.width,contentSizeHeight));

		this->schedule(schedule_selector(MissionAndTeam::updateFamilyFightRemainTime),1.0f);
		this->schedule(schedule_selector(MissionAndTeam::updateOffLineArenaRemainTime),1.0f);
		return true;
	}
	return false;
}


void MissionAndTeam::onEnter()
{
	UIScene::onEnter();
}
void MissionAndTeam::onExit()
{
	UIScene::onExit();
}

bool MissionAndTeam::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
	touchBeganPoint = pTouch->getLocation();

	// convert the touch point to OpenGL coordinates
	CCPoint location = pTouch->getLocation();

	CCPoint pos = this->getPosition();
	CCSize size = this->getContentSize();
	if (s_curPresentPanel == p_mission)
	{
		if (GameView::getInstance()->missionManager->MissionList_MainScene.size()>0)
		{
			if (60*GameView::getInstance()->missionManager->MissionList_MainScene.size() > this->getContentSize().height)
			{
			}
			else
			{
				pos = ccp(this->getPosition().x,this->getPosition().y+3+60*(3-GameView::getInstance()->missionManager->MissionList_MainScene.size()));
				size = CCSizeMake(this->getContentSize().width,60*(GameView::getInstance()->missionManager->MissionList_MainScene.size()));
			}
		}
		else
		{
			pos = ccp(this->getPosition().x,60*(3-GameView::getInstance()->missionManager->MissionList_MainScene.size()));
			size = CCSizeMake(0,0);
		}
	}
	else if (s_curPresentPanel == p_team)
	{
		if (GameView::getInstance()->teamMemberVector.size()>0)
		{
			if (40*GameView::getInstance()->teamMemberVector.size() > this->getContentSize().height)
			{
			}
			else
			{
				pos = ccp(this->getPosition().x,this->getPosition().y+40*(4-GameView::getInstance()->teamMemberVector.size()));
				size = CCSizeMake(this->getContentSize().width,40*(GameView::getInstance()->teamMemberVector.size()));
			}
		}
		else
		{
			pos = ccp(this->getPosition().x,40*(4-GameView::getInstance()->teamMemberVector.size()));
			size = CCSizeMake(0,0);
		}
	}
	else
	{

	}
	
	CCPoint anchorPoint = this->getAnchorPointInPoints();
	pos = ccpSub(pos, anchorPoint);
	CCRect rect(pos.x, pos.y, size.width, size.height);
	if(rect.containsPoint(location))
	{
		//add by yangjun 2014.10.13
		MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
		if (mainScene)
		{
			if (mainScene->isHeadMenuOn)
			{
				mainScene->ButtonHeadEvent(NULL);
			}
		}

		return true;
	}

	return false;

	// convert the touch point to OpenGL coordinates
// 	CCPoint location = pTouch->getLocation();
// 
// 	CCPoint pos = this->getPosition();
// 	CCSize size = this->getContentSize();
// 	CCPoint anchorPoint = this->getAnchorPointInPoints();
// 	pos = ccpSub(pos, anchorPoint);
// 	CCRect rect(pos.x, pos.y, size.width, size.height);
// 	if(rect.containsPoint(location))
// 	{
// 		return true;
// 	}
// 
// 	return false;
}
void MissionAndTeam::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
{
}
void MissionAndTeam::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
{
	CCPoint touchEndedPoint = pTouch->getLocation();
	
	if (isOn)
	{
		int offsetX = touchEndedPoint.x-touchBeganPoint.x;
		int offsetY = touchEndedPoint.y-touchBeganPoint.y;
		if ( ( abs(offsetY) < 30 )  && ( offsetX < -60 ) )
		{
			//开启等级
			int openlevel = 0;
			std::map<int,int>::const_iterator cIter;
			cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(21);
			if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // 没找到就是指向END了  
			{
			}
			else
			{
				openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[21];
			}

			if (openlevel > GameView::getInstance()->myplayer->getActiveRole()->level())
			{
				char s_des [200];
				const char* str_des = StringDataManager::getString("shrinkage_will_be_open");
				sprintf(s_des,str_des,openlevel);
				GameView::getInstance()->showAlertDialog(s_des);
				return;
			}

			CCAction * action1 = CCMoveTo::create(0.15f,ccp(layer_leader->getPositionX()-LAYER_SIZE_WIDTH,layer_leader->getPositionY()));
			action1->setTag(KTag_MoveAction);
			layer_leader->runAction(action1);

			isOn = false;
			ImageView_missionAndTeam_on_off->setRotation(0);
			this->setContentSize(CCSizeMake(32,contentSizeHeight));

			refreshTutorial();
		}
	}
	else
	{
		if ((touchEndedPoint.x-touchBeganPoint.x) > 60 )
		{
			//开启等级
			int openlevel = 0;
			std::map<int,int>::const_iterator cIter;
			cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(21);
			if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // 没找到就是指向END了  
			{
			}
			else
			{
				openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[21];
			}

			if (openlevel > GameView::getInstance()->myplayer->getActiveRole()->level())
			{
				char s_des [200];
				const char* str_des = StringDataManager::getString("shrinkage_will_be_open");
				sprintf(s_des,str_des,openlevel);
				GameView::getInstance()->showAlertDialog(s_des);
				return;
			}

			//open
			CCAction * action1 = CCMoveTo::create(0.15f,ccp(layer_leader->getPositionX()+LAYER_SIZE_WIDTH,layer_leader->getPositionY()));
			action1->setTag(KTag_MoveAction);
			layer_leader->runAction(action1);

			isOn = true;
			ImageView_missionAndTeam_on_off->setRotation(180);
			this->setContentSize(CCSizeMake(LAYER_SIZE_WIDTH,contentSizeHeight));

			refreshTutorial();
		}
	}
}
void MissionAndTeam::ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent)
{
}

void MissionAndTeam::update( float dt )
{
}

void MissionAndTeam::ButtonMissionEvent( CCObject *pSender )
{
	//add by yangjun 2014.10.13
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		if (mainScene->isHeadMenuOn)
		{
			mainScene->ButtonHeadEvent(NULL);
		}
	}

	setCurPresentPanel(p_mission);
	initMissionLayer();

	Button_mission->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
	MissionLayer->setVisible(true);
	missionTableView->reloadData();
	this->refreshDownArrowStatus();

	Button_team->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
	TeamLayer->setVisible(false);

	Button_fiveInstance->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
	FiveInstanceLayer->setVisible(false);

	Button_familyFight->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
	FamilyFightLayer->setVisible(false);

	Button_singCopy->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
	SingCopyLayer->setVisible(false);

	Button_offLineArena->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
	OffLineArenaLayer->setVisible(false);

	if(!isOn)
	{
		popUpMission(pSender);
	}

	refreshTutorial();
}

void MissionAndTeam::ButtonTeamEvent( CCObject *pSender )
{
	int selfLevel_ = GameView::getInstance()->myplayer->getActiveRole()->level();
	int openlevel = 0;
	std::map<int,int>::const_iterator cIter;
	cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(27);
	if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end())
	{
	}
	else
	{
		openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[27];
	}

	if (selfLevel_ < openlevel)
	{
		const char * str_ = StringDataManager::getString("missAndTeam_buttonTeamisOpen");

		char str_open[50];
		sprintf(str_open,str_,openlevel);

		GameView::getInstance()->showAlertDialog(str_open);
		return;
	}

	//add by yangjun 2014.10.13
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		if (mainScene->isHeadMenuOn)
		{
			mainScene->ButtonHeadEvent(NULL);
		}
	}

	setCurPresentPanel(p_team);
	initTeamLayer();

	Button_mission->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
	MissionLayer->setVisible(false);

	Button_fiveInstance->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
	FiveInstanceLayer->setVisible(false);

	Button_team->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
	TeamLayer->setVisible(true);
	teamTableView->reloadData();

	Button_familyFight->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
	FamilyFightLayer->setVisible(false);

	Button_singCopy->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
	SingCopyLayer->setVisible(false);

	Button_offLineArena->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
	OffLineArenaLayer->setVisible(false);

	if(!isOn)
	{
		popUpMission(pSender);
	}

	refreshTutorial();
}

void MissionAndTeam::ButtonFiveInstanceEvent( CCObject *pSender )
{
	//add by yangjun 2014.10.13
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		if (mainScene->isHeadMenuOn)
		{
			mainScene->ButtonHeadEvent(NULL);
		}
	}
	
	setCurPresentPanel(p_fiveInstance);
	initFiveInstanceLayer();
	
	Button_mission->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
	MissionLayer->setVisible(false);

	Button_fiveInstance->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
	FiveInstanceLayer->setVisible(true);
	fiveInstanceTableView->reloadData();

	Button_team->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
	TeamLayer->setVisible(false);

	Button_familyFight->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
	FamilyFightLayer->setVisible(false);

	Button_singCopy->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
	SingCopyLayer->setVisible(false);

	Button_offLineArena->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
	OffLineArenaLayer->setVisible(false);

	if(!isOn)
	{
		popUpMission(pSender);
	}

	refreshTutorial();
}

void MissionAndTeam::ButtonFamilyFightEvent( CCObject *pSender )
{
	//add by yangjun 2014.10.13
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		if (mainScene->isHeadMenuOn)
		{
			mainScene->ButtonHeadEvent(NULL);
		}
	}

	setCurPresentPanel(p_familyFight);
	initFamilyFightLayer();
	
	Button_mission->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
	MissionLayer->setVisible(false);

	Button_fiveInstance->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
	FiveInstanceLayer->setVisible(false);;

	Button_team->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
	TeamLayer->setVisible(false);

	Button_familyFight->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
	FamilyFightLayer->setVisible(true);
	familyFightTableView->reloadData();

	Button_singCopy->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
	SingCopyLayer->setVisible(false);

	Button_offLineArena->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
	OffLineArenaLayer->setVisible(false);

	if(!isOn)
	{
		popUpMission(pSender);
	}

	refreshTutorial();
}

void MissionAndTeam::ButtonSingCopyChallengeEvent( CCObject *pSender )
{
	//add by yangjun 2014.10.13
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		if (mainScene->isHeadMenuOn)
		{
			mainScene->ButtonHeadEvent(NULL);
		}
	}

	setCurPresentPanel(p_singCopyChallenge);
	initSingCopyLayer();
	
	Button_mission->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
	MissionLayer->setVisible(false);

	Button_fiveInstance->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
	FiveInstanceLayer->setVisible(false);

	Button_team->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
	TeamLayer->setVisible(false);

	Button_familyFight->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
	FamilyFightLayer->setVisible(false);

	Button_singCopy->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
	SingCopyLayer->setVisible(true);
	//singCopyTableview->reloadData();

	Button_offLineArena->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
	OffLineArenaLayer->setVisible(false);

	if(!isOn)
	{
		popUpMission(pSender);
	}

	refreshTutorial();
}


void MissionAndTeam::ButtonOffLineArenaEvent( CCObject *pSender )
{
	//add by yangjun 2014.10.13
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		if (mainScene->isHeadMenuOn)
		{
			mainScene->ButtonHeadEvent(NULL);
		}
	}

	setCurPresentPanel(p_offLineArena);
	initOffLineArenaLayer();
	
	Button_mission->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
	MissionLayer->setVisible(false);

	Button_fiveInstance->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
	FiveInstanceLayer->setVisible(false);

	Button_team->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
	TeamLayer->setVisible(false);

	Button_familyFight->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
	FamilyFightLayer->setVisible(false);

	Button_singCopy->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
	SingCopyLayer->setVisible(false);

	Button_offLineArena->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
	OffLineArenaLayer->setVisible(true);
	CCTableView * offLineArenaTableView_left = (CCTableView*)OffLineArenaLayer->getChildByTag(kTag_offLineArena_tableView_left);
	if (offLineArenaTableView_left)
		offLineArenaTableView_left->reloadData();
	CCTableView * offLineArenaTableView_right = (CCTableView*)OffLineArenaLayer->getChildByTag(kTag_offLineArena_tableView_right);
	if (offLineArenaTableView_right)
		offLineArenaTableView_right->reloadData();

	if(!isOn)
	{
		popUpMission(pSender);
	}

	refreshTutorial();
}


void MissionAndTeam::initMissionLayer()
{
	if (isExistMissionLayer)
		return;
// 	CCScale9Sprite * sprite_frame = CCScale9Sprite::create("gamescene_state/zhujiemian3/renwuduiwu/renwu_di.png");
// 	sprite_frame->setAnchorPoint(ccp(0,0));
// 	sprite_frame->setPosition(ccp(0,0));
// 	sprite_frame->setPreferredSize(CCSizeMake(160,160));
// 	sprite_frame->setCapInsets(CCRectMake(12,12,1,1));
// 	MissionLayer->addChild(sprite_frame);

	CCPoint _s = MissionManager::getInstance()->getMissionAndTeamOffSet();

	missionTableView = CCTableView::create(this,CCSizeMake(LAYER_SIZE_WIDTH,LAYER_SIZE_HEIGHT));
	missionTableView->setDirection(kCCScrollViewDirectionVertical);
	missionTableView->setAnchorPoint(ccp(0,0));
	missionTableView->setPosition(ccp(0,0));
	missionTableView->setDelegate(this);
	missionTableView->setVerticalFillOrder(kCCTableViewFillTopDown);
	missionTableView->setPressedActionEnabled(true, 0.8f, 1.2f);
	MissionLayer->addChild(missionTableView);

	if (!MissionManager::getInstance()->getIsFirstCreateMissionAndTeam())
	{
		int _h = missionTableView->getContentSize().height + _s.y;
		missionTableView->reloadData();
		CCPoint temp = ccp(_s.x,_h - missionTableView->getContentSize().height);
		missionTableView->setContentOffset(temp); 

		this->refreshDownArrowStatus();
	}

	UILayer * layer_hasNextMission= UILayer::create();
	MissionLayer->addChild(layer_hasNextMission);
	layer_hasNextMission->setTag(MISSIONLAYER_LAYER_HASNEXT_TAG);
	UIImageView * image_hasNextMission = UIImageView::create();
	image_hasNextMission->setTexture("gamescene_state/zhujiemian3/renwuduiwu/arrow2.png");
	image_hasNextMission->setAnchorPoint(ccp(0.5f,0.5f));
	image_hasNextMission->setPosition(ccp(LAYER_SIZE_WIDTH/2,0));
	image_hasNextMission->setName("image_hasNextMission");
	layer_hasNextMission->addWidget(image_hasNextMission);
	this->refreshDownArrowStatus();

	MissionManager::getInstance()->setIsFirstCreateMissionAndTeam(false);

	isExistMissionLayer = true;
}

void MissionAndTeam::initTeamLayer()
{
	if (isExistTeamLayer)
		return;

	UIImageView * image_frame = UIImageView::create();
	image_frame->setTexture("res_ui/zhezhao70.png");
	image_frame->setAnchorPoint(ccp(0,0));
	image_frame->setPosition(ccp(0,0));
	image_frame->setScale9Size(CCSizeMake(LAYER_SIZE_WIDTH,LAYER_SIZE_HEIGHT));
	image_frame->setCapInsets(CCRectMake(7,7,1,1));
	image_frame->setOpacity(160);
	image_frame->setZOrder(0);
	TeamLayer->addWidget(image_frame);

	teamInvite_btn = UIButton::create();
	teamInvite_btn->setScale9Enable(true);
	teamInvite_btn->setScale9Size(CCSizeMake(160,30));
	teamInvite_btn->setCapInsets(CCRectMake(12,12,1,1));
	teamInvite_btn->setTextures("res_ui/none.png","res_ui/none.png","res_ui/none.png");
	teamInvite_btn->setTouchEnable(true);
	teamInvite_btn->setPressedActionEnabled(true);
	teamInvite_btn->setAnchorPoint(ccp(0.5f,0.5f));
	teamInvite_btn->setPosition(ccp(image_frame->getSize().width/2,image_frame->getSize().height - teamInvite_btn->getSize().height/2 - 7));
	teamInvite_btn->addReleaseEvent(this,coco_releaseselector(MissionAndTeam::callBackTeamInvite));
	teamInvite_btn->setZOrder(1);
	teamInvite_btn->setName("teamInvite_btn_name");
	TeamLayer->addWidget(teamInvite_btn);

	UILabelBMFont * teamInvite_label = UILabelBMFont::create();
	teamInvite_label->setAnchorPoint(ccp(0.5f,0.5f));
	teamInvite_label->setPosition(ccp(0,0));
	teamInvite_label->setText(StringDataManager::getString("mainScene_missionAndTeam_invite"));
	teamInvite_label->setFntFile("res_ui/font/ziti_3.fnt");
	teamInvite_label->setZOrder(5);
	teamInvite_btn->addChild(teamInvite_label);

	teamTableView = CCTableView::create(this,CCSizeMake(LAYER_SIZE_WIDTH,LAYER_SIZE_HEIGHT));
	teamTableView->setDirection(kCCScrollViewDirectionVertical);
	teamTableView->setAnchorPoint(ccp(0,0));
	teamTableView->setPosition(ccp(0,4));
	teamTableView->setDelegate(this);
	teamTableView->setVerticalFillOrder(kCCTableViewFillTopDown);
	teamTableView->setPressedActionEnabled(true, 0.8f, 1.2f);
	TeamLayer->addChild(teamTableView);
	isExistTeamLayer = true;
	refreshTeamPlayer();
 }

void MissionAndTeam::initFiveInstanceLayer()
{
	if (isExistFiveInstanceLayer)
		return;

	CCScale9Sprite * sprite_frame = CCScale9Sprite::create("res_ui/zhezhao70.png");
	sprite_frame->setAnchorPoint(ccp(0,0));
	sprite_frame->setPosition(ccp(0,0));
	sprite_frame->setPreferredSize(CCSizeMake(LAYER_SIZE_WIDTH,LAYER_SIZE_HEIGHT));
	sprite_frame->setCapInsets(CCRectMake(7,7,1,1));
	sprite_frame->setOpacity(160);
	FiveInstanceLayer->addChild(sprite_frame);

	fiveInstanceTableView = CCTableView::create(this,CCSizeMake(LAYER_SIZE_WIDTH,LAYER_SIZE_HEIGHT));
	fiveInstanceTableView->setDirection(kCCScrollViewDirectionVertical);
	fiveInstanceTableView->setAnchorPoint(ccp(0,0));
	fiveInstanceTableView->setPosition(ccp(0,2));
	fiveInstanceTableView->setDelegate(this);
	fiveInstanceTableView->setVerticalFillOrder(kCCTableViewFillTopDown);
	fiveInstanceTableView->setPressedActionEnabled(true, 0.8f, 1.2f);
	fiveInstanceTableView->setBounceable(false);
	FiveInstanceLayer->addChild(fiveInstanceTableView);

	CCLabelTTF * l_des = CCLabelTTF::create(StringDataManager::getString("mainScene_missionAndTeam_instanceDes"),APP_FONT_NAME,16, CCSizeMake(155, 0), kCCTextAlignmentLeft);
	l_des->setAnchorPoint(ccp(0,0));
	l_des->setPosition(ccp(6,10));
	l_des->setColor(ccc3(0,255,0));
	FiveInstanceLayer->addChild(l_des);

	isExistFiveInstanceLayer = true;
}

void MissionAndTeam::initFamilyFightLayer()
{
	if(isExistFamilyFightLayer)
		return;

	CCScale9Sprite * sprite_frame = CCScale9Sprite::create("res_ui/zhezhao70.png");
	sprite_frame->setAnchorPoint(ccp(0,0));
	sprite_frame->setPosition(ccp(0,0));
	sprite_frame->setPreferredSize(CCSizeMake(LAYER_SIZE_WIDTH,24));
	sprite_frame->setCapInsets(CCRectMake(7,7,15,15));
	sprite_frame->setOpacity(160);
	FamilyFightLayer->addChild(sprite_frame);
	//剩余时间：
	CCLabelTTF * l_familyFight_remainTime = CCLabelTTF::create(StringDataManager::getString("Arena_remainTime"),APP_FONT_NAME,16);
	l_familyFight_remainTime->setAnchorPoint(ccp(0,.5f));
	l_familyFight_remainTime->setPosition(ccp(3,sprite_frame->getContentSize().height/2));
	sprite_frame->addChild(l_familyFight_remainTime);
	//00:12:34
	CCLabelTTF * l_familyFight_remainTimeValue = CCLabelTTF::create("00:15:00",APP_FONT_NAME,16);
	l_familyFight_remainTimeValue->setAnchorPoint(ccp(0,.5f));
	l_familyFight_remainTimeValue->setPosition(ccp(l_familyFight_remainTime->getPositionX()+l_familyFight_remainTime->getContentSize().width,sprite_frame->getContentSize().height/2));
	l_familyFight_remainTimeValue->setTag(KTag_FamilyFight_Label_RemainTime);
	FamilyFightLayer->addChild(l_familyFight_remainTimeValue);

	familyFightTableView = CCTableView::create(this,CCSizeMake(LAYER_SIZE_WIDTH,LAYER_SIZE_HEIGHT-25));
	familyFightTableView->setDirection(kCCScrollViewDirectionVertical);
	familyFightTableView->setAnchorPoint(ccp(0,0));
	familyFightTableView->setPosition(ccp(0,25));
	familyFightTableView->setDelegate(this);
	familyFightTableView->setVerticalFillOrder(kCCTableViewFillTopDown);
	familyFightTableView->setPressedActionEnabled(true, 0.8f, 1.2f);
	//familyFightTableView->setBounceable(false);
	FamilyFightLayer->addChild(familyFightTableView);

	isExistFamilyFightLayer = true;
}

void  MissionAndTeam::initSingCopyLayer()
{
	if (isExistSingCopylayer)
		return;

	UIImageView * image_fram = UIImageView::create();
	image_fram->setTexture("res_ui/zhezhao70.png");
	image_fram->setScale9Enable(true);
	image_fram->setScale9Size(CCSizeMake(LAYER_SIZE_WIDTH,LAYER_SIZE_HEIGHT));
	image_fram->setAnchorPoint(ccp(0,0));
	image_fram->setCapInsets(CCRectMake(7,7,1,1));
	image_fram->setOpacity(160);
	SingCopyLayer->addWidget(image_fram);

	UIScrollView * scrollView_info = UIScrollView::create();
	scrollView_info->setTouchEnable(true);
	scrollView_info->setDirection(SCROLLVIEW_DIR_VERTICAL);
	scrollView_info->setSize(CCSizeMake(LAYER_SIZE_WIDTH,LAYER_SIZE_HEIGHT));
	scrollView_info->setPosition(ccp(0,0));
	scrollView_info->setBounceEnabled(true);
	scrollView_info->setTag(10);
	SingCopyLayer->addWidget(scrollView_info);
	
	UIButton * btn_sp = UIButton::create();
	btn_sp->setTouchEnable(true);
	btn_sp->setPressedActionEnabled(true);
	btn_sp->setTextures("res_ui/suibian.png","gamescene_state/zhujiemian3/renwuduiwu/light_1.png","");//
	btn_sp->setScale9Enable(true);
	btn_sp->setScale9Size(CCSizeMake(LAYER_SIZE_WIDTH - 1,LAYER_SIZE_HEIGHT - 1));
	btn_sp->setAnchorPoint(ccp(0.5f,0.5f));
	btn_sp->setPosition(ccp(LAYER_SIZE_WIDTH/2,LAYER_SIZE_HEIGHT/2));
	btn_sp->addReleaseEvent(this,coco_releaseselector(MissionAndTeam::callBackSingCopy));
	scrollView_info->addChild(btn_sp);

	const char *strings_curLevelBmfont = StringDataManager::getString("singleCopy_curLevelLabelBmFont");
	char levelStr[50];
	sprintf(levelStr,strings_curLevelBmfont,m_singCopy_curLevel);
	curLevel_BmFont = UILabelBMFont::create();
	curLevel_BmFont->setAnchorPoint(ccp(0.5f,1.0));
	curLevel_BmFont->setFntFile("res_ui/font/ziti_3.fnt");
	curLevel_BmFont->setText(levelStr);
	curLevel_BmFont->setScale(1.1f);
	curLevel_BmFont->setPosition(ccp(0,btn_sp->getSize().height/2 - curLevel_BmFont->getContentSize().height/2));
	btn_sp->addChild(curLevel_BmFont);
	//test 当前进度： 1/5
	std::string curLevelShowString = StringDataManager::getString("singCopy_curLevelShowLabel");
	UILabel * l_curLevelShowLabel = UILabel::create();
	l_curLevelShowLabel->setAnchorPoint(ccp(0,1.0f));
	l_curLevelShowLabel->setText(curLevelShowString.c_str());
	l_curLevelShowLabel->setFontSize(16);
	l_curLevelShowLabel->setPosition(ccp(5 - btn_sp->getSize().width/2,curLevel_BmFont->getPosition().y - curLevel_BmFont->getContentSize().height - 2));
	l_curLevelShowLabel->setColor(ccc3(243,252,2));
	btn_sp->addChild(l_curLevelShowLabel);

	m_curLevelShowValue = UILabelAtlas::create();
	m_curLevelShowValue->setProperty("0/0", "res_ui/font/yellow_num.png", 10, 21, "/");
	m_curLevelShowValue->setAnchorPoint(ccp(0,1.0f));
	m_curLevelShowValue->setPosition(ccp(5+l_curLevelShowLabel->getPosition().x+l_curLevelShowLabel->getContentSize().width,l_curLevelShowLabel->getPosition().y+2));
	btn_sp->addChild(m_curLevelShowValue);
	//monster 本波怪物剩余：50
	std::string curMonsterShowString = StringDataManager::getString("singCopy_curMonsterNum");
	UILabel * label_monster = UILabel::create();
	label_monster->setAnchorPoint(ccp(0.f,1.0f));
	label_monster->setText(curMonsterShowString.c_str());
	label_monster->setPosition(ccp(5 - btn_sp->getSize().width/2,l_curLevelShowLabel->getPosition().y-l_curLevelShowLabel->getContentSize().height-2));
	label_monster->setColor(ccc3(243,252,2));
	label_monster->setFontSize(16);
	btn_sp->addChild(label_monster);

	label_monsterValue = UILabelAtlas::create();
	label_monsterValue->setProperty("0", "res_ui/font/yellow_num.png", 10, 21, "/");
	label_monsterValue->setAnchorPoint(ccp(0.f,1.0f));
	label_monsterValue->setPosition(ccp(5+label_monster->getPosition().x+ label_monster->getContentSize().width,label_monster->getPosition().y+2));
	btn_sp->addChild(label_monsterValue);
	//use time 已用时：100
	std::string useTimeShowString = StringDataManager::getString("singCopy_useTime_");
	UILabel * l_useTime = UILabel::create();
	l_useTime->setAnchorPoint(ccp(0.f,1.0f));
	l_useTime->setText(useTimeShowString.c_str());
	l_useTime->setPosition(ccp(5- btn_sp->getSize().width/2,label_monster->getPosition().y - label_monster->getContentSize().height - 2));
	l_useTime->setColor(ccc3(243,252,2)); 
	l_useTime->setFontSize(16);
	btn_sp->addChild(l_useTime);
	
	m_useTimeValue = UILabel::create();
	m_useTimeValue->setAnchorPoint(ccp(0.f,1.0f));
	m_useTimeValue->setText("0");
	m_useTimeValue->setFontSize(16);
	m_useTimeValue->setPosition(ccp(5+l_useTime->getPosition().x+l_useTime->getContentSize().width,l_useTime->getPosition().y));
	btn_sp->addChild(m_useTimeValue);
	
	//all time 总时长：500
	std::string allTimeShowString_xian = StringDataManager::getString("singCopy_allTime_xian");
	UILabel * l_allTime_xian = UILabel::create();
	l_allTime_xian->setAnchorPoint(ccp(0.f,1.0f));
	l_allTime_xian->setText(allTimeShowString_xian.c_str());
	l_allTime_xian->setColor(ccc3(243,252,2)); 
	l_allTime_xian->setPosition(ccp(5- btn_sp->getSize().width/2,l_useTime->getPosition().y-l_useTime->getContentSize().height-2));
	l_allTime_xian->setFontSize(16);
	btn_sp->addChild(l_allTime_xian);

	std::string allTimeShowString_shi = StringDataManager::getString("singCopy_allTime_shi");
	UILabel * l_allTime_shi = UILabel::create();
	l_allTime_shi->setAnchorPoint(ccp(1.0f,1.0f));
	l_allTime_shi->setText(allTimeShowString_shi.c_str());
	l_allTime_shi->setColor(ccc3(243,252,2));
	l_allTime_shi->setPosition(ccp(l_useTime->getPosition().x+l_useTime->getContentSize().width,l_allTime_xian->getPosition().y));
	l_allTime_shi->setFontSize(16);
	btn_sp->addChild(l_allTime_shi);

	allTimeShowValue = UILabel::create();
	allTimeShowValue->setAnchorPoint(ccp(0.f,1.0f));
	allTimeShowValue->setText("0");
	allTimeShowValue->setFontSize(16);
	allTimeShowValue->setPosition(ccp(5+l_useTime->getPosition().x+l_useTime->getContentSize().width,l_allTime_xian->getPosition().y));
	btn_sp->addChild(allTimeShowValue);

	int tmep_scrollHigh = curLevel_BmFont->getContentSize().height + l_curLevelShowLabel->getContentSize().height*4+12;
	scrollView_info->setInnerContainerSize(CCSizeMake(LAYER_SIZE_WIDTH,tmep_scrollHigh));
	/*
	curLevel_BmFont->setPosition(ccp(85,scrollView_info->getContentSize().height - curLevel_BmFont->getContentSize().height/2));
	l_curLevelShowLabel->setPosition(ccp(5,curLevel_BmFont->getPosition().y - curLevel_BmFont->getContentSize().height - 2));
	m_curLevelShowValue->setPosition(ccp(5+l_curLevelShowLabel->getPosition().x+l_curLevelShowLabel->getContentSize().width,l_curLevelShowLabel->getPosition().y+2));

	label_monster->setPosition(ccp(5,l_curLevelShowLabel->getPosition().y-l_curLevelShowLabel->getContentSize().height-2));
	label_monsterValue->setPosition(ccp(5+label_monster->getPosition().x+ label_monster->getContentSize().width,label_monster->getPosition().y+2));

	l_useTime->setPosition(ccp(5,label_monster->getPosition().y - label_monster->getContentSize().height - 2));
	m_useTimeValue->setPosition(ccp(5+l_useTime->getPosition().x+l_useTime->getContentSize().width,l_useTime->getPosition().y));

	l_allTime_xian->setPosition(ccp(5,l_useTime->getPosition().y-l_useTime->getContentSize().height-2));
	l_allTime_shi->setPosition(ccp(l_useTime->getPosition().x+l_useTime->getContentSize().width,l_allTime_xian->getPosition().y));

	allTimeShowValue->setPosition(ccp(5+l_useTime->getPosition().x+l_useTime->getContentSize().width,l_allTime_xian->getPosition().y));
	*/
	/*
	CCScale9Sprite * sprite_frame = CCScale9Sprite::create("gamescene_state/zhujiemian3/renwuduiwu/renwu_di.png");
	sprite_frame->setAnchorPoint(ccp(0,0));
	sprite_frame->setPosition(ccp(0,0));
	sprite_frame->setPreferredSize(CCSizeMake(135,160));
	sprite_frame->setCapInsets(CCRectMake(12,12,1,1));
	SingCopyLayer->addChild(sprite_frame);

	singCopyTableview = CCTableView::create(this,CCSizeMake(135,156));
	singCopyTableview->setDirection(kCCScrollViewDirectionVertical);
	singCopyTableview->setAnchorPoint(ccp(0,0));
	singCopyTableview->setPosition(ccp(0,2));
	singCopyTableview->setDelegate(this);
	singCopyTableview->setVerticalFillOrder(kCCTableViewFillTopDown);
	singCopyTableview->setPressedActionEnabled(true, 0.8f, 1.2f);
	SingCopyLayer->addChild(singCopyTableview);
	*/
	isExistSingCopylayer = true;
}


void MissionAndTeam::initOffLineArenaLayer()
{
	if (isExistOffLineArenaLayer)
		return;

	CCScale9Sprite * sprite_frame = CCScale9Sprite::create("res_ui/zhezhao70.png");
	sprite_frame->setAnchorPoint(ccp(0,0));
	sprite_frame->setPosition(ccp(0,0));
	sprite_frame->setPreferredSize(CCSizeMake(LAYER_SIZE_WIDTH,24));
	sprite_frame->setCapInsets(CCRectMake(7,7,15,15));
	sprite_frame->setOpacity(160);
	OffLineArenaLayer->addChild(sprite_frame);
	//剩余时间：
	CCLabelTTF * l_OffLineArena_remainTime = CCLabelTTF::create(StringDataManager::getString("Arena_remainTime"),APP_FONT_NAME,16);
	l_OffLineArena_remainTime->setAnchorPoint(ccp(0,.5f));
	l_OffLineArena_remainTime->setPosition(ccp(3,sprite_frame->getContentSize().height/2));
	sprite_frame->addChild(l_OffLineArena_remainTime);
	//00:12:34
	CCLabelTTF * l_OffLineArena_remainTimeValue = CCLabelTTF::create("00:10:00",APP_FONT_NAME,16);
	l_OffLineArena_remainTimeValue->setAnchorPoint(ccp(0,.5f));
	l_OffLineArena_remainTimeValue->setPosition(ccp(l_OffLineArena_remainTime->getPositionX()+l_OffLineArena_remainTime->getContentSize().width,sprite_frame->getContentSize().height/2));
	l_OffLineArena_remainTimeValue->setTag(KTag_OffLineArena_Label_RemainTime);
	OffLineArenaLayer->addChild(l_OffLineArena_remainTimeValue);

	CCScale9Sprite * sprite_infoFrame_left = CCScale9Sprite::create("res_ui/zhezhao70.png");
	sprite_infoFrame_left->setAnchorPoint(ccp(0,0));
	sprite_infoFrame_left->setPosition(ccp(0,25));
	sprite_infoFrame_left->setPreferredSize(CCSizeMake(LAYER_SIZE_WIDTH/2-1,LAYER_SIZE_HEIGHT-25));
	sprite_infoFrame_left->setCapInsets(CCRectMake(7,7,1,1));
	OffLineArenaLayer->addChild(sprite_infoFrame_left);
	CCScale9Sprite * sprite_infoFrame_right = CCScale9Sprite::create("res_ui/zhezhao70.png");
	sprite_infoFrame_right->setAnchorPoint(ccp(0,0));
	sprite_infoFrame_right->setPosition(ccp(LAYER_SIZE_WIDTH/2,25));
	sprite_infoFrame_right->setPreferredSize(CCSizeMake(LAYER_SIZE_WIDTH/2-1,LAYER_SIZE_HEIGHT-25));
	sprite_infoFrame_right->setCapInsets(CCRectMake(7,7,1,1));
	OffLineArenaLayer->addChild(sprite_infoFrame_right);

	CCTableView *offLineArenaTableView_left = CCTableView::create(this,CCSizeMake(LAYER_SIZE_WIDTH/2-1,LAYER_SIZE_HEIGHT-25));
	offLineArenaTableView_left->setDirection(kCCScrollViewDirectionVertical);
	offLineArenaTableView_left->setAnchorPoint(ccp(0,0));
	offLineArenaTableView_left->setPosition(ccp(0,25));
	offLineArenaTableView_left->setDelegate(this);
	offLineArenaTableView_left->setVerticalFillOrder(kCCTableViewFillTopDown);
	offLineArenaTableView_left->setPressedActionEnabled(true, 0.8f, 1.2f);
	offLineArenaTableView_left->setBounceable(false);
	offLineArenaTableView_left->setTag(kTag_offLineArena_tableView_left);
	OffLineArenaLayer->addChild(offLineArenaTableView_left);

	CCTableView * offLineArenaTableView_right = CCTableView::create(this,CCSizeMake(LAYER_SIZE_WIDTH/2-1,LAYER_SIZE_HEIGHT-25));
	offLineArenaTableView_right->setDirection(kCCScrollViewDirectionVertical);
	offLineArenaTableView_right->setAnchorPoint(ccp(0,0));
	offLineArenaTableView_right->setPosition(ccp(LAYER_SIZE_WIDTH/2,25));
	offLineArenaTableView_right->setDelegate(this);
	offLineArenaTableView_right->setVerticalFillOrder(kCCTableViewFillTopDown);
	offLineArenaTableView_right->setPressedActionEnabled(true, 0.8f, 1.2f);
	offLineArenaTableView_right->setBounceable(false);
	offLineArenaTableView_right->setTag(kTag_offLineArena_tableView_right);
	OffLineArenaLayer->addChild(offLineArenaTableView_right);

	isExistOffLineArenaLayer = true;
}


CCSize MissionAndTeam::tableCellSizeForIndex(CCTableView *table, unsigned int idx)
{
	if (getCurPresentPanel() == p_mission)
	{
		return CCSizeMake(170, 61);
	}
	else if (getCurPresentPanel() == p_team)
	{
		return CCSizeMake(170, 45);
	}
	else if (getCurPresentPanel() == p_fiveInstance)
	{
		return CCSizeMake(170, 100);
	}
	else if (getCurPresentPanel() == p_familyFight)
	{
		return CCSizeMake(170, 53);
	}
	else if (getCurPresentPanel() == p_singCopyChallenge)
	{
		return CCSizeMake(130, 93);
	}
	else if (getCurPresentPanel() == p_offLineArena)
	{
		return CCSizeMake(84,23);
	}
	else
	{
		return CCSizeMake(170, 60);
	}
}

CCTableViewCell* MissionAndTeam::tableCellAtIndex(CCTableView *table, unsigned int idx)
{
	CCString *string = CCString::createWithFormat("%d", idx);
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called
	
	if (getCurPresentPanel() == p_mission)
	{
		//cell=MissionCell::Create(GameView::getInstance()->missionManager->MissionList_MainScene.at(idx));
		if (GameView::getInstance()->missionManager->MissionList_MainScene.size()>0)
		{
			CCTableViewCell *tempCell = table->dequeueCell();
			if (!tempCell)
			{
				tempCell = MissionCell::Create(GameView::getInstance()->missionManager->MissionList_MainScene.at(idx)); 
			}
			else
			{
				MissionCell * missionCell = dynamic_cast<MissionCell*>(tempCell);
				if (missionCell)
				{
					if (missionCell->getChildByTag(CCTUTORIALPARTICLETAG) != NULL)
					{
						missionCell->getChildByTag(CCTUTORIALPARTICLETAG)->removeFromParent();
					}
					missionCell->RefreshCell(GameView::getInstance()->missionManager->MissionList_MainScene.at(idx));
				}
			}

			//add by yangjun 2014.7.2
			if (idx == 0 && GameView::getInstance()->myplayer->getActiveRole()->level() <= K_GuideForMainMission_Level)
			{
				if ((GameView::getInstance()->missionManager->MissionList_MainScene.at(idx)->isNeedTeach && GameView::getInstance()->missionManager->MissionList_MainScene.at(idx)->isStateChanged) || (GameView::getInstance()->missionManager->isExistGuideForMmission))
				{
					addGuidePen();
				}
			}

			if (idx == 0 && GameView::getInstance()->myplayer->getActiveRole()->level() <= 20)
			{
				if (this->getChildByTag(CCTUTORIALINDICATORTAG) == NULL)
				{
					if (tempCell->getChildByTag(CCTUTORIALPARTICLETAG) == NULL)
					{
						if (this->getChildByTag(KTag_GuidePen) == NULL)
						{
							if (GameView::getInstance()->missionManager->MissionList_MainScene.at(idx)->isNeedTeach && GameView::getInstance()->missionManager->MissionList_MainScene.at(idx)->isStateChanged)
							{
								CCTutorialParticle * tutorialParticle = CCTutorialParticle::create("tuowei0.plist",100,55);
								tutorialParticle->setPosition(ccp(75,0));
								tutorialParticle->setTag(CCTUTORIALPARTICLETAG);
								tempCell->addChild(tutorialParticle);
							}
						}
					}
				}
			}
			return tempCell;
		}
	}
	else if (getCurPresentPanel() == p_team)
	{
		cell=new CCTableViewCell();
		cell->autorelease();

		if (idx == selfTeamPlayerVector.size())
		{
			UILayer * temp_layer = UILayer::create();
			cell->addChild(temp_layer);
			//管理
			UIButton * btn_manager = UIButton::create();
			btn_manager->setTouchEnable(true);
			btn_manager->setPressedActionEnabled(true);
			btn_manager->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
			btn_manager->setScale9Enable(true);
			btn_manager->setScale9Size(CCSizeMake(60,32));
			btn_manager->setCapInsets(CCRect(18,18,1,1));
			btn_manager->setAnchorPoint(ccp(0.5f,0.5f));
			btn_manager->setPosition(ccp(47,20));
			btn_manager->addReleaseEvent(this,coco_releaseselector(MissionAndTeam::ManageTeamEvent));
			temp_layer->addWidget(btn_manager);
			UILabel * l_manager = UILabel::create();
			l_manager->setText(StringDataManager::getString("mainScene_missionAndTeam_manager"));
			l_manager->setFontName(APP_FONT_NAME);
			l_manager->setFontSize(18);
			l_manager->setAnchorPoint(ccp(.5f,.5f));
			l_manager->setPosition(ccp(0,0));
			btn_manager->addChild(l_manager);
			//退组
			UIButton * btn_ExitTeam = UIButton::create();
			btn_ExitTeam->setTouchEnable(true);
			btn_ExitTeam->setPressedActionEnabled(true);
			btn_ExitTeam->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
			btn_ExitTeam->setScale9Enable(true);
			btn_ExitTeam->setScale9Size(CCSizeMake(60,32));
			btn_ExitTeam->setCapInsets(CCRect(18,18,1,1));
			btn_ExitTeam->setAnchorPoint(ccp(0.5f,0.5f));
			btn_ExitTeam->setPosition(ccp(123,20));
			btn_ExitTeam->addReleaseEvent(this,coco_releaseselector(MissionAndTeam::ExitTeamEvent));
			temp_layer->addWidget(btn_ExitTeam);
			UILabel * l_ExitTeam = UILabel::create();
			l_ExitTeam->setText(StringDataManager::getString("mainScene_missionAndTeam_exitTeam"));
			l_ExitTeam->setFontName(APP_FONT_NAME);
			l_ExitTeam->setFontSize(18);
			l_ExitTeam->setAnchorPoint(ccp(.5f,.5f));
			l_ExitTeam->setPosition(ccp(0,0));
			btn_ExitTeam->addChild(l_ExitTeam);
		}
		else
		{
			CTeamMember * member_ =new CTeamMember();
			member_->CopyFrom(*selfTeamPlayerVector.at(idx));
			ShowTeamPlayer * team_= ShowTeamPlayer::create(member_,leaderId);
			team_->setAnchorPoint(ccp(0,0));
			team_->setPosition(ccp(17,0));
			team_->setTag(idx);
			cell->addChild(team_);
			delete member_;
		}
	}
	else if (getCurPresentPanel() == p_fiveInstance)
	{
		cell=new CCTableViewCell();
		cell->autorelease();

		CCLabelBMFont * l_win = CCLabelBMFont::create(StringDataManager::getString("fivePerson_fucPanel_win"),"res_ui/font/ziti_3.fnt");
		l_win->setAnchorPoint(ccp(0.5f,1.0f));
		l_win->setPosition(ccp(85,93));
		l_win->setScale(1.1f);
		cell->addChild(l_win);
		
		CCLabelTTF * l_des = CCLabelTTF::create(StringDataManager::getString("fivePerson_fucPanel_info"),APP_FONT_NAME,18,CCSizeMake(160,0),kCCTextAlignmentLeft,kCCVerticalTextAlignmentTop);
		l_des->setAnchorPoint(ccp(0.f,1.0f));
		l_des->setPosition(ccp(5,70));
		l_des->setColor(ccc3(243,252,2)); 	//黄色
		cell->addChild(l_des);
		//Boss
		CCLabelTTF * l_boss = CCLabelTTF::create(StringDataManager::getString("fivePerson_boss"),APP_FONT_NAME,18);
		l_boss->setAnchorPoint(ccp(0.f,1.0f));
		l_boss->setPosition(ccp(5,l_des->getPosition().y-l_des->getContentSize().height-2));
		//l_boss->setColor(ccc3(255,51,51));   //红色
		l_boss->setColor(ccc3(243,252,2)); 	//黄色
		cell->addChild(l_boss);
		//boss value
		std::string str_all ;
		char str_curValue[20];
		sprintf(str_curValue,"%d",cur_boss_count);
		char str_maxValue[20];
		sprintf(str_maxValue,"%d",max_boss_count);
		str_all.append(str_curValue);
		str_all.append("/");
		str_all.append(str_maxValue);
		//CCLabelTTF * l_boss_Value = CCLabelTTF::create(str_all.c_str(),APP_FONT_NAME,18);
// 		CCLabelBMFont * l_boss_Value = CCLabelBMFont::create(str_all.c_str(),"res_ui/font/ziti_3.fnt");
// 		l_boss_Value->setAnchorPoint(ccp(0.f,1.0f));
// 		l_boss_Value->setPosition(ccp(l_boss->getPositionX()+l_boss->getContentSize().width+3,l_boss->getPosition().y));
// 		cell->addChild(l_boss_Value);
		CCLabelAtlas * l_boss_Value = CCLabelAtlas::create(str_all.c_str(),"res_ui/font/yellow_num.png", 10, 21, '/');
		l_boss_Value->setAnchorPoint(ccp(0.f,1.0f));
		l_boss_Value->setPosition(ccp(l_boss->getPositionX()+l_boss->getContentSize().width+3,l_boss->getPosition().y+1));
		cell->addChild(l_boss_Value);

		//Monster
		CCLabelTTF * l_monster = CCLabelTTF::create(StringDataManager::getString("fivePerson_monster"),APP_FONT_NAME,18);
		l_monster->setAnchorPoint(ccp(0.f,1.0f));
		l_monster->setPosition(ccp(l_boss->getPositionX(),l_boss->getPosition().y-l_boss->getContentSize().height-2));
		//l_monster->setColor(ccc3(255,51,51));   //红色
		l_monster->setColor(ccc3(243,252,2)); 	//黄色
		cell->addChild(l_monster);

		//monster value
		std::string str_all_monster ;
		char str_curMonsterValue[20];
		sprintf(str_curMonsterValue,"%d",cur_monster_count);
		char str_maxMonsterValue[20];
		sprintf(str_maxMonsterValue,"%d",max_monster_count);
		str_all_monster.append(str_curMonsterValue);
		str_all_monster.append("/");
		str_all_monster.append(str_maxMonsterValue);
// 		CCLabelTTF * l_monster_Value = CCLabelTTF::create(str_all_monster.c_str(),APP_FONT_NAME,18);
// 		l_monster_Value->setAnchorPoint(ccp(0.f,1.0f));
// 		l_monster_Value->setPosition(ccp(l_monster->getPositionX()+l_monster->getContentSize().width+3,l_monster->getPosition().y));
// 		cell->addChild(l_monster_Value);
		CCLabelAtlas * l_monster_Value = CCLabelAtlas::create(str_all_monster.c_str(),"res_ui/font/yellow_num.png", 10, 21, '/');
		l_monster_Value->setAnchorPoint(ccp(0.f,1.0f));
		l_monster_Value->setPosition(ccp(l_monster->getPositionX()+l_monster->getContentSize().width+3,l_monster->getPosition().y+1));
		cell->addChild(l_monster_Value);
	}
	else if (getCurPresentPanel() == p_familyFight)
	{
		cell=new CCTableViewCell();
		cell->autorelease();

		CCScale9Sprite * cellFrame = CCScale9Sprite::create("res_ui/zhezhao70.png");
		cellFrame->setAnchorPoint(ccp(0,0));
		cellFrame->setPosition(ccp(0,0));
		cellFrame->setPreferredSize(CCSizeMake(170,52));
		cellFrame->setCapInsets(CCRectMake(7,7,1,1));
		cellFrame->setOpacity(160);
		cell->addChild(cellFrame);

		CCScale9Sprite * sp_frame = CCScale9Sprite::create("gamescene_state/zhujiemian3/renwuduiwu/bossdi.png");
		sp_frame->setAnchorPoint(ccp(0,0));
		sp_frame->setPosition(ccp(16,5));
		sp_frame->setPreferredSize(CCSizeMake(140,20));
		sp_frame->setCapInsets(CCRectMake(12,9,1,1));
		cell->addChild(sp_frame);

		CCSprite * sp_blood = CCSprite::create("gamescene_state/zhujiemian3/renwuduiwu/bosshp.png");
		sp_blood->setAnchorPoint(ccp(0,0.5f));
		sp_blood->setPosition(ccp(19,16));
		cell->addChild(sp_blood);

		CCLabelTTF * l_bloodValue = CCLabelTTF::create("",APP_FONT_NAME,14);
		l_bloodValue->setAnchorPoint(ccp(0.5f,0.5f));
		l_bloodValue->setPosition(ccp(86,16));
		cell->addChild(l_bloodValue);

		float scale = 1.0f;
		if (idx == 0)
		{
			if (FamilyFightData::getBossOwnerHpInfo().maxHp > 0)
			{
				scale = FamilyFightData::getBossOwnerHpInfo().curHp*1.0f/FamilyFightData::getBossOwnerHpInfo().maxHp;
			}
		}
		else if (idx == 1)
		{
			if (FamilyFightData::getBossCommonHpInfo().maxHp > 0)
			{
				scale = FamilyFightData::getBossCommonHpInfo().curHp*1.0f/FamilyFightData::getBossCommonHpInfo().maxHp;
			}
		}
		else if (idx == 2)
		{
			if (FamilyFightData::getBossEnemyHpInfo().maxHp > 0)
			{
				scale = FamilyFightData::getBossEnemyHpInfo().curHp*1.0f/FamilyFightData::getBossEnemyHpInfo().maxHp;
			}
		}

		int tempScale = scale*100;
		std::string str_scale;
		char s_scale[20];
		sprintf(s_scale,"%d",tempScale);
		str_scale.append(s_scale);
		str_scale.append("%");
		l_bloodValue->setString(str_scale.c_str());
		sp_blood->setTextureRect(CCRectMake(0,0,sp_blood->getContentSize().width*scale,sp_blood->getContentSize().height));
		
		//Boss
		CCLabelTTF * l_boss = CCLabelTTF::create("",APP_FONT_NAME,16);
		l_boss->setAnchorPoint(ccp(0.5f,0.5f));
		l_boss->setPosition(ccp(83,35));
		//l_boss->setColor(ccc3(10,227,221));   //蓝色
		cell->addChild(l_boss);
		if (idx == 0)
		{
			l_boss->setString(StringDataManager::getString("FamilyFightUI_boss_owner"));
			l_boss->setColor(ccc3(31,252,2));             //绿色
		}
		else if (idx == 1)
		{
			l_boss->setString(StringDataManager::getString("FamilyFightUI_boss_common"));
			l_boss->setColor(ccc3(243,252,2)); 				//黄色
		}
		else if (idx == 2)
		{
			l_boss->setString(StringDataManager::getString("FamilyFightUI_boss_enemy"));
			l_boss->setColor(ccc3(252,2,2)); 				//红色
		}
	}
	else if (getCurPresentPanel() == p_singCopyChallenge)
	{
		cell=new CCTableViewCell();
		cell->autorelease();
	}
	else if (getCurPresentPanel() == p_offLineArena)   //竞技场
	{
		cell=new CCTableViewCell();
		cell->autorelease();

		if (table->getTag() == kTag_offLineArena_tableView_left)   //我方
		{
			CCSprite * frame = CCSprite::create("gamescene_state/zhujiemian3/renwuduiwu/jingjichang_di.png");
			frame->setAnchorPoint(ccp(0,0));
			frame->setPosition(ccp(1,0));
			cell->addChild(frame);

			CCSprite *sp_blood = CCSprite::create("gamescene_state/zhujiemian3/renwuduiwu/jingjichang_mine.png");
			sp_blood->setAnchorPoint(ccp(0,0.5f));
			sp_blood->setPosition(ccp(23,9));
			cell->addChild(sp_blood);

			CCLabelTTF * l_name= CCLabelTTF::create("",APP_FONT_NAME,12);
			l_name->setAnchorPoint(ccp(0.5f,0.5f));
			l_name->setPosition(ccp(49,9));
			cell->addChild(l_name);

			float blood_scale = 1.0f;
			if (idx == 0)
			{
				frame->setPosition(ccp(1,0));
				frame->setScaleX(1.0f);
				frame->setScaleY(1.0f);

				blood_scale = GameView::getInstance()->myplayer->getActiveRole()->hp()*1.0f/GameView::getInstance()->myplayer->getActiveRole()->maxhp();
				l_name->setString(StringDataManager::getString("WorldBossUI_mine"));
				sp_blood->setTextureRect(CCRectMake(0,0,sp_blood->getContentSize().width*blood_scale,sp_blood->getContentSize().height));
				if (blood_scale <= 0.f)
				{
					CCTexture2D * texture = CCTextureCache::sharedTextureCache()->addImage("gamescene_state/zhujiemian3/renwuduiwu/jingjichang_di2.png");
					frame->setTexture(texture);
				}
				else
				{
					CCTexture2D * texture = CCTextureCache::sharedTextureCache()->addImage("gamescene_state/zhujiemian3/renwuduiwu/jingjichang_di.png");
					frame->setTexture(texture);
				}

				CCLabelTTF * l_lv= CCLabelTTF::create("",APP_FONT_NAME,12);
				l_lv->setAnchorPoint(ccp(0.5f,0.5f));
				l_lv->setPosition(ccp(12,9));
				cell->addChild(l_lv);

				char s_lv[10];
				sprintf(s_lv,"%d",GameView::getInstance()->myplayer->getActiveRole()->level());
				l_lv->setString(s_lv);
			}
			else
			{
				frame->setPosition(ccp(20,1));
				frame->setScaleX(0.77f);
				frame->setScaleY(0.84f);

				GeneralInfoInArena * temp = generalsInArena_mine.at(idx-1);
				blood_scale = temp->cur_hp*1.0f/temp->max_hp;
				sp_blood->setTextureRect(CCRectMake(0,0,sp_blood->getContentSize().width*blood_scale,sp_blood->getContentSize().height));

				CCSprite * sprite_star= CCSprite::create(RecuriteActionItem::getStarPathByNum(temp->star).c_str());
				sprite_star->setAnchorPoint(ccp(0.5f,0.5f));
				sprite_star->setPosition(ccp(12,9));
				sprite_star->setScale(0.45f);
				cell->addChild(sprite_star);

				if (blood_scale <= 0.f || temp->isFinishedFight)
				{
					CCTexture2D * texture = CCTextureCache::sharedTextureCache()->addImage("gamescene_state/zhujiemian3/renwuduiwu/jingjichang_di4.png");
					frame->setTexture(texture);
				}
				else
				{
					CCTexture2D * texture = CCTextureCache::sharedTextureCache()->addImage("gamescene_state/zhujiemian3/renwuduiwu/jingjichang_di3.png");
					frame->setTexture(texture);
				}

				l_name->setString(StrUtils::unApplyColor(temp->name.c_str()).c_str());
			}	                                                          
		}
		else                                                                                          //敌方
		{
			if (idx == 0)
			{
				if (p_enemyInfoInArena->generalId > 0)
				{
					CCSprite * frame = CCSprite::create("gamescene_state/zhujiemian3/renwuduiwu/jingjichang_di.png");
					frame->setAnchorPoint(ccp(0,0));
					frame->setPosition(ccp(1,0));
					cell->addChild(frame);

					CCSprite *sp_blood = CCSprite::create("gamescene_state/zhujiemian3/renwuduiwu/jingjichang_diren.png");
					sp_blood->setAnchorPoint(ccp(0,0.5f));
					sp_blood->setPosition(ccp(23,9));
					cell->addChild(sp_blood);

					CCLabelTTF * l_name= CCLabelTTF::create("",APP_FONT_NAME,12);
					l_name->setAnchorPoint(ccp(0.5f,0.5f));
					l_name->setPosition(ccp(49,9));
					cell->addChild(l_name);

					float blood_scale = 1.0f;                                                            
					l_name->setString(StringDataManager::getString("WorldBossUI_other"));

					blood_scale = p_enemyInfoInArena->cur_hp*1.0f/p_enemyInfoInArena->max_hp;

					CCLabelTTF * l_lv= CCLabelTTF::create("",APP_FONT_NAME,12);
					l_lv->setAnchorPoint(ccp(0.5f,0.5f));
					l_lv->setPosition(ccp(12,9));
					cell->addChild(l_lv);
					char s_lv[10];
					sprintf(s_lv,"%d",p_enemyInfoInArena->level);
					l_lv->setString(s_lv);

					sp_blood->setTextureRect(CCRectMake(0,0,sp_blood->getContentSize().width*blood_scale,sp_blood->getContentSize().height));
					if (blood_scale <= 0.f)
					{
						CCTexture2D * texture = CCTextureCache::sharedTextureCache()->addImage("gamescene_state/zhujiemian3/renwuduiwu/jingjichang_di2.png");
						frame->setTexture(texture);
					}
				}
			}
			else
			{
				CCSprite * frame = CCSprite::create("gamescene_state/zhujiemian3/renwuduiwu/jingjichang_di3.png");
				frame->setAnchorPoint(ccp(0,0));
				frame->setPosition(ccp(20,1));
				cell->addChild(frame);

				CCSprite *sp_blood = CCSprite::create("gamescene_state/zhujiemian3/renwuduiwu/jingjichang_diren.png");
				sp_blood->setAnchorPoint(ccp(0,0.5f));
				sp_blood->setPosition(ccp(23,9));
				cell->addChild(sp_blood);

				CCLabelTTF * l_name= CCLabelTTF::create("",APP_FONT_NAME,12);
				l_name->setAnchorPoint(ccp(0.5f,0.5f));
				l_name->setPosition(ccp(49,9));
				cell->addChild(l_name);

				float blood_scale = 1.0f;                                                            

				GeneralInfoInArena * temp = generalsInArena_other.at(idx-1);
				blood_scale = temp->cur_hp*1.0f/temp->max_hp;

				l_name->setString(StrUtils::unApplyColor(temp->name.c_str()).c_str());

				sp_blood->setTextureRect(CCRectMake(0,0,sp_blood->getContentSize().width*blood_scale,sp_blood->getContentSize().height));
				if (blood_scale <= 0.f || temp->isFinishedFight)
				{
					CCTexture2D * texture = CCTextureCache::sharedTextureCache()->addImage("gamescene_state/zhujiemian3/renwuduiwu/jingjichang_di4.png");
					frame->setTexture(texture);
				}

				CCSprite * sprite_star= CCSprite::create(RecuriteActionItem::getStarPathByNum(temp->star).c_str());
				sprite_star->setAnchorPoint(ccp(0.5f,0.5f));
				sprite_star->setPosition(ccp(12,9));
				sprite_star->setScale(0.45f);
				cell->addChild(sprite_star);
			}
			
		}
	}

	return cell;
}

unsigned int MissionAndTeam::numberOfCellsInTableView(CCTableView *table)
{

	//return GameView::getInstance()->missionManager->MissionList_MainScene.size();

	if (getCurPresentPanel() == p_mission)
	{
		return GameView::getInstance()->missionManager->MissionList_MainScene.size();
	}
	else if (getCurPresentPanel() == p_team)
	{
		if(TeamLayer->getWidgetByName("teamInvite_btn_name"))
		{
			if (selfTeamPlayerVector.size() > 0 )
			{
				teamInvite_btn->setVisible(false);
			}else
			{
				teamInvite_btn->setVisible(true);
			}
		}

		if (selfTeamPlayerVector.size() > 0)
		{
			return selfTeamPlayerVector.size()+1;
		}
		else
		{
			return 0;
		}
	}
	else if (getCurPresentPanel() == p_fiveInstance)
	{
		return 1;
	}
	else if (getCurPresentPanel() == p_familyFight)
	{
		return 3;
	}
	else if (getCurPresentPanel() == p_singCopyChallenge)
	{
		return 0;
	}
	else if (getCurPresentPanel() == p_offLineArena)
	{
		if (table->getTag() == kTag_offLineArena_tableView_left)
		{
			return generalsInArena_mine.size()+1;
		}
		else if (table->getTag() == kTag_offLineArena_tableView_right)
		{
			return generalsInArena_other.size()+1;
		}
		else
		{
			return 0;
		}
	}
	else
	{
		return 0;
	}
}

void MissionAndTeam::tableCellTouched(CCTableView* table, CCTableViewCell* cell)
{
	if (GameUtils::millisecondNow() - m_nLastTouchTime <= 250.0f)
	{
		m_nLastTouchTime = GameUtils::millisecondNow();
		return;
	}

	m_nLastTouchTime = GameUtils::millisecondNow();
	 
	if (getCurPresentPanel() == p_mission)
	{
		//执行各种任务
		//GameView::getInstance()->missionManager->curMissionInfo->CopyFrom(*GameView::getInstance()->missionManager->MissionList_MainScene.at(cell->getIdx()));
		GameView::getInstance()->missionManager->curMissionInfo->CopyFrom(*GameView::getInstance()->missionManager->MissionList_MainScene.at(cell->getIdx()));
		GameView::getInstance()->missionManager->addMission(GameView::getInstance()->missionManager->curMissionInfo);

		if (cell->getIdx() == 0)
		{
			MissionManager::getInstance()->guideForMmission_time = 0.f;
			//add by yangjun 2014.7.2
			if (this->getChildByTag(KTag_GuidePen))
			{
				GameView::getInstance()->missionManager->MissionList_MainScene.at(cell->getIdx())->isNeedTeach = false;
				this->removeGuidePen();
			}
			

			if (cell->getChildByTag(CCTUTORIALPARTICLETAG))
			{
				GameView::getInstance()->missionManager->MissionList_MainScene.at(cell->getIdx())->isNeedTeach = false;
				cell->getChildByTag(CCTUTORIALPARTICLETAG)->removeFromParent();
			}

			if (m_nTutorialIndicatorIndex == 1)
			{
				Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
				if(sc != NULL)
					sc->endCommand(this);
			}
		}

		if (cell->getIdx() == 1)
		{
			if (cell->getChildByTag(CCTUTORIALPARTICLETAG))
			{
				GameView::getInstance()->missionManager->MissionList_MainScene.at(cell->getIdx())->isNeedTeach = false;
				cell->getChildByTag(CCTUTORIALPARTICLETAG)->removeFromParent();
			}

			if (m_nTutorialIndicatorIndex == 2)
			{
				Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
				if(sc != NULL)
					sc->endCommand(this);
			}
		}

// 		selectCellId = cell->getIdx();
// 		//取出上次选中状态，更新本次选中状态
// 		if(table->cellAtIndex(lastSelectCellId))
// 		{
// 			if (table->cellAtIndex(lastSelectCellId)->getChildByTag(MISSION_TABALEVIEW_HIGHLIGHT_TAG))
// 			{
// 				table->cellAtIndex(lastSelectCellId)->getChildByTag(MISSION_TABALEVIEW_HIGHLIGHT_TAG)->removeFromParent();
// 			}
// 		}
// 
// 		CCScale9Sprite* pHighlightSpr = CCScale9Sprite::create(CCRectMake(7, 7, 1, 1) , "res_ui/kuang_0_on.png");
// 		pHighlightSpr->setPreferredSize(CCSize(145,40));
// 		pHighlightSpr->setAnchorPoint(CCPointZero);
// 		pHighlightSpr->setTag(MISSION_TABALEVIEW_HIGHLIGHT_TAG);
// 		cell->addChild(pHighlightSpr);
// 
// 		lastSelectCellId = cell->getIdx();
	}
	else if (getCurPresentPanel() == p_team)
	{
		CCLog("this.cell.id = %d",cell->getIdx());

		MainScene * mainscene =(MainScene *)GameView::getInstance()->getMainUIScene();
		CCSize winsize =CCDirector::sharedDirector()->getVisibleSize();
		long long selfId =GameView::getInstance()->myplayer->getRoleId();
		long long numIdx_ = selfTeamPlayerVector.at(cell->getIdx())->roleid();
		CTeamMember * member_ =new CTeamMember();
		member_->CopyFrom(*selfTeamPlayerVector.at(cell->getIdx()));
		if (selfId != numIdx_)
		{
			TeamMemberListOperator * teamlistPlayer =TeamMemberListOperator::create(member_,leaderId);
			teamlistPlayer->setAnchorPoint(ccp(0,0));
			teamlistPlayer->setPosition(ccp(mainscene->getChildByTag(kTagMissionAndTeam)->getPositionX()+this->getContentSize().width,
											winsize.height/2+teamlistPlayer->getContentSize().height/2));
			teamlistPlayer->setZOrder(10);
			mainscene->addChild(teamlistPlayer);
		}
		delete member_;

// 		float x = GameView::getInstance()->myplayer->MissionData[cell->getIdx()].coordinateX;
// 		float y = GameView::getInstance()->myplayer->MissionData[cell->getIdx()].coordinateY;
// 		CCPoint targetPos = ccp(x,y);
// 		//移动命令
// 		MyPlayerCommandMove* cmd = new MyPlayerCommandMove();
// 		cmd->targetPosition = targetPos;
// 		GameView::getInstance()->myplayer->setNextCommand(cmd, true);
	}
	else if (getCurPresentPanel() == p_fiveInstance)
	{
		FiveInstanceInfoEvent(NULL);
	}
	else if (getCurPresentPanel() == p_familyFight)
	{
		bool isNeedOpenHungUp = false;
		CCPoint targetPos = ccp(0,0);
		if (cell->getIdx() == 0)              //寻路到我方BOSS
		{
			targetPos = ccp(FamilyFightData::getBossOwnerPos().pos_x*1.0f,FamilyFightData::getBossOwnerPos().pos_y*1.0f);
			isNeedOpenHungUp = false;
		}
		else if (cell->getIdx() == 1)       //寻路到中立BOSS
		{
			targetPos = ccp(FamilyFightData::getBossCommonPos().pos_x*1.0f,FamilyFightData::getBossCommonPos().pos_y*1.0f);
			isNeedOpenHungUp = true;
		}
		else if (cell->getIdx() == 2)        //寻路到敌方BOSS
		{
			targetPos = ccp(FamilyFightData::getBossEnemyPos().pos_x*1.0f,FamilyFightData::getBossEnemyPos().pos_y*1.0f);
			isNeedOpenHungUp = true;
		}
		if (targetPos.x != 0 && targetPos.y != 0)
		{
			MyPlayerCommandMove* cmd = new MyPlayerCommandMove();
			short tileX = GameView::getInstance()->getGameScene()->positionToTileX(targetPos.x);
			short tileY = GameView::getInstance()->getGameScene()->positionToTileY(targetPos.y);
			cmd->targetPosition = targetPos;
			cmd->method = MyPlayerCommandMove::method_searchpath;
			bool bSwitchCommandImmediately = true;
			if(GameView::getInstance()->myplayer->isAttacking())
				bSwitchCommandImmediately = false;
			GameView::getInstance()->myplayer->setNextCommand(cmd, bSwitchCommandImmediately);	

			if (isNeedOpenHungUp)
			{
				m_Target_pos_inFamilyFight = targetPos;
				this->schedule(schedule_selector(MissionAndTeam::CheckDistanceInFamilyFight));
			}
		}
	}
}

void MissionAndTeam::tableCellHighlight(CCTableView* table, CCTableViewCell* cell)
{
	if (getCurPresentPanel() == p_mission || getCurPresentPanel() == p_fiveInstance ||  getCurPresentPanel() == p_familyFight )
	{
		cell->setContentSize(tableCellSizeForIndex(table, cell->getIdx()));

		// highlight sprite
		CCScale9Sprite* pHighlightSpr = CCScale9Sprite::create(CCRectMake(11, 11, 1, 1) , "gamescene_state/zhujiemian3/renwuduiwu/light_1.png");
		pHighlightSpr->setPreferredSize(cell->getContentSize());
		pHighlightSpr->setAnchorPoint(CCPointZero);
		pHighlightSpr->setTag(MISSION_TABALEVIEW_HIGHLIGHT_TAG);
		cell->addChild(pHighlightSpr);
	}
	else
	{
		
	}
	// please set cell's content size
	
}
void MissionAndTeam::tableCellUnhighlight(CCTableView* table, CCTableViewCell* cell)
{
	cell->removeChildByTag(MISSION_TABALEVIEW_HIGHLIGHT_TAG);
}

void MissionAndTeam::scrollViewDidScroll(CCScrollView* view )
{
	MissionManager::getInstance()->setMissionAndTeamOffSet(missionTableView->getContentOffset());
}

void MissionAndTeam::scrollViewDidZoom(CCScrollView* view )
{
}

void MissionAndTeam::popUpMission( CCObject *pSender )
{
	ImageView_missionAndTeam_on_off->setRotation(180);

	CCAction * action1 = CCMoveTo::create(0.15f,ccp(layer_leader->getPositionX()+LAYER_SIZE_WIDTH,layer_leader->getPositionY()));
	action1->setTag(KTag_MoveAction);
	layer_leader->runAction(action1);

// 	CCAction * action2 = CCMoveTo::create(0.15f,ccp(LAYER_SIZE_WIDTH-missionAndTeam_on_off->getContentSize().width/2,missionAndTeam_on_off->getPosition().y));
// 	missionAndTeam_on_off->runAction(action2);

	isOn = true;
	this->setContentSize(CCSizeMake(LAYER_SIZE_WIDTH,contentSizeHeight));

	refreshTutorial();
}

void MissionAndTeam::takeBackMission( CCObject *pSender )
{
	ImageView_missionAndTeam_on_off->setRotation(0);

	CCAction * action1 = CCMoveTo::create(0.15f,ccp(layer_leader->getPositionX()-LAYER_SIZE_WIDTH,layer_leader->getPositionY()));
	action1->setTag(KTag_MoveAction);
	layer_leader->runAction(action1);

// 	CCAction * action2 = CCMoveTo::create(0.15f,ccp(missionAndTeam_on_off->getContentSize().width/2,missionAndTeam_on_off->getPosition().y));
// 	missionAndTeam_on_off->runAction(action2);

	isOn = false;
	this->setContentSize(CCSizeMake(32,contentSizeHeight));

	refreshTutorial();
}


void MissionAndTeam::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void MissionAndTeam::addCCTutorialIndicator1( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
	if (getCurPresentPanel() == p_mission)
	{
		if(GameView::getInstance()->missionManager->MissionList_MainScene.size()>0)
		{
			for(int i = 0;i<GameView::getInstance()->missionManager->MissionList_MainScene.size();++i)
			{
				MissionCell* cell = dynamic_cast<MissionCell*>(missionTableView->cellAtIndex(i));
				if (!cell)
					continue;

				if (cell->getChildByTag(CCTUTORIALPARTICLETAG) != NULL)
				{
					cell->getChildByTag(CCTUTORIALPARTICLETAG)->removeFromParent();
				}
			}
		}

		if (this->getChildByTag(KTag_GuidePen))
		{
			this->removeGuidePen();
		}

		CCSize winSize = CCDirector::sharedDirector()->getWinSize();
		int _w = (winSize.width-800)/2;
		int _h = (winSize.height-480)/2;
		CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,170,60,true);
		tutorialIndicator->setDrawNodePos(ccp(pos.x+170/2,pos.y-67));
		tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
		MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
		if (mainScene)
		{
			mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
		}
	}
	else
	{
	}

// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,true);
// 	tutorialIndicator->setPosition(ccp(pos.x+147,pos.y+235));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);
}

void MissionAndTeam::addCCTutorialIndicator2( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
	if (getCurPresentPanel() == p_mission)
	{
		if(GameView::getInstance()->missionManager->MissionList_MainScene.size()>0)
		{
			for(int i = 0;i<GameView::getInstance()->missionManager->MissionList_MainScene.size();++i)
			{
				MissionCell* cell = dynamic_cast<MissionCell*>(missionTableView->cellAtIndex(i));
				if (!cell)
					continue;

				if (cell->getChildByTag(CCTUTORIALPARTICLETAG) != NULL)
				{
					cell->getChildByTag(CCTUTORIALPARTICLETAG)->removeFromParent();
				}
			}
		}

		if (this->getChildByTag(KTag_GuidePen))
		{
			this->removeGuidePen();
		}
	}
	else
	{
	}
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,true);
// 	tutorialIndicator->setPosition(ccp(pos.x+147,pos.y+175));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);

	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height-480)/2;
	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,170,60,true);
	tutorialIndicator->setDrawNodePos(ccp(pos.x+170/2,pos.y-127));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void MissionAndTeam::addCCTutorialIndicator3( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,true);
	tutorialIndicator->setPosition(ccp(pos.x+105,pos.y+130));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	tutorialIndicator->addHighLightFrameAction(170,100,ccp(-23,5));
	tutorialIndicator->addCenterAnm(170,100,ccp(-23,5));
	this->addChild(tutorialIndicator);
}

void MissionAndTeam::removeCCTutorialIndicator()
{
	CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		CCTeachingGuide* teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}

void MissionAndTeam::MissionAndTeamOnOff( CCObject *pSender )
{
	//开启等级
	int openlevel = 0;
	std::map<int,int>::const_iterator cIter;
	cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(21);
	if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // 没找到就是指向END了  
	{
	}
	else
	{
		openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[21];
	}

	if (openlevel > GameView::getInstance()->myplayer->getActiveRole()->level())
	{
		char s_des [200];
		const char* str_des = StringDataManager::getString("shrinkage_will_be_open");
		sprintf(s_des,str_des,openlevel);
		GameView::getInstance()->showAlertDialog(s_des);
		return;
	}

	if (layer_leader->getActionByTag(KTag_MoveAction))
		return;

	if(isOn)
	{
// 		if (getCurPresentPanel() == p_mission)
// 		{
// 			if (MissionLayer->getChildByTag(KTag_MoveAction))
// 				return;
// 
// 		}
// 		else if (getCurPresentPanel() == p_team)
// 		{
// 			if (TeamLayer->getChildByTag(KTag_MoveAction))
// 				return;
// 
// 		}
		
		takeBackMission(pSender);
	}
	else
	{
// 		if (getCurPresentPanel() == p_mission)
// 		{
// 			if (MissionLayer->getChildByTag(KTag_MoveAction))
// 				return;
// 
// 		}
// 		else if (getCurPresentPanel() == p_team)
// 		{
// 			if (TeamLayer->getChildByTag(KTag_MoveAction))
// 				return;
// 		}

		popUpMission(pSender);
	}
}

void MissionAndTeam::reloadMissionData()
{
	if (this->getCurPresentPanel() != p_mission)
		return;

	missionTableView->reloadData();
	this->refreshDownArrowStatus();

	this->removeCCTutorialIndicator();
}

void MissionAndTeam::reloadMissionDataWithoutChangeOffSet(CCPoint p_offset,CCSize p_contentSize)
{
	if (this->getCurPresentPanel() != p_mission)
		return;

	CCPoint _s = p_offset;
	int _h = p_contentSize.height + _s.y;
	missionTableView->reloadData();
	CCPoint temp = ccp(_s.x,_h - missionTableView->getContentSize().height);
	missionTableView->setContentOffset(temp); 

	this->refreshDownArrowStatus();

	this->removeCCTutorialIndicator();
}

void MissionAndTeam::refreshTeamPlayer()
{
	if (isExistTeamLayer)
	{
		std::vector<CTeamMember *>::iterator iter;
		for (iter = selfTeamPlayerVector.begin(); iter != selfTeamPlayerVector.end(); ++iter)
		{
			delete * iter;
		}
		selfTeamPlayerVector.clear();
		long long selfId = GameView::getInstance()->myplayer->getRoleId();
		for (int i = 0; i< GameView::getInstance()->teamMemberVector.size(); i++)
		{
			if (selfId != GameView::getInstance()->teamMemberVector.at(i)->roleid())
			{
				CTeamMember * members_ = new CTeamMember();
				members_->CopyFrom( *GameView::getInstance()->teamMemberVector.at(i));
				selfTeamPlayerVector.push_back(members_);
			}
		}

		if (this->getCurPresentPanel() == p_team)
		{
			teamTableView->reloadData();
		}
	}
}

void MissionAndTeam::addGuidePen()
{
	if (this->getCurPresentPanel() != p_mission)
		return;

	if (isExistTutorialInMainScene())
	{
		return;
	}

	if (GameView::getInstance()->myplayer->getActiveRole()->level() <= K_GuideForMainMission_Level)
	{
		if (this->getChildByTag(CCTUTORIALINDICATORTAG) == NULL)
		{
			CCTutorialIndicator * guidePen = (CCTutorialIndicator*)this->getChildByTag(KTag_GuidePen);
			if (guidePen == NULL)
			{
				guidePen = CCTutorialIndicator::create("",ccp(0,0),CCTutorialIndicator::Direction_LU,true,false);
				//guidePen->setPresentDesFrame(false);
				guidePen->setPosition(ccp(105,150));
				guidePen->setTag(KTag_GuidePen);
				guidePen->addHighLightFrameAction(170,60,ccp(-21,3),true);
				guidePen->addCenterAnm(170,60,ccp(-21,3));
				this->addChild(guidePen);
			}
			else
			{
				guidePen->setVisible(true);
				guidePen->addHighLightFrameAction(170,60,ccp(-21,3),true);
				guidePen->addCenterAnm(170,60,ccp(-21,3));
			}

			GameView::getInstance()->missionManager->isExistGuideForMmission = true;
		}
	}
}

void MissionAndTeam::removeGuidePen()
{
	if (this->getChildByTag(KTag_GuidePen))
	{
		this->getChildByTag(KTag_GuidePen)->setVisible(false);
		GameView::getInstance()->missionManager->isExistGuideForMmission = false;
	}
}

bool MissionAndTeam::isExistTutorialInMainScene()
{
	if(GameView::getInstance())
	{
		if (GameView::getInstance()->getGameScene())
		{
			MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
			if (mainScene)
			{
				if (mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG))
				{
					return true;
				}
				//主角头像区域
				if (mainScene->getChildByTag(kTagMyHeadInfo))
				{
					if (mainScene->getChildByTag(kTagMyHeadInfo)->getChildByTag(CCTUTORIALINDICATORTAG))
					{
						return true;
					}
				}
				//小任务面板区域
				if (this->getChildByTag(CCTUTORIALINDICATORTAG))
				{
					return true;
				}
				//底部菜单栏区域
				if (mainScene->getChildByTag(kTagHeadMenu))
				{
					if (mainScene->getChildByTag(kTagHeadMenu)->getChildByTag(CCTUTORIALINDICATORTAG))
					{
						return true;
					}
				}
				//技能快捷栏区域
				ShortcutLayer * shortcutLayer = (ShortcutLayer*)mainScene->getChildByTag(kTagShortcutLayer);
				if (shortcutLayer)
				{
					if (shortcutLayer->u_teachLayer->getChildByTag(CCTUTORIALINDICATORTAG))
					{
						return true;
					}
				}
				//右上角地图区域
				if (mainScene->getChildByTag(kTagGuideMapUI))
				{
					if (mainScene->getChildByTag(kTagGuideMapUI)->getChildByTag(CCTUTORIALINDICATORTAG))
					{
						return true;
					}
				}
				//第一个武将出战，学习第一个技能面板
				if (mainScene->getChildByTag(kTagTeachRemind))
				{
					if (mainScene->getChildByTag(kTagTeachRemind)->getChildByTag(CCTUTORIALINDICATORTAG))
					{
						return true;
					}
				}
			}
		}
	}

	return false;
}

void MissionAndTeam::setPanelType( int stype )
{
	s_panelType = stype;
}

int MissionAndTeam::getPanelType()
{
	return s_panelType;
}

void MissionAndTeam::setCurPresentPanel( int cpPanel )
{
	s_curPresentPanel = cpPanel;
}

int MissionAndTeam::getCurPresentPanel()
{
	return s_curPresentPanel;
}

void MissionAndTeam::refreshFunctionBtn()
{
	switch(GameView::getInstance()->getMapInfo()->maptype())
	{
	case com::future::threekingdoms::server::transport::protocol::copy:        //副本
		{
			setPanelType(type_FiveInstanceAndTeam);
			Button_mission->setVisible(false);
			Button_fiveInstance->setVisible(true);
			Button_team->setVisible(true);
			Button_familyFight->setVisible(false);
			Button_singCopy->setVisible(false);
			Button_offLineArena->setVisible(false);

			if (getCurPresentPanel() != p_team && getCurPresentPanel() != p_fiveInstance)
			{
				setCurPresentPanel(p_fiveInstance);
				Button_mission->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				MissionLayer->setVisible(false);

				Button_fiveInstance->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
				FiveInstanceLayer->setVisible(true);
				initFiveInstanceLayer();
				fiveInstanceTableView->reloadData();

				Button_familyFight->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				FamilyFightLayer->setVisible(false);

				Button_team->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				TeamLayer->setVisible(false);

				Button_singCopy->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
				SingCopyLayer->setVisible(false);

				Button_offLineArena->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
				OffLineArenaLayer->setVisible(false);
			}
		}
		break;
	case com::future::threekingdoms::server::transport::protocol::guildFight:        //家族战
		{
			setPanelType(type_FamilyFightAndTeam);
			Button_mission->setVisible(false);
			Button_fiveInstance->setVisible(false);
			Button_team->setVisible(true);
			Button_familyFight->setVisible(true);
			Button_singCopy->setVisible(false);
			Button_offLineArena->setVisible(false);

			if (getCurPresentPanel() != p_team && getCurPresentPanel() != p_familyFight)
			{
				setCurPresentPanel(p_familyFight);
				Button_mission->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				MissionLayer->setVisible(false);

				Button_fiveInstance->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				FiveInstanceLayer->setVisible(false);

				Button_familyFight->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
				FamilyFightLayer->setVisible(true);
				initFamilyFightLayer();
				familyFightTableView->reloadData();

				Button_team->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				TeamLayer->setVisible(false);

				Button_singCopy->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
				SingCopyLayer->setVisible(false);

				Button_offLineArena->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
				OffLineArenaLayer->setVisible(false);
			}
		}
		break;
	case com::future::threekingdoms::server::transport::protocol::coliseum:  //离线竞技场
		{
			setPanelType(type_OfflineArenaOnly);
			Button_mission->setVisible(false);
			Button_fiveInstance->setVisible(false);
			Button_team->setVisible(true);
			Button_familyFight->setVisible(false);
			Button_singCopy->setVisible(false);
			Button_offLineArena->setVisible(true);

			if (getCurPresentPanel() != p_team && getCurPresentPanel() != p_offLineArena)
			{
				setCurPresentPanel(p_offLineArena);
				Button_mission->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				MissionLayer->setVisible(false);

				Button_fiveInstance->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				FiveInstanceLayer->setVisible(false);

				Button_familyFight->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				FamilyFightLayer->setVisible(false);

				Button_team->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				TeamLayer->setVisible(false);

				Button_singCopy->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
				SingCopyLayer->setVisible(false);

				Button_offLineArena->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
				OffLineArenaLayer->setVisible(true);
				initOffLineArenaLayer();
				CCTableView * offLineArenaTableView_left = (CCTableView*)OffLineArenaLayer->getChildByTag(kTag_offLineArena_tableView_left);
				if (offLineArenaTableView_left)
					offLineArenaTableView_left->reloadData();
				CCTableView * offLineArenaTableView_right = (CCTableView*)OffLineArenaLayer->getChildByTag(kTag_offLineArena_tableView_right);
				if (offLineArenaTableView_right)
					offLineArenaTableView_right->reloadData();
			}
		}break;
	case com::future::threekingdoms::server::transport::protocol::tower:  //singCopy
		{
			setPanelType(type_SingCopyChanllengeOnly);
			Button_mission->setVisible(false);
			Button_fiveInstance->setVisible(false);
			Button_team->setVisible(true);
			Button_familyFight->setVisible(false);
			Button_singCopy->setVisible(true);
			Button_offLineArena->setVisible(false);

			if (getCurPresentPanel() != p_team && getCurPresentPanel() != p_singCopyChallenge)
			{
				setCurPresentPanel(p_singCopyChallenge);
				Button_mission->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				MissionLayer->setVisible(false);

				Button_fiveInstance->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				FiveInstanceLayer->setVisible(false);

				Button_familyFight->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				FamilyFightLayer->setVisible(false);

				Button_team->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				TeamLayer->setVisible(false);

				Button_singCopy->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
				SingCopyLayer->setVisible(true);
				initSingCopyLayer();
				//singCopyTableview->reloadData();

				Button_offLineArena->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
				OffLineArenaLayer->setVisible(false);
			}
		}break;
	default:
		{
			setPanelType(type_MissionAndTeam);
			Button_mission->setVisible(true);
			Button_fiveInstance->setVisible(false);
			Button_team->setVisible(true);
			Button_familyFight->setVisible(false);
			Button_singCopy->setVisible(false);
			Button_offLineArena->setVisible(false);

			if (getCurPresentPanel() != p_team && getCurPresentPanel() != p_mission)
			{
				setCurPresentPanel(p_mission);
				Button_mission->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
				MissionLayer->setVisible(true);
				initMissionLayer();
				missionTableView->reloadData();
				this->refreshDownArrowStatus();

				Button_team->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				TeamLayer->setVisible(false);

				Button_fiveInstance->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				FiveInstanceLayer->setVisible(false);

				Button_familyFight->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				FamilyFightLayer->setVisible(false);

				Button_singCopy->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
				SingCopyLayer->setVisible(false);

				Button_offLineArena->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
				OffLineArenaLayer->setVisible(false);
			}
			MainScene * mainScene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
			if (mainScene_!= NULL)
			{
				SingCopyCountDownUI * singCopyCountDownUi_ = (SingCopyCountDownUI * )mainScene_->getChildByTag(kTagSingCopyCountDownUi);
				if (singCopyCountDownUi_ != NULL)
				{
					singCopyCountDownUi_->removeFromParentAndCleanup(true);
				}
			}
		}
	}

	refreshTutorial();
}

void MissionAndTeam::refreshDownArrowStatus()
{
	UILayer * layer_hasNextMission= (UILayer*)MissionLayer->getChildByTag(MISSIONLAYER_LAYER_HASNEXT_TAG);
	if (!layer_hasNextMission)
		return;

	UIImageView * image_hasNextMission = (UIImageView*)layer_hasNextMission->getWidgetByName("image_hasNextMission");
	if (image_hasNextMission)
	{
		if (GameView::getInstance()->missionManager->MissionList_MainScene.size() > 3)
		{
			image_hasNextMission->setVisible(true);
		}
		else
		{
			image_hasNextMission->setVisible(false);
		}
	}
}

void MissionAndTeam::FiveInstanceInfoEvent( CCObject * pSender )
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

	if (m_nTutorialIndicatorIndex == 3)
	{
		CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
		if(tutorialIndicator != NULL)
		{
			Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
			if(sc != NULL)
				sc->endCommand(this);
		}
	}
 		
	if(GameView::getInstance()->myplayer->isAction(ACT_ATTACK))
		return;

	if (GameView::getInstance()->myplayer->selectEnemy(winSize.width))
	{
		if (GameView::getInstance()->myplayer->hasLockedActor()) 
		{
			GameActor* lockedActor = GameView::getInstance()->myplayer->getLockedActor();
			//same map
			CCPoint cp_role = GameView::getInstance()->myplayer->getWorldPosition();
			CCPoint cp_target = lockedActor->getWorldPosition();
			if (cp_target.x == 0 && cp_target.y == 0)
			{
				return;
			}

			MyPlayerCommandMove* cmd = new MyPlayerCommandMove();
			// NOT reachable, so find the near one
			short tileX = GameView::getInstance()->getGameScene()->positionToTileX(cp_target.x);
			short tileY = GameView::getInstance()->getGameScene()->positionToTileY(cp_target.y);
			if(GameView::getInstance()->getGameScene()->isLimitOnGround(tileX, tileY))
			{
				cp_target = GameView::getInstance()->missionManager->getNearestReachablePos(cp_target);
			}
			cmd->targetPosition = cp_target;
			cmd->method = MyPlayerCommandMove::method_searchpath;
			bool bSwitchCommandImmediately = true;
			if(GameView::getInstance()->myplayer->isAttacking())
				bSwitchCommandImmediately = false;
			GameView::getInstance()->myplayer->setNextCommand(cmd, bSwitchCommandImmediately);	

			DoThingWhenFinishedMove(cp_target);
		}
		
// 		MyPlayerAIConfig::setAutomaticSkill(1);
// 		MyPlayerAIConfig::enableRobot(true);
// 		GameView::getInstance()->myplayer->getMyPlayerAI()->start();
// 
// 		if (GameView::getInstance()->getGameScene())
// 		{
// 			if (GameView::getInstance()->getMainUIScene())
// 			{
// 				GuideMap * guide_ =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
// 				if (guide_)
// 				{
// 					if (guide_->getBtnHangUp())
// 					{
// 						guide_->getBtnHangUp()->setTextures("gamescene_state/zhujiemian3/tubiao/off_hook.png","gamescene_state/zhujiemian3/tubiao/off_hook.png","");
// 					}
// 				}
// 			}
// 		}
	}
	else
	{
		//req monster in instance
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1929);
	}
}


void MissionAndTeam::reloadToDefaultConfig( int instanceId )
{
	//显示副本进度（boss数量，普通怪数量）

	if (instanceId != -1)
	{
		if (FivePersonInstanceConfigData::s_fivePersonInstance.size()>0)
		{
			for(int i = 0;i<FivePersonInstanceConfigData::s_fivePersonInstance.size();++i)
			{
				if (instanceId == FivePersonInstanceConfigData::s_fivePersonInstance.at(i)->get_id())
				{
					max_boss_count = FivePersonInstanceConfigData::s_fivePersonInstance.at(i)->get_boss_count();
					max_monster_count = FivePersonInstanceConfigData::s_fivePersonInstance.at(i)->get_monster_count();
					cur_boss_count = 0;
					cur_monster_count = 0;
					break;
				}
			}
		}
	}

// 	//boss value
// 	std::string str_all ;
// 	char str_curValue[20];
// 	sprintf(str_curValue,"%d",cur_boss_count);
// 	char str_maxValue[20];
// 	sprintf(str_maxValue,"%d",max_boss_count);
// 	str_all.append(str_curValue);
// 	str_all.append("/");
// 	str_all.append(str_maxValue);
// 	l_boss_Value->setText(str_all.c_str());
// 	//monster value
// 	std::string str_all_monster ;
// 	char str_curMonsterValue[20];
// 	sprintf(str_curMonsterValue,"%d",cur_monster_count);
// 	char str_maxMonsterValue[20];
// 	sprintf(str_maxMonsterValue,"%d",max_monster_count);
// 	str_all_monster.append(str_curMonsterValue);
// 	str_all_monster.append("/");
// 	str_all_monster.append(str_maxMonsterValue);
// 	l_monster_Value->setText(str_all_monster.c_str());
// 	
// 	
	initFiveInstanceLayer();
	fiveInstanceTableView->reloadData();
}

void MissionAndTeam::updateInstanceSchedule( int bossCount,int monsterCount )
{
	cur_boss_count = bossCount;
	cur_monster_count = monsterCount;

	//boss value
// 	std::string str_all ;
// 	char str_curValue[20];
// 	sprintf(str_curValue,"%d",cur_boss_count);
// 	char str_maxValue[20];
// 	sprintf(str_maxValue,"%d",max_boss_count);
// 	str_all.append(str_curValue);
// 	str_all.append("/");
// 	str_all.append(str_maxValue);
// 	l_boss_Value->setText(str_all.c_str());

	//monster value
// 	std::string str_all_monster ;
// 	char str_curMonsterValue[20];
// 	sprintf(str_curMonsterValue,"%d",cur_monster_count);
// 	char str_maxMonsterValue[20];
// 	sprintf(str_maxMonsterValue,"%d",max_monster_count);
// 	str_all_monster.append(str_curMonsterValue);
// 	str_all_monster.append("/");
// 	str_all_monster.append(str_maxMonsterValue);
// 	l_monster_Value->setText(str_all_monster.c_str());
// 	
// 	
	if (isExistFiveInstanceLayer)
	{
		fiveInstanceTableView->reloadData();
	}
}


void MissionAndTeam::updateFamilyFightSchedult()
{
	familyFightTableView->reloadData();
}

void MissionAndTeam::DoThingWhenFinishedMove( CCPoint targetPos )
{
	m_Target_pos_inInstance = targetPos;
	this->schedule(schedule_selector(MissionAndTeam::CheckDistance));
}

void MissionAndTeam::CheckDistance(float dt)
{
	if (GameView::getInstance()->myplayer)
	{
		CCPoint cp_role = GameView::getInstance()->myplayer->getWorldPosition();
		CCPoint cp_target = m_Target_pos_inInstance;
		//if (sqrt(pow((cp_role.x-cp_target.x),2)+pow((cp_role.y-cp_target.y),2)) < 64)
		if (ccpDistance(cp_role,cp_target) < 64)
		{
			this->unschedule(schedule_selector(MissionAndTeam::CheckDistance));

			//began to kill monster
			GameView::getInstance()->myplayer->getMyPlayerAI()->startFight();

			if (GameView::getInstance()->getGameScene())
			{
				if (GameView::getInstance()->getMainUIScene())
				{
					GuideMap * guide_ =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
					if (guide_)
					{
						if (guide_->getBtnHangUp())
						{
							guide_->getBtnHangUp()->setTextures("gamescene_state/zhujiemian3/tubiao/off_hook.png","gamescene_state/zhujiemian3/tubiao/off_hook.png","");
						}
					}
				}
			}
		}
	}
}

void MissionAndTeam::updateSingCopyCellInfo( int level,int curMon,int allMon,int remMon,int useTime,int timeCount )
{
	if (m_singCopy_curMonsterNumber != curMon)
	{	
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

		UILayer  * layer_ = UILayer::create();
		GameView::getInstance()->getMainUIScene()->addChild(layer_);

		std::string temp_ = "";
		if (curMon == allMon)
		{
			const char *strings_curLevelBmfont = StringDataManager::getString("singleCopy_curLevelLabelBmFont_monster_2");
			temp_.append(strings_curLevelBmfont);
		}else
		{
			const char *strings_ = StringDataManager::getString("singleCopy_curLevelLabelBmFont_monster_1");
			char levelStr[50];
			sprintf(levelStr,strings_,curMon);

			temp_.append(levelStr);
		}
		
		UIImageView * background_ =UIImageView::create();
		background_->setTexture("res_ui/zhezhao80.png");
		background_->setAnchorPoint(ccp(0.5f,0.5f));
		background_->setScale9Enable(true);
		background_->setScale9Size(CCSizeMake(winSize.width,60));
		background_->setPosition(ccp(winSize.width/2,winSize.height*0.8f));
		layer_->addWidget(background_);
		
		UILabelBMFont * curLevel_BmFont = UILabelBMFont::create();
		curLevel_BmFont->setAnchorPoint(ccp(0.5f,0.5f));
		curLevel_BmFont->setFntFile("res_ui/font/ziti_3.fnt");
		curLevel_BmFont->setText(temp_.c_str());
		curLevel_BmFont->setPosition(ccp(winSize.width/2,winSize.height*0.8f-2));
		curLevel_BmFont->setScale(8.0f);
		layer_->addWidget(curLevel_BmFont);
		//curLevel_BmFont->runAction(CCScaleTo::create(0.3f,2.5f));

		CCAction* spawn_action = CCSpawn::create(
			CCFadeOut::create(0.25f),
			CCScaleTo::create(0.25f, 8.0f),
			NULL);

		CCSequence * seq_Font = CCSequence::create(
			CCScaleTo::create(0.3f,2.5f),
			CCDelayTime::create(1.4f),
			spawn_action,
			CCRemoveSelf::create(),
			NULL);

		curLevel_BmFont->runAction(seq_Font);

		CCSequence * seq_ = CCSequence::create(
			CCShow::create(),
			CCDelayTime::create(2.0f),
			CCRemoveSelf::create(),
			NULL);
		layer_->runAction(seq_);
	}

	m_singCopy_curLevel = level;
	m_singCopy_curMonsterNumber = curMon;
	m_singCopy_monsterNumbers = allMon;
	m_singCopy_remainMonster = remMon;
	//m_singCopy_useTime = useTime;
	m_singCopy_timeCount = timeCount;

	const char *strings_curLevelBmfont = StringDataManager::getString("singleCopy_curLevelLabelBmFont");
	char levelStr[50];
	sprintf(levelStr,strings_curLevelBmfont,m_singCopy_curLevel);
	curLevel_BmFont->setText(levelStr);

	char monsterString[10];
	sprintf(monsterString,"%d",m_singCopy_curMonsterNumber);
	char allMonsterString[10];
	sprintf(allMonsterString,"%d",m_singCopy_monsterNumbers);
	std::string temp_monsterString ="";
	temp_monsterString.append(monsterString);
	temp_monsterString.append("/");
	temp_monsterString.append(allMonsterString);
	m_curLevelShowValue->setStringValue(temp_monsterString.c_str());

	char remMonsterString[50];
	sprintf(remMonsterString,"%d",m_singCopy_remainMonster);
	label_monsterValue->setStringValue(remMonsterString);

	//RecuriteActionItem::timeFormatToString(m_singCopy_useTime/1000).c_str();
	allTimeShowValue->setText(RecuriteActionItem::timeFormatToString(m_singCopy_timeCount/1000).c_str());
	//singCopyTableview->updateCellAtIndex(0);
	//singCopyTableview->reloadData();
}

void MissionAndTeam::starRunUpdate()
{
	setSingCopy_useTime(0);
	schedule(schedule_selector(MissionAndTeam::updateSingCopyCurUseTime),1.0f);
}


void MissionAndTeam::updateSingCopyCurUseTime( float delta )
{
	m_singCopy_useTime++;
	m_useTimeValue->setText(RecuriteActionItem::timeFormatToString(m_singCopy_useTime).c_str());
}

void MissionAndTeam::setSingCopy_useTime( int time_ )
{
	m_singCopy_useTime = time_;
}

int MissionAndTeam::getSingCopy_useTime()
{
	return m_singCopy_useTime;
}

void MissionAndTeam::updateFamilyFightRemainTime(float delta )
{
	//in familyFight
	if (this->getPanelType() == MissionAndTeam::type_FamilyFightAndTeam)
	{
		long long tempdata = FamilyFightData::getRemainTime();
		if (tempdata >= 0)
		{
			tempdata -= 1000 ;
			FamilyFightData::setRemainTime(tempdata);

			CCLabelTTF * l_ff_remaintimeValue = (CCLabelTTF*)FamilyFightLayer->getChildByTag(KTag_FamilyFight_Label_RemainTime);
			if (l_ff_remaintimeValue)
			{
				l_ff_remaintimeValue->setString(RecuriteActionItem::timeFormatToString(FamilyFightData::getRemainTime()/1000).c_str());
			}
		}
	}
}

void MissionAndTeam::CheckDistanceInFamilyFight( float dt )
{
	if (GameView::getInstance()->myplayer)
	{
		CCPoint cp_role = GameView::getInstance()->myplayer->getWorldPosition();
		CCPoint cp_target = m_Target_pos_inFamilyFight;
		//if (sqrt(pow((cp_role.x-cp_target.x),2)+pow((cp_role.y-cp_target.y),2)) < 64)
		if (ccpDistance(cp_role,cp_target) < 64)
		{
			this->unschedule(schedule_selector(MissionAndTeam::CheckDistanceInFamilyFight));

			//began to kill monster
			GameView::getInstance()->myplayer->getMyPlayerAI()->startFight();

			if (GameView::getInstance()->getGameScene())
			{
				if (GameView::getInstance()->getMainUIScene())
				{
					GuideMap * guide_ =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
					if (guide_)
					{
						if (guide_->getBtnHangUp())
						{
							guide_->getBtnHangUp()->setTextures("gamescene_state/zhujiemian3/tubiao/off_hook.png","gamescene_state/zhujiemian3/tubiao/off_hook.png","");
						}
					}
				}
			}
		}
	}
}

void MissionAndTeam::updateOffLineArenaRemainTime( float delta )
{
	//in offLineArena
	if (this->getPanelType() == MissionAndTeam::type_OfflineArenaOnly)
	{
		if (m_nRemainTime_Arena >= 0)
		{
			m_nRemainTime_Arena-- ;

			CCLabelTTF * l_ofl_remaintimeValue = (CCLabelTTF*)OffLineArenaLayer->getChildByTag(KTag_OffLineArena_Label_RemainTime);
			if (l_ofl_remaintimeValue)
			{
				l_ofl_remaintimeValue->setString(RecuriteActionItem::timeFormatToString(m_nRemainTime_Arena).c_str());
			}
		}
		else
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(3021);
		}
		
	}
}

void MissionAndTeam::setArenaDataToDefault()
{
	m_nRemainTime_Arena = 600;
	p_enemyInfoInArena->generalId = -1;
	std::vector<GeneralInfoInArena*>::iterator iter_mine;
	for (iter_mine = generalsInArena_mine.begin(); iter_mine != generalsInArena_mine.end(); ++iter_mine)
	{
		delete *iter_mine;
	}
	generalsInArena_mine.clear();

	std::vector<GeneralInfoInArena*>::iterator iter_other;
	for (iter_other = generalsInArena_other.begin(); iter_other != generalsInArena_other.end(); ++iter_other)
	{
		delete *iter_other;
	}
	generalsInArena_other.clear();
}

void MissionAndTeam::callBackTeamInvite( CCObject * obj )
{
	CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();

	MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	FriendUi * friendUILayer = (FriendUi*)mainscene->getChildByTag(kTagFriendUi);

	if(friendUILayer == NULL)
	{
		FriendUi * friendui = FriendUi::create();
		friendui->ignoreAnchorPointForPosition(false);
		friendui->setAnchorPoint(ccp(0.5f,0.5f));
		friendui->setPosition(ccp(winsize.width/2,winsize.height/2));
		GameView::getInstance()->getMainUIScene()->addChild(friendui,0,kTagFriendUi);
		mainscene->remindGetFriendPhypower();

		friendui->friendType->setDefaultPanelByIndex(3);
		friendui->callBackChangeFriendType(friendui->friendType);
	}
}

CCTableView * MissionAndTeam::getLeftOffLineArenaTableView()
{
	CCTableView * temp = (CCTableView*)OffLineArenaLayer->getChildByTag(kTag_offLineArena_tableView_left);
	if (temp)
		return temp;

	return NULL;
}

CCTableView * MissionAndTeam::getRightOffLineArenaTableView()
{
	CCTableView * temp = (CCTableView*)OffLineArenaLayer->getChildByTag(kTag_offLineArena_tableView_right);
	if (temp)
		return temp;

	return NULL;
}

void MissionAndTeam::refreshTutorial()
{
	CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
	{
		if (!isOn)
		{
			tutorialIndicator->setVisible(false);
		}
		else
		{
			if (m_nTutorialIndicatorIndex == 1 || m_nTutorialIndicatorIndex == 2)  //任务的教学
			{
				if(getCurPresentPanel() != p_mission)
				{
					tutorialIndicator->setVisible(false);
				}
				else
				{
					tutorialIndicator->setVisible(true);
				}
			}
			else if (m_nTutorialIndicatorIndex == 3 ) //地图面板的教学
			{
				if(getCurPresentPanel() != p_fiveInstance)
				{
					tutorialIndicator->setVisible(false);
				}
				else
				{
					tutorialIndicator->setVisible(true);
				}
			}
		}
	}

	//指引任务的笔
	if (GameView::getInstance()->myplayer->getActiveRole()->level() <= K_GuideForMainMission_Level)
	{
		CCTutorialIndicator * guidePen = (CCTutorialIndicator*)this->getChildByTag(KTag_GuidePen);
		if (guidePen)
		{
			if (!isOn)
			{
				guidePen->setVisible(false);
			}
			else
			{
				if(getCurPresentPanel() != p_mission)
				{
					guidePen->setVisible(false);
				}
				else
				{
					guidePen->setVisible(true);
					guidePen->addHighLightFrameAction(170,60,ccp(-21,3),true);
					guidePen->addCenterAnm(170,60,ccp(-21,3));

					GameView::getInstance()->missionManager->isExistGuideForMmission = true;
				}
			}
		}
	}
}

void MissionAndTeam::ManageTeamEvent( CCObject *pSender )
{
	CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();

	MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	FriendUi * friendUILayer = (FriendUi*)mainscene->getChildByTag(kTagFriendUi);

	if(friendUILayer == NULL)
	{
		FriendUi * friendui = FriendUi::create();
		friendui->ignoreAnchorPointForPosition(false);
		friendui->setAnchorPoint(ccp(0.5f,0.5f));
		friendui->setPosition(ccp(winsize.width/2,winsize.height/2));
		GameView::getInstance()->getMainUIScene()->addChild(friendui,0,kTagFriendUi);
		mainscene->remindGetFriendPhypower();

		friendui->friendType->setDefaultPanelByIndex(3);
		friendui->callBackChangeFriendType(friendui->friendType);
	}
}

void MissionAndTeam::ExitTeamEvent( CCObject *pSender )
{
	int teamWork=3;
	int playerId=0;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1406,(void *)teamWork,(void *)playerId);
}

void MissionAndTeam::callBackSingCopy( CCObject * pSender )
{
	this->setStateIsRobot(true);
	this->schedule(schedule_selector(MissionAndTeam::updateSingCopyCheckMonst),1.0f);
}

void MissionAndTeam::updateSingCopyCheckMonst( float delta )
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

	if(GameView::getInstance()->myplayer->isAction(ACT_ATTACK))
		return;

	if (GameView::getInstance()->myplayer->selectEnemy(winSize.width))
	{
		if (GameView::getInstance()->myplayer->hasLockedActor()) 
		{
			GameActor* lockedActor = GameView::getInstance()->myplayer->getLockedActor();
			//same map
			CCPoint cp_role = GameView::getInstance()->myplayer->getWorldPosition();
			CCPoint cp_target = lockedActor->getWorldPosition();
			if (cp_target.x == 0 && cp_target.y == 0)
			{
				return;
			}

			MyPlayerCommandMove* cmd = new MyPlayerCommandMove();
			// NOT reachable, so find the near one
			short tileX = GameView::getInstance()->getGameScene()->positionToTileX(cp_target.x);
			short tileY = GameView::getInstance()->getGameScene()->positionToTileY(cp_target.y);
			if(GameView::getInstance()->getGameScene()->isLimitOnGround(tileX, tileY))
			{
				cp_target = GameView::getInstance()->missionManager->getNearestReachablePos(cp_target);
			}
			cmd->targetPosition = cp_target;
			cmd->method = MyPlayerCommandMove::method_searchpath;
			bool bSwitchCommandImmediately = true;
			if(GameView::getInstance()->myplayer->isAttacking())
				bSwitchCommandImmediately = false;
			GameView::getInstance()->myplayer->setNextCommand(cmd, bSwitchCommandImmediately);	

			DoThingWhenFinishedMove(cp_target);
		}
	}
	else
	{
		//req monster in instance
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1929);
	}
}

void MissionAndTeam::setStateIsRobot( bool value_ )
{
	m_state_start = value_;
}

bool MissionAndTeam::getStateIsRobot()
{
	return m_state_start;
}



