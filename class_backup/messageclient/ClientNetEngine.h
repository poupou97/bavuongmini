/* 
 * File:    ClientNetEngine.h
 * Author: gbk
 *
 * Created on 2013.09.05
 */

#ifndef ClientNetEngine_ClientNetEngine_h
#define ClientNetEngine_ClientNetEngine_h

#include "cocos2d.h"

USING_NS_CC;

#include <sys/types.h>  

//#if CC_TARGET_PLATFORM==CC_PLATFORM_WIN32
//#include <winsock2.h>
//#elif CC_TARGET_PLATFORM==CC_PLATFORM_ANDROID
//#include <sys/socket.h>
//#include <netinet/in.h>
//#include <netdb.h>
//#include <arpa/inet.h>
//#elif CC_TARGET_PLATFORM==CC_PLATFORM_IOS
//#include <sys/socket.h>
//#include <netinet/in.h>
//#include <netdb.h>
//#include <arpa/inet.h>
//#endif

#include <queue>
//#include <google/protobuf/io/coded_stream.h>
//#include <google/protobuf/io/zero_copy_stream_impl.h> 
#include "google/protobuf/io/coded_stream.h"
#include "google/protobuf/io/zero_copy_stream_impl.h"

//#if CC_TARGET_PLATFORM==CC_PLATFORM_WIN32
//#else
//#include "socketcc.h"
//#endif
#include "GameProtobuf.h"
#include <pthread.h>
#include "ODSocket.h"

using namespace std;
USING_NS_THREEKINGDOMS_PROTOCOL;

class MessageQueue
{
public:
	MessageQueue(); 
	~MessageQueue();

    CommonMessage popMessage();
    CommonMessage waitPopMessage();
    bool empty();
    int size();
    void push(CommonMessage msgBean);

	void exit();

private:
    queue<CommonMessage> msgQueue;
    pthread_mutex_t mu;
    pthread_cond_t cond;
};


class SocketEngineDelegateHelper;
class WsMessage;

class ClientNetEngine
{
public:
    /**
     *  socket state
     */
    enum State
    {
        kStateConnecting = 0,
        kStateOpen,
        kStateClosed
    };

	enum socket_callback_reasons {
		SE_CALLBACK_CLIENT_ESTABLISHED = 0,
		SE_CALLBACK_CLOSED,
	};
    
public:
    ClientNetEngine(const char *ip, int port);
    ~ClientNetEngine();

    /**
     *  @brief Errors in websocket
     */
    enum ErrorCode
    {
        kErrorTimeout = 0,
        kErrorConnectionFailure,
        kErrorUnknown
    };

	/**
     *  @brief The delegate class to process websocket events.
     *  @js NA
     *  @lua NA
     */
    class Delegate
    {
    public:
        virtual ~Delegate() {}

		virtual void onSocketOpen(ClientNetEngine* socketEngine) = 0;
		virtual void onSocketClose(ClientNetEngine* socketEngine) = 0;
		virtual void onSocketError(ClientNetEngine* ssocketEngines, const ClientNetEngine::ErrorCode& error) = 0;
    };

	bool init(const Delegate& delegate);

    void* connect(void* engine);
    void send(CommonMessage msgBean);

	/**
     *  @brief Closes the connection to server.
     */
    void close();

    CommonMessage  getMsg();
    void addMsg(CommonMessage bean);
    void printMessage(CommonMessage message);
	int getMsgSize();

	/**
     *  @brief Gets current state of connection.
     */
    State getReadyState();
    
    //inline TCPClientSocket* getSocket()
    //{
    //    return m_socket;
    //}
	inline ODSocket* getSocket()
    {
        return m_socket;
    }
    
    inline MessageQueue* getSendMsgs()
    {
        return sendMsgs;
    }

	void setAddress(const char* ip, int port);

	inline long long getLastRecieveTime() { return m_lastRecieveTime; };
	inline void setLastRecieveTime(double time) { m_lastRecieveTime = time; };

	inline long long getEstablishedTime() { return m_establishedTime; };
	inline void setEstablishedTime(double time) { m_establishedTime = time; };

    /** returns a shared instance of the director */
    static ClientNetEngine* sharedSocketEngine(void);

	static void* reciveData(void* engine);
	static void* sendData(void* engine);

private:
	int onSocketCallback(enum socket_callback_reasons reason);
	virtual void onUIThreadReceiveMessage(WsMessage* msg);
	void clear();

	void quitSubThread();
	inline bool isQuitSubThread() { return _needQuit; };

private:
    //TCPClientSocket* m_socket;
    //IPAddress address;
	ODSocket* m_socket;
	std::string address;
    int port;
    pthread_t pthead_rec;
    pthread_t pthead_send;
	pthread_t _subThreadInstance;
    MessageQueue *sendMsgs;
    
    queue<CommonMessage>* rcvMsgQueue;
    pthread_mutex_t mu;

	State        _readyState;

	friend class SocketEngineDelegateHelper;
    SocketEngineDelegateHelper* _delegateHelper;

	Delegate* _delegate;

	bool _needQuit;

	double m_lastRecieveTime;   // the system time for last reciveData()
	double m_establishedTime;   // the system time for connection established

};

#endif
