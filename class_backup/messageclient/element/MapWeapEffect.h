#ifndef _MAPWEAPEFFECT_ 
#define _MAPWEAPEFFECT_
#include <string>
#include <vector>

class MapWeapEffect
{
public:
	MapWeapEffect(void);
	~MapWeapEffect(void);

	void set_priority(int value_);
	int get_priority();

	void set_weapPression(int value_);
	int get_weapPression();

	void set_RefineLevel(int value_);
	int get_RefineLevel();

	void set_StarLevel(int value_);
	int get_starLevel();

	void set_weapEffectRefine(std::string value_);
	std::string get_weapEffectRefine();

	void set_weapEffectStar(std::string value_);
	std::string get_weapEffectStar();
private:
	int m_priority;
	int m_weapPression;
	int m_refineLevel;
	int m_starLevel;
	std::string m_weapEffectRefine;
	std::string m_weapEffectStar;
};
#endif;

