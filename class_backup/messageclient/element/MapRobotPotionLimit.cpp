#include "MapRobotPotionLimit.h"


MapRobotPotionLimit::MapRobotPotionLimit(void)
{
}


MapRobotPotionLimit::~MapRobotPotionLimit(void)
{
}

void MapRobotPotionLimit::set_id( std::string _vale )
{
	m_propId = _vale;
}

std::string MapRobotPotionLimit::get_id()
{
	return m_propId;
}

void MapRobotPotionLimit::set_level( int _value )
{
	m_level = _value;
}

int MapRobotPotionLimit::get_level()
{
	return m_level;
}

void MapRobotPotionLimit::set_price( int _value )
{
	m_price = _value;
}

int MapRobotPotionLimit::get_price()
{
	return m_price;
}

