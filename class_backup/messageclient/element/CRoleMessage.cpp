#include "CRoleMessage.h"


CRoleMessage::CRoleMessage(void)
	:m_longTimeFlag(0)
	,m_nRankType(0)
	,m_nPageCur(0)
	,m_nPageAll(0)
	,m_nPlayerMyRank(0)
	,m_longMyPlayerId(0)
	,m_longTimeRefreshFlag(0)
	,m_strMyPlayerName("")
{
}


CRoleMessage::~CRoleMessage(void)
{
}

void CRoleMessage::set_timeFlag( long long timeFlag )
{
	this->m_longTimeFlag = timeFlag;
}

long long CRoleMessage::get_timeFlag()
{
	return m_longTimeFlag;
}

void CRoleMessage::set_rankType( int nRankType )
{
	this->m_nRankType = nRankType;
}

int CRoleMessage::get_rankType()
{
	return m_nRankType;
}

void CRoleMessage::set_pageCur( int nPageCur )
{
	this->m_nPageCur = nPageCur;
}

int CRoleMessage::get_pageCur()
{
	return this->m_nPageCur;
}

void CRoleMessage::set_pageAll( int nPageAll )
{
	this->m_nPageAll = nPageAll;
}

int CRoleMessage::get_pageAll()
{
	return this->m_nPageAll;
}

void CRoleMessage::set_myPlayerRank( int nMyRank )
{
	this->m_nPlayerMyRank = nMyRank;
}

int CRoleMessage::get_myPlayerRank()
{
	return this->m_nPlayerMyRank;
}

void CRoleMessage::set_myPlayerId( long long longMyPlayerId )
{
	this->m_longMyPlayerId = longMyPlayerId;
}

long long CRoleMessage::get_myPlayerId()
{
	return this->m_longMyPlayerId;
}

void CRoleMessage::set_timeRefreshFlag(long long longTimeRefreshFlag)
{
	this->m_longTimeRefreshFlag = longTimeRefreshFlag;
}

long long CRoleMessage::get_timeRefreshFlag()
{
	return this->m_longTimeRefreshFlag;
}

void CRoleMessage::set_myPlayerName( std::string strMyPlayerName )
{
	this->m_strMyPlayerName = strMyPlayerName;
}

std::string CRoleMessage::get_myPlayerName()
{
	return this->m_strMyPlayerName;
}

void CRoleMessage::set_flowerType( int nFlowerType )
{
	this->m_nFlowerType = nFlowerType;
}

int CRoleMessage::get_flowerType()
{
	return this->m_nFlowerType;
}
