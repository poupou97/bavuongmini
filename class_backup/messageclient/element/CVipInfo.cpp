#include "CVipInfo.h"


CVipInfo::CVipInfo()
{
}


CVipInfo::~CVipInfo()
{
}

int CVipInfo::get_vip_level()
{
	return vip_level;
}

void CVipInfo::set_vip_level( int _value )
{
	vip_level = _value;
}

std::string CVipInfo::get_type()
{
	return type;
}

void CVipInfo::set_type( std::string _value )
{
	type = _value;
}

int CVipInfo::get_type_id()
{
	return type_id;
}

void CVipInfo::set_type_id( int _value )
{
	type_id = _value;
}

int CVipInfo::get_add_type()
{
	return add_type;
}

void CVipInfo::set_add_type( int _value )
{
	add_type = _value;
}

long long CVipInfo::get_add_number()
{
	return add_number;
}

void CVipInfo::set_add_number( long long _value )
{
	add_number = _value;
}
