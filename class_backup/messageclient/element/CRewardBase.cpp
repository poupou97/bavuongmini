#include "CRewardBase.h"
#include "../../utils/GameUtils.h"
#include "../../utils/StaticDataManager.h"

CRewardBase::CRewardBase()
{
}

CRewardBase::~CRewardBase()
{
}

void CRewardBase::setId( int id )
{
	m_id = id;
}

int CRewardBase::getId()
{
	return m_id;
}

void CRewardBase::setName( std::string name )
{
	m_name = name;
}

std::string CRewardBase::getName()
{
	return m_name;
}

void CRewardBase::setIcon( std::string icon )
{
	m_icon = icon;
}

std::string CRewardBase::getIcon()
{
	return m_icon;
}

void CRewardBase::setDes( std::string des )
{
	m_des = des;
}

std::string CRewardBase::getDes()
{
	return m_des;
}

void CRewardBase::setClose( int value_ )
{
	m_isClose = value_;
}

int CRewardBase::getClose()
{
	return m_isClose;
}

void CRewardBase::setRewardState( int value_ )
{
	m_state = value_;
}

int CRewardBase::getRewardState()
{
	return m_state;
}
