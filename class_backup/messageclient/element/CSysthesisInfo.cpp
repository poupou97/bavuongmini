#include "CSysthesisInfo.h"


CSysthesisInfo::CSysthesisInfo(void)
{
}


CSysthesisInfo::~CSysthesisInfo(void)
{
}

void CSysthesisInfo::set_id( int _value )
{
	id = _value;
}

int CSysthesisInfo::get_id()
{
	return id;
}

void CSysthesisInfo::set_merge_prop_id( std::string _value )
{
	merge_prop_id = _value;
}

std::string CSysthesisInfo::get_merge_prop_id()
{
	return merge_prop_id;
}

void CSysthesisInfo::set_output_prop_id( std::string _value )
{
	output_prop_id = _value;
}

std::string CSysthesisInfo::get_output_prop_id()
{
	return output_prop_id;
}

void CSysthesisInfo::set_need_num( int _value )
{
	need_num = _value;
}

int CSysthesisInfo::get_need_num()
{
	return need_num;
}

void CSysthesisInfo::set_need_price( int _value )
{
	need_price = _value;
}

int CSysthesisInfo::get_need_price()
{
	return need_price;
}

void CSysthesisInfo::set_output_num( int _value )
{
	output_num = _value;
}

int CSysthesisInfo::get_output_num()
{
	return output_num;
}

void CSysthesisInfo::set_merge_prop_name( std::string _value )
{
	merge_prop_name = _value;
}

std::string CSysthesisInfo::get_merge_prop_name()
{
	return merge_prop_name;
}

void CSysthesisInfo::set_merge_prop_icon( std::string _value )
{
	merge_prop_icon = _value;
}

std::string CSysthesisInfo::get_merge_prop_icon()
{
	return merge_prop_icon;
}

void CSysthesisInfo::set_merge_prop_quality( int _value )
{
	merge_prop_quality = _value;
}

int CSysthesisInfo::get_merge_prop_quality()
{
	return merge_prop_quality;
}

void CSysthesisInfo::set_output_prop_name( std::string _value )
{
	output_prop_name = _value;
}

std::string CSysthesisInfo::get_output_prop_name()
{
	return output_prop_name;
}

void CSysthesisInfo::set_output_prop_icon( std::string _value )
{
	output_prop_icon = _value;
}

std::string CSysthesisInfo::get_output_prop_icon()
{
	return output_prop_icon;
}

void CSysthesisInfo::set_output_prop_quality( int _value )
{
	output_prop_quality = _value;
}

int CSysthesisInfo::get_output_prop_quality()
{
	return output_prop_quality;
}


void CSysthesisInfo::CopyFrom( CSysthesisInfo* info )
{
	this->set_id(info->get_id());
	this->set_merge_prop_id(info->get_merge_prop_id());
	this->set_output_prop_id(info->get_output_prop_id());
	this->set_need_num(info->get_need_num());
	this->set_need_price(info->get_need_price());
	this->set_output_num(info->get_output_num());
	this->set_merge_prop_name(info->get_merge_prop_name());
	this->set_merge_prop_icon(info->get_merge_prop_icon());
	this->set_merge_prop_quality(info->get_merge_prop_quality());
	this->set_output_prop_name(info->get_output_prop_name());
	this->set_output_prop_icon(info->get_output_prop_icon());
	this->set_output_prop_quality(info->get_output_prop_quality());
}

void CSysthesisInfo::set_typename( std::string _value )
{
	m_goodsName = _value;
}

std::string CSysthesisInfo::get_typename()
{
	return m_goodsName;
}
