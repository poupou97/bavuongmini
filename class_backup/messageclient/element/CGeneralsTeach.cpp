#include "CGeneralsTeach.h"


CGeneralsTeach::CGeneralsTeach()
{
}


CGeneralsTeach::~CGeneralsTeach()
{
}

int CGeneralsTeach::get_quality_level()
{
	return quality_level;
}

void CGeneralsTeach::set_quality_level( int _value )
{
	quality_level = _value;
}

int CGeneralsTeach::get_need_exp()
{
	return need_exp;
}

void CGeneralsTeach::set_need_exp( int _value )
{
	need_exp = _value;
}
