
#ifndef _MESSAGECLIENT_CGENERALBASEMSG_H_
#define _MESSAGECLIENT_CGENERALBASEMSG_H_

#include "../GameProtobuf.h"
#include "../protobuf/GeneralMessage.pb.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class CGeneralBaseMsg : public GeneralBaseMsg
{
public:
	CGeneralBaseMsg();
	virtual ~CGeneralBaseMsg();

	/** check if the general is onLine */
	static bool isOnLine(long long generalid);

	/**
	   * 冷却时间相关
	   */
	void updateCD();
	void startCD();
	bool isInCD();

	void set_modelID(int _value);
	int get_modelId();
	void set_description(std::string _value);
	std::string get_description();
	void set_profession(int _value);
	int get_profession();
	void set_avatar(std::string _value);
	std::string get_avatar();
	void set_half_photo(std::string _value);
	std::string get_half_photo();
	void set_head_photo(std::string _value);
	std::string get_head_photo();

	void set_changeMoment(long long _value);
	long long get_changeMoment();

	long long getRemainTime();

// 	void set_LeftTime(long long _value);
// 	long long get_LeftTime();

private:
	int modelID;
	std::string description;
	int profession;
	std::string avatar;
	std::string half_photo;
	std::string head_photo;

private:
	long long m_changeMoment;

	bool m_bIsInCD;
	long long coolTimeCount;   // 冷却启动时的系统时间(ms)
	long long remainTime;

/*	long long m_nLeftTime;*/
};


////////////////////////////////////////////////////////

/**
  * 负责管理武将状态
  * 目前主要负责倒计时
  */
class GeneralsStateManager
{
public:
	GeneralsStateManager();
	~GeneralsStateManager();

	static GeneralsStateManager* getInstance();

	void update(float dt);

	CGeneralBaseMsg * getGeneralBaseMsgById(long long generalId);
};

#endif

