
#ifndef _GEM_PROPERTY_H
#define _GEM_PROPERTY_H

#include "../GameProtobuf.h"

#include "cocos2d.h"
USING_NS_THREEKINGDOMS_PROTOCOL;
class CGemPropertyMap
{
public:
	CGemPropertyMap();
	~CGemPropertyMap();

	void set_propId(std::string vale_);
	std::string get_propId();

	void set_propClazz(int value_);
	int get_propClazz();


private:
	std::string m_gemId;
	int m_gemClazz;
};
#endif
