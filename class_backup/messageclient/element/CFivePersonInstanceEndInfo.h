#ifndef _MESSAGECLIENT_CFIVEPERSIONINSTANCEENDINFO_H_
#define _MESSAGECLIENT_CFIVEPERSIONINSTANCEENDINFO_H_

#include "../GameProtobuf.h"

class CRewardProp;

class CFivePersonInstanceEndInfo
{
public:
	CFivePersonInstanceEndInfo();
	~CFivePersonInstanceEndInfo();

	void set_instanceId(int _value);
	int get_instanceId();
	void set_star(int _value);
	int get_star();
	void set_exp(int _value);
	int get_exp();
	void set_goldType(int _value);
	int get_goldType();
	void set_gold(int _value);
	int get_gold();
	void set_baseExp(int _value);
	int get_baseExp();
	void set_baseGold(int _value);
	int get_baseGold();
	void set_timeAdd(int _value);
	int get_timeAdd();
	void set_skillAdd(int _value);
	int get_skillAdd();
	void set_vipAdd(int _value);
	int get_vipAdd();
	void set_dragonValue(int _value);
	int get_dragonValue();
	void set_timeDouble(int _value);
	int get_timeDouble();

	void copyFrom(CFivePersonInstanceEndInfo* info);

private:
	int instanceId;
	int star;
	int exp;
	int goldType;
	int gold;
	int baseExp;
	int baseGold;
	int timeAdd;
	int skillAdd;
	int vipAdd;
	int dragonValue;
	int timeDouble;

public:
	std::vector<CRewardProp *> rewardProps;
};

#endif

