
#ifndef _MESSAGECLIENT_CNPCDIALOG_H_
#define _MESSAGECLIENT_CNPCDIALOG_H_

#include "../protobuf/NPCMessage.pb.h"
#include "../GameProtobuf.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class CNpcDialog : public NpcDialog
{
public:
	CNpcDialog();
	~CNpcDialog();
};
#endif;