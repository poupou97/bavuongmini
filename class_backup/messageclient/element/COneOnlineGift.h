#ifndef _MESSAGECLIENT_CONELINEGIFT_H_
#define _MESSAGECLIENT_CONELINEGIFT_H_

#include "../GameProtobuf.h"
#include "../protobuf/DailyGift.pb.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class COneOnlineGift:public OneOnlineGift
{
public:
	COneOnlineGift(void);
	~COneOnlineGift(void);
};
#endif;
