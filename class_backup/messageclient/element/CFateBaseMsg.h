
#ifndef _MESSAGECLIENT_CFATEBASEMSG_H_
#define _MESSAGECLIENT_CFATEBASEMSG_H_

#include <string>

class CFateBaseMsg
{
public:
	CFateBaseMsg();
	~CFateBaseMsg();

	std::string get_fateid();
	void set_fateid(std::string _value);
	std::string get_fatename();
	void set_fatename(std::string _value);
	int get_fatetype();
	void set_fatetype(int _value);
	int get_property();
	void set_property(int _value);
	int get_valuetype();
	void set_valuetype(int _value);
	float get_addvalue();
	void set_addvalue(float _value);
	std::string get_extendld1();
	void set_extendld1(std::string _value);
	std::string get_extendld2();
	void set_extendld2(std::string _value);
	std::string get_extendld3();
	void set_extendld3(std::string _value);
	std::string get_extendld4();
	void set_extendld4(std::string _value);
	std::string get_extendld5();
	void set_extendld5(std::string _value);
	std::string get_description();
	void set_description(std::string _value);

private:
	std::string fateid;
	std::string fatename;
	int fatetype;
	int property;
	int valuetype;
	float addvalue;
	std::string extendld1;
	std::string extendld2;
	std::string extendld3;
	std::string extendld4;
	std::string extendld5;
	std::string description;
};

#endif