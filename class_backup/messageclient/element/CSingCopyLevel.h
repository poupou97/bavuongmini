
#ifndef _SINGCOPYLEVEL_LEVEL_H
#define _SINGCOPYLEVEL_LEVEL_H

#include "../GameProtobuf.h"

#include "cocos2d.h"
USING_NS_THREEKINGDOMS_PROTOCOL;
class CSingCopyLevel
{
public:

	CSingCopyLevel();
	~CSingCopyLevel();

	void set_clazz(int value_);
	int get_clazz();

	void set_level(int value_);
	int get_level();

private:
	int m_clazz;
	int m_level;
};
#endif
