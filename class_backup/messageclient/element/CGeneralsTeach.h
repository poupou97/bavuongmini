#ifndef _MESSAGECLIENT_CGENERALSTEACH_H_
#define _MESSAGECLIENT_CGENERALSTEACH_H_

class CGeneralsTeach
{
public:
	CGeneralsTeach();
	~CGeneralsTeach();

	int get_quality_level();
	void set_quality_level(int _value);
	int get_need_exp();
	void set_need_exp(int _value);

private:
	int quality_level;
	int need_exp;
};

#endif