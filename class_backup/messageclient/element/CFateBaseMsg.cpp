#include "CFateBaseMsg.h"


CFateBaseMsg::CFateBaseMsg()
{
}


CFateBaseMsg::~CFateBaseMsg()
{
}

std::string CFateBaseMsg::get_fateid()
{
	return fateid;
}

void CFateBaseMsg::set_fateid(std::string _value)
{
	fateid = _value;
}

std::string CFateBaseMsg::get_fatename()
{
	return fatename;
}

void CFateBaseMsg::set_fatename( std::string _value )
{
	fatename = _value;
}

int CFateBaseMsg::get_fatetype()
{
	return fatetype;
}

void CFateBaseMsg::set_fatetype( int _value )
{
	fatetype = _value;
}

int CFateBaseMsg::get_property()
{
	return property;
}

void CFateBaseMsg::set_property( int _value )
{
	property = _value;
}

int CFateBaseMsg::get_valuetype()
{
	return valuetype ;
}

void CFateBaseMsg::set_valuetype( int _value )
{
	valuetype = _value;
}

float CFateBaseMsg::get_addvalue()
{
	return addvalue;
}

void CFateBaseMsg::set_addvalue( float _value )
{
	addvalue = _value;
}

std::string CFateBaseMsg::get_extendld1()
{
	return extendld1;
}

void CFateBaseMsg::set_extendld1( std::string _value )
{
	extendld1 = _value;
}

std::string CFateBaseMsg::get_extendld2()
{
	return extendld2;
}

void CFateBaseMsg::set_extendld2( std::string _value )
{
	extendld2 = _value;
}

std::string CFateBaseMsg::get_extendld3()
{
	return extendld3;
}

void CFateBaseMsg::set_extendld3( std::string _value )
{
	extendld3 = _value;
}

std::string CFateBaseMsg::get_extendld4()
{
	return extendld4;
}

void CFateBaseMsg::set_extendld4( std::string _value )
{
	extendld4 = _value;
}

std::string CFateBaseMsg::get_extendld5()
{
	return extendld5;
}

void CFateBaseMsg::set_extendld5( std::string _value )
{
	extendld5 = _value;
}

std::string CFateBaseMsg::get_description()
{
	return description;
}

void CFateBaseMsg::set_description( std::string _value )
{
	description = _value;
}
