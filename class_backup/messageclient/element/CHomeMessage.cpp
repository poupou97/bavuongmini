#include "CHomeMessage.h"


CHomeMessage::CHomeMessage(void)
	:m_longTimeFlag(0)
{
}


CHomeMessage::~CHomeMessage(void)
{
}

void CHomeMessage::set_timeFlag( long long timeFlag )
{
	this->m_longTimeFlag = timeFlag;
}

long long CHomeMessage::get_timeFlag()
{
	return this->m_longTimeFlag;
}

void CHomeMessage::set_pageCur( int nPageCur )
{
	this->m_nPageCur = nPageCur;
}

int CHomeMessage::get_pageCur()
{
	return this->m_nPageCur;
}

void CHomeMessage::set_pageAll( int nPageAll )
{
	this->m_nPageAll = nPageAll;
}

int CHomeMessage::get_pageAll()
{
	return this->m_nPageAll;
}

void CHomeMessage::set_myRank( int nMyRank )
{
	this->m_nMyRank = nMyRank;
}

int CHomeMessage::get_myRank()
{
	return this->m_nMyRank;
}

void CHomeMessage::set_timeRefreshFlag( long long timeRefreshFlag )
{
	this->m_longTimeRefreshFlag = timeRefreshFlag;
}

long long CHomeMessage::get_timeRefreshFlag()
{
	return this->m_longTimeRefreshFlag;
}

//void CHomeMessage::set_createrName( std::string strCreaterName )
//{
//	this->m_strCreaterName = strCreaterName;
//}
//
//std::string CHomeMessage::get_createrName()
//{
//	return this->m_strCreaterName;
//}
//
//void CHomeMessage::set_announcement( std::string strAnnouncement )
//{
//	this->m_strAnnouncement = strAnnouncement;
//}
//
//std::string CHomeMessage::get_announcement()
//{
//	return this->m_strAnnouncement;
//}
