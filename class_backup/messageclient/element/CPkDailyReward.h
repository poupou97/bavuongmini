
#ifndef _MESSAGECLIENT_CPKDAILYREWARD_H_
#define _MESSAGECLIENT_CPKDAILYREWARD_H_

#include "../protobuf/PlayerMessage.pb.h"
#include "../GameProtobuf.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class CPkDailyReward: public PkDailyReward 
{
public:
    CPkDailyReward() ;
    virtual ~CPkDailyReward() ;
} ;

#endif
