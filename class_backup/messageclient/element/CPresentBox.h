#ifndef _MESSAGECLIENT_CPRESENTBOX_H_
#define _MESSAGECLIENT_CPRESENTBOX_H_


class CPresentBox
{
public:
    CPresentBox() ;
    ~CPresentBox() ;

private:
    long long m_instanceId;
    
public:
    void set_id(long long instanceId);
    long long get_id();
    

} ;

#endif
