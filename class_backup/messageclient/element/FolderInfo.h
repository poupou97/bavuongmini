#ifndef _MESSAGECLIENT_FOLDERINFO_H_
#define _MESSAGECLIENT_FOLDERINFO_H_

#include "../protobuf/ModelMessage.pb.h"
#include "../GameProtobuf.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class FolderInfo : public Folder
{
public:
	FolderInfo();
	 virtual ~FolderInfo();
};
#endif;

