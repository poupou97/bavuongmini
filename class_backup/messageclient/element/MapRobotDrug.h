#ifndef _MESSAGECLIENT_ROBOTDRUG_
#define _MESSAGECLIENT_ROBOTDRUG_

#include <string>
#include <vector>

class MapRobotDrug
{
public:
	MapRobotDrug(void);
	~MapRobotDrug(void);

	void set_id(std::string _vale);
	std::string get_id();

	void set_name(std::string _vale);
	std::string get_name();

	void set_level(int _vale);
	int get_level();

	void set_type(int _vale);
	int get_type();

	void set_target(int _vale);
	int get_target();

	void set_icon(std::string _vale);
	std::string  get_icon();
private:
	std::string m_drugId;
	std::string m_drugName;
	int m_level;
	int m_type;
	int m_target;
	std::string m_icon;
};

#endif
