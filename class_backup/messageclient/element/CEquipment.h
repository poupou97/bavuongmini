#ifndef _MESSAGECLIENT_CEquipment_H_
#define _MESSAGECLIENT_CEquipment_H_

#include "../protobuf/ModelMessage.pb.h"

#include "../GameProtobuf.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class CEquipment: public Equipment
{
public:
	/**
	 * 身上部位 
	 * 头 0, 颈 1, 身 2, 腰 3, 手 4, 左腕 5, 右腕 6,
	 * 左指 7, 右指 8, 足 9, 背 10, 胸 11，披风 12， 没装备 100;
	 */
	enum BodyPart {
		HEAD = 0,
		NECK,
		BODY,
		WAIST,
		HAND,
		WRIST_LEFT,
		WRIST_RIGHT,
		FINGER_LEFT,
		FINGER_RIGHT,
		FEET,
		WING,
		HONOR,
		SHOULDER,   // 骑乘用的披风
	};

public:
	CEquipment() ;
	virtual ~CEquipment() ;

public:


} ;

#endif
