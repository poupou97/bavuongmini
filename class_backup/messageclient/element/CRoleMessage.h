#ifndef _MESSAGECLIENT_CROLEMESSAGE_H_
#define _MESSAGECLIENT_CROLEMESSAGE_H_

#include "../GameProtobuf.h"
#include "../protobuf/DailyMessage.pb.h"


USING_NS_THREEKINGDOMS_PROTOCOL;

class CRoleMessage:public RoleMessage
{
public:
	CRoleMessage(void);
	~CRoleMessage(void);

private:
	long long m_longTimeFlag;
	int m_nRankType;																// 0:等级;1:战斗力
	int m_nPageCur;
	int m_nPageAll;
	int m_nPlayerMyRank;
	long long m_longMyPlayerId;
	long long m_longTimeRefreshFlag;
	std::string m_strMyPlayerName;
	int m_nFlowerType;																// 1.今日收花、2.今日送花、3.历史收花、4.历史送花

public:
	void set_timeFlag(long long timeFlag);
	long long get_timeFlag();

	void set_rankType(int nRankType);
	int get_rankType();

	void set_pageCur(int nPageCur);
	int get_pageCur();

	void set_pageAll(int nPageAll);
	int get_pageAll();

	void set_myPlayerRank(int nMyRank);
	int get_myPlayerRank();

	void set_myPlayerId(long long longMyPlayerId);
	long long get_myPlayerId();

	void set_timeRefreshFlag(long long longTimeRefreshFlag);
	long long get_timeRefreshFlag();

	void set_myPlayerName(std::string strMyPlayerName);
	std::string get_myPlayerName();

	void set_flowerType(int nFlowerType);
	int get_flowerType();
};
#endif;
