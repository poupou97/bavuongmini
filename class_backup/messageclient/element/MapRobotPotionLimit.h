#ifndef _MESSAGECLIENT_ROBOTPOTIONMILIT_
#define _MESSAGECLIENT_ROBOTPOTIONMILIT_

#include <string>
#include <vector>

class MapRobotPotionLimit
{
public:
	MapRobotPotionLimit(void);
	~MapRobotPotionLimit(void);

	void set_id(std::string _value);
	std::string get_id();

	void set_level(int _value);
	int get_level();

	void set_price(int _value);
	int get_price();

private:
	std::string m_propId;
	int m_level;
	int m_price;
};

#endif
