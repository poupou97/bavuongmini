#ifndef _MESSAGECLIENT_CACTIVENOTE_H_
#define _MESSAGECLIENT_CACTIVENOTE_H_

#include "../GameProtobuf.h"
#include "../protobuf/ActiveMessage.pb.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class CActiveNote:public Note
{
public:
	CActiveNote(void);
	~CActiveNote(void);
};
#endif;
