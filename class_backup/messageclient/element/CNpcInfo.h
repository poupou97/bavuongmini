
#ifndef _MESSAGECLIENT_CNPCINFO_H_
#define _MESSAGECLIENT_CNPCINFO_H_

#include <string>

class CNpcInfo {
public:
	CNpcInfo() ;
    virtual ~CNpcInfo() ;

public:
	/** id 唯一标示符 */
	long long npcId;

	/** npc名称 */
	std::string npcName;

	/** 1排行npc 0普通npc */
	char normal;

	/** 介绍 */
	std::string intro;

	/** 头像 */
	std::string icon;

	/**  */
	std::string welcome;

	/**  */
	std::string des;

	/** 使用模式 */
	char byteValue;

	/** Mapicon */
	std::string mapIcon;
};

#endif