
#ifndef Blog_C___Reflection_PushHandler1305_h
#define Blog_C___Reflection_PushHandler1305_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler1305 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1305)

public:
	SYNTHESIZE(PushHandler1305, int*, m_pValue)

		PushHandler1305() ;
	virtual ~PushHandler1305() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

private:

protected:
	int *m_pValue ;

} ;

#endif
