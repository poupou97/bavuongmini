
#ifndef Blog_C___Reflection_PushHandler1210_h
#define Blog_C___Reflection_PushHandler1210_h

#include "PushHandlerProtocol.h"
#include "../../common/CKBaseClass.h"

class PushHandler1210 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1210)

public:
	SYNTHESIZE(PushHandler1210, int*, m_pValue)

	PushHandler1210() ;
	virtual ~PushHandler1210() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

private:
	std::string generateExperienceAlertString(const char* stringId, int exp);

} ;

#endif
