
#ifndef Blog_C___Reflection_PushHandler1207_h
#define Blog_C___Reflection_PushHandler1207_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler1207 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1207)
    
public:
    SYNTHESIZE(PushHandler1207, int*, m_pValue)
    
    PushHandler1207() ;
    virtual ~PushHandler1207() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;

} ;

#endif
