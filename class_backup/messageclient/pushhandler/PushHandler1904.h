
#ifndef Blog_C___Reflection_PushHandler1904_h
#define Blog_C___Reflection_PushHandler1904_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

#include "cocos2d.h"
USING_NS_CC;

class PushHandler1904 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1904)
    
public:
    SYNTHESIZE(PushHandler1904, int*, m_pValue)
    
    PushHandler1904() ;
    virtual ~PushHandler1904() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;

} ;

#endif
