#include "PushHandler2801.h"
#include "../protobuf/QuestionMessage.pb.h"
#include "../../ui/Question_ui/QuestionData.h"

#define  PAGE_NUM 10

IMPLEMENT_CLASS(PushHandler2801)

PushHandler2801::PushHandler2801() 
{

}
PushHandler2801::~PushHandler2801() 
{
	
}
void* PushHandler2801::createInstance()
{
	return new PushHandler2801() ;
}
void PushHandler2801::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler2801::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler2801::handle(CommonMessage* mb)
{
	Push2801 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	int nAllQuestion = bean.everydaytimes();										// 每日答题次数
	int nHasAnswerQuestion = bean.answertimes();							// 已回答次数
	int nRightAnswerQuestion = bean.right();										// 已经答对的题目数目
	int nCardNum = bean.rewardnumber();											// 可翻牌的次数

	QuestionData::instance()->set_allQuestion(nAllQuestion);
	QuestionData::instance()->set_hasAnswerQuestion(nHasAnswerQuestion);
	QuestionData::instance()->set_rightAnswerQuestion(nRightAnswerQuestion);
	QuestionData::instance()->set_cardNum(nCardNum);

}