
#include "PushHandler1546.h"
#include "../protobuf/GuildMessage.pb.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/family_ui/familyFight_ui/FamilyFightData.h"
#include "../../ui/family_ui/FamilyUI.h"
#include "../../ui/family_ui/familyFight_ui/FamilyFightUI.h"
#include "../../ui/family_ui/familyFight_ui/FamilyFightRewardsUI.h"
#include "../../ui/GameUIConstant.h"
#include "../../ui/Reward_ui/RewardUi.h"

IMPLEMENT_CLASS(PushHandler1546)

PushHandler1546::PushHandler1546() 
{

}
PushHandler1546::~PushHandler1546() 
{

}
void* PushHandler1546::createInstance()
{
	return new PushHandler1546() ;
}
void PushHandler1546::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1546::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1546::handle(CommonMessage* mb)
{
	Push1546 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	// 家族排名
	FamilyFightData::getInstance()->setFamilyFightRanking(bean.guildranking());
	//奖励领取状态
	FamilyFightData::getInstance()->setFamilyRewardStatus(bean.rewardstatus());

	FamilyFightRewardsUI * familyFightRewardUI = (FamilyFightRewardsUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyFightRewardUI);
	if (familyFightRewardUI)
	{
		familyFightRewardUI->ReloadTableViewWithOutChangeOffSet();
	}

	if (bean.rewardstatus() == true)
	{
		if (GameView::getInstance()->getMainUIScene())
		{
			FamilyUI *familyui = (FamilyUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
			if (familyui)
			{
				FamilyFightUI * familyFightUI = (FamilyFightUI*)familyui->GetFamilyFightUI();
				if (familyFightUI)
				{
					familyFightUI->RemoveParticleForBtnGetReward();
				}
			}
		}

		RewardUi::removeRewardListEvent(REWARD_LIST_ID_FAMILYFIGHT);
	}
}
