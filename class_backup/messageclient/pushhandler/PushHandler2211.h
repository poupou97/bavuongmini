
#ifndef Blog_C___Reflection_PushHandler2211_h
#define Blog_C___Reflection_PushHandler2211_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"
#include "cocos2d.h"

USING_NS_CC;

class PushHandler2211 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler2211)
    
public:
    SYNTHESIZE(PushHandler2211, int*, m_pValue)
    
    PushHandler2211() ;
    virtual ~PushHandler2211() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
