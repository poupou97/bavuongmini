#include "PushHandler5110.h"

#include "../element/COneOnlineGift.h"
#include "../element/GoodsInfo.h"
#include "GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/OnlineReward_ui/OnLineRewardUI.h"
#include "../protobuf/DailyGift.pb.h"
#include "../../ui/OnlineReward_ui/OnlineGiftData.h"
#include "../../ui/OnlineReward_ui/OnlineGiftIconWidget.h"
#include "../../gamescene_state/sceneelement/GuideMap.h"
#include "../../ui/OnlineReward_ui/OnlineGiftLayer.h"


IMPLEMENT_CLASS(PushHandler5110)

	PushHandler5110::PushHandler5110() 
{

}
PushHandler5110::~PushHandler5110() 
{
	
}
void* PushHandler5110::createInstance()
{
	return new PushHandler5110() ;
}
void PushHandler5110::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5110::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5110::handle(CommonMessage* mb)
{
	Rsp5110 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	// 清空数据
	std::vector<COneOnlineGift *>::iterator iter;
	for (iter = OnlineGiftData::instance()->m_vector_oneOnLineGift.begin(); iter != OnlineGiftData::instance()->m_vector_oneOnLineGift.end(); iter++)
	{
		delete *iter;
	}
	OnlineGiftData::instance()->m_vector_oneOnLineGift.clear();


	// 初始化 保存服务器的数据
	int size = bean.auctioninfos_size();
	for (int i = 0; i<size; i++)
	{
		COneOnlineGift * dayGift =new COneOnlineGift();
		dayGift->CopyFrom(bean.auctioninfos(i));

		OnlineGiftData::instance()->m_vector_oneOnLineGift.push_back(dayGift);
	}

	OnlineGiftData::instance()->initDataFromIntent();

	if (NULL != GameView::getInstance()->getGameScene())
	{
		GuideMap * pGuideMap = (GuideMap *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
		if (NULL != pGuideMap)
		{
			// 判断在线奖励icon是否存在；如果icon窗体不存在，说明：（1）等级超过上限，不会再有在线奖励（2）之前的所有奖励已领取（3）用户刚上线
			// 如果存在的话，说明 之前的奖励没有领取完
			if (pGuideMap->getActionLayer())
			{
				OnlineGiftIconWidget* pOnlineGiftIconWidget = (OnlineGiftIconWidget *)pGuideMap->getActionLayer()->getWidgetByTag(kTagOnLineGiftIcon);
				if (NULL != pOnlineGiftIconWidget)
				{
					// 如果服务器中数据所有领取 都为 已领取，说明 等级超过上限 将现有的icon窗体关闭；否则 更新窗体（icon窗体 和 UI窗体）
					if (OnlineGiftData::instance()->isAllGiftHasGet())
					{
						pOnlineGiftIconWidget->allGiftGetIconExit();
					}
					else	
					{
						// icon窗体
						pOnlineGiftIconWidget->initDataFromIntent();

						// 更新UI窗体
						OnLineRewardUI * pOnLineRewardUI = (OnLineRewardUI *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOnLineGiftUI);
						if (NULL != pOnLineRewardUI)
						{
							pOnLineRewardUI->refreshData();
							pOnLineRewardUI->refreshTimeAndNum();					// 更新 奖励编号 和 时间
							pOnLineRewardUI->update(0);
						}
					}
				}
			}
		}

		OnlineGiftLayer * pOnlineGiftLayer = (OnlineGiftLayer *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOnLineGiftLayer);
		if (NULL != pOnlineGiftLayer)
		{
			pOnlineGiftLayer->initDataFromIntent();
		}
	}




	//if (NULL !=  GameView::getInstance()->getGameScene())
	//{
	//	// 判断在线奖励icon是否存在;如果icon窗体不存在，说明：（1）等级超过上限，不会再有 在线奖励（2）之前的所有奖励已领取（3）用户刚上线
	//	OnlineGiftIconWidget * pOnlineGiftIconWidget = (OnlineGiftIconWidget *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOnLineGiftIcon);
	//	if (NULL == pOnlineGiftIconWidget)
	//	{
	//		// 如果服务器中数据所有领取 都为 已领取，说明 等级超过上限 不再创建icon窗体
	//		if (OnlineGiftData::instance()->isAllGiftHasGet())
	//		{
	//			// 不再创建窗体
	//		}
	//		else
	//		{
	//			//创建窗体（此时应为 24点更新奖励，且没有超过等级上限）（或者 用户刚上线）

	//			if (NULL != GameView::getInstance()->getMainUIScene())
	//			{
	//				OnlineGiftIconWidget * pOnlineGiftIcon = (OnlineGiftIconWidget *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOnLineGiftIcon);
	//				if (NULL != pOnlineGiftIcon)
	//				{
	//					pOnlineGiftIcon->initDataFromIntent();
	//				}
	//				else
	//				{
	//					/*OnlineGiftIcon * pOnlineGiftIcon = OnlineGiftIcon::create();
	//					pOnlineGiftIcon->ignoreAnchorPointForPosition(false);
	//					pOnlineGiftIcon->setAnchorPoint(ccp(0,0));
	//					pOnlineGiftIcon->setPosition(ccp(0,0));
	//					pOnlineGiftIcon->setTag(kTagOnLineGiftIcon);
	//					pOnlineGiftIcon->initDataFromIntent();

	//					GameView::getInstance()->getMainUIScene()->addChild(pOnlineGiftIcon);*/
	//				}
	//			}

	//		}
	//	}
	//	else
	//	{
	//		// 窗体存在，说明 之前的奖励没有领取完

	//		// 如果服务器中数据所有领取 都为 已领取，说明 等级超过上限 将现有的icon窗体关闭；否则 更新窗体（icon窗体 和 UI窗体）
	//		if (OnlineGiftData::instance()->isAllGiftHasGet())
	//		{
	//			pOnlineGiftIconWidget->allGiftGetIconExit();
	//		}
	//		else
	//		{
	//			// icon窗体
	//			pOnlineGiftIconWidget->initDataFromIntent();

	//			// UI窗体
	//			OnLineRewardUI * pOnLineRewardUI = (OnLineRewardUI *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOnLineGiftUI);
	//			if (NULL != pOnLineRewardUI)
	//			{
	//				pOnLineRewardUI->refreshData();
	//				pOnLineRewardUI->refreshTimeAndNum();					// 更新 奖励编号 和 时间
	//				pOnLineRewardUI->update(0);
	//			}
	//		}
	//	}
	//}


}