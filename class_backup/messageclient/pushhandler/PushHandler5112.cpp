#include "PushHandler5112.h"
#include "../protobuf/DailyGiftSign.pb.h"
#include "../element/COneSignEverydayGift.h"
#include "../../ui/SignDaily_ui/SignDailyData.h"
#include "../../GameView.h"
#include "../../ui/SignDaily_ui/SignDailyIcon.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../ui/SignDaily_ui/SignDailyUI.h"
#include "../../ui/Rank_ui/RankData.h"
#include "../../ui/GameUIConstant.h"
#include "../../ui/Reward_ui/RewardUi.h"
#include "../../messageclient/element/CRewardBase.h"

IMPLEMENT_CLASS(PushHandler5112)

PushHandler5112::PushHandler5112() 
{

}
PushHandler5112::~PushHandler5112() 
{
	
}
void* PushHandler5112::createInstance()
{
	return new PushHandler5112() ;
}
void PushHandler5112::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5112::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5112::handle(CommonMessage* mb)
{
	Push5112 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	// 清空数据（保存服务器中的数据）
	std::vector<COneSignEverydayGift *>::iterator iter;
	for (iter = SignDailyData::instance()->m_vector_internt_sign.begin(); iter != SignDailyData::instance()->m_vector_internt_sign.end(); iter++)
	{
		delete *iter;
	}
	SignDailyData::instance()->m_vector_internt_sign.clear();


	// 初始化 保存服务器的数据
	int size = bean.auctioninfos_size();
	for (int i = 0; i<size; i++)
	{
		COneSignEverydayGift * oneSignDailyGift =new COneSignEverydayGift();
		oneSignDailyGift->CopyFrom(bean.auctioninfos(i));

		SignDailyData::instance()->m_vector_internt_sign.push_back(oneSignDailyGift);
	}

	/** 是登陆还是24点推送 0推送，1登陆*/
	int nIsLogn = bean.islogin();
	SignDailyData::instance()->setIsLogin(nIsLogn);

	/*  保存服务器推送的 服务器刷新时间(排行榜-服务器重启时间)*/
	long long longTimeRefresh = bean.toptime();
	RankData::instance()->set_timeRefreshFlag(longTimeRefresh);

	if (1 == nIsLogn)
	{
		// 记录登陆时用户等级
		SignDailyData::instance()->setLoginPlayerLevel(GameView::getInstance()->myplayer->getActiveRole()->level());
	}

	// 将 internt 数据 更新到 native数据
	SignDailyData::instance()->initDataFromIntent();

	// 判断MainScene是否存在
	if (NULL != GameView::getInstance()->getGameScene())
	{
		// 判断 icon窗体是否存在：窗体存在则 更新数据；否则，创建窗体
		SignDailyIcon* pSignDailyIcon = (SignDailyIcon *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagSignDailyIcon);
		if (NULL != pSignDailyIcon)
		{
			pSignDailyIcon->initDataFromIntent();
		}

		// 更新 领奖 的状态(签到)
		if (SignDailyData::instance()->isCanSign())
		{
			for (int i = 0;i < GameView::getInstance()->rewardvector.size();i++)
			{
				if (GameView::getInstance()->rewardvector.at(i)->getId() == REWARD_LIST_ID_SIGNDAILY)
				{
					GameView::getInstance()->rewardvector.at(i)->setRewardState(1);
					RewardUi::setRewardListParticle();
				}
			}
		}

		// 判断 UI窗体是否存在:窗体存在则 更新数据；
		SignDailyUI* pSignDailyUI = (SignDailyUI *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagSignDailyUI);
		if (NULL != pSignDailyUI)
		{
			pSignDailyUI->refreshUI();
		}
		//else if (NULL == pSignDailyUI && 1 == nIsLogn && GameView::getInstance()->myplayer->getActiveRole()->level() >= SIGN_DAILYGIFT_LEVEL)					// 第一次登陆
		//{
		//	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

		//	SignDailyUI * pSignDailyUI = SignDailyUI::create();
		//	pSignDailyUI->ignoreAnchorPointForPosition(false);
		//	pSignDailyUI->setAnchorPoint(ccp(0.5f, 0.5f));
		//	pSignDailyUI->setPosition(ccp(winSize.width / 2, winSize.height / 2));
		//	pSignDailyUI->setTag(kTagSignDailyUI);
		//	
		//	GameView::getInstance()->getMainUIScene()->addChild(pSignDailyUI);

		//	pSignDailyUI->refreshUI();
		//}
	}
}