
#ifndef Blog_C___Family_PushHandler1504_h
#define Blog_C___Family_PushHandler1504_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1504 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1504)
    
public:
    SYNTHESIZE(PushHandler1504, int*, m_pValue)
    
    PushHandler1504() ;
    virtual ~PushHandler1504() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
