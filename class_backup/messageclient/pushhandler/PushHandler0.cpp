
#include "PushHandler0.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "GameView.h"





//using namespace threekingdoms::protocol;

IMPLEMENT_CLASS(PushHandler0)

PushHandler0::PushHandler0() 
{
    
}
PushHandler0::~PushHandler0() 
{
    
}
void* PushHandler0::createInstance()
{
    return new PushHandler0() ;
}
void PushHandler0::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler0::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler0::handle(CommonMessage* mb)
{
	Push0 bean;
	bean.ParseFromString(mb->data());
	
	if (bean.has_msg())
	{
		CCLOG("msg: %d, common message: %s", mb->cmdid(), bean.msg().c_str());
		GameView::getInstance()->showAlertDialog(bean.msg());
	}
}
