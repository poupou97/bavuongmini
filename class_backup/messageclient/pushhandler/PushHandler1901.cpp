#include "PushHandler1901.h"

#include "../protobuf/CopyMessage.pb.h"
#include "GameView.h"
#include "../../ui/challengeRound/SingleCopyResult.h"
#include "../../gamescene_state/MainScene.h"
#include "gamescene_state/sceneelement/MissionAndTeam.h"

IMPLEMENT_CLASS(PushHandler1901)

PushHandler1901::PushHandler1901() 
{

}
PushHandler1901::~PushHandler1901() 
{

}
void* PushHandler1901::createInstance()
{
	return new PushHandler1901() ;
}
void PushHandler1901::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1901::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1901::handle(CommonMessage* mb)
{
	Rsp1901 bean;
	bean.ParseFromString(mb->data());

	//copy is over
	CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();

	int m_curTime = bean.curtime()/1000;
	int m_bestTime = bean.besttime()/1000;

	if (GameView::getInstance()->getMainUIScene()->getChildByTag(ktagSingCopyResutUi)==NULL)
	{
		MissionAndTeam * missionAndTeam = (MissionAndTeam *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMissionAndTeam);
		if (missionAndTeam)
		{
			missionAndTeam->unschedule(schedule_selector(MissionAndTeam::updateSingCopyCheckMonst));
		}

		SingleCopyResult * copyResultui = SingleCopyResult::create(bean.clazz(),bean.level(),m_curTime,m_bestTime,bean.money(),bean.result());
		copyResultui->ignoreAnchorPointForPosition(false);
		copyResultui->setAnchorPoint(ccp(0.5f,0.5f));
		copyResultui->setPosition(ccp(winsize.width/2,winsize.height/2));
		copyResultui->setTag(ktagSingCopyResutUi);
		GameView::getInstance()->getMainUIScene()->addChild(copyResultui);
	}
}