
#include "PushHandler7020.h"

#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../protobuf/MissionMessage.pb.h"
#include "../../ui/rewardTask_ui/RewardTaskData.h"
#include "../element/CBoardMissionInfo.h"
#include "../../ui/rewardTask_ui/RewardTaskMainUI.h"
#include "../../ui/backpackscene/UseMissionPropUI.h"
#include "../element/CMissionReward.h"

IMPLEMENT_CLASS(PushHandler7020)

PushHandler7020::PushHandler7020() 
{

}
PushHandler7020::~PushHandler7020() 
{

}
void* PushHandler7020::createInstance()
{
	return new PushHandler7020() ;
}
void PushHandler7020::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler7020::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler7020::handle(CommonMessage* mb)
{
	PushPreviewScrollMission7020 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagUseMissionPropUI) == NULL)
	{
		CCSize winSize=CCDirector::sharedDirector()->getVisibleSize();
		CMissionReward * missionReward = new CMissionReward();
		missionReward->CopyFrom(bean.reward());
		UseMissionPropUI * useMissionPropUI = UseMissionPropUI::create(bean.npcid(),bean.dialog().c_str(),missionReward);
		if (useMissionPropUI)
		{
			GameView::getInstance()->getMainUIScene()->addChild(useMissionPropUI,0,kTagUseMissionPropUI);
			useMissionPropUI->ignoreAnchorPointForPosition(false);
			useMissionPropUI->setAnchorPoint(ccp(0,0.5f));
			useMissionPropUI->setPosition(ccp(0,winSize.height/2));
		}
		delete missionReward;
	}
}
