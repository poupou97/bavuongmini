#include "PushHandler5143.h"

#include "../../GameView.h"
#include "../protobuf/DailyMessage.pb.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/giveFlower/GetFlower.h"

IMPLEMENT_CLASS(PushHandler5143)

PushHandler5143::PushHandler5143() 
{

}
PushHandler5143::~PushHandler5143() 
{
	
}
void* PushHandler5143::createInstance()
{
	return new PushHandler5143() ;
}
void PushHandler5143::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5143::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5143::handle(CommonMessage* mb)
{
	Push5143 bean;
	bean.ParseFromString(mb->data());

	std::string said_ = bean.said();
	std::string flowerId_ = bean.flowername();

	MainScene * mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	if (mainscene == NULL)
		return;
	
	std::string str_ = said_;
	mainscene->flowerMarqueeVector.push_back(str_);

	if (mainscene != NULL)
	{
		std::string flowerType = bean.flowername();
		//mainscene->addFlowerParticleOnRole(flowerType);

		mainscene->addFlowerParticleOnScene(flowerType);
	}
}

