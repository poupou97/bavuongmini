#ifndef Blog_C___Reflection_PushHandler2801_h
#define Blog_C___Reflection_PushHandler2801_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler2801 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler2801)

public:
	SYNTHESIZE(PushHandler2801, int*, m_pValue)

	PushHandler2801() ;
	virtual ~PushHandler2801() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
