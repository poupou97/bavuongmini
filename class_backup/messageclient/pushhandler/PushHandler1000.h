
#ifndef Blog_C___Reflection_PushHandler1000_h
#define Blog_C___Reflection_PushHandler1000_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

/*
 * the heartbeat response message
 * Author: Zhao Gang
 */
class PushHandler1000 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1000)
    
public:
    SYNTHESIZE(PushHandler1000, int*, m_pValue)
    
    PushHandler1000() ;
    virtual ~PushHandler1000() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;

} ;

#endif
