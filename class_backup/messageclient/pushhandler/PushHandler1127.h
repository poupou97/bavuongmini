
#ifndef Blog_C___Reflection_PushHandler1127_h
#define Blog_C___Reflection_PushHandler1127_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "../protobuf/MainMessage.pb.h"
#include "../protobuf/PlayerMessage.pb.h"
#include "../../ui/extensions/PopupPanel.h"

using namespace std;
//using namespace threekingdoms::protocol;

#include "cocos2d.h"
USING_NS_CC;

class PushHandler1127 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1127)
    
public:
    SYNTHESIZE(PushHandler1127, int*, m_pValue)
    
    PushHandler1127() ;
    virtual ~PushHandler1127() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	std::vector<PopupPanel::PopuBoardOption *>OptionVector;
} ;

#endif
