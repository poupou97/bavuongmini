
#ifndef Blog_C___Reflection_PushHandler1905_h
#define Blog_C___Reflection_PushHandler1905_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

#include "cocos2d.h"
USING_NS_CC;

class PushHandler1905 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1905)
    
public:
    SYNTHESIZE(PushHandler1905, int*, m_pValue)
    
    PushHandler1905() ;
    virtual ~PushHandler1905() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;

} ;

#endif
