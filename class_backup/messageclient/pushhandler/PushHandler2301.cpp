#include "PushHandler2301.h"
#include "../protobuf/DailyMessage.pb.h"
#include "../../ui/Rank_ui/RankDataTableView.h"
#include "../element/CHomeMessage.h"
#include "../../ui/Rank_ui/RankData.h"
#include "../../ui/Rank_ui/RankDb.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/Rank_ui/RankUI.h"
#include "../../utils/GameUtils.h"

#define  PAGE_NUM 10

IMPLEMENT_CLASS(PushHandler2301)

PushHandler2301::PushHandler2301() 
{

}
PushHandler2301::~PushHandler2301() 
{
	
}
void* PushHandler2301::createInstance()
{
	return new PushHandler2301() ;
}
void PushHandler2301::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler2301::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler2301::handle(CommonMessage* mb)
{
	Rsp2301 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	// 更新的时间（时间戳）
	long long longTimeBean = bean.time();
	long long longTimeRefreshBean = bean.timerefresh();

	// 我的排行
	int nMyRank = bean.index();

	if (-1 == RankData::instance()->get_timeFlag())
	{
		long long longTimeFinal = longTimeBean + GameUtils::millisecondNow();
		RankData::instance()->set_timeFlag(longTimeFinal);
	}
	else
	{
		// 时间戳 与 现在的时间对比。如果timeFlag 等于 milliscondNow()，（1）清空数据库（2）关闭窗体，重新打开（3）return
		if (GameUtils::millisecondNow() >= RankData::instance()->get_timeFlag())
		{
			//GameView::getInstance()->showAlertDialog("pushHandler2301 timeFlag equals");

			// 时间戳更新
			RankDb::instance()->timeIsOut();

			return;
		}
	}

	if (-1 == RankData::instance()->get_timeRefreshFlag())
	{
		RankData::instance()->set_timeRefreshFlag(longTimeRefreshBean);
	}
	else
	{
		if (longTimeRefreshBean != RankData::instance()->get_timeRefreshFlag())
		{
			// 说明时间戳失效
			RankDb::instance()->timeIsOut();
		}
	}

	// 当前页 和 总页数
	int nPageCur = bean.pagenumber();
	int nPageAll = bean.pageall();

	// 更新 当前页 和 总页数 状态
	RankDataTableView::instance()->refreshDataTableStatus(nPageCur, nPageAll);

	// 清空FamilyData中m_vector_family_internet的数据
	RankData::instance()->clearVectorFamilyInternet();

	// HomeMessage添加到vector
	int nSizeHomeMessage = bean.roles_size();
	for (int i = 0; i < nSizeHomeMessage; i++)
	{
		CHomeMessage * homeMessage = new CHomeMessage();
		homeMessage->CopyFrom(bean.roles(i));
		homeMessage->set_timeFlag(RankData::instance()->get_timeFlag());
		homeMessage->set_pageCur(nPageCur);
		homeMessage->set_pageAll(nPageAll);
		homeMessage->set_myRank(nMyRank);
		homeMessage->set_timeRefreshFlag(RankData::instance()->get_timeRefreshFlag());
		RankData::instance()->m_vector_family_internet.push_back(homeMessage);
	}

	// tableName
	std::string strTableName = "t_rank_family";

	// 从db中获取数据到（临时的vector）
	std::vector<CHomeMessage *> tmpVectorFamily;
	RankDb::instance()->getDataFromDb(tmpVectorFamily);
	int nSizeVector = tmpVectorFamily.size();
	int nPageCurDb = nSizeVector / PAGE_NUM;


	// 当服务器 给予的数据的 当前页 大于 db中的页数时，才会插入db
	if (nPageCurDb - 1 < nPageCur)
	{
		// 将数据插入数据库
		RankDb::instance()->insertData(strTableName, RankData::instance()->m_vector_family_internet);
	}
	else
	{
		// 更新 当前页 和 总页数 状态
		RankDataTableView::instance()->refreshDataTableStatus(nPageCurDb - 1, nPageAll);
	}


	// 从db中取数据到vector(m_vector_family_db)
	RankDb::instance()->getDataFromDb(RankData::instance()->m_vector_family_db);

	// 通知 更新UI数据
	if (NULL != GameView::getInstance()->getMainUIScene())
	{
		RankUI* pRankUI = (RankUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRankUI);
		if (NULL != pRankUI)
		{
			pRankUI->reloadDataTableView();
		}
	}

	// 清除临时vector
	std::vector<CHomeMessage *>::iterator iterTmpVector;
	for (iterTmpVector = tmpVectorFamily.begin(); iterTmpVector != tmpVectorFamily.end(); iterTmpVector++)
	{
		delete *iterTmpVector;
	}
	tmpVectorFamily.clear();
}