
#ifndef Blog_C___Reflection_PushHandler2201_h
#define Blog_C___Reflection_PushHandler2201_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;
USING_NS_CC_EXT;
class PushHandler2201 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler2201)
    
public:
    SYNTHESIZE(PushHandler2201, int*, m_pValue)
    
    PushHandler2201() ;
    virtual ~PushHandler2201() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
