#include "PushHandler5171.h"
#include "../protobuf/GeneralMessage.pb.h"
#include "../element/CShowGeneralInfo.h"
#include "GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/role/General.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../utils/StaticDataManager.h"
#include "../element/CGeneralBaseMsg.h"
#include "../../gamescene_state/role/ShowBaseGeneral.h"


IMPLEMENT_CLASS(PushHandler5171)

PushHandler5171::PushHandler5171() 
{

}
PushHandler5171::~PushHandler5171() 
{

}
void* PushHandler5171::createInstance()
{
	return new PushHandler5171() ;
}
void PushHandler5171::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5171::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5171::handle(CommonMessage* mb)
{
	Rsp5171 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	CShowGeneralInfo* pShowGeneralInfo = new CShowGeneralInfo();
	pShowGeneralInfo->CopyFrom(bean.showgeneral());

	// 在设置数据前，无论此次是展示或者收回武将，先把先前的武将收回（如果有的话）
	ShowMyGeneral::getInstance()->generalHide();

	int nActionType = bean.actiontype();
	long long longGeneralId = pShowGeneralInfo->generalid();
	int nTemplateId = pShowGeneralInfo->templateid();
	int nCurrentQuality = pShowGeneralInfo->currentquality();
	int nEvolution = pShowGeneralInfo->evolution();

	ShowMyGeneral::getInstance()->set_generalId(longGeneralId);
	ShowMyGeneral::getInstance()->set_templateId(nTemplateId);
	ShowMyGeneral::getInstance()->set_currentQuality(nCurrentQuality);
	ShowMyGeneral::getInstance()->set_evolution(nEvolution);

	ShowMyGeneral::getInstance()->set_actionType(nActionType);


	// 展示武将 or 收回武将
	if (ShowMyGeneral::SHOW_ACTIONTYPE == nActionType)
	{
		const char *str1 = StringDataManager::getString("generals_show_zhanshi_sucess");
		char* p1 =const_cast<char*>(str1);

		GameView::getInstance()->showAlertDialog(p1);

		if (ShowMyGeneral::getInstance()->canGeneralShow())
		{
			// 展示武将
			ShowMyGeneral::getInstance()->generalShow(pShowGeneralInfo);
		}
	}
	else if(ShowMyGeneral::HIDE_ACTIONTYPE == nActionType)
	{
		const char *str1 = StringDataManager::getString("generals_show_shouhui_sucess");
		char* p1 =const_cast<char*>(str1);

		GameView::getInstance()->showAlertDialog(p1);

		// 收回武将(此处貌似可以不调用，因为 函数开头已经调用过)
		ShowMyGeneral::getInstance()->generalHide();
	}

	CC_SAFE_DELETE(pShowGeneralInfo);
}