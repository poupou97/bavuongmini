#include "PushHandler5060.h"

#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS

#include "../../messageclient/protobuf/CollectMessage.pb.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/PickingActor.h"
#include "../../messageclient/element/CPickingInfo.h"
#include "../../GameView.h"
#include "../../legend_engine/GameWorld.h"
#include "../../messageclient/element/CPushCollectInfo.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/protobuf/RecruitMessage.pb.h"
#include "../../messageclient/element/CActionDetail.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/generals_ui/GeneralsUI.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../ui/generals_ui/GeneralsListUI.h"
#include "../../newcomerstory/NewCommerStoryManager.h"

//#include "NotificationClass.h"

#else

#include "../protobuf/CollectMessage.pb.h"  
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/PickingActor.h"
#include "../element/CPickingInfo.h"
#include "../../GameView.h"
#include "../../legend_engine/GameWorld.h"
#include "../element/CPushCollectInfo.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../protobuf/RecruitMessage.pb.h"
#include "../element/CActionDetail.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/generals_ui/GeneralsUI.h"
#include "../GameMessageProcessor.h"
#include "../../ui/generals_ui/GeneralsListUI.h"
#include "../../newcomerstory/NewCommerStoryManager.h"

#endif


IMPLEMENT_CLASS(PushHandler5060)

PushHandler5060::PushHandler5060() 
{

}
PushHandler5060::~PushHandler5060() 
{

}
void* PushHandler5060::createInstance()
{
	return new PushHandler5060() ;
}
void PushHandler5060::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5060::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5060::handle(CommonMessage* mb)
{
	PushRecruitPreview5060 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	std::vector<CActionDetail*>::iterator iter_actionDetail;
	for (iter_actionDetail = GameView::getInstance()->actionDetailList.begin(); iter_actionDetail != GameView::getInstance()->actionDetailList.end(); ++iter_actionDetail)
	{
		delete *iter_actionDetail;
	}
	GameView::getInstance()->actionDetailList.clear();

	for (int i = 0;i<bean.actions_size();++i)
	{
		CActionDetail * actionDetail = new CActionDetail();
		actionDetail->CopyFrom(bean.actions(i));
		GameView::getInstance()->actionDetailList.push_back(actionDetail);
	}

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

// 	if (NewCommerStoryManager::getInstance()->IsNewComer())
// 		return;

	if (MainScene::GeneralsScene)
	{
		if (GameView::getInstance()->myplayer->getActiveRole()->level() <= 1)
		{
			// 请求阵法列表
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5071, this);
			//武将列表 
			GeneralsUI::generalsListUI->isReqNewly = true;
			GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
			temp->page = 0;
			temp->pageSize = GeneralsUI::generalsListUI->everyPageNum;
			temp->type = 1;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
			delete temp;
		}
	}
	else
	{
		MainScene::GeneralsScene = GeneralsUI::create();
		MainScene::GeneralsScene->retain();

		// 请求阵法列表
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5071, this);
		//武将列表 
		GeneralsUI::generalsListUI->isReqNewly = true;
		GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
		temp->page = 0;
		temp->pageSize = GeneralsUI::generalsListUI->everyPageNum;
		temp->type = 1;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
		delete temp;
	}

#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS

	// 武将招募 本地推送
	std::vector<CActionDetail*> actionDetailList = GameView::getInstance()->actionDetailList;
	int nSize = actionDetailList.size();
	if (4 == nSize)
	{
		for (int i = 0; i < nSize; i++)
		{
			CActionDetail * pActionDetail = actionDetailList.at(i);
			if (BASE == pActionDetail->type())
			{
//                [NotificationClass unregisterNotificationGeneral:1];
			}
			else if (BETTER == pActionDetail->type())
			{
//                [NotificationClass unregisterNotificationGeneral:2];
			}
			else if (BEST == pActionDetail->type())
			{
//                [NotificationClass unregisterNotificationGeneral:3];
			}
		}

		for (int i = 0; i < nSize; i++)
		{
			CActionDetail * pActionDetail = actionDetailList.at(i);
			if (BASE == pActionDetail->type())
			{
				//				long long longCDTime = pActionDetail->cdtime();
				//                int nMaxFreeGetCard = pActionDetail->playerremainnumber();
				//                
				//                if (nMaxFreeGetCard > 1)
				//                {
				//                    [NotificationClass pushNotificationGeneral:1 andCDTime:longCDTime];
				//                }
				long long longCDTime = pActionDetail->cdtime();
//                [NotificationClass pushNotificationGeneral:2 andCDTime:longCDTime];

			}
			else if (BETTER == pActionDetail->type())
			{
				long long longCDTime = pActionDetail->cdtime();
//                [NotificationClass pushNotificationGeneral:2 andCDTime:longCDTime];
			}
			else if (BEST == pActionDetail->type())
			{
				long long longCDTime = pActionDetail->cdtime();
//                [NotificationClass pushNotificationGeneral:3 andCDTime:longCDTime];
			}
		}
	}

#endif

}
