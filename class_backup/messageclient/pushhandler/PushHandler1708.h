
#ifndef Blog_C___Family_PushHandler1708_h
#define Blog_C___Family_PushHandler1708_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1708 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1708)
    
public:
    SYNTHESIZE(PushHandler1708, int*, m_pValue)
    
    PushHandler1708() ;
    virtual ~PushHandler1708() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
