
#ifndef Blog_C___Family_PushHandler1531_h
#define Blog_C___Family_PushHandler1531_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1531 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1531)

public:
	SYNTHESIZE(PushHandler1531, int*, m_pValue)

	PushHandler1531() ;
	virtual ~PushHandler1531() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
