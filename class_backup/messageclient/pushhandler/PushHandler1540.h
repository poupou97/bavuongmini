
#ifndef Blog_C___Family_PushHandler1540_h
#define Blog_C___Family_PushHandler1540_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1540 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1540)

public:
	SYNTHESIZE(PushHandler1540, int*, m_pValue)

		PushHandler1540() ;
	virtual ~PushHandler1540() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
