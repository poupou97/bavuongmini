
#ifndef Blog_C___Reflection_PushHandler5202_h
#define Blog_C___Reflection_PushHandler5202_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5202 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5202)

public:
	SYNTHESIZE(PushHandler5202, int*, m_pValue)

		PushHandler5202() ;
	virtual ~PushHandler5202() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
