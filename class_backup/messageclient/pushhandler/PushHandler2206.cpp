
#include "PushHandler2206.h"
#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/RelationMessage.pb.h"
#include "GameView.h"
#include "../element/CRelationPlayer.h"
#include "../../gamescene_state/MainScene.h"

IMPLEMENT_CLASS(PushHandler2206)

PushHandler2206::PushHandler2206() 
{
    
}
PushHandler2206::~PushHandler2206() 
{
    
}
void* PushHandler2206::createInstance()
{
    return new PushHandler2206() ;
}
void PushHandler2206::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler2206::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler2206::handle(CommonMessage* mb)
{
	Req2206 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());
	
	/**
	* 客户端申请加为好友指令送到服务器后，服务器把申请信息发到目标玩家，请他确认。
	* 本条指令虽然是Req开头，但是是由服务器发到客户端。
	*/
	
	CRelationPlayer * relationPlayer = new CRelationPlayer();
	relationPlayer->CopyFrom(bean.applicant());
	MainScene * mainui = (MainScene *)GameView::getInstance()->getMainUIScene();
	mainui->interactRelationOfPlayers(relationPlayer);
	delete relationPlayer;
	
}
