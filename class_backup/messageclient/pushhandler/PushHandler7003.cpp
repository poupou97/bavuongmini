#include "PushHandler7003.h"
#include "../protobuf/MainMessage.pb.h"
#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/OtherPlayer.h"
#include "../../gamescene_state/role/OtherPlayerOwnedStates.h"
#include "../../gamescene_state/role/BaseFighterOwnedCommand.h"
#include "../../gamescene_state/role/Monster.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../element/FolderInfo.h"
#include "../../ui/backpackscene/PackageItem.h"
#include "../protobuf/MissionMessage.pb.h"
#include "../../ui/missionscene/MissionManager.h"
#include "../../ui/missionscene/MissionScene.h"
#include "../../gamescene_state/role/FunctionNPC.h"
#include "../../gamescene_state/sceneelement/MissionAndTeam.h"
#include "../element/MissionInfo.h"
#include "../../ui/missionscene/HandleMission.h"

IMPLEMENT_CLASS(PushHandler7003)

PushHandler7003::PushHandler7003() 
{

}
PushHandler7003::~PushHandler7003() 
{

}
void* PushHandler7003::createInstance()
{
	return new PushHandler7003() ;
}
void PushHandler7003::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler7003::display() 
{
	cout << *getm_pValue() << endl ;
}


bool SortByPackageType(MissionInfo * missionInfo1 , MissionInfo *missionInfo2)
{
	//return (missionInfo1->missionpackagetype() < missionInfo2->missionpackagetype());

	if (missionInfo1->missionpackagetype() == 1)      //主线
	{
		if (missionInfo2->missionpackagetype() == 1)
		{
			return (missionInfo1->missionstate() > missionInfo2->missionstate());
		}
		else
		{
			return (missionInfo1->missionpackagetype()< missionInfo2->missionpackagetype());
		}

// 		if(missionInfo1->missionpackagetype()< missionInfo2->missionpackagetype())
// 		{
// 			return true;
// 		}	
// 		else if (missionInfo1->missionpackagetype() == missionInfo2->missionpackagetype())
// 		{
// 			return (missionInfo1->missionstate() > missionInfo2->missionstate());
// 		}
// 
// 		return false;
	}
	else
	{
		if (missionInfo2->missionpackagetype() == 1)
		{
			return (missionInfo1->missionpackagetype()< missionInfo2->missionpackagetype());
		}
		else
		{
			if (missionInfo1->missionstate() > missionInfo2->missionstate())
			{
				return true;
			}
			else if (missionInfo1->missionstate() == missionInfo2->missionstate())
			{
				return (missionInfo1->missionpackagetype()< missionInfo2->missionpackagetype());
			}

			return false;	
		}
// 		if (missionInfo1->missionstate() > missionInfo2->missionstate())
// 		{
// 			return true;
// 		}
// 	 	else if (missionInfo1->missionstate() == missionInfo2->missionstate())
// 		{
// 			return (missionInfo1->missionpackagetype() < missionInfo2->missionpackagetype());
// 		}
// 
// 		return false;
	}
}

void PushHandler7003::handle(CommonMessage* mb)
{
	Push7003 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	bool isFirstLogin = GameView::getInstance()->getIsCheckRemind();
	
	MissionInfo * missionInfo = new MissionInfo();
	missionInfo->CopyFrom(bean.missioninfo());
	bool isExist = false;
	if(GameView::getInstance()->missionManager->MissionList.size()>0)
	{
		for(int i = 0;i<(int)GameView::getInstance()->missionManager->MissionList.size();++i)
		{
			MissionInfo * tempInfo = GameView::getInstance()->missionManager->MissionList.at(i);
			if (strcmp(tempInfo->missionpackageid().c_str(),missionInfo->missionpackageid().c_str()) == 0)
			{
				isExist = true;
				tempInfo->CopyFrom(bean.missioninfo());
				delete missionInfo;
				break;
			}
		}

		if (isExist == false )
		{
			GameView::getInstance()->missionManager->MissionList.push_back(missionInfo);
		}

	}
	else
	{
		GameView::getInstance()->missionManager->MissionList.push_back(missionInfo);
	}
	


 	MissionInfo * missionInfo1 = new MissionInfo();
 	missionInfo1->CopyFrom(bean.missioninfo());
	bool isExist_main = false;
	int m_nIndex = 0;
	bool isNeedRefreshAll = false;
	bool isNeedRefreshWithOffSet = false;
	if (bean.missioninfo().missionpackagetype() <= 2 ||(bean.missioninfo().missionpackagetype() > 2 && bean.missioninfo().missionstate() > 0))
	{
		if(GameView::getInstance()->missionManager->MissionList_MainScene.size()>0)
		{
			for(int i = 0;i<(int)GameView::getInstance()->missionManager->MissionList_MainScene.size();++i)
			{
				MissionInfo * tempInfo = GameView::getInstance()->missionManager->MissionList_MainScene.at(i);
				if (strcmp(tempInfo->missionpackageid().c_str(),bean.missioninfo().missionpackageid().c_str()) == 0)
				{
					bool isstatechanged = true;
					if(tempInfo->missionstate()== bean.missioninfo().missionstate())
					{
						isstatechanged = false;
					}

					isExist_main = true;
					m_nIndex = i;
					tempInfo->CopyFrom(bean.missioninfo());
					tempInfo->isStateChanged = isstatechanged;
					tempInfo->isNeedTeach = true;
					delete missionInfo1;
					break;
				}
			}

			if (isExist_main == false )
			{
				if (missionInfo1->missionpackagetype() == 1)
				{
					GameView::getInstance()->missionManager->MissionList_MainScene.insert(GameView::getInstance()->missionManager->MissionList_MainScene.begin(),missionInfo1);
					isNeedRefreshAll = true;
				}
				else
				{
					if (GameView::getInstance()->missionManager->m_sLastMissionPackageId == missionInfo1->missionpackageid())
					{
						GameView::getInstance()->missionManager->MissionList_MainScene.insert(GameView::getInstance()->missionManager->MissionList_MainScene.begin()+GameView::getInstance()->missionManager->m_nLastMissionIndexInMain,missionInfo1);
						isNeedRefreshWithOffSet = true;
					}
					else
					{
						GameView::getInstance()->missionManager->MissionList_MainScene.push_back(missionInfo1);
					}
				}
			}
		}
		else
		{
			if (missionInfo1->missionpackagetype() == 1)
			{
				GameView::getInstance()->missionManager->MissionList_MainScene.insert(GameView::getInstance()->missionManager->MissionList_MainScene.begin(),missionInfo1);
				isNeedRefreshAll = true;
			}
			else
			{
				GameView::getInstance()->missionManager->MissionList_MainScene.push_back(missionInfo1);
			}
		}

	}
 
	if (isFirstLogin)
	{
		//按类型排序
		sort(GameView::getInstance()->missionManager->MissionList_MainScene.begin(),GameView::getInstance()->missionManager->MissionList_MainScene.end(),SortByPackageType);
	}

	// it's not game state, ignore
	CCScene* pScene = CCDirector::sharedDirector()->getRunningScene();
	if(pScene->getTag() != GameView::STATE_GAME)
	{
		return;
	}
	else
	{
		GameSceneState* pState = (GameSceneState*)pScene;
		if(pState->getState() == GameSceneState::state_load)
		{
			return;
		}
	}


	MainScene * mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	if (mainscene->getChildByTag(kTagMissionAndTeam) != NULL)
	{
		MissionAndTeam * missionAndTeam = (MissionAndTeam *)mainscene->getChildByTag(kTagMissionAndTeam);
		//missionAndTeam->reloadMissionData();
		//missionAndTeam->reloadMissionDataWithoutChangeOffSet();
		if (isExist_main)
		{
			missionAndTeam->missionTableView->updateCellAtIndex(m_nIndex);
		}
		else
		{
			if (isNeedRefreshAll)
			{
				missionAndTeam->reloadMissionData();
			}
			else if (isNeedRefreshWithOffSet)
			{
				missionAndTeam->reloadMissionDataWithoutChangeOffSet(GameView::getInstance()->missionManager->m_pLastMissionOffSet,GameView::getInstance()->missionManager->m_missionTableViewSize);
			}
			else
			{
				missionAndTeam->reloadMissionDataWithoutChangeOffSet(missionAndTeam->missionTableView->getContentOffset(),missionAndTeam->missionTableView->getContentSize());
			}
		}
	}

	if (mainscene->getChildByTag(kTagMissionScene) != NULL)
	{
		MissionScene * missionScene = (MissionScene *)mainscene->getChildByTag(kTagMissionScene);
		//missionScene->m_tableView->reloadData();
		//reload
		missionScene->ReloadDataWithOutChangeOffSet();
		//设置默认
		if (missionScene->curMissionDataSource.size()>0)
		{
			if (missionScene->curMissionDataSource.size() > missionScene->selectCellIdx)
			{
			}
			else if(missionScene->curMissionDataSource.size() == missionScene->selectCellIdx)
			{
				missionScene->selectCellIdx -= 1;
			}
			else
			{
				missionScene->selectCellIdx = 0;
			}

			missionScene->m_tableView->selectCell(missionScene->selectCellIdx);
			missionScene->curMissionInfo->CopyFrom(*(missionScene->curMissionDataSource.at(missionScene->selectCellIdx)));
			missionScene->ReloadMissionInfoData();
		}
	}

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	MissionInfo * curMissionInfo = new MissionInfo();
	curMissionInfo->CopyFrom(bean.missioninfo());
	if (curMissionInfo->has_action())
	{
		if (curMissionInfo->action().has_moveto())
		{
			if (curMissionInfo->action().moveto().has_targetnpc())
			{
				FunctionNPC* fn = dynamic_cast<FunctionNPC*>(scene->getActor(curMissionInfo->action().moveto().targetnpc().npcid()));
				if (fn != NULL)
				{
					fn->RefreshNpc();

// 					if (GameView::getInstance()->missionManager->curMissionList.size()>0)
// 					{
// 						MissionInfo * tempMissionInfo = GameView::getInstance()->missionManager->curMissionList.at(0);
// 						if(tempMissionInfo->has_action())
// 						{
// 							if (tempMissionInfo->action().has_moveto())
// 							{
// 								if(tempMissionInfo->action().moveto().targetnpc().npcid() == fn->getRoleId())
// 								{
// 									for (int i = 0;i<(int)fn->npcMissionList.size();++i)
// 									{
// 										if (fn->npcMissionList.at(i)->missionstate() == dispatched)  //任务可接
// 										{
// 											if (fn->npcMissionList.at(i)->missionpackagetype() == 1)  //主线任务
// 											{
// 												GameView::getInstance()->missionManager->curMissionInfo->CopyFrom(*fn->npcMissionList.at(i));
// 
// 												if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHandleMission))
// 												{
// 													GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHandleMission)->removeFromParent();
// 												}
// 
// 										
// 												CCNode* pNode = scene->getChildByTag(kTagMovieBaseNode);
// 												if(pNode == NULL)
// 												{
// 													CCActionInterval * m_action =(CCActionInterval *)CCSequence::create(CCDelayTime::create(0.5f),
// 														CCCallFuncND::create(scene, callfuncND_selector(PushHandler7003::showHandleMission),(void *)(fn->npcMissionList.at(i))),
// 														NULL);
// 
// 													scene->runAction(m_action);
// 													break;
// 												}
// 											}
// 										}
// 									}
// 								}
// 							}
// 						}
 					}
 			}
 		}
 	}
	GameView::getInstance()->missionManager->autoPopUpHandleMission();
	delete curMissionInfo;
	mainscene->remindMission();
	//mainscene->checkIsNewRemind();
}

// void PushHandler7003::showHandleMission( CCNode* sender, void* data )
// {
// 	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHandleMission))
// 	{
// 		GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHandleMission)->removeFromParent();
// 	}
// 
// 	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
// 	HandleMission * getMission = HandleMission::create((MissionInfo*)data);
// 	getMission->ignoreAnchorPointForPosition(false);
// 	getMission->setAnchorPoint(ccp(0,0.5f));
// 	getMission->setPosition(ccp(0,winSize.height/2));
// 	GameView::getInstance()->getMainUIScene()->addChild(getMission,0,kTagHandleMission);	
// }

