#include "PushHandler5117.h"
#include "../protobuf/ActiveMessage.pb.h"
#include "../../ui/Active_ui/ActiveData.h"
#include "../element/CActiveLabel.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/Active_ui/ActiveUI.h"
#include "../../ui/Chat_ui/ChatCell.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/sceneelement/ChatWindows.h"
#include "../../ui/Chat_ui/Tablist.h"
#include "../../ui/Chat_ui/ChatUI.h"

IMPLEMENT_CLASS(PushHandler5117)

PushHandler5117::PushHandler5117() 
{

}
PushHandler5117::~PushHandler5117() 
{
	
}
void* PushHandler5117::createInstance()
{
	return new PushHandler5117() ;
}
void PushHandler5117::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5117::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5117::handle(CommonMessage* mb)
{
	Push5117 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	//// 清空 保存服务器的数据
	//ActiveData::instance()->clearActiveLabel();

	//// 保存服务器数据到 ActiveData
	//int nActiveLabelSize = bean.label_size();
	//for (int i = 0; i < nActiveLabelSize; i++)
	//{
	//	CActiveLabel* activeLabel = new CActiveLabel();
	//	activeLabel->CopyFrom(bean.label(i));

	//	ActiveData::instance()->m_vector_internet_label.push_back(activeLabel);
	//}

	//// 更新 服务器数据 到 native_note
	//ActiveData::instance()->initNoteFromInternet();
	
	// 要更新的数据
	CActiveLabel * activeLabel = new CActiveLabel();
	activeLabel->CopyFrom(bean.label());

	// 更新服务器的数据
	unsigned int nLabelSize = ActiveData::instance()->m_vector_internet_label.size();
	for (unsigned int i = 0; i < nLabelSize; i++)
	{
		if (activeLabel->id() == ActiveData::instance()->m_vector_internet_label.at(i)->id())
		{
			ActiveData::instance()->m_vector_internet_label.at(i) = activeLabel;
		}
	}

	// 玩家活跃度
	int nOldActive = ActiveData::instance()->getUserActive();
	int nNewActive = bean.number();
	int nGetActive = nNewActive - nOldActive;

	// 更新活跃度
	ActiveData::instance()->setUserActive(bean.number());

	// 更新今日活跃度
	ActiveData::instance()->set_todayActive(bean.numbertoday());

	// 更新 服务器数据到 native_label
	ActiveData::instance()->initLabelFromInternet();

	// 通知更新窗体
	MainScene* mainUILayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
	if(mainUILayer->getChildByTag(kTagActiveUI) != NULL)
	{
		ActiveUI * activeUI = (ActiveUI *)(mainUILayer->getChildByTag(kTagActiveUI));
		if (NULL != activeUI)
		{
			activeUI->initLabelFromInternet();
			activeUI->initUserActiveFromInternet();
			activeUI->initTodayActiveFromInternet();
			activeUI->refrehTableView();

			if (0 == nGetActive)
			{
				const char * charInfo = StringDataManager::getString("activy_refresh");
				GameView::getInstance()->showAlertDialog(charInfo);
			}
		}
	}

	// 当玩家获得活跃度为0时，不下发消息（获得活跃度为0说明开启了新活动）
	if (0 == nGetActive)
	{
		return;
	}

	// 系统频道信息，发送 获得多少活跃度
	int nChannelId = 4;				// 系统频道

	const char * str_chat1 = StringDataManager::getString("activy_chartInfo1");
	char * str_chatTmp1 = const_cast<char*>(str_chat1);

	const char * str_chat2 = StringDataManager::getString("activy_chartInfo2");
	char * str_chatTmp2 = const_cast<char*>(str_chat2);

	char str_activeNum[4];
	sprintf(str_activeNum, "%d", nGetActive);

	std::string str_activeInfo = "";
	str_activeInfo.append(str_chatTmp1);
	str_activeInfo.append(str_activeNum);
	str_activeInfo.append(str_chatTmp2);

	// 此为showAlert 
	GameView::getInstance()->showAlertDialog(str_activeInfo);

	// 此段代码为向聊天框发送 获取活跃度的提示消息
	/*----------------------------------------------------------------------------

	// cache chat cell(大窗体)（给大窗体发送获得活跃度的消息）
	if (strlen(str_activeInfo.c_str()) > 0)
	{
		ChatUI::addToMainChatUiMessage(nChannelId,str_activeInfo);
	}
	

	// 给小窗体发送获得活跃度的消息
	// for the reason of performance ( fps )
	// when the chat main window is opened, ignore the mini chat window at the bottom of the main ui interface
	if(mainUILayer->getChildByTag(kTabChat) == NULL)
	{
		ChatWindows::addToMiniChatWindow(nChannelId,"","",str_activeInfo,NULL,false,0,0,"");
	}
	
	----------------------------------------------------------------------------*/
}