
#include "PushHandler5012.h"

#include "../protobuf/CollectMessage.pb.h"  
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/PickingActor.h"
#include "../element/CPickingInfo.h"
#include "../../GameView.h"
#include "../../legend_engine/GameWorld.h"
#include "../element/CPushCollectInfo.h"
#include "../../gamescene_state/role/MyPlayer.h"

IMPLEMENT_CLASS(PushHandler5012)

	PushHandler5012::PushHandler5012() 
{

}
PushHandler5012::~PushHandler5012() 
{

}
void* PushHandler5012::createInstance()
{
	return new PushHandler5012() ;
}
void PushHandler5012::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5012::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5012::handle(CommonMessage* mb)
{
	PushCollectionState5012 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

 	long long instanceID = bean.instanceid();

	if (scene->getActor(instanceID) != NULL)
	{
		PickingActor * pickingActor = (PickingActor *)scene->getActor(instanceID);
		pickingActor->p_collectInfo->set_actiondescription(bean.actiondescription());
		if (bean.state() == INVAILD)
		{
			pickingActor->setActivied(false);
			pickingActor->p_collectInfo->set_state(INVAILD);
		}
		else
		{
			pickingActor->setActivied(true);
			pickingActor->p_collectInfo->set_state(ACTIVITY);
		}
		
	}
}
