#ifndef Blog_C___Reflection_PushHandler5102_h
#define Blog_C___Reflection_PushHandler5102_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5102 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5102)

public:
	SYNTHESIZE(PushHandler5102, int*, m_pValue)

	PushHandler5102() ;
	virtual ~PushHandler5102() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
