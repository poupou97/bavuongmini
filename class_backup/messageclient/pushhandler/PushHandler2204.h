
#ifndef Blog_C___Reflection_PushHandler2204_h
#define Blog_C___Reflection_PushHandler2204_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler2204 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler2204)
    
public:
    SYNTHESIZE(PushHandler2204, int*, m_pValue)
    
    PushHandler2204() ;
    virtual ~PushHandler2204() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
