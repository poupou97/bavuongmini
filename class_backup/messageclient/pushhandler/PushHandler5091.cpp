
#include "PushHandler5091.h"

#include "../protobuf/FightMessage.pb.h"
#include "GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"

IMPLEMENT_CLASS(PushHandler5091)

PushHandler5091::PushHandler5091() 
{
    
}
PushHandler5091::~PushHandler5091() 
{
    
}
void* PushHandler5091::createInstance()
{
    return new PushHandler5091() ;
}
void PushHandler5091::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5091::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler5091::handle(CommonMessage* mb)
{
	Push5091 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, add or remove aggressor for MyPlayer, role id: %lld", mb->cmdid(), bean.roleid());

	std::map<long long, int>& aggressorMap = GameView::getInstance()->myplayer->getAggressorMap();
	// 0��ɾ�� 1������
	if(bean.flag() == 0) {
		aggressorMap.erase(bean.roleid());
	}
	else {
		aggressorMap.insert(std::pair<long long, int>(bean.roleid(), 1));
	}
}