#include "PushHandler1322.h"

#include "../protobuf/ItemMessage.pb.h"  
#include "../../ui/backpackscene/GoodsItemInfoBase.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../element/GoodsInfo.h"



IMPLEMENT_CLASS(PushHandler1322)

PushHandler1322::PushHandler1322() 
{

}
PushHandler1322::~PushHandler1322() 
{

}
void* PushHandler1322::createInstance()
{
	return new PushHandler1322() ;
}
void PushHandler1322::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1322::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1322::handle(CommonMessage* mb)
{
	Push1322 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	GoodsInfo * goods_ =new GoodsInfo();
	goods_->CopyFrom(bean.goods());

	CCSize winSize=CCDirector::sharedDirector()->getVisibleSize();
	GoodsItemInfoBase * goodsInfo =GoodsItemInfoBase::create(goods_,GameView::getInstance()->EquipListItem,0);
	goodsInfo->ignoreAnchorPointForPosition(false);
	goodsInfo->setAnchorPoint(ccp(0.5f,0.5f));
	//goodsInfo->setPosition(ccp(winSize.width/2,winSize.height/2));
	GameView::getInstance()->getMainUIScene()->addChild(goodsInfo);

	delete goods_;
}
