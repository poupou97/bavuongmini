
#include "PushHandler2201.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/OtherPlayer.h"
#include "../../gamescene_state/role/OtherPlayerOwnedStates.h"
#include "../../gamescene_state/role/BaseFighterOwnedCommand.h"

#include "../protobuf/RelationMessage.pb.h"
#include "GameView.h"
#include "../element/CRelationPlayer.h"
#include "../../ui/Friend_ui/FriendWorkList.h"
#include "../../ui/Friend_ui/FriendUi.h"
#include "../../ui/Mail_ui/MailFriend.h"
#include "../../ui/Mail_ui/MailUI.h"
#include "../../utils/StaticDataManager.h"

IMPLEMENT_CLASS(PushHandler2201)

PushHandler2201::PushHandler2201() 
{
    
}
PushHandler2201::~PushHandler2201() 
{
    
}
void* PushHandler2201::createInstance()
{
    return new PushHandler2201() ;
}
void PushHandler2201::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler2201::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler2201::handle(CommonMessage* mb)
{
	Push2201 bean;
	bean.ParseFromString(mb->data());
	
	CRelationPlayer * relationPlayer =new CRelationPlayer();
	relationPlayer->CopyFrom(bean.player());

	switch(bean.changetype())//0-add  1-delete 2-onLine 3-offline
	{
	case 0:
		{
			//add
			switch(bean.relationtype())//0-friend, 1-blackList，2-enemy
			{
			case 0:
				{
					GameView::getInstance()->relationFriendVector.push_back(relationPlayer);
				}break;
			case 1:
				{
					std::string str_tips = "";
					const char * str_tips1 = StringDataManager::getString("friend_black_tips1");
					const char * str_tips2 = StringDataManager::getString("friend_black_tips2");
					str_tips.append(str_tips1);
					str_tips.append(" ");
					str_tips.append(bean.playername());
					str_tips.append(" ");
					str_tips.append(str_tips2);

					GameView::getInstance()->showAlertDialog(str_tips);
					GameView::getInstance()->relationBlackVector.push_back(relationPlayer);
				}break;
			case 2:
				{
					GameView::getInstance()->relationEnemyVector.push_back(relationPlayer);
				}break;
			}
		}break;
	case 1:
		{
			//delete
			switch(bean.relationtype())
			{
			case 0:
				{
					for (int i=0;i<GameView::getInstance()->relationFriendVector.size();i++)
					{
						if (bean.playerid()==GameView::getInstance()->relationFriendVector.at(i)->playerid())
						{
							std::vector<CRelationPlayer *>::iterator iter= GameView::getInstance()->relationFriendVector.begin()+i;
							CRelationPlayer * relation=*iter;
							GameView::getInstance()->relationFriendVector.erase(iter);
							delete relation;
						}
					}
				}break;
			case 1:
				{
					for (int i=0;i<GameView::getInstance()->relationBlackVector.size();i++)
					{
						if (bean.playerid()==GameView::getInstance()->relationBlackVector.at(i)->playerid())
						{
							std::vector<CRelationPlayer *>::iterator iter= GameView::getInstance()->relationBlackVector.begin()+i;
							CRelationPlayer * relation=*iter;
							GameView::getInstance()->relationBlackVector.erase(iter);
							delete relation;
						}
					}
				}break;
			case 2:
				{
					for (int i=0;i<GameView::getInstance()->relationEnemyVector.size();i++)
					{
						if (bean.playerid()==GameView::getInstance()->relationEnemyVector.at(i)->playerid())
						{
							std::vector<CRelationPlayer *>::iterator iter= GameView::getInstance()->relationEnemyVector.begin()+i;
							CRelationPlayer * relation=*iter;
							GameView::getInstance()->relationEnemyVector.erase(iter);
							delete relation;
						}
					}
				}break;
			}
		}break;
	case 2:
		{
			//on line 
			return;
		}break;
	case 3:
		{
			//off line
			return;
		}break;
	}

	if (GameView::getInstance()->getMainUIScene())
	{
		if (bean.changetype() != 2 && bean.changetype() != 3)
		{
			FriendUi * friendui =(FriendUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);
			if (friendui!=NULL)
			{
				CCTableView * friendTab =(CCTableView *)friendui->getChildByTag(FRIENDADDLAYER)->getChildByTag(FRIENDWORKLISTTAG)->getChildByTag(FRIENDWORKLISTTAB);
				friendTab->reloadData();
				//int curPlayerNum_ = friendui->totalPlayerNum_;
				switch(bean.changetype())
				{
				case 0:
					{
						//add
						switch(bean.relationtype())//0-friend, 1-blackList，2-enemy
						{
						case 0:
							{
								friendui->totalPlayerNum_++;
							}break;
						case 1:
							{
								
							}break;
						case 2:
							{
								//仇人和好友或者黑名单是可以共存的
							}break;
						}
					}break;
				case 1:
					{
						//delete
						friendui->totalPlayerNum_--;
					}break;
				case 2:
					{
						//on line
					}break;
				case 3:
					{
						//off line
					}break;
				}
				friendui->reloadSourceData(friendui->totalPlayerNum_);
			}
		}
	}
}
