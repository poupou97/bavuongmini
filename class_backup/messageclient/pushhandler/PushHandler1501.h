
#ifndef Blog_C___Family_PushHandler1501_h
#define Blog_C___Family_PushHandler1501_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1501 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1501)
    
public:
    SYNTHESIZE(PushHandler1501, int*, m_pValue)
    
    PushHandler1501() ;
    virtual ~PushHandler1501() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
