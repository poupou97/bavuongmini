#include "PushHandler1928.h"

#include "../protobuf/CopyMessage.pb.h"
#include "../../ui/FivePersonInstance/FivePersonInstance.h"
#include "../../ui/FivePersonInstance/InstanceEndUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../element/CFivePersonInstanceEndInfo.h"
#include "../element/CRewardProp.h"
#include "../../ui/FivePersonInstance/InstanceRewardUI.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../element/GoodsInfo.h"
#include "../../gamescene_state/sceneelement/GuideMap.h"

IMPLEMENT_CLASS(PushHandler1928)

	PushHandler1928::PushHandler1928() 
{

}
PushHandler1928::~PushHandler1928() 
{

}
void* PushHandler1928::createInstance()
{
	return new PushHandler1928() ;
}
void PushHandler1928::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1928::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1928::handle(CommonMessage* mb)
{
	Push1928 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, instance is over", mb->cmdid());

	for(int i = 0;i<bean.stars_size();i++)
	{
		bool isExit = false;
		std::map<int, int>::iterator it = FivePersonInstance::getInstance()->s_m_copyStar.begin();
		for ( ; it != FivePersonInstance::getInstance()->s_m_copyStar.end(); ++ it )
		{
			if (it->first == bean.stars(i).copyid())
			{
				it->second = bean.stars(i).star();
				isExit = true;
			}
		}

		if (!isExit)
		{
			FivePersonInstance::getInstance()->s_m_copyStar.insert(make_pair(bean.stars(i).copyid(),bean.stars(i).star()));
		}
	}
	
}