
#ifndef Blog_C___Reflection_PushHandler7015_h
#define Blog_C___Reflection_PushHandler7015_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler7015 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler7015)

public:
	SYNTHESIZE(PushHandler7015, int*, m_pValue)

		PushHandler7015() ;
	virtual ~PushHandler7015() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

private:

protected:
	int *m_pValue ;

} ;

#endif
