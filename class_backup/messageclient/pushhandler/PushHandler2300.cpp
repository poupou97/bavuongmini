#include "PushHandler2300.h"
#include "../protobuf/DailyMessage.pb.h"
#include "../element/CRoleMessage.h"
#include "../../ui/Rank_ui/RankData.h"
#include "../../ui/Rank_ui/RankDb.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../../ui/Rank_ui/RankUI.h"
#include "../../ui/Rank_ui/RankDataTableView.h"
#include "../../utils/GameUtils.h"
#include "../../gamescene_state/role/MyPlayer.h"

#define  PAGE_NUM 10

IMPLEMENT_CLASS(PushHandler2300)

PushHandler2300::PushHandler2300() 
{

}
PushHandler2300::~PushHandler2300() 
{
	
}
void* PushHandler2300::createInstance()
{
	return new PushHandler2300() ;
}
void PushHandler2300::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler2300::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler2300::handle(CommonMessage* mb)
{
	Rsp2300 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	//请求类别 0等级，1战斗力，2金币数，3战功，4家族, 5鲜花
	int nType = bean.type();

	//职业 0.全部、1.猛将、2.鬼谋、3.豪杰、4.神射、
	//国家 0.公共、1.魏、2.蜀、3.吴、
	//鲜花 1.今日收花、2.今日送花、3.历史收花、4.历史送花
	int nWork = bean.work();

	//我的排行
	int nMyPlayerRank = bean.index();

	//用户id和name
	RoleBase* base = GameView::getInstance()->myplayer->getActiveRole()->mutable_rolebase();
	long long longMyPlayerId = base->roleid();
	std::string str_myPlayerName = base->name();

	 //更新的时间(时间戳)----服务器返回的是 还差多长时间 更新数据，保存时 保存 服务器数据 + millisecondNow;
	long long longTimeBean = bean.time();

	// 服务器重启的时间戳（服务器的和本地的对比，若不一样，时间戳失效，删除所有数据；否则不做任何处理）
	long long longTimeRefreshBean = bean.timerefresh();

	if (-1 == RankData::instance()->get_timeFlag())
	{
		long long longTimeFinal = longTimeBean + GameUtils::millisecondNow();
		RankData::instance()->set_timeFlag(longTimeFinal);
	}
	else
	{
		// 时间戳 与 现在的时间对比。如果timeFlag 等于 milliscondNow()，（1）清空数据库（2）更新用户当前页（3）return
		if (GameUtils::millisecondNow() >= RankData::instance()->get_timeFlag())
		{
			  //GameView::getInstance()->showAlertDialog("push2300 timeFlag equals");

			  // 时间戳更新
			  RankDb::instance()->timeIsOut();

			  return;
		}
	}

	if (-1 == RankData::instance()->get_timeRefreshFlag())
	{
		RankData::instance()->set_timeRefreshFlag(longTimeRefreshBean);
	}
	else
	{
		// 时间戳 与 现在的 timeRefresh时间对比。若不相等，则 说明时间戳失效
		if (longTimeRefreshBean != RankData::instance()->get_timeRefreshFlag())
		{
			// 时间戳更新
			RankDb::instance()->timeIsOut();

			return;
		}
	}

	// 当前页 和 总页数
	int nPageCur = bean.pagenumber();
	int nPageAll = bean.pageall();

	// 更新 当前页 和 总页数 状态
	RankDataTableView::instance()->refreshDataTableStatus(nPageCur, nPageAll);

	// 计算鲜花type
	int nFlowerType = 0;
	if (5 == nType)
	{
		nFlowerType = nWork;
	}

	// 清空RankData中m_vector_player_internet的数据
	RankData::instance()->clearVectorPlayerInternet();

	// roleMessage添加到vector
	int nSizeRoleMessage = bean.roles_size();
	for (int i = 0; i < nSizeRoleMessage; i++)
	{
		CRoleMessage * roleMesage = new CRoleMessage();
		roleMesage->CopyFrom(bean.roles(i));
		roleMesage->set_rankType(nType);
		roleMesage->set_timeFlag(RankData::instance()->get_timeFlag());															// 插入的时间戳（需要运算）
		roleMesage->set_pageCur(nPageCur);
		roleMesage->set_pageAll(nPageAll);
		roleMesage->set_myPlayerRank(nMyPlayerRank);
		roleMesage->set_myPlayerId(longMyPlayerId);
		roleMesage->set_timeRefreshFlag(RankData::instance()->get_timeRefreshFlag());
		roleMesage->set_myPlayerName(str_myPlayerName);
		roleMesage->set_flowerType(nFlowerType);
		RankData::instance()->m_vector_player_internet.push_back(roleMesage);
	}

	// tableName
	std::string strTableName = "";
	if (0 == nWork && 3 != nType)
	{
		strTableName = "t_rank_player_all";
	}
	else if (0 == nWork && 3 == nType)
	{
		strTableName = "t_rank_country_all";
	}
	else
	{
		strTableName = "t_rank_player";
	}

	// 从db中取数据到 vector(临时的vector)
	std::vector<CRoleMessage *> tmpVector;
	RankDb::instance()->getDataFromDb(tmpVector, nType, nWork);
	int nSizeVector = tmpVector.size();
	int nPageCurDb = nSizeVector / PAGE_NUM;


	// 当 服务器 给予的数据的 当前页 大于 db中的页数时，才会插入db
	if (nPageCurDb - 1< nPageCur)
	{
		// 将数据插入数据库
		RankDb::instance()->insertData(strTableName, RankData::instance()->m_vector_player_internet);
	}
	else
	{
		// 更新 当前页 和 总页数 状态
		RankDataTableView::instance()->refreshDataTableStatus(nPageCurDb - 1, nPageAll);
	}

	// 从db中取数据到 vector(m_vector_player_db)
	RankDb::instance()->getDataFromDb(RankData::instance()->m_vector_player_db, nType, nWork);

	// 通知 更新UI数据
	if (NULL != GameView::getInstance()->getMainUIScene())
	{
		RankUI* pRankUI = (RankUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRankUI);
		if (NULL != pRankUI)
		{
			pRankUI->reloadDataTableView();
		}
	}


	// 清除临时vector
	std::vector<CRoleMessage *>::iterator iterTmpVector;
	for (iterTmpVector = tmpVector.begin(); iterTmpVector != tmpVector.end(); iterTmpVector++)
	{
		delete *iterTmpVector;
	}
	tmpVector.clear();
}