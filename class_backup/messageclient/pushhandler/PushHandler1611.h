
#ifndef Blog_C___Reflection_PushHandler1611_h
#define Blog_C___Reflection_PushHandler1611_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1611 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1611)
    
public:
    SYNTHESIZE(PushHandler1611, int*, m_pValue)
    
    PushHandler1611() ;
    virtual ~PushHandler1611() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
