
#ifndef Blog_C___Reflection_PushHandler5073_h
#define Blog_C___Reflection_PushHandler5073_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5073 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5073)

public:
	SYNTHESIZE(PushHandler5073, int*, m_pValue)

		PushHandler5073() ;
	virtual ~PushHandler5073() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
