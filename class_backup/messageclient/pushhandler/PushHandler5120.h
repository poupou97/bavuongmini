
#ifndef Blog_C___Reflection_PushHandler5120_h
#define Blog_C___Reflection_PushHandler5120_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5120 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5120)

public:
	SYNTHESIZE(PushHandler5120, int*, m_pValue)

		PushHandler5120() ;
	virtual ~PushHandler5120() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
