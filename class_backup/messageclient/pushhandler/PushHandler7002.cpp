#include "PushHandler7002.h"
#include "../protobuf/MainMessage.pb.h"
#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/OtherPlayer.h"
#include "../../gamescene_state/role/OtherPlayerOwnedStates.h"
#include "../../gamescene_state/role/BaseFighterOwnedCommand.h"
#include "../../gamescene_state/role/Monster.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../element/FolderInfo.h"
#include "../../ui/backpackscene/PackageItem.h"
#include "../protobuf/MissionMessage.pb.h"
#include "../../ui/missionscene/MissionManager.h"
#include "../element/MissionInfo.h"
#include "../../ui/missionscene/MissionScene.h"
#include "../../gamescene_state/role/FunctionNPC.h"
#include "../../gamescene_state/sceneelement/MissionAndTeam.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/extensions//UITab.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "../../utils/GameUtils.h"
#include "../../gamescene_state/MainAnimationScene.h"
#include "../../gamescene_state/EffectDispatch.h"
#include "../../utils/StaticDataManager.h"

using namespace CocosDenshion;

IMPLEMENT_CLASS(PushHandler7002)

PushHandler7002::PushHandler7002() 
{

}
PushHandler7002::~PushHandler7002() 
{

}
void* PushHandler7002::createInstance()
{
	return new PushHandler7002() ;
}
void PushHandler7002::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler7002::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler7002::handle(CommonMessage* mb)
{
	Push7002 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	std::string pacId = bean.missionpackageid();
	//std::string missId = bean.missionid();
	MissionState missionState = bean.missionstate();
	//std::string des = bean.description();

	MissionInfo * curMission = new MissionInfo();

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if (missionState == canceled || missionState == submitted || missionState == abandoned)
	{
		GameUtils::playGameSound(MISSION_COMPLETED, 2, false);
		for (int i = 0;i<GameView::getInstance()->missionManager->MissionList.size();i++)
		{
			MissionInfo * tempMInfo = GameView::getInstance()->missionManager->MissionList.at(i);
			if (strcmp(tempMInfo->missionpackageid().c_str(),pacId.c_str()) == 0)
			{
					vector<MissionInfo*>::iterator iter = GameView::getInstance()->missionManager->MissionList.begin()+i;
					curMission->CopyFrom(*tempMInfo);
					GameView::getInstance()->missionManager->MissionList.erase(iter);
					delete tempMInfo;
			}
		}
		for (int i = 0;i<GameView::getInstance()->missionManager->MissionList_MainScene.size();i++)
		{
			MissionInfo * tempInfo = GameView::getInstance()->missionManager->MissionList_MainScene.at(i);
			if (strcmp(tempInfo->missionpackageid().c_str(),pacId.c_str()) == 0)
			{
				GameView::getInstance()->missionManager->m_sLastMissionPackageId = tempInfo->missionpackageid();
				GameView::getInstance()->missionManager->m_nLastMissionIndexInMain = i;

				vector<MissionInfo*>::iterator iter = GameView::getInstance()->missionManager->MissionList_MainScene.begin()+i;
				GameView::getInstance()->missionManager->MissionList_MainScene.erase(iter);
				delete tempInfo;
			}
		}


		//scene 
		// it's not game state, ignore
		CCScene* pScene = CCDirector::sharedDirector()->getRunningScene();
		if(pScene->getTag() != GameView::STATE_GAME)
		{
			return;
		}
		else
		{
			GameSceneState* pState = (GameSceneState*)pScene;
			if(pState->getState() == GameSceneState::state_load)
			{
				return;
			}
		}
		/*
		MainAnimationScene * mainAnmScene = GameView::getInstance()->getMainAnimationScene();
		if (mainAnmScene)
		{
			mainAnmScene->addInterfaceAnm("wcrw/wcrw.anm");
		}
		*/

		if (missionState == submitted)
		{
			EffectOfNormal * effect_normal = new EffectOfNormal();
			effect_normal->setEffectStrPath("wcrw/wcrw.anm");
			effect_normal->setType(type_normal);
			EffectDispatcher::pushEffectToVector(effect_normal);
		}
		else if (missionState == canceled || missionState == abandoned)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("mission_cancled"));
		}

 		MainScene * mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
// 		if (missionState == submitted)
// 		{
// 			mainscene->addInterfaceAnm("wcrw/wcrw.anm");
// 		}

		if (mainscene->getChildByTag(kTagMissionAndTeam) != NULL)
		{
			MissionAndTeam * missionAndTeam = (MissionAndTeam *)mainscene->getChildByTag(kTagMissionAndTeam);
			GameView::getInstance()->missionManager->m_pLastMissionOffSet = missionAndTeam->missionTableView->getContentOffset();
			GameView::getInstance()->missionManager->m_missionTableViewSize = missionAndTeam->missionTableView->getContentSize();
			missionAndTeam->reloadMissionData();
		}

		if (mainscene->getChildByTag(kTagMissionScene) != NULL)
		{
			MissionScene * missionScene = (MissionScene *)mainscene->getChildByTag(kTagMissionScene);
			if (missionScene->mainTab->getCurrentIndex() == 0)
			{
				missionScene->updataCurDataSourceByMissionState(1);
			}
			else if (missionScene->mainTab->getCurrentIndex() == 1)
			{
				missionScene->updataCurDataSourceByMissionState(0);
			}

			//reload
			//missionScene->m_tableView->reloadData();
			missionScene->ReloadDataWithOutChangeOffSet();
			//����Ĭ��
			if (missionScene->curMissionDataSource.size()>0)
			{
				if (missionScene->curMissionDataSource.size() > missionScene->selectCellIdx)
				{
				}
				else if(missionScene->curMissionDataSource.size() == missionScene->selectCellIdx)
				{
					missionScene->selectCellIdx -= 1;
				}
				else
				{
					missionScene->selectCellIdx = 0;
				}

				missionScene->m_tableView->selectCell(missionScene->selectCellIdx);
				missionScene->curMissionInfo->CopyFrom(*(missionScene->curMissionDataSource.at(missionScene->selectCellIdx)));
				missionScene->ReloadMissionInfoData();
			}
		}


		if (curMission->action().moveto().has_targetnpc())
		{
			FunctionNPC* fn = dynamic_cast<FunctionNPC*>(scene->getActor(curMission->action().moveto().targetnpc().npcid()));
			// 			for(int j = 0;j<fn->npcMissionList.size();j++)
			// 			{
			// 				if (strcmp(fn->npcMissionList.at(j)->missionpackageid().c_str(),pacId.c_str()) == 0)
			// 				{
			// 					if (strcmp(fn->npcMissionList.at(j)->missionid().c_str(),missId.c_str()) == 0)
			// 					{
			// 						vector<MissionInfo*>::iterator iter = fn->npcMissionList.begin()+j;

			// 						MissionInfo * tm = *iter;

			// 						fn->npcMissionList.erase(iter);
			// 						delete tm;
			// 					}
			// 				}
			// 			}
			if (fn != NULL)
			{
				fn->RefreshNpc();

				GameView::getInstance()->missionManager->autoPopUpHandleMission();
			}
		}
	}
	else
	{
	}

	delete curMission;
}
