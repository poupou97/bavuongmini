
#include "PushHandler3020.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "GameView.h"
#include "../protobuf/OfflineFight.pb.h"
#include "../../ui/offlinearena_ui/OffLineArenaUI.h"
#include "../../ui/offlinearena_ui/OffLineArenaResultUI.h"
#include "../../ui/offlinearena_ui/OffLineArenaCountDownUI.h"
#include "../../ui/offlinearena_ui/OffLineArenaState.h"
#include "../../utils/GameUtils.h"
#include "../../ui/offlinearena_ui/OffLineArenaData.h"

IMPLEMENT_CLASS(PushHandler3020)

PushHandler3020::PushHandler3020() 
{

}
PushHandler3020::~PushHandler3020() 
{

}
void* PushHandler3020::createInstance()
{
	return new PushHandler3020() ;
}
void PushHandler3020::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler3020::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler3020::handle(CommonMessage* mb)
{
	Resp3020 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());


	if (GameView::getInstance()->getGameScene() == NULL)
		return;

	if (GameView::getInstance()->getMainUIScene() == NULL)
		return;

	if (bean.has_error())
	{
		//GameView::getInstance()->showAlertDialog(bean.error());
	}
	else
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		switch(bean.status())
		{
		case 1:      //进入(显示倒计时)
			{
				OffLineArenaData::getInstance()->setCurStatus(OffLineArenaData::s_inCountDown);
				OffLineArenaState::setStartTime(GameUtils::millisecondNow());
				OffLineArenaCountDownUI * offLineArenaCountDownUI = OffLineArenaCountDownUI::create();
				offLineArenaCountDownUI->ignoreAnchorPointForPosition(false);
				offLineArenaCountDownUI->setAnchorPoint(ccp(0.5f,0.5f));
				offLineArenaCountDownUI->setPosition(ccp(winSize.width/2,winSize.height/2));
				GameView::getInstance()->getMainUIScene()->addChild(offLineArenaCountDownUI);
			}
			break;
		case 2:      //成功退出
			{
				OffLineArenaData::getInstance()->setCurStatus(OffLineArenaData::s_none);
				if(GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOffLineArenaResultUI) == NULL)
				{
					OffLineArenaResultUI * resultUI = OffLineArenaResultUI::create(true,bean.exp());
					resultUI->ignoreAnchorPointForPosition(false);
					resultUI->setAnchorPoint(ccp(0.5f,0.5f));
					resultUI->setPosition(ccp(winSize.width/2,winSize.height/2));
					resultUI->setTag(kTagOffLineArenaResultUI);
					GameView::getInstance()->getMainUIScene()->addChild(resultUI);
				}
			}
			break;
		case 3:     //失败退出
			{
				OffLineArenaData::getInstance()->setCurStatus(OffLineArenaData::s_none);
				if(GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOffLineArenaResultUI) == NULL)
				{
					OffLineArenaResultUI * resultUI = OffLineArenaResultUI::create(false,bean.exp());
					resultUI->ignoreAnchorPointForPosition(false);
					resultUI->setAnchorPoint(ccp(0.5f,0.5f));
					resultUI->setPosition(ccp(winSize.width/2,winSize.height/2));
					resultUI->setTag(kTagOffLineArenaResultUI);
					GameView::getInstance()->getMainUIScene()->addChild(resultUI);
				}
			}
			break;
		case 4:    //领取奖励退出
			{
				OffLineArenaData::getInstance()->setCurStatus(OffLineArenaData::s_none);
			}
			break;
		}

	}
}
