#include "PushHandler1129.h"

#include "../protobuf/NPCMessage.pb.h"

#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../element/CNpcDialog.h"
#include "../../ui/npcscene/NpcTalkWindow.h"

IMPLEMENT_CLASS(PushHandler1129)

PushHandler1129::PushHandler1129() 
{

}
PushHandler1129::~PushHandler1129() 
{

}
void* PushHandler1129::createInstance()
{
	return new PushHandler1129() ;
}
void PushHandler1129::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1129::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1129::handle(CommonMessage* mb)
{
	Rsp1129 bean;
	bean.ParseFromString(mb->data());

	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	CNpcDialog * npcDialog = new CNpcDialog();
	npcDialog->CopyFrom(bean.npcdialog());
	                
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHandleMission))
	{
		GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHandleMission)->removeFromParent();
	}
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkWithNpc))
	{
		GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkWithNpc)->removeFromParent();
	}
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkMissionUI))
	{
		GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkMissionUI)->removeFromParent();
	}

	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagNpcTalkWindow) == NULL)
	{
		CCSize winSize=CCDirector::sharedDirector()->getVisibleSize();
		NpcTalkWindow * npcTalkWindow = NpcTalkWindow::create(npcDialog);
		if (npcTalkWindow)
		{
			GameView::getInstance()->getMainUIScene()->addChild(npcTalkWindow,0,kTagNpcTalkWindow);
			npcTalkWindow->ignoreAnchorPointForPosition(false);
			npcTalkWindow->setAnchorPoint(ccp(0,0.5f));
			npcTalkWindow->setPosition(ccp(0,winSize.height/2));
		}
	}
}