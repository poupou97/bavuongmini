
#ifndef Blog_C___Reflection_PushHandler5116_h
#define Blog_C___Reflection_PushHandler5116_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5116 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5116)

public:
	SYNTHESIZE(PushHandler5116, int*, m_pValue)

	PushHandler5116() ;
	virtual ~PushHandler5116() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
