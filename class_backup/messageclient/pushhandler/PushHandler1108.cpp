
#include "PushHandler1108.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/OtherPlayer.h"
#include "../../gamescene_state/role/OtherPlayerOwnedStates.h"
#include "../../gamescene_state/role/BaseFighterOwnedCommand.h"
#include "../../GameView.h"
#include "../../gamescene_state/role/Monster.h"

IMPLEMENT_CLASS(PushHandler1108)

PushHandler1108::PushHandler1108() 
{

}
PushHandler1108::~PushHandler1108() 
{

}
void* PushHandler1108::createInstance()
{
	return new PushHandler1108() ;
}
void PushHandler1108::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1108::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1108::handle(CommonMessage* mb)
{
	Push1108 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, player id: %lld, logout", mb->cmdid(), bean.playerid());

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;
	GameActor* actor = scene->getActor(bean.playerid());
	if(actor != NULL)
	{
		Monster* pMonster = dynamic_cast<Monster*>(actor);
		if(pMonster != NULL)
		{
			// this actor will be dead, so let it be
			if(pMonster->isAction(ACT_DIE))
				return;

			pMonster->changeAction(ACT_DISAPPEAR);
			return;
		}

		actor->setDestroy(true);
	}

}
