
#ifndef Blog_C___Reflection_PushHandler1138_h
#define Blog_C___Reflection_PushHandler1138_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler1138 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1138)
public:
	SYNTHESIZE(PushHandler1138, int*, m_pValue)

		PushHandler1138() ;
	virtual ~PushHandler1138() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;
} ;

#endif