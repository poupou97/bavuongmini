
#ifndef Blog_C___Reflection_PushHandler7018_h
#define Blog_C___Reflection_PushHandler7018_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler7018 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler7018)

public:
	SYNTHESIZE(PushHandler7018, int*, m_pValue)

		PushHandler7018() ;
	virtual ~PushHandler7018() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

private:

protected:
	int *m_pValue ;

} ;

#endif
