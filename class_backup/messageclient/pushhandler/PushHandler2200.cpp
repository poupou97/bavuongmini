
#include "PushHandler2200.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/OtherPlayer.h"
#include "../../gamescene_state/role/OtherPlayerOwnedStates.h"
#include "../../gamescene_state/role/BaseFighterOwnedCommand.h"

#include "../protobuf/RelationMessage.pb.h"
#include "GameView.h"
#include "../element/CRelationPlayer.h"
#include "../../ui/Friend_ui/FriendWorkList.h"
#include "../../ui/Friend_ui/FriendUi.h"

IMPLEMENT_CLASS(PushHandler2200)

PushHandler2200::PushHandler2200() 
{
    
}
PushHandler2200::~PushHandler2200() 
{
    
}
void* PushHandler2200::createInstance()
{
    return new PushHandler2200() ;
}
void PushHandler2200::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler2200::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler2200::handle(CommonMessage* mb)
{
	Rsp2200 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());
	
	bean.targetid();//操作的目标玩家ID
	int operator_= bean.operation();//0-添加，1-删除 -1
	int type = bean.relationtype();// 类型, 0-好友, 1-黑名单，2-仇人
	int resultid= bean.result();// 0 成功 1 失败
	std::string result = bean.reason();//成功:null,失败:原因

	if (resultid==1)
	{
		GameView::getInstance()->showAlertDialog(result);
		return;
	}else
	{
		GameView::getInstance()->showAlertDialog(result);
	}

	CRelationPlayer * player=new CRelationPlayer();
	std::vector<CRelationPlayer *>::iterator iter;
	for (int i=0;i<GameView::getInstance()->relationFriendVector.size();i++)
	{
		if (bean.targetid()==GameView::getInstance()->relationFriendVector.at(i)->playerid())
		{
			iter = GameView::getInstance()->relationFriendVector.begin()+i;
			player->CopyFrom(**iter);
		}

	}

	for (int i=0;i<GameView::getInstance()->relationBlackVector.size();i++)
	{
		if (bean.targetid()==GameView::getInstance()->relationBlackVector.at(i)->playerid())
		{
			iter = GameView::getInstance()->relationBlackVector.begin()+i;
			player->CopyFrom(**iter);
	
		}
	}

	for (int i=0;i<GameView::getInstance()->relationEnemyVector.size();i++)
	{
		if (bean.targetid()==GameView::getInstance()->relationEnemyVector.at(i)->playerid())
		{
			iter =GameView::getInstance()->relationEnemyVector.begin()+i;
			player->CopyFrom(**iter);
		}
	}

	if (operator_==0)
	{
		switch (type)
		{
		case 0:
			{
				GameView::getInstance()->relationFriendVector.push_back(player);
			}break;
		case 1:
			{
				GameView::getInstance()->relationBlackVector.push_back(player);
			}break;
		case 2:
			{
				GameView::getInstance()->relationEnemyVector.push_back(player);
			}break;
		}
	}else
		if (operator_==1)
		{
			switch(type)
			{
			case 0:
				{
					for (int i=0;i<GameView::getInstance()->relationFriendVector.size();i++)
					{
						if (bean.targetid()==GameView::getInstance()->relationFriendVector.at(i)->playerid())
						{
							GameView::getInstance()->relationFriendVector.erase(GameView::getInstance()->relationFriendVector.begin()+i);

							delete *(GameView::getInstance()->relationFriendVector.begin()+i);
						}
					}
					
				}break;
			case 1:
				{
					for (int i=0;i<GameView::getInstance()->relationBlackVector.size();i++)
					{
						if (bean.targetid()==GameView::getInstance()->relationBlackVector.at(i)->playerid())
						{
							GameView::getInstance()->relationBlackVector.erase(GameView::getInstance()->relationFriendVector.begin()+i);
							delete *(GameView::getInstance()->relationFriendVector.begin()+i);
						}
					}

				}break;
			case 2:
				{
					for (int i=0;i<GameView::getInstance()->relationEnemyVector.size();i++)
					{
						if (bean.targetid()==GameView::getInstance()->relationEnemyVector.at(i)->playerid())
						{
							GameView::getInstance()->relationEnemyVector.erase(GameView::getInstance()->relationFriendVector.begin()+i);
							delete *(GameView::getInstance()->relationEnemyVector.begin()+i);
						}
					}
				}break;
			}
		}
	FriendUi *friendui= (FriendUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);
	if (friendui !=NULL)
	{
		FriendWorkList * friendworklist_ =(FriendWorkList *)friendui->getChildByTag(FRIENDADDLAYER)->getChildByTag(FRIENDWORKLISTTAG);
		CCTableView * friendTab =(CCTableView *)friendworklist_->getChildByTag(FRIENDWORKLISTTAB);
		friendTab->reloadData();
	}
	

}
