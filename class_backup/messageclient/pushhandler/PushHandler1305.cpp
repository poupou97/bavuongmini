
#include "PushHandler1305.h"

#include "../protobuf/ItemMessage.pb.h"  
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/OtherPlayer.h"
#include "../../gamescene_state/role/OtherPlayerOwnedStates.h"
#include "../../gamescene_state/role/BaseFighterOwnedCommand.h"
#include "../../gamescene_state/role/Monster.h"
#include "../element/FolderInfo.h"
#include "../../ui/backpackscene/PackageItem.h"
#include "../../ui/backpackscene/PackageScene.h"
#include "../../ui/Mail_ui/MailUI.h"
#include "../../GameView.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "../../ui/storehouse_ui/StoreHouseUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/shop_ui/ShopUI.h"
#include "../../ui/equipMent_ui/EquipMentUi.h"
#include "../element/GoodsInfo.h"
#include "../../ui/equipMent_ui/SystheSisUI.h"
#include "../../messageclient/element/CSysthesisInfo.h"
#include "../../utils/StrUtils.h"
#include "../../utils/GameUtils.h"
#include "../../ui/GameUIConstant.h"
#include "../../ui/generals_ui/GeneralsListUI.h"
#include "../../ui/generals_ui/GeneralsListBase.h"
#include "../element/CGeneralBaseMsg.h"
#include "../element/CEquipment.h"
#include "../../gamescene_state/EffectDispatch.h"
#include "../../ui/extensions/ShortCutItem.h"

IMPLEMENT_CLASS(PushHandler1305)

PushHandler1305::PushHandler1305() 
{

}
PushHandler1305::~PushHandler1305() 
{

}
void* PushHandler1305::createInstance()
{
	return new PushHandler1305() ;
}
void PushHandler1305::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1305::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1305::handle(CommonMessage* mb)
{
	Push1305 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, you got items, change cause: = %d", mb->cmdid(), bean.changecause());
	CCSize winsize =CCDirector::sharedDirector()->getVisibleSize();

	int type = bean.source();
	if (type == 0)
	{

	}
	else if (type == 1)
	{
		if(bean.changecause()&&bean.changecause()!=8)
		{
			//GameView::getInstance()->myplayer->onGotItem(bean.folder().goods());
			GoodsInfo * goods_ = new GoodsInfo();
			goods_->CopyFrom(bean.folder().goods());
			EffectOfItemWave * itemwave_effect = new EffectOfItemWave();
			itemwave_effect->setItemInfo(goods_);
			itemwave_effect->setType(type_itemWave);
			EffectDispatcher::pushEffectToVector(itemwave_effect);
			//---------------
			MainScene *mainScene_ =(MainScene *)GameView::getInstance()->getMainUIScene();
			//弹出快捷使用道具框
			if((bean.folder().goods().additionproperty() & 0X00080000 ) != 0)
			{
				if (mainScene_ != NULL)
				{
					FolderInfo * temp_folder = new FolderInfo();
					temp_folder->CopyFrom(bean.folder());
					
					EffectEquipItem * effect_teach = new EffectEquipItem();
					effect_teach->setType(type_equipWave);
					effect_teach->setFolderInfo(temp_folder);
					effect_teach->setId(0);
					effect_teach->setPriority(false);
					EffectDispatcher::pushEffectToVector(effect_teach);

					FolderInfo * item_folder = new FolderInfo();
					item_folder->CopyFrom(bean.folder());
					EffectEquipItem::folderItemVector.push_back(item_folder);
				}
			}
			///equipment 
			FolderInfo * folder_ = new FolderInfo();
			folder_->CopyFrom(bean.folder());
			int playerLevel = GameView::getInstance()->myplayer->getActiveRole()->level();
			int playerPression_ = GameView::getInstance()->myplayer->getProfession();

			int equipLevel_ = folder_->goods().uselevel();
			int equipPression_ = folder_->goods().equipmentdetail().profession();
			
			int playerEquipFight_ = 0;
			for (int i=0;i<GameView::getInstance()->EquipListItem.size();i++)
			{
				if (folder_->goods().equipmentclazz() == GameView::getInstance()->EquipListItem.at(i)->goods().equipmentclazz())
				{
					playerEquipFight_ = GameView::getInstance()->EquipListItem.at(i)->goods().equipmentdetail().fightcapacity();
				}
			}
			
			//level  profession general role
			int curRoleLevel_ = GameView::getInstance()->myplayer->getActiveRole()->level();
			int openlevel = 0;
			std::map<int,int>::const_iterator cIter;
			cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(26);
			if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end())
			{
			}
			else
			{
				openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[26];
			}

			//快捷穿装：优先于主角，N级之前不考虑武将
			if (playerLevel >= equipLevel_ && playerPression_ == equipPression_ && folder_->goods().equipmentdetail().fightcapacity() > playerEquipFight_)
			{
				FolderInfo * item_folder = new FolderInfo();
				item_folder->CopyFrom(bean.folder());
				EffectEquipItem::folderItemVector.push_back(item_folder);
				
				EffectEquipItem * effect_teach = new EffectEquipItem();
				effect_teach->setType(type_equipWave);
				effect_teach->setFolderInfo(folder_);
				effect_teach->setId(0);
				effect_teach->setPriority(false);
				EffectDispatcher::pushEffectToVector(effect_teach);

			}else
			{
				if (curRoleLevel_ >= openlevel)
				{
					bool isFind_ = false;
					for (int i = 0;i<GameView::getInstance()->generalsInLineList.size();i++)
					{
						if (isFind_ == true)
						{
							break;
						}

						for (int j = 0;j< GameView::getInstance()->generalsInLineDetailList.size();j++)
						{
							if (GameView::getInstance()->generalsInLineList.at(i)->id() == GameView::getInstance()->generalsInLineDetailList.at(j)->generalid())
							{
								int generalLevel_ = GameView::getInstance()->generalsInLineDetailList.at(j)->activerole().level();
								std::string generalPression_str = GameView::getInstance()->generalsInLineDetailList.at(j)->profession();
								int generalPression_id = BasePlayer::getProfessionIdxByName(generalPression_str.c_str());
								int generalEquipFight_ = 0;
								for (int index_ = 0;index_<GameView::getInstance()->generalsInLineDetailList.at(j)->equipments_size();index_++)
								{
									if (GameView::getInstance()->generalsInLineDetailList.at(j)->equipments(index_).goods().equipmentclazz() == folder_->goods().equipmentclazz())
									{
										generalEquipFight_ = GameView::getInstance()->generalsInLineDetailList.at(j)->equipments(index_).goods().equipmentdetail().fightcapacity();
									}
								}
								long long generalId_ = GameView::getInstance()->generalsInLineDetailList.at(j)->generalid();
								if (generalLevel_ >= equipLevel_ && generalPression_id == equipPression_ && folder_->goods().equipmentdetail().fightcapacity() > generalEquipFight_)
								{
									FolderInfo * item_folder = new FolderInfo();
									item_folder->CopyFrom(bean.folder());
									EffectEquipItem::folderItemVector.push_back(item_folder);
									
									EffectEquipItem * effect_teach = new EffectEquipItem();
									effect_teach->setType(type_equipWave);
									effect_teach->setFolderInfo(folder_);
									effect_teach->setId(generalId_);
									effect_teach->setPriority(false);
									EffectDispatcher::pushEffectToVector(effect_teach);

									isFind_ = true;
									break;
								}
							}
						}
					}
				}
			}
			//// show item's name
			//std::string wholeString = StringDataManager::getString("mission_self_recieved");
			//ccColor3B color = GameView::getInstance()->getGoodsColorByQuality(folder_->goods().quality());
			//wholeString.append(StrUtils::applyColor(folder_->goods().name().c_str(), color));
			//GameView::getInstance()->showAlertDialog(wholeString.c_str());
			
			//delete folder_;

		}
		int id = bean.folder().id();
		int pageIndex  = 0 ;
		//change data
		FolderInfo * oldFolderInfo = new FolderInfo();
		if (id<GameView::getInstance()->AllPacItem.size())
		{
			oldFolderInfo->CopyFrom(*GameView::getInstance()->AllPacItem.at(id));
		}
		
		if (id <GameView::getInstance()->AllPacItem.size())
		{
			FolderInfo * f = GameView::getInstance()->AllPacItem.at(id);
			f->CopyFrom(bean.folder());
			GameView::getInstance()->pacPageView->ReloadOnePacItem(f);
		}
		else
		{
			FolderInfo * f = new FolderInfo();
			f->CopyFrom(bean.folder());
			GameView::getInstance()->AllPacItem.push_back(f);
			GameView::getInstance()->pacPageView->ReloadOnePacItem(f);
		}

		//yangjun 2014/7/ 
		//refresh SysthesisUI
// 		if (GameView::getInstance()->getGameScene())
// 		{
// 			if (GameView::getInstance()->getMainUIScene())
// 			{
// 				EquipMentUi * equipmentUI = (EquipMentUi*)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
// 				if (equipmentUI)
// 				{
// 					if (equipmentUI->synthesisLayer->isVisible())
// 					{
// 						SysthesisUI * systhesisiUI = (SysthesisUI*)equipmentUI->synthesisLayer->getChildByTag(CLASS_SYSTHESIS_TAG);
// 						if (systhesisiUI)
// 						{
// 							if (systhesisiUI->getCurSysthesisInfo() != NULL)
// 							{
// 								long long startTime = GameUtils::millisecondNow();
// 								systhesisiUI->refreshUI(systhesisiUI->getCurSysthesisInfo());
//  								systhesisiUI->refreshTableViewWithoutChangeOffSet();
//  								systhesisiUI->addBonusSpecialEffect();
// 								long long endTime = GameUtils::millisecondNow();
// 								CCLOG("time time time = %ld",endTime-startTime);
// 							}
// 						}
// 					}
// 				}
// 			}
// 		}
		
		//refresh CD
		if (bean.changecause() == 8)  //变动原因是药品使用
		{
			if (GameView::getInstance()->getGameScene())
			{
				if (GameView::getInstance()->getMainUIScene())
				{
				 	if (!bean.folder().has_goods())
				 		return;

					GameView::getInstance()->pacPageView->RefreshCD(bean.folder().goods().id());
				}
			}
		}

		//购买
		if ( bean.changecause() == 2)
		{
		}

		//change by yangjun 2014.2.21
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer))
		{
			ShortcutLayer * shortcutLayer = (ShortcutLayer *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
			FolderInfo * f = new FolderInfo();
			f->CopyFrom(bean.folder());
			if ((oldFolderInfo->has_goods()) && (!f->has_goods()))
			{
				shortcutLayer->RefreshOne(oldFolderInfo);
			}

			if (f->has_goods())
			{
				if (f->goods().canshortcut())
				{
					shortcutLayer->RefreshOne(f);
				}
			}
			
			delete f;
		}
		delete oldFolderInfo;
	}
	else if (type == 2)
	{
		int id = bean.folder().id();
		//change data
		if (GameView::getInstance()->storeHouseItemList.size()>id)
		{
			FolderInfo * f = GameView::getInstance()->storeHouseItemList.at(id);
			f->CopyFrom(bean.folder());

			StoreHouseUI * storeHouseUI = (StoreHouseUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStoreHouseUI);
			if (storeHouseUI != NULL)
			{
				storeHouseUI->ReloadOneStoreHouseItem(f);
			}
		}
		else
		{
			FolderInfo * f = new FolderInfo();
			f->CopyFrom(bean.folder());
			GameView::getInstance()->storeHouseItemList.push_back(f);

			StoreHouseUI * storeHouseUI = (StoreHouseUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStoreHouseUI);
			if (storeHouseUI != NULL)
			{
				storeHouseUI->ReloadOneStoreHouseItem(f);
			}
		}
	}
	else
	{}

	EffectEquipItem::reloadShortItem();
	//equipmentui 
	EquipMentUi * equipui = (EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
	if (equipui != NULL)
	{
		if (GameView::getInstance()->AllPacItem.at(bean.folder().id())->has_goods())
		{
			int curClazz = bean.folder().goods().clazz();
			int equipMentClazz = bean.folder().goods().equipmentclazz();

			if (equipui->mainEquipIndexRefine ==  bean.folder().id())
			{
				GameView::getInstance()->pacPageView->SetCurFolderGray(true,bean.folder().id());
			}
			
			switch(equipui->currType)
			{
			case REFININGTYPE:
				{
					if (curClazz != GOODS_CLASS_JINGLIANCAILIAO && !(equipMentClazz > 0 && equipMentClazz<10))
					{
						GameView::getInstance()->pacPageView->getPackageItem(bean.folder().id())->setAvailable(false);
					}
				}break;
			case STARTYPE:
				{
					if (curClazz != GOODS_CLASS_SHENGXINGSHI && !(equipMentClazz > 0 && equipMentClazz<10))
					{
						GameView::getInstance()->pacPageView->getPackageItem(bean.folder().id())->setAvailable(false);
					}
				}break;
			case GEMTYPE:
				{
					if ( curClazz != GOODS_CLASS_XIANGQIANCAILIAO)
					{
						GameView::getInstance()->pacPageView->getPackageItem(bean.folder().id())->setAvailable(false);
					}else
					{
						GameView::getInstance()->pacPageView->getPackageItem(bean.folder().id())->setAvailable(true);
						GameView::getInstance()->pacPageView->getPackageItem(bean.folder().id())->setBoundJewelPropertyVisible(true);
					}
				}break;
			case BAPTIZETYPE:
				{
					GoodsInfo * goods = new GoodsInfo();
					goods->CopyFrom(bean.folder().goods());
					if (!(equipMentClazz >0 &&equipMentClazz < 10) || (equipui->isCanBaptize(goods) == false) )
					{
						GameView::getInstance()->pacPageView->getPackageItem(bean.folder().id())->setAvailable(false);
					}
				}break;
			}
		}
	}
}
