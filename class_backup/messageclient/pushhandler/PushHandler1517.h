
#ifndef Blog_C___Family_PushHandler1517_h
#define Blog_C___Family_PushHandler1517_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1517 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1517)
    
public:
    SYNTHESIZE(PushHandler1517, int*, m_pValue)
    
    PushHandler1517() ;
    virtual ~PushHandler1517() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
