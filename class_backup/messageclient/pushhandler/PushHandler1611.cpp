
#include "PushHandler1611.h"

#include "../protobuf/EquipmentMessage.pb.h"  
#include "../element/CAdditionProperty.h"
#include "GameView.h"
#include "../../ui/equipMent_ui/EquipMentUi.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/GameSceneEffects.h"
#include "../../ui/GameUIConstant.h"
#include "../../gamescene_state/EffectDispatch.h"



IMPLEMENT_CLASS(PushHandler1611)

PushHandler1611::PushHandler1611() 
{
    
}
PushHandler1611::~PushHandler1611() 
{
    
}
void* PushHandler1611::createInstance()
{
	return new PushHandler1611() ;
}
void PushHandler1611::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1611::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1611::handle(CommonMessage* mb)
{
	Rsp1611 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());
	
	EquipMentUi * equip= (EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
	/*
	if (bean.result()==1)
	{
		const char *str_ = StringDataManager::getString("equip_baptize_success");
		GameView::getInstance()->showAlertDialog(str_);
	}else
	{
		const char *str_ = StringDataManager::getString("equip_baptize_fail");
		GameView::getInstance()->showAlertDialog(str_);
	}
	*/
	if (equip->tabviewSelectIndex != 0)
	{
		equip->generalList->generalList_tableView->selectCell(equip->tabviewSelectIndex);
	}

	std::vector<CAdditionProperty *>::iterator iter;
	for (iter = equip->additionProperty_baptize.begin(); iter != equip->additionProperty_baptize.end(); ++iter)
	{
		delete *iter;
	}
	equip->additionProperty_baptize.clear();
	for (int i=0;i<bean.refineproperties_size();i++)
	{
		CAdditionProperty * addproperty_ =new CAdditionProperty();
		addproperty_->CopyFrom(bean.refineproperties(i));
		equip->additionProperty_baptize.push_back(addproperty_);
	}
	
	equip->refreshEquipProperty_baptize(equip->baptizeCost);
	equip->refreshPlayerMoney();

	if (bean.result()==1)
	{
		CCNodeRGBA* pEffect = BonusSpecialEffect::create();
		pEffect->setPosition(ccp(EQUIPMENT_UI_CENTER_SLOT_X, EQUIPMENT_UI_CENTER_SLOT_Y));
		pEffect->setScale(EQUIPMENT_UI_BONUSSPECIALEFFECT_SCALE);
		equip->baptizePropertylayer->addChild(pEffect);
	}

	EffectEquipItem::reloadShortItem();
}
