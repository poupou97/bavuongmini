#ifndef Blog_C___Reflection_PushHandler5137_h
#define Blog_C___Reflection_PushHandler5137_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5137 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5137)

public:
	SYNTHESIZE(PushHandler5137, int*, m_pValue)

	PushHandler5137() ;
	virtual ~PushHandler5137() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
