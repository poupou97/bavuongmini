
#ifndef Blog_C___Reflection_PushHandler5113_h
#define Blog_C___Reflection_PushHandler5113_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5113 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5113)

public:
	SYNTHESIZE(PushHandler5113, int*, m_pValue)

	PushHandler5113() ;
	virtual ~PushHandler5113() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
