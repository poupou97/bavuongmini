
#include "PushHandler1501.h"
#include "../protobuf/GuildMessage.pb.h"
#include "../element/CGuildBase.h"
#include "../../ui/family_ui/ApplyFamily.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"


IMPLEMENT_CLASS(PushHandler1501)

PushHandler1501::PushHandler1501() 
{
    
}
PushHandler1501::~PushHandler1501() 
{
    
}
void* PushHandler1501::createInstance()
{
    return new PushHandler1501() ;
}
void PushHandler1501::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1501::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1501::handle(CommonMessage* mb)
{
	Push1501 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	bean.page();
	bean.hasmorepage();//
	
	ApplyFamily * applyfamily_ =(ApplyFamily *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagApplyFamilyUI);
	if (applyfamily_ == NULL)
	{
		return;
	}
	
	for (int i=0;i<bean.guilds_size();i++)
	{
		CGuildBase * guilds =new CGuildBase();
		guilds->CopyFrom(bean.guilds(i));

		applyfamily_->vectorGuildBase.push_back(guilds);
	}
	
	if (bean.hasmorepage() ==1)
	{
		applyfamily_->hasNextPage =true;
	}else
	{
		applyfamily_->hasNextPage =false;
	}

	applyfamily_->selfGuildId =bean.selfguildid();//������ļ���id
	applyfamily_->tableviewstate->reloadData();

}
