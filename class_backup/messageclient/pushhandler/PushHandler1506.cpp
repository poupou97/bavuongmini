
#include "PushHandler1506.h"
#include "../protobuf/GuildMessage.pb.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/family_ui/FamilyUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/extensions/CCRichLabel.h"



IMPLEMENT_CLASS(PushHandler1506)

PushHandler1506::PushHandler1506() 
{
    
}
PushHandler1506::~PushHandler1506() 
{
    
}
void* PushHandler1506::createInstance()
{
    return new PushHandler1506() ;
}
void PushHandler1506::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1506::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1506::handle(CommonMessage* mb)
{
	Rsp1506 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	int type_ = bean.type();
	if (type_==0)
	{
		const char *str_ = StringDataManager::getString("family_annoucement_success");	
		GameView::getInstance()->showAlertDialog(str_);
		FamilyUI *familyui =(FamilyUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
		familyui->familyNoticeLabel->setString(familyui->familyNotice);
		int h_ =familyui->familyNoticeLabel->getContentSize().height;
		if (h_ < familyui->noticeScrollView->getContentSize().height)
		{
			h_ =familyui->noticeScrollView->getContentSize().height;
		}

		familyui->noticeScrollView->setContentSize(CCSizeMake(270,h_));
		familyui->familyNoticeLabel->setPosition(ccp(0,familyui->noticeScrollView->getContentSize().height - familyui->familyNoticeLabel->getContentSize().height-10));
	}else
	{
		const char *str_ = StringDataManager::getString("family_annoucement_fail");
		GameView::getInstance()->showAlertDialog(str_);
	}
}
