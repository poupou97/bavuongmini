
#ifndef Blog_C___Reflection_PushHandler1204_h
#define Blog_C___Reflection_PushHandler1204_h

#include "cocos2d.h"
#include "PushHandlerProtocol.h"
#include "../../common/CKBaseClass.h"

USING_NS_CC;

class PushHandler1204 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1204)
    
public:
    SYNTHESIZE(PushHandler1204, int*, m_pValue)
    
    PushHandler1204() ;
    virtual ~PushHandler1204() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;

} ;

#endif
