
#ifndef Blog_C___Reflection_PushHandler7014_h
#define Blog_C___Reflection_PushHandler7014_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler7014 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler7014)

public:
	SYNTHESIZE(PushHandler7014, int*, m_pValue)

	PushHandler7014() ;
	virtual ~PushHandler7014() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

private:

protected:
	int *m_pValue ;

} ;

#endif
