
#ifndef Blog_C___Reflection_PushHandler1627_h
#define Blog_C___Reflection_PushHandler1627_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1627 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1627)

public:
	SYNTHESIZE(PushHandler1627, int*, m_pValue)

		PushHandler1627() ;
	virtual ~PushHandler1627() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
