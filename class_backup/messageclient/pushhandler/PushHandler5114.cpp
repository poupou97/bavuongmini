#include "PushHandler5114.h"
#include "../protobuf/AdditionStore.pb.h"
#include "../../ui/Active_ui/ActiveShopData.h"
#include "../element/CHonorCommodity.h"


IMPLEMENT_CLASS(PushHandler5114)

PushHandler5114::PushHandler5114() 
{

}
PushHandler5114::~PushHandler5114() 
{
	
}
void* PushHandler5114::createInstance()
{
	return new PushHandler5114() ;
}
void PushHandler5114::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5114::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5114::handle(CommonMessage* mb)
{
	Rsp5114 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	int nSize = bean.commodity_size();
	for (int i = 0; i < nSize; i++)
	{
		CHonorCommodity * activeShopSource = new CHonorCommodity();
		activeShopSource->CopyFrom(bean.commodity(i));

		ActiveShopData::instance()->m_vector_activeShopSource.push_back(activeShopSource);
	}
}