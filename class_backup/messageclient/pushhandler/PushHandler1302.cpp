
#include "PushHandler1302.h"

#include "../protobuf/ItemMessage.pb.h"
#include "../element/CDrug.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "GameView.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "../../ui/backpackscene/PackageScene.h"

IMPLEMENT_CLASS(PushHandler1302)

PushHandler1302::PushHandler1302() 
{

}
PushHandler1302::~PushHandler1302() 
{

}
void* PushHandler1302::createInstance()
{
	return new PushHandler1302() ;
}
void PushHandler1302::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1302::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1302::handle(CommonMessage* mb)
{
	Rsp1302 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, the result of using the drug: %s", mb->cmdid(), bean.propid().c_str());

	if(CDrug::isDrug(bean.propid()) )
	{
		CDrug* pDrug = DrugManager::getInstance()->getDrugById(bean.propid());
		pDrug->startCD();

		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer))
		{
			ShortcutLayer * shortcutLayer = (ShortcutLayer *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
			shortcutLayer->RefreshCD(bean.propid());
			GameView::getInstance()->pacPageView->RefreshCD(bean.propid());
		}
	}
}