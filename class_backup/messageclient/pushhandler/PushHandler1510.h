
#ifndef Blog_C___Family_PushHandler1510_h
#define Blog_C___Family_PushHandler1510_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1510 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1510)
    
public:
    SYNTHESIZE(PushHandler1510, int*, m_pValue)
    
    PushHandler1510() ;
    virtual ~PushHandler1510() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
