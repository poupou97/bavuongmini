#include "PushHandler5142.h"

#include "../../GameView.h"
#include "../protobuf/DailyMessage.pb.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/giveFlower/GetFlower.h"
#include "../../gamescene_state/role/MyPlayer.h"

IMPLEMENT_CLASS(PushHandler5142)

PushHandler5142::PushHandler5142() 
{

}
PushHandler5142::~PushHandler5142() 
{
	
}
void* PushHandler5142::createInstance()
{
	return new PushHandler5142() ;
}
void PushHandler5142::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5142::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5142::handle(CommonMessage* mb)
{
	Push5142 bean;
	bean.ParseFromString(mb->data());
	
	if (bean.endtime() > 0 && bean.number() >0)
	{
		//if player is women add particle
		int pressionId_ = GameView::getInstance()->myplayer->getProfession();
		
		if (BasePlayer::getRoleSexByProfessionId(pressionId_) == 2)
		{
			GameView::getInstance()->setFlowerParticleTime(bean.endtime());
			GameView::getInstance()->setFlowerNum(bean.number());
		}
	}
}

