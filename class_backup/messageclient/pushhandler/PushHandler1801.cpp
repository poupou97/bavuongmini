
#include "PushHandler1801.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/OtherPlayer.h"
#include "../../gamescene_state/role/OtherPlayerOwnedStates.h"
#include "../../gamescene_state/role/BaseFighterOwnedCommand.h"
#include "../protobuf/AuctionMessage.pb.h"
#include "../../GameView.h"
#include "../../ui/Auction_ui/AuctionUi.h"
#include "../element/CAuctionInfo.h"
#include "../../ui/Auction_ui/AuctionTabView.h"

IMPLEMENT_CLASS(PushHandler1801)

PushHandler1801::PushHandler1801() 
{
    
}
PushHandler1801::~PushHandler1801() 
{
    
}
void* PushHandler1801::createInstance()
{
    return new PushHandler1801() ;
}
void PushHandler1801::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1801::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1801::handle(CommonMessage* mb)
{
	Rsp1801 bean;
	bean.ParseFromString(mb->data());

	AuctionUi * auctionui =(AuctionUi*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagAuction);
	std::vector<CAuctionInfo*>::iterator iter;
	for (iter=GameView::getInstance()->auctionSourceVector.begin();iter != GameView::getInstance()->auctionSourceVector.end();iter++)
	{
		delete *iter;
	}
	GameView::getInstance()->auctionSourceVector.clear();
	if (auctionui == NULL)
	{
		return;
	}
	
	if (auctionui->setpageindex == 1)
	{
		std::vector<CAuctionInfo*>::iterator iter;
		for(iter=GameView::getInstance()->auctionConsignVector.begin();iter!=GameView::getInstance()->auctionConsignVector.end();iter++)
		{
			delete *iter;
		}
		GameView::getInstance()->auctionConsignVector.clear();
	}
	
	for (int i=0;i<bean.auctioninfos_size();i++)
	{
		CAuctionInfo * auctioninfo1=new CAuctionInfo();
		auctioninfo1->CopyFrom(bean.auctioninfos(i));	
		GameView::getInstance()->auctionConsignVector.push_back(auctioninfo1);
	}

	for (int i=0;i<GameView::getInstance()->auctionConsignVector.size();i++)
	{
		CAuctionInfo * auctions=new CAuctionInfo();
		auctions->CopyFrom(*GameView::getInstance()->auctionConsignVector.at(i));	
		GameView::getInstance()->auctionSourceVector.push_back(auctions);
	}
	//auctionui->setname = "";
	if (bean.hasmorepage())
	{
		auctionui->hasMorePage =true;
	}else
	{
		auctionui->hasMorePage =false;
	}
	CCTableView * auctionTab=(CCTableView *)auctionui->getChildByTag(AUCTIONLAYER)->getChildByTag(AUCTIONTABVIEW)->getChildByTag(AUCTIONCCTABVIEW);
	if (auctionui->setpageindex == 1)
	{
		auctionTab->reloadData();
	}else
	{
		CCPoint off_ = auctionTab->getContentOffset();
		int h_ = auctionTab->getContentSize().height + off_.y;
		auctionTab->reloadData();	
		CCPoint temp = ccp(off_.x,h_ - auctionTab->getContentSize().height);
		auctionTab->setContentOffset(temp);
	}
}
