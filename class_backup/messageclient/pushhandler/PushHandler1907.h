
#ifndef Blog_C___Reflection_PushHandler1907_h
#define Blog_C___Reflection_PushHandler1907_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

#include "cocos2d.h"
USING_NS_CC;

class PushHandler1907 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1907)
    
public:
    SYNTHESIZE(PushHandler1907, int*, m_pValue)
    
    PushHandler1907() ;
    virtual ~PushHandler1907() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;

} ;

#endif
