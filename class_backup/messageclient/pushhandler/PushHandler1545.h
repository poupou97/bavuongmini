
#ifndef Blog_C___Family_PushHandler1545_h
#define Blog_C___Family_PushHandler1545_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1545 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1545)

public:
	SYNTHESIZE(PushHandler1545, int*, m_pValue)

		PushHandler1545() ;
	virtual ~PushHandler1545() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
