
#ifndef Blog_C___Family_PushHandler1524_h
#define Blog_C___Family_PushHandler1524_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1524 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1524)
    
public:
    SYNTHESIZE(PushHandler1524, int*, m_pValue)
    
    PushHandler1524() ;
    virtual ~PushHandler1524() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
