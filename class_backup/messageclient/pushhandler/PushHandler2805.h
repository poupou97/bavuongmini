#ifndef Blog_C___Reflection_PushHandler2805_h
#define Blog_C___Reflection_PushHandler2805_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler2805 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler2805)

public:
	SYNTHESIZE(PushHandler2805, int*, m_pValue)

		PushHandler2805() ;
	virtual ~PushHandler2805() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
