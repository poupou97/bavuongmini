
#include "PushHandler2211.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/OtherPlayer.h"
#include "../../gamescene_state/role/OtherPlayerOwnedStates.h"
#include "../../gamescene_state/role/BaseFighterOwnedCommand.h"

#include "../protobuf/RelationMessage.pb.h"
#include "../../ui/extensions/RichTextInput.h"
#include "GameView.h"
#include "../../ui/Friend_ui/FriendUi.h"
#include "../../ui/Friend_ui/FriendWorkList.h"
#include "../../messageclient/element/CRelationPlayer.h"

IMPLEMENT_CLASS(PushHandler2211)

PushHandler2211::PushHandler2211() 
{
    
}
PushHandler2211::~PushHandler2211() 
{
    
}
void* PushHandler2211::createInstance()
{
    return new PushHandler2211() ;
}
void PushHandler2211::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler2211::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler2211::handle(CommonMessage* mb)
{
	Rsp2211 bean;
	bean.ParseFromString(mb->data());
	
	int playListSize = bean.playerlist_size();	
	std::vector<CRelationPlayer *>::iterator iter;
	for(iter=GameView::getInstance()->relationSourceVector.begin();iter!=GameView::getInstance()->relationSourceVector.end();iter++)
	{
		delete *iter;
	}
	GameView::getInstance()->relationSourceVector.clear();
	for (int i=0;i<playListSize;i++)
	{
		CRelationPlayer * relationPlayerinfo = new CRelationPlayer();
		relationPlayerinfo->CopyFrom(bean.playerlist(i));

		GameView::getInstance()->relationSourceVector.push_back(relationPlayerinfo);
	}
	
	FriendUi * friend_ui=(FriendUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);
	if (friend_ui)
	{
		friend_ui->reloadSourceData(GameView::getInstance()->relationSourceVector.size());
		FriendWorkList * friendworklist_ =(FriendWorkList *)friend_ui->getChildByTag(FRIENDADDLAYER)->getChildByTag(FRIENDWORKLISTTAG);
		if (friendworklist_)
		{
			CCTableView * friendTab =(CCTableView *)friendworklist_->getChildByTag(FRIENDWORKLISTTAB);
			friendTab->reloadData();	
		}
	}
}
