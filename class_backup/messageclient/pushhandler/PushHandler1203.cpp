
#include "PushHandler1203.h"

#include "../protobuf/FightMessage.pb.h"
#include "../../GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "../../newcomerstory/NewCommerStoryManager.h"

IMPLEMENT_CLASS(PushHandler1203)

PushHandler1203::PushHandler1203() 
{
    
}
PushHandler1203::~PushHandler1203() 
{
    
}
void* PushHandler1203::createInstance()
{
    return new PushHandler1203() ;
}
void PushHandler1203::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1203::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1203::handle(CommonMessage* mb)
{
	Push1203 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, MuSou value changed", mb->cmdid());

	MyPlayer* myplayer = GameView::getInstance()->myplayer;
	if(bean.value() == bean.capacity())   // MuSou value is full!
	{
		//myplayer->onAddMusouEffect();
	}
	else
	{
		//myplayer->onRemoveMusouEffect();
	}

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

	GameView::getInstance()->myplayer->setAngerValue(bean.value());
	GameView::getInstance()->myplayer->setAngerCapacity(bean.capacity());

	// the musou skill is not open, so ignore
	int musouSkillOpenLevel = ShortcutSlotOpenLevelConfigData::s_shortcutSlotOpenLevelMsg[7];
	if (!NewCommerStoryManager::getInstance()->IsNewComer())
	{
		if (GameView::getInstance()->myplayer->getActiveRole()->level() < musouSkillOpenLevel)
			return;
	}

	float per = (bean.value()*1.0f)/(bean.capacity()*1.0f);
	if (scene->getMainUIScene()->getChildByTag(kTagShortcutLayer))
	{
		ShortcutLayer *shortcutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
		shortcutLayer->RefreshMusouEffect(per);
	}
}