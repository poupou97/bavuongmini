
#ifndef Blog_C___Reflection_PushHandler1322_h
#define Blog_C___Reflection_PushHandler1322_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler1322 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1322)

public:
	SYNTHESIZE(PushHandler1322, int*, m_pValue)

	PushHandler1322();
	virtual ~PushHandler1322() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

private:

protected:
	int *m_pValue ;

} ;

#endif
