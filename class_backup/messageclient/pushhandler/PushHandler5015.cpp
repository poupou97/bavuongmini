#include "PushHandler5015.h"

#include "../protobuf/CollectMessage.pb.h"  
#include "GameView.h"
#include "../../gamescene_state/role/MyPlayerOwnedCommand.h"
#include "../element/CMapInfo.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../ui/Active_ui/ActiveUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/Active_ui/ActiveManager.h"

IMPLEMENT_CLASS(PushHandler5015)

PushHandler5015::PushHandler5015() 
{
    
}
PushHandler5015::~PushHandler5015() 
{
    
}
void* PushHandler5015::createInstance()
{
    return new PushHandler5015() ;
}
void PushHandler5015::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5015::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler5015::handle(CommonMessage* mb)
{
	ResBoxPosition5015 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	// 初始化 保存服务器的数据
	// bean.positions_size()
	/*bean.positions(0)
	int size = bean.positions_size();
	for (int i = 0; i < size; i++)
	{
		COneOnlineGift * dayGift =new COneOnlineGift();
		dayGift->CopyFrom(bean.auctioninfos(i));

		OnlineGiftData::instance()->m_vector_oneOnLineGift.push_back(dayGift);
	}*/

	int nSize = bean.positions_size();
	if (nSize <= 0)
	{
		return;
	}


	// 说明此时 宝箱地图及坐标不可用
	if (INVAILD == bean.positions(0).state())
	{
		// 关闭 活跃度 窗体
		ActiveUI * activeUI = (ActiveUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveUI);
		if(NULL != activeUI)
		{
			activeUI->closeAnim();
		}

		//// 提示玩家，当前宝箱不可用
		//const char * charInfo = StringDataManager::getString("activy_presentBoxInfo");
		//GameView::getInstance()->showAlertDialog(charInfo);

		//return;
	}

	std::string targetMapId = bean.positions(0).mapid();
	int xPos = bean.positions(0).x();
	int yPos = bean.positions(0).y();

	int posX = GameView::getInstance()->getGameScene()->tileToPositionX(xPos);
	int posY = GameView::getInstance()->getGameScene()->tileToPositionY(yPos);

	CCPoint targetPos = CCPointMake(posX, posY);

	//// 寻路逻辑
	//if (strcmp(GameView::getInstance()->getMapInfo()->mapid().c_str(),targetMapId.c_str()) == 0)
	//{
	//	//same map
	//	CCPoint cp_role = GameView::getInstance()->myplayer->getPosition();
	//	CCPoint cp_target = targetPos;
	//	if (sqrt(pow((cp_role.x-cp_target.x),2)+pow((cp_role.y-cp_target.y),2)) < 64)
	//	{
	//		return;
	//	}
	//	else
	//	{
	//		if (targetPos.x == 0 && targetPos.y == 0)
	//		{
	//			return;
	//		}

	//		MyPlayerCommandMove* cmd = new MyPlayerCommandMove();
	//		// NOT reachable, so find the near one

	//		cmd->targetPosition = targetPos;
	//		cmd->method = MyPlayerCommandMove::method_searchpath;
	//		bool bSwitchCommandImmediately = true;
	//		if(GameView::getInstance()->myplayer->isAttacking())
	//			bSwitchCommandImmediately = false;
	//		GameView::getInstance()->myplayer->setNextCommand(cmd, bSwitchCommandImmediately);	
	//	}
	//}
	//else
	//{
	//	MyPlayerCommandMove* cmd = new MyPlayerCommandMove();
	//	cmd->targetMapId = targetMapId;
	//	if (targetPos.x == 0 && targetPos.y == 0)
	//	{
	//	}
	//	else
	//	{
	//		cmd->targetPosition = targetPos;
	//	}
	//	cmd->method = MyPlayerCommandMove::method_searchpath;
	//	bool bSwitchCommandImmediately = true;
	//	if(GameView::getInstance()->myplayer->isAttacking())
	//		bSwitchCommandImmediately = false;
	//	GameView::getInstance()->myplayer->setNextCommand(cmd, bSwitchCommandImmediately);	
	//}

	// 监听自动寻路
	// 判断是否加到mainUIScene
	ActiveManager * pActiveManager = (ActiveManager *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveLayer);
	if (NULL == pActiveManager)
	{
		ActiveManager* activeManager = ActiveManager::create();
		activeManager->set_targetMapId(targetMapId);
		activeManager->set_targetPos(targetPos);
		activeManager->setLayerUserful(true);
		activeManager->set_activeType(ActiveManager::TYPE_PRESENTBOX);
		activeManager->setTag(kTagActiveLayer);

		GameView::getInstance()->getMainUIScene()->addChild(activeManager);
	}
	else 
	{
		pActiveManager->set_targetMapId(targetMapId);
		pActiveManager->set_targetPos(targetPos);
		pActiveManager->setLayerUserful(true);
		pActiveManager->set_activeType(ActiveManager::TYPE_PRESENTBOX);
	}

	// 寻路逻辑
	if (strcmp(GameView::getInstance()->getMapInfo()->mapid().c_str(),targetMapId.c_str()) == 0)
	{
		////same map
		//CCPoint cp_role = GameView::getInstance()->myplayer->getPosition();
		//CCPoint cp_target = targetPos;
		//if (sqrt(pow((cp_role.x-cp_target.x),2)+pow((cp_role.y-cp_target.y),2)) < 64)
		//{
		//	return;
		//}
		//else
		//{
		//	if (targetPos.x == 0 && targetPos.y == 0)
		//	{
		//		return;
		//	}

		//	MyPlayerCommandMove* cmd = new MyPlayerCommandMove();
		//	// NOT reachable, so find the near one

		//	cmd->targetPosition = targetPos;
		//	cmd->method = MyPlayerCommandMove::method_searchpath;
		//	bool bSwitchCommandImmediately = true;
		//	if(GameView::getInstance()->myplayer->isAttacking())
		//		bSwitchCommandImmediately = false;
		//	GameView::getInstance()->myplayer->setNextCommand(cmd, bSwitchCommandImmediately);	
		//}
		
		// 提示“本地图有宝箱，请注意查找”
		const char * charInfo = StringDataManager::getString("PresentBox_tip_1");
		GameView::getInstance()->showAlertDialog(charInfo);
		
	}
	else
	{
		// 不在同一地图，直接飞到目的地
		ActiveManager::AcrossMapTransport(targetMapId, targetPos);

		// 判断是否加到mainUIScene
		ActiveManager * pActiveLayer = (ActiveManager *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveLayer);
		if (NULL == pActiveLayer)
		{
			ActiveManager* activeLayer = ActiveManager::create();
			activeLayer->set_transportFinish(false);
			activeLayer->setTag(kTagActiveLayer);

			GameView::getInstance()->getMainUIScene()->addChild(activeLayer);
		}
		else 
		{
			pActiveLayer->set_transportFinish(false);
		}
	}

	ActiveUI * activeUI = (ActiveUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveUI);
	if(NULL != activeUI)
	{
		activeUI->closeAnim();
	}
}
