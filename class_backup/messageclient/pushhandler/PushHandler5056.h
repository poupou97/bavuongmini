
#ifndef Blog_C___Reflection_PushHandler5056_h
#define Blog_C___Reflection_PushHandler5056_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5056 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5056)

public:
	SYNTHESIZE(PushHandler5056, int*, m_pValue)

		PushHandler5056() ;
	virtual ~PushHandler5056() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
