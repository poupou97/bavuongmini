
#ifndef Blog_C___Reflection_PushHandler5111_h
#define Blog_C___Reflection_PushHandler5111_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5111 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5111)

public:
	SYNTHESIZE(PushHandler5111, int*, m_pValue)

	PushHandler5111() ;
	virtual ~PushHandler5111() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
