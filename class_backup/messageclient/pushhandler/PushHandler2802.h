#ifndef Blog_C___Reflection_PushHandler2802_h
#define Blog_C___Reflection_PushHandler2802_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler2802 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler2802)

public:
	SYNTHESIZE(PushHandler2802, int*, m_pValue)

	PushHandler2802() ;
	virtual ~PushHandler2802() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
