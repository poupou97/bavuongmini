
#include "PushHandler7019.h"

#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../protobuf/MissionMessage.pb.h"
#include "../../ui/rewardTask_ui/RewardTaskData.h"
#include "../element/CBoardMissionInfo.h"
#include "../../ui/rewardTask_ui/RewardTaskMainUI.h"
#include "../../ui/rewardTask_ui/RewardTaskDetailUI.h"

IMPLEMENT_CLASS(PushHandler7019)

PushHandler7019::PushHandler7019() 
{

}
PushHandler7019::~PushHandler7019() 
{

}
void* PushHandler7019::createInstance()
{
	return new PushHandler7019() ;
}
void PushHandler7019::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler7019::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler7019::handle(CommonMessage* mb)
{
	PushBoardMissionStateUpdate7019 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	//refresh taskList
	for(int i = 0;i<RewardTaskData::getInstance()->p_taskList.size();i++)
	{
		CBoardMissionInfo * temp = RewardTaskData::getInstance()->p_taskList.at(i);
		if (temp->index() == bean.index())
		{
			temp->set_state(bean.state());
		}
	}

	//refresh ui
	RewardTaskMainUI * rewardTaskMainUI = (RewardTaskMainUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRewardTaskMainUI);
	if (rewardTaskMainUI)
	{
		rewardTaskMainUI->RefreshMissionState(bean.index(),bean.state());
	}
	RewardTaskDetailUI * rewardDetailUI = (RewardTaskDetailUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRewardTaskDetailUI);
	if (rewardDetailUI)
	{
		if (rewardDetailUI->curBoardMissionInfo->index() == bean.index())
		{
			rewardDetailUI->curBoardMissionInfo->set_state(bean.state());
			rewardDetailUI->refreshBtnState(bean.state());
		}
	}
}
