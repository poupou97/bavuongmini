
#ifndef Blog_C___Reflection_PushHandler5092_h
#define Blog_C___Reflection_PushHandler5092_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5092 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5092)

public:
	SYNTHESIZE(PushHandler5092, int*, m_pValue)

		PushHandler5092() ;
	virtual ~PushHandler5092() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
