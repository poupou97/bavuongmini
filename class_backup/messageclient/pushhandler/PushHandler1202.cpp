
#include "PushHandler1202.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/OtherPlayer.h"
#include "../../gamescene_state/role/BaseFighter.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/role/OtherPlayerOwnedStates.h"
#include "../../gamescene_state/role/BaseFighterOwnedCommand.h"
#include "../../gamescene_state/role/MyPlayerOwnedCommand.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/CExtStatusInfo.h"
#include "../../gamescene_state/SimpleEffectManager.h"
#include "../ProtocolHelper.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/skill/GameFightSkill.h"
#include "../../gamescene_state/skill/CSkillResult.h"
#include "AppMacros.h"
#include "../../gamescene_state/skill/processor/SkillProcessor.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "../protobuf/ModelMessage.pb.h"
#include "../../gamescene_state/GameStatistics.h"

IMPLEMENT_CLASS(PushHandler1202)

PushHandler1202::PushHandler1202() 
{
    
}
PushHandler1202::~PushHandler1202() 
{
    
}
void* PushHandler1202::createInstance()
{
    return new PushHandler1202() ;
}
void PushHandler1202::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1202::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1202::handle(CommonMessage* mb)
{
	Push1202 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, role id: %lld, skill: %s", mb->cmdid(), bean.source(), bean.showeffect().c_str());

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

	BaseFighter* attActor = dynamic_cast<BaseFighter*>(scene->getActor(bean.source()));
	if(attActor == NULL)
		return;
	
	std::string skillId = bean.skillid();
	std::string skillProcessorId = bean.showeffect();

	if (attActor->isMyPlayer())
	{
		if (scene->getMainUIScene()->getChildByTag(kTagShortcutLayer))
		{
			ShortcutLayer * shortcutLayer = (ShortcutLayer *)scene->getMainUIScene()->getChildByTag(kTagShortcutLayer);
			shortcutLayer->RefreshCD(bean.showeffect());
		}

		attActor->getGameFightSkill(skillId.c_str(), skillProcessorId.c_str())->startCD();
	}

	// 使用技能后，角色可能会回血
	int hpNumber = bean.attackerhp() - attActor->getActiveRole()->hp();
	if (hpNumber > 0)
		attActor->onHealed(attActor, hpNumber);
	// 使用技能后，角色可能会回蓝
	int mpNumber = bean.attackermp() - attActor->getActiveRole()->mp();
	if (mpNumber > 0)
		attActor->onMPAdded(attActor, mpNumber);
	ProtocolHelper::sharedProtocolHelper()->updateFighterHPMP(attActor, bean.attackerhp(), bean.attackermp());

	//实时更新上阵武将详细信息
	for (unsigned int i = 0;i < GameView::getInstance()->generalsInLineDetailList.size();++i)
	{
		if (bean.source() == GameView::getInstance()->generalsInLineDetailList.at(i)->generalid())
		{
			ActiveRole * activeRole = dynamic_cast<ActiveRole*>(GameView::getInstance()->generalsInLineDetailList.at(i)->mutable_activerole());
			if (activeRole)
			{
				activeRole->set_hp(bean.attackerhp());
				activeRole->set_mp(bean.attackermp());
			}
		}
	}

	//// test code, simulate the server data
	//float random = CCRANDOM_0_1();
	//if(random < 0.7f)
	//	bean.set_attacktime(2);
	//// test code end
	
	SkillBin* skillBinConfig = StaticDataSkillBin::s_data[skillProcessorId];

	// target
	long long targetRoleId = NULL_ROLE_ID;
	if(bean.defenders_size() > 0)
	{
		targetRoleId = bean.defenders(0).target();
	}

	// setup attack command
	bool bAllowTwiceAttack = false;
	if(skillBinConfig != NULL) {
		if(skillBinConfig->allow_twiceattack) {
			bAllowTwiceAttack = true;
		}
	}
	BaseFighterCommandAttack* cmd = NULL;
	if(GameView::getInstance()->isOwn(bean.source())) {
		// if it's MyPlayer, don't use skill. 
		// because MyPlayer has use this skill before send message to GameServer
		// 特殊情况，为了展示 2连击 效果 (  if twice-attack, it's cool, so show it! )
		if(bean.attacktime() > 1 && bAllowTwiceAttack)
			cmd = new MyPlayerCommandAttack();
	}
	else {
		cmd = new BaseFighterCommandAttack();

		// apply skill's attack_time
		if(skillBinConfig != NULL)
		{
			cmd->attackMaxCount = skillBinConfig->attack_time;
			if(cmd->attackMaxCount > 1)
			{
				float modifier = skillBinConfig->attack_time_modifier;   // default 1.0f
				cmd->attackSpeed = cmd->attackMaxCount * modifier;
			}
		}
	}
	if(cmd != NULL)
	{
		cmd->sendAttackRequest = false;   // MyPlayer don't need to re-send using skill message

		cmd->skillId = bean.skillid();
		cmd->skillProcessorId = bean.showeffect();

		// apply the attack count
		if(cmd->attackMaxCount == 1)
		{
			if(bean.attacktime() == 0)
				bean.set_attacktime(1);   // default value should be 1

			// monster will do twice-attack randomly
			if(attActor->getType() == GameActor::type_monster)
			{
				if(CCRANDOM_0_1() < 0.2f && BasePlayer::isDefaultAttack(cmd->skillId.c_str()))
					bean.set_attacktime(2);
			}

			cmd->attackMaxCount = bAllowTwiceAttack ? bean.attacktime() : 1;
			if(bean.attacktime() > 1) {
				float modifier = 1.4f;   // 1.0f
				cmd->attackSpeed = bean.attacktime() * modifier;
			}
		}

		if(targetRoleId != NULL_ROLE_ID) {
			cmd->targetId = targetRoleId;
		}
		attActor->setNextCommand(cmd, true/*!attActor->isAttacking()*/);
	}

	int skillType = -1;
	if(bean.has_skilltype())
		skillType = bean.skilltype();
	attActor->onAttack(bean.skillid().c_str(), bean.showeffect().c_str(), targetRoleId, bean.doubleattack() == 1, skillType);

	// 添加"2连击"文字特效
	if(attActor->isMyPlayerGroup() && bean.attacktime() > 1 && BasePlayer::isDefaultAttack(skillId.c_str())) 
	{
		CCSprite* sprite = CCSprite::create("res_ui/font/hit.png");
		sprite->setAnchorPoint(ccp(0.5f,0.0f));
		sprite->setScale(1.0f);
		SimpleEffectManager::applyEffectAction(sprite, SimpleEffectManager::EFFECT_FLYINGUP);
		//attActor->addEffect(sprite, false, 80/*BASEFIGHTER_ROLE_HEIGHT*/);

		sprite->setPosition(ccp(attActor->getPositionX(), attActor->getPositionY() + BASEFIGHTER_ROLE_HEIGHT/4));
		scene->getActorLayer()->addChild(sprite, SCENE_TOP_LAYER_BASE_ZORDER);
	}

	CSkillResult* result = new CSkillResult();
	result->CopyFrom(bean);
	attActor->onSkillApplied(skillId.c_str(), result);

	//if(attActor->isMyPlayerGroup())
	//{
	//	if(attActor->getPosInGroup() >= 1)
	//		CCLOG("general attack, skill id: %s, target pos, (%d, %d)", skillId.c_str(), bean.x(), bean.y());
	//}

	setDefenders(attActor, bean);
}

void PushHandler1202::setDefenders(BaseFighter* attacker, Push1202 bean) {
	GameView* gv = GameView::getInstance();
	GameSceneLayer* scene = gv->getGameScene();

	// 设置被攻击者的状态信息、伤害获得，等
	for (int i = 0; i < bean.defenders_size(); i++) {
		Defender def = bean.defenders(i);

		if(attacker->isMyPlayerGroup())
			DamageStatistics::collect(attacker->getRoleId(), def.damage());

		BaseFighter* defActor = dynamic_cast<BaseFighter*>(scene->getActor(def.target()));
		if(defActor == NULL)
			continue;

		// synchronize the real position
		defActor->setRealWorldPosition(ccp(def.targetx(), def.targety()));

		if(defActor->isAction(ACT_DIE))
			continue;

		// 使用技能后，角色可能会回血
		int hpNumber = def.targethp() - defActor->getActiveRole()->hp();
		if (hpNumber > 0)
			defActor->onHealed(attacker, hpNumber);
		// 使用技能后，角色可能会回蓝或被抽蓝
		int mpNumber = def.targetmp() - defActor->getActiveRole()->mp();
		if (mpNumber != 0)
			defActor->onMPAdded(attacker, mpNumber);
		ProtocolHelper::sharedProtocolHelper()->updateFighterHPMP(defActor, def.targethp(), def.targetmp());
				
		// 更新各种状态( buff and debuff )
		//addFighterStates(def.status, defActor, fs);
		ProtocolHelper::sharedProtocolHelper()->updateFighterStatus(def.mutable_extstatus(), defActor);
				
		// 添加躲避特效
		if (def.dodge() == 1)
		{
			if(defActor->isMyPlayerGroup() || attacker->isMyPlayerGroup()) {
				//defActor.addEffect(BaseFighter.STATE_DODGE);
				CCSprite* sprite = CCSprite::create("res_ui/font/shanbi.png");
				sprite->setAnchorPoint(ccp(0.5f,0.0f));
				sprite->setScale(1.0f);
				SimpleEffectManager::applyEffectAction(sprite, SimpleEffectManager::EFFECT_FLYINGUP);
				//defActor->addEffect(sprite, false, 80/*BASEFIGHTER_ROLE_HEIGHT*/);

				sprite->setPosition(ccp(defActor->getPositionX(), defActor->getPositionY() + BASEFIGHTER_ROLE_HEIGHT/4));
				scene->getActorLayer()->addChild(sprite, SCENE_TOP_LAYER_BASE_ZORDER);
			}
		}

		// 免疫效果
		if(def.has_immunity() && def.immunity() == 1 && defActor->isMyPlayerGroup())
		{
			CCSprite* sprite = CCSprite::create("res_ui/font/mianyi.png");
			sprite->setAnchorPoint(ccp(0.5f,0.0f));
			sprite->setScale(1.0f);
			SimpleEffectManager::applyEffectAction(sprite, SimpleEffectManager::EFFECT_FLYINGUP);
			defActor->addEffect(sprite, false, BASEFIGHTER_ROLE_HEIGHT/2);
		}

		// 无伤害，则不计算伤害
		if (def.damage() <= 0) {
			continue;
		}

		CSkillResult result;
		result.CopyFrom(bean);

		// apply twice-attack effect
		std::string skillProcessorId = bean.showeffect();
		bool bAllowTwiceAttack = false;
		if(StaticDataSkillBin::s_data[skillProcessorId] != NULL) {
			if(StaticDataSkillBin::s_data[skillProcessorId]->allow_twiceattack) {
				bAllowTwiceAttack = true;
			}
		}
		if(bAllowTwiceAttack &&  bean.attacktime() > 1)
		{
			// show some damage number at the same
			for(int i = 0; i < bean.attacktime(); i++)
			{
				int damage = def.damage() / bean.attacktime();
				if(damage == 0)
					damage = 1;
				defActor->onDamaged(attacker, damage, def.doubleattacked() == 1, &result);
			}
		}
		else
		{
			bool bDouble = def.doubleattacked() == 1;
			///bool bDouble = true;   // test code
			defActor->onDamaged(attacker, def.damage(), bDouble, &result);
		}

		//// the skills with damage will knockback the enemy
		//SkillBin* skillBinConfig = StaticDataSkillBin::s_data[skillId];
		//if(skillBinConfig != NULL) 
		//{
		//	if(skillBinConfig->canKnockBack)
		//		bool ret = SkillProcessor::knockback(attacker, defActor, 16, 0.15f);
		//}
	}

	// calc total damage number
	//int totalDamageNumber = 0;
	//for (int i = 0; i < bean.defenders_size(); i++) {
	//	Defender def = bean.defenders(i);
	//	totalDamageNumber += def.damage();
	//}

	// show the skill's damage number
	//if(attacker->isMyPlayerGroup() && !attacker->isMyPlayer())
	//{
	//	if(totalDamageNumber == 0 || BasePlayer::isDefaultAttack(bean.showeffect().c_str()))
	//		return;

	//	CCSize s = CCDirector::sharedDirector()->getVisibleSize();

	//	std::string text = "Total ";
	//	char number[12];
	//	sprintf(number, "%d", totalDamageNumber);
	//	text.append(number);
	//	CCLabelTTF* label = CCLabelTTF::create(text.c_str(), APP_FONT_NAME, 40);   // yellow color
	//	label->setAnchorPoint(ccp(0.5f, 0.5f));
	//	label->setPosition(ccp(s.width - 130, s.height - 120));

	//	CCFiniteTimeAction*  action = CCSequence::create(
	//		CCShow::create(),
	//		CCScaleTo::create(0.25f*1/2,2.0f),
	//		CCScaleTo::create(0.25f*1/3,1.0f),
	//		CCDelayTime::create(1.5f),
	//		CCFadeOut::create(0.5f),
	//		CCRemoveSelf::create(),
	//		NULL);
	//	label->runAction(action);

	//	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	//	scene->addChild(label, SCENE_TOP_LAYER_BASE_ZORDER);
	//}
}