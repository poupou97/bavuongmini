
#ifndef Blog_C___Reflection_PushHandler2001_h
#define Blog_C___Reflection_PushHandler2001_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"

USING_NS_CC;

class PushHandler2001 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler2001)

public:
	SYNTHESIZE(PushHandler2001, int*, m_pValue)

		PushHandler2001() ;
	virtual ~PushHandler2001() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
