
#include "PushHandler5074.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../protobuf/GeneralMessage.pb.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/skill/GameFightSkill.h"
#include "../../ui/generals_ui/GeneralsUI.h"
#include "../../ui/generals_ui/GeneralsStrategiesUI.h"
#include "../../gamescene_state/role/GameActor.h"
#include "../element/CFightWayBase.h"
#include "../../ui/generals_ui/generals_popup_ui/StrategiesUpgradeUI.h"
#include "../../utils/StaticDataManager.h"

IMPLEMENT_CLASS(PushHandler5074)

	PushHandler5074::PushHandler5074() 
{

}
PushHandler5074::~PushHandler5074() 
{

}
void* PushHandler5074::createInstance()
{
	return new PushHandler5074() ;
}
void PushHandler5074::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5074::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5074::handle(CommonMessage* mb)
{
	Resp5074 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

	if (scene->getMainUIScene()->getChildByTag(kTagGeneralsUI))
	{
		if (bean.has_error())
		{
			GameView::getInstance()->showAlertDialog(bean.error().msg());
		}
		else
		{
			GeneralsUI * genentalsUI = (GeneralsUI*)scene->getMainUIScene()->getChildByTag(kTagGeneralsUI);

			for (int i = 0;i<GeneralsUI::generalsStrategiesUI->generalsStrategiesList.size();++i)
			{
				CFightWayBase * fightWayBase = GeneralsUI::generalsStrategiesUI->generalsStrategiesList.at(i);
				if (fightWayBase->fightwayid() == bean.fightway().fightwayid())
				{
					//fightWayBase->CopyFrom(bean.fightway());
					fightWayBase->set_level(bean.fightway().level());
					fightWayBase->set_activeflag(bean.fightway().activeflag());

					break;
				}
			}

			if (scene->getMainUIScene()->getChildByTag(kTagStrategiesUpGradeUI))
			{
				StrategiesUpgradeUI * strategiesUpgradeUI = (StrategiesUpgradeUI*)scene->getMainUIScene()->getChildByTag(kTagStrategiesUpGradeUI);
				strategiesUpgradeUI->curFightWayBase->set_level(bean.fightway().level());
				strategiesUpgradeUI->curFightWayBase->set_activeflag(bean.fightway().activeflag());

				strategiesUpgradeUI->RefreshPropertyInfo(strategiesUpgradeUI->curFightWayBase);
				strategiesUpgradeUI->RefreshButtonStatus();
			}

			CFightWayBase * fightWayBase = new CFightWayBase();
			fightWayBase->CopyFrom(bean.fightway());
			GeneralsUI::generalsStrategiesUI->RefreshAllInfo(fightWayBase);
			delete fightWayBase;

			GeneralsUI::generalsStrategiesUI->RefreshData();

			for(int i = 0;i<FightWayConfigData::s_fightWayBase.size();++i)
			{
				if (FightWayConfigData::s_fightWayBase.at(i)->fightwayid() == bean.fightway().fightwayid())
				{
					//dialog
					std::string str_dialog;
					str_dialog.append(FightWayConfigData::s_fightWayBase.at(i)->get_name().c_str());
					str_dialog.append(StringDataManager::getString("generals_skill_isUpdating"));
					char s_lv[10];
					sprintf(s_lv,"%d",bean.fightway().level());
					str_dialog.append(s_lv);
					str_dialog.append(StringDataManager::getString("fivePerson_ji"));
					GameView::getInstance()->showAlertDialog(str_dialog.c_str());
					break;
				}
			}

		}
	}
}
