
#include "PushHandler2304.h"
#include "../protobuf/PlayerMessage.pb.h" 
#include "../../GameView.h"


IMPLEMENT_CLASS(PushHandler2304)

PushHandler2304::PushHandler2304() 
{
    
}
PushHandler2304::~PushHandler2304() 
{
    
}
void* PushHandler2304::createInstance()
{
	return new PushHandler2304() ;
}
void PushHandler2304::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler2304::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler2304::handle(CommonMessage* mb)
{
	Push2304 bean;
	bean.ParseFromString(mb->data());

	GameView::getInstance()->registerNoticeIsShow = true;

	GameView::getInstance()->registerNoticeVector.clear();

	GameView::getInstance()->registerNoticeVector.push_back(bean.notice());

}
