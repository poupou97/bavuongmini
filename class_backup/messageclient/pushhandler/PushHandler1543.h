
#ifndef Blog_C___Family_PushHandler1543_h
#define Blog_C___Family_PushHandler1543_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1543 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1543)

public:
	SYNTHESIZE(PushHandler1543, int*, m_pValue)

		PushHandler1543() ;
	virtual ~PushHandler1543() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
