
#ifndef Blog_C___Family_PushHandler1518_h
#define Blog_C___Family_PushHandler1518_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1518 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1518)
    
public:
    SYNTHESIZE(PushHandler1518, int*, m_pValue)
    
    PushHandler1518() ;
    virtual ~PushHandler1518() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
