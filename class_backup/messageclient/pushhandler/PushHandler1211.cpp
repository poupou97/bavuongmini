
#include "PushHandler1211.h"

#include "../protobuf/FightMessage.pb.h"
#include "../protobuf/ModelMessage.pb.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/BaseFighter.h"
#include "../../GameView.h"
#include "../ProtocolHelper.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "../element/CActiveRole.h"
#include "../../gamescene_state/sceneelement/HeadMenu.h"
#include "../../utils/StaticDataManager.h"
#include "../element/CFunctionOpenLevel.h"
#include "../../gamescene_state/sceneelement/GuideMap.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "../../gamescene_state/sceneelement/NewFunctionRemindUI.h"
#include "../../utils/GameUtils.h"
#include "../../gamescene_state/role/MyPlayerAIConfig.h"
#include "../../gamescene_state/GameSceneEffects.h"
#include "../../gamescene_state/role/BaseFighterConstant.h"
#include "../element/CGeneralBaseMsg.h"
#include "../../gamescene_state/sceneelement/GeneralsInfoUI.h"
#include "../../ui/moneyTree_ui/MoneyTreeData.h"
#include "../../ui/GameUIConstant.h"
#include "../../ui/Reward_ui/RewardUi.h"

#define POPUP_LEVELUP_EFFECT_ZORDER 301

using namespace CocosDenshion;

IMPLEMENT_CLASS(PushHandler1211)

PushHandler1211::PushHandler1211() 
{

}
PushHandler1211::~PushHandler1211() 
{

}
void* PushHandler1211::createInstance()
{
	return new PushHandler1211() ;
}
void PushHandler1211::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1211::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1211::handle(CommonMessage* mb)
{
	Push1211 bean;
	bean.ParseFromString(mb->data());

	//CCLOG("msg: %d, player levelup", mb->cmdid());

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

	BaseFighter* target = dynamic_cast<BaseFighter*>(scene->getActor(bean.roldid()));
	if(target == NULL)
		return;

	MyPlayer* me = GameView::getInstance()->myplayer;
	//// aptitude popup
	//if(bean.has_aptitude() && GameView::getInstance()->isOwn(bean.roldid()))
	//{
	//	CCNodeRGBA* pEffect = AptitudePopupEffect::create(me->player->mutable_aptitude(), bean.mutable_aptitude());
	//	me->addEffect(pEffect, false, MYPLAYER_APTITUDE_EFFECT_HEIGHT);
	//}
	if (GameView::getInstance()->isOwn(bean.roldid())) 
	{
		me->player->set_experience(bean.experience());
		me->player->set_nextlevelexperience(bean.nextlevelexperience());
		me->player->set_skillpoint(bean.skillpoint());

		if (bean.has_aptitude())
		{
			me->player->mutable_aptitude()->CopyFrom(bean.aptitude());
		}

		// refresh UI
		// ReloadTarget(target) will be called in the updateFighterHPMP
		//GameView::getInstance()->getMainUIScene()->ReloadTarget(target);
	}
	ActiveRole * activeRole = dynamic_cast<ActiveRole*>(target->getActiveRole());
	if (activeRole)
	{
		activeRole->set_level(bean.currlevel());
		activeRole->set_maxhp(bean.maxhp());
		activeRole->set_maxmp(bean.maxmp());
	}
	ProtocolHelper::sharedProtocolHelper()->updateFighterHPMP(target, bean.hp(), bean.mp());

	MainScene* mainScene = scene->getMainUIScene();
 	if (mainScene != NULL)
 	{
 		if (GameView::getInstance()->isOwn(bean.roldid())) 
 		{
 			mainScene->isLevelUped = true;
 			//mainScene->addInterfaceAnm("gxsj/gxsj.anm");
 		}
 	}
	// the level up effect on the role's body
	BaseFighter* pPlayer = dynamic_cast<BaseFighter*>(scene->getActor(bean.roldid()));
	if(pPlayer != NULL && pPlayer->isMyPlayer())
	{
		//CCLegendAnimation* effect = CCLegendAnimation::create("animation/texiao/renwutexiao/SJTX/sjtx1.anm");
		//pPlayer->addEffect(effect, false, 0);

		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		// alpha mask layer
		//CCLayerColor *mask = CCLayerColor::create(ccc4(0,0,0,128));
		CCLayerColor *mask = CCLayerColor::create(ccc4(0,0,0,0));
		mask->setContentSize(winSize);
		CCDirector::sharedDirector()->getRunningScene()->addChild(mask, POPUP_LEVELUP_EFFECT_ZORDER);
		CCFiniteTimeAction*  action0 = CCSequence::create(
			//CCShow::create(),
			CCFadeTo::create(0.6f, 100),
			CCDelayTime::create(0.3f),
			CCFadeOut::create(1.6f),
			CCRemoveSelf::create(),
			NULL);
		mask->runAction(action0);

		CCLegendAnimation* pAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/renwushengji/lvlup.anm");
		pAnim->setScale(1.4f);
		pAnim->setPosition(ccp(winSize.width/2,winSize.height/2));
		CCDirector::sharedDirector()->getRunningScene()->addChild(pAnim, POPUP_LEVELUP_EFFECT_ZORDER);

		createLevelupText(bean.currlevel());

		// the text of "gong xi sheng ji"
		//this->createNameAction(pPlayer);

		/*
		// add effect on the foot
		CCLegendAnimation* pAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/renwushengji/lvlup.anm");
		pAnim->setScale(1.2f);
		//CCLegendAnimation* pAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/FUHUO/fuhuo1.anm");
		pPlayer->addEffect(pAnim, false, 0);
		*/

		GameUtils::playGameSound(MYPLAYER_UPGRADE, 2, false);
	}

	//遍历查看是否在该等级开启了新功能（底部菜单栏）
	HeadMenu * headMenu = (HeadMenu *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHeadMenu);
	if(headMenu)
	{
		int num = 0;
		for(unsigned int i = 0;i<FunctionOpenLevelConfigData::s_functionOpenLevelMsg.size();++i)
		{
			if (FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionType() != 1)
				continue;

			if (bean.currlevel() == FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel())
			{
				//headMenu->addNewFunction(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i));
				num++;
				if (num == 0)
				{
					headMenu->addNewFunction(headMenu,FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i));
				}
				else
				{
					CCFiniteTimeAction*  action = CCSequence::create(
						CCDelayTime::create(num*0.3f),
						CCCallFuncND::create(headMenu,callfuncND_selector(HeadMenu::addNewFunction),FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)),
						NULL);
					headMenu->runAction(action);
				}
					
				//change by yangjun 2014.9.26
// 				if(strcmp(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionId().c_str(),"btn_generals") == 0)//是武将
// 				{
// 						MainScene * mainScene = GameView::getInstance()->getMainUIScene();
// 						if (mainScene)
// 						{
// 							mainScene->initGeneral();
// 						}
// 				}
			}
		}
	}

	//遍历查看是否在该等级开启了新功能（1号地图区域）
	GuideMap * guideMap = (GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
	if(guideMap)
	{
		for(unsigned int i = 0;i<FunctionOpenLevelConfigData::s_functionOpenLevelMsg.size();++i)
		{
			if (FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionType() != 2)
				continue;

			if (bean.currlevel() == FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel())
			{
				guideMap->addNewFunctionForArea1(guideMap,FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i));
			}
		}
	}

	//遍历查看是否在该等级开启了新功能（2号地图区域）
	if(guideMap)
	{
		int num = 0;
		for(unsigned int i = 0;i<FunctionOpenLevelConfigData::s_functionOpenLevelMsg.size();++i)
		{
			if (FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionType() != 3)
				continue;

			if (bean.currlevel() == FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel())
			{
				num++;
				if (num == 1)
				{
					guideMap->addNewFunctionForArea2(guideMap,FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i));
				}
				else
				{
					CCFiniteTimeAction*  action = CCSequence::create(
						CCDelayTime::create(num*0.5f),
						CCCallFuncND::create(guideMap,callfuncND_selector(GuideMap::addNewFunctionForArea2),FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)),
						NULL);
					guideMap->runAction(action);
				}

			}
		}
	}

	//按等级开发技能快捷栏
	ShortcutLayer * shortcutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
	if (shortcutLayer)
	{
		shortcutLayer->addNewShortcutSlot();
	}

	//是否开启了武将头像
	GeneralsInfoUI * generalsInfoUI = (GeneralsInfoUI *)GameView::getInstance()->getMainUIScene()->getGeneralInfoUI();
	if(generalsInfoUI)
	{
		//开启等级
		int openlevel = 0;
		//上阵第一个武将对应ID   8   一次增加
		for (int i = 0;i<5;i++)
		{
			std::map<int,int>::const_iterator cIter;
			cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(8+i);
			if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // 没找到就是指向END了  
			{
			}
			else
			{
				openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[8+i];
			}

			if (openlevel == bean.currlevel())
			{
				generalsInfoUI->RefreshAllGeneralItem();
			}
		}
	}

	//新功能提示
	if(GameView::getInstance()->getMainUIScene()->getChildByTag(kTagNewFunctionRemindUI))
	{
		GameView::getInstance()->getMainUIScene()->getChildByTag(kTagNewFunctionRemindUI)->removeFromParent();
	}

	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	NewFunctionRemindUI* newFunctionRemindUI = NewFunctionRemindUI::create(bean.currlevel());
	if (newFunctionRemindUI)
	{
		newFunctionRemindUI->ignoreAnchorPointForPosition(false);
		newFunctionRemindUI->setAnchorPoint(ccp(0.5f,0.5f));
		newFunctionRemindUI->setPosition(ccp(winSize.width/2,winSize.height/2));
		newFunctionRemindUI->setTag(kTagNewFunctionRemindUI);
		GameView::getInstance()->getMainUIScene()->addChild(newFunctionRemindUI,ZOrder_ToolTips);
	}
	
	int roleLevel = GameView::getInstance()->myplayer->getActiveRole()->level();
	for (unsigned int i = 0;i<FunctionOpenLevelConfigData::s_functionOpenLevelMsg.size();i++)
	{
		if (FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionType() == 2)
		{
			if (strcmp(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionId().c_str(),"btn_remind") == 0)
			{
				if ( GameView::getInstance()->myplayer->getActiveRole()->level() >= FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel())
				{
					GameView::getInstance()->getMainUIScene()->setRemindOpened(true);
					break;
				}
			}
		}
	}
	mainScene->checkIsNewRemind();

	/**
	  * the fight point effect will be activated by general's fight point change, so ignore here
	  **/
	//int m_old_AllFightPoint = GameView::getInstance()->myplayer->player->fightpoint();
	//for (unsigned int i = 0;i<GameView::getInstance()->generalsInLineList.size();++i)
	//{
	//	m_old_AllFightPoint += GameView::getInstance()->generalsInLineList.at(i)->fightpoint();
	//}
	//int m_new_AllFightPoint = bean.fightpoint();
	//for (int i = 0;i<GameView::getInstance()->generalsInLineList.size();++i)
	//{
	//	m_new_AllFightPoint += GameView::getInstance()->generalsInLineList.at(i)->fightpoint();
	//}
	//if (m_old_AllFightPoint != m_new_AllFightPoint && GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFightPointChange)==NULL)
	//{
	//	FightPointChangeEffect * fightEffect_ = FightPointChangeEffect::create(m_old_AllFightPoint,m_new_AllFightPoint);
	//	if (fightEffect_)
	//	{
	//		fightEffect_->setTag(kTagFightPointChange);
	//		fightEffect_->setZOrder(SCENE_STORY_LAYER_BASE_ZORDER);
	//		GameView::getInstance()->getMainUIScene()->addChild(fightEffect_);
	//	}
	//}

	GameView::getInstance()->myplayer->player->set_fightpoint(bean.fightpoint());

	//when level is up, robot is change the drug hp mp 
	if (bean.currlevel() == 15)
	{
		MyPlayerAIConfig::reSetRobotUseDrug(bean.currlevel());
	}

	//money tree reward
// 	if (GameView::getInstance()->myplayer->getActiveRole()->level() == 20)
// 	{
// 		if (!MoneyTreeData::instance()->hasAllGiftGet())
// 		{
// 			RewardUi::addRewardListEvent(REWARD_LIST_ID_MONEYTREE);
// 		}
// 	}
}

void PushHandler1211::createNameAction( BaseFighter* pBaseFighter )
{
	if (NULL == pBaseFighter)
	{
		return;
	}

	// add name action
	CCNodeRGBA* name_node = CCNodeRGBA::create();
	name_node->setCascadeOpacityEnabled(true);

	/*
	CCSprite* spr = CCSprite::create("res_ui/generalname_di.png");
	spr->setAnchorPoint(ccp(0.5f,0.5f));
	name_node->addChild(spr);

	// 升级 generals_strategies_upgrade
	const char *str_update_tmp = StringDataManager::getString("generals_strategies_upgrade");
	std::string str_update = "";
	str_update.append(str_update_tmp);
	str_update.append("!");

	CCLabelBMFont* skillNameLabel = CCLabelBMFont::create(str_update.c_str(), "res_ui/font/ziti_3.fnt");
	skillNameLabel->setAnchorPoint(ccp(0.5f,0.9f));
	name_node->addChild(skillNameLabel);
	*/

	CCLegendAnimation* pAnim = CCLegendAnimation::create("animation/texiao/jiemiantexiao/gxsj/gxsj.anm");
	pAnim->setPlayLoop(true);
	name_node->addChild(pAnim);

	CCAction* spawn_action = CCSpawn::create(
		CCFadeOut::create(0.25f),
		CCScaleTo::create(0.25f, 2.0f),
		NULL);

	CCAction*  action = CCSequence::create(
		CCMoveBy::create(0.5f, ccp(0, 65)),
		CCDelayTime::create(0.5f),
		spawn_action,
		CCRemoveSelf::create(),
		NULL);
	name_node->runAction(action);
	
	// add animation to scene layer
	//name_node->setPosition(ccp(pBaseFighter->getPositionX(), pBaseFighter->getPositionY() + 5));
	//pBaseFighter->getGameScene()->getActorLayer()->addChild(name_node, SCENE_TOP_LAYER_BASE_ZORDER);

	// add animation to UI layer
	if(pBaseFighter->isMyPlayer())
	{
		//CCNode* sceneLayer = pBaseFighter->getGameScene()->getChildByTag(GameSceneLayer::kTagSceneLayer);
		//CCPoint screenPos = pBaseFighter->getPosition() + sceneLayer->getPosition();
		//name_node->setPosition(screenPos);

		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		name_node->setPosition(ccp(winSize.width/2,winSize.height/2));

		CCDirector::sharedDirector()->getRunningScene()->addChild(name_node, POPUP_LEVELUP_EFFECT_ZORDER);
	}
}

void PushHandler1211::createLevelupText(int level)
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

	CCNodeRGBA* pTextNode = CCNodeRGBA::create();
	pTextNode->setCascadeOpacityEnabled(true);
	//char level_up_text[30];
	//const char* templateStr = StringDataManager::getString("role_level_up");
	//sprintf(level_up_text, templateStr, bean.currlevel());
	const char* font_name = "res_ui/font/ziti_14.fnt";
	// text 1
	const char* lvlup_1_str = StringDataManager::getString("role_level_up_1");
	CCLabelBMFont* lvlup_label_1 = CCLabelBMFont::create(lvlup_1_str, font_name);
	lvlup_label_1->setAnchorPoint(ccp(0, 0));
	lvlup_label_1->setPosition(ccp(0, 0));
	pTextNode->addChild(lvlup_label_1);
	// level num
	char level_num[5];
	sprintf(level_num, "%d", level);
	CCLabelBMFont* lvlup_num = CCLabelBMFont::create(level_num, "res_ui/font/ziti_14.fnt");
	const float lvlup_num_scale = 1.35f;
	lvlup_num->setScale(lvlup_num_scale);
	lvlup_num->setAnchorPoint(ccp(0,0));
	lvlup_num->setPosition(ccp(lvlup_label_1->getContentSize().width, -8.0f));
	float lvlup_num_width = lvlup_num->getContentSize().width * lvlup_num_scale;
	float lvlup_num_height = lvlup_num->getContentSize().height * lvlup_num_scale;
	pTextNode->addChild(lvlup_num);
	// text 2
	const char* lvlup_2_str = StringDataManager::getString("role_level_up_2");
	CCLabelBMFont* lvlup_label_2 = CCLabelBMFont::create(lvlup_2_str, font_name);
	lvlup_label_2->setAnchorPoint(ccp(0, 0));
	lvlup_label_2->setPosition(ccp((lvlup_label_1->getContentSize().width+lvlup_num_width), 0));
	pTextNode->addChild(lvlup_label_2);

	pTextNode->setContentSize(CCSizeMake(lvlup_label_1->getContentSize().width+lvlup_num_width+lvlup_label_2->getContentSize().width, lvlup_num_height));
	pTextNode->setPosition(ccp(winSize.width/2, winSize.height/2));
	pTextNode->setAnchorPoint(ccp(0.5f, 0.5f));
	pTextNode->setScale(2.0f);

	CCSpawn* spawnAction = CCSpawn::create(
		CCMoveBy::create(1.0f, ccp(0, 600)),
		CCFadeOut::create(0.85f),
		NULL);
	pTextNode->runAction(CCSequence::create(
		CCDelayTime::create(0.1f),
		CCShow::create(),
		CCScaleTo::create(0.3f, 1.0f),
		CCDelayTime::create(1.8f),
		//CCFadeOut::create(0.9f),
		//CCMoveBy::create(0.8f, ccp(0, 600)),
		spawnAction,
		CCRemoveSelf::create(),
		NULL));
	CCDirector::sharedDirector()->getRunningScene()->addChild(pTextNode, POPUP_LEVELUP_EFFECT_ZORDER);
}