
#include "PushHandler3003.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "GameView.h"
#include "../protobuf/OfflineFight.pb.h"
#include "../../ui/offlinearena_ui/OffLineArenaUI.h"
#include "../element/CHonorCommodity.h"
#include "../element/CPartner.h"
#include "../../ui/offlinearena_ui/RankListUI.h"

IMPLEMENT_CLASS(PushHandler3003)

	PushHandler3003::PushHandler3003() 
{

}
PushHandler3003::~PushHandler3003() 
{

}
void* PushHandler3003::createInstance()
{
	return new PushHandler3003() ;
}
void PushHandler3003::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler3003::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler3003::handle(CommonMessage* mb)
{
	PushChallengeList3003 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());


	if (GameView::getInstance()->getGameScene() == NULL)
		return;

	if (GameView::getInstance()->getGameScene() == NULL)
		return;

	OffLineArenaUI * offLineArenaUI = (OffLineArenaUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOffLineArenaUI);
	if (offLineArenaUI == NULL)
		return;

	std::vector<CPartner *>tempList;
	for (int i = 0;i<bean.challengelist_size();++i)
	{
		CPartner * partner = new CPartner();
		partner->CopyFrom(bean.challengelist(i));
		tempList.push_back(partner);
	}

	//ˢ��������ʾ
	offLineArenaUI->RefreshArenaDisplay(tempList);

	std::vector<CPartner*>::iterator iter;
	for (iter = tempList.begin(); iter != tempList.end(); ++iter)
	{
		delete *iter;
	}
	tempList.clear();

}
