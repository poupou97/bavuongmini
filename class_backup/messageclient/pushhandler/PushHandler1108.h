
#ifndef Blog_C___Reflection_PushHandler1108_h
#define Blog_C___Reflection_PushHandler1108_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

using namespace std;
#include "cocos2d.h"
USING_NS_CC;

/**
 * 通知客户端指定的玩家已离开
 */
class PushHandler1108 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1108)

public:
	SYNTHESIZE(PushHandler1108, int*, m_pValue)

	PushHandler1108() ;
	virtual ~PushHandler1108() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
