
#ifndef Blog_C___Reflection_PushHandler5074_h
#define Blog_C___Reflection_PushHandler5074_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5074 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5074)

public:
	SYNTHESIZE(PushHandler5074, int*, m_pValue)

		PushHandler5074() ;
	virtual ~PushHandler5074() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
