
#ifndef Blog_C___Reflection_PushHandler1610_h
#define Blog_C___Reflection_PushHandler1610_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1610 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1610)
    
public:
    SYNTHESIZE(PushHandler1610, int*, m_pValue)
    
    PushHandler1610() ;
    virtual ~PushHandler1610() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
