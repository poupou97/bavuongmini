
#ifndef Blog_C___Reflection_PushHandler1115_h
#define Blog_C___Reflection_PushHandler1115_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

using namespace std;

#include "cocos2d.h"
USING_NS_CC;

class PushHandler1115 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1115)
public:
	SYNTHESIZE(PushHandler1115, int*, m_pValue)

		PushHandler1115() ;
	virtual ~PushHandler1115() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;
} ;

#endif