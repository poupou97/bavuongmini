
#include "PushHandler5063.h"

#include "../protobuf/CollectMessage.pb.h"  
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/PickingActor.h"
#include "../element/CPickingInfo.h"
#include "../../GameView.h"
#include "../../legend_engine/GameWorld.h"
#include "../element/CPushCollectInfo.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../protobuf/RecruitMessage.pb.h"
#include "../../ui/generals_ui/GeneralsUI.h"
#include "../../ui/generals_ui/RecuriteActionItem.h"
#include "../../ui/generals_ui/GeneralsRecuriteUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../element/CActionDetail.h"
#include "../element/CGeneralDetail.h"
#include "../../ui/extensions/RecruitGeneralCard.h"

IMPLEMENT_CLASS(PushHandler5063)

	PushHandler5063::PushHandler5063() 
{

}
PushHandler5063::~PushHandler5063() 
{

}
void* PushHandler5063::createInstance()
{
	return new PushHandler5063() ;
}
void PushHandler5063::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5063::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5063::handle(CommonMessage* mb)
{
	PushPropRecruitResult5063 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

	if (bean.generals_size() == 1)
	{
		CGeneralDetail * tempData = new CGeneralDetail();
		tempData->CopyFrom(bean.generals(0));
		GeneralsUI::generalsRecuriteUI->presentRecruiteResult(tempData,-1,RecruitGeneralCard::kTypeUsePropRecuriteGeneral,false);
		delete tempData;
	}
	else
	{
		std::vector<CGeneralDetail * >tempVector ;
		for (int i = 0;i<bean.generals_size();++i)
		{
			CGeneralDetail * tempGD = new CGeneralDetail();
			tempGD->CopyFrom(bean.generals(i));
			tempVector.push_back(tempGD);
		}
		//显示十连抽的结果界面
		GeneralsUI::generalsRecuriteUI->presentTenTimesRecruiteResult(tempVector,false);

		std::vector<CGeneralDetail*>::iterator iter_generalDetail;
		for (iter_generalDetail = tempVector.begin(); iter_generalDetail != tempVector.end(); ++iter_generalDetail)
		{
			delete *iter_generalDetail;
		}
		tempVector.clear();
	}
}
