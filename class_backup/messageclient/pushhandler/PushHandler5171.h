#ifndef Blog_C___Reflection_PushHandler5171_h
#define Blog_C___Reflection_PushHandler5171_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5171 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5171)

public:
	SYNTHESIZE(PushHandler5171, int*, m_pValue)

	PushHandler5171() ;
	virtual ~PushHandler5171() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
