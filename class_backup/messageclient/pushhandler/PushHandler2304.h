
#ifndef Blog_C___CheckOtherEquip_PushHandler2304_h
#define Blog_C___CheckOtherEquip_PushHandler2304_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler2304 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler2304)
    
public:
    SYNTHESIZE(PushHandler2304, int*, m_pValue)
    
    PushHandler2304() ;
    virtual ~PushHandler2304() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
