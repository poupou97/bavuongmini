
#ifndef Blog_C___Reflection_PushHandler7003_h
#define Blog_C___Reflection_PushHandler7003_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class MissionInfo;

class PushHandler7003 : public CKBaseClass, public PushHandlerProtocol,public CCNode
{
private:
	DECLARE_CLASS(PushHandler7003)

public:
	SYNTHESIZE(PushHandler7003, int*, m_pValue)

	PushHandler7003() ;
	virtual ~PushHandler7003() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);


	//void showHandleMission(CCNode* sender, void* data);

private:

protected:
	int *m_pValue ;

} ;

#endif
