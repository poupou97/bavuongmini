
#include "PushHandler1207.h"

#include "../protobuf/FightMessage.pb.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/sceneelement/MyHeadInfo.h"
#include "GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"

IMPLEMENT_CLASS(PushHandler1207)

PushHandler1207::PushHandler1207() 
{
    
}
PushHandler1207::~PushHandler1207() 
{
    
}
void* PushHandler1207::createInstance()
{
    return new PushHandler1207() ;
}
void PushHandler1207::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1207::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1207::handle(CommonMessage* mb)
{
	Push1207 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, pk mode: %d, role id: %lld", mb->cmdid(), bean.status(), bean.roleid());

	MainScene * mainscene=(MainScene *) GameView::getInstance()->getMainUIScene();
	mainscene->curMyHeadInfo->applyPKMode(bean.status());

	GameView::getInstance()->myplayer->setPKMode(bean.status());
}