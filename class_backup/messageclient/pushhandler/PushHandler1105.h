
#ifndef Blog_C___Reflection_PushHandler1105_h
#define Blog_C___Reflection_PushHandler1105_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"
#include "../protobuf/PlayerMessage.pb.h"

#include "cocos2d.h"
USING_NS_CC;

class BaseFighter;

class PushHandler1105 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1105)
    
public:
    SYNTHESIZE(PushHandler1105, int*, m_pValue)
    
    PushHandler1105() ;
    virtual ~PushHandler1105() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

private:
	void createRoleInScene(Push1105* bean);
	void refreshGeneralsInfoInArena(Push1105* bean);
	void addFootFlagForBoss( BaseFighter* actor );

protected:
    int *m_pValue ;

} ;

#endif
