#include "PushHandler1112.h"

#include "../protobuf/PlayerMessage.pb.h"  

#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/BaseFighter.h"
#include "../../gamescene_state/role/BaseFighterOwnedCommand.h"
#include "../../gamescene_state/role/Monster.h"
#include "../../gamescene_state/role/MonsterOwnedStates.h"

IMPLEMENT_CLASS(PushHandler1112)

PushHandler1112::PushHandler1112() 
{
    
}
PushHandler1112::~PushHandler1112() 
{
    
}
void* PushHandler1112::createInstance()
{
    return new PushHandler1112() ;
}
void PushHandler1112::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1112::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1112::handle(CommonMessage* mb)
{
	Push1112 bean;
	bean.ParseFromString(mb->data());

	CCLOG("msg: %d, role id: %lld, target: ( %d, %d )", mb->cmdid(), bean.roleid(), bean.targetx(), bean.targety());

	GameView* gv = GameView::getInstance();
	BaseFighter* bf = dynamic_cast<BaseFighter*>(gv->getGameScene()->getActor(bean.roleid()));

	switch(bean.movemode()){
	case 0://  瞬移
		//if (bf != null) {
		//	Util.toTarget(bf, bean.x, bean.y);
		//}
		break;
	case 1://正常移动
		if (bf != NULL) {
			// setup move command
			BaseFighterCommandMove* cmd = new BaseFighterCommandMove();
			//cmd->targetPosition = ccp(bean.x(), bean.y());
			cmd->targetPosition = ccp(bean.targetx(), bean.targety());
			bf->setNextCommand(cmd);

			if (bf->getActiveRole()->type() == 1 /*ActiveRole.TYPE_MONSTER*/) {
				float speed = (64*2) / (bf->getActiveRole()->speed() / 1000.f);

				Monster* m = (Monster*)bf;
				//if(m->GetFSM()->isInState(*(MonsterStand::Instance())))
				//	speed = speed * 0.1f;
				m->setMoveSpeed(speed);
			}
		}
		break;
	}
}