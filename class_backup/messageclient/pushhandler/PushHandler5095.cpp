
#include "PushHandler5095.h"

#include "../protobuf/FightMessage.pb.h"
#include "../../utils/StaticDataManager.h"
#include "GameView.h"
#include "../../ui/GameUIConstant.h"
#include "../../gamescene_state/MainScene.h"
#include "../element/CRewardBase.h"
#include "../../ui/Reward_ui/RewardUi.h"

IMPLEMENT_CLASS(PushHandler5095)

PushHandler5095::PushHandler5095() 
{

}
PushHandler5095::~PushHandler5095() 
{

}
void* PushHandler5095::createInstance()
{
	return new PushHandler5095() ;
}
void PushHandler5095::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5095::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5095::handle(CommonMessage* mb)
{
	Push5095 bean;
	bean.ParseFromString(mb->data());

	if(bean.status())
	{
		bool isHave = false;
		for (int i = 0;i<GameView::getInstance()->rewardvector.size();i++)
		{
			if (GameView::getInstance()->rewardvector.at(i)->getId() == REWARD_LIST_ID_PHYSICAL_FRIEND)
			{
				isHave = true;
			}
		}

		if (isHave == false)
		{
			RewardUi::addRewardListEvent(REWARD_LIST_ID_PHYSICAL_FRIEND);
			if (GameView::getInstance()->getMainUIScene())
			{
				GameView::getInstance()->getMainUIScene()->remindGetFriendPhypower();
			}
		}
	}
}