
#ifndef Blog_C___Reflection_PushHandler1121_h
#define Blog_C___Reflection_PushHandler1121_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

using namespace std;

#include "cocos2d.h"
USING_NS_CC;

class PushHandler1121 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1121)
public:
	SYNTHESIZE(PushHandler1121, int*, m_pValue)

	PushHandler1121() ;
	virtual ~PushHandler1121() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;
} ;

#endif