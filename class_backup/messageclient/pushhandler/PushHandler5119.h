
#ifndef Blog_C___Reflection_PushHandler5119_h
#define Blog_C___Reflection_PushHandler5119_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5119 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5119)

public:
	SYNTHESIZE(PushHandler5119, int*, m_pValue)

		PushHandler5119() ;
	virtual ~PushHandler5119() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
