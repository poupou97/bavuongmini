
#ifndef Blog_C___Reflection_ReqSender1608_h
#define Blog_C___Reflection_ReqSender1608_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1608 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1608)
    
public:
    SYNTHESIZE(ReqSender1608, int*, m_pValue)
    
    ReqSender1608() ;
    virtual ~ReqSender1608() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
