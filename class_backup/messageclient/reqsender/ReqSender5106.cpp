
#include "ReqSender5106.h"

#include "../ClientNetEngine.h"

IMPLEMENT_CLASS(ReqSender5106)

ReqSender5106::ReqSender5106() 
{

}
ReqSender5106::~ReqSender5106() 
{

}
void* ReqSender5106::createInstance()
{
	return new ReqSender5106() ;
}
void ReqSender5106::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5106::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5106::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5106);
	CCLOG("send msg: %d", comMessage.cmdid());

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}