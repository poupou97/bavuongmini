#include "ReqSender2301.h"

#include "../protobuf/GeneralMessage.pb.h"
#include "../ClientNetEngine.h"
#include "../protobuf/DailyMessage.pb.h"

IMPLEMENT_CLASS(ReqSender2301)

ReqSender2301::ReqSender2301() 
{

}
ReqSender2301::~ReqSender2301() 
{

}
void* ReqSender2301::createInstance()
{
	return new ReqSender2301() ;
}
void ReqSender2301::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender2301::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender2301::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(2301);
	CCLOG("send msg: %d", comMessage.cmdid());

	Req2301 message1;
	message1.set_pagenumber((int) source);

	std::string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
	
	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}