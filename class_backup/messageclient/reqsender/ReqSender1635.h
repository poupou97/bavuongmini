
#ifndef Blog_C___Reflection_ReqSender1635_h
#define Blog_C___Reflection_ReqSender1635_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1635 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender1635)

public:
	SYNTHESIZE(ReqSender1635, int*, m_pValue)

	ReqSender1635() ;
	virtual ~ReqSender1635() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
