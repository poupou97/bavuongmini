
#include "ReqSender1924.h"

#include "../ClientNetEngine.h"
#include "../protobuf/CopyMessage.pb.h" 
#include "../../ui/FivePersonInstance/FivePersonInstance.h"
#include "../../ui/FivePersonInstance/InstanceEndUI.h"
#include "GameView.h"
#include "../../gamescene_state/MainScene.h"

IMPLEMENT_CLASS(ReqSender1924)
ReqSender1924::ReqSender1924() 
{
    
}
ReqSender1924::~ReqSender1924() 
{
    
}
void* ReqSender1924::createInstance()
{
    return new ReqSender1924() ;
}
void ReqSender1924::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1924::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1924::send(void* source, void* source1)
{
// 	InstanceEndUI * resultUI = (InstanceEndUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagInstanceEndUI);
// 	if (resultUI)
// 	{
// 		resultUI->endSlowMotion();
// 	}

	CommonMessage comMessage;
	comMessage.set_cmdid(1924);

	CCLOG("send msg: %d, request to exit the instance ", comMessage.cmdid());

	Req1924 message2;
	message2.set_copyid(FivePersonInstance::getCurrentInstanceId());

	string msgData;
	message2.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}