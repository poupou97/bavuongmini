
#include "ReqSender1133.h"

#include "../ClientNetEngine.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"
#include "../protobuf/NPCMessage.pb.h"

IMPLEMENT_CLASS(ReqSender1133)
	ReqSender1133::ReqSender1133() 
{

}
ReqSender1133::~ReqSender1133() 
{

}
void* ReqSender1133::createInstance()
{
	return new ReqSender1133() ;
}
void ReqSender1133::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1133::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1133::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1133);
	Req1133 message1;

	message1.set_folderid((int)source);
	message1.set_amount((int)source1);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}