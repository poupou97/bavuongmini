
#include "ReqSender1904.h"

#include "../ClientNetEngine.h"

#include "../protobuf/CopyMessage.pb.h"  
#include "../../ui/challengeRound/ShowLevelUi.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../../ui/challengeRound/SingleRankListUi.h"
#include "../../ui/challengeRound/ChallengeRoundUi.h"
#include "../../ui/Active_ui/ActiveUI.h"

IMPLEMENT_CLASS(ReqSender1904)
ReqSender1904::ReqSender1904() 
{
    
}
ReqSender1904::~ReqSender1904() 
{
    
}
void* ReqSender1904::createInstance()
{
    return new ReqSender1904() ;
}
void ReqSender1904::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1904::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1904::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1904);

	ReqTowerRanking1904 message2;
	SingleRankListUi * ranklist_ = (SingleRankListUi *)source;

	//ActiveUI * activeui = (ActiveUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveUI);
	ChallengeRoundUi * challengeui_ = (ChallengeRoundUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagChallengeRoundUi);
	message2.set_clazz(challengeui_->getCopyClazz());
	message2.set_page(ranklist_->curRanklistPage);
	message2.set_num(ranklist_->everyPageNum);
	
	string msgData;
	message2.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}