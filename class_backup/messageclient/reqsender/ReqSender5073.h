
#ifndef Blog_C___Reflection_ReqSender5073_h
#define Blog_C___Reflection_ReqSender5073_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5073 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5073)

public:
	SYNTHESIZE(ReqSender5073, int*, m_pValue)

		ReqSender5073() ;
	virtual ~ReqSender5073() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
