
#include "ReqSender1128.h"

#include "../ClientNetEngine.h"

#include "../protobuf/AuctionMessage.pb.h"
#include "../../ui/Auction_ui/AuctionUi.h"
#include "../protobuf/PlayerMessage.pb.h"
#include "../../ui/extensions/PopupPanel.h"


IMPLEMENT_CLASS(ReqSender1128)
ReqSender1128::ReqSender1128() 
{
    
}
ReqSender1128::~ReqSender1128() 
{
}
void* ReqSender1128::createInstance()
{
    return new ReqSender1128() ;
}
void ReqSender1128::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1128::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1128::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1128);
	
	Req1128 message1;

	PopupPanel::sendContent *popup_=(PopupPanel::sendContent *)source;
	message1.set_option(popup_->option_);
	message1.set_id(popup_->id_);
	message1.set_inputcontent(popup_->inputContent_);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}