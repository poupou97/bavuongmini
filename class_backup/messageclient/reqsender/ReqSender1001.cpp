
#include "ReqSender1001.h"

#include "../ClientNetEngine.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"

#include "../../login_state/LoginState.h"
#include "../../login_state/CreateRole.h"
#include "../../logo_state/LogoState.h"
#include "../../login_state/LoginStateTest.h"
#include "../../login_state/Login.h"
#include "../../GameUserDefault.h"
#include "../../utils/GameUtils.h"
#include "../../newcomerstory/NewCommerStoryManager.h"

IMPLEMENT_CLASS(ReqSender1001)
ReqSender1001::ReqSender1001() 
{
    
}
ReqSender1001::~ReqSender1001() 
{
    
}
void* ReqSender1001::createInstance()
{
    return new ReqSender1001() ;
}
void ReqSender1001::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1001::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1001::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1001);

	Req1001 message2;
#if THREEKINGDOMS_SERVERLIST_MODE
	//LoginLayer* login = (LoginLayer*)source;
	message2.set_roleid(LoginLayer::mRoleId);
	CCLOG("LoginLayer::mRoleId = %d", LoginLayer::mRoleId);
#else
	LoginTestLayer* login = (LoginTestLayer*)source;
	message2.set_roleid(login->getSelectedRoleId());
#endif
	char* stringUserid = new char[10];
	sprintf(stringUserid,"%d", Login::userId);
	message2.set_passportid(stringUserid);
	delete [] stringUserid;

    message2.set_sessionid(Login::sessionId);

	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	message2.set_viewwidth(winSize.width);
	message2.set_viewheight(winSize.height*2);   // (*2): convert to game world position

	std::string macId = GameUtils::getAppUniqueID();
	message2.set_mac(macId);

#if CC_TARGET_PLATFORM==CC_PLATFORM_WIN32
	message2.set_os(1);
	message2.set_platform("laoh");
#elif defined(CC_TARGET_OS_IPHONE)
	message2.set_os(MACHINE_TYPE);
	message2.set_platform(OPERATION_PLATFORM);
#endif

	if (NewCommerStoryManager::getInstance()->IsNewComer())
	{
		message2.set_reload(1);
	}

	string msgData;
	message2.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}