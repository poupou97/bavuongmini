
#include "ReqSender5014.h"

#include "../protobuf/GeneralMessage.pb.h"
#include "../ClientNetEngine.h"
#include "../protobuf/CollectMessage.pb.h"
#include "../element/CPresentBox.h"

IMPLEMENT_CLASS(ReqSender5014)

ReqSender5014::ReqSender5014() 
{

}
ReqSender5014::~ReqSender5014() 
{

}
void* ReqSender5014::createInstance()
{
	return new ReqSender5014() ;
}
void ReqSender5014::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5014::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5014::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5014);
	CCLOG("send msg: %d", comMessage.cmdid());

	CPresentBox* pPresentBox = (CPresentBox *)source;
	long long tmpId = pPresentBox->get_id();

	// set message content
	ReqOpenBox5014 message1;
	message1.set_instanceid(tmpId);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}