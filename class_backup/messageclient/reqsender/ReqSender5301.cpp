
#include "ReqSender5301.h"

#include "../protobuf/WorldBossMessage.pb.h"
#include "../ClientNetEngine.h"

IMPLEMENT_CLASS(ReqSender5301)

	ReqSender5301::ReqSender5301() 
{

}
ReqSender5301::~ReqSender5301() 
{

}
void* ReqSender5301::createInstance()
{
	return new ReqSender5301() ;
}
void ReqSender5301::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5301::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5301::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5301);
	CCLOG("send msg: %d", comMessage.cmdid());

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}