

#ifndef Blog_C___Reflection_ReqSender1704_h
#define Blog_C___Reflection_ReqSender1704_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1704 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender1704)

public:
	SYNTHESIZE(ReqSender1704, int*, m_pValue)

		ReqSender1704() ;
	virtual ~ReqSender1704() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;

} ;

#endif
