
#include "ReqSender1611.h"
#include "../ClientNetEngine.h"
#include "../protobuf/EquipmentMessage.pb.h"
#include "../../ui/equipMent_ui/EquipMentUi.h"
#include "../element/CAdditionProperty.h"

IMPLEMENT_CLASS(ReqSender1611)
ReqSender1611::ReqSender1611() 
{
    
}
ReqSender1611::~ReqSender1611() 
{
}
void* ReqSender1611::createInstance()
{
    return new ReqSender1611() ;
}
void ReqSender1611::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1611::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1611::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1611);
	
	Req1611 message1;
	
	EquipMentUi * equipUI= (EquipMentUi *)source;
	for(int i=0;i<equipUI->propertyLock.size();i++)
	{
		int lockIndex_ = equipUI->propertyLock.at(i);
		message1.add_lockproperties(lockIndex_);
	}
	message1.set_nobinding((int)source1);
	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}