

#ifndef Blog_C___Reflection_ReqSender1901_h
#define Blog_C___Reflection_ReqSender1901_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1901 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1901)
    
public:
    SYNTHESIZE(ReqSender1901, int*, m_pValue)
    
    ReqSender1901() ;
    virtual ~ReqSender1901() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
