
#ifndef Blog_C___Reflection_ReqSender5303_h
#define Blog_C___Reflection_ReqSender5303_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5303: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5303)

public:
	SYNTHESIZE(ReqSender5303, int*, m_pValue)

		ReqSender5303() ;
	virtual ~ReqSender5303() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
