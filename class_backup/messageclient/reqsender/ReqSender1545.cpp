
#include "ReqSender1545.h"
#include "../ClientNetEngine.h"
#include "../protobuf/GuildMessage.pb.h"

IMPLEMENT_CLASS(ReqSender1545)
	ReqSender1545::ReqSender1545() 
{

}
ReqSender1545::~ReqSender1545() 
{
}
void* ReqSender1545::createInstance()
{
	return new ReqSender1545() ;
}
void ReqSender1545::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1545::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1545::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1545);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}