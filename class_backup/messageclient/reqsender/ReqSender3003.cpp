
#include "ReqSender3003.h"
#include "../ClientNetEngine.h"
#include "../protobuf/OfflineFight.pb.h"
#include "../../ui/offlinearena_ui/OffLineArenaUI.h"
#include "../../ui/offlinearena_ui/OffLineArenaData.h"

IMPLEMENT_CLASS(ReqSender3003)
	ReqSender3003::ReqSender3003() 
{

}
ReqSender3003::~ReqSender3003() 
{
}
void* ReqSender3003::createInstance()
{
	return new ReqSender3003() ;
}
void ReqSender3003::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender3003::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender3003::send(void* source, void* source1)
{
	

	CommonMessage comMessage;
	comMessage.set_cmdid(3003);

	ReqFight3003 message1;

	OffLineArenaUI::ReqForChallenge * temp = (OffLineArenaUI::ReqForChallenge *)(source);
	message1.set_currentranking(temp->myRanking);
	message1.set_fightplayer(temp->targetId);
	message1.set_fightplayerranking(temp->targetRanking);

	OffLineArenaData::getInstance()->m_nTargetRanking = temp->targetRanking;

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}