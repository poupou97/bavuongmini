
#include "ReqSender1906.h"

#include "../ClientNetEngine.h"

#include "../protobuf/CopyMessage.pb.h"  
#include "../../ui/challengeRound/ShowLevelUi.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../../ui/challengeRound/SingleRankListUi.h"

IMPLEMENT_CLASS(ReqSender1906)
ReqSender1906::ReqSender1906() 
{
    
}
ReqSender1906::~ReqSender1906() 
{
    
}
void* ReqSender1906::createInstance()
{
    return new ReqSender1906() ;
}
void ReqSender1906::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1906::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1906::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1906);

	ReqReceivePrize1906 message2;

	message2.set_clazz((int)source);
	
	string msgData;
	message2.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}