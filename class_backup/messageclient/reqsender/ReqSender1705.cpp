
#include "ReqSender1705.h"
#include "../ClientNetEngine.h"
#include "../protobuf/EquipmentMessage.pb.h"
#include "../protobuf/AdditionStore.pb.h"
#include "../element/GoodsInfo.h"
#include "../element/CLableGoods.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/goldstore_ui/GoldStoreUI.h"

IMPLEMENT_CLASS(ReqSender1705)
	ReqSender1705::ReqSender1705() 
{

}
ReqSender1705::~ReqSender1705() 
{
}
void* ReqSender1705::createInstance()
{
	return new ReqSender1705() ;
}
void ReqSender1705::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1705::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1705::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1705);

	CLableGoods * temp = (CLableGoods*)(source);
	if (temp == NULL)
		return;

	int lableid = 0;
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoldStoreUI))
	{
		GoldStoreUI * goldStoreUI = (GoldStoreUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoldStoreUI);
		lableid = goldStoreUI->curLableId;
	}

	ReqBuyCommodity1705 message1;
	message1.set_lable(lableid);
	message1.set_commdity(temp->id());
	message1.set_amount((int)source1);  
	message1.set_isbindstore(temp->isbind());
	//message1.set_holeid((int)source);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}