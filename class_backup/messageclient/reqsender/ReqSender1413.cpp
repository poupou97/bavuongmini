
#include "ReqSender1413.h"

#include "../ClientNetEngine.h"

#include "../protobuf/RelationMessage.pb.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../ui/Friend_ui/FriendUi.h"
#include "GameView.h"


IMPLEMENT_CLASS(ReqSender1413)
ReqSender1413::ReqSender1413() 
{
    
}
ReqSender1413::~ReqSender1413() 
{
}
void* ReqSender1413::createInstance()
{
    return new ReqSender1413() ;
}
void ReqSender1413::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1413::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1413::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1413);
	
	
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}