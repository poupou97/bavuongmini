#ifndef Blog_C___Reflection_ReqSender5171_h
#define Blog_C___Reflection_ReqSender5171_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5171: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5171)

public:
	SYNTHESIZE(ReqSender5171, int*, m_pValue)

	ReqSender5171() ;
	virtual ~ReqSender5171() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
