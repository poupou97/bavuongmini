#include "ReqSender5114.h"

#include "../protobuf/GeneralMessage.pb.h"
#include "../ClientNetEngine.h"
#include "../protobuf/AdditionStore.pb.h"

IMPLEMENT_CLASS(ReqSender5114)

ReqSender5114::ReqSender5114() 
{

}
ReqSender5114::~ReqSender5114() 
{

}
void* ReqSender5114::createInstance()
{
	return new ReqSender5114() ;
}
void ReqSender5114::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5114::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5114::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5114);
	CCLOG("send msg: %d", comMessage.cmdid());

	Req5114 message1;
	

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}