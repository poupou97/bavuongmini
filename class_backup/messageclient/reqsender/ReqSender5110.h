
#ifndef Blog_C___Reflection_ReqSender5110_h
#define Blog_C___Reflection_ReqSender5110_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5110: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5110)

public:
	SYNTHESIZE(ReqSender5110, int*, m_pValue)

	ReqSender5110() ;
	virtual ~ReqSender5110() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
