
#include "ReqSender1507.h"
#include "../ClientNetEngine.h"

IMPLEMENT_CLASS(ReqSender1507)
ReqSender1507::ReqSender1507() 
{
    
}
ReqSender1507::~ReqSender1507() 
{
}
void* ReqSender1507::createInstance()
{
    return new ReqSender1507() ;
}
void ReqSender1507::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1507::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1507::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1507);
	
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}