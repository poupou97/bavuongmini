
#include "ReqSender1518.h"
#include "../ClientNetEngine.h"

IMPLEMENT_CLASS(ReqSender1518)
ReqSender1518::ReqSender1518() 
{
    
}
ReqSender1518::~ReqSender1518() 
{
}
void* ReqSender1518::createInstance()
{
    return new ReqSender1518() ;
}
void ReqSender1518::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1518::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1518::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1518);
	
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}