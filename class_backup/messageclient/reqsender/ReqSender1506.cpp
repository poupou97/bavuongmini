
#include "ReqSender1506.h"
#include "../ClientNetEngine.h"
#include "../protobuf/GuildMessage.pb.h"


IMPLEMENT_CLASS(ReqSender1506)
ReqSender1506::ReqSender1506() 
{
    
}
ReqSender1506::~ReqSender1506() 
{
}
void* ReqSender1506::createInstance()
{
    return new ReqSender1506() ;
}
void ReqSender1506::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1506::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1506::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1506);
	
	Req1506 message1;
	
	const char * annoucement_ =(const char *)source;
	message1.set_announcement(annoucement_);
	
	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}