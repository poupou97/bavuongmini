
#ifndef Blog_C___Reflection_ReqSender5053_h
#define Blog_C___Reflection_ReqSender5053_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5053 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5053)

public:
	SYNTHESIZE(ReqSender5053, int*, m_pValue)

		ReqSender5053() ;
	virtual ~ReqSender5053() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
