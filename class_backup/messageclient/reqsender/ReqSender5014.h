
#ifndef Blog_C___Reflection_ReqSender5014_h
#define Blog_C___Reflection_ReqSender5014_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5014: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5014)

public:
	SYNTHESIZE(ReqSender5014, int*, m_pValue)

	ReqSender5014() ;
	virtual ~ReqSender5014() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
