
#include "ReqSender1902.h"

#include "../ClientNetEngine.h"

#include "../protobuf/CopyMessage.pb.h"  
#include "../../ui/challengeRound/SingleCopyResult.h"
#include "GameView.h"
#include "../../gamescene_state/MainScene.h"

IMPLEMENT_CLASS(ReqSender1902)
ReqSender1902::ReqSender1902() 
{
    
}
ReqSender1902::~ReqSender1902() 
{
    
}
void* ReqSender1902::createInstance()
{
    return new ReqSender1902() ;
}
void ReqSender1902::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1902::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1902::send(void* source, void* source1)
{
// 	SingleCopyResult * resultUI = (SingleCopyResult *)GameView::getInstance()->getMainUIScene()->getChildByTag(12301);
// 	if (resultUI)
// 	{
// 		resultUI->endSlowMotion();
// 	}

	CommonMessage comMessage;
	comMessage.set_cmdid(1902);

	Req1902 message2;
	//clazz level
	message2.set_clazz((int)source);
	message2.set_level((int)source1);

	string msgData;
	message2.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}