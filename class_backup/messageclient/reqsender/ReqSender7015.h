
#ifndef Blog_C___Reflection_ReqSender7015_h
#define Blog_C___Reflection_ReqSender7015_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender7015: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender7015)

public:
	SYNTHESIZE(ReqSender7015, int*, m_pValue)

	ReqSender7015() ;
	virtual ~ReqSender7015() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
