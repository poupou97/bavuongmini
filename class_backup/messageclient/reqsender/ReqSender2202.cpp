
#include "ReqSender2202.h"

#include "../ClientNetEngine.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/RelationMessage.pb.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../ui/Friend_ui/FriendUi.h"
#include "../../GameView.h"
#include "../../ui/Mail_ui/MailUI.h"


IMPLEMENT_CLASS(ReqSender2202)
ReqSender2202::ReqSender2202() 
{
    
}
ReqSender2202::~ReqSender2202() 
{
}
void* ReqSender2202::createInstance()
{
    return new ReqSender2202() ;
}
void ReqSender2202::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender2202::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender2202::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(2202);
	
	Req2202 message1;
	
	FriendStruct *friendList =(FriendStruct *)source;
	
	
	message1.set_relationtype(friendList->relationType);
	message1.set_pagenum(friendList->pageNum);
	message1.set_pageindex(friendList->pageIndex);
	message1.set_showonline(friendList->showOnline);


	/*
	MailUI * mailui =(MailUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMailUi);
	if (mailui !=NULL)
	{
		message1.set_relationtype(0); //0好友 1黑名单 2仇人
		message1.set_pagenum(20); //每页的显示数量
		message1.set_pageindex(mailui->setFriendsPage);//第几页
		//是否在线
	}
	
	FriendUi * friend_ui=(FriendUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);
	if (friend_ui !=NULL)
	{
		message1.set_relationtype(friend_ui->curType); //0好友 1黑名单 2仇人
		message1.set_pagenum(friend_ui->pageNumSelect); //每页的显示数量
		message1.set_pageindex(friend_ui->pageIndexSelect);//第几页
	}
	*/


	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}