
#ifndef Blog_C___Reflection_ReqSender5094_h
#define Blog_C___Reflection_ReqSender5094_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5094: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5094)

public:
	SYNTHESIZE(ReqSender5094, int*, m_pValue)

	ReqSender5094() ;
	virtual ~ReqSender5094() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
