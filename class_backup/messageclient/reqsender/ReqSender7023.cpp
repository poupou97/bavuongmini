
#include "ReqSender7023.h"

#include "../ClientNetEngine.h"
#include "../protobuf/MissionMessage.pb.h"

IMPLEMENT_CLASS(ReqSender7023)

ReqSender7023::ReqSender7023() 
{

}
ReqSender7023::~ReqSender7023() 
{

}
void* ReqSender7023::createInstance()
{
	return new ReqSender7023() ;
}
void ReqSender7023::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender7023::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender7023::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(7023);
	CCLOG("send msg: %d", comMessage.cmdid());

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}