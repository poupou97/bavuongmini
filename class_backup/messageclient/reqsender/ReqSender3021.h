#ifndef Blog_C___Reflection_ReqSender3021_h
#define Blog_C___Reflection_ReqSender3021_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender3021 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender3021)

public:
	SYNTHESIZE(ReqSender3021, int*, m_pValue)

	ReqSender3021() ;
	virtual ~ReqSender3021() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;

} ;

#endif
