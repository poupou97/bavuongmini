
#include "ReqSender1501.h"
#include "../ClientNetEngine.h"
#include "../protobuf/GuildMessage.pb.h"


IMPLEMENT_CLASS(ReqSender1501)
ReqSender1501::ReqSender1501() 
{
    
}
ReqSender1501::~ReqSender1501() 
{
}
void* ReqSender1501::createInstance()
{
    return new ReqSender1501() ;
}
void ReqSender1501::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1501::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1501::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1501);
	
	Req1501 message1;
	
	message1.set_page((int)source);

	
	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}