
#ifndef Blog_C___Reflection_ReqSender5140_h
#define Blog_C___Reflection_ReqSender5140_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5140: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5140)

public:
	SYNTHESIZE(ReqSender5140, int*, m_pValue)

	ReqSender5140() ;
	virtual ~ReqSender5140() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
