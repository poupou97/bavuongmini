
#ifndef Blog_C___Reflection_ReqSender5061_h
#define Blog_C___Reflection_ReqSender5061_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5061 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5061)

public:
	SYNTHESIZE(ReqSender5061, int*, m_pValue)

		ReqSender5061() ;
	virtual ~ReqSender5061() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
