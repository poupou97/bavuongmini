
#include "ReqSender5074.h"

#include "../ClientNetEngine.h"
#include "../protobuf/GeneralMessage.pb.h"

IMPLEMENT_CLASS(ReqSender5074)

ReqSender5074::ReqSender5074() 
{

}
ReqSender5074::~ReqSender5074() 
{

}
void* ReqSender5074::createInstance()
{
	return new ReqSender5074() ;
}
void ReqSender5074::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5074::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5074::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5074);
	CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	Req5074 message1;
	message1.set_fightwayid((int)source);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}