
#include "ReqSender1200.h"

#include "../ClientNetEngine.h"
#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"
#include "../../gamescene_state/skill/SkillSeed.h"

IMPLEMENT_CLASS(ReqSender1200)

ReqSender1200::ReqSender1200() 
{
    
}
ReqSender1200::~ReqSender1200() 
{
    
}
void* ReqSender1200::createInstance()
{
    return new ReqSender1200() ;
}
void ReqSender1200::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1200::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1200::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1201);

	Req1200 message2;
	SkillSeed* ss = (SkillSeed*)source;
	message2.set_targetid(ss->attacker);
	message2.set_x(ss->x);
	message2.set_y(ss->y);
	message2.set_skillid(ss->skillId);

	string msgData;
	message2.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}