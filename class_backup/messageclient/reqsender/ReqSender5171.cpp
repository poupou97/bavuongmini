#include "ReqSender5171.h"

#include "../protobuf/GeneralMessage.pb.h"
#include "../ClientNetEngine.h"


IMPLEMENT_CLASS(ReqSender5171)

ReqSender5171::ReqSender5171() 
{

}
ReqSender5171::~ReqSender5171() 
{

}
void* ReqSender5171::createInstance()
{
	return new ReqSender5171() ;
}
void ReqSender5171::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5171::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5171::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5171);
	CCLOG("send msg: %d", comMessage.cmdid());

	Req5171 message1;

	long long general_id = (long long )source;
	message1.set_generalid(general_id);

	int nActionType = (int)source1;
	message1.set_actiontype(nActionType);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}