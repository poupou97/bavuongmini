

#ifndef Blog_C___Reflection_ReqSender2205_h
#define Blog_C___Reflection_ReqSender2205_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender2205 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender2205)
    
public:
    SYNTHESIZE(ReqSender2205, int*, m_pValue)
    
    ReqSender2205() ;
    virtual ~ReqSender2205() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
