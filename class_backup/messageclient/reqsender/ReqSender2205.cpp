
#include "ReqSender2205.h"

#include "../ClientNetEngine.h"


#include "../protobuf/RelationMessage.pb.h"



IMPLEMENT_CLASS(ReqSender2205)
ReqSender2205::ReqSender2205() 
{
    
}
ReqSender2205::~ReqSender2205() 
{
}
void* ReqSender2205::createInstance()
{
    return new ReqSender2205() ;
}
void ReqSender2205::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender2205::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender2205::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(2205);
	
	Req2205 message1;

	/*
	FriendUi * friend_ui=(FriendUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);
	message1.set_relationtype(friend_ui->relationtype_);// 0-好友, 1-黑名单，2-仇人
	message1.set_playerid(friend_ui->playerId_);// 玩家ID
	*/


	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}