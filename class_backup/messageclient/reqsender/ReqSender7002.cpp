
#include "ReqSender7002.h"

#include "../ClientNetEngine.h"
#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../protobuf/ItemMessage.pb.h"
#include "../../ui/backpackscene/PackageScene.h"
#include "../../GameView.h"
#include "../protobuf/MissionMessage.pb.h"
#include "../../gamescene_state/sceneelement/MissionCell.h"
#include "../../ui/missionscene/MissionManager.h"
#include "../../messageclient/element/MissionInfo.h"

IMPLEMENT_CLASS(ReqSender7002)

ReqSender7002::ReqSender7002() 
{

}
ReqSender7002::~ReqSender7002() 
{

}
void* ReqSender7002::createInstance()
{
	return new ReqSender7002() ;
}
void ReqSender7002::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender7002::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender7002::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(7002);
	CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	Req7002 message1;

	//MissionInfo * curInfo =((MissionCell*)source)->curMissionInfo;
	//message1.set_missionpackageid(curInfo->missionpackageid());
	//message1.set_missionid(curInfo->missionid());
	std::string pid = GameView::getInstance()->missionManager->curMissionInfo->missionpackageid();
	std::string mid = GameView::getInstance()->missionManager->curMissionInfo->missionid();
	int mseq = GameView::getInstance()->missionManager->curMissionInfo->missionsequnce();
	message1.set_missionpackageid(pid);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}