

#ifndef Blog_C___Reflection_ReqSender1624_h
#define Blog_C___Reflection_ReqSender1624_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1624 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1624)
    
public:
    SYNTHESIZE(ReqSender1624, int*, m_pValue)
    
    ReqSender1624() ;
    virtual ~ReqSender1624() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
