
#include "ReqSender1532.h"
#include "../ClientNetEngine.h"
#include "../protobuf/GuildMessage.pb.h"


IMPLEMENT_CLASS(ReqSender1532)
ReqSender1532::ReqSender1532() 
{

}
ReqSender1532::~ReqSender1532() 
{
}
void* ReqSender1532::createInstance()
{
	return new ReqSender1532() ;
}
void ReqSender1532::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1532::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1532::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1532);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}