

#ifndef Blog_C___Reflection_ReqSender1803_h
#define Blog_C___Reflection_ReqSender1803_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1803 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1803)
    
public:
    SYNTHESIZE(ReqSender1803, int*, m_pValue)
    
    ReqSender1803() ;
    virtual ~ReqSender1803() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;

} ;

#endif
