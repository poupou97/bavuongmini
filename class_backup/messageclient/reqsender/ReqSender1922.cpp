#include "ReqSender1922.h"

#include "../ClientNetEngine.h"
#include "../protobuf/CopyMessage.pb.h" 
#include "../../ui/FivePersonInstance/FivePersonInstance.h"

IMPLEMENT_CLASS(ReqSender1922)
ReqSender1922::ReqSender1922() 
{
    
}
ReqSender1922::~ReqSender1922() 
{
    
}
void* ReqSender1922::createInstance()
{
    return new ReqSender1922() ;
}
void ReqSender1922::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1922::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1922::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1922);

	CCLOG("send msg: %d, request to cancel instance sequence ", comMessage.cmdid());

	Req1922 message2;
	message2.set_copyid((int)source);

	string msgData;
	message2.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}