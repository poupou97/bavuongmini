
#include "ReqSender5059.h"

#include "../ClientNetEngine.h"
#include "../protobuf/GeneralMessage.pb.h"
#include "../../ui/generals_ui/GeneralsUI.h"
#include "../../ui/generals_ui/GeneralsTeachUI.h"

IMPLEMENT_CLASS(ReqSender5059)

ReqSender5059::ReqSender5059() 
{

}
ReqSender5059::~ReqSender5059() 
{

}
void* ReqSender5059::createInstance()
{
	return new ReqSender5059() ;
}
void ReqSender5059::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5059::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5059::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5059);
	CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	Req5059 message1;
	message1.set_generalid((int)source);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}