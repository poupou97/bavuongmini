
#include "ReqSender5111.h"

#include "../protobuf/GeneralMessage.pb.h"
#include "../protobuf/DailyGift.pb.h"
#include "../ClientNetEngine.h"

IMPLEMENT_CLASS(ReqSender5111)

ReqSender5111::ReqSender5111() 
{

}
ReqSender5111::~ReqSender5111() 
{

}
void* ReqSender5111::createInstance()
{
	return new ReqSender5111() ;
}
void ReqSender5111::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5111::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5111::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5111);
	CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	Req5111 message1;
	message1.set_number((int)source);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}