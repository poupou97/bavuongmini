
#include "ReqSender1206.h"

#include "../ClientNetEngine.h"
#include "../protobuf/GeneralMessage.pb.h"  

IMPLEMENT_CLASS(ReqSender1206)

	ReqSender1206::ReqSender1206() 
{

}
ReqSender1206::~ReqSender1206() 
{

}
void* ReqSender1206::createInstance()
{
	return new ReqSender1206() ;
}
void ReqSender1206::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1206::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1206::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1206);

	Req1206 message1;
	message1.set_petmode((int)source);
	message1.set_autofight((int)source1);

	//CCLOG("send msg: %d, skill id: %s", comMessage.cmdid(), ss->skillId.c_str());

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}