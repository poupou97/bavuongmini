
#include "ReqSender5303.h"

#include "../protobuf/WorldBossMessage.pb.h"
#include "../ClientNetEngine.h"

IMPLEMENT_CLASS(ReqSender5303)

	ReqSender5303::ReqSender5303() 
{

}
ReqSender5303::~ReqSender5303() 
{

}
void* ReqSender5303::createInstance()
{
	return new ReqSender5303() ;
}
void ReqSender5303::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5303::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5303::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5303);
	CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	ReqReceivePrize5303 message1;
	message1.set_id((long long)source);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}