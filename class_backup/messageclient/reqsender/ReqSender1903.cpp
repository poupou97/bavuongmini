
#include "ReqSender1903.h"

#include "../ClientNetEngine.h"

#include "../protobuf/CopyMessage.pb.h"  
#include "../../ui/challengeRound/ShowLevelUi.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../../ui/challengeRound/ChallengeRoundUi.h"
#include "../../ui/Active_ui/ActiveUI.h"

IMPLEMENT_CLASS(ReqSender1903)
ReqSender1903::ReqSender1903() 
{
    
}
ReqSender1903::~ReqSender1903() 
{
    
}
void* ReqSender1903::createInstance()
{
    return new ReqSender1903() ;
}
void ReqSender1903::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1903::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1903::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1903);

	ReqTowerHistory1903 message2;
	
	//ActiveUI * activeui = (ActiveUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveUI);
	ChallengeRoundUi * challengeui_ = (ChallengeRoundUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagChallengeRoundUi);
	ShowLevelUi * levelui_ = (ShowLevelUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagSingCopylevel);

	message2.set_clazz(challengeui_->getCopyClazz());
	message2.set_page(levelui_->getCurpageIndex());
	message2.set_num(levelui_->m_everyPageNum);

	string msgData;
	message2.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}