
#include "ReqSender1610.h"
#include "../ClientNetEngine.h"
#include "../protobuf/EquipmentMessage.pb.h"
#include "../../ui/equipMent_ui/EquipMentUi.h"



IMPLEMENT_CLASS(ReqSender1610)
ReqSender1610::ReqSender1610() 
{
    
}
ReqSender1610::~ReqSender1610() 
{
}
void* ReqSender1610::createInstance()
{
    return new ReqSender1610() ;
}
void ReqSender1610::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1610::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1610::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1610);
	
	Req1610 message1;
	EquipMentUi * equip=(EquipMentUi *)source;
	
	switch(equip->mes_source)
	{
	case 1:
		{
			message1.set_source(equip->mes_source);
			message1.set_petid(equip->generalMainId);
			message1.set_pob(equip->generalEquipOfPart);
		}break;
	case 2:
		{
			message1.set_source(equip->mes_source);
			message1.set_index(equip->mes_index);
		}break;
	}

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}