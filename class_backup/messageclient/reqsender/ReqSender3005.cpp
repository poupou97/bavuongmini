
#include "ReqSender3005.h"
#include "../ClientNetEngine.h"
#include "../protobuf/OfflineFight.pb.h"

IMPLEMENT_CLASS(ReqSender3005)
	ReqSender3005::ReqSender3005() 
{

}
ReqSender3005::~ReqSender3005() 
{
}
void* ReqSender3005::createInstance()
{
	return new ReqSender3005() ;
}
void ReqSender3005::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender3005::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender3005::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(3005);

	ReqBuyFightTimes3005 message1;
	message1.set_amount(int(source));

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}