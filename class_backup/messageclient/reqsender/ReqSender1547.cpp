
#include "ReqSender1547.h"
#include "../ClientNetEngine.h"
#include "../protobuf/GuildMessage.pb.h"

IMPLEMENT_CLASS(ReqSender1547)
ReqSender1547::ReqSender1547() 
{

}
ReqSender1547::~ReqSender1547() 
{
}
void* ReqSender1547::createInstance()
{
	return new ReqSender1547() ;
}
void ReqSender1547::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1547::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1547::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1547);
	Req1547 message1;
	message1.set_bossflg((int)source);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}