
#include "ReqSender1523.h"
#include "../ClientNetEngine.h"
#include "../protobuf/GuildMessage.pb.h"


IMPLEMENT_CLASS(ReqSender1523)
ReqSender1523::ReqSender1523() 
{
    
}
ReqSender1523::~ReqSender1523() 
{
}
void* ReqSender1523::createInstance()
{
    return new ReqSender1523() ;
}
void ReqSender1523::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1523::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1523::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1523);
	
	Req1523 message1;
	
	const char * declaration_ =(const char *)source;
	message1.set_declaration(declaration_);
	
	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}