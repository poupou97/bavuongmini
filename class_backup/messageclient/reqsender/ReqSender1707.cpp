
#include "ReqSender1707.h"
#include "../ClientNetEngine.h"
#include "../protobuf/AdditionStore.pb.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"

IMPLEMENT_CLASS(ReqSender1707)
ReqSender1707::ReqSender1707() 
{

}
ReqSender1707::~ReqSender1707() 
{
}
void* ReqSender1707::createInstance()
{
	return new ReqSender1707() ;
}
void ReqSender1707::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1707::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1707::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1707);

	ReqBuyHonorCommodity1707 message1;

	message1.set_id((int)source);
	message1.set_amount((int)source1);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}