#include "ReqSender5130.h"

#include "../protobuf/GeneralMessage.pb.h"
#include "../ClientNetEngine.h"
#include "../protobuf/ActiveMessage.pb.h"

IMPLEMENT_CLASS(ReqSender5130)

ReqSender5130::ReqSender5130() 
{

}
ReqSender5130::~ReqSender5130() 
{

}
void* ReqSender5130::createInstance()
{
	return new ReqSender5130() ;
}
void ReqSender5130::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5130::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5130::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5130);
	CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	Req5130 message1;
	message1.set_number((int)source);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}