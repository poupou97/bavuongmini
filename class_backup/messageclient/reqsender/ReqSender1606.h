

#ifndef Blog_C___Reflection_ReqSender1606_h
#define Blog_C___Reflection_ReqSender1606_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1606 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1606)
    
public:
    SYNTHESIZE(ReqSender1606, int*, m_pValue)
    
    ReqSender1606() ;
    virtual ~ReqSender1606() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
