
#include "ReqSender3002.h"
#include "../ClientNetEngine.h"
#include "../protobuf/OfflineFight.pb.h"

IMPLEMENT_CLASS(ReqSender3002)
	ReqSender3002::ReqSender3002() 
{

}
ReqSender3002::~ReqSender3002() 
{
}
void* ReqSender3002::createInstance()
{
	return new ReqSender3002() ;
}
void ReqSender3002::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender3002::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender3002::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(3002);

	ReqRanking3002 message1;

	message1.set_page(int(source));
	message1.set_limit(int(source1));

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}