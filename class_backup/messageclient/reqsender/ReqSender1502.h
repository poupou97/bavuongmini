

#ifndef Blog_C___Family_ReqSender1502_h
#define Blog_C___Family_ReqSender1502_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1502 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1502)
    
public:
	SYNTHESIZE(ReqSender1502, int*, m_pValue)

	ReqSender1502() ;
    virtual ~ReqSender1502() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
