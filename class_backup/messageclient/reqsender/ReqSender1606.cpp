
#include "ReqSender1606.h"
#include "../ClientNetEngine.h"
#include "../protobuf/EquipmentMessage.pb.h"


IMPLEMENT_CLASS(ReqSender1606)
ReqSender1606::ReqSender1606() 
{
    
}
ReqSender1606::~ReqSender1606() 
{
}
void* ReqSender1606::createInstance()
{
    return new ReqSender1606() ;
}
void ReqSender1606::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1606::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1606::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1606);
	
	Req1606 message1;
	message1.set_index((int )source);
	message1.set_amount((int )source1);

	
	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}