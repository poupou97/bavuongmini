

#ifndef Blog_C___Reflection_ReqSender1706_h
#define Blog_C___Reflection_ReqSender1706_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1706 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender1706)

public:
	SYNTHESIZE(ReqSender1706, int*, m_pValue)

		ReqSender1706() ;
	virtual ~ReqSender1706() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;

} ;

#endif
