
#include "ReqSender1534.h"
#include "../ClientNetEngine.h"
#include "../protobuf/GuildMessage.pb.h"


IMPLEMENT_CLASS(ReqSender1534)
ReqSender1534::ReqSender1534() 
{

}
ReqSender1534::~ReqSender1534() 
{
}
void* ReqSender1534::createInstance()
{
	return new ReqSender1534() ;
}
void ReqSender1534::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1534::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1534::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1534);

	Req1534 message1;
	message1.set_pageno((int)source);
	message1.set_pagesize((int)source1);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}