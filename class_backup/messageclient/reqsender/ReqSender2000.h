

#ifndef Blog_C___Reflection_ReqSender2000_h
#define Blog_C___Reflection_ReqSender2000_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender2000 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender2000)
    
public:
    SYNTHESIZE(ReqSender2000, int*, m_pValue)
    
    ReqSender2000() ;
    virtual ~ReqSender2000() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
