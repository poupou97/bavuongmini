
#include "ReqSender2200.h"

#include "../ClientNetEngine.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/RelationMessage.pb.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../ui/Friend_ui/FriendUi.h"
#include "../../GameView.h"
#include "../../ui/Chat_ui/ChatUI.h"


IMPLEMENT_CLASS(ReqSender2200)
ReqSender2200::ReqSender2200() 
{
    
}
ReqSender2200::~ReqSender2200() 
{
}
void* ReqSender2200::createInstance()
{
    return new ReqSender2200() ;
}
void ReqSender2200::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender2200::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender2200::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(2200);
	
	Req2200 message1;
	 

	int worktype_ ;
	int relationType_;
	long long playerId;
	std::string playName;

	FriendStruct *friendStr = (FriendStruct *)source;
	worktype_=	friendStr->operation;
	relationType_ = friendStr->relationType;
	playerId= friendStr->playerid;
	playName= friendStr->playerName;

	message1.set_operation(worktype_);
	message1.set_relationtype(relationType_);
	message1.set_playerid(playerId);
	message1.set_playername(playName);
	
	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}