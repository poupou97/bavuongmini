
#include "ReqSender1110.h"

#include "../ClientNetEngine.h"

#include "../protobuf/PlayerMessage.pb.h"  

IMPLEMENT_CLASS(ReqSender1110)
	ReqSender1110::ReqSender1110() 
{

}
ReqSender1110::~ReqSender1110() 
{

}
void* ReqSender1110::createInstance()
{
	return new ReqSender1110() ;
}
void ReqSender1110::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1110::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1110::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1110);

	Req1110 message1;
	message1.set_channelidx((int)source);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}