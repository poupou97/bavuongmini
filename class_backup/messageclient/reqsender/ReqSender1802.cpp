
#include "ReqSender1802.h"

#include "../ClientNetEngine.h"

#include "../protobuf/AuctionMessage.pb.h"
#include "../../ui/Auction_ui/AuctionUi.h"


IMPLEMENT_CLASS(ReqSender1802)
ReqSender1802::ReqSender1802() 
{
    
}
ReqSender1802::~ReqSender1802() 
{
}
void* ReqSender1802::createInstance()
{
    return new ReqSender1802() ;
}
void ReqSender1802::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1802::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1802::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1802);
	
	Req1802 message1;

	AuctionUi* auctionui =(AuctionUi *)source; //(AuctionUi*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagAuction);

	message1.set_index(auctionui->setIndex);//寄售品在背包的索引
	message1.set_price(auctionui->setPrice);// 
	message1.set_propnum(auctionui->setPropNum);//
	message1.set_currency(auctionui->setPriceType);//价格类型 1:金币 2、元宝

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}