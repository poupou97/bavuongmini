#include "ReqSender1330.h"
#include "../ClientNetEngine.h"
#include "../protobuf/ItemMessage.pb.h"

IMPLEMENT_CLASS(ReqSender1330)

ReqSender1330::ReqSender1330() 
{

}
ReqSender1330::~ReqSender1330() 
{

}
void* ReqSender1330::createInstance()
{
	return new ReqSender1330() ;
}
void ReqSender1330::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1330::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1330::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1330);
	
	Req1330 message1;
	message1.set_petid((long long )source);
	
	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}
