

#ifndef Blog_C___Reflection_ReqSender2202_h
#define Blog_C___Reflection_ReqSender2202_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender2202 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender2202)
    
public:
    SYNTHESIZE(ReqSender2202, int*, m_pValue)
    
    ReqSender2202() ;
    virtual ~ReqSender2202() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;
	struct FriendStruct
	{
		int relationType;
		int pageNum;
		int pageIndex;
		int showOnline;
	};

} ;

#endif
