
#include "ReqSender5121.h"

#include "../protobuf/GeneralMessage.pb.h"
#include "../ClientNetEngine.h"
#include "../protobuf/DailyGiftSign.pb.h"

IMPLEMENT_CLASS(ReqSender5121)

ReqSender5121::ReqSender5121() 
{

}
ReqSender5121::~ReqSender5121() 
{

}
void* ReqSender5121::createInstance()
{
	return new ReqSender5121() ;
}
void ReqSender5121::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5121::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5121::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5121);
	CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	Req5121 message1;
	message1.set_talentid((int)source);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}