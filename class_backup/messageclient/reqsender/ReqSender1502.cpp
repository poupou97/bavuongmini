
#include "ReqSender1502.h"
#include "../ClientNetEngine.h"
#include "../protobuf/GuildMessage.pb.h"


IMPLEMENT_CLASS(ReqSender1502)
ReqSender1502::ReqSender1502() 
{
    
}
ReqSender1502::~ReqSender1502() 
{
}
void* ReqSender1502::createInstance()
{
    return new ReqSender1502() ;
}
void ReqSender1502::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1502::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1502::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1502);
	
	Req1502 message1;
	
	message1.set_id((long long)source);

	
	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}