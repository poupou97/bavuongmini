
#include "ReqSender5053.h"

#include "../ClientNetEngine.h"
#include "../protobuf/GeneralMessage.pb.h"
#include "../../ui/generals_ui/generals_popup_ui/GeneralsSkillListUI.h"

IMPLEMENT_CLASS(ReqSender5053)

	ReqSender5053::ReqSender5053() 
{

}
ReqSender5053::~ReqSender5053() 
{

}
void* ReqSender5053::createInstance()
{
	return new ReqSender5053() ;
}
void ReqSender5053::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5053::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5053::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5053);
	CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	Req5053 message1;

	GeneralsSkillListUI::ReqData * temp = (GeneralsSkillListUI::ReqData *)source;
	message1.set_generalid(temp->generalsid);
	message1.set_skillpropid(temp->skillid);
	message1.set_index(temp->index);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}