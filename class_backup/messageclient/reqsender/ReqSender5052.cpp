
#include "ReqSender5052.h"

#include "../ClientNetEngine.h"
#include "../protobuf/GeneralMessage.pb.h"

IMPLEMENT_CLASS(ReqSender5052)

	ReqSender5052::ReqSender5052() 
{

}
ReqSender5052::~ReqSender5052() 
{

}
void* ReqSender5052::createInstance()
{
	return new ReqSender5052() ;
}
void ReqSender5052::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5052::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5052::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5052);
	//CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	Req5052 message1;

	long long g_id = (long long )source;
	message1.set_generalid(g_id);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}