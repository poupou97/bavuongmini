
#ifndef Blog_C___Reflection_ReqSender1319_h
#define Blog_C___Reflection_ReqSender1319_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1319 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender1319)

public:
	SYNTHESIZE(ReqSender1319, int*, m_pValue)

		ReqSender1319() ;
	virtual ~ReqSender1319() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
