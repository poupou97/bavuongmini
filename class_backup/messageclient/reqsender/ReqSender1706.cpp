
#include "ReqSender1706.h"
#include "../ClientNetEngine.h"
#include "../protobuf/AdditionStore.pb.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"

IMPLEMENT_CLASS(ReqSender1706)
	ReqSender1706::ReqSender1706() 
{

}
ReqSender1706::~ReqSender1706() 
{
}
void* ReqSender1706::createInstance()
{
	return new ReqSender1706() ;
}
void ReqSender1706::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1706::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1706::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1706);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}