#ifndef Blog_C___Reflection_ReqSender3006_h
#define Blog_C___Reflection_ReqSender3006_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender3006 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender3006)

public:
	SYNTHESIZE(ReqSender3006, int*, m_pValue)

		ReqSender3006() ;
	virtual ~ReqSender3006() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;

} ;

#endif
