#ifndef Blog_C___Reflection_ReqSender1923_h
#define Blog_C___Reflection_ReqSender1923_h

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1923 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1923)
    
public:
    SYNTHESIZE(ReqSender1923, int*, m_pValue)
    
    ReqSender1923() ;
    virtual ~ReqSender1923() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
