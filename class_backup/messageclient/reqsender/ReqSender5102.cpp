
#include "ReqSender5102.h"

#include "../protobuf/GeneralMessage.pb.h"
#include "../ClientNetEngine.h"

IMPLEMENT_CLASS(ReqSender5102)

ReqSender5102::ReqSender5102() 
{

}
ReqSender5102::~ReqSender5102() 
{

}
void* ReqSender5102::createInstance()
{
	return new ReqSender5102() ;
}
void ReqSender5102::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5102::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5102::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5102);
	CCLOG("send msg: %d", comMessage.cmdid());

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}