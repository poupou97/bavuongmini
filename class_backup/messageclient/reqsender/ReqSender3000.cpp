
#include "ReqSender3000.h"
#include "../ClientNetEngine.h"
#include "../protobuf/OfflineFight.pb.h"

IMPLEMENT_CLASS(ReqSender3000)
	ReqSender3000::ReqSender3000() 
{

}
ReqSender3000::~ReqSender3000() 
{
}
void* ReqSender3000::createInstance()
{
	return new ReqSender3000() ;
}
void ReqSender3000::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender3000::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender3000::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(3000);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}