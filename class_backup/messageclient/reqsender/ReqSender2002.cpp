
#include "ReqSender2002.h"

#include "../ClientNetEngine.h"

#include "../protobuf/MailMessage.pb.h"
#include "../../ui/Mail_ui/MailUI.h"
#include "GameView.h"
#include "../../messageclient/element/CMailInfo.h"


IMPLEMENT_CLASS(ReqSender2002)
ReqSender2002::ReqSender2002() 
{
    
}
ReqSender2002::~ReqSender2002() 
{
}
void* ReqSender2002::createInstance()
{
    return new ReqSender2002() ;
}
void ReqSender2002::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender2002::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender2002::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(2002);

	Req2002 message1;
	message1.set_type((int)source);
	message1.set_mailid((long long)source1);
	
	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}