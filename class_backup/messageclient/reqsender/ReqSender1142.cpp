
#include "ReqSender1142.h"

#include "../ClientNetEngine.h"
#include "../protobuf/EquipmentMessage.pb.h"



IMPLEMENT_CLASS(ReqSender1142)
	ReqSender1142::ReqSender1142() 
{

}
ReqSender1142::~ReqSender1142() 
{

}
void* ReqSender1142::createInstance()
{
	return new ReqSender1142() ;
}
void ReqSender1142::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1142::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1142::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1142);
	Req1142 message1;


	message1.set_playerid((long long)source);


	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);


	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}