
#include "ReqSender5108.h"

#include "../ClientNetEngine.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../../GameView.h"

IMPLEMENT_CLASS(ReqSender5108)
	ReqSender5108::ReqSender5108() 
{

}
ReqSender5108::~ReqSender5108() 
{

}
void* ReqSender5108::createInstance()
{
	return new ReqSender5108() ;
}
void ReqSender5108::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5108::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5108::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5108);
	CCLOG("send msg: %d, logout", comMessage.cmdid());

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}