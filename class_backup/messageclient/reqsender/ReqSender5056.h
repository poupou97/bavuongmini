
#ifndef Blog_C___Reflection_ReqSender5056_h
#define Blog_C___Reflection_ReqSender5056_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5056 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5056)

public:
	SYNTHESIZE(ReqSender5056, int*, m_pValue)

		ReqSender5056() ;
	virtual ~ReqSender5056() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
