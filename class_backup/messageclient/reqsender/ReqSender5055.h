
#ifndef Blog_C___Reflection_ReqSender5055_h
#define Blog_C___Reflection_ReqSender5055_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5055: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5055)

public:
	SYNTHESIZE(ReqSender5055, int*, m_pValue)

		ReqSender5055() ;
	virtual ~ReqSender5055() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
