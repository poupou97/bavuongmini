

#ifndef Blog_C___Reflection_ReqSender1002_h
#define Blog_C___Reflection_ReqSender1002_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1002 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1002)
    
public:
    SYNTHESIZE(ReqSender1002, int*, m_pValue)
    
    ReqSender1002() ;
    virtual ~ReqSender1002() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
