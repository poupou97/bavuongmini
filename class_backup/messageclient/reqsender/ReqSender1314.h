
#ifndef Blog_C___Reflection_ReqSender1314_h
#define Blog_C___Reflection_ReqSender1314_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1314 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender1314)

public:
	SYNTHESIZE(ReqSender1314, int*, m_pValue)

		ReqSender1314() ;
	virtual ~ReqSender1314() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
