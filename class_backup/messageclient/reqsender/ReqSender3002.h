#ifndef Blog_C___Reflection_ReqSender3002_h
#define Blog_C___Reflection_ReqSender3002_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender3002 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender3002)

public:
	SYNTHESIZE(ReqSender3002, int*, m_pValue)

		ReqSender3002() ;
	virtual ~ReqSender3002() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;

} ;

#endif
