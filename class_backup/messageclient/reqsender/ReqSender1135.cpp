
#include "ReqSender1135.h"

#include "../ClientNetEngine.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"
#include "../protobuf/NPCMessage.pb.h"
#include "../../ui/shop_ui/ShopUI.h"

IMPLEMENT_CLASS(ReqSender1135)
	ReqSender1135::ReqSender1135() 
{

}
ReqSender1135::~ReqSender1135() 
{

}
void* ReqSender1135::createInstance()
{
	return new ReqSender1135() ;
}
void ReqSender1135::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1135::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1135::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1135);
	Req1135 message1;

	ShopUI::sellGoodsStruct * goodsStruct = (ShopUI::sellGoodsStruct *)source;

	message1.set_folder(goodsStruct->folderIndex);
	message1.set_amount(goodsStruct->amount);
	message1.set_autoflag(goodsStruct->autoflag);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);


	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}