
#include "ReqSender1303.h"

#include "../ClientNetEngine.h"
#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../protobuf/ItemMessage.pb.h"
#include "../../ui/backpackscene/PackageScene.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../messageclient/element/FolderInfo.h"

IMPLEMENT_CLASS(ReqSender1303)

	ReqSender1303::ReqSender1303() 
{

}
ReqSender1303::~ReqSender1303() 
{

}
void* ReqSender1303::createInstance()
{
	return new ReqSender1303() ;
}
void ReqSender1303::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1303::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1303::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1303);
	CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	Req1303 message1;

	FolderInfo * folder_ =(FolderInfo *)source;
	long long generalId_ =(long long)source1;

	int pop =0;
	switch(folder_->goods().equipmentclazz())
	{
	case 0 :
		break;
	case 1 :
		pop = 4;
		break;
	case 2 :
		pop = 2;
		break;
	case 3 :
		pop = 0;
		break;
	case 4 :
		pop = 1;
		break;
	case 5 :
		pop = 7;
		break;
	case 6 :
		pop = 0;
		break;
	case 7 :
		pop = 3;
		break;
	case 8 :
		pop = 9;
		break;
	case 9 :
		pop = 10;
		break;
	case 10 :
		pop = 11;
		break;
	}

	message1.set_folderid(folder_->id());
	message1.set_pob(pop);
	message1.set_generalid(generalId_);




	/*
	PackageScene * packageScene = (PackageScene *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
	message1.set_folderid(packageScene->pacPageView->curPackageItemIndex);
	int pop = 0;
	switch(GameView::getInstance()->pacPageView->curFolder->goods().equipmentclazz())
	{
	case 0 :
		break;
	case 1 :
		pop = 4;
		break;
	case 2 :
		pop = 2;
		break;
	case 3 :
		pop = 0;
		break;
	case 4 :
		pop = 1;
		break;
	case 5 :
		pop = 7;
		break;
	case 6 :
		pop = 0;
		break;
	case 7 :
		pop = 3;
		break;
	case 8 :
		pop = 9;
		break;
	case 9 :
		pop = 10;
		break;
	case 10 :
		pop = 11;
		break;
	}
	message1.set_pob(pop);
	message1.set_generalid(packageScene->selectActorId);
	*/
	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}

