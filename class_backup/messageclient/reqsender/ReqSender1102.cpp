
#include "ReqSender1102.h"

#include "../ClientNetEngine.h"
#include "../protobuf/PlayerMessage.pb.h"  
#include "../../gamescene_state/role/MyPlayer.h"

IMPLEMENT_CLASS(ReqSender1102)

ReqSender1102::ReqSender1102() 
{
    
}
ReqSender1102::~ReqSender1102() 
{
    
}
void* ReqSender1102::createInstance()
{
    return new ReqSender1102() ;
}
void ReqSender1102::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1102::display() 
{
    cout << *getm_pValue() << endl ;
}

/*
* @param source1, it indicate the target point
*/
void ReqSender1102::send(void* source, void* source1)
{
	// set message id
	CommonMessage comMessage;
	comMessage.set_cmdid(1102);

	// set message content
	CCPoint* destination = (CCPoint*)source;

	Req1102 message2;
	if(destination == NULL)
	{
		// when using joystick, don't send target
		message2.set_touchx(0);
		message2.set_touchy(0);
	}
	else
	{
		message2.set_touchx(destination->x);
		message2.set_touchy(destination->y);
	}

	//CCLOG("send msg: %d, MyPlayer target Position: ( %d, %d ) ", comMessage.cmdid(), message2.touchx(), message2.touchy());

	string msgData;
	message2.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}