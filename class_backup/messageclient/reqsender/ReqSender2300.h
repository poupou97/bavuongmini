#ifndef Blog_C___Reflection_ReqSender2300_h
#define Blog_C___Reflection_ReqSender2300_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender2300: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender2300)

public:
	SYNTHESIZE(ReqSender2300, int*, m_pValue)

	ReqSender2300() ;
	virtual ~ReqSender2300() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
