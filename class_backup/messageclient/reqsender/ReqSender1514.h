
#ifndef Blog_C___Family_ReqSender1514_h
#define Blog_C___Family_ReqSender1514_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1514 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1514)
    
public:
	SYNTHESIZE(ReqSender1514, int*, m_pValue)

	ReqSender1514() ;
    virtual ~ReqSender1514() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
