
#include "ReqSender1627.h"
#include "../ClientNetEngine.h"
#include "../protobuf/EquipmentMessage.pb.h"
#include "../../ui/equipMent_ui/SysthesisUI.h"

IMPLEMENT_CLASS(ReqSender1627)
	ReqSender1627::ReqSender1627() 
{

}
ReqSender1627::~ReqSender1627() 
{
}
void* ReqSender1627::createInstance()
{
	return new ReqSender1627() ;
}
void ReqSender1627::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1627::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1627::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1627);

	ReqMerge1627 message1;

	SysthesisUI::ReqData *data = (SysthesisUI::ReqData *)source;
	message1.set_mergeid(data->mergeid);
	message1.set_num(data->num);
	message1.set_unbindfirst(data->unbindfirst);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}