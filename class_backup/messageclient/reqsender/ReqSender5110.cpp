
#include "ReqSender5110.h"

#include "../protobuf/GeneralMessage.pb.h"
#include "../ClientNetEngine.h"

IMPLEMENT_CLASS(ReqSender5110)

ReqSender5110::ReqSender5110() 
{

}
ReqSender5110::~ReqSender5110() 
{

}
void* ReqSender5110::createInstance()
{
	return new ReqSender5110() ;
}
void ReqSender5110::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5110::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5110::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5110);
	CCLOG("send msg: %d", comMessage.cmdid());

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}