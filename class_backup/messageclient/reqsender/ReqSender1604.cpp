
#include "ReqSender1604.h"
#include "../ClientNetEngine.h"
#include "../protobuf/EquipmentMessage.pb.h"
#include "../../ui/equipMent_ui/EquipMentUi.h"



IMPLEMENT_CLASS(ReqSender1604)
ReqSender1604::ReqSender1604() 
{
    
}
ReqSender1604::~ReqSender1604() 
{
}
void* ReqSender1604::createInstance()
{
    return new ReqSender1604() ;
}
void ReqSender1604::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1604::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1604::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1604);
	//CCLOG("req1604 is refine");
	Req1604 message1;
	EquipMentUi * equip=(EquipMentUi *)source;
	
	
	
	
	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}