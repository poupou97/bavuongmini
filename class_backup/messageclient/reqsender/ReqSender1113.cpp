
#include "ReqSender1113.h"

#include "../ClientNetEngine.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"

IMPLEMENT_CLASS(ReqSender1113)
ReqSender1113::ReqSender1113() 
{
    
}
ReqSender1113::~ReqSender1113() 
{
    
}
void* ReqSender1113::createInstance()
{
    return new ReqSender1113() ;
}
void ReqSender1113::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1113::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1113::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1113);
	
	// no content
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}