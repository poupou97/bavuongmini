
#include "ReqSender1531.h"
#include "../ClientNetEngine.h"
#include "../protobuf/GuildMessage.pb.h"


IMPLEMENT_CLASS(ReqSender1531)
	ReqSender1531::ReqSender1531() 
{

}
ReqSender1531::~ReqSender1531() 
{
}
void* ReqSender1531::createInstance()
{
	return new ReqSender1531() ;
}
void ReqSender1531::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1531::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1531::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1531);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}