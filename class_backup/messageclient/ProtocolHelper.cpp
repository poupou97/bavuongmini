
#include "ProtocolHelper.h"

#include "../gamescene_state/role/BaseFighter.h"
#include "../gamescene_state/GameSceneState.h"
#include "../gamescene_state/MainScene.h"
#include "../gamescene_state/exstatus/ExStatus.h"
#include "../gamescene_state/exstatus/ExStatusType.h"
#include "../gamescene_state/exstatus/ExStatusFactory.h"
#include "GameView.h"
#include "../gamescene_state/role/General.h"
#include "element/CTeamMember.h"
#include "../gamescene_state/role/MyPlayer.h"
#include "../gamescene_state/sceneelement/MissionAndTeam.h"
#include "../gamescene_state/sceneelement/TargetInfoMini.h"
#include "element/CMapInfo.h"
#include "../gamescene_state/role/OtherPlayer.h"
#include "../gamescene_state/sceneelement/GeneralsInfoUI.h"
#include "../gamescene_state/role/ActorUtils.h"

static ProtocolHelper *s_SharedProtocolHelper = NULL;

ProtocolHelper* ProtocolHelper::sharedProtocolHelper(void)
{
    if (!s_SharedProtocolHelper)
    {
		s_SharedProtocolHelper = new ProtocolHelper();
    }

    return s_SharedProtocolHelper;
}

ProtocolHelper::ProtocolHelper() 
{
    
}
ProtocolHelper::~ProtocolHelper() 
{
    
}

void ProtocolHelper::updateFighterStatus(::google::protobuf::RepeatedPtrField< ExtStatusInfo >* newExStatusList, BaseFighter* target)
{
	if(newExStatusList->size() > 0)
	{
		// update exstatus effect for display
		// 1. remove the exstatus which will disappear
		// 1.1 get all exstatus types which will be removed
		std::map<int, ExStatus*> needRemovedTypeMap;
		std::vector<ExStatus*>& oldStatus = target->getExStatusVector();
		for (std::vector<ExStatus*>::iterator it = oldStatus.begin(); it != oldStatus.end(); ++it) 
		{
			ExStatus* status = (*it);

			bool bRemove = true;
			for(int i = 0; i < newExStatusList->size(); i++)
			{
				if(status->type() == newExStatusList->Get(i).type())
				{
					bRemove = false;
					break;
				}
			}

			if(bRemove)
				needRemovedTypeMap[status->type()] = status;
		}
		// 1.2 cancel the exstatus which is removed
	    for (std::map<int, ExStatus*>::iterator it = needRemovedTypeMap.begin(); it != needRemovedTypeMap.end(); ++it) 
		{
			it->second->onCancel();
		}

		// 2. add new exstatus
		for(int i = 0; i < newExStatusList->size(); i++)
		{
			ExtStatusInfo statusInfo = newExStatusList->Get(i);
			//CCLOG("actor %lld, new exstatus: %d", target->getRoleId(), statusInfo.type());

			if(statusInfo.type() == ExStatusType::dead
				|| statusInfo.duration() == statusInfo.remaintime()   // new status, call its onAdd() 
				|| !target->hasExStatus(statusInfo.type()) )   // target has not the status, add it
			{
				ExStatus* newStatus = ExStatusFactory::getInstance()->getExStatus(statusInfo.type(), statusInfo.duration(), statusInfo.remaintime());
				newStatus->setFighter(target);
				newStatus->onAdd(&statusInfo);
				delete newStatus;
			}

			//add flower three effect, type = 37 38 39 ,flower effect is buff
			if (statusInfo.type() == 37 || statusInfo.type() == 38 || statusInfo.type() ==39)
			{
				ActorUtils::addFlowerParticle(target);
			}
		}

		// at last, refresh exstatus
		target->removeAllExStatus();
		// add new exstatus
		for(int i = 0; i < newExStatusList->size(); i++)
		{
			ExtStatusInfo statusInfo = newExStatusList->Get(i);

			// dead status is a special one, it will not be insert into exstatus vector
			if(statusInfo.type() == ExStatusType::dead)
				continue;

			ExStatus* newStatus = ExStatusFactory::getInstance()->getExStatus(statusInfo.type(), statusInfo.duration(), statusInfo.remaintime());
			newStatus->setFighter(target);
			newStatus->CopyFrom(statusInfo);
			target->getExStatusVector().push_back(newStatus);
		}
	}
	else
	{
		// no exstatus
		// first, cancel all old exstatus
		std::vector<ExStatus*>& oldStatus = target->getExStatusVector();
		for (std::vector<ExStatus*>::iterator it = oldStatus.begin(); it != oldStatus.end(); ++it) 
		{
			ExStatus* status = (*it);
			status->onCancel();
		}

		target->removeAllExStatus();
	}

	MainScene * mainscene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if(mainscene)
		mainscene->ReloadBuffData();
}

void ProtocolHelper::updateFighterHPMP(BaseFighter* target, int newHP, int newMP)
{
	// 设置lastHp
	if (0 == target->getActiveRole()->lasthp())
	{
		target->getActiveRole()->set_lasthp(target->getActiveRole()->hp());
	}
	else if (target->getActiveRole()->hp() != newHP)
	{
		if (newHP < target->getActiveRole()->hp())
		{
			target->getActiveRole()->set_lasthp(target->getActiveRole()->hp());
		}
		else
		{
			target->getActiveRole()->set_lasthp(newHP);
		}
	}

	// apply new hp and mp, modifiy the actor's property
	target->getActiveRole()->set_hp(newHP);
	target->getActiveRole()->set_mp(newMP);

	MainScene * mainScene = GameView::getInstance()->getMainUIScene();
	if(mainScene == NULL)
		return;

	// refresh UI
	mainScene->ReloadTarget(target);

	// refresh general's head on mainScene
	//change by yangjun 2014.9.26
// 	General * general = dynamic_cast<General*>(target);
// 	if (general != NULL)
// 		mainScene->RefreshGeneralInfo(general);

	//add by yangjun 2014.9.30
 	General * general = dynamic_cast<General*>(target);
 	if (general != NULL)
	{
		GeneralsInfoUI * generalsUI = (GeneralsInfoUI*)mainScene->getGeneralInfoUI();
		if (generalsUI)
		{
			generalsUI->RefreshGeneralHP(general);
		}
	}

	TargetInfoMini* pBloodBar = (TargetInfoMini*)target->getChildByTag(GameActor::kTagBloodBar);
	if(pBloodBar != NULL)
		pBloodBar->ReloadTargetData(target);

	/*
	//team self 
	if (GameView::getInstance()->isOwn(target->getRoleId()))
	{
		long long selfId = GameView::getInstance()->myplayer->getRoleId();
		for ( int i=0;i<GameView::getInstance()->teamMemberVector.size();i++)
		{
			if (GameView::getInstance()->teamMemberVector.at(i)->roleid() == selfId)
			{
				GameView::getInstance()->teamMemberVector.at(i)->set_hp(newHP);
				GameView::getInstance()->teamMemberVector.at(i)->set_mp(newMP);

				MissionAndTeam * mainSceneTeam_ =(MissionAndTeam *)mainScene->getChildByTag(kTagMissionAndTeam);
				if (mainSceneTeam_ != NULL  && GameView::getInstance()->teamMemberVector.size()>0)
				{
					mainSceneTeam_->teamTableView->updateCellAtIndex(i);
				}
			}
		}
	}
	*/

	//reload offlineArena generals info
	if(GameView::getInstance()->getMapInfo()->maptype() == com::future::threekingdoms::server::transport::protocol::coliseum)
	{
		MissionAndTeam * missionAndTeam = (MissionAndTeam*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMissionAndTeam);
		if (missionAndTeam)
		{
			//实时更新竞技场中武将信息
			bool isChangedMine = false;
			for (int i = 0;i < missionAndTeam->generalsInArena_mine.size();++i)
			{
				if (target->getRoleId() == missionAndTeam->generalsInArena_mine.at(i)->generalId)
				{
					missionAndTeam->generalsInArena_mine.at(i)->cur_hp = target->getActiveRole()->hp();
					missionAndTeam->generalsInArena_mine.at(i)->max_hp = target->getActiveRole()->maxhp();
					isChangedMine = true;
				}
			}
			if (isChangedMine)
			{
				if (missionAndTeam->getLeftOffLineArenaTableView())
				{
					missionAndTeam->getLeftOffLineArenaTableView()->reloadData();
				}
			}

			bool isChangedOther = false;
			for (int i = 0;i < missionAndTeam->generalsInArena_other.size();++i)
			{
				if (target->getRoleId() == missionAndTeam->generalsInArena_other.at(i)->generalId)
				{
					missionAndTeam->generalsInArena_other.at(i)->cur_hp = target->getActiveRole()->hp();
					missionAndTeam->generalsInArena_other.at(i)->max_hp = target->getActiveRole()->maxhp();
					isChangedOther = true;
				}
			}
			if (isChangedOther)
			{
				if (missionAndTeam->getRightOffLineArenaTableView())
				{
					missionAndTeam->getRightOffLineArenaTableView()->reloadData();
				}
			}

			//实时更新竞技场中角色信息
			if (GameView::getInstance()->isOwn(target->getRoleId())) 
			{
				CCTableView * temp = (CCTableView*)missionAndTeam->getLeftOffLineArenaTableView();
				if (temp)
					temp->updateCellAtIndex(0);
			}
			else
			{
				OtherPlayer * temp = dynamic_cast<OtherPlayer*>(target);
				if(temp)
				{
					if (target->getRoleId() == missionAndTeam->p_enemyInfoInArena->generalId)
					{
						missionAndTeam->p_enemyInfoInArena->level = target->getActiveRole()->level();
						missionAndTeam->p_enemyInfoInArena->cur_hp = target->getActiveRole()->hp();
						missionAndTeam->p_enemyInfoInArena->max_hp = target->getActiveRole()->maxhp();

						CCTableView * offLineArenaTableView_right = (CCTableView*)missionAndTeam->getRightOffLineArenaTableView();
						if (offLineArenaTableView_right)
							offLineArenaTableView_right->updateCellAtIndex(0);
					}
				}
			}
		}
	}
}