#include "CCBinaryFileReader.h"

/* Utility methods. */
union int_float_bits {
     int int_bits;
     float float_bits; 
};  
float intBitsToFloat(int x) 
{     
    union int_float_bits bits;     
    bits.int_bits = x;     
    return bits.float_bits; 
} 

CCBinaryFileReader::CCBinaryFileReader(const char *binFile)
: mCurrentByte(0)
, mCurrentBit(0)
, mBytes(NULL)
{
	unsigned long size = 0;
	this->mBytes = CCFileUtils::sharedFileUtils()->getFileData(binFile, "rb", &size);
}

CCBinaryFileReader::~CCBinaryFileReader(void)
{
	CC_SAFE_DELETE_ARRAY(this->mBytes);
}

unsigned char CCBinaryFileReader::readByte() {
    unsigned char byte = this->mBytes[this->mCurrentByte];
    this->mCurrentByte++;
    return byte;
}

bool CCBinaryFileReader::readBool() {
    return 0 == this->readByte() ? false : true;
}

short CCBinaryFileReader::readShort() {
    int b0 = this->readByte();
    int b1 = this->readByte();

    short num = b0 << 8 | b1;
	return num;
}

int CCBinaryFileReader::readInt(bool pSigned) {
    //int numBits = 0;
    //while(!this->getBit()) {
    //    numBits++;
    //}
    //
    //long long current = 0;
    //for(int a = numBits - 1; a >= 0; a--) {
    //    if(this->getBit()) {
    //        current |= 1LL << a;
    //    }
    //}
    //current |= 1LL << numBits;
    //
    //int num;
    //if(pSigned) {
    //    int s = current % 2;
    //    if(s) {
    //        num = (int)(current / 2);
    //    } else {
    //        num = (int)(-current / 2);
    //    }
    //} else {
    //    num = current - 1;
    //}
    //
    //this->alignBits();

	int b0 = this->readByte();
    int b1 = this->readByte();
	int b2 = this->readByte();
    int b3 = this->readByte();

    int num = b0 << 24 | b1 << 16 | b2 << 8 | b3;
    
    return num;
}

float CCBinaryFileReader::readFloat() {
    /* using a memcpy since the compiler isn't
     * doing the float ptr math correctly on device.
     * TODO still applies in C++ ? */
 //   float * pF = (float*)(this->mBytes + this->mCurrentByte);
 //   float f = 0;
 //   memcpy(&f, pF, sizeof(float));
 //   this->mCurrentByte += 4;

	//return f;

	int b0 = this->readByte();
    int b1 = this->readByte();
	int b2 = this->readByte();
    int b3 = this->readByte();

    int num = b0 << 24 | b1 << 16 | b2 << 8 | b3;
	float f = intBitsToFloat(num);
	return f;
}

CCString * CCBinaryFileReader::readString() {
    int b0 = this->readByte();
    int b1 = this->readByte();

    int numBytes = b0 << 8 | b1;

    const unsigned char * src = (const unsigned char *) (this->mBytes + this->mCurrentByte);
    CCString * string = CCString::createWithData(src, (unsigned long)numBytes);

    this->mCurrentByte += numBytes;

	return string;
}

bool CCBinaryFileReader::getBit() {
    bool bit;
    unsigned char byte = *(this->mBytes + this->mCurrentByte);
    if(byte & (1 << this->mCurrentBit)) {
        bit = true;
    } else {
        bit = false;
    }
            
    this->mCurrentBit++;

    if(this->mCurrentBit >= 8) {
        this->mCurrentBit = 0;
        this->mCurrentByte++;
    }
    
    return bit;
}

void CCBinaryFileReader::alignBits() {
    if(this->mCurrentBit) {
        this->mCurrentBit = 0;
        this->mCurrentByte++;
    }
}