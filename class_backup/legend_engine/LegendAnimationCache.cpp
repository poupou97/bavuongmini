
#include "LegendAnimationCache.h"

#include "LegendAAnimation.h"
#include "CCLegendAnimation.h"

// CCLegendAnimationCache - Alloc, Init & Dealloc
static LegendAAnimationCache *g_sharedLegendAAnimationCache = NULL;

LegendAAnimationCache * LegendAAnimationCache::sharedLegendAnimationCache()
{
    if (!g_sharedLegendAAnimationCache)
    {
        g_sharedLegendAAnimationCache = new LegendAAnimationCache();
    }
    return g_sharedLegendAAnimationCache;
}

LegendAAnimationCache::LegendAAnimationCache()
{
    CCAssert(g_sharedLegendAAnimationCache == NULL, "Attempted to allocate a second instance of a singleton.");
}

LegendAAnimationCache::~LegendAAnimationCache()
{
	removeAllAnimations();
}

void LegendAAnimationCache::purgeSharedAnimationCache()
{
    CC_SAFE_DELETE(g_sharedLegendAAnimationCache);
}

AAnimation * LegendAAnimationCache::addAAnimation(const char * path)
{
    CCAssert(path != NULL, "CCLegendAnimationCache: path MUST not be NULL");

    AAnimation * pAnimation = m_pAAnimations[path];
    if (! pAnimation) 
    {
		pAnimation = new AAnimation();
		pAnimation->loadAnimationData(path);

		m_pAAnimations[path] = pAnimation;
    }

    return pAnimation;
}

void LegendAAnimationCache::removeAllAnimations()
{
    for (std::map<std::string, AAnimation*>::iterator it = m_pAAnimations.begin(); it != m_pAAnimations.end(); ++it) 
	{
		CC_SAFE_RELEASE(it->second);
	}   
	m_pAAnimations.clear();
}

///////////////////////////////////////////////////////////////////

// CCLegendAnimationCache - Alloc, Init & Dealloc
static CCLegendAnimationCache *g_sharedCClegendAnimationCache = NULL;

CCLegendAnimationCache * CCLegendAnimationCache::sharedCache()
{
    if (!g_sharedCClegendAnimationCache)
    {
        g_sharedCClegendAnimationCache = new CCLegendAnimationCache();
    }
    return g_sharedCClegendAnimationCache;
}

CCLegendAnimationCache::CCLegendAnimationCache()
{
    CCAssert(g_sharedCClegendAnimationCache == NULL, "Attempted to allocate a second instance of a singleton.");
}

CCLegendAnimationCache::~CCLegendAnimationCache()
{
	removeAllCache();
}

void CCLegendAnimationCache::purgeSharedCache()
{
    CC_SAFE_DELETE(g_sharedCClegendAnimationCache);
}

CCLegendAnimation* CCLegendAnimationCache::addCCLegendAnimation(const char * path)
{
    CCAssert(path != NULL, "CCLegendAnimationCache: path MUST not be NULL");

	CCLegendAnimation* pAnimation = NULL;
	std::vector<CCLegendAnimation*>& animVector = m_CCLegendAnimationPool[path];
	for(unsigned int i = 0; i < animVector.size(); i++)
	{
		CCLegendAnimation* tmp = animVector.at(i);
		// 若引用计数为1，则为可用
		if(tmp->isSingleReference())
		{
			pAnimation = tmp;
			pAnimation->reset();   // 必须重置动画，确保后续使用的正确
			break;
		}
	}

    if (! pAnimation) 
    {
		pAnimation = new CCLegendAnimation();
		pAnimation->initWithAnmFilename(path);
		pAnimation->setVisible(false);

		animVector.push_back(pAnimation);
    }

    return pAnimation;
}

void CCLegendAnimationCache::removeAllCache()
{
	for (std::map<std::string, std::vector<CCLegendAnimation*> >::iterator it = m_CCLegendAnimationPool.begin(); it != m_CCLegendAnimationPool.end(); ++it) 
	{
		std::vector<CCLegendAnimation*>& animVector = it->second;
		for(unsigned int i = 0; i < animVector.size(); i++)
		{
			CCLegendAnimation* tmp = animVector.at(i);
			CC_SAFE_RELEASE(tmp);
		}
		animVector.clear();
	}
	m_CCLegendAnimationPool.clear();
}