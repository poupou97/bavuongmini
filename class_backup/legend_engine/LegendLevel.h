#ifndef __LEGEND_LEGENDLEVEL_H__
#define __LEGEND_LEGENDLEVEL_H__

#include "cocos2d.h"
#include "CCBinaryFileReader.h"

USING_NS_CC;
using namespace std;

/** 
 * class TiledMap, this map indicate the background which is organized by tiles
 * 1 TileMap maybe contains some TiledMapLayer(s)
 * one TiledMap object is the data from *.map (Legend TiledMap file)
 * @author zhaogang
 * @version 0.1.0
*/

class ActorClass
{
public:
	int classId;
	std::string aniName;
	int actIdx;

public:
    ActorClass();
    virtual ~ActorClass();

	void load(CCBinaryFileReader* reader);
};

/**
 * class GameMetaData
 * 对应world文件，主要记录了游戏用到的所有的actorclass信息
 */
class GameMetaData {
public:
	static void load(const char* worldFile);

	static int getActorClassCount();
	static ActorClass* getActorClass(int id);

private:
	static int s_ActorClassCount;
	static ActorClass** s_actorClasses;
};

class LevelActorInfo
{
public:
	LevelActorInfo();
	virtual ~LevelActorInfo();

	virtual void load(CCBinaryFileReader* reader) {};
};

class DecorationActorInfo : public LevelActorInfo
{
public:
	// 场景唯一id
	long instanceId;
	// 动画编号
	int actorClassId;
	// x坐标
	int posX;
	// y坐标
	int posY;
	// 动作编号
	int animActionID;
	// FLAG
	unsigned char flag;		
	
	int paramLength;
	short* paramData;

public:
	DecorationActorInfo();
	virtual ~DecorationActorInfo();

	virtual void load(CCBinaryFileReader* reader);
};

class NPCActorInfo : public LevelActorInfo
{
public:
	// 场景唯一id
	int instanceId;
	// 动画xml的上一级文件夹名
	std::string npc_name;
	// x行
	short rol;
	// y列
	short col;
	// NPC动作编号
	char actionID;
	// flag属性
	char flag;
	// 模板ID
	long templateId;

public:
	NPCActorInfo();
	virtual ~NPCActorInfo();

	virtual void load(CCBinaryFileReader* reader);
};

class PickingActorInfo : public LevelActorInfo
{
public:
	// 场景唯一id
	int instanceId;
	// 动画xml的上一级文件夹名
	std::string actor_name;
	// x行
	short rol;
	// y列
	short col;
	// NPC动作编号
	char actionID;
	// flag属性
	char flag;
	// 模板ID
	int templateId;

public:
	PickingActorInfo();
	virtual ~PickingActorInfo();

	virtual void load(CCBinaryFileReader* reader);
};

class MonsterActorInfo : public LevelActorInfo
{
public:
	// 场景唯一id
	int instanceId;
	// 动画xml的上一级文件夹名
	std::string monster_name;
	// x行
	short rol;
	// y列
	short col;
	// NPC动作编号
	char actionID;
	// flag属性
	char flag;
	// 模板ID
	int templateId;

public:
	MonsterActorInfo();
	virtual ~MonsterActorInfo();

	virtual void load(CCBinaryFileReader* reader);
};

class LegendLevel
{
public:
	std::string tiledmapName;

	int decorationActorsNumber;
	DecorationActorInfo** decorationActorsInfo;

	int npcActorsNumber;
	NPCActorInfo** npcActorsInfo;

	// 采集物
	int pickingActorsNumber;
	PickingActorInfo** pickingActorsInfo;

	int monsterActorsNumber;
	MonsterActorInfo** monsterActorsInfo;

public:
	LegendLevel();
	virtual ~LegendLevel();
	
	void load(const char* levelFile);
};

#endif