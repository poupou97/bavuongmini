/*
 *  class GameWorld
 *  this class is from "level/configure" file
 *  it's a singleton instance, include all NPCs, and all maps in this game world
 *  Created by Zhao Gang on 09-30-2013.
 *  Copyright 2013 Perfect Future. All rights reserved.
 */

#ifndef _GAMEUTILS_GAMEWORLD_H_
#define _GAMEUTILS_GAMEWORLD_H_

#include "cocos2d.h"
using namespace cocos2d;

class MapInfo;
class CNpcInfo;
class CPickingInfo;
class GraphSTDijkstra;

class GameWorld {
public :
	static void loadWorldConfigure();
	static std::vector<std::string>* searchLevelPath(std::string from, std::string to);

	static std::map<std::string, int> doorIndex;
	static std::map<int, std::string> indexdoor;
	static std::map<std::string, std::vector<std::string>*> maps;

	static std::map<long long, CNpcInfo*> NpcInfos;
	static std::map<std::string, MapInfo*> MapInfos;
	static std::map<int, CPickingInfo*> PickingInfos;

	// 所有场景的邻接图表示，用于跨地图的寻路
	static GraphSTDijkstra* sLevelGraph;
};

#endif