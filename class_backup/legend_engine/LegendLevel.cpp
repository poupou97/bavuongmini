#include "LegendLevel.h"

ActorClass::ActorClass()
{

}

ActorClass::~ActorClass()
{

}

void ActorClass::load(CCBinaryFileReader* reader)
{
	CCString * name = reader->readString();
	aniName = name->getCString();

	actIdx = reader->readByte();
}

////////////////////////////////////////////////////////////////////////////////////////

//定义并初始化静态数据成
int GameMetaData::s_ActorClassCount = 0;
ActorClass** GameMetaData::s_actorClasses = NULL;

void GameMetaData::load(const char* worldFile)
{
	CCBinaryFileReader* reader = new CCBinaryFileReader(worldFile);

	s_ActorClassCount = reader->readShort();
	s_actorClasses = new ActorClass*[s_ActorClassCount];
	for(int i = 0; i < s_ActorClassCount; i++)
	{
		s_actorClasses[i] = new ActorClass();
		s_actorClasses[i]->load(reader);
	}

	CC_SAFE_DELETE(reader);

	//CCLOG("world actor class count: %d", GameMetaData::getActorClassCount());
}

ActorClass* GameMetaData::getActorClass(int id)
{
	if(id < 0)
	{
		return NULL;
	}

	return s_actorClasses[id];
}

int GameMetaData::getActorClassCount()
{
	return s_ActorClassCount;
}

////////////////////////////////////////////////////////////////////////////////////////

LevelActorInfo::LevelActorInfo()
{
}

LevelActorInfo::~LevelActorInfo()
{
}

////////////////////////////////////////////////////////////////////////////////////////

DecorationActorInfo::DecorationActorInfo()
{
}

DecorationActorInfo::~DecorationActorInfo()
{
	CC_SAFE_DELETE_ARRAY(paramData);
}

void DecorationActorInfo::load(CCBinaryFileReader* reader) {
	// 场景唯一id
	instanceId = reader->readInt(true);
	// actorclass编号
	actorClassId = reader->readShort();
	// x坐标
	posX = reader->readShort();
	// y坐标
	posY = reader->readShort();
	// 动作编号
	animActionID = reader->readByte();
	// FLAG属性
	flag = reader->readByte();		
	
	//参数
	paramLength = reader->readByte();
	this->paramData = new short[paramLength];
	for (int i = 0; i < paramLength; i++) {
		this->paramData[i] = reader->readShort();
	}
}

////////////////////////////////////////////////////////////////////////////////////////

NPCActorInfo::NPCActorInfo()
{
}

NPCActorInfo::~NPCActorInfo()
{
}

void NPCActorInfo::load(CCBinaryFileReader* reader) {
	// 场景唯一id
	instanceId = reader->readInt(true);
	// 动画xml的上一级文件夹名
	npc_name = reader->readString()->getCString();
	// x行
	rol = reader->readShort();
	// y列
	col = reader->readShort();
	// NPC动作编号
	actionID = reader->readByte();
	// flag属性
	flag = reader->readByte();
	// 模板ID
	templateId = reader->readInt(true);
}

////////////////////////////////////////////////////////////////////////////////////////

PickingActorInfo::PickingActorInfo()
{
}

PickingActorInfo::~PickingActorInfo()
{
}

void PickingActorInfo::load(CCBinaryFileReader* reader) {
	// 场景唯一id
	instanceId = reader->readInt(true);
	// 动画xml的上一级文件夹名
	actor_name = reader->readString()->getCString();
	// x行
	rol = reader->readInt(true);
	// y列
	col = reader->readInt(true);
	// NPC动作编号
	actionID = reader->readInt(true);
	// flag属性
	flag = reader->readByte();
	// 模板ID
	templateId = reader->readInt(true);
}

////////////////////////////////////////////////////////////////////////////////////////

MonsterActorInfo::MonsterActorInfo()
{
}

MonsterActorInfo::~MonsterActorInfo()
{
}

void MonsterActorInfo::load(CCBinaryFileReader* reader) {
	// 场景唯一id
	instanceId = reader->readInt(true);
	// 动画xml的上一级文件夹名
	monster_name = reader->readString()->getCString();
	// x行
	rol = reader->readInt(true);
	// y列
	col = reader->readInt(true);
	// NPC动作编号
	actionID = reader->readInt(true);
	// flag属性
	flag = reader->readByte();
	// 模板ID
	templateId = reader->readInt(true);
}

////////////////////////////////////////////////////////////////////////////////////////

LegendLevel::LegendLevel()
: decorationActorsNumber(0)
, decorationActorsInfo(NULL)
, npcActorsNumber(0)
, npcActorsInfo(NULL)
, pickingActorsNumber(0)
, pickingActorsInfo(NULL)
, monsterActorsNumber(0)
, monsterActorsInfo(NULL)
{
}

LegendLevel::~LegendLevel()
{
	// delete array
	for(int i = 0; i < decorationActorsNumber; i++)   
		CC_SAFE_DELETE(decorationActorsInfo[i]);  
	CC_SAFE_DELETE_ARRAY(decorationActorsInfo);

	// delete array
	for(int i = 0; i < npcActorsNumber; i++)   
		CC_SAFE_DELETE(npcActorsInfo[i]);  
	CC_SAFE_DELETE_ARRAY(npcActorsInfo);

	// delete array
	for(int i = 0; i < pickingActorsNumber; i++)   
		CC_SAFE_DELETE(pickingActorsInfo[i]);  
	CC_SAFE_DELETE_ARRAY(pickingActorsInfo);

	// delete array
	for(int i = 0; i < monsterActorsNumber; i++)   
		CC_SAFE_DELETE(monsterActorsInfo[i]);  
	CC_SAFE_DELETE_ARRAY(monsterActorsInfo);
}

void LegendLevel::load(const char* levelFile)
{
	CCBinaryFileReader* reader = new CCBinaryFileReader(levelFile);

	// load map name
	CCString * mapName = reader->readString();
	this->tiledmapName = mapName->getCString();

	// decoration actor
	decorationActorsNumber = reader->readShort();
	decorationActorsInfo = new DecorationActorInfo*[decorationActorsNumber];
	for (int i = 0; i < decorationActorsNumber; i++) {
		decorationActorsInfo[i] = new DecorationActorInfo();
		decorationActorsInfo[i]->load(reader);
	}

	// load tile_paint_mask info
	// this info indicates which the tiles will be painted or not
	int tilesCnt = reader->readShort();
	if(tilesCnt > 0)
	{
		CCAssert(false, "it's not implemented, should not run to here.");

		int* tile_paint_mask = new int[tilesCnt];
		for (int i = 0; i < tilesCnt; i++) {
			tile_paint_mask[i] = reader->readInt(true);
		}
	}

	// NPC
	npcActorsNumber = reader->readInt(true);
	npcActorsInfo = new NPCActorInfo*[npcActorsNumber];
	for (int i = 0; i < npcActorsNumber; i++) 
	{
		npcActorsInfo[i] = new NPCActorInfo();
		npcActorsInfo[i]->load(reader);
	}

	// Picking Actors
	pickingActorsNumber = reader->readInt(true);
	pickingActorsInfo = new PickingActorInfo*[pickingActorsNumber];
	for (int i = 0; i < pickingActorsNumber; i++) 
	{
		pickingActorsInfo[i] = new PickingActorInfo();
		pickingActorsInfo[i]->load(reader);
	}

	// Monster
	monsterActorsNumber = reader->readInt(true);
	monsterActorsInfo = new MonsterActorInfo*[monsterActorsNumber];
	for (int i = 0; i < monsterActorsNumber; i++) 
	{
		monsterActorsInfo[i] = new MonsterActorInfo();
		monsterActorsInfo[i]->load(reader);
	}

	CC_SAFE_DELETE(reader);
}