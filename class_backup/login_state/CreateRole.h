#ifndef _GAMESTATE_CREATEROLE_STATE_H_
#define _GAMESTATE_CREATEROLE_STATE_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../GameStateBasic.h"
#include "../messageclient/ClientNetEngine.h"
#include "../messageclient/protobuf/LoginMessage.pb.h"
#include "../messageclient/reqsender/ReqSenderProtocol.h"
#include "Login.h"
#include "../gamescene_state/role/BasePlayer.h"
#include "SpriteBlur.h"

USING_NS_CC;

/**
 create a role to the game server
 */
class CreateRoleLayer : public CCLayer, public ClientNetEngine::Delegate
{
public:
    CreateRoleLayer();
	virtual ~CreateRoleLayer();

	virtual void onSocketOpen(ClientNetEngine* socketEngine);
    virtual void onSocketClose(ClientNetEngine* socketEngine);
    virtual void onSocketError(ClientNetEngine* ssocketEngines, const ClientNetEngine::ErrorCode& error);

	virtual void update(float dt);

	virtual void ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent);
	virtual void ccTouchesBegan(CCSet *pTouches, CCEvent *pEvent);
	virtual void ccTouchesMoved(CCSet *pTouches, CCEvent *pEvent);

	inline int getSelectedRoleId() { return mSelectedId; }

	void onButtonGetClicked(cocos2d::CCObject *sender);
	
	void onHttpRequestCompleted(cocos2d::extension::CCHttpClient *sender, cocos2d::extension::CCHttpResponse *response);

	void onHttpRequestCompletedCreateRole(cocos2d::extension::CCHttpClient *sender, cocos2d::extension::CCHttpResponse *response);

	void TouchedLiangZhou( CCObject * obj );
	void TouchedYouZhou( CCObject * obj );
	void TouchedYiZhou( CCObject * obj );
	void TouchedJingZhou( CCObject * obj );
	void TouchedYangZhou( CCObject * obj );

	void callBackExit(CCObject * obj);
private:
	void menuSelectRoleCallback(CCObject* pSender);
	void menuSelectCountry(CCObject* pSender);
	void onTouchedRandomName(CCObject* pSender);
	void onClickedArrow(CCObject* pSender);
	void connectGameServer();
	void initProfessionSprite(int minProfession);
	//void initCountrySprite(int minCountry);
	void initProfessionIcon(int profession);
	void updateProfessionIcon(int profession);
	void updateSpriteFront(int profession);
	void onClickIconButton(CCObject *obj);
	void spriteRunAction(float time, int mode);
	void updateCountryButton(int country);
	void onClickSpriteButton(CCObject *senderz, CCControlEvent controlEvent);
	int isTouchedPosInButton(CCPoint touchPos);

	void setSpriteState(float scale, int mode);
	void setSpriteRightState(CCSprite* sprite, float scale, int status);
	void setSpriteLeftState(CCSprite* sprite, float scale, int status);

	void showBlurSprite(SpriteBlur* sprite, int flag);
	void hideBlurSprite();
	char* getBlurSpritePath(int status);
private:
	int m_state;

	//struct timeval m_sStartTime;
	long double m_dStartTime;
	long double m_dEndedTime;

	int mSpriteStatus;

	int mSelectedId;

	int mCurrentServerId;

	RandomNameRsp* m_pRandomNameRsp;

	ServerListRsp* m_pServerListRsp;
	RichTextInputBox * pNameLayer;
	
	CCPoint m_tBeginPos;

	int mCountryFlag;
	int isTouchSlipped;
	int mActionSpriteTag;
	int mZOrder;
	bool isRunActionTwice;

	int m_iTouchedState;
	int m_iNum;

	CCSprite* m_pBlurLeft;
	CCSprite* m_pBlurRight;
protected:
	UILayer *m_CountryLayer;
};

class CreateRoleState : public GameState
{
public:
	~CreateRoleState();

    virtual void runThisState();

	virtual void onSocketOpen(ClientNetEngine* socketEngine);
    virtual void onSocketClose(ClientNetEngine* socketEngine);
    virtual void onSocketError(ClientNetEngine* ssocketEngines, const ClientNetEngine::ErrorCode& error);

};

#endif // _GAMESTATE_LOGIN_STATE_H_
