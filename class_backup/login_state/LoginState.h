#ifndef _GAMESTATE_LOGIN_STATE_H_
#define _GAMESTATE_LOGIN_STATE_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../GameStateBasic.h"
#include "../messageclient/ClientNetEngine.h"
#include "../messageclient/protobuf/LoginMessage.pb.h"
#include "../ui/extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

/**
 login to the game server
 */
class LoginLayer : public UIScene, public CCTableViewDataSource, public CCTableViewDelegate
{
public:
    LoginLayer();
	virtual ~LoginLayer();

	virtual void onSocketOpen(ClientNetEngine* socketEngine);
    virtual void onSocketClose(ClientNetEngine* socketEngine);
    virtual void onSocketError(ClientNetEngine* ssocketEngines, const ClientNetEngine::ErrorCode& error);

	virtual void update(float dt);

	virtual void ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent);

	inline int getSelectedRoleId() { return mSelectedId; };

	void onButtonGetClicked(cocos2d::CCObject *sender);
	void onEnterButtonGetClicked(cocos2d::CCObject *sender);

	void onMenuGetTestClicked();
	void onHttpRequestCompleted(CCHttpClient *sender, CCHttpResponse *response);

	void onRoleListReq();
	void onRoleListRequestCompleted(CCHttpClient *sender, CCHttpResponse *response);
	
	static long  mRoleId;
	static int mCountryMin;
	static int mProfessionMin;

	virtual void tableCellHighlight(CCTableView* table, CCTableViewCell* cell);
    virtual void tableCellUnhighlight(CCTableView* table, CCTableViewCell* cell);
	
	virtual void scrollViewDidScroll(cocos2d::extension::CCScrollView* view) {};
    virtual void scrollViewDidZoom(cocos2d::extension::CCScrollView* view) {}
    virtual void tableCellTouched(cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell);
    virtual cocos2d::CCSize tableCellSizeForIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
    virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
    virtual unsigned int numberOfCellsInTableView(cocos2d::extension::CCTableView *table);
	/*return the level of the role in last server
		if the level is none,return 0*/
	static int getLastServerRoleLevel();
	static void reconnectServer();

	CCTableView *tableView;
	CCTableView* tableViewLast;

	static int s_iLastServerRoleLevel;
private:
	void menuSelectRoleCallback(CCObject* pSender);
	void menuSelectServerCallback(CCObject* pSender);

	void connectGameServer();

	void initLastServer(int idx);
	void onLastServerGetClicked(cocos2d::CCObject *sender);

	void requestServerList();
	void onRequestServerListCompleted(CCHttpClient *sender, CCHttpResponse *response);
	void reloadServerInfo();
private:
	int m_state;

	long double m_dStartTime;
	long double m_dEndedTime;

	int mSelectedId;

	static int mCurrentServerId;

	ServerListRsp* m_pServerListRsp;

	int mTouchedId;
	int mTouchedStatus;
	int lastServerIndex;
	bool isLastSelected;
	bool isTableViewTouched;

	int m_iStatus;
public:
	static int getCurrentServerId(){return mCurrentServerId;};
	
};

class LoginState : public GameState
{
public:
	~LoginState();

    virtual void runThisState();

	virtual void onSocketOpen(ClientNetEngine* socketEngine);
    virtual void onSocketClose(ClientNetEngine* socketEngine);
    virtual void onSocketError(ClientNetEngine* ssocketEngines, const ClientNetEngine::ErrorCode& error);

	static void addAppVersionInfo(CCNode* parent);

};

#endif // _GAMESTATE_LOGIN_STATE_H_
