#include "DefaultServer.h"
#include "LoginState.h"

#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS

#include "../classes/gamescene_state/gamescenestate.h"
#include "../classes/loadscene_state/loadscenestate.h"
#include "../classes/legend_engine/cclegendanimation.h"
#include "../classes/legend_engine/legendlevel.h"
#include "../classes/ui/extensions/ccrichlabel.h"
#include "../classes/ui/extensions/richelement.h"

#include "../classes/utils/gameutils.h"

#include "../classes/messageclient/gamemessageprocessor.h"
#include "../classes/messageclient/clientnetengine.h"

#include "../classes/ui/extensions/richtextinput.h"
#include "../classes/gameview.h"
#include "../classes/messageclient/protobuf/loginmessage.pb.h"
#include "../classes/login_state/login.h"
#include "../classes/login_state/createrole.h"
#include "../classes/ui/generals_ui/generalsui.h"
#include "../classes/utils/staticdatamanager.h"
#include "../classes/gamescene_state/role/baseplayer.h"
#include "../classes/appmacros.h"
#include "../classes/gamescene_state/role/myplayeraiconfig.h"
#include "../classes/gamescene_state/role/myplayer.h"
#include "../classes/gamescene_state/role/myplayerai.h"
#include "../classes/gameuserdefault.h"
#include "../classes/login_state/moutainbackgroundlayer.h"
#include "../classes/ui/registernotice/registernoticeui.h"

#include "share_sdk/shareloginstate.h"

#else

#include "../gamescene_state/GameSceneState.h"
#include "../loadscene_state/LoadSceneState.h"
#include "../legend_engine/CCLegendAnimation.h"
#include "../legend_engine/LegendLevel.h"
#include "../ui/extensions/CCRichLabel.h"
#include "../ui/extensions/RichElement.h"

#include "../utils/GameUtils.h"

#include "../messageclient/GameMessageProcessor.h"
#include "../messageclient/ClientNetEngine.h"

#include "../ui/extensions/RichTextInput.h"
#include "../GameView.h"
#include "../messageclient/protobuf/LoginMessage.pb.h"
#include "Login.h"
#include "CreateRole.h"
#include "../ui/generals_ui/GeneralsUI.h"
#include "../utils/StaticDataManager.h"
#include "../gamescene_state/role/BasePlayer.h"
#include "AppMacros.h"
#include "../gamescene_state/role/MyPlayerAIConfig.h"
#include "../gamescene_state/role/MyPlayer.h"
#include "../gamescene_state/role/MyPlayerAI.h"
#include "GameUserDefault.h"
#include "MoutainBackGroundLayer.h"
#include "../ui/registerNotice/RegisterNoticeui.h"

#endif


//#define THREEKINGDOMS_HOST_INTERNET "192.168.1.138"//"61.149.218.125"//"qianliang2013.vicp.cc"//"192.168.1.117"
//#define THREEKINGDOMS_PORT_INTERNET 8088
//#define THREEKINGDOMS_HOST_WANMEI "124.202.137.33"
//#define THREEKINGDOMS_PORT_WANMEI 8088
#define THREEKINGDOMS_HOST_STUDIO "139.99.49.200"  
#define THREEKINGDOMS_PORT_STUDIO 8188
//#define THREEKINGDOMS_HOST_GBK "192.168.1.118"  
//#define THREEKINGDOMS_PORT_GBK 8088  
//#define THREEKINGDOMS_HOST_ZHAGNGJING "192.168.1.132"  
//#define THREEKINGDOMS_PORT_ZHAGNGJING 8088  
//#define THREEKINGDOMS_HOST_LOCAL "127.0.0.1"  
//#define THREEKINGDOMS_PORT_LOCAL 8088  

#define MAX_LOGIN_ROLE_NUM 24

#define LASTSERVER_TAG 201
#define HIGHLIGHT_FRAME_TAG 202
#define NEWSERVER_TAG 203
#define WAIT_TIME 30*1000

#define TABLEVIEW_LAST_TAG 55
#define TABLEVIEW_TAG 56

#define CLIP_TIME 3.0f
#define DELAY_TIME 15.0f

#define FRAME_TAG 60
#define STATUS_TAG 61

enum {
	kStateStart = 0,
	kStateWait = 1,
	kStateReload = 2,
};

enum {
	kStateRoleList = 0,
	kStateConnectingGameServer,
};

enum {
	kTagPreparingBackGround = 2,
	kTagPreparingTip = 3,
	kTagServerTip = 4,
	kTagRoleListMenu = 16,   // last one !! please add more tag before it
};

int DefaultServerLayer::mCurrentServerId = -1;

DefaultServerLayer::DefaultServerLayer()
: m_state(kStateRoleList)
,isLastSelected(true)
,isTableViewTouched(true)
,m_iStatus(kStateStart)
{
	setTouchEnabled(true);

	m_pServerListRsp = new ServerListRsp();
	Login::getServerListInfo(m_pServerListRsp);

	if(mCurrentServerId < 0)
	{
		int i;
		for(i = 0; i < m_pServerListRsp->serverinfos_size(); i++)
		{
			if(m_pServerListRsp->lastserverid() == m_pServerListRsp->serverinfos(i).serverid())
			{
				lastServerIndex = i;
				break;
			}
		}
		if(i == m_pServerListRsp->serverinfos_size())
		{
			lastServerIndex = 0;
		}
		mCurrentServerId = lastServerIndex;
	}
	initLayer();

	scheduleUpdate();
}

DefaultServerLayer::~DefaultServerLayer()
{

}

void DefaultServerLayer::initLayer()
{
	CCSize s = CCDirector::sharedDirector()->getVisibleSize();
	
	MoutainBackGroundLayer* pBackGround = MoutainBackGroundLayer::create();
	pBackGround->setBackGroundColor(ccc3(255, 255, 255));
	addChild(pBackGround);

	UILayer* layer= UILayer::create();
	addChild(layer, 9999, 255);

	UILayer* m_pUiLayer= UILayer::create();
	m_pUiLayer->ignoreAnchorPointForPosition(false);
	// 锚点在中心，并放置在屏幕中心点
	m_pUiLayer->setAnchorPoint(ccp(0.5f,0.5f));
	m_pUiLayer->setPosition(ccp(s.width/2,s.height/2));  
	m_pUiLayer->setTag(255);
	// 设置基准分辨率
	m_pUiLayer->setContentSize(CCSizeMake(UI_DESIGN_RESOLUTION_WIDTH, UI_DESIGN_RESOLUTION_HEIGHT));
	addChild(m_pUiLayer);
	
// 	//gamelogo
// 	CCSprite* pSpriteLogo = CCSprite::create("res_ui/select_the_sercer/gamelogo.png");
// 	pSpriteLogo->setPosition(ccp(410, 340));
// 	pSpriteLogo->setAnchorPoint(ccp(0.5f, 0.5f));
// 	pSpriteLogo->setScale(1.0f);
// 	pSpriteLogo->setOpacity(255);
// 	m_pUiLayer->addChild(pSpriteLogo, 1);
// 	
// 	//yanjing
// 	CCSprite* pSpriteYanjing = CCSprite::create("images/yanjing.png");
// 	pSpriteYanjing->setPosition(ccp(pSpriteLogo->getContentSize().width/2+5, pSpriteLogo->getContentSize().height - 38));
// 	pSpriteYanjing->setAnchorPoint(ccp(0.5f, 0.5f));
// 	pSpriteYanjing->setScaleX(2.0f);
// 	pSpriteYanjing->setScaleY(1.16f);
// 	pSpriteLogo->addChild(pSpriteYanjing,1);
// 
// 	CCFadeTo*  action1 = CCFadeTo::create(1.2f,30);
// 	CCDelayTime * delay_1 = CCDelayTime::create(1.5f);
// 	CCFadeTo*  action2 = CCFadeTo::create(1.2f,255);
// 	CCDelayTime * delay_2 = CCDelayTime::create(0.6f);
// 	CCRepeatForever * repeapAction = CCRepeatForever::create(CCSequence::create(action1,delay_1,action2,delay_2,NULL));
// 	pSpriteYanjing->runAction(repeapAction);
	
// 	CCClippingNode* clip = CCClippingNode::create();//创建裁剪节点
// 	CCSprite* gameTitle = CCSprite::create("res_ui/select_the_sercer/gamelogo_text_mask.png");
// 	clip->setStencil(gameTitle);//设置裁剪模板
// 	clip->setAlphaThreshold(0.0f);//设置透明度阈值
// 	//clip->setInverted(true);
// 	clip->setContentSize(CCSize(gameTitle->getContentSize().width,gameTitle->getContentSize().height));//设置裁剪节点大小    
// 	CCSize clipSize = clip->getContentSize();//获取裁剪节点大小
// 	clip->setPosition(ccp(410,340));//设置裁剪节点位置
// 

	//CCSprite* gameTitle_show = CCSprite::create("res_ui/select_the_sercer/gamelogo.png");//创建要显示的对象
// 	CCSprite* spark = CCSprite::create("images/guangtiao.png");//创建闪亮精灵
// 	//clip->addChild(gameTitle_show,2);//把要显示的内容放在裁剪节点中，其实可以直接clip->addChild(gameTitle,1);此处为了说明更改显示内容
// 	spark->setPosition(ccp(-400, 0));//设置闪亮精灵位置
// 	clip->addChild(spark,3);//添加闪亮精灵到裁剪节点
// 	m_pUiLayer->addChild(clip,1);//添加裁剪节点
// 
// 	CCMoveTo* moveAction = CCMoveTo::create(CLIP_TIME, ccp(clipSize.width, 0));//创建精灵节点的动作
// 	CCMoveTo* moveBack = CCMoveTo::create(0, ccp(-clipSize.width, 0));
// 	CCDelayTime* delayTime = CCDelayTime::create(DELAY_TIME);
// 	CCSequence* seq = CCSequence::create(moveAction, delayTime, moveBack, NULL);
// 	CCRepeatForever* repreatAction = CCRepeatForever::create(seq);
// 	spark->runAction(repreatAction);//精灵节点重复执行动作

	LoginState::addAppVersionInfo(this);

	//UIImageView * ImageView_biankuang= (UIImageView *)UIHelper::seekWidgetByName(serverListPanel,"ImageView_biankuang1");
	
	cocos2d::extension::UIButton* frame = cocos2d::extension::UIButton::create();
	frame->loadTextures("res_ui/select_the_sercer/namedi.png", "res_ui/select_the_sercer/namedi.png", "");
	frame->setAnchorPoint(ccp(0.5f, 0.5f));
	frame->setScale9Enabled(true);
	frame->setCapInsets(CCRectMake(14,14,1,1));
	//sprite->setPreferredSize(CCSizeMake(370,55));
	frame->setSize(CCSizeMake(321,50));
	frame->setPosition(ccp(400, 160));
	frame->setTouchEnable(true);
	frame->setPressedActionEnabled(true, 0.9f, 1.1f);
	frame->setTag(FRAME_TAG);
	frame->addReleaseEvent(this, coco_releaseselector(DefaultServerLayer::onLastServerGetClicked));
	m_pUiLayer->addWidget(frame);
	
	cocos2d::extension::UIImageView * serverStatus = cocos2d::extension::UIImageView::create();
	switch(m_pServerListRsp->serverinfos(mCurrentServerId).status())
	{
		case 1:
			serverStatus->setTexture("res_ui/select_the_sercer/green_new.png");
			break;
		case 2:
			serverStatus->setTexture("res_ui/select_the_sercer/yellow_new.png");
			break;
		case 3:
			serverStatus->setTexture("res_ui/select_the_sercer/red_new.png");
			break;
		case 4:
			serverStatus->setTexture("res_ui/select_the_sercer/gray_new.png");
			break;
		case 5:
			serverStatus->setTexture("res_ui/select_the_sercer/gray_new.png");
			break;
		default:
			break;
	}
	serverStatus->setVisible(true);
	serverStatus->setAnchorPoint(ccp(0.5f,0.5f));
	serverStatus->setPosition(ccp(-130, 0));
	serverStatus->setScale(0.85f);
	serverStatus->setTag(STATUS_TAG);
	frame->addChild(serverStatus);

	cocos2d::extension::UILabel * serverName = cocos2d::extension::UILabel::create();
	serverName->setStrokeEnabled(true);
	serverName->setText(m_pServerListRsp->serverinfos(mCurrentServerId).name().c_str());
	serverName->setAnchorPoint(ccp(0,0.5f));
	serverName->setFontName(APP_FONT_NAME);
	serverName->setFontSize(18);
	serverName->setPosition(ccp(-100,0));
	serverName->setColor(ccc3(0, 255, 6));
	frame->addChild(serverName);
	
	cocos2d::extension::UILabel * serverChange = cocos2d::extension::UILabel::create();
	serverChange->setStrokeEnabled(true);
	serverChange->setText(StringDataManager::getString("server_change"));
	serverChange->setAnchorPoint(ccp(0.5f,0.5f));
	serverChange->setFontName(APP_FONT_NAME);
	serverChange->setFontSize(18);
	serverChange->setPosition(ccp(100,0));
	serverChange->setColor(ccc3(255, 255, 255));
	frame->addChild(serverChange);
	
	cocos2d::extension::UIButton *change = cocos2d::extension::UIButton::create();
	change->setAnchorPoint(ccp(0.5f, 0.5f));
	change->setTouchEnable(true);
	change->loadTextures("res_ui/tab_4_off.png", "res_ui/tab_4_off.png", "");
	change->setPosition(ccp(s.width-80, s.height-45));
	change->addReleaseEvent(this, coco_releaseselector(DefaultServerLayer::onChangeGetClicked));
	change->setPressedActionEnabled(true);
	layer->addWidget(change);
	
	cocos2d::extension::UILabel * change_label = cocos2d::extension::UILabel::create();
	change_label->setStrokeEnabled(true);
	change_label->setText(StringDataManager::getString("account_change"));
	change_label->setAnchorPoint(ccp(0.5f,0.5f));
	change_label->setFontName(APP_FONT_NAME);
	change_label->setFontSize(20);
	//serverChange->setPosition(ccp(100,0));
	//serverChange->setColor(ccc3(255, 255, 255));
	change->addChild(change_label);
	
	cocos2d::extension::UIButton *enter = cocos2d::extension::UIButton::create();
	enter->setAnchorPoint(ccp(0.5f, 0.5f));
	enter->setTouchEnable(true);
	enter->loadTextures("res_ui/creating_a_role/button_di.png", "res_ui/creating_a_role/button_di.png", "");
	enter->setScale9Enabled(true);
	enter->setCapInsets(CCRectMake(50,30,1,1));
	enter->setSize(CCSizeMake(150, 56));
	enter->setPosition(ccp(400, 80));
	enter->addReleaseEvent(this, coco_releaseselector(DefaultServerLayer::onEnterGetClicked));
	enter->setPressedActionEnabled(true);
	m_pUiLayer->addWidget(enter);
	
	cocos2d::extension::UIImageView * enterText = cocos2d::extension::UIImageView::create();
	enterText->setTexture("res_ui/select_the_sercer/enter.png");
	enterText->setVisible(true);
	enter->addChild(enterText);

	//add registerNotice
	int registerSize = GameView::getInstance()->registerNoticeVector.size();
	if (GameView::getInstance()->registerNoticeIsShow == true && registerSize > 0)
	{
		std::string strings_ = GameView::getInstance()->registerNoticeVector.at(0);
		if (strlen(strings_.c_str()) >0)
		{
			GameView::getInstance()->registerNoticeIsShow = false;
			RegisterNoticeui * registerui = RegisterNoticeui::create();
			registerui->ignoreAnchorPointForPosition(false);
			registerui->setAnchorPoint(ccp(0.5f,0.5f));
			registerui->setPosition(ccp(s.width/2,s.height/2));
			layer->addChild(registerui, 1);
		}
	}
}

void DefaultServerLayer::ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent)
{

}

void DefaultServerLayer::onLastServerGetClicked(cocos2d::CCObject *sender)
{
	GameState* pScene = new LoginState();
	if (pScene)
	{
		pScene->runThisState();
		pScene->release();
	}
}

void DefaultServerLayer::onChangeGetClicked(cocos2d::CCObject *sender)
{
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS

	// SDK需求加入（注销接口）
	ShareLoginState::logout();

#endif

	GameState* pScene = new LoginGameState();
	if (pScene)
	{
		pScene->runThisState();
		pScene->release();
	}
}

void DefaultServerLayer::onEnterGetClicked(cocos2d::CCObject *sender)
{
	if(4 == m_pServerListRsp->serverinfos(mCurrentServerId).status())
	{
		//GameView::getInstance()->showAlertDialog(StringDataManager::getString("server_status_maintain"));
		GameView::getInstance()->showAlertDialog(m_pServerListRsp->serverinfos(mCurrentServerId).statusvalue().c_str());
		return;
	}
	else if(5 == m_pServerListRsp->serverinfos(mCurrentServerId).status())
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("server_status_willopen"));
		return;
	}

	CCLOG("m_pServerListRsp->serverinfos(0).ip().c_str() = %s \n", m_pServerListRsp->serverinfos(mCurrentServerId).ip().c_str());
	CCLOG("m_pServerListRsp->serverinfos(0).port() = %d \n", m_pServerListRsp->serverinfos(mCurrentServerId).port());
	ClientNetEngine::sharedSocketEngine()->setAddress(m_pServerListRsp->serverinfos(mCurrentServerId).ip().c_str(), m_pServerListRsp->serverinfos(mCurrentServerId).port());

	if(1 == m_pServerListRsp->serverinfos(mCurrentServerId).has_roleid())
	{
		LoginLayer::mRoleId = m_pServerListRsp->serverinfos(mCurrentServerId).roleid();

		// tip's background
		CCSize s = CCDirector::sharedDirector()->getVisibleSize();
		CCRect insetRect = CCRectMake(32,15,1,1);
		CCScale9Sprite* backGround = CCScale9Sprite::create("res_ui/kuang_1.png"); 
		backGround->setCapInsets(insetRect);
		backGround->setPreferredSize(CCSizeMake(550, 50));
		backGround->setPosition(ccp(s.width/2, s.height/2));
		backGround->setAnchorPoint(ccp(0.5f, 0.5f));
		//backGround->setTag(kTagPreparingBackGround);
		backGround->setVisible(false);
		addChild(backGround);

		// show tip
		CCLabelTTF* label = CCLabelTTF::create(StringDataManager::getString("con_enter_game"), "Arial", 25);
		addChild(label);
		//label->setTag(kTagPreparingTip);
		label->setAnchorPoint(ccp(0.5f, 0.5f));
		label->setPosition( ccp(s.width/2, s.height/2) );
		label->setVisible(false);

		CCActionInterval *action = (CCActionInterval*)CCSequence::create
			(
				//CCShow::create(),
				CCDelayTime::create(0.5f),
				CCCallFunc::create(this, callfunc_selector(DefaultServerLayer::connectGameServer)), 
				NULL
			);
		label->runAction(action);

		CCUserDefault::sharedUserDefault()->setIntegerForKey("general_mode",1);


		#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
		
		// 浮动工具条（显示）
		ShareLoginState::showFloatView();

		#endif
	}
	else
	{
		onRoleListReq();
	}
}

void DefaultServerLayer::onRoleListReq()
{    
    // test 1
    {
        CCHttpRequest* request = new CCHttpRequest();
        std::string url = Login::s_loginserver_ip;
        url.append(":48688/rolelist");
        request->setUrl(url.c_str());
        request->setRequestType(CCHttpRequest::kHttpPost);
        request->setResponseCallback(this, httpresponse_selector(DefaultServerLayer::onRoleListRequestCompleted));

		RoleListReq httpReq;
		httpReq.set_userid(Login::userId);
		httpReq.set_sessionid(Login::sessionId);
		httpReq.set_serverid(m_pServerListRsp->serverinfos(mCurrentServerId).serverid());

		string msgData;
		httpReq.SerializeToString(&msgData);

		request->setRequestData(msgData.c_str(), msgData.size());
        request->setTag("POST");
		//CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
        CCHttpClient::getInstance()->send(request);
        request->release();
    }
}

void DefaultServerLayer::onRoleListRequestCompleted(CCHttpClient *sender, CCHttpResponse *response)
{
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag())) 
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed()) 
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    // dump data
	std::string strdata;
    strdata.assign(response->getResponseData()->data(), response->getResponseData()->size());

	RoleListRsp roleListRsp;
	CCAssert(response->getResponseData()->size() > 0, "should not be empty");
	roleListRsp.ParseFromString(strdata);

	CCLOG("response.result = %d", roleListRsp.result());

	if(1 == roleListRsp.result()&&roleListRsp.roleinfos().size())
	{
		LoginLayer::mRoleId = roleListRsp.roleinfos(0).id();

		// tip's background
		CCSize s = CCDirector::sharedDirector()->getVisibleSize();
		CCRect insetRect = CCRectMake(32,15,1,1);
		CCScale9Sprite* backGround = CCScale9Sprite::create("res_ui/kuang_1.png"); 
		backGround->setCapInsets(insetRect);
		backGround->setPreferredSize(CCSizeMake(550, 50));
		backGround->setPosition(ccp(s.width/2, s.height/2));
		backGround->setAnchorPoint(ccp(0.5f, 0.5f));
		backGround->setTag(kTagPreparingBackGround);
		backGround->setVisible(false);
		addChild(backGround);

		// show tip
		CCLabelTTF* label = CCLabelTTF::create(StringDataManager::getString("con_enter_game"), "Arial", 25);
		addChild(label);
		label->setTag(kTagPreparingTip);
		label->setAnchorPoint(ccp(0.5f, 0.5f));
		label->setPosition( ccp(s.width/2, s.height/2) );
		label->setVisible(false);

		CCActionInterval *action = (CCActionInterval*)CCSequence::create
			(
				//CCShow::create(),
				CCDelayTime::create(0.5f),
				CCCallFunc::create(this, callfunc_selector(DefaultServerLayer::connectGameServer)), 
				NULL
			);
		label->runAction(action);

		m_state = kStateConnectingGameServer;

		MyPlayerAIConfig::setAutomaticSkill(0);
		MyPlayerAIConfig::enableRobot(false);
	}
	else
	{
		onGetMinReq();
	}
}

void DefaultServerLayer::onGetMinReq()
{    
    // test 1
    {
        CCHttpRequest* request = new CCHttpRequest();
        std::string url = Login::s_loginserver_ip;
        url.append(":48688/countryprofession");
        request->setUrl(url.c_str());
        request->setRequestType(CCHttpRequest::kHttpPost);
        request->setResponseCallback(this, httpresponse_selector(DefaultServerLayer::onGetMinResp));

		CountryProfessionReq httpReq;
		httpReq.set_userid(Login::userId);
		httpReq.set_sessionid(Login::sessionId);
		httpReq.set_serverid(m_pServerListRsp->serverinfos(mCurrentServerId).serverid());

		string msgData;
		httpReq.SerializeToString(&msgData);

		request->setRequestData(msgData.c_str(), msgData.size());
        request->setTag("POST");
		//CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
        CCHttpClient::getInstance()->send(request);
        request->release();
    }
}

void DefaultServerLayer::onGetMinResp(CCHttpClient *sender, CCHttpResponse *response)
{
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag())) 
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed()) 
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    // dump data
	std::string strdata;
    strdata.assign(response->getResponseData()->data(), response->getResponseData()->size());

	CountryProfessionRsp countryProfessionRsp;
	CCAssert(response->getResponseData()->size() > 0, "should not be empty");
	countryProfessionRsp.ParseFromString(strdata);

	CCLOG("response.result = %d", countryProfessionRsp.result());

	if(1 == countryProfessionRsp.result())
	{
		LoginLayer::mCountryMin = countryProfessionRsp.country();
		LoginLayer::mProfessionMin = countryProfessionRsp.profession();

		GameState* pScene = new CreateRoleState();
		if (pScene)
		{
			pScene->runThisState();
			pScene->release();
		}
	}
	else
	{
		GameView::getInstance()->showAlertDialog(countryProfessionRsp.resultmessage());
	}
}

void DefaultServerLayer::requestServerList()
{    
    // test 1
    {
        CCHttpRequest* request = new CCHttpRequest();
        std::string url = Login::s_loginserver_ip;
        url.append(":48688/serverlist");
        request->setUrl(url.c_str());
        request->setRequestType(CCHttpRequest::kHttpPost);
        request->setResponseCallback(this, httpresponse_selector(DefaultServerLayer::onRequestServerListCompleted));

		ServerListReq httpReq;
		httpReq.set_userid(Login::userId);
		httpReq.set_sessionid(Login::sessionId);

		string msgData;
		httpReq.SerializeToString(&msgData);

		request->setRequestData(msgData.c_str(), msgData.size());
        request->setTag("POST");
		//CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
        CCHttpClient::getInstance()->send(request);
        request->release();
    }
}

void DefaultServerLayer::onRequestServerListCompleted(CCHttpClient *sender, CCHttpResponse *response)
{
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag())) 
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed()) 
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    // dump data
	std::string strdata;
    strdata.assign(response->getResponseData()->data(), response->getResponseData()->size());
	m_pServerListRsp->ParseFromString(strdata);

	CCLOG("response.result = %d", m_pServerListRsp->result());
	/*
	CCLOG("m_pServerListRsp->serverinfos().roleid() = %d",m_pServerListRsp->serverinfos(0).roleid());
	CCLOG("response.serverinfos_size = %d", m_pServerListRsp->serverinfos_size());
	CCLOG("m_pServerListRsp->serverinfos(0).has_roleid() = %d",m_pServerListRsp->serverinfos(0).has_roleid());
	CCLOG("m_pServerListRsp->serverinfos(0).roleid() = %lld\n",m_pServerListRsp->serverinfos(0).roleid());*/
	if(1 == m_pServerListRsp->result())
	{
		reloadServerInfo();
	}
	else
	{
		//GameView::getInstance()->showAlertDialog(StringDataManager::getString("login_server_list"));
	}
}

void DefaultServerLayer::reloadServerInfo()
{
	UILayer* layer = (UILayer*)getChildByTag(255);
	cocos2d::extension::UIButton* frame = (cocos2d::extension::UIButton*)layer->getWidgetByTag(FRAME_TAG);
	cocos2d::extension::UIImageView* status = (cocos2d::extension::UIImageView*)frame->getChildByTag(STATUS_TAG);
	switch(m_pServerListRsp->serverinfos(mCurrentServerId).status())
	{
		case 1:
			status->setTexture("res_ui/select_the_sercer/green_new.png");
			break;
		case 2:
			status->setTexture("res_ui/select_the_sercer/yellow_new.png");
			break;
		case 3:
			status->setTexture("res_ui/select_the_sercer/red_new.png");
			break;
		case 4:
			status->setTexture("res_ui/select_the_sercer/gray_new.png");
			break;
		case 5:
			status->setTexture("res_ui/select_the_sercer/gray_new.png");
			break;
		default:
			break;
	}
}

void DefaultServerLayer::update(float dt)
{
	switch(m_iStatus)
	{
	case kStateStart:
		struct timeval m_sStartTime;
		gettimeofday(&m_sStartTime, NULL);
		m_dStartTime = m_sStartTime.tv_sec*1000 + m_sStartTime.tv_usec/1000;

		m_iStatus = kStateWait;
		break;
	case kStateWait:
		struct timeval m_sEndedTime;
		gettimeofday(&m_sEndedTime, NULL);
		m_dEndedTime = m_sEndedTime.tv_sec*1000 + m_sEndedTime.tv_usec/1000;
		//wait 30 second to convert scene
		if(m_dEndedTime - m_dStartTime >= WAIT_TIME)
		{
			m_iStatus = kStateReload;
		}
		break;
	case kStateReload:
		requestServerList();
		m_iStatus = kStateStart;
		break;
	default:
		break;
	}
}

void DefaultServerLayer::connectGameServer()
{
	CCLOG("connectGameServer\n");
	if(ClientNetEngine::sharedSocketEngine()->getReadyState() == ClientNetEngine::kStateOpen)
		return;
	ClientNetEngine::sharedSocketEngine()->init(*(GameView::getInstance()));
}

void DefaultServerLayer::onSocketOpen(ClientNetEngine* socketEngine)
{
	// request to enter game
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1001, this);

	GameState* pScene = new GameSceneState();
	if (pScene)
	{
		pScene->runThisState();
		pScene->release();
	}
}
void DefaultServerLayer::onSocketClose(ClientNetEngine* socketEngine)
{
	//GameView::getInstance()->showAlertDialog(StringDataManager::getString("con_reconnection"));

	// player can re-connect
	if(m_state == kStateConnectingGameServer)
	{
		this->getChildByTag(kTagPreparingBackGround)->removeFromParent();
		this->getChildByTag(kTagPreparingTip)->removeFromParent();
		// enable role list
		/*for(int i = 0; i < MAX_LOGIN_ROLE_NUM; i++)
		{
			CCMenu* menu = (CCMenu*)this->getChildByTag(kTagRoleListMenu+i);
			menu->setEnabled(true);
		}
		*/
		m_state = kStateRoleList;
	}
}
void DefaultServerLayer::onSocketError(ClientNetEngine* socketEngines, const ClientNetEngine::ErrorCode& error)
{
}

///////////////////////////////////////////////////////////////////////////////////////

enum {
	kTagMainLayer = 1,
};

DefaultServerState::~DefaultServerState()
{
}

void DefaultServerState::runThisState()
{
    CCLayer* pLayer = new DefaultServerLayer();
    addChild(pLayer, 0, kTagMainLayer);
	pLayer->release();

	if(CCDirector::sharedDirector()->getRunningScene() != NULL)
		CCDirector::sharedDirector()->replaceScene(this);
	else
		CCDirector::sharedDirector()->runWithScene(this);
}

void DefaultServerState::onSocketOpen(ClientNetEngine* socketEngine)
{
	DefaultServerLayer* layer = (DefaultServerLayer*)this->getChildByTag(kTagMainLayer);
	layer->onSocketOpen(socketEngine);
}
void DefaultServerState::onSocketClose(ClientNetEngine* socketEngine)
{
	//GameView::getInstance()->showAlertDialog(StringDataManager::getString("con_reconnection"));

	DefaultServerLayer* layer = (DefaultServerLayer*)this->getChildByTag(kTagMainLayer);
	layer->onSocketClose(socketEngine);
}
void DefaultServerState::onSocketError(ClientNetEngine* socketEngines, const ClientNetEngine::ErrorCode& error)
{
}
