#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../GameStateBasic.h"
#include "../messageclient/ClientNetEngine.h"
#include "../messageclient/protobuf/LoginMessage.pb.h"
#include "../ui/extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

/**
 login to the game server
 */
class DefaultServerLayer : public UIScene
{
public:
    DefaultServerLayer();
	virtual ~DefaultServerLayer();

	void initLayer();

	virtual void onSocketOpen(ClientNetEngine* socketEngine);
    virtual void onSocketClose(ClientNetEngine* socketEngine);
    virtual void onSocketError(ClientNetEngine* ssocketEngines, const ClientNetEngine::ErrorCode& error);
	
	virtual void update(float dt);

	virtual void ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent);

private:
	void menuSelectRoleCallback(CCObject* pSender);
	void menuSelectServerCallback(CCObject* pSender);

	void connectGameServer();

	void initLastServer(int idx);
	void onLastServerGetClicked(cocos2d::CCObject *sender);
	
	void onRoleListReq();
	void onRoleListRequestCompleted(CCHttpClient *sender, CCHttpResponse *response);
	void onGetMinReq();
	void onGetMinResp(CCHttpClient *sender, CCHttpResponse *response);

	void requestServerList();
	void onRequestServerListCompleted(CCHttpClient *sender, CCHttpResponse *response);

	void reloadServerInfo();
	void onEnterGetClicked(cocos2d::CCObject *sender);
	void onChangeGetClicked(cocos2d::CCObject *sender);
private:
	int m_state;

	long double m_dStartTime;
	long double m_dEndedTime;

	int mSelectedId;

	static int mCurrentServerId;

	ServerListRsp* m_pServerListRsp;

	int mTouchedId;
	int mTouchedStatus;
	int lastServerIndex;
	bool isLastSelected;
	bool isTableViewTouched;

	int m_iStatus;
public:
	static int getCurrentServerId(){return mCurrentServerId;};
	static void setCurrentServerId(int serverIndex){mCurrentServerId = serverIndex;}
	
};

class DefaultServerState : public GameState
{
public:
	~DefaultServerState();

    virtual void runThisState();

	virtual void onSocketOpen(ClientNetEngine* socketEngine);
    virtual void onSocketClose(ClientNetEngine* socketEngine);
    virtual void onSocketError(ClientNetEngine* ssocketEngines, const ClientNetEngine::ErrorCode& error);
};
