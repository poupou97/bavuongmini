#include "MoutainBackGroundLayer.h"

#include "../legend_engine/CCLegendAnimation.h"

#define BG1_ROLL_TIME 60.0f
#define BG1_FIRST_TAG 120
#define BG1_SECOND_TAG 121

#define BG2_ROLL_TIME 100.0f
#define BG2_FIRST_TAG 130
#define BG2_SECOND_TAG 131

#define BG3_ROLL_TIME 200.0f
#define BG3_FIRST_TAG 140
#define BG3_SECOND_TAG 141

#define BG4_ROLL_TIME 1000.0f
#define BG4_FIRST_TAG 150
#define BG4_SECOND_TAG 151

#define BG_IMAGE_HEIGHT 640

MoutainBackGroundLayer::MoutainBackGroundLayer()
{

}

MoutainBackGroundLayer::~MoutainBackGroundLayer()
{
}

MoutainBackGroundLayer* MoutainBackGroundLayer::create()
{
    MoutainBackGroundLayer *pRet = new MoutainBackGroundLayer();
    if(pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    CC_SAFE_DELETE(pRet);
    return NULL;
}

bool MoutainBackGroundLayer::init()
{
	if(CCLayer::init())
	{
		CCSize winSize = CCDirector::sharedDirector()->getWinSize();

		BG4_scroll();
		BG3_scroll();
		BG2_scroll();
		BG1_scroll();

		//addBirds(this);

		

		//addBuddhaLight(pSprite, pSpriteNext);

		return true;
	}
	return false;
}

void MoutainBackGroundLayer::addBirds(CCNode* parent)
{
	CCSize s = CCDirector::sharedDirector()->getVisibleSize();

	const char* birdAnimFileName = "animation/texiao/changjingtexiao/xianhe/xianhe.anm";

	// birds
	// the head bird
	CCLegendAnimation* pDoorNode = CCLegendAnimation::create(birdAnimFileName);
	pDoorNode->setPlayLoop(true);
	pDoorNode->setReleaseWhenStop(false);
	pDoorNode->setAnchorPoint(ccp(0.5f,0.5f));
	pDoorNode->setPosition(ccp(s.width/2-200, s.height-50));
	pDoorNode->setScale(0.33f);
	parent->addChild(pDoorNode);

	pDoorNode = CCLegendAnimation::create(birdAnimFileName);
	pDoorNode->setPlayLoop(true);
	pDoorNode->setReleaseWhenStop(false);
	pDoorNode->setAnchorPoint(ccp(0.5f,0.5f));
	pDoorNode->setPosition(ccp(s.width/2-213, s.height-42));
	pDoorNode->setScale(0.28f);
	parent->addChild(pDoorNode);

	pDoorNode = CCLegendAnimation::create(birdAnimFileName);
	pDoorNode->setPlayLoop(true);
	pDoorNode->setReleaseWhenStop(false);
	pDoorNode->setAnchorPoint(ccp(0.5f,0.5f));
	pDoorNode->setPosition(ccp(s.width/2-226, s.height-34));
	pDoorNode->setScale(0.26f);
	parent->addChild(pDoorNode);

	pDoorNode = CCLegendAnimation::create(birdAnimFileName);
	pDoorNode->setPlayLoop(true);
	pDoorNode->setReleaseWhenStop(false);
	pDoorNode->setAnchorPoint(ccp(0.5f,0.5f));
	pDoorNode->setPosition(ccp(s.width/2-225, s.height-60));
	pDoorNode->setScale(0.38f);
	parent->addChild(pDoorNode);

	pDoorNode = CCLegendAnimation::create(birdAnimFileName);
	pDoorNode->setPlayLoop(true);
	pDoorNode->setReleaseWhenStop(false);
	pDoorNode->setAnchorPoint(ccp(0.5f,0.5f));
	pDoorNode->setPosition(ccp(s.width/2-250, s.height-70));
	pDoorNode->setScale(0.4f);
	parent->addChild(pDoorNode);
}

void MoutainBackGroundLayer::addWaterfall1(CCNode* parent, CCNode* next)
{
	CCSize s = CCDirector::sharedDirector()->getVisibleSize();

	const char* pWaterfallAnm = "animation/texiao/changjingtexiao/pubu/pubu03.anm";

	CCLegendAnimation* pWaterfallLong = CCLegendAnimation::create(pWaterfallAnm);
	pWaterfallLong->setPlayLoop(true);
	pWaterfallLong->setReleaseWhenStop(false);
	pWaterfallLong->setAnchorPoint(ccp(0.5f,0.5f));
	pWaterfallLong->setPosition(ccp(255, 120));
	pWaterfallLong->setScaleY(1.55f);
	//pWaterfall->setScaleX(0.6f);
	parent->addChild(pWaterfallLong);

	CCLegendAnimation* pWaterfallLongNext = CCLegendAnimation::create(pWaterfallAnm);
	pWaterfallLongNext->setPlayLoop(true);
	pWaterfallLongNext->setReleaseWhenStop(false);
	pWaterfallLongNext->setAnchorPoint(ccp(0.5f,0.5f));
	pWaterfallLongNext->setPosition(ccp(255, 120));
	pWaterfallLongNext->setScaleY(1.55f);
	//pWaterfall->setScaleX(0.6f);
	next->addChild(pWaterfallLongNext);
}
void MoutainBackGroundLayer::addWaterfall2(CCNode* parent, CCNode* next)
{
	CCSize s = CCDirector::sharedDirector()->getVisibleSize();

	const char* pWaterfallAnm = "animation/texiao/changjingtexiao/pubu/pubu03.anm";
	
	CCLegendAnimation* pWaterfallShort = CCLegendAnimation::create(pWaterfallAnm);
	pWaterfallShort->setPlayLoop(true);
	pWaterfallShort->setReleaseWhenStop(false);
	pWaterfallShort->setAnchorPoint(ccp(0.5f,0.5f));
	pWaterfallShort->setPosition(ccp(1905, 20));
	pWaterfallShort->setScaleY(0.4f);
	pWaterfallShort->setScaleX(-3.0f);
	parent->addChild(pWaterfallShort);
	
	CCLegendAnimation* pRiver = CCLegendAnimation::create(pWaterfallAnm);
	pRiver->setPlayLoop(true);
	pRiver->setReleaseWhenStop(false);
	pRiver->setAnchorPoint(ccp(0.5f,0.5f));
	pRiver->setPosition(ccp(1935, 65));
	pRiver->setScaleY(0.7f);
	pRiver->setScaleX(-1.2f);
	pRiver->setRotation(60);
	parent->addChild(pRiver);

	CCLegendAnimation* pWaterfallShortNext = CCLegendAnimation::create(pWaterfallAnm);
	pWaterfallShortNext->setPlayLoop(true);
	pWaterfallShortNext->setReleaseWhenStop(false);
	pWaterfallShortNext->setAnchorPoint(ccp(0.5f,0.5f));
	pWaterfallShortNext->setPosition(ccp(1905, 20));
	pWaterfallShortNext->setScaleY(0.4f);
	pWaterfallShortNext->setScaleX(-3.0f);
	next->addChild(pWaterfallShortNext);
	
	CCLegendAnimation* pRiverNext = CCLegendAnimation::create(pWaterfallAnm);
	pRiverNext->setPlayLoop(true);
	pRiverNext->setReleaseWhenStop(false);
	pRiverNext->setAnchorPoint(ccp(0.5f,0.5f));
	pRiverNext->setPosition(ccp(1935, 65));
	pRiverNext->setScaleY(0.7f);
	pRiverNext->setScaleX(-1.2f);
	pRiverNext->setRotation(60);
	next->addChild(pRiverNext);
}

CCParticleSystem* MoutainBackGroundLayer::generateSmokeEffect()
{
	const char* EFFECTNAME = "animation/texiao/particledesigner/langyan.plist";

	// smoke effect
	CCParticleSystem* particleEffect = CCParticleSystemQuad::create(EFFECTNAME);
	particleEffect->setPositionType(kCCPositionTypeGrouped);

	// fire effect
	const char* FIRE_EFFECTNAME = "animation/texiao/particledesigner/huo2.plist";
	CCParticleSystem* fireEffect = CCParticleSystemQuad::create(FIRE_EFFECTNAME);
	fireEffect->setPositionType(kCCPositionTypeGrouped);
	fireEffect->setScaleX(0.2f);
	fireEffect->setPosition(0, 0);

	particleEffect->addChild(fireEffect);

	return particleEffect;
}

void MoutainBackGroundLayer::addSmoke1(CCNode* parent, CCNode* next)
{
	// effect 1
	CCParticleSystem* particleEffect = generateSmokeEffect();
	particleEffect->setScale(0.2f);
	particleEffect->setPosition(ccp(74, 471));
	parent->addChild(particleEffect);

	particleEffect = generateSmokeEffect();
	particleEffect->setScale(0.2f);
	particleEffect->setPosition(ccp(74, 471));
	next->addChild(particleEffect);

	// effect 2
	particleEffect = generateSmokeEffect();
	particleEffect->setScale(0.3f);
	particleEffect->setPosition(ccp(1507, 362));
	parent->addChild(particleEffect);

	particleEffect = generateSmokeEffect();
	particleEffect->setScale(0.3f);
	particleEffect->setPosition(ccp(1507, 362));
	next->addChild(particleEffect);

	// effect 3
	particleEffect = generateSmokeEffect();
	particleEffect->setScale(0.3f);
	particleEffect->setPosition(ccp(1053, 354));
	parent->addChild(particleEffect);

	particleEffect = generateSmokeEffect();
	particleEffect->setScale(0.3f);
	particleEffect->setPosition(ccp(1053, 354));
	next->addChild(particleEffect);
}

void MoutainBackGroundLayer::addSmoke2(CCNode* parent, CCNode* next)
{
	// effect 1
	CCParticleSystem* particleEffect = generateSmokeEffect();
	particleEffect->setScale(0.1f);
	particleEffect->setPosition(ccp(1247, 563));
	parent->addChild(particleEffect);

	particleEffect = generateSmokeEffect();
	particleEffect->setScale(0.1f);
	particleEffect->setPosition(ccp(1247, 563));
	next->addChild(particleEffect);
}

void MoutainBackGroundLayer::addBuddhaLight(CCNode* parent, CCNode* next)
{
	CCSize s = CCDirector::sharedDirector()->getVisibleSize();

	const char* pBuddhaLightAnm = "animation/texiao/renwutexiao/feguang/feguang.anm";

	CCLegendAnimation* pWaterfallLong = CCLegendAnimation::create(pBuddhaLightAnm);
	pWaterfallLong->setPlayLoop(true);
	pWaterfallLong->setReleaseWhenStop(false);
	pWaterfallLong->setAnchorPoint(ccp(0.5f,0.5f));
	pWaterfallLong->setPosition(ccp(1472, 178));
	parent->addChild(pWaterfallLong);

	CCLegendAnimation* pWaterfallLongNext = CCLegendAnimation::create(pBuddhaLightAnm);
	pWaterfallLongNext->setPlayLoop(true);
	pWaterfallLongNext->setReleaseWhenStop(false);
	pWaterfallLongNext->setAnchorPoint(ccp(0.5f,0.5f));
	pWaterfallLongNext->setPosition(ccp(1472, 178));
	next->addChild(pWaterfallLongNext);

}

void MoutainBackGroundLayer::BG1_scroll()
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();

	const char* moutain_picture_file = "images/denglu_fc1.png";

	//background image
	CCSprite* pSprite = CCSprite::create(moutain_picture_file);
	pSprite->setPosition(ccp(0, 0));
	pSprite->setAnchorPoint(ccp(0, 0));
	float scaleValue;
	scaleValue = (float)winSize.height/(float)BG_IMAGE_HEIGHT;
	pSprite->setScale(scaleValue);
	float widthMultiple = pSprite->getContentSize().width*scaleValue/(float)winSize.width;
	pSprite->setTag(BG1_FIRST_TAG);
	//pSprite->setColor(ccc3(160, 160, 160));
	addChild(pSprite, 0);

	CCSprite* pSpriteNext = CCSprite::create(moutain_picture_file);
	pSpriteNext->setPosition(ccp(winSize.width-1, 0));
	pSpriteNext->setAnchorPoint(ccp(0, 0));
	pSpriteNext->setScale(scaleValue);
	pSpriteNext->setTag(BG1_SECOND_TAG);
	addChild(pSpriteNext, 0);

	CCMoveTo * moveToAction = CCMoveTo::create(BG1_ROLL_TIME,ccp(-pSprite->getContentSize().width*scaleValue, 0));
	CCMoveTo * moveToReturn = CCMoveTo::create(0,ccp(winSize.width-1, 0));
	CCMoveTo * moveToStart = CCMoveTo::create(BG1_ROLL_TIME/widthMultiple,ccp(0, 0));
	CCRepeatForever * repeapAction = CCRepeatForever::create(
		CCSequence::create(
			moveToAction,
			moveToReturn,
			CCDelayTime::create(BG1_ROLL_TIME*(widthMultiple-1)/widthMultiple),
			moveToStart,
			NULL));
	pSprite->runAction(repeapAction);
	
	CCSequence * nextAction = CCSequence::create(
		CCDelayTime::create(BG1_ROLL_TIME*(widthMultiple-1)/widthMultiple), 
		CCCallFunc::create(this, callfunc_selector(MoutainBackGroundLayer::BG1_repeatFunc)), 
		NULL);
	pSpriteNext->runAction(nextAction);

	addWaterfall1(pSprite, pSpriteNext);
}
void MoutainBackGroundLayer::BG1_repeatFunc()
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	CCSprite* pSprite = (CCSprite*)getChildByTag(BG1_SECOND_TAG);
	float scaleValue = pSprite->getScale();
	float widthMultiple = pSprite->getContentSize().width*scaleValue/(float)winSize.width;

	CCMoveTo * moveToActionNext = CCMoveTo::create(BG1_ROLL_TIME*(widthMultiple+1)/widthMultiple,ccp(-pSprite->getContentSize().width*scaleValue, 0));
	CCMoveTo * moveToReturnNext = CCMoveTo::create(0,ccp(winSize.width-1, 0));
	CCRepeatForever * repeapActionNext = CCRepeatForever::create(
		CCSequence::create(
			moveToActionNext,
			moveToReturnNext,
			CCDelayTime::create(BG1_ROLL_TIME*(widthMultiple-1)/widthMultiple),
			NULL));
	pSprite->runAction(repeapActionNext);
}

void MoutainBackGroundLayer::BG2_scroll()
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();

	const char* moutain_picture_file = "images/denglu_fc2.png";

	//background image
	CCSprite* pSprite = CCSprite::create(moutain_picture_file);
	pSprite->setPosition(ccp(0, 0));
	pSprite->setAnchorPoint(ccp(0, 0));
	float scaleValue;
	scaleValue = (float)winSize.height/(float)BG_IMAGE_HEIGHT;
	pSprite->setScale(scaleValue);
	float widthMultiple = pSprite->getContentSize().width*scaleValue/(float)winSize.width;
	pSprite->setTag(BG2_FIRST_TAG);
	//pSprite->setColor(ccc3(160, 160, 160));
	addChild(pSprite, 0);

	CCSprite* pSpriteNext = CCSprite::create(moutain_picture_file);
	pSpriteNext->setPosition(ccp(winSize.width-1, 0));
	pSpriteNext->setAnchorPoint(ccp(0, 0));
	pSpriteNext->setScale(scaleValue);
	pSpriteNext->setTag(BG2_SECOND_TAG);
	addChild(pSpriteNext, 0);

	CCMoveTo * moveToAction = CCMoveTo::create(BG2_ROLL_TIME,ccp(-pSprite->getContentSize().width*scaleValue, 0));
	CCMoveTo * moveToReturn = CCMoveTo::create(0,ccp(winSize.width-1, 0));
	CCMoveTo * moveToStart = CCMoveTo::create(BG2_ROLL_TIME/widthMultiple,ccp(0, 0));
	CCRepeatForever * repeapAction = CCRepeatForever::create(
		CCSequence::create(
			moveToAction,
			moveToReturn,
			CCDelayTime::create(BG2_ROLL_TIME*(widthMultiple-1)/widthMultiple),
			moveToStart,
			NULL));
	pSprite->runAction(repeapAction);
	
	CCSequence * nextAction = CCSequence::create(
		CCDelayTime::create(BG2_ROLL_TIME*(widthMultiple-1)/widthMultiple), 
		CCCallFunc::create(this, callfunc_selector(MoutainBackGroundLayer::BG2_repeatFunc)), 
		NULL);
	pSpriteNext->runAction(nextAction);

	addWaterfall2(pSprite, pSpriteNext);

	addSmoke1(pSprite, pSpriteNext);
}
void MoutainBackGroundLayer::BG2_repeatFunc()
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	CCSprite* pSprite = (CCSprite*)getChildByTag(BG2_SECOND_TAG);
	float scaleValue = pSprite->getScale();
	float widthMultiple = pSprite->getContentSize().width*scaleValue/(float)winSize.width;

	CCMoveTo * moveToActionNext = CCMoveTo::create(BG2_ROLL_TIME*(widthMultiple+1)/widthMultiple,ccp(-pSprite->getContentSize().width*scaleValue, 0));
	CCMoveTo * moveToReturnNext = CCMoveTo::create(0,ccp(winSize.width-1, 0));
	CCRepeatForever * repeapActionNext = CCRepeatForever::create(
		CCSequence::create(
			moveToActionNext,
			moveToReturnNext,
			CCDelayTime::create(BG2_ROLL_TIME*(widthMultiple-1)/widthMultiple),
			NULL));
	pSprite->runAction(repeapActionNext);
}

void MoutainBackGroundLayer::BG3_scroll()
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();

	const char* moutain_picture_file = "images/denglu_fc3.png";

	//background image
	CCSprite* pSprite = CCSprite::create(moutain_picture_file);
	pSprite->setPosition(ccp(0, 0));
	pSprite->setAnchorPoint(ccp(0, 0));
	float scaleValue;
	scaleValue = (float)winSize.height/(float)BG_IMAGE_HEIGHT;
	pSprite->setScale(scaleValue);
	float widthMultiple = pSprite->getContentSize().width*scaleValue/(float)winSize.width;
	pSprite->setTag(BG3_FIRST_TAG);
	//pSprite->setColor(ccc3(160, 160, 160));
	addChild(pSprite, 0);

	CCSprite* pSpriteNext = CCSprite::create(moutain_picture_file);
	pSpriteNext->setPosition(ccp(winSize.width-1, 0));
	pSpriteNext->setAnchorPoint(ccp(0, 0));
	pSpriteNext->setScale(scaleValue);
	pSpriteNext->setTag(BG3_SECOND_TAG);
	addChild(pSpriteNext, 0);

	CCMoveTo * moveToAction = CCMoveTo::create(BG3_ROLL_TIME,ccp(-pSprite->getContentSize().width*scaleValue, 0));
	CCMoveTo * moveToReturn = CCMoveTo::create(0,ccp(winSize.width-1, 0));
	CCMoveTo * moveToStart = CCMoveTo::create(BG3_ROLL_TIME/widthMultiple,ccp(0, 0));
	CCRepeatForever * repeapAction = CCRepeatForever::create(
		CCSequence::create(
			moveToAction,
			moveToReturn,
			CCDelayTime::create(BG3_ROLL_TIME*(widthMultiple-1)/widthMultiple),
			moveToStart,
			NULL));
	pSprite->runAction(repeapAction);
	
	CCSequence * nextAction = CCSequence::create(
		CCDelayTime::create(BG3_ROLL_TIME*(widthMultiple-1)/widthMultiple), 
		CCCallFunc::create(this, callfunc_selector(MoutainBackGroundLayer::BG3_repeatFunc)), 
		NULL);
	pSpriteNext->runAction(nextAction);

	addSmoke2(pSprite, pSpriteNext);
}
void MoutainBackGroundLayer::BG3_repeatFunc()
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	CCSprite* pSprite = (CCSprite*)getChildByTag(BG3_SECOND_TAG);
	float scaleValue = pSprite->getScale();
	float widthMultiple = pSprite->getContentSize().width*scaleValue/(float)winSize.width;

	CCMoveTo * moveToActionNext = CCMoveTo::create(BG3_ROLL_TIME*(widthMultiple+1)/widthMultiple,ccp(-pSprite->getContentSize().width*scaleValue, 0));
	CCMoveTo * moveToReturnNext = CCMoveTo::create(0,ccp(winSize.width-1, 0));
	CCRepeatForever * repeapActionNext = CCRepeatForever::create(
		CCSequence::create(
			moveToActionNext,
			moveToReturnNext,
			CCDelayTime::create(BG3_ROLL_TIME*(widthMultiple-1)/widthMultiple),
			NULL));
	pSprite->runAction(repeapActionNext);
}

void MoutainBackGroundLayer::BG4_scroll()
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();

	const char* moutain_picture_file = "images/denglu_fc4.jpg";

	//background image
	CCSprite* pSprite = CCSprite::create(moutain_picture_file);
	pSprite->setPosition(ccp(0, 0));
	pSprite->setAnchorPoint(ccp(0, 0));
	float scaleValue;
	scaleValue = (float)winSize.height/(float)BG_IMAGE_HEIGHT;
	pSprite->setScale(scaleValue);
	float widthMultiple = pSprite->getContentSize().width*scaleValue/(float)winSize.width;
	pSprite->setTag(BG4_FIRST_TAG);
	//pSprite->setColor(ccc3(160, 160, 160));
	addChild(pSprite, 0);

	CCSprite* pSpriteNext = CCSprite::create(moutain_picture_file);
	pSpriteNext->setPosition(ccp(winSize.width-1, 0));
	pSpriteNext->setAnchorPoint(ccp(0, 0));
	pSpriteNext->setScale(scaleValue);
	pSpriteNext->setTag(BG4_SECOND_TAG);
	addChild(pSpriteNext, 0);

	CCMoveTo * moveToAction = CCMoveTo::create(BG4_ROLL_TIME,ccp(-pSprite->getContentSize().width*scaleValue, 0));
	CCMoveTo * moveToReturn = CCMoveTo::create(0,ccp(winSize.width-1, 0));
	CCMoveTo * moveToStart = CCMoveTo::create(BG4_ROLL_TIME/widthMultiple,ccp(0, 0));
	CCRepeatForever * repeapAction = CCRepeatForever::create(
		CCSequence::create(
			moveToAction,
			moveToReturn,
			CCDelayTime::create(BG4_ROLL_TIME*(widthMultiple-1)/widthMultiple),
			moveToStart,
			NULL));
	pSprite->runAction(repeapAction);
	
	CCSequence * nextAction = CCSequence::create(
		CCDelayTime::create(BG4_ROLL_TIME*(widthMultiple-1)/widthMultiple), 
		CCCallFunc::create(this, callfunc_selector(MoutainBackGroundLayer::BG4_repeatFunc)), 
		NULL);
	pSpriteNext->runAction(nextAction);
}
void MoutainBackGroundLayer::BG4_repeatFunc()
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	CCSprite* pSprite = (CCSprite*)getChildByTag(BG4_SECOND_TAG);
	float scaleValue = pSprite->getScale();
	float widthMultiple = pSprite->getContentSize().width*scaleValue/(float)winSize.width;

	CCMoveTo * moveToActionNext = CCMoveTo::create(BG4_ROLL_TIME*(widthMultiple+1)/widthMultiple,ccp(-pSprite->getContentSize().width*scaleValue, 0));
	CCMoveTo * moveToReturnNext = CCMoveTo::create(0,ccp(winSize.width-1, 0));
	CCRepeatForever * repeapActionNext = CCRepeatForever::create(
		CCSequence::create(
			moveToActionNext,
			moveToReturnNext,
			CCDelayTime::create(BG4_ROLL_TIME*(widthMultiple-1)/widthMultiple),
			NULL));
	pSprite->runAction(repeapActionNext);
}

void MoutainBackGroundLayer::setBackGroundColor(const ccColor3B& color3)
{
	CCSprite* pSprite1 = (CCSprite*)getChildByTag(BG1_FIRST_TAG);
	pSprite1->setColor(color3);
	CCSprite* pSprite2 = (CCSprite*)getChildByTag(BG1_SECOND_TAG);
	pSprite2->setColor(color3);

	pSprite1 = (CCSprite*)getChildByTag(BG2_FIRST_TAG);
	pSprite1->setColor(color3);
	pSprite2 = (CCSprite*)getChildByTag(BG2_SECOND_TAG);
	pSprite2->setColor(color3);

	pSprite1 = (CCSprite*)getChildByTag(BG3_FIRST_TAG);
	pSprite1->setColor(color3);
	pSprite2 = (CCSprite*)getChildByTag(BG3_SECOND_TAG);
	pSprite2->setColor(color3);

	pSprite1 = (CCSprite*)getChildByTag(BG4_FIRST_TAG);
	pSprite1->setColor(color3);
	pSprite2 = (CCSprite*)getChildByTag(BG4_SECOND_TAG);
	pSprite2->setColor(color3);
}

void MoutainBackGroundLayer::ccTouchesBegan(CCSet *pTouches, CCEvent *pEvent)
{
}

void MoutainBackGroundLayer::ccTouchesMoved(CCSet *pTouches, CCEvent *pEvent)
{
}

void MoutainBackGroundLayer::ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent)
{
}
