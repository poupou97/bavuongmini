#include "LoginState.h"

#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS

#include "../gamescene_state/GameSceneState.h"
#include "../loadscene_state/LoadSceneState.h"
#include "../legend_engine/CCLegendAnimation.h"
#include "../legend_engine/LegendLevel.h"
#include "../ui/extensions/CCRichLabel.h"
#include "../ui/extensions/RichElement.h"

#include "../utils/GameUtils.h"

#include "../messageclient/GameMessageProcessor.h"
#include "../messageclient/ClientNetEngine.h"

#include "../ui/extensions/RichTextInput.h"
#include "../GameView.h"
#include "../messageclient/protobuf/LoginMessage.pb.h"
#include "../login_state/Login.h"
#include "../login_state/CreateRole.h"
#include "../ui/generals_ui/GeneralsUI.h"
#include "../utils/StaticDataManager.h"
#include "../gamescene_state/role/BasePlayer.h"
#include "../AppMacros.h"
#include "../gamescene_state/role/MyPlayerAIConfig.h"
#include "../gamescene_state/role/MyPlayer.h"
#include "../gamescene_state/role/MyPlayerAI.h"
#include "../GameUserDefault.h"
#include "../login_state/MoutainBackGroundLayer.h"
#include "../ui/registerNotice/RegisterNoticeui.h"
#include "../login_state/DefaultServer.h"

//#include "share_sdk/ShareLoginState.h"

#else

#include "../gamescene_state/GameSceneState.h"
#include "../loadscene_state/LoadSceneState.h"
#include "../legend_engine/CCLegendAnimation.h"
#include "../legend_engine/LegendLevel.h"
#include "../ui/extensions/CCRichLabel.h"
#include "../ui/extensions/RichElement.h"

#include "../utils/GameUtils.h"

#include "../messageclient/GameMessageProcessor.h"
#include "../messageclient/ClientNetEngine.h"

#include "../ui/extensions/RichTextInput.h"
#include "../GameView.h"
#include "../messageclient/protobuf/LoginMessage.pb.h"
#include "Login.h"
#include "CreateRole.h"
#include "../ui/generals_ui/GeneralsUI.h"
#include "../utils/StaticDataManager.h"
#include "../gamescene_state/role/BasePlayer.h"
#include "AppMacros.h"
#include "../gamescene_state/role/MyPlayerAIConfig.h"
#include "../gamescene_state/role/MyPlayer.h"
#include "../gamescene_state/role/MyPlayerAI.h"
#include "GameUserDefault.h"
#include "MoutainBackGroundLayer.h"
#include "../ui/registerNotice/RegisterNoticeui.h"
#include "DefaultServer.h"

#endif


//#define THREEKINGDOMS_HOST_INTERNET "192.168.1.138"//"61.149.218.125"//"qianliang2013.vicp.cc"//"192.168.1.117"
//#define THREEKINGDOMS_PORT_INTERNET 8088
//#define THREEKINGDOMS_HOST_WANMEI "124.202.137.33"
//#define THREEKINGDOMS_PORT_WANMEI 8088
#define THREEKINGDOMS_HOST_STUDIO "139.99.49.200"  
#define THREEKINGDOMS_PORT_STUDIO 8188
//#define THREEKINGDOMS_HOST_GBK "192.168.1.118"  
//#define THREEKINGDOMS_PORT_GBK 8088  
//#define THREEKINGDOMS_HOST_ZHAGNGJING "192.168.1.132"  
//#define THREEKINGDOMS_PORT_ZHAGNGJING 8088  
//#define THREEKINGDOMS_HOST_LOCAL "127.0.0.1"  
//#define THREEKINGDOMS_PORT_LOCAL 8088  

#define MAX_LOGIN_ROLE_NUM 24

#define LASTSERVER_TAG 201
#define HIGHLIGHT_FRAME_TAG 202
#define NEWSERVER_TAG 203
#define WAIT_TIME 30*1000

#define TABLEVIEW_LAST_TAG 55
#define TABLEVIEW_TAG 56

enum {
	kStateStart = 0,
	kStateWait = 1,
	kStateReload = 2,
};

enum {
	kStateRoleList = 0,
	kStateConnectingGameServer,
};

enum {
	kTagPreparingBackGround = 2,
	kTagPreparingTip = 3,
	kTagServerTip = 4,
	kTagRoleListMenu = 16,   // last one !! please add more tag before it
};

long LoginLayer::mRoleId = 0;
int LoginLayer::mCurrentServerId = 0;
int LoginLayer::mCountryMin = 1;
int LoginLayer::mProfessionMin = 1;
int LoginLayer::s_iLastServerRoleLevel = 0;

LoginLayer::LoginLayer()
: m_state(kStateRoleList)
,isLastSelected(true)
,isTableViewTouched(true)
,m_iStatus(kStateStart)
{
	setTouchEnabled(true);
	scheduleUpdate();

    CCSize s = CCDirector::sharedDirector()->getVisibleSize();
	
	m_pServerListRsp = new ServerListRsp();
	Login::getServerListInfo(m_pServerListRsp);

	MoutainBackGroundLayer* pBackGround = MoutainBackGroundLayer::create();
	pBackGround->setBackGroundColor(ccc3(160, 160, 160));
	addChild(pBackGround);

	LoginState::addAppVersionInfo(this);
	
	UILayer* layer= UILayer::create();
	layer->setPosition(ccp(0,0));
	addChild(layer);

	UILayer* m_pUiLayer= UILayer::create();
	m_pUiLayer->ignoreAnchorPointForPosition(false);
	// 锚点在中心，并放置在屏幕中心点
	m_pUiLayer->setAnchorPoint(ccp(0.5f,0.5f));
	m_pUiLayer->setPosition(ccp(s.width/2,s.height/2));  
	m_pUiLayer->setTag(255);
	// 设置基准分辨率
	m_pUiLayer->setContentSize(CCSizeMake(UI_DESIGN_RESOLUTION_WIDTH, UI_DESIGN_RESOLUTION_HEIGHT));
	addChild(m_pUiLayer);

	UIPanel *serverListPanel=(UIPanel *)GUIReader::shareReader()->widgetFromJsonFile("res_ui/Select_the_server_1.json");
	serverListPanel->setAnchorPoint(ccp(0.5f,0.5f));
	serverListPanel->setPosition(ccp(s.width/2,s.height/2));

	layer->addWidget(serverListPanel);
	
	//UIImageView * ImageView_biankuang= (UIImageView *)UIHelper::seekWidgetByName(serverListPanel,"ImageView_biankuang1");
	int i;
	for(i = 0; i < m_pServerListRsp->serverinfos_size(); i++)
	{
		if(m_pServerListRsp->lastserverid() == m_pServerListRsp->serverinfos(i).serverid())
		{
			lastServerIndex = i;
			break;
		}
	}
	if(i == m_pServerListRsp->serverinfos_size())
	{
		lastServerIndex = 0;
	}
	initLastServer(lastServerIndex);
	mCurrentServerId = lastServerIndex;

	UITextButton* textbutton = UITextButton::create();
    //button->setTouchEnable(true);
    textbutton->loadTextures("res_ui/new_button_12pro.png", "res_ui/new_button_12pro.png", "");
	textbutton->setText("青龙偃月");
	textbutton->setFontSize(18);
	textbutton->setScale9Enabled(true);
	textbutton->setSize(CCSizeMake(86, 43));
	textbutton->setPosition(ccp(201,265));
	textbutton->setTouchEnable(true);
	textbutton->setPressedActionEnabled(true);
	m_pUiLayer->addWidget(textbutton);

	tableViewLast = CCTableView::create(this, CCSizeMake(420, 63));
	//tableViewLast->setSelectedEnable(true);
	//tableViewLast->setSelectedScale9Texture("res_ui/kuang_0_on.png", CCRectMake(7, 7, 1, 1), ccp(20,0));
	tableViewLast->setDirection(kCCScrollViewDirectionVertical);
	tableViewLast->setAnchorPoint(ccp(0.5f,0.5f));
	tableViewLast->setPosition(ccp(251,302));
	tableViewLast->setDelegate(this);
	tableViewLast->setVerticalFillOrder(kCCTableViewFillTopDown);
	tableViewLast->setPressedActionEnabled(true);
	tableViewLast->setBounceable(false);
	tableViewLast->setTag(TABLEVIEW_LAST_TAG);
	m_pUiLayer->addChild(tableViewLast);
	tableViewLast->reloadData();
	
	tableView = CCTableView::create(this, CCSizeMake(420, 240));
	//tableView->setSelectedEnable(true);
	//tableView->setSelectedScale9Texture("res_ui/kuang_0_on.png", CCRectMake(7, 7, 1, 1), ccp(0,0));
	tableView->setDirection(kCCScrollViewDirectionVertical);
	tableView->setAnchorPoint(ccp(0.5f,0.5f));
	tableView->setPosition(ccp(251,57));
	tableView->setDelegate(this);
	//tableView->selectCell(lastServerIndex);
	tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
	tableView->setPressedActionEnabled(true);
	tableView->setBounceable(true);
	tableView->setTag(TABLEVIEW_TAG);
	//this->addChild(tableView);
	m_pUiLayer->addChild(tableView);
	tableView->reloadData();
	////
	//cocos2d::extension::UIButton *enter = (cocos2d::extension::UIButton *)UIHelper::seekWidgetByName(serverListPanel,"Button_enter");
	//enter->setTouchEnable(true);
	//enter->setPressedActionEnabled(true);
	//enter->setVisible(false);
	//enter->addReleaseEvent(this,coco_releaseselector(LoginLayer::onEnterButtonGetClicked));

	cocos2d::extension::UIButton *back = (cocos2d::extension::UIButton *)UIHelper::seekWidgetByName(serverListPanel,"Button_back");
    back->setTouchEnable(true);
	back->addReleaseEvent(this, coco_releaseselector(LoginLayer::onButtonGetClicked));
	back->setPressedActionEnabled(true);
	
	//add registerNotice
	int registerSize = GameView::getInstance()->registerNoticeVector.size();
	if (GameView::getInstance()->registerNoticeIsShow == true && registerSize > 0)
	{
		std::string strings_ = GameView::getInstance()->registerNoticeVector.at(0);
		if (strlen(strings_.c_str()) >0)
		{
			GameView::getInstance()->registerNoticeIsShow = false;
			RegisterNoticeui * registerui = RegisterNoticeui::create();
			registerui->ignoreAnchorPointForPosition(false);
			registerui->setAnchorPoint(ccp(0.5f,0.5f));
			registerui->setPosition(ccp(s.width/2,s.height/2));
			addChild(registerui);
		}
	}

}

LoginLayer::~LoginLayer()
{

}

int LoginLayer::getLastServerRoleLevel()
{
	return s_iLastServerRoleLevel;
}

void LoginLayer::onLastServerGetClicked(cocos2d::CCObject *sender)
{
	UILayer* m_pUiLayer = (UILayer*)getChildByTag(255);
	cocos2d::extension::UIButton* btn = (cocos2d::extension::UIButton*)m_pUiLayer->getWidgetByTag(LASTSERVER_TAG);
	cocos2d::extension::UIImageView* frame = (cocos2d::extension::UIImageView*)btn->getChildByTag(HIGHLIGHT_FRAME_TAG);
	frame->setVisible(true);
	
	isLastSelected = true;
	//tableView->setSelectedEnable(false);
	tableView->reloadData();
	mCurrentServerId = lastServerIndex;
	//tableView->selectCell(lastServerIndex);
}

void LoginLayer::initLastServer(int idx)
{
	if(idx<0||idx>=m_pServerListRsp->serverinfos_size())
	{
		return;
	}

	UILayer* m_pUiLayer = (UILayer*)getChildByTag(255);
	cocos2d::extension::UIButton* btn = (cocos2d::extension::UIButton*)m_pUiLayer->getWidgetByTag(LASTSERVER_TAG);
	if(btn != NULL)
	{
		btn->removeFromParentAndCleanup(true);
	}

	cocos2d::extension::UIButton *sprite = cocos2d::extension::UIButton::create();
	sprite->loadTextures("res_ui/kuang_0.png", "res_ui/kuang_0.png", "");
    sprite->setAnchorPoint(ccp(0.5f, 0.5f));
	sprite->setScale9Enabled(true);
	//sprite->setPreferredSize(CCSizeMake(370,55));
	sprite->setSize(CCSizeMake(370,55));
    sprite->setPosition(ccp(456, 337));
	sprite->setTouchEnable(true);
	sprite->setPressedActionEnabled(true, 0.9f, 1.1f);
	sprite->setTag(LASTSERVER_TAG);
	sprite->setVisible(false);
	sprite->addReleaseEvent(this, coco_releaseselector(LoginLayer::onLastServerGetClicked));
	m_pUiLayer->addWidget(sprite);

	cocos2d::extension::UIImageView* pHighlightSpr = cocos2d::extension::UIImageView::create();
	pHighlightSpr->loadTexture("res_ui/kuang_0_on.png");
	pHighlightSpr->setScale9Enable(true);
	pHighlightSpr->setCapInsets(CCRectMake(7, 7, 1, 1));
	pHighlightSpr->setScale9Size(CCSizeMake(370,55));
	pHighlightSpr->setAnchorPoint(ccp(0.5f, 0.5f));
	pHighlightSpr->setTag(HIGHLIGHT_FRAME_TAG);
	pHighlightSpr->setVisible(isLastSelected);
	sprite->addChild(pHighlightSpr);

    CCLabelTTF *label = CCLabelTTF::create(m_pServerListRsp->serverinfos(idx).name().c_str(), APP_FONT_NAME, 18);
	label->setColor(ccc3(47,93,13));
    label->setPosition(ccp(-118, 0));
	label->setAnchorPoint(ccp(0, 0.5f));
	sprite->addCCNode(label);
		
	CCSprite * serverStatus = CCSprite::create();
	switch(m_pServerListRsp->serverinfos(idx).status())
	{
		case 1:
			serverStatus->initWithFile("res_ui/select_the_sercer/green_new.png");
			break;
		case 2:
			serverStatus->initWithFile("res_ui/select_the_sercer/yellow_new.png");
			break;
		case 3:
			serverStatus->initWithFile("res_ui/select_the_sercer/red_new.png");
			break;
		case 4:
			serverStatus->initWithFile("res_ui/select_the_sercer/gray_new.png");
			break;
		case 5:
			serverStatus->initWithFile("res_ui/select_the_sercer/gray_new.png");
			break;
		default:
			break;
	}
	serverStatus->setVisible(true);
	serverStatus->setAnchorPoint(ccp(0.5f,0.5f));
	serverStatus->setPosition(ccp(-146, 0));
	sprite->addCCNode(serverStatus);

	if(1 == m_pServerListRsp->serverinfos(idx).newopen())
	{
		cocos2d::extension::UIImageView * serverNewOpen = cocos2d::extension::UIImageView::create();
		serverNewOpen->loadTexture("res_ui/select_the_sercer/new.png");
		serverNewOpen->setVisible(true);
		serverNewOpen->setPosition(ccp(181,13));
		serverNewOpen->setAnchorPoint(ccp(0.5f, 0.5f));
		sprite->addChild(serverNewOpen);
	}

	if(1 == m_pServerListRsp->serverinfos(idx).has_roleid())
	{
		int profession = m_pServerListRsp->serverinfos(idx).roleprofession();
		std::string headPicture = BasePlayer::getSmallHeadPathByProfession(profession);

		CCSprite * serverIcon = CCSprite::create();
		serverIcon->initWithFile(headPicture.c_str());

		serverIcon->setVisible(true);
		serverIcon->setPosition(ccp(0,2));
		sprite->addCCNode(serverIcon);
		
		CCLabelTTF *labelName = CCLabelTTF::create(m_pServerListRsp->serverinfos(idx).rolename().c_str(), APP_FONT_NAME, 16);
		labelName->setColor(ccc3(47,93,13));
		labelName->setPosition(ccp(25, 0));
		labelName->setAnchorPoint(ccp(0, 0.5f));
		sprite->addCCNode(labelName);
		
		char roleLevelArray[8];
        //itoa(m_pServerListRsp->serverinfos(idx).rolelevel(), roleLevelArray, 8);
        sprintf(roleLevelArray,"LV%d",m_pServerListRsp->serverinfos(idx).rolelevel());
		CCLabelTTF *label = CCLabelTTF::create(roleLevelArray, APP_FONT_NAME, 14);
		label->setColor(ccc3(47,93,13));
		label->setPosition(ccp(145, -1));
		label->setAnchorPoint(ccp(0.5f, 0.5f));
		sprite->addCCNode(label);
		
	}
	else
	{
		CCLabelTTF *labelName = CCLabelTTF::create(StringDataManager::getString("NoneRole"), APP_FONT_NAME, 16);
		labelName->setColor(ccc3(47,93,13));
		labelName->setPosition(ccp(25, 0));
		labelName->setAnchorPoint(ccp(0, 0.5f));
		sprite->addCCNode(labelName);
	}
}

void LoginLayer::ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent)
{

}

void LoginLayer::tableCellHighlight(CCTableView* table, CCTableViewCell* cell)
{
    // please set cell's content size
	cell->setContentSize(tableCellSizeForIndex(table, cell->getIdx()));

    // highlight sprite
	CCScale9Sprite* pHighlightSpr = CCScale9Sprite::create(CCRectMake(11, 11, 1, 1) , "gamescene_state/zhujiemian3/renwuduiwu/light_1.png");
	pHighlightSpr->setPreferredSize(cell->getContentSize());
	pHighlightSpr->setAnchorPoint(CCPointZero);
	pHighlightSpr->setPosition(20, 0);
	pHighlightSpr->setTag(55);
	cell->addChild(pHighlightSpr);
}

void LoginLayer::tableCellUnhighlight(CCTableView* table, CCTableViewCell* cell)
{
    cell->removeChildByTag(55);
}

void LoginLayer::tableCellTouched(CCTableView* table, CCTableViewCell* cell)
{
	if(table->getTag() == TABLEVIEW_LAST_TAG)
	{
		//tableViewLast->setSelectedEnable(true);
		//tableViewLast->setSelectedScale9Texture("res_ui/kuang_0_on.png", CCRectMake(7, 7, 1, 1), ccp(20,0));

		isLastSelected = true;
		//tableView->setSelectedEnable(false);
		tableView->reloadData();
		mCurrentServerId = lastServerIndex;
		CCLOG("mCurrentServerId = %d\n",mCurrentServerId);

		DefaultServerLayer::setCurrentServerId(mCurrentServerId);
		GameState* pScene = new DefaultServerState();
		if (pScene)
		{
			pScene->runThisState();
			pScene->release();
		}
	}
	else
	{
		isTableViewTouched = true;
		isLastSelected = false;
		mCurrentServerId = cell->getIdx();
		CCLOG("mCurrentServerId = %d\n",mCurrentServerId);

		//tableView->setSelectedEnable(true);
		//tableView->setSelectedScale9Texture("res_ui/kuang_0_on.png", CCRectMake(7, 7, 1, 1), ccp(20,0));

		//tableViewLast->setSelectedEnable(false);
		tableViewLast->reloadData();
		
		DefaultServerLayer::setCurrentServerId(mCurrentServerId);
		GameState* pScene = new DefaultServerState();
		if (pScene)
		{
			pScene->runThisState();
			pScene->release();
		}
	}
}

CCSize LoginLayer::tableCellSizeForIndex(CCTableView *table, unsigned int idx)
{
    return CCSizeMake(370,56);
}

CCTableViewCell* LoginLayer::tableCellAtIndex(CCTableView *table, unsigned int idx)
{
    CCTableViewCell *cell = table->dequeueCell();

	if(table->getTag() == TABLEVIEW_LAST_TAG)
		idx = lastServerIndex;

    cell = new CCTableViewCell();
    cell->autorelease();
    CCScale9Sprite *sprite = CCScale9Sprite::create("res_ui/kuang_0.png");
    sprite->setAnchorPoint(ccp(0, 0));
	sprite->setPreferredSize(CCSizeMake(370,55));
    sprite->setPosition(ccp(20, 0));
	sprite->setTag(123);
    cell->addChild(sprite);

    CCLabelTTF *label = CCLabelTTF::create(m_pServerListRsp->serverinfos(idx).name().c_str(), APP_FONT_NAME, 18);
	label->setColor(ccc3(47,93,13));
    label->setPosition(ccp(87, 27));
	label->setAnchorPoint(ccp(0, 0.5f));
    cell->addChild(label);
		
	CCSprite * serverStatus = CCSprite::create();
	switch(m_pServerListRsp->serverinfos(idx).status())
	{
		case 1:
			serverStatus->initWithFile("res_ui/select_the_sercer/green_new.png");
			break;
		case 2:
			serverStatus->initWithFile("res_ui/select_the_sercer/yellow_new.png");
			break;
		case 3:
			serverStatus->initWithFile("res_ui/select_the_sercer/red_new.png");
			break;
		case 4:
			serverStatus->initWithFile("res_ui/select_the_sercer/gray_new.png");
			break;
		case 5:
			serverStatus->initWithFile("res_ui/select_the_sercer/gray_new.png");
			break;
		default:
			break;
	}
	serverStatus->setVisible(true);
	serverStatus->setAnchorPoint(ccp(0.5f,0.5f));
	serverStatus->setPosition(ccp(59, 27));
	cell->addChild(serverStatus);

	if(1 == m_pServerListRsp->serverinfos(idx).newopen())
	{
		CCSprite * serverNewOpen = CCSprite::create("res_ui/select_the_sercer/new.png");
		serverNewOpen->setVisible(true);
		serverNewOpen->setPosition(ccp(386,40));
		serverNewOpen->setAnchorPoint(ccp(0.5f, 0.5f));
		serverNewOpen->setTag(NEWSERVER_TAG);
		cell->addChild(serverNewOpen);
	}

	if(1 == m_pServerListRsp->serverinfos(idx).has_roleid())
	{
		if(table->getTag() == TABLEVIEW_LAST_TAG)
			s_iLastServerRoleLevel = m_pServerListRsp->serverinfos(lastServerIndex).rolelevel();

		int profession = m_pServerListRsp->serverinfos(idx).roleprofession();
		std::string headPicture = BasePlayer::getSmallHeadPathByProfession(profession);

		CCSprite * serverIcon = CCSprite::create();
		serverIcon->initWithFile(headPicture.c_str());

		serverIcon->setVisible(true);
		serverIcon->setPosition(ccp(205,29));
		cell->addChild(serverIcon);
		
		CCLabelTTF *labelName = CCLabelTTF::create(m_pServerListRsp->serverinfos(idx).rolename().c_str(), APP_FONT_NAME, 16);
		labelName->setColor(ccc3(47,93,13));
		labelName->setPosition(ccp(230, 27));
		labelName->setAnchorPoint(ccp(0, 0.5f));
		cell->addChild(labelName);
		
		char roleLevelArray[8];
        //itoa(m_pServerListRsp->serverinfos(idx).rolelevel(), roleLevelArray, 8);
        sprintf(roleLevelArray,"LV%d",m_pServerListRsp->serverinfos(idx).rolelevel());
		CCLabelTTF *label = CCLabelTTF::create(roleLevelArray, APP_FONT_NAME, 14);
		label->setColor(ccc3(47,93,13));
		label->setPosition(ccp(350, 26));
		label->setAnchorPoint(ccp(0.5f, 0.5f));
		cell->addChild(label);
		
	}
	else
	{
		CCLabelTTF *labelName = CCLabelTTF::create(StringDataManager::getString("NoneRole"), APP_FONT_NAME, 16);
		labelName->setColor(ccc3(47,93,13));
		labelName->setPosition(ccp(230, 27));
		labelName->setAnchorPoint(ccp(0, 0.5f));
		cell->addChild(labelName);
	}

    return cell;
}

unsigned int LoginLayer::numberOfCellsInTableView(CCTableView *table)
{
	//CCLOG("m_pServerListRsp->serverinfos_size() = %d\n",m_pServerListRsp->serverinfos_size());
	if(table->getTag() == TABLEVIEW_LAST_TAG)
		return 1;
    return m_pServerListRsp->serverinfos_size();
}

void LoginLayer::connectGameServer()
{
	CCLOG("connectGameServer\n");
	if(ClientNetEngine::sharedSocketEngine()->getReadyState() == ClientNetEngine::kStateOpen)
		return;
	ClientNetEngine::sharedSocketEngine()->init(*(GameView::getInstance()));
}

void LoginLayer::requestServerList()
{    
    // test 1
    {
        CCHttpRequest* request = new CCHttpRequest();
        std::string url = Login::s_loginserver_ip;
        url.append(":48688/serverlist");
        request->setUrl(url.c_str());
        request->setRequestType(CCHttpRequest::kHttpPost);
        request->setResponseCallback(this, httpresponse_selector(LoginLayer::onRequestServerListCompleted));

		ServerListReq httpReq;
		httpReq.set_userid(Login::userId);
		httpReq.set_sessionid(Login::sessionId);

		string msgData;
		httpReq.SerializeToString(&msgData);

		request->setRequestData(msgData.c_str(), msgData.size());
        request->setTag("POST");
		//CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
        CCHttpClient::getInstance()->send(request);
        request->release();
    }
}

void LoginLayer::onRequestServerListCompleted(CCHttpClient *sender, CCHttpResponse *response)
{
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag())) 
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed()) 
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    // dump data
	std::string strdata;
    strdata.assign(response->getResponseData()->data(), response->getResponseData()->size());
	m_pServerListRsp->ParseFromString(strdata);

	CCLOG("response.result = %d", m_pServerListRsp->result());
	/*
	CCLOG("m_pServerListRsp->serverinfos().roleid() = %d",m_pServerListRsp->serverinfos(0).roleid());
	CCLOG("response.serverinfos_size = %d", m_pServerListRsp->serverinfos_size());
	CCLOG("m_pServerListRsp->serverinfos(0).has_roleid() = %d",m_pServerListRsp->serverinfos(0).has_roleid());
	CCLOG("m_pServerListRsp->serverinfos(0).roleid() = %lld\n",m_pServerListRsp->serverinfos(0).roleid());*/
	if(1 == m_pServerListRsp->result())
	{
		reloadServerInfo();
	}
	else
	{
		//GameView::getInstance()->showAlertDialog(StringDataManager::getString("login_server_list"));
	}
}

void LoginLayer::reloadServerInfo()
{
	initLastServer(lastServerIndex);
	isTableViewTouched = true;
	isLastSelected = true;
	tableView->reloadData();
	tableViewLast->reloadData();
}

void LoginLayer::update(float dt)
{
	//if(isTableViewTouched)
	{
		CCTableViewCell* cell = tableView->cellAtIndex(mCurrentServerId);
		if(cell != NULL&&isTableViewTouched)
		{
			cell->removeChildByTag(NEWSERVER_TAG);
		
			CCSprite * serverNewOpen = CCSprite::create("res_ui/select_the_sercer/new.png");
			serverNewOpen->setVisible(true);
			serverNewOpen->setPosition(ccp(386,40));
			serverNewOpen->setAnchorPoint(ccp(0.5f, 0.5f));
			serverNewOpen->setTag(NEWSERVER_TAG);
			cell->addChild(serverNewOpen);
		
			isTableViewTouched = false;
		}
		else if(cell == NULL&&!isTableViewTouched)
		{
			isTableViewTouched = true;
		}

		CCTableViewCell* cellLast = tableViewLast->cellAtIndex(0);
		if(cellLast!=NULL&&isLastSelected)
		{
			cellLast->removeChildByTag(NEWSERVER_TAG);

			CCSprite * serverNewOpen = CCSprite::create("res_ui/select_the_sercer/new.png");
			serverNewOpen->setVisible(true);
			serverNewOpen->setPosition(ccp(386,40));
			serverNewOpen->setAnchorPoint(ccp(0.5f, 0.5f));
			serverNewOpen->setTag(NEWSERVER_TAG);
			cellLast->addChild(serverNewOpen);

			isLastSelected = false;
		}
	}
	switch(m_iStatus)
	{
	case kStateStart:
		struct timeval m_sStartTime;
		gettimeofday(&m_sStartTime, NULL);
		m_dStartTime = m_sStartTime.tv_sec*1000 + m_sStartTime.tv_usec/1000;

		m_iStatus = kStateWait;
		break;
	case kStateWait:
		struct timeval m_sEndedTime;
		gettimeofday(&m_sEndedTime, NULL);
		m_dEndedTime = m_sEndedTime.tv_sec*1000 + m_sEndedTime.tv_usec/1000;
		//wait 30 second to convert scene
		if(m_dEndedTime - m_dStartTime >= WAIT_TIME)
		{
			m_iStatus = kStateReload;
		}
		break;
	case kStateReload:
		requestServerList();
		m_iStatus = kStateStart;
		break;
	default:
		break;
	}
}

void LoginLayer::onButtonGetClicked(cocos2d::CCObject *sender)
{
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS

	// sdk需求加入（注销接口）
//    ShareLoginState::logout();

#endif

	//runLogin();
	GameState* pScene = new DefaultServerState();
	if (pScene)
	{
		pScene->runThisState();
		pScene->release();
	}
}

void LoginLayer::onEnterButtonGetClicked(cocos2d::CCObject *sender)
{
	if(4 == m_pServerListRsp->serverinfos(mCurrentServerId).status())
	{
		//GameView::getInstance()->showAlertDialog(StringDataManager::getString("server_status_maintain"));
		GameView::getInstance()->showAlertDialog(m_pServerListRsp->serverinfos(mCurrentServerId).statusvalue().c_str());
		return;
	}
	else if(5 == m_pServerListRsp->serverinfos(mCurrentServerId).status())
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("server_status_willopen"));
		return;
	}

	CCLOG("m_pServerListRsp->serverinfos(0).ip().c_str() = %s \n", m_pServerListRsp->serverinfos(mCurrentServerId).ip().c_str());
	CCLOG("m_pServerListRsp->serverinfos(0).port() = %d \n", m_pServerListRsp->serverinfos(mCurrentServerId).port());
	ClientNetEngine::sharedSocketEngine()->setAddress(m_pServerListRsp->serverinfos(mCurrentServerId).ip().c_str(), m_pServerListRsp->serverinfos(mCurrentServerId).port());

	if(1 == m_pServerListRsp->serverinfos(mCurrentServerId).has_roleid())
	{
		mRoleId = m_pServerListRsp->serverinfos(mCurrentServerId).roleid();

		// tip's background
		CCSize s = CCDirector::sharedDirector()->getVisibleSize();
		CCRect insetRect = CCRectMake(32,15,1,1);
		CCScale9Sprite* backGround = CCScale9Sprite::create("res_ui/kuang_1.png"); 
		backGround->setCapInsets(insetRect);
		backGround->setPreferredSize(CCSizeMake(550, 50));
		backGround->setPosition(ccp(s.width/2, s.height/2));
		backGround->setAnchorPoint(ccp(0.5f, 0.5f));
		backGround->setTag(kTagPreparingBackGround);
		addChild(backGround);

		// show tip
		CCLabelTTF* label = CCLabelTTF::create(StringDataManager::getString("con_enter_game"), "Arial", 25);
		addChild(label);
		label->setTag(kTagPreparingTip);
		label->setAnchorPoint(ccp(0.5f, 0.5f));
		label->setPosition( ccp(s.width/2, s.height/2) );

		CCActionInterval *action = (CCActionInterval*)CCSequence::create
			(
				CCShow::create(),
				CCDelayTime::create(0.5f),
				CCCallFunc::create(this, callfunc_selector(LoginLayer::connectGameServer)), 
				NULL
			);
		label->runAction(action);

		m_state = kStateConnectingGameServer;
		/*
		MyPlayerAIConfig::setAutomaticSkill(0);
		MyPlayerAIConfig::enableRobot(false);
		
		std::vector<ChatCell * >::iterator iter;
		for (iter = GameView::getInstance()->chatCell_Vdata.begin(); iter != GameView::getInstance()->chatCell_Vdata.end(); ++ iter)
		{
			delete * iter;
		}
		GameView::getInstance()->chatCell_Vdata.clear();
		*/
		CCUserDefault::sharedUserDefault()->setIntegerForKey("general_mode",1);

#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS

		// 浮动工具条（显示）
//        ShareLoginState::showFloatView();
#endif

	}
	else
	{
		onRoleListReq();
	}
}


void LoginLayer::onRoleListReq()
{    
    // test 1
    {
        CCHttpRequest* request = new CCHttpRequest();
        std::string url = Login::s_loginserver_ip;
        url.append(":48688/rolelist");
        request->setUrl(url.c_str());
        request->setRequestType(CCHttpRequest::kHttpPost);
        request->setResponseCallback(this, httpresponse_selector(LoginLayer::onRoleListRequestCompleted));

		RoleListReq httpReq;
		httpReq.set_userid(Login::userId);
		httpReq.set_sessionid(Login::sessionId);
		httpReq.set_serverid(m_pServerListRsp->serverinfos(mCurrentServerId).serverid());

		string msgData;
		httpReq.SerializeToString(&msgData);

		request->setRequestData(msgData.c_str(), msgData.size());
        request->setTag("POST");
		//CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
        CCHttpClient::getInstance()->send(request);
        request->release();
    }
}

void LoginLayer::onRoleListRequestCompleted(CCHttpClient *sender, CCHttpResponse *response)
{
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag())) 
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed()) 
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    // dump data
	std::string strdata;
    strdata.assign(response->getResponseData()->data(), response->getResponseData()->size());

	RoleListRsp roleListRsp;
	CCAssert(response->getResponseData()->size() > 0, "should not be empty");
	roleListRsp.ParseFromString(strdata);

	CCLOG("response.result = %d", roleListRsp.result());

	if(1 == roleListRsp.result()&&roleListRsp.roleinfos().size())
	{
		mRoleId = roleListRsp.roleinfos(0).id();

		// tip's background
		CCSize s = CCDirector::sharedDirector()->getVisibleSize();
		CCRect insetRect = CCRectMake(32,15,1,1);
		CCScale9Sprite* backGround = CCScale9Sprite::create("res_ui/kuang_1.png"); 
		backGround->setCapInsets(insetRect);
		backGround->setPreferredSize(CCSizeMake(550, 50));
		backGround->setPosition(ccp(s.width/2, s.height/2));
		backGround->setAnchorPoint(ccp(0.5f, 0.5f));
		backGround->setTag(kTagPreparingBackGround);
		addChild(backGround);

		// show tip
		CCLabelTTF* label = CCLabelTTF::create(StringDataManager::getString("con_enter_game"), "Arial", 25);
		addChild(label);
		label->setTag(kTagPreparingTip);
		label->setAnchorPoint(ccp(0.5f, 0.5f));
		label->setPosition( ccp(s.width/2, s.height/2) );

		CCActionInterval *action = (CCActionInterval*)CCSequence::create
			(
				CCShow::create(),
				CCDelayTime::create(0.5f),
				CCCallFunc::create(this, callfunc_selector(LoginLayer::connectGameServer)), 
				NULL
			);
		label->runAction(action);

		m_state = kStateConnectingGameServer;

		MyPlayerAIConfig::setAutomaticSkill(0);
		MyPlayerAIConfig::enableRobot(false);
	}
	else
	{
		onMenuGetTestClicked();
	}
}

void LoginLayer::onMenuGetTestClicked()
{    
    // test 1
    {
        CCHttpRequest* request = new CCHttpRequest();
        std::string url = Login::s_loginserver_ip;
        url.append(":48688/countryprofession");
        request->setUrl(url.c_str());
        request->setRequestType(CCHttpRequest::kHttpPost);
        request->setResponseCallback(this, httpresponse_selector(LoginLayer::onHttpRequestCompleted));

		CountryProfessionReq httpReq;
		httpReq.set_userid(Login::userId);
		httpReq.set_sessionid(Login::sessionId);
		httpReq.set_serverid(m_pServerListRsp->serverinfos(mCurrentServerId).serverid());

		string msgData;
		httpReq.SerializeToString(&msgData);

		request->setRequestData(msgData.c_str(), msgData.size());
        request->setTag("POST");
		//CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
        CCHttpClient::getInstance()->send(request);
        request->release();
    }
}

void LoginLayer::onHttpRequestCompleted(CCHttpClient *sender, CCHttpResponse *response)
{
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag())) 
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed()) 
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    // dump data
	std::string strdata;
    strdata.assign(response->getResponseData()->data(), response->getResponseData()->size());

	CountryProfessionRsp countryProfessionRsp;
	CCAssert(response->getResponseData()->size() > 0, "should not be empty");
	countryProfessionRsp.ParseFromString(strdata);

	CCLOG("response.result = %d", countryProfessionRsp.result());

	if(1 == countryProfessionRsp.result())
	{
		mCountryMin = countryProfessionRsp.country();
		mProfessionMin = countryProfessionRsp.profession();

		GameState* pScene = new CreateRoleState();
		if (pScene)
		{
			pScene->runThisState();
			pScene->release();
		}
	}
	else
	{
		GameView::getInstance()->showAlertDialog(countryProfessionRsp.resultmessage());
	}
}


void LoginLayer::menuSelectRoleCallback(CCObject* pSender)
{

	CCNode* button = (CCNode*)pSender;
	mSelectedId = button->getTag();

	//
	// changet to kStateConnectingGameServer
	//
	// disable role list
	for(int i = 0; i < MAX_LOGIN_ROLE_NUM; i++)
	{
		CCMenu* menu = (CCMenu*)this->getChildByTag(kTagRoleListMenu+i);
		menu->setEnabled(false);
	}
	
	// tip's background
	CCSize s = CCDirector::sharedDirector()->getVisibleSize();
	CCRect insetRect = CCRectMake(32,15,1,1);
	CCScale9Sprite* backGround = CCScale9Sprite::create("res_ui/kuang_1.png"); 
	backGround->setCapInsets(insetRect);
	backGround->setPreferredSize(CCSizeMake(550, 50));
	backGround->setPosition(ccp(s.width/2, s.height/2));
	backGround->setAnchorPoint(ccp(0.5f, 0.5f));
	backGround->setTag(kTagPreparingBackGround);
	addChild(backGround);

	// show tip
	CCLabelTTF* label = CCLabelTTF::create(StringDataManager::getString("con_enter_game"), "Arial", 25);
	addChild(label);
	label->setTag(kTagPreparingTip);
	label->setAnchorPoint(ccp(0.5f, 0.5f));
	label->setPosition( ccp(s.width/2, s.height/2) );

    CCActionInterval *action = (CCActionInterval*)CCSequence::create
        (
            CCShow::create(),
			CCDelayTime::create(0.5f),
            CCCallFunc::create(this, callfunc_selector(LoginLayer::connectGameServer)), 
            NULL
        );
	label->runAction(action);

	m_state = kStateConnectingGameServer;
}

void LoginLayer::reconnectServer()
{
	if(GameView::getInstance()->reconnectGameServer)
		return;
	ServerListRsp* m_pServerListRsp = new ServerListRsp();
	Login::getServerListInfo(m_pServerListRsp);
	mRoleId = m_pServerListRsp->serverinfos(mCurrentServerId).roleid();

	CCScene* runningScene = CCDirector::sharedDirector()->getRunningScene();

	// tip's background
	CCSize s = CCDirector::sharedDirector()->getVisibleSize();
	CCRect insetRect = CCRectMake(32,15,1,1);
	CCScale9Sprite* backGround = CCScale9Sprite::create("res_ui/kuang_1.png"); 
	backGround->setCapInsets(insetRect);
	backGround->setPreferredSize(CCSizeMake(550, 50));
	backGround->setPosition(ccp(s.width/2, s.height/2));
	backGround->setAnchorPoint(ccp(0.5f, 0.5f));
	backGround->setTag(kTagPreparingBackGround);
	backGround->setVisible(false);
	runningScene->addChild(backGround);

	// show tip
	CCLabelTTF* label = CCLabelTTF::create(StringDataManager::getString("con_enter_game"), "Arial", 25);
	runningScene->addChild(label);
	label->setTag(kTagPreparingTip);
	label->setVisible(false);
	label->setAnchorPoint(ccp(0.5f, 0.5f));
	label->setPosition( ccp(s.width/2, s.height/2) );

    CCActionInterval *action = (CCActionInterval*)CCSequence::create
        (
            //CCShow::create(),
			CCDelayTime::create(0.5f),
            CCCallFunc::create(runningScene, callfunc_selector(LoginLayer::connectGameServer)), 
            NULL
        );
	label->runAction(action);

	CCUserDefault::sharedUserDefault()->setIntegerForKey("general_mode",1);

#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS

	// 浮动工具条（显示）
//    ShareLoginState::showFloatView();
#endif

}

void LoginLayer::onSocketOpen(ClientNetEngine* socketEngine)
{
	// request to enter game
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1001, this);
	
	GameState* pScene = new GameSceneState();
	if (pScene)
	{
		pScene->runThisState();
		pScene->release();
	}
}
void LoginLayer::onSocketClose(ClientNetEngine* socketEngine)
{
	//GameView::getInstance()->showAlertDialog(StringDataManager::getString("con_reconnection"));

	// player can re-connect
	if(m_state == kStateConnectingGameServer)
	{
		this->getChildByTag(kTagPreparingBackGround)->removeFromParent();
		this->getChildByTag(kTagPreparingTip)->removeFromParent();
		// enable role list
		/*for(int i = 0; i < MAX_LOGIN_ROLE_NUM; i++)
		{
			CCMenu* menu = (CCMenu*)this->getChildByTag(kTagRoleListMenu+i);
			menu->setEnabled(true);
		}
		*/
		m_state = kStateRoleList;
	}
}
void LoginLayer::onSocketError(ClientNetEngine* socketEngines, const ClientNetEngine::ErrorCode& error)
{
}

///////////////////////////////////////////////////////////////////////////////////////

enum {
	kTagMainLayer = 1,
};

LoginState::~LoginState()
{
}

void LoginState::runThisState()
{
    CCLayer* pLayer = new LoginLayer();
    addChild(pLayer, 0, kTagMainLayer);
	pLayer->release();

	if(CCDirector::sharedDirector()->getRunningScene() != NULL)
		CCDirector::sharedDirector()->replaceScene(this);
	else
		CCDirector::sharedDirector()->runWithScene(this);
}

void LoginState::onSocketOpen(ClientNetEngine* socketEngine)
{
	LoginLayer* layer = (LoginLayer*)this->getChildByTag(kTagMainLayer);
	layer->onSocketOpen(socketEngine);
}
void LoginState::onSocketClose(ClientNetEngine* socketEngine)
{
	//GameView::getInstance()->showAlertDialog(StringDataManager::getString("con_reconnection"));

	LoginLayer* layer = (LoginLayer*)this->getChildByTag(kTagMainLayer);
	layer->onSocketClose(socketEngine);
}
void LoginState::onSocketError(ClientNetEngine* socketEngines, const ClientNetEngine::ErrorCode& error)
{
}

void LoginState::addAppVersionInfo(CCNode* parent)
{
    CCSize s = CCDirector::sharedDirector()->getVisibleSize();

	// game code version
	ccColor3B blackColor = ccc3(0,0,0);   // black
	char appVerStr[20];
	sprintf(appVerStr, "%s: %s", StringDataManager::getString("sys_app_version"), GAME_CODE_VERSION);
	
	// game res version
	int resVersion = CCUserDefault::sharedUserDefault()->getIntegerForKey(KEY_OF_RES_VERSION, GAME_RES_VERSION);
	char resVerStr[50];
	sprintf(resVerStr, "%s.%d", appVerStr, resVersion);

	CCLabelTTF * labelVersion = CCLabelTTF::create(resVerStr, APP_FONT_NAME, 18);
	labelVersion->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, blackColor);
	labelVersion->setAnchorPoint(ccp(0, 1));
	labelVersion->setPosition(ccp(5, s.height-5));
	parent->addChild(labelVersion);
}
