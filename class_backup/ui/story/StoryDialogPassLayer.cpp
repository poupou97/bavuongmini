#include "StoryDialogPassLayer.h"
#include "../../legend_script/ScriptManager.h"
#include "../../GameView.h"

StoryDialogPassLayer::StoryDialogPassLayer()
	:m_pSpriteSkip(NULL)
	,m_pSpriteBg(NULL)
{
	
}

StoryDialogPassLayer::~StoryDialogPassLayer()
{

}

StoryDialogPassLayer* StoryDialogPassLayer::create()
{
    StoryDialogPassLayer *pDialogPassLayer = new StoryDialogPassLayer();
    if (pDialogPassLayer && pDialogPassLayer->init())
    {
        pDialogPassLayer->autorelease();
        return pDialogPassLayer;
    }
    CC_SAFE_DELETE(pDialogPassLayer);
    return NULL;
}

bool StoryDialogPassLayer::init()
{
	if(CCLayer::init())
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

		// 基础layer
		UILayer * pBaseLayer = UILayer::create();
		pBaseLayer->setAnchorPoint(ccp(0.5f, 0.5f));
		pBaseLayer->setContentSize(winSize);
		pBaseLayer->setPosition(CCPointZero);
		addChild(pBaseLayer);

		// skip sprite
		m_pSpriteSkip = CCSprite::create("res_ui/dianji_zi.png");

		m_pSpriteBg = CCSprite::create("res_ui/moji_4.png");
		m_pSpriteBg->setScaleX(1.3f);
		m_pSpriteBg->setScaleY(1.6f);
		m_pSpriteBg->setAnchorPoint(CCPointZero);
		m_pSpriteBg->setPosition((ccp(winSize.width - m_pSpriteSkip->getContentSize().width - 22 - 10, winSize.height - m_pSpriteSkip->getContentSize().height - 15 - 4)));
		addChild(m_pSpriteBg);

		m_pSpriteSkip->setAnchorPoint(CCPointZero);
		m_pSpriteSkip->setPosition((ccp(winSize.width - m_pSpriteSkip->getContentSize().width - 22, winSize.height - m_pSpriteSkip->getContentSize().height - 15)));
		addChild(m_pSpriteSkip);

		
		this->setContentSize(winSize);
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);

		return true;
	}
	return false;
}

void StoryDialogPassLayer::update(float dt)
{

}

bool StoryDialogPassLayer::ccTouchBegan(CCTouch* touch, CCEvent* event)
{
	CCPoint touchpoint = touch->getLocation();		 //获取触摸坐标

	// 触摸坐标 在 skip sprite范围内，则跳过剧情（实现点击图片的效果）
	if (touchpoint.x >  m_pSpriteSkip->getPositionX() - 50 && touchpoint.x < m_pSpriteSkip->getPositionX() + m_pSpriteSkip->getContentSize().width + 50 &&
		touchpoint.y > m_pSpriteSkip->getPositionY() - 50 && touchpoint.y < m_pSpriteSkip->getPositionY() + m_pSpriteSkip->getContentSize().height + 50)
	{
		return true;
	}

	return false;
}

void StoryDialogPassLayer::ccTouchEnded(CCTouch* touch, CCEvent* event)
{
	CCPoint touchpoint = touch->getLocation();		 //获取触摸坐标
	// 触摸坐标 在 skip sprite范围内，则跳过剧情（实现点击图片的效果）
	if (touchpoint.x >  m_pSpriteSkip->getPositionX() - 50 && touchpoint.x < m_pSpriteSkip->getPositionX() + m_pSpriteSkip->getContentSize().width + 50 &&
		touchpoint.y > m_pSpriteSkip->getPositionY() - 50 && touchpoint.y < m_pSpriteSkip->getPositionY() + m_pSpriteSkip->getContentSize().height + 50)
	{
		ScriptManager::getInstance()->skipScript();
	}
}

void StoryDialogPassLayer::onEnter()
{
	CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this, -127, true);
}

void StoryDialogPassLayer::onExit()
{
	CCDirector::sharedDirector()->getTouchDispatcher()->removeDelegate(this);
}