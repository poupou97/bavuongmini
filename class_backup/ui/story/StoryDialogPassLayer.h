#ifndef _STORY_STORYDIALOGPASSLAYER_H_
#define _STORY_STORYDIALOGPASSLAYER_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

/**
  * 跳过剧情的layer
  */
class StoryDialogPassLayer : public CCLayer
{
public:
	StoryDialogPassLayer();
	~StoryDialogPassLayer();

	static StoryDialogPassLayer* create();
	virtual bool init();

	virtual void update(float dt);

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch* touch, CCEvent* event);
	virtual void ccTouchEnded(CCTouch* touch, CCEvent* event);

public:
	void callBackSpriteSkip(CCObject* obj);																	// 点击Skip时的响应
	void refreshDelegate();
private:
	CCSprite * m_pSpriteSkip;
	CCSprite * m_pSpriteBg;
};

#endif
