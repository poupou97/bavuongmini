#include "StoryBlackBand.h"
#include "StoryDialogPassLayer.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/GameSceneCamera.h"

StoryBlackBand::StoryBlackBand()
{
	//setTouchEnabled(true);
	//scheduleUpdate();
}

StoryBlackBand::~StoryBlackBand()
{
}

StoryBlackBand* StoryBlackBand::create()
{
    StoryBlackBand *pStoryBlackBand = new StoryBlackBand();
    if (pStoryBlackBand && pStoryBlackBand->init())
    {
        pStoryBlackBand->autorelease();
        return pStoryBlackBand;
    }
    CC_SAFE_DELETE(pStoryBlackBand);
    return NULL;
}

bool StoryBlackBand::init()
{
	if(CCLayer::init())
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

		// black band at the top
		/*CCSprite * sprite_bg_up = CCSprite::create("res_ui/zhezhao_upside.png", CCRectMake(0, 0, winSize.width, STORYBLACKBAND_TOP_HEIGHT));
		sprite_bg_up->setPosition(ccp(0, winSize.height - STORYBLACKBAND_TOP_HEIGHT));
		sprite_bg_up->setAnchorPoint(CCPointZero);
		sprite_bg_up->ignoreAnchorPointForPosition(false);
		addChild(sprite_bg_up);*/


		//// black band at the top
		//CCScale9Sprite * pBgUpSprite = CCScale9Sprite::create(CCRectMake(12, 13, 500, 1) , "res_ui/zhezhao_mo.png");
		//pBgUpSprite->setPreferredSize(CCSize(winSize.width, STORYBLACKBAND_TOP_HEIGHT));
		//pBgUpSprite->setAnchorPoint(CCPointZero);
		//pBgUpSprite->ignoreAnchorPointForPosition(false);
		//pBgUpSprite->setPosition(ccp(0, winSize.height));
		//pBgUpSprite->setScaleY(-1);
		//addChild(pBgUpSprite);


		/*CCLayerColor *background = CCLayerColor::create(ccc4(0,0,0,255), s.width, STORYBLACKBAND_TOP_HEIGHT);
		background->setPosition(ccp(0,s.height-STORYBLACKBAND_TOP_HEIGHT));
		addChild(background);*/


		
		//CCParticleSystem* particleEffect = CCParticleSystemQuad::create("animation/texiao/particledesigner/xingxingqunti.plist");
		//particleEffect->setPositionType(kCCPositionTypeFree);
		//particleEffect->setPosition(ccp(0, STORYBLACKBAND_TOP_HEIGHT));
		//particleEffect->setAnchorPoint(ccp(0.5f,0.5f));
		//particleEffect->setVisible(true);
		//particleEffect->setGravity(ccp(0,-100));
		//particleEffect->setScaleX(1.5f);
		//background->addChild(particleEffect, 1);

		// black band at the bottom
		/*CCSprite * sprite_bg_bottom = CCSprite::create("res_ui/zhezhao_below.png", CCRectMake(0, 0, winSize.width, STORYBLACKBAND_HEIGHT));
		sprite_bg_bottom->setPosition(CCPointZero);
		sprite_bg_bottom->setAnchorPoint(CCPointZero);
		sprite_bg_bottom->ignoreAnchorPointForPosition(false);
		addChild(sprite_bg_bottom);*/

		// black band at the bottom
		CCScale9Sprite * pBgBottomSprite = CCScale9Sprite::create(CCRectMake(12, 13, 500, 1) , "res_ui/zhezhao_mo.png");
		pBgBottomSprite->setOpacity(192);
		pBgBottomSprite->setPreferredSize(CCSize(winSize.width, STORYBLACKBAND_HEIGHT));
		pBgBottomSprite->setAnchorPoint(CCPointZero);
		pBgBottomSprite->ignoreAnchorPointForPosition(false);
		//pBgBottomSprite->setPosition(CCPointZero);
		pBgBottomSprite->setPosition(ccp(0, 0 - STORYBLACKBAND_HEIGHT));
		addChild(pBgBottomSprite);
		


		/*background = CCLayerColor::create(ccc4(0,0,0,255), s.width, STORYBLACKBAND_HEIGHT);
		background->setPosition(ccp(0,0));
		addChild(background);*/
		
		//particleEffect = CCParticleSystemQuad::create("animation/texiao/particledesigner/xingxingqunti.plist");
		//particleEffect->setPositionType(kCCPositionTypeFree);
		//particleEffect->setPosition(ccp(0.5f,0.5f));
		//particleEffect->setAnchorPoint(ccp(0.5f,0.5f));
		//particleEffect->setVisible(true);
		//particleEffect->setGravity(ccp(0,100));
		//particleEffect->setScaleX(1.5f);
		//background->addChild(particleEffect, 1);
		// 

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		
		return true;
	}
	return false;
}

void StoryBlackBand::update(float dt)
{

}

bool StoryBlackBand::ccTouchBegan(CCTouch* touch, CCEvent* event)
{
	// 在剧情模式时，拦截触摸事件，不让玩家点击游戏场景
	return true;
}

void StoryBlackBand::endActionCallBack(CCObject * object)
{
	CCNode* pNode = (CCNode *)object;
	if (NULL != pNode)
	{
		pNode->removeFromParent();
	}

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if (NULL != scene)
	{
		scene->getSceneCamera()->FollowActor(NULL);
	}
}
