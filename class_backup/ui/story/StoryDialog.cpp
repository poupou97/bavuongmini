#include "StoryDialog.h"

#include "../extensions/CCRichLabel.h"
#include "GameView.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "StoryBlackBand.h"
#include "AppMacros.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/command/MovieScripts.h"
#include "../../utils/GameConfig.h"

#define STORYDIALOG_SIMPLE_DIALOG_TAG 101
#define STORYDIALOG_SIMPLE_DIALOG_FADEIN_TAG 102

std::string StoryDialog::s_lastName = "";

StoryDialog::StoryDialog()
{
	//scheduleUpdate();
}

StoryDialog::~StoryDialog()
{
}

StoryDialog* StoryDialog::create(const char* content, const char* headFigure, int headPosition, const char* roleName)
{
    StoryDialog *pDialog = new StoryDialog();
    if (pDialog && pDialog->init(content, headFigure, headPosition, roleName))
    {
        pDialog->autorelease();
        return pDialog;
    }
    CC_SAFE_DELETE(pDialog);
    return NULL;
}

bool StoryDialog::init(const char* content, const char* headFigure, int headPosition, const char* roleName)
{
	if(CCLayer::init())
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

		const int blackBandHeight = STORYBLACKBAND_HEIGHT;
		const int headIconWidth = 166;
		const int roleNameHeight = 26;
		const int headIconY = 45;
		const int nOffsetY = 7;
		const int nLabelOffsetX = 10;
		const int nHeadOffsetY = 30;   // 105

		// dialog content
		/*CCRichLabel* pRichLabel = CCRichLabel::createWithString(content, CCSizeMake(winSize.width - headIconWidth, blackBandHeight), NULL, NULL);
		addChild(pRichLabel);*/
		CCRichLabel * pRichLabel = NULL;

		// name
		CCLabelTTF* pNameLabel = NULL;

		// name_di
		CCSprite* pSpriteNameFrame = CCSprite::create("res_ui/huabian_a.png");
		pSpriteNameFrame->ignoreAnchorPointForPosition(false);
		pSpriteNameFrame->setAnchorPoint(ccp(0.5f, 0));

		// head icon
		CCSprite* pHeadSprite = NULL;
		std::string currentName = "";
		int profession = GameView::getInstance()->myplayer->getProfession();
		if(strcmp(headFigure, STORY_HERO_ID) == 0)
		{
			// MyPlayer's head icon
			pHeadSprite = CCSprite::create(BasePlayer::getHalfBodyPathByProfession(profession).c_str());
			if(pHeadSprite == NULL)
			{
				pHeadSprite = CCSprite::create();   // it's hero
			}
				
			// role name
			//const char* name = GameView::getInstance()->myplayer->getActorName().c_str();
			const char* name = GameView::getInstance()->myplayer->getActiveRole()->rolebase().name().c_str();
			pNameLabel = CCLabelTTF::create(name, APP_FONT_NAME, 20);
			currentName = name;
			//pNameLabel = CCLabelTTF::create("hero", APP_FONT_NAME, 20);   // test code
			pNameLabel->setColor(ccc3(234, 222, 0));
			
		}
		else if(strcmp(headFigure, STORY_HEROINE_ID) == 0)
		{
			// test code
			//profession = PROFESSION_GM_INDEX;

			// head icon
			std::string figureName = StoryDialog::getHeroineFigureName(profession);
			pHeadSprite = createHeadSprite(figureName.c_str());

			// role name
			std::string name =  StoryDialog::getHeroineName(profession);
			pNameLabel = CCLabelTTF::create(name.c_str(), APP_FONT_NAME, 20);
			currentName = name;
			pNameLabel->setColor(ccc3(234, 222, 0));
			
		}
		else
		{
			// head icon
			pHeadSprite = createHeadSprite(headFigure);

			// role name
			pNameLabel = CCLabelTTF::create(roleName, APP_FONT_NAME, 20);
			currentName = roleName;
			pNameLabel->setColor(ccc3(234, 222, 0));
			
		}

		// dialog content
		int nWidthRichLabel = winSize.width - (nLabelOffsetX + headIconWidth);
		pRichLabel = CCRichLabel::createWithString(content,  CCSizeMake(nWidthRichLabel, blackBandHeight), NULL, NULL, 0, 20.f, 3);
		addChild(pRichLabel);
		
		//CCAssert(pSprite != NULL, "should not be nil");
		float headSpritePosX = 0;
		float headSpritePosY = 0;
        if(pHeadSprite != NULL)
		{
			pHeadSprite->setVisible(false);
			addChild(pHeadSprite);

			const float ACTION_MOVE_DISTANCE = 20;
			if(headPosition == head_left) 
			{
				headSpritePosX = (headIconWidth - pHeadSprite->getContentSize().width) / 2;
				headSpritePosY = nHeadOffsetY;
				pHeadSprite->setAnchorPoint(ccp(0,0));
				pHeadSprite->setPosition(ccp(headSpritePosX-ACTION_MOVE_DISTANCE, headSpritePosY));

				addChild(pSpriteNameFrame);

				// name
				pNameLabel->setAnchorPoint(ccp(0.5f, 0));
				pNameLabel->setPosition(ccp(headIconWidth/2, 5));
				addChild(pNameLabel);

				// name_di
				pSpriteNameFrame->setPosition(ccp(pNameLabel->getPositionX() + 6, 
					pNameLabel->getPositionY() - (pSpriteNameFrame->getContentSize().height - pNameLabel->getContentSize().height) / 2 + 5));
			}
			else
			{
				headSpritePosX = winSize.width - headIconWidth + (headIconWidth - pHeadSprite->getContentSize().width) / 2;
				headSpritePosY = nHeadOffsetY;
				pHeadSprite->setAnchorPoint(ccp(0, 0));
				pHeadSprite->setPosition(ccp(headSpritePosX+ACTION_MOVE_DISTANCE, headSpritePosY));

				addChild(pSpriteNameFrame);

				// name
				pNameLabel->setAnchorPoint(ccp(0.5f, 0));
				pNameLabel->setPosition(ccp(winSize.width - headIconWidth/2, 5));
				addChild(pNameLabel);

				//name_di
				pSpriteNameFrame->setPosition(ccp(pNameLabel->getPositionX() + 6, 
					pNameLabel->getPositionY() - (pSpriteNameFrame->getContentSize().height - pNameLabel->getContentSize().height) / 2 + 5));
			}
			if(StoryDialog::s_lastName == currentName)
			{
				pHeadSprite->setVisible(true);
				pHeadSprite->setPosition(ccp(headSpritePosX, headSpritePosY));
			}
			else
			{
				CCFiniteTimeAction* move_action = CCSpawn::create(
					CCMoveTo::create(0.2f, ccp(headSpritePosX, headSpritePosY)),
					CCFadeIn::create(0.2f),
					NULL);
				CCAction* action = CCSequence::create(
					CCDelayTime::create(0.05f),
					CCShow::create(),
					move_action,
					NULL);
				pHeadSprite->runAction(action);
			}

			// head icon 遮罩层
			//CCSprite * pHeadZheZhaoSprite = CCSprite::create("res_ui/zhezhao_role.png", CCRectMake(0, 0, 116, 16));
			/*CCSprite * pHeadZheZhaoSprite = CCSprite::create("res_ui/zhezhao_role.png", CCRectMake(0, 0, pHeadSprite->getContentSize().width, 16));
			pHeadZheZhaoSprite->setAnchorPoint(CCPointZero);
			pHeadZheZhaoSprite->ignoreAnchorPointForPosition(false);
			pHeadZheZhaoSprite->setPosition(ccp(headSpritePosX, headSpritePosY));
			addChild(pHeadZheZhaoSprite);*/
		}
		StoryDialog::s_lastName = currentName;

		if(headPosition == head_left) 
		{
			int _offsetY = STORYBLACKBAND_HEIGHT - 2 * nOffsetY;
			//pNameLabel->setAnchorPoint(ccp(0, 0));
			//pNameLabel->setPosition(ccp(headIconWidth, _offsetY - pNameLabel->getContentSize().height));

			pRichLabel->setAnchorPoint(ccp(0,0));
			//pRichLabel->setPosition(ccp(headIconWidth, pNameLabel->getPositionY() - pRichLabel->getContentSize().height));
			pRichLabel->setPosition(ccp(headIconWidth, _offsetY - pRichLabel->getContentSize().height));
		}
		else 
		{
			int _offsetY = STORYBLACKBAND_HEIGHT - 2 * nOffsetY;
			//pNameLabel->setAnchorPoint(ccp(0, 0));
			//pNameLabel->setPosition(ccp(winSize.width - headIconWidth - pNameLabel->getContentSize().width, _offsetY - pNameLabel->getContentSize().height));

			pRichLabel->setAnchorPoint(ccp(0,0));
			//pRichLabel->setPosition(ccp(winSize.width - headIconWidth - pRichLabel->getContentSize().width, pNameLabel->getPositionY() - pRichLabel->getContentSize().height));
			pRichLabel->setPosition(ccp(winSize.width - headIconWidth - pRichLabel->getContentSize().width, _offsetY - pRichLabel->getContentSize().height));
		}

		// 基础layer
		UILayer * pBaseLayer = UILayer::create();
		pBaseLayer->setAnchorPoint(ccp(0.5f, 0.5f));
		pBaseLayer->setContentSize(winSize);
		pBaseLayer->setPosition(CCPointZero);
		addChild(pBaseLayer);

		/******************************绿色小三角暂且去掉（改为用小手）***********************************************/
		//// 绿色的小三角
		//CCSprite * pTrangleSprite = CCSprite::create("res_ui/triangle_green.png");
		//pTrangleSprite->setAnchorPoint(CCPointZero);
		//pTrangleSprite->ignoreAnchorPointForPosition(false);
		//pTrangleSprite->setPosition(ccp(pRichLabel->getPositionX() + pRichLabel->getEndPos().x, pRichLabel->getPositionY() + pRichLabel->getEndPos().y - 2));
		//addChild(pTrangleSprite);

		//// 上下动的 效果
		//CCMoveBy * pActionMoveByDown = CCMoveBy::create(1.0f, ccp(0, 8));
		//CCMoveBy * pActionMoveByUp = CCMoveBy::create(1.0f, ccp(0, -8)); 
		//CCEaseSineInOut * pActionExpoIn = CCEaseSineInOut::create(pActionMoveByDown);
		//CCEaseSineInOut * pActionExpoOut = CCEaseSineInOut::create(pActionMoveByUp);
		//CCSequence * pActionSequence = CCSequence::createWithTwoActions(pActionExpoIn, pActionExpoOut);
		//CCRepeatForever * pActionRepeat = CCRepeatForever::create(pActionSequence);
		//pTrangleSprite->runAction(pActionRepeat);
		/*************************************************************************************************************/

		// 小手指引动画
		std::string animFileName = "animation/texiao/renwutexiao/shou/shou.anm";
		CCLegendAnimation *m_pAnim = CCLegendAnimation::create(animFileName);
		m_pAnim->setPlayLoop(true);
		m_pAnim->setPosition(ccp(pRichLabel->getPositionX() + pRichLabel->getEndPos().x, pRichLabel->getPositionY() + pRichLabel->getEndPos().y - 2));
		m_pAnim->setReleaseWhenStop(true);
		this->addChild(m_pAnim);

		// 15秒后自动跳过该剧情
		const float AUTO_SKIP_TIME = 15.0f;

		CCActionInterval * action =(CCActionInterval *)CCSequence::create(
			CCDelayTime::create(AUTO_SKIP_TIME),
			CCCallFunc::create(this, callfunc_selector(StoryDialog::skipDialog)),
			NULL);
		this->runAction(action);

		this->setContentSize(winSize);
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);

		return true;
	}
	return false;
}

StoryDialog* StoryDialog::create(const char* content, float fadeInTime)
{
    StoryDialog *pDialog = new StoryDialog();
    if (pDialog && pDialog->init(content, fadeInTime))
    {
        pDialog->autorelease();
        return pDialog;
    }
    CC_SAFE_DELETE(pDialog);
    return NULL;
}

bool StoryDialog::init(const char* content, float fadeInTime)
{
	if(CCLayer::init())
	{
		CCSize s = CCDirector::sharedDirector()->getVisibleSize();

		const int dialogHeight = 30;

		// dialog content
		CCRichLabel* pRichLabel = CCRichLabel::createWithString(content, CCSizeMake(s.width, dialogHeight), NULL, NULL, 0, 30.f);
		pRichLabel->setAnchorPoint(ccp(0.5f,0.5f));
		pRichLabel->setPosition(ccp(s.width/2, s.height/2));

		pRichLabel->setOpacity(0);
		CCFadeIn *fadeInAction = CCFadeIn::create(fadeInTime);
		fadeInAction->setTag(STORYDIALOG_SIMPLE_DIALOG_FADEIN_TAG);
		pRichLabel->runAction(fadeInAction);

		addChild(pRichLabel,0, STORYDIALOG_SIMPLE_DIALOG_TAG);

		CCActionInterval * action =(CCActionInterval *)CCSequence::create(
				CCDelayTime::create(fadeInTime + 2.0f),
				CCCallFunc::create(this, callfunc_selector(StoryDialog::skipDialog)),
				NULL);
		this->runAction(action);

		this->setContentSize(s);
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);

		return true;
	}
	return false;
}

void StoryDialog::update(float dt)
{

}

void StoryDialog::skipDialog()
{
	// dialog is over
	Script* sc = ScriptManager::getInstance()->getScriptById(m_pDialogScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(this);

	this->removeFromParent();
}

bool StoryDialog::ccTouchBegan(CCTouch* touch, CCEvent* event)
{
	return true;
}

void StoryDialog::ccTouchEnded(CCTouch* touch, CCEvent* event)
{
	// 当黑屏字，正在淡入时，不响应触屏
	CCNode* pSimpleDiaglogNode = this->getChildByTag(STORYDIALOG_SIMPLE_DIALOG_TAG);
	if(pSimpleDiaglogNode != NULL)
	{
		if(pSimpleDiaglogNode->getActionByTag(STORYDIALOG_SIMPLE_DIALOG_FADEIN_TAG) != NULL)
			return;
	}

	// dialog is over
	skipDialog();
}

void StoryDialog::registerScriptCommand(int scriptId)
{
	m_pDialogScriptInstanceId = scriptId;
}

std::string StoryDialog::getHeroineName(int profession)
{
	const char* _id = NULL;
	if(profession == PROFESSION_MJ_INDEX)
	{
		_id = "heroine_1_name";
	}
	else if (profession == PROFESSION_GM_INDEX)
	{
		_id = "heroine_2_name";
	}
	else if (profession == PROFESSION_HJ_INDEX)
	{
		_id = "heroine_3_name";
	}
	else if (profession == PROFESSION_SS_INDEX)
	{
		_id = "heroine_4_name";
	}
	CCAssert(_id != NULL, "wrong data");
	return GameConfig::getStringForKey(_id);
}
std::string StoryDialog::getHeroineFigureName(int profession)
{
	const char* _id = NULL;
	if(profession == PROFESSION_MJ_INDEX)
	{
		_id = "heroine_1_figure";
	}
	else if (profession == PROFESSION_GM_INDEX)
	{
		_id = "heroine_2_figure";
	}
	else if (profession == PROFESSION_HJ_INDEX)
	{
		_id = "heroine_3_figure";
	}
	else if (profession == PROFESSION_SS_INDEX)
	{
		_id = "heroine_4_figure";
	}
	CCAssert(_id != NULL, "wrong data");
	return GameConfig::getStringForKey(_id);
}
std::string StoryDialog::getHeroineAnimName(int profession)
{
	const char* _id = NULL;
	if(profession == PROFESSION_MJ_INDEX)
	{
		_id = "heroine_1_animation";
	}
	else if (profession == PROFESSION_GM_INDEX)
	{
		_id = "heroine_2_animation";
	}
	else if (profession == PROFESSION_HJ_INDEX)
	{
		_id = "heroine_3_animation";
	}
	else if (profession == PROFESSION_SS_INDEX)
	{
		_id = "heroine_4_animation";
	}
	CCAssert(_id != NULL, "wrong data");
	return GameConfig::getStringForKey(_id);
}

CCSprite* StoryDialog::createHeadSprite(const char* headFigure)
{
	CCSprite* pHeadSprite = NULL;

	// head icon
	std::string headFigureName = headFigure;
	int index = headFigureName.find(".png");
	headFigureName = headFigureName.substr(0, index);

	std::string headFigureFileName = "res_ui/";
	headFigureFileName.append(headFigureName);
	headFigureFileName.append(".png");
	pHeadSprite = CCSprite::create(headFigureFileName.c_str());

	//// use big picture
	//std::string headFigureFileName = "res_ui/";
	//headFigureFileName.append(headFigureName);
	//headFigureFileName.append("_big.png");
	//pHeadSprite = CCSprite::create(headFigureFileName.c_str());
	//if(pHeadSprite == NULL)   // no big picture, then use the normal one
	//{
	//	headFigureFileName = "res_ui/";
	//	headFigureFileName.append(headFigureName);
	//	headFigureFileName.append(".png");
	//	pHeadSprite = CCSprite::create(headFigureFileName.c_str());
	//}

	return pHeadSprite;
}