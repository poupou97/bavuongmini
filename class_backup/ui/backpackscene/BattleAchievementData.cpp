#include "BattleAchievementData.h"
#include "../../messageclient/element/CPkDailyReward.h"


BattleAchievementData * BattleAchievementData::s_battleAchData = NULL;

BattleAchievementData::BattleAchievementData(void)
	:m_nRank(0)
	,m_bHasGetGift(false)
{
}


BattleAchievementData::~BattleAchievementData(void)
{
}

BattleAchievementData * BattleAchievementData::instance()
{
	if (NULL == s_battleAchData)
	{
		s_battleAchData = new BattleAchievementData();
	}

	return s_battleAchData;
}

void BattleAchievementData::set_rank( int nRank )
{
	this->m_nRank = nRank;
}

int BattleAchievementData::get_rank()
{
	return this->m_nRank;
}

void BattleAchievementData::set_hasGetGift( bool bFlag )
{
	this->m_bHasGetGift = bFlag;
}

bool BattleAchievementData::get_hasGetGift()
{
	return this->m_bHasGetGift;
}

void BattleAchievementData::initVectorFromInternet()
{
	//// 清空本地数据
	//std::vector<CPkDailyReward *>::iterator iter;
	//for (iter = m_vector_reward.begin(); iter != m_vector_reward.end(); iter++)
	//{
	//	delete *iter;
	//}
	//m_vector_reward.clear();

	//for (unsigned int i = 0; i < m_vector_reward.size(); i++)
	//{
	//	/*ActiveNoteData * activeNote = new ActiveNoteData();
	//	activeNote->m_activeNote = new CActiveNote();
	//	activeNote->m_activeNote->CopyFrom(*m_vector_internet_note.at(i));

	//	m_vector_native_activeNote.push_back(activeNote);*/

	//}
}



