#include "PackageItem.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"
#include "PackageItemInfo.h"
#include "GameView.h"
#include "../Mail_ui/MailUI.h"
#include "AppMacros.h"
#include "../Auction_ui/AuctionUi.h"
#include "../Chat_ui/ChatUI.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../utils/GameUtils.h"
#include "PacPageView.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "GoodsItemInfoBase.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "PackageScene.h"
#include "../../messageclient/element/CDrug.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/equipMent_ui/EquipMentUi.h"
#include "../../ui/Chat_ui/BackPackage.h"
#include "../../messageclient/element/CEquipment.h"
#include "../../messageclient/element/CGemPropertyMap.h"
#include "../../legend_script/CCTeachingGuide.h"

#define voidFramePath "res_ui/di_none.png"

#define whiteFramePath "res_ui/di_white.png"
#define greenFramePath "res_ui/di_green.png"
#define blueFramePath "res_ui/di_bule.png"
#define purpleFramePath "res_ui/di_purple.png"
#define orangeFramePath "res_ui/di_orange.png"

#define fontBgPath "res_ui/zidi.png"

#define  MengBan_Red_Name "m_mengban_red"
#define  MengBan_Black_Name "m_mengban_black"
#define  MengBan_Avalible "m_mengban_black"

PackageItem::PackageItem():
index(0),
cur_type(COMMONITEM)
{
	curFolder = new FolderInfo();
}


PackageItem::~PackageItem()
{
	delete curFolder;
}


PackageItem * PackageItem::create(FolderInfo* folder)
{
	PackageItem *widget = new PackageItem();
	if (widget && widget->init(folder))
	{
		widget->autorelease();
		return widget;
	}
	CC_SAFE_DELETE(widget);
	return NULL;
}
bool PackageItem::init(FolderInfo* folder)
{
	if (UIWidget::init())
	{
		curFolder->CopyFrom(*folder);
		index = folder->id();
		ImageView_bound = UIImageView::create();
		if (folder->has_goods() == true)
		{
			cur_type = COMMONITEM;

			/*********************判断颜色***************************/
			std::string frameColorPath;
			if (folder->goods().quality() == 1)
			{
				frameColorPath = whiteFramePath;
			}
			else if (folder->goods().quality() == 2)
			{
				frameColorPath = greenFramePath;
			}
			else if (folder->goods().quality() == 3)
			{
				frameColorPath = blueFramePath;
			}
			else if (folder->goods().quality() == 4)
			{
				frameColorPath = purpleFramePath;
			}
			else if (folder->goods().quality() == 5)
			{
				frameColorPath = orangeFramePath;
			}
			else
			{
				frameColorPath = voidFramePath;
			}

			Btn_pacItemFrame = UIButton::create();
			Btn_pacItemFrame->setTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
			Btn_pacItemFrame->setTouchEnable(true);
// 			Btn_pacItemFrame->setScale9Enable(true);
// 			CCRect cr = CCRect(9.5f,9.5f,6.0f,6.0f);
// 			Btn_pacItemFrame->setCapInsets(cr);
// 			Btn_pacItemFrame->setScale9Size(CCSizeMake(91,84));
			Btn_pacItemFrame->setAnchorPoint(ccp(0.5f,0.5f));
			Btn_pacItemFrame->setPosition(ccp(Btn_pacItemFrame->getContentSize().width/2,Btn_pacItemFrame->getContentSize().height/2));
			Btn_pacItemFrame->addReleaseEvent(this,coco_releaseselector(PackageItem::PackageItemEvent));
			Btn_pacItemFrame->setPressedActionEnabled(true);
			this->addChild(Btn_pacItemFrame);

			ImageView_packageItem = UIImageView::create();
			if (strcmp(folder->goods().icon().c_str(),"") == 0)
			{
				ImageView_packageItem->setTexture("res_ui/props_icon/prop.png");
			}
			else
			{
				std::string _path = "res_ui/props_icon/";
				_path.append(folder->goods().icon().c_str());
				_path.append(".png");
				ImageView_packageItem->setTexture(_path.c_str());
			}
			//ImageView_packageItem->setTexture("res_ui/props_icon/cubuyi.png");
			ImageView_packageItem->setAnchorPoint(ccp(0.5f,0.5f));
			ImageView_packageItem->setPosition(ccp(0,7));   //(ccp(42,43));
			Btn_pacItemFrame->addChild(ImageView_packageItem);

			Lable_name = UILabel::create();
			Lable_name->setText(folder->goods().name().c_str());
			Lable_name->setColor(GameView::getInstance()->getGoodsColorByQuality(folder->goods().quality()));
			Lable_name->setFontName(APP_FONT_NAME);
			Lable_name->setFontSize(12);
			Lable_name->setAnchorPoint(ccp(0.5f,1.0f));
			Lable_name->setPosition(ccp(0,-18));
			Lable_name->setName("Label_name");
			Btn_pacItemFrame->addChild(Lable_name);

			Lable_num = UILabel::create();
			if (folder->quantity() > 1)
			{
				char s_num[5];
				sprintf(s_num,"%d",folder->quantity());
				Lable_num->setText(s_num);
			}
			else
			{
				Lable_num->setText("");
			}
			Lable_num->setFontName(APP_FONT_NAME);
			Lable_num->setFontSize(13);
			Lable_num->setAnchorPoint(ccp(0.0f,1.0f));
			Lable_num->setPosition(ccp(33-Lable_num->getContentSize().width,-3));
			Lable_num->setName("Label_num");
			Btn_pacItemFrame->addChild(Lable_num);

			ImageView_bound->setTexture("res_ui/binding.png");
			ImageView_bound->setAnchorPoint(ccp(1.0f,0.0f));
			ImageView_bound->setPosition(ccp(-18,14));
			ImageView_bound->setVisible(false);
			
			if (folder->goods().binding() == 1)
			{
				Btn_pacItemFrame->addChild(ImageView_bound);
			}
			

			//如果是装备
			if(folder->goods().has_equipmentdetail())
			{
				if (folder->goods().equipmentdetail().gradelevel() > 0)  //精练等级
				{
					std::string ss_gradeLevel = "+";
					char str_gradeLevel [10];
					sprintf(str_gradeLevel,"%d",folder->goods().equipmentdetail().gradelevel());
					ss_gradeLevel.append(str_gradeLevel);
					UILabel *Lable_gradeLevel = UILabel::create();
					Lable_gradeLevel->setText(ss_gradeLevel.c_str());
					Lable_gradeLevel->setFontName(APP_FONT_NAME);
					Lable_gradeLevel->setFontSize(13);
					Lable_gradeLevel->setAnchorPoint(ccp(0.0f,0.0f));
					Lable_gradeLevel->setPosition(ccp(30-Lable_gradeLevel->getContentSize().width,31-Lable_gradeLevel->getContentSize().height));
					Lable_gradeLevel->setName("Lable_gradeLevel");
					Btn_pacItemFrame->addChild(Lable_gradeLevel);
				}

				if (folder->goods().equipmentdetail().starlevel() > 0)  //星级
				{
					char str_starLevel [10];
					sprintf(str_starLevel,"%d",folder->goods().equipmentdetail().starlevel());
					UILabel *Lable_starLevel = UILabel::create();
					Lable_starLevel->setText(str_starLevel);
					Lable_starLevel->setFontName(APP_FONT_NAME);
					Lable_starLevel->setFontSize(13);
					Lable_starLevel->setAnchorPoint(ccp(1.0f,1.0f));
					Lable_starLevel->setPosition(ccp(-34+Lable_starLevel->getContentSize().width,-17+Lable_starLevel->getContentSize().height));
					Lable_starLevel->setName("Lable_starLevel");
					Btn_pacItemFrame->addChild(Lable_starLevel);

					UIImageView *ImageView_star = UIImageView::create();
					ImageView_star->setTexture("res_ui/star_on.png");
					ImageView_star->setAnchorPoint(ccp(1.0f,1.0f));
					ImageView_star->setPosition(ccp(Lable_starLevel->getContentSize().height,-2));
					ImageView_star->setVisible(true);
					ImageView_star->setScale(0.5f);
					Lable_starLevel->addChild(ImageView_star);
				}
			}

			//装备耐久为零
			if (folder->goods().equipmentclazz()>0 && folder->goods().equipmentclazz()<11)
			{
				if (folder->goods().equipmentdetail().durable() > 10)
				{
					this->setRed(false);
				}
				else
				{
					this->setRed(true);
				}
			}

			//如果是宝石
			if (folder->goods().clazz() == GOODS_CLASS_XIANGQIANCAILIAO)
			{
				std::string goodsId = folder->goods().id();
				std::string goodsIcon = getCurGemIconPath(goodsId);

				UIImageView *ImageView_gemPropertyIcon = UIImageView::create();
				ImageView_gemPropertyIcon->setTexture(goodsIcon.c_str());
				ImageView_gemPropertyIcon->setAnchorPoint(ccp(0,0));
				ImageView_gemPropertyIcon->setPosition(ccp(30-ImageView_gemPropertyIcon->getContentSize().width,31-ImageView_gemPropertyIcon->getContentSize().height));
				ImageView_gemPropertyIcon->setVisible(true);
				ImageView_gemPropertyIcon->setName("gemPropertyIcon");
				Btn_pacItemFrame->addChild(ImageView_gemPropertyIcon);
				ImageView_gemPropertyIcon->setVisible(false);
			}
		}
		else
		{
			this->cur_type = VOIDITEM;
			//init(VOIDITEM);
			UIImageView *imageView_background = UIImageView::create();
			imageView_background->setTexture(voidFramePath);
			imageView_background->setAnchorPoint(CCPointZero);
			imageView_background->setPosition(ccp(0,0));
			imageView_background->setName("imageView_background");
			this->addChild(imageView_background);
		}

		CCSprite *progress_skill = CCSprite::create("res_ui/zhezhao_75.png");
		mProgressTimer_skill = CCProgressTimer::create(progress_skill);
		mProgressTimer_skill->setAnchorPoint(ccp(0.5f, 0.5f));
		mProgressTimer_skill->setPosition(ccp(83/2,75/2));
		mProgressTimer_skill->setVisible(false);
		mProgressTimer_skill->setType(kCCProgressTimerTypeRadial);
		mProgressTimer_skill->setReverseProgress(true); // 设置进度条为逆时针
		this->getValidNode()->addChild(mProgressTimer_skill,100);

		this->setTouchEnable(true);
		this->setSize(CCSizeMake(83,75));
		return true;
	}
	return false;
}

PackageItem * PackageItem::create(GoodsInfo* goods)
{
	PackageItem *widget = new PackageItem();
	if (widget && widget->initWithGoods(goods))
	{
		widget->autorelease();
		return widget;
	}
	CC_SAFE_DELETE(widget);
	return NULL;
}

bool PackageItem::initWithGoods(GoodsInfo* goods)
{
	if (UIWidget::init())
	{
		curGood = goods;
		/*********************判断颜色***************************/
		std::string frameColorPath;
		if (goods->quality() == 1)
		{
			frameColorPath = whiteFramePath;
		}
		else if (goods->quality() == 2)
		{
			frameColorPath = greenFramePath;
		}
		else if (goods->quality() == 3)
		{
			frameColorPath = blueFramePath;
		}
		else if (goods->quality() == 4)
		{
			frameColorPath = purpleFramePath;
		}
		else if (goods->quality() == 5)
		{
			frameColorPath = orangeFramePath;
		}
		else
		{
			frameColorPath = voidFramePath;
		}

		Btn_pacItemFrame = UIButton::create();
		Btn_pacItemFrame->setTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
		Btn_pacItemFrame->setTouchEnable(true);
// 		Btn_pacItemFrame->setScale9Enable(true);
// 		CCRect cr = CCRect(9.5f,9.5f,6.0f,6.0f);
// 		Btn_pacItemFrame->setCapInsets(cr);
// 		Btn_pacItemFrame->setScale9Size(CCSizeMake(83,75));
		Btn_pacItemFrame->setAnchorPoint(ccp(0.5f,0.5f));
		Btn_pacItemFrame->setPosition(ccp(Btn_pacItemFrame->getContentSize().width/2,Btn_pacItemFrame->getContentSize().height/2));
		Btn_pacItemFrame->addReleaseEvent(this,coco_releaseselector(PackageItem::GoodItemEvent));
		Btn_pacItemFrame->setPressedActionEnabled(true);
		this->addChild(Btn_pacItemFrame);

		ImageView_packageItem = UIImageView::create();
		if (strcmp(goods->icon().c_str(),"") == 0)
		{
			ImageView_packageItem->setTexture("res_ui/props_icon/prop.png");
		}
		else
		{
			std::string _path = "res_ui/props_icon/";
			_path.append(goods->icon().c_str());
			_path.append(".png");
			ImageView_packageItem->setTexture(_path.c_str());
		}
		ImageView_packageItem->setAnchorPoint(ccp(0.5f,0.5f));
		ImageView_packageItem->setPosition(ccp(0,7));
		Btn_pacItemFrame->addChild(ImageView_packageItem);

		Lable_name = UILabel::create();
		//CCLog("%s",goods->name().c_str());
		Lable_name->setText(goods->name().c_str());
		Lable_name->setFontName(APP_FONT_NAME);
		Lable_name->setFontSize(12);
		Lable_name->setAnchorPoint(ccp(0.5f,1.0f));
		Lable_name->setPosition(ccp(0,-18));
		Lable_name->setName("Label_name");
		Btn_pacItemFrame->addChild(Lable_name);
		Lable_name->setColor(GameView::getInstance()->getGoodsColorByQuality(goods->quality()));

		Lable_num = UILabel::create();
// 		if (goods->availabletime() > 1)
// 		{
// 			char s_num[5];
// 			sprintf(s_num,"%d",goods->availabletime());
// 			Lable_num->setText(s_num);
// 		}
// 		else
// 		{
			Lable_num->setText("");
//		}
		Lable_num->setFontName(APP_FONT_NAME);
		
		Lable_num->setFontSize(13);
		Lable_num->setAnchorPoint(ccp(0,1.0f));
		Lable_num->setPosition(ccp(33-Lable_num->getContentSize().width,-3));
		Lable_num->setName("Label_num");
		Btn_pacItemFrame->addChild(Lable_num);

		ImageView_bound = UIImageView::create();
		ImageView_bound->setTexture("res_ui/binding.png");
		ImageView_bound->setAnchorPoint(ccp(1.0f,0));
		ImageView_bound->setPosition(ccp(-18,14));
		ImageView_bound->setVisible(false);

		//如果是装备
		if(goods->has_equipmentdetail())
		{
			if (goods->equipmentdetail().gradelevel() > 0)  //精练等级
			{
				std::string ss_gradeLevel = "+";
				char str_gradeLevel [10];
				sprintf(str_gradeLevel,"%d",goods->equipmentdetail().gradelevel());
				ss_gradeLevel.append(str_gradeLevel);
				UILabel *Lable_gradeLevel = UILabel::create();
				Lable_gradeLevel->setText(ss_gradeLevel.c_str());
				Lable_gradeLevel->setFontName(APP_FONT_NAME);
				Lable_gradeLevel->setFontSize(13);
				Lable_gradeLevel->setAnchorPoint(ccp(0,0));
				Lable_gradeLevel->setPosition(ccp(30-Lable_gradeLevel->getContentSize().width,31-Lable_gradeLevel->getContentSize().height));
				Lable_gradeLevel->setName("Lable_gradeLevel");
				Btn_pacItemFrame->addChild(Lable_gradeLevel);
			}

			if (goods->equipmentdetail().starlevel() > 0)  //星级
			{
				char str_starLevel [10];
				sprintf(str_starLevel,"%d",goods->equipmentdetail().starlevel());
				UILabel *Lable_starLevel = UILabel::create();
				Lable_starLevel->setText(str_starLevel);
				Lable_starLevel->setFontName(APP_FONT_NAME);
				Lable_starLevel->setFontSize(13);
				Lable_starLevel->setAnchorPoint(ccp(1.0f,1.0f));
				Lable_starLevel->setPosition(ccp(-34+Lable_starLevel->getContentSize().width,-17+Lable_starLevel->getContentSize().height));
				Lable_starLevel->setName("Lable_starLevel");
				Btn_pacItemFrame->addChild(Lable_starLevel);

				UIImageView *ImageView_star = UIImageView::create();
				ImageView_star->setTexture("res_ui/star_on.png");
				ImageView_star->setAnchorPoint(ccp(1.0f,1.0f));
				ImageView_star->setPosition(ccp(Lable_starLevel->getContentSize().height,-2));
				ImageView_star->setVisible(true);
				ImageView_star->setScale(0.5f);
				Lable_starLevel->addChild(ImageView_star);
			}
		}


		if (goods->binding() == 1)
		{
			Btn_pacItemFrame->addChild(ImageView_bound);
		}

		if ((goods->equipmentclazz()>0) && (goods->equipmentclazz()<11))
		{
			if (goods->equipmentdetail().durable() > 10)
			{
				this->setRed(false);
			}
			else
			{
				this->setRed(true);
			}
		}

		this->setTouchEnable(true);
		this->setSize(CCSizeMake(83,75));

		return true;
	}
	return false;
}

PackageItem * PackageItem::createLockedItem(int folderIndex)
{
	PackageItem *widget = new PackageItem();
	if (widget && widget->initLockedItem(folderIndex))
	{
		widget->autorelease();
		return widget;
	}
	CC_SAFE_DELETE(widget);
	return NULL;
}
bool PackageItem::initLockedItem(int folderIndex)
{
	if (UIWidget::init())
	{
		this->setSize(CCSizeMake(83,75));
		this->cur_type = LOCKITEM;
		this->index = folderIndex;

		UIButton * Btn_lockFrame = UIButton::create();
		Btn_lockFrame->setTextures(voidFramePath,voidFramePath,"");
		Btn_lockFrame->setTouchEnable(true);
		Btn_lockFrame->setAnchorPoint(ccp(0.5f,0.5f));
		Btn_lockFrame->setPosition(ccp(Btn_lockFrame->getContentSize().width/2,Btn_lockFrame->getContentSize().height/2));
		Btn_lockFrame->addReleaseEvent(this,coco_releaseselector(PackageItem::BtnLockEvent));
		Btn_lockFrame->setPressedActionEnabled(true);
		this->addChild(Btn_lockFrame);

		//锁定图标
		UIImageView * imageView_lock = UIImageView::create();
		imageView_lock->setTexture("res_ui/suo.png");
		imageView_lock->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_lock->setPosition(ccp(0,0));
		imageView_lock->setName("imageView_lock");
		Btn_lockFrame->addChild(imageView_lock);

		return true;
	}
	return false;
}

void PackageItem::setLabelNameText(char * str)
{
	this->Lable_name->setText(str);
}

void PackageItem::PackageItemEvent( CCObject * pSender )
{
	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(pSender);
	CCLog("PackageItemEvent PackageItemEvent PackageItemEvent");

	if ((bool)isDoubleTouch())
	{
		CCLOG("165435165432165465415");
	}
	
	CCScene* pScene = CCDirector::sharedDirector()->getRunningScene();
	UILayer * uiScene = (UILayer*)pScene->getChildren()->objectAtIndex(0);

	GameView::getInstance()->pacPageView->curFolder = curFolder;
	GameView::getInstance()->pacPageView->curPackageItemIndex = curFolder->id();
	CCSize size= CCDirector::sharedDirector()->getVisibleSize();
// 	EquipMentUi *equip_ui = (EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
// 	if (equip_ui != NULL)
// 	{
// 		if (equip_ui->currType == 2 && curFolder->goods().clazz()== 3)
// 		{
// 			PackageItemInfo * packageItemInfo = PackageItemInfo::create(curFolder,GameView::getInstance()->EquipListItem,0);
// 			packageItemInfo->ignoreAnchorPointForPosition(false);
// 			packageItemInfo->setAnchorPoint(ccp(0.5f,0.5f));
// 			packageItemInfo->setTag(kTagGoodsItemInfoBase);
// 			GameView::getInstance()->getMainUIScene()->addChild(packageItemInfo);
// 		}else
// 		{
// 			equip_ui->addOperationFloder(curFolder);
// 		}
// 		return;
// 	}

	PackageItemInfo * packageItemInfo;
	switch (GameView::getInstance()->pacPageView->getCurUITag())
	{
	case kTagBackpack:
		{
			std::vector<CEquipment *> equipVector;
			PackageScene * package_ = (PackageScene *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
			if (package_ != NULL)
			{
				if (package_->selectActorId != 0)
				{
					for (int i = 0;i<GameView::getInstance()->generalsInLineDetailList.size();i++)
					{
						if (GameView::getInstance()->generalsInLineDetailList.at(i)->generalid() == package_->selectActorId)
						{
							for (int j=0;j<GameView::getInstance()->generalsInLineDetailList.at(i)->equipments_size() ;j++)
							{
								CEquipment * equip_ = new CEquipment();
								equip_->CopyFrom(GameView::getInstance()->generalsInLineDetailList.at(i)->equipments(j));	
								equipVector.push_back(equip_);
							}
						}
					}
					packageItemInfo = PackageItemInfo::create(curFolder,equipVector,package_->selectActorId);

					std::vector<CEquipment *>::iterator iter;
					for (iter = equipVector.begin(); iter!= equipVector.end();iter++)
					{
						delete *iter;
					}
					equipVector.clear();
				}else
				{
					packageItemInfo = PackageItemInfo::create(curFolder,GameView::getInstance()->EquipListItem,package_->selectActorId);
				}
			}
		}break;
	case kTabChat:
		{
			ChatUI * chatui_ = (ChatUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTabChat);
			if (chatui_ != NULL)
			{
				BackPackage * backage_ = (BackPackage *)chatui_->layer_3->getChildByTag(PACKAGEAGE);
				long long generalId = 0;
				if (backage_->curRoleIndex == 0)
				{
					generalId = 0;
					packageItemInfo = PackageItemInfo::create(curFolder,GameView::getInstance()->EquipListItem,0);
				}else
				{
					generalId = GameView::getInstance()->generalsInLineDetailList.at(backage_->curRoleIndex - 1)->generalid();
					packageItemInfo = PackageItemInfo::create(curFolder,backage_->playerEquipments,generalId);
				}
			}
		}break;
	case ktagEquipMentUI:
		{
			EquipMentUi *equip_ui = (EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
			if (equip_ui != NULL)
			{
				PackageItemInfo * packageItemInfo = PackageItemInfo::create(curFolder,equip_ui->generalEquipVector,equip_ui->mes_petid);
				packageItemInfo->ignoreAnchorPointForPosition(false);
				packageItemInfo->setAnchorPoint(ccp(0.5f,0.5f));
				packageItemInfo->setTag(kTagGoodsItemInfoBase);
				GameView::getInstance()->getMainUIScene()->addChild(packageItemInfo);

				/*
				if (equip_ui->currType == 2 && curFolder->goods().clazz()== 3)
				{
					PackageItemInfo * packageItemInfo = PackageItemInfo::create(curFolder,equip_ui->generalEquipVector,equip_ui->mes_petid);
					packageItemInfo->ignoreAnchorPointForPosition(false);
					packageItemInfo->setAnchorPoint(ccp(0.5f,0.5f));
					packageItemInfo->setTag(kTagGoodsItemInfoBase);
					GameView::getInstance()->getMainUIScene()->addChild(packageItemInfo);
				}else
				{
					equip_ui->addOperationFloder(curFolder);
				}
				*/
				return;
			}
		}break;
	default:
		{
			packageItemInfo = PackageItemInfo::create(curFolder,GameView::getInstance()->EquipListItem,0);
		}break;
	}

	//PackageItemInfo * packageItemInfo = PackageItemInfo::create(curFolder,GameView::getInstance()->EquipListItem,0);
	//packageItemInfo = PackageItemInfo::create(curFolder,GameView::getInstance()->EquipListItem,0);
	packageItemInfo->ignoreAnchorPointForPosition(false);
	packageItemInfo->setAnchorPoint(ccp(0.5f,0.5f));
	//packageItemInfo->setPosition(autoGetPosition(pSender));
	//packageItemInfo->setPosition(ccp(size.width/2,size.height/2));
	packageItemInfo->setTag(kTagGoodsItemInfoBase);
	GameView::getInstance()->getMainUIScene()->addChild(packageItemInfo);
}

void PackageItem::GoodItemEvent( CCObject * pSender )
{

	if ((bool)isDoubleTouch())
	{
		CCLOG("165435165432165465415");
	}

	CCSize size= CCDirector::sharedDirector()->getVisibleSize();
	GoodsItemInfoBase * goodsItemInfoBase = GoodsItemInfoBase::create(curGood,GameView::getInstance()->EquipListItem,0);
	goodsItemInfoBase->ignoreAnchorPointForPosition(false);
	goodsItemInfoBase->setAnchorPoint(ccp(0.5f,0.5f));
	//goodsItemInfoBase->setPosition(ccp(size.width/2,size.height/2));
	GameView::getInstance()->getMainUIScene()->addChild(goodsItemInfoBase);
}

CCPoint PackageItem::autoGetPosition(CCObject * pSender)
{

	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();
	CCPoint cur_positon = ((PackageItem*)(((UIButton*)pSender)->getWidgetParent()))->getPosition();
	CCSize cur_size = CCSizeMake(83,75);
	if (380+GameView::getInstance()->pacPageView->getPositionX()+cur_positon.x+cur_size.width+272+(winSize.width-designResolutionSize.width)/2 < winSize.width)
	{
		//可以在右侧显示
		if(86+cur_positon.y > 360)
		{
			//可以在右下显示
			int x= 380+cur_positon.x+cur_size.width+origin.x+(winSize.width-designResolutionSize.width)/2+GameView::getInstance()->pacPageView->getPositionX();
			int y = 86+cur_positon.y+origin.y;
			return ccp(x,y);
		}
		else
		{
			//在右侧中间显示
			int x= 380+cur_positon.x+cur_size.width+origin.x+(winSize.width-designResolutionSize.width)/2+GameView::getInstance()->pacPageView->getPositionX();
			int y = (480-360)/2;
			return ccp(x,y);
		}
	}
	else
	{
		//在左侧显示
		if(86+cur_positon.y > 360)
		{
			//可以在左下显示
			int x= 380+cur_positon.x-272+origin.x+(winSize.width-designResolutionSize.width)/2+GameView::getInstance()->pacPageView->getPositionX();
			int y = 86+cur_positon.y-360+origin.y;
			return ccp(x,y);
		}
		else
		{
			//在左侧中间显示
			int x= 380+cur_positon.x-272+origin.x+(winSize.width-designResolutionSize.width)/2+GameView::getInstance()->pacPageView->getPositionX();
			int y = (480-360)/2;
			return ccp(x,y);
		}

	}

}

// 判断是不是 双击
 bool PackageItem::isDoubleTouch(){
	//static long lastTouchTime=0;
	long thisTouchTime=GameUtils::millisecondNow();
	if(abs(thisTouchTime-lastTouchTime)<250){
		lastTouchTime=0;
		return true;
	}
	else{
		lastTouchTime=GameUtils::millisecondNow();
		return false;
	}
 }

 void PackageItem::setGray( bool isGray,std::string isShowGoodsNum)
 {
	 if (this->getChildByName(MengBan_Black_Name))
	 {
		 this->getChildByName(MengBan_Black_Name)->removeFromParent();
	 }

	 if (isGray)
	 {
		 if (curFolder->quantity() <=1)
		 {
			 Btn_pacItemFrame->setTouchEnable(false);
		 }

		 UIImageView * m_mengban_black = UIImageView::create();
		 m_mengban_black->setTexture("res_ui/mengban_black65.png");
		 m_mengban_black->setScale9Enable(true);
		 m_mengban_black->setScale9Size(CCSizeMake(83,75));
		 m_mengban_black->setCapInsets(CCRectMake(5,5,1,1));
		 m_mengban_black->setName(MengBan_Black_Name);
		 m_mengban_black->setAnchorPoint(ccp(0,0));
		 m_mengban_black->setPosition(ccp(0,0));
		 this->addChild(m_mengban_black);

		 UIImageView * image_selectIcon = UIImageView::create();
		 image_selectIcon->setTexture("res_ui/greengou.png");
		 image_selectIcon->setAnchorPoint(ccp(0.5f,0.5f));
		 image_selectIcon->setPosition(ccp(m_mengban_black->getSize().width/2 + 3,
			 m_mengban_black->getSize().height - image_selectIcon->getContentSize().height+2));
		 m_mengban_black->addChild(image_selectIcon);

		 if (strlen(isShowGoodsNum.c_str()) > 0)
		 {
			 UILabel * label_num = UILabel::create();
			 label_num->setText(isShowGoodsNum.c_str());
			 label_num->setAnchorPoint(ccp(0.5f,0.5f));
			 label_num->setFontSize(16);
			 label_num->setPosition(ccp(m_mengban_black->getSize().width/2,label_num->getContentSize().height*3/2));
			 m_mengban_black->addChild(label_num);
		 }
	 }
	 else
	 {
		 Btn_pacItemFrame->setTouchEnable(true);
	 }
 }

 void PackageItem::setRed( bool isRed )
 {
	 if (this->getChildByName(MengBan_Red_Name))
	 {
		 this->getChildByName(MengBan_Red_Name)->removeFromParent();
	 }

	 if (isRed)
	 {
		 UIImageView * m_mengban_red = UIImageView::create();
		 m_mengban_red->setTexture("res_ui/mengban_red55.png");
		 m_mengban_red->setScale9Enable(true);
		 m_mengban_red->setScale9Size(CCSizeMake(83,75));
		 m_mengban_red->setCapInsets(CCRectMake(5,5,1,1));
		 m_mengban_red->setName(MengBan_Red_Name);
		 m_mengban_red->setAnchorPoint(ccp(0,0));
		 m_mengban_red->setPosition(ccp(0,0));
		 this->addChild(m_mengban_red);

		 UILabel * l_des = UILabel::create();
		 l_des->setText(StringDataManager::getString("packageitem_notAvailable"));
		 l_des->setFontName(APP_FONT_NAME);
		 l_des->setFontSize(18);
		 l_des->setAnchorPoint(ccp(0.5f,0.5f));
		 l_des->setPosition(ccp(m_mengban_red->getSize().width/2,m_mengban_red->getSize().height/2));
		 m_mengban_red->addChild(l_des);
	 }
	 else
	 {
	 }
 }

 void PackageItem::setAvailable(bool isAvailable)
 {
	 if (this->getChildByName(MengBan_Avalible))
	 {
		 this->getChildByName(MengBan_Avalible)->removeFromParent();
	 }

	 if (isAvailable)
	 {
		 Btn_pacItemFrame->setTouchEnable(true);
	 }
	 else
	 {
		 Btn_pacItemFrame->setTouchEnable(false);
		 //add mengban
		 UIImageView * m_mengban_black = UIImageView::create();
		 m_mengban_black->setTexture("res_ui/mengban_black65.png");
		 m_mengban_black->setScale9Enable(true);
		 m_mengban_black->setScale9Size(CCSizeMake(83,75));
		 m_mengban_black->setCapInsets(CCRectMake(5,5,1,1));
		 m_mengban_black->setName(MengBan_Avalible);
		 m_mengban_black->setAnchorPoint(ccp(0,0));
		 m_mengban_black->setPosition(ccp(0,0));
		 this->addChild(m_mengban_black);
		 /*
		 UIImageView * m_able = UIImageView::create();
		 m_able->setTexture("res_ui/font/nono.png");
		 m_able->setAnchorPoint(ccp(0.5f,0.5f));
		 m_able->setPosition(ccp(41,37));
		 m_mengban_black->addChild(m_able);

		
		 //add lable
		 UILabel * l_des = UILabel::create();
		 l_des->setText(StringDataManager::getString("packageitem_notAvailable"));
		 l_des->setFontName(APP_FONT_NAME);
		 l_des->setFontSize(18);
		 l_des->setAnchorPoint(ccp(0.5f,0.5f));
		 l_des->setPosition(ccp(41,37));
		 m_mengban_black->addChild(l_des);
		 */

	 }
 }

 void PackageItem::BtnLockEvent( CCObject * pSender )
 {
	 if (this->index >= (int)GameView::getInstance()->AllPacItem.size())
	 {
// 		 int num = this->index + 1 - GameView::getInstance()->AllPacItem.size();
// 		 std::string dialog = "suer to open ";
// 		 char * s = new char [5];
// 		 sprintf(s,"%d",num);
// 		 dialog.append(s);
// 		 delete [] s;
// 		 dialog.append(" folder ? ");
// 		 GameView::getInstance()->showPopupWindow(dialog,2,this,coco_selectselector(PackageItem::SureEvent),coco_selectselector(PackageItem::CancelEvent));
		 int num =  this->index + 1 - GameView::getInstance()->AllPacItem.size();
		 GameMessageProcessor::sharedMsgProcessor()->sendReq(1320,(void *)0,(void *)num);
	 }
 }

 void PackageItem::SureEvent( CCObject * pSender )
 {
	 int num =  this->index + 1 - GameView::getInstance()->AllPacItem.size();
	 GameMessageProcessor::sharedMsgProcessor()->sendReq(1320,(void *)0,(void *)num);
 }

 void PackageItem::CancelEvent( CCObject * pSender )
 {

 }

 void PackageItem::setNumVisible( bool visible )
 {
	 Lable_num->setVisible(visible);
 }

 void PackageItem::setBoundVisible( bool visible )
 {
	 ImageView_bound->setVisible(visible);
 }

 void PackageItem::setBoundJewelPropertyVisible( bool visible )
 {
	//gem 
	 if (Btn_pacItemFrame->getChildByName("gemPropertyIcon"))
	 {
		 Btn_pacItemFrame->getChildByName("gemPropertyIcon")->setVisible(visible);
	 }
 }
 
 ////////////////
 void PackageItem::registerScriptCommand( int scriptId )
 {
	 mTutorialScriptInstanceId = scriptId;
 }

 void PackageItem::addCCTutorialIndicator( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
 {
	 CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	 int _w = (winSize.width-800)/2;
	 int _h = (winSize.height - 480)/2;
// 	 CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,40,60);
// 	 tutorialIndicator->setAnchorPoint(ccp(0,0));
// 	 tutorialIndicator->setPosition(ccp(pos.x+ GameView::getInstance()->pacPageView->getPositionX()+_w,
// 													pos.y+130+GameView::getInstance()->pacPageView->getPositionY()+_h));
// 	 tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	 tutorialIndicator->setZOrder(10);
// 	 PackageScene * packageScene = (PackageScene *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
// 	 packageScene->addChild(tutorialIndicator);

	 CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,83,75);
	 tutorialIndicator->setDrawNodePos(ccp(pos.x+GameView::getInstance()->pacPageView->getPositionX()+_w +42,
												pos.y+36.5f+GameView::getInstance()->pacPageView->getPositionY()+_h));
	 tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	 MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	 if (mainScene)
	 {
		 mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	 }
 }

 void PackageItem::removeCCTutorialIndicator()
 {
// 	  PackageScene * packageScene = (PackageScene *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
// 	  if (packageScene != NULL)
// 	  {
// 		  CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(packageScene->getChildByTag(CCTUTORIALINDICATORTAG));
// 		  if(tutorialIndicator != NULL)
// 			  tutorialIndicator->removeFromParent();
// 	  }

	 MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	 if (mainScene)
	 {
		 CCTeachingGuide* teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		 if(teachingGuide != NULL)
			 teachingGuide->removeFromParent();
	 }
	
 }


 void PackageItem::BeganToRunCD()
 {
	 //CD
	 if (!curFolder->has_goods())
		 return;

	 if (!CDrug::isDrug(curFolder->goods().id()))
		 return;

	 //CD时间
	 float mCDTime_drug = 0.0f;
	 float mRemainTime_drug = 0.0f;
	 if (DrugManager::getInstance()->getDrugById(curFolder->goods().id()))
	 {
		 CDrug* pDrug = DrugManager::getInstance()->getDrugById(curFolder->goods().id());
		 if (!pDrug)
			 return;

		 mCDTime_drug = pDrug->get_cool_time()/1000.0f;
		 mRemainTime_drug = pDrug->getRemainTime()/1000.0f;
		 //CCLOG("remainTime = %f",mRemainTime_drug);

		 if (mRemainTime_drug <= 0.f)
		 {
			 if (mProgressTimer_skill->isVisible())
				 mProgressTimer_skill->setVisible(false);

			 return;
		 }

		 mProgressTimer_skill->setVisible(true);

		 CCProgressFromTo * action_progress_from_to = CCProgressFromTo::create(mRemainTime_drug , mRemainTime_drug/mCDTime_drug*100, 0);     
		 CCCallFuncN * action_callback = CCCallFuncN::create(this, callfuncN_selector(PackageItem::drugCoolDownCallBack));
		 mProgressTimer_skill->runAction(CCSequence::create(action_progress_from_to, action_callback, NULL));
	 }
 }

 void PackageItem::drugCoolDownCallBack( CCNode* node )
 {
	 mProgressTimer_skill->setVisible(false);
 }

 float PackageItem::getCurProgress()
 {
	 return mProgressTimer_skill->getPercentage();
 }

 std::string PackageItem::getCurGemIconPath( std::string goodsId )
 {
	CGemPropertyMap * gemMsg  = gempropertyConfig::s_GemPropertyMsgData[goodsId];
	int gemClazz = gemMsg->get_propClazz();
	std::string gemIconpath = "res_ui/font/property/";
	switch(gemClazz)
	{
	case 1:
		{
			gemIconpath.append("strength");
		}break;
	case 2:
		{
			gemIconpath.append("dexterity");
		}break;
	case 3:
		{
			gemIconpath.append("intelligence");
		}break;
	case 4:
		{
			gemIconpath.append("focus");
		}break;
	case 11:
		{
			gemIconpath.append("hp_capacity");
		}break;
	case 12:
		{
			gemIconpath.append("mp_capacity");	
		}break;
	case 13:
		{
			gemIconpath.append("min_attack");
		}break;
	case 14:
		{
			gemIconpath.append("max_attack");
		}break;
	case 15:
		{
			gemIconpath.append("min_magic_attack");
		}break;
	case 16:
		{
			gemIconpath.append("max_magic_attack");
		}break;
	case 17:
		{
			gemIconpath.append("min_defend");
		}break;
	case 18:
		{
			gemIconpath.append("max_defend");
		}break;
	case 19:
		{
			gemIconpath.append("min_magic_defend");
		}break;
	case 20:
		{
			gemIconpath.append("max_magic_defend");
		}break;
	case 21:
		{
			gemIconpath.append("hit");
		}break;
	case 22:
		{
			gemIconpath.append("dodge");
		}break;
	case 23:
		{
			gemIconpath.append("crit");
		}break;
	case 24:
		{
			gemIconpath.append("crit_damage");
		}break;
	case 25:
		{
			gemIconpath.append("attack_speed");
		}break;
	case 26:
		{
			//gemIconpath.append("move_speed");
		}break;
	}
	gemIconpath.append(".png");
	return gemIconpath;
 }
