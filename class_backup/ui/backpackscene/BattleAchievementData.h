#ifndef _UI_BACKPACKSCENE_BATTLEACHIEVEMENTDATA_H_
#define _UI_BACKPACKSCENE_BATTLEACHIEVEMENTDATA_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

/////////////////////////////////
/**
 * 战功模块（用于保存服务器数据）
 * @author liuliang
 * @version 0.1.0
 * @date 2014.08.08
 */
class CPkDailyReward;

class BattleAchievementData
{
public:
	static BattleAchievementData * s_battleAchData;
	static BattleAchievementData * instance();

private:
	BattleAchievementData(void);
	~BattleAchievementData(void);	

private:
	int m_nRank;						// 排名
	bool m_bHasGetGift;					// 是否领取奖励
	
public:
	std::vector<CPkDailyReward *> m_vector_reward;

	void initVectorFromInternet();


public:
	void set_rank(int nRank);
	int get_rank();

	void set_hasGetGift(bool bFlag);
	bool get_hasGetGift();
};

#endif

