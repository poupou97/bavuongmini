#ifndef _PACKAGESCENE_PACKAGESCENE_H
#define _PACKAGESCENE_PACKAGESCENE_H

#include "../extensions/UIScene.h"
#include "PacPageView.h"
#include "../generals_ui/GeneralsListBase.h"
#include "../../legend_script/CCTutorialIndicator.h"

USING_NS_CC;
USING_NS_CC_EXT;

class GameSceneState;
class UITab;
class GoodsInfo;
class FolderInfo;
class CEquipment;
class CGeneralBaseMsg;
class CActiveRole;
class CCRichLabel;
class GameActorAnimation;

class PackageScene : public GeneralsListBase,public CCTableViewDelegate,public CCTableViewDataSource
{
friend class MailUI;
public:

	struct IdxAndNum{
		int index;
		int num;
	};

 	std::vector<Coordinate> Coordinate_equip;
 	std::string equipmentItem_name[12];

	int curEquipmentIndex;
	CEquipment * curEquipment;

	int itemNumEveryPage ;


private:
	PackageScene();
public:
    
    ~PackageScene();
    bool init();
	static PackageScene* create();
	//void reloadInit();
	void reCreatePageVeiw();

	void RefreshEquipment(std::vector<CEquipment *>& equipmentList);
	void RefreshRoleAnimation(CActiveRole * activeRole);
	void RefreshRoleInfo(CActiveRole * activeRole,int fightpoint);
	GameActorAnimation* getRoleAnimation();

	virtual void onEnter();
	virtual void onExit();

	void closeBeganEvent(CCObject *pSender);
	void closeMovedEvent(CCObject *pSender);
    void closeEndedEvent(CCObject *pSender);

	void SortEvent(CCObject *pSender);
	void GoldStoreEvent(CCObject *pSender);
	void TitleEvent(CCObject *pSender);
	void AchievementEvent(CCObject *pSender);								// 战功

	void RemoteStoreHouseEvent(CCObject *pSender);

	void oneKeyAllEquip(CCObject * obj);
		 
 	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
 	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
 	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
 	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	void menuCloseCallback(CCObject* pSender);
	
	virtual void update(float dt);

	void PlayerEvent(CCObject* pSender);
	void DateEvent(CCObject* pSender);
	void LeftIndexChangedEvent(CCObject* pSender);
	void ChangeLeftPanelByIndex(int index);

	void CurrentPageViewChanged(CCObject* pSender);

	//void ReloadData();
	//void ReloadOnePacItem(FolderInfo* folderInfo);
	void sdEvent(CCObject *pSender);

	void ReloadProperty();
	void ReloadInfo();

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);
	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);

	void ReloadGeneralsTableView();
	void ReloadGeneralsTableViewWithoutChangeOffSet();

	void RefreshGeneralLevelInTableView(long long generalId);

	void PageScrollToDefault();

	//
	void setPresentVoidEquipItem(int part,bool isPresent);

	//一级，二级属性介绍
	void FirstGradeDesEvent(CCObject *pSender);
	void SecondGradeDesEvent(CCObject *pSender);

public:
	UIButton * Button_close;
	//教学
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction );
	void removeCCTutorialIndicator();
	//教学
	int mTutorialScriptInstanceId;
public:
	UIPanel *ppanel;
	UILayer *u_layer;
	UILayer * playerLayer;
	UILayer *tempLayer;

	CCMenuItemImage* playerItemImage;
	CCMenuItemImage* dataItemImage;

	PacPageView * pacPageView ;

	UITab * leftTab ;
	UITab * rightTab ;

	UIPanel * playerPanel;
	UIPanel * dataPanel;
	UIPanel * infoPanle;

	UIImageView * currentPage;

	UIPanel * allGoodsPanel;
	UIPanel * equipmentPanel;
	UIPanel * normalGoods;

	CCPoint a;

	//std::vector<CGeneralBaseMsg *>generalBaseMsgList;
	CCTableView * generalList_tableView;
	CGeneralBaseMsg * curGeneralBaseMsg;

	//当前选中的ActorID
	long long selectActorId;
	//cur select general equip part
	int selectGeneralEquipPart;
	//cur select general equip stalevel
	int selectGeneralEquipStarLevel;
	//cur select tabview of index
	int selectTabviewIndex;


	float m_remainArrangePac;
private:
	//主角名字
	UILabel * Label_roleName;
	//武将名字
	CCRichLabel * l_generalName;
	UILabel * l_roleLevel;
	UIImageView * imageView_professionName;
	UIImageView * imageView_country;
	UILabelBMFont * l_fightPoint;
	UILabelBMFont * l_allFightPoint;

	/**************属性***************/
	UILabel * l_nameValue;
	UILabel * l_levelValue;
	UILabel * l_powerValue;
	UILabel * l_aglieValue;  //敏捷
	UILabel * l_intelligenceValue;  //智力
	UILabel * l_focusValue;  //专注
	UILabel * l_curHpValue;
	UILabel * l_allHpValue;
	UIImageView * imageView_hp;
	UILabel * l_curMpValue;
	UILabel * l_allMpValue;
	UIImageView * imageView_mp;
	UILabel * l_phyAttackValue;
	UILabel * l_magicAttackValue;
	UILabel * l_phyDenValue;
	UILabel * l_magicDenValue;
	UILabel * l_hitValue;  //命中
	UILabel * l_dodgeValue; //闪避
	UILabel * l_critValue;  //暴击
	UILabel * l_critDamageValue;//暴击伤害
	UILabel * l_atkSpeedValue;
	UILabel * l_moveSpeedValue;

	
	/**************信息***************/
	UILabel * l_roleNameValue;  //名字
	UILabel * l_roleLvValue;  //等级
	UILabel * l_jobValue;  //职业
	UILabel * l_genderValue;  //性别
	UILabel * l_countryValue;   //国家
	UILabel * l_titleValue;   //称号
	UILabel * l_factionValue;   //帮派
	UILabel * l_honorValue;  //荣誉
	UILabel * l_familyValue;  //家族
	UILabel * l_battleAchValue;		// 战功
	//UILabel * l_experienceValue;  //历练
	UILabel * l_physicalValue;  //体力
	UILabel * l_expValue;
	UIImageView * imageView_exp; //经验条

	UILabel *Label_gold;
	UILabel *Label_goldIngot;
	UILabel *Label_bingGoldIngot;

	//equipment
	UIImageView * ImageView_wuqi;
	UIImageView * ImageView_toukui;
	UIImageView * ImageView_yifu;
	UIImageView * ImageView_xiezi;
	UIImageView * ImageView_jiezhi;
	UIImageView * ImageView_peishi;
};


/////////////////////////////////
/**
 * 武将小头像
 * @author yangjun
 * @version 0.1.0
 * @date 2013.12.17
 */
class CGeneralBaseMsg;

class GeneralsSmallHeadCell : public CCTableViewCell
{
public:
	GeneralsSmallHeadCell();
	~GeneralsSmallHeadCell();
	static GeneralsSmallHeadCell* create(CGeneralBaseMsg * generalBaseMsg);
	bool init(CGeneralBaseMsg * generalBaseMsg);

	CGeneralBaseMsg* getCurGeneralBaseMsg();

private:
	CGeneralBaseMsg * curGeneralBaseMsg; 
};

#endif
