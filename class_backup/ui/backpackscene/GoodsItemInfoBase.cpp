#include "GoodsItemInfoBase.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "GameView.h"
#include "../../messageclient/element/CEquipment.h"
#include "../../messageclient/element/CEquipProperty.h"
#include "../../utils/StaticDataManager.h"
#include "../generals_ui/GeneralsUI.h"
#include "../generals_ui/GeneralsStrategiesUI.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "../../gamescene_state/MainScene.h"
#include "PackageScene.h"
#include "../../ui/equipMent_ui/EquipMentGeneralList.h"
#include "../../ui/extensions/QuiryUI.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"
#include "../../ui/Chat_ui/ChatUI.h"
#include "../../ui/Chat_ui/BackPackage.h"
#include "../../utils/GameUtils.h"
#include "PackageItemInfo.h"
#include "../../ui/extensions/ShowSystemInfo.h"

GoodsItemInfoBase::GoodsItemInfoBase()
{
	selfEquipSuit = 0;
	isSetSuitColor =false;
	isRoleEquip_ =false;
	buttonImagePath = "res_ui/new_button_1.png";
}


GoodsItemInfoBase::~GoodsItemInfoBase()
{
	delete goodsInfo_General;

	std::vector<CEquipment*>::iterator iter;
	for (iter = selfEquipVector.begin(); iter != selfEquipVector.end(); ++iter)
	{
		delete *iter;
	}
	selfEquipVector.clear();
}

GoodsItemInfoBase * GoodsItemInfoBase::create(GoodsInfo * goodsInfo,std::vector<CEquipment *> curEquipment,long long generalId /*= 0*/ )
{
	GoodsItemInfoBase * goodsbase= new GoodsItemInfoBase();
	if (goodsbase && goodsbase->init(goodsInfo,curEquipment,generalId))
	{
		goodsbase->autorelease();
		return goodsbase;
	}
	CC_SAFE_DELETE(goodsbase);
	return NULL;
}

bool GoodsItemInfoBase::init(GoodsInfo * goodsInfo,std::vector<CEquipment *> curEquipment,long long generalId)
{
	if (UIScene::init())
	{	
		CCSize size= CCDirector::sharedDirector()->getVisibleSize();
		goodsInfo_General =new GoodsInfo();
		goodsInfo_General->CopyFrom(*goodsInfo);

// 		if (goodsInfo->equipmentclazz()>0 && goodsInfo->equipmentclazz()<11)
// 		{
// 			UIImageView *mengban = UIImageView::create();
// 			mengban->setTexture("res_ui/zhezhao80.png");
// 			mengban->setScale9Enable(true);
// 			mengban->setScale9Size(size);
// 			mengban->setAnchorPoint(ccp(0.5f,0.5f));
// 			mengban->setPosition(ccp(548/2+40,362/2));
// 			m_pUiLayer->addWidget(mengban);
// 		}
		
		generalLevel = GameView::getInstance()->myplayer->getActiveRole()->level();
		generalProfession = BasePlayer::getProfessionIdxByName(GameView::getInstance()->myplayer->getActiveRole()->profession());
	
		UIImageView * imageFrame =UIImageView::create();
		imageFrame->setTexture("res_ui/zhezhao01.png");
		imageFrame->setAnchorPoint(ccp(0,0));
		imageFrame->setScale9Enable(true);
		imageFrame->setScale9Size(CCSizeMake(274,362));
		imageFrame->setCapInsets(CCRect(50,50,1,1));
		imageFrame->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(imageFrame);
		
		scrollView_info = UIScrollView::create();
		scrollView_info->setTouchEnable(true);
		scrollView_info->setDirection(SCROLLVIEW_DIR_VERTICAL);
		scrollView_info->setSize(CCSizeMake(274,303));
		scrollView_info->setPosition(ccp(0,54));
		scrollView_info->setBounceEnabled(true);
		scrollView_info->setZOrder(10);
		m_pUiLayer->addWidget(scrollView_info);

		touchPanelInfo = UIPanel::create();
		touchPanelInfo->setSize(CCSizeMake(274,304));
		touchPanelInfo->setAnchorPoint(ccp(0,1));
		touchPanelInfo->setTouchEnable(true);
		touchPanelInfo->setZOrder(10);
		scrollView_info->addChild(touchPanelInfo);

		if (goodsInfo->equipmentclazz()>0 && goodsInfo->equipmentclazz()<11)
		{
			UIImageView * bodyImageFrame =UIImageView::create();
			bodyImageFrame->setTexture("res_ui/zhezhao01.png");
			bodyImageFrame->setScale9Enable(true);
			bodyImageFrame->setScale9Size(CCSizeMake(274,362));
			bodyImageFrame->setCapInsets(CCRect(50,50,1,1));
			bodyImageFrame->setAnchorPoint(ccp(0,0));
			bodyImageFrame->setPosition(ccp(274,0));
			m_pUiLayer->addWidget(bodyImageFrame);

			//if role lv is not enouhg not show generalbutton
			int openlevel = 0;
			std::map<int,int>::const_iterator cIter;
			cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(26);
			if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end())
			{
			}
			else
			{
				openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[26];
			}
			if (GameView::getInstance()->myplayer->getActiveRole()->level() >= openlevel)
			{
				this->addGeneralButton(generalId);
			}

			this->getSelfEquipVector(goodsInfo,curEquipment);
			
			this->showTouchEquipMentInfo(goodsInfo,0);

			flag_toucheSelfEquip =UIImageView::create();
			flag_toucheSelfEquip->setTexture("res_ui/yichuzhan.png");
			flag_toucheSelfEquip->setAnchorPoint(ccp(1,1));
			flag_toucheSelfEquip->setPosition(ccp(touchPanelInfo->getContentSize().width,360));
			flag_toucheSelfEquip->setZOrder(10);
			m_pUiLayer->addWidget(flag_toucheSelfEquip);
			flag_toucheSelfEquip->setVisible(false);
			this->isTouchSameEquipment();

			scrollView_info->setSize(CCSizeMake(548,304));

			bodyPanelInfo = UIPanel::create();
			bodyPanelInfo->setSize(CCSizeMake(274,304));
			bodyPanelInfo->setAnchorPoint(ccp(0,1));
			bodyPanelInfo->setPosition(ccp(touchPanelInfo->getContentSize().width,scrollView_info->getContentSize().height));
			bodyPanelInfo->setTouchEnable(true);
			bodyPanelInfo->setZOrder(10);
			scrollView_info->addChild(bodyPanelInfo);

			const char *strings_addMain = StringDataManager::getString("equipinfo_notEquip");
			notEquipment =UILabel::create();
			notEquipment->setText(strings_addMain);
			notEquipment->setAnchorPoint(ccp(0.5f,0.5f));
			notEquipment->setPosition(ccp(2*touchPanelInfo->getContentSize().width*3/4,200));
			notEquipment->setFontSize(20);
			m_pUiLayer->addWidget(notEquipment);

			flag_equip =UIImageView::create();
			flag_equip->setTexture("res_ui/yichuzhan.png");
			flag_equip->setAnchorPoint(ccp(1,1));
			flag_equip->setPosition(ccp(2*touchPanelInfo->getContentSize().width,360));
			flag_equip->setZOrder(10);
			m_pUiLayer->addWidget(flag_equip);
		
			for (int i=0;i<curEquipment.size();i++)
			{
				if (goodsInfo->equipmentclazz()==curEquipment.at(i)->goods().equipmentclazz())
				{
					GoodsInfo * playerEquip = new GoodsInfo();
					playerEquip->CopyFrom(curEquipment.at(i)->goods());
					isRoleEquip_ = true;
					isSetSuitColor = true;
					this->showTouchEquipMentInfo(playerEquip,1);
					delete playerEquip;
					break;
				}
			}
			
			if (isRoleEquip_ ==false)
			{
				flag_equip->setVisible(false);
				notEquipment->setVisible(true);
			}else
			{
				flag_equip->setVisible(true);
				notEquipment->setVisible(false);
			}

			int width = touchPanelInfo->getContentSize().width + bodyPanelInfo->getContentSize().width;
			if (touchPanelInfo->getContentSize().height > bodyPanelInfo->getContentSize().height)
			{
				touchPanelInfo->setPosition(ccp(0,touchPanelInfo->getContentSize().height));
				bodyPanelInfo->setPosition(ccp(touchPanelInfo->getContentSize().width,touchPanelInfo->getContentSize().height));
				scrollView_info->setInnerContainerSize(CCSizeMake(width, touchPanelInfo->getContentSize().height));
			}else
			{
				touchPanelInfo->setPosition(ccp(0,bodyPanelInfo->getContentSize().height));
				bodyPanelInfo->setPosition(ccp(touchPanelInfo->getContentSize().width,bodyPanelInfo->getContentSize().height));
				scrollView_info->setInnerContainerSize(CCSizeMake(width, bodyPanelInfo->getContentSize().height));
			}
			this->setTouchEnabled(true);
			this->setTouchMode(kCCTouchesOneByOne);
			this->setContentSize(CCSizeMake(548,362));

			if (GameView::getInstance()->myplayer->getActiveRole()->level() >= openlevel)
			{
				this->setPosition(ccp(size.width/2 - 40,size.height/2)); 
			}else
			{
				this->setPosition(ccp(size.width/2,size.height/2)); 
			}

		}else
		{
			this->showTouchGoodsInfo(goodsInfo);
			touchPanelInfo->setPosition(ccp(0,touchPanelInfo->getContentSize().height));
			scrollView_info->setInnerContainerSize(CCSizeMake(touchPanelInfo->getContentSize().width, touchPanelInfo->getContentSize().height));
			
			this->setTouchEnabled(true);
			this->setTouchMode(kCCTouchesOneByOne);
			this->setContentSize(CCSizeMake(274,362));
			this->setPosition(ccp(size.width/2,size.height/2));
		}

		return true;
	}
	return false;
}
void GoodsItemInfoBase::showTouchGoodsInfo(GoodsInfo * goodsInfo)
{
	CCSize size= scrollView_info->getInnerContainerSize();

	int quality_ =goodsInfo->quality();
	std::string qualityStr_ = getEquipmentQualityByIndex(quality_);
	UIImageView * headImageFrame =UIImageView::create();
	headImageFrame->setTexture(qualityStr_.c_str());
	headImageFrame->setScale9Enable(true);
	headImageFrame->setScale9Size(CCSizeMake(50,50));
	headImageFrame->setCapInsets(CCRect(1.5f,1.5f,1.0f,1.0f));
	headImageFrame->setAnchorPoint(ccp(0.5f,0.5f));
	touchPanelInfo->addChild(headImageFrame);

	std::string headImageStr ="res_ui/props_icon/";
	headImageStr.append(goodsInfo->icon());
	headImageStr.append(".png");

	headImage_goods = UIImageView::create();
	headImage_goods->setTexture(headImageStr.c_str());
	touchPanelInfo->addChild(headImage_goods);

	ccColor3B color_ =getEquipmentColorByQuality(quality_);
	label_goods_name=UILabel::create();
	label_goods_name->setAnchorPoint(ccp(0,0));
	label_goods_name->setText(goodsInfo->name().c_str());
	label_goods_name->setColor(color_);
	label_goods_name->setFontSize(16);
	touchPanelInfo->addChild(label_goods_name);

	int goodsBingding_ = goodsInfo->binding();
	UILabel *binding_label=UILabel::create();
	binding_label->setAnchorPoint(ccp(0,0));
	binding_label->setFontSize(16);
	if (goodsBingding_ == 1)
	{
		const char *string = StringDataManager::getString("equipInfo_bingding");
		binding_label->setText(string);
		binding_label->setColor(ccc3(255,0,0));
	}else
	{
		const char *string = StringDataManager::getString("equipInfo_notBingding");
		binding_label->setText(string);
		binding_label->setColor(ccc3(153,248,167));
	}
	touchPanelInfo->addChild(binding_label);

	///////////////////////////////////////////
	UIImageView * boundary_first= UIImageView::create();
	boundary_first->setAnchorPoint(ccp(0.5f,0.5f));
	boundary_first->setTexture("res_ui/wupinzhuangbeixiangqing/gaoguang.png");
	touchPanelInfo->addChild(boundary_first);

	const char *str_uselevel = StringDataManager::getString("equipment_info_useLevel");
	UILabel * m_goodsuselevel=UILabel::create();
	m_goodsuselevel->setAnchorPoint(ccp(0,0));
	m_goodsuselevel->setText(str_uselevel);
	m_goodsuselevel->setColor(ccc3(101,255,221));
	m_goodsuselevel->setFontSize(16);
	touchPanelInfo->addChild(m_goodsuselevel);

	int gradeId = goodsInfo->uselevel();
	char * gradeStr =new char[10];
	sprintf(gradeStr,"%d",gradeId);
	label_goods_uselevel=UILabel::create();
	label_goods_uselevel->setAnchorPoint(ccp(0,0));
	label_goods_uselevel->setText(gradeStr);
	label_goods_uselevel->setFontSize(16);
	label_goods_uselevel->setColor(ccc3(101,255,221));
	touchPanelInfo->addChild(label_goods_uselevel);
	delete []gradeStr;

	const char *str_price = StringDataManager::getString("goods_info_price");
	UILabel * m_goodsPrice=UILabel::create();
	m_goodsPrice->setAnchorPoint(ccp(0,0));
	m_goodsPrice->setText(str_price);
	m_goodsPrice->setColor(ccc3(101,255,221));
	m_goodsPrice->setFontSize(16);
	touchPanelInfo->addChild(m_goodsPrice);

	int priceId= goodsInfo->sellprice();
	char priceStr[10];
	sprintf(priceStr,"%d",priceId);
	label_goodsPrice=UILabel::create();
	label_goodsPrice->setAnchorPoint(ccp(0,0));
	label_goodsPrice->setText(priceStr);
	label_goodsPrice->setFontSize(16);
	label_goodsPrice->setColor(ccc3(101,255,221));
	touchPanelInfo->addChild(label_goodsPrice);

	UIImageView * image_SellPriceType = UIImageView::create();
	image_SellPriceType->setAnchorPoint(ccp(0,0));
	image_SellPriceType->setTexture("res_ui/coins.png");
	touchPanelInfo->addChild(image_SellPriceType);
	///////////////////////////////////////////
	UIImageView * boundary_second= UIImageView::create();
	boundary_second->setAnchorPoint(ccp(0.5f,0.5f));
	boundary_second->setTexture("res_ui/wupinzhuangbeixiangqing/gaoguang.png");
	touchPanelInfo->addChild(boundary_second);
	
	UITextArea* textArea = UITextArea::create();
	textArea->setTextAreaSize(CCSizeMake(250, 90));
	textArea->setAnchorPoint(ccp(0,0.5f));
	textArea->setTextHorizontalAlignment(kCCTextAlignmentLeft);
	textArea->setTextVerticalAlignment(kCCVerticalTextAlignmentTop);
	textArea->setText(goodsInfo->desc().c_str());
	textArea->setFontSize(16);
	textArea->setColor(ccc3(228,173,4));
	touchPanelInfo->addChild(textArea);        

	int touchFirst =headImage_goods->getRect().size.height + boundary_first->getRect().size.height;
	int touchSecond = m_goodsuselevel->getRect().size.height+ m_goodsPrice->getRect().size.height;
	int toucheThird = boundary_second->getRect().size.height + textArea->getRect().size.height;
	//int touchHight_ = touchFirst + touchSecond + toucheThird;
	touchHight=touchFirst + touchSecond + toucheThird;
	int innerWidth=scrollView_info->getRect().size.width;
	int innerHeight=scrollView_info->getRect().size.height + touchHight/2;

	if (touchHight <= scrollView_info->getRect().size.height)
	{
		innerHeight =scrollView_info->getRect().size.height;
	}

	scrollView_info->setInnerContainerSize(CCSizeMake(innerWidth,innerHeight));
	touchPanelInfo->setSize(CCSizeMake(innerWidth,innerHeight));
	headImage_goods->setPosition(ccp(43,innerHeight-39));
	headImageFrame->setPosition(ccp(43,innerHeight-39));
	label_goods_name->setPosition(ccp(100,innerHeight-40));
	binding_label->setPosition(ccp(100,label_goods_name->getPosition().y-20));
	boundary_first->setPosition(ccp(size.width/2,headImage_goods->getPosition().y - headImage_goods->getRect().size.height/2 -10));
	m_goodsuselevel->setPosition(ccp(16,boundary_first->getPosition().y - 28));
	label_goods_uselevel->setPosition(ccp(m_goodsuselevel->getContentSize().width+20,boundary_first->getPosition().y - 28));
	m_goodsPrice->setPosition(ccp(16,m_goodsuselevel->getPosition().y - 16));
	label_goodsPrice->setPosition(ccp(m_goodsPrice->getContentSize().width+20,label_goods_uselevel->getPosition().y - 16));
	image_SellPriceType->setPosition(ccp(label_goodsPrice->getPosition().x +label_goodsPrice->getContentSize().width + 5,label_goodsPrice->getPosition().y));
	boundary_second->setPosition(ccp(size.width/2,m_goodsPrice->getPosition().y - 10));
	textArea->setPosition(ccp(20,boundary_second->getPosition().y - textArea->getContentSize().height/2-15));
}

void GoodsItemInfoBase::showTouchEquipMentInfo(GoodsInfo * goodsInfo,int index)
{
	bool isHavesuibt =false;

	int w_space = 30;

	baseValueNum=0;
	starValueNum=0;
	activeValueNum=0;
	gemValueNum=0;
	suitValueNum =0;
	UIPanel * panelbackGround;
	if (index == 0)
	{
		panelbackGround =touchPanelInfo;
	}else
	{
		panelbackGround =bodyPanelInfo;
	}

	int quality_ = goodsInfo->equipmentdetail().quality();
	std::string qualityStr_ = getEquipmentQualityByIndex(quality_);

	UIButton * btn_frame = UIButton::create();
	btn_frame->setTextures(qualityStr_.c_str(),qualityStr_.c_str(),"");
	btn_frame->setTouchEnable(true);
	btn_frame->setPressedActionEnabled(true);
	btn_frame->addReleaseEvent(this,coco_releaseselector(GoodsItemInfoBase::callBackQualityDes));
	btn_frame->setAnchorPoint(ccp(0.5f,0.5f));
	panelbackGround->addChild(btn_frame);

	std::string headImageStr ="res_ui/props_icon/";
	headImageStr.append(goodsInfo->icon());
	headImageStr.append(".png");
	UIImageView * headImage_equip= UIImageView::create();
	headImage_equip->setTexture(headImageStr.c_str());
	headImage_equip->setAnchorPoint(ccp(0.5f,0.5f));
	headImage_equip->setPosition(ccp(0,0));
	btn_frame->addChild(headImage_equip);

	int refineLv_ = goodsInfo->equipmentdetail().gradelevel();
	if (refineLv_ > 0)
	{
		char equipRefineStr[5];
		sprintf(equipRefineStr,"%d",refineLv_);
		std::string showRefineLv = "+";
		showRefineLv.append(equipRefineStr);
		UILabel * label_refine = UILabel::create();
		label_refine->setAnchorPoint(ccp(1,1));
		label_refine->setText(showRefineLv.c_str());
		label_refine->setFontSize(12);
		label_refine->setPosition(ccp(btn_frame->getSize().width/2 - 4,btn_frame->getSize().height/2 -1));
		btn_frame->addChild(label_refine);
	}

	int starLv_ = goodsInfo->equipmentdetail().starlevel();
	if (starLv_ > 0)
	{
		char equipStarStr[5];
		sprintf(equipStarStr,"%d",starLv_);
		UILabel * label_star = UILabel::create();
		label_star->setAnchorPoint(ccp(0,0));
		label_star->setText(equipStarStr);
		label_star->setFontSize(12);
		label_star->setPosition(ccp(-btn_frame->getSize().width/2+2,-btn_frame->getSize().height/2+2));
		btn_frame->addChild(label_star);

		UIImageView * starlevel_int = UIImageView::create();
		starlevel_int->setTexture("res_ui/star_on.png");
		starlevel_int->setAnchorPoint(ccp(0,0));
		starlevel_int->setPosition(ccp(label_star->getPosition().x+label_star->getContentSize().width,label_star->getPosition().y));
		starlevel_int->setScale(0.5f);
		btn_frame->addChild(starlevel_int);
	}

	int goodsBingding_ = goodsInfo->binding();
	if (goodsBingding_)
	{
		UIImageView * Image_Bingding= UIImageView::create();
		Image_Bingding->setTexture("res_ui/binding.png");
		Image_Bingding->setAnchorPoint(ccp(0,1));
		Image_Bingding->setPosition(ccp(-btn_frame->getSize().width/2+2,btn_frame->getSize().height/2 - 2));
		Image_Bingding->setScale(0.8f);
		btn_frame->addChild(Image_Bingding);
	}
	
 	std::string showgrade_ =goodsInfo->name();
// 	int userGrade_ =goodsInfo->equipmentdetail().gradelevel();
// 	if (userGrade_ > 0)
// 	{
// 		char userGradeStr[5];
// 		sprintf(userGradeStr,"%d",userGrade_);
// 		showgrade_.append("(+");
// 		showgrade_.append(userGradeStr);
// 		showgrade_.append(")");
// 	}
	ccColor3B equipmentColor_ = getEquipmentColorByQuality(quality_);
	//updata color
	UILabel * label_equipName=UILabel::create();
	label_equipName->setAnchorPoint(ccp(0,0));
	label_equipName->setFontSize(16);
	label_equipName->setColor(equipmentColor_);
	label_equipName->setText(showgrade_.c_str());
	panelbackGround->addChild(label_equipName);
	
	const char *str_profession = StringDataManager::getString("equipment_info_profession");
	UILabel * m_profession=UILabel::create();
	m_profession->setAnchorPoint(ccp(0,0));
	m_profession->setText(str_profession);
	m_profession->setColor(ccc3(39,238,194));
	m_profession->setFontSize(16);
	panelbackGround->addChild(m_profession);

	std::string professionName = BasePlayer::getProfessionNameIdxByIndex(goodsInfo->equipmentdetail().profession());
	UILabel * label_profession=UILabel::create();
	label_profession->setAnchorPoint(ccp(0,0));
	label_profession->setText(professionName.c_str());
	label_profession->setFontSize(16);
	label_profession->setName("toucheEquipPression");
	label_profession->setColor(ccc3(39,238,194));
	panelbackGround->addChild(label_profession);

	UILabel * binding_label=UILabel::create();
	binding_label->setAnchorPoint(ccp(0,0));
	if (goodsBingding_ == 1)
	{
		const char *string = StringDataManager::getString("equipInfo_bingding");
		binding_label->setText(string);
		binding_label->setColor(ccc3(255,0,0));
	}else
	{
		const char *string = StringDataManager::getString("equipInfo_notBingding");
		binding_label->setText(string);
		binding_label->setColor(ccc3(153,248,167));
	}
	binding_label->setFontSize(16);
	panelbackGround->addChild(binding_label);

	int imageStarIconHigh_ = 24;
	UIImageView *boundary_Zero= UIImageView::create();
	boundary_Zero->setAnchorPoint(ccp(0.5f,0.5f));
	boundary_Zero->setTexture("res_ui/wupinzhuangbeixiangqing/gaoguang.png");
	panelbackGround->addChild(boundary_Zero);
	
	//scrollview high and wight
	for (int i=0;i<goodsInfo->equipmentdetail().properties_size();i++)
	{
		if (goodsInfo->equipmentdetail().properties(i).type()==5 || goodsInfo->equipmentdetail().properties(i).type()==6 )
		{
			touchHight =boundary_Zero->getRect().size.height *6+binding_label->getRect().size.height*30 +imageStarIconHigh_+290;
			isHavesuibt = true;
		}else
		{
			touchHight =boundary_Zero->getRect().size.height *5 +binding_label->getRect().size.height*24+imageStarIconHigh_+165;
		}
	}
	
	int innerWidth=scrollView_info->getRect().size.width;
	int innerHeight=scrollView_info->getRect().size.height + touchHight/2;
	scrollView_info->setInnerContainerSize(CCSizeMake(innerWidth,innerHeight));

	panelbackGround->setSize(CCSizeMake(274,innerHeight));
	
	int indexWidth =panelbackGround->getContentSize().width;
	btn_frame->setPosition(ccp(58,innerHeight-40));//44
	//headImage_equip->setPosition(ccp(44 ,innerHeight-38));
	label_equipName->setPosition(ccp(104,innerHeight-32));//90
	m_profession->setPosition(ccp(104 ,innerHeight-50));
	label_profession->setPosition(ccp(m_profession->getContentSize().width+104,innerHeight-50));
	binding_label->setPosition(ccp(104,m_profession->getPosition().y - 18));

	UILabelBMFont * power_labelBmDes = UILabelBMFont::create();
	power_labelBmDes->setAnchorPoint(ccp(0,0));
	power_labelBmDes->setPosition(ccp(30.5f,binding_label->getPosition().y - 33));
	power_labelBmDes->setText(StringDataManager::getString("equipment_info_powerCount"));
	power_labelBmDes->setFntFile("res_ui/font/ziti_3.fnt");
	panelbackGround->addChild(power_labelBmDes);

	int fightCapacity = goodsInfo->equipmentdetail().fightcapacity();
	char fightStr[50];
	sprintf(fightStr,"%d",fightCapacity);

	UILabelBMFont * power_labelBmNum = UILabelBMFont::create();
	power_labelBmNum->setAnchorPoint(ccp(0,0));
	power_labelBmNum->setPosition(ccp(power_labelBmDes->getPosition().x+power_labelBmDes->getContentSize().width,
										power_labelBmDes->getPosition().y-7));
	power_labelBmNum->setText(fightStr);
	power_labelBmNum->setFntFile("res_ui/font/ziti_1.fnt");
	power_labelBmNum->setScale(1.3f);
	panelbackGround->addChild(power_labelBmNum);

	/*
	int cur_starLevel = goodsInfo->equipmentdetail().starlevel();
	int m_star_all= cur_starLevel/2;
	int m_star_hafl =cur_starLevel%2;

	for (int i=1;i<=m_star_all;i++)
	{
		UIImageView * starlevel_int = UIImageView::create();
		starlevel_int->setAnchorPoint(ccp(1,0));
		starlevel_int->setPosition(ccp(10+25*i,binding_label->getPosition().y -25));
		starlevel_int->setTexture("res_ui/star_on.png");
		panelbackGround->addChild(starlevel_int);
	}

	if (m_star_hafl > 0)
	{
		UIImageView * starlevel_half = UIImageView::create();
		starlevel_half->setAnchorPoint(ccp(1,0));
		starlevel_half->setPosition(ccp(10+25*(m_star_all+1),binding_label->getPosition().y -25));
		starlevel_half->setTexture("res_ui/star_half.png");
		panelbackGround->addChild(starlevel_half);
	}

	for (int m= m_star_all +m_star_hafl;m<10;m++)
	{
		UIImageView * starlevel_All = UIImageView::create();
		starlevel_All->setAnchorPoint(ccp(1,0));
		starlevel_All->setPosition(ccp(35+25*m,binding_label->getPosition().y - 25));
		starlevel_All->setTexture("res_ui/star_off.png");
		panelbackGround->addChild(starlevel_All);
	}
	*/
	int starPosition_ = binding_label->getPosition().y - 25;
	boundary_Zero->setPosition(ccp(panelbackGround->getContentSize().width/2,starPosition_ - 10));
	///////////////
	const char *str_uselevel = StringDataManager::getString("equipment_info_useLevel");
	UILabel * m_uselevel=UILabel::create();
	m_uselevel->setAnchorPoint(ccp(0,0));
	m_uselevel->setText(str_uselevel);
	m_uselevel->setFontSize(16);
	m_uselevel->setColor(ccc3(101,255,221));
	m_uselevel->setPosition(ccp(w_space,boundary_Zero->getPosition().y - 28));//16
	panelbackGround->addChild(m_uselevel);

	int useLevel_ = goodsInfo->equipmentdetail().usergrade();
	int selfLevel =  GameView::getInstance()->myplayer->getActiveRole()->level();
	char uselevelStr[10];
	sprintf(uselevelStr,"%d",useLevel_);
	UILabel * label_uselevel=UILabel::create();
	label_uselevel->setAnchorPoint(ccp(0,0));
	label_uselevel->setText(uselevelStr);
	label_uselevel->setFontSize(16);
	label_uselevel->setColor(ccc3(101,255,221));
	label_uselevel->setName("toucheEquipLevel");
	label_uselevel->setPosition(ccp(m_uselevel->getContentSize().width+m_uselevel->getPosition().x+2,boundary_Zero->getPosition().y - 28));
	panelbackGround->addChild(label_uselevel);

	if (index == 0)
	{
		this->setEquipProfessionAndLevelColor(generalProfession,generalLevel);
	}
	
	////////////
	const char *str_durle = StringDataManager::getString("equipment_info_durle");
	UILabel * equipDurleLabel =UILabel::create();
	equipDurleLabel->setAnchorPoint(ccp(0,0));
	equipDurleLabel->setText(str_durle);
	equipDurleLabel->setFontSize(16);
	equipDurleLabel->setColor(ccc3(101,255,221));
	equipDurleLabel->setPosition(ccp(w_space,m_uselevel->getPosition().y -20));
	panelbackGround->addChild(equipDurleLabel);

	int m_durleVale = goodsInfo->equipmentdetail().durable();
	int m_maxDurleVale = goodsInfo->equipmentdetail().maxdurable();
	char durleValueStr_[5];
	sprintf(durleValueStr_,"%d",m_durleVale);
	char maxdurleValueStr_[5];
	sprintf(maxdurleValueStr_,"%d",m_maxDurleVale);

	std::string durableStr_ =durleValueStr_;
	durableStr_.append("/");
	durableStr_.append(maxdurleValueStr_);
	UILabel *labelDurleVale =UILabel::create();
	labelDurleVale->setAnchorPoint(ccp(0,0));
	labelDurleVale->setText(durableStr_.c_str());
	labelDurleVale->setFontSize(16);
	labelDurleVale->setColor(ccc3(101,255,221));
	labelDurleVale->setPosition(ccp(equipDurleLabel->getContentSize().width+equipDurleLabel->getPosition().x+2,equipDurleLabel->getPosition().y));
	panelbackGround->addChild(labelDurleVale);
	if (m_durleVale <= 10 )
	{
		equipDurleLabel->setColor(ccc3(255,0,0));
		labelDurleVale->setColor(ccc3(255,0,0));

		std::string repairLabelStr = "(";
		const char *str_durle = StringDataManager::getString("equipment_info_repair");
		repairLabelStr.append(str_durle);
		repairLabelStr.append(")");

		UILabel * repairLabel =UILabel::create();
		repairLabel->setAnchorPoint(ccp(0,0));
		repairLabel->setText(repairLabelStr.c_str());
		repairLabel->setFontSize(16);
		repairLabel->setColor(ccc3(255,0,0));
		repairLabel->setPosition(ccp(labelDurleVale->getContentSize().width+labelDurleVale->getPosition().x,labelDurleVale->getPosition().y));
		panelbackGround->addChild(repairLabel);
	}

	const char *str_equipPrice_ = StringDataManager::getString("equipment_info_price");
	UILabel * equipPriceLabel =UILabel::create();
	equipPriceLabel->setAnchorPoint(ccp(0,0));
	equipPriceLabel->setText(str_equipPrice_);
	equipPriceLabel->setFontSize(16);
	equipPriceLabel->setColor(ccc3(101,255,221));
	equipPriceLabel->setPosition(ccp(w_space,equipDurleLabel->getPosition().y -20));
	panelbackGround->addChild(equipPriceLabel);

	int m_priceValue =goodsInfo->sellprice();
	char priceValueStr_[5];
	sprintf(priceValueStr_,"%d",m_priceValue);
	UILabel *labelPriceValue =UILabel::create();
	labelPriceValue->setAnchorPoint(ccp(0,0));
	labelPriceValue->setText(priceValueStr_);
	labelPriceValue->setFontSize(16);
	labelPriceValue->setColor(ccc3(101,255,221));
	labelPriceValue->setPosition(ccp(equipPriceLabel->getContentSize().width+equipPriceLabel->getPosition().x+2,equipPriceLabel->getPosition().y));
	panelbackGround->addChild(labelPriceValue);

	UIImageView * image_SellPriceType = UIImageView::create();
	image_SellPriceType->setAnchorPoint(ccp(0,0));
	image_SellPriceType->setTexture("res_ui/coins.png");
	image_SellPriceType->setPosition(ccp(labelPriceValue->getPosition().x + labelPriceValue->getContentSize().width + 5,labelPriceValue->getPosition().y));
	panelbackGround->addChild(image_SellPriceType);
	////////////////
	/*
	const char *str_powerCount = StringDataManager::getString("equipment_info_powerCount");
	UILabel * m_powerCount=UILabel::create();
	m_powerCount->setAnchorPoint(ccp(0,0));
	m_powerCount->setText(str_powerCount);
	m_powerCount->setFontSize(16);
	m_powerCount->setColor(ccc3(101,255,221));
	m_powerCount->setPosition(ccp(16,equipPriceLabel->getPosition().y - 20));
	panelbackGround->addChild(m_powerCount);
	int fightCapacity_ =goodsInfo->equipmentdetail().fightcapacity();
	char figStr[10];
	sprintf(figStr,"%d",fightCapacity_);
	UILabel * label_powercount=UILabel::create();
	label_powercount->setAnchorPoint(ccp(0,0));
	label_powercount->setText(figStr);
	label_powercount->setFontSize(16);
	label_powercount->setColor(ccc3(101,255,221));
	label_powercount->setPosition(ccp(m_powerCount->getContentSize().width+20,equipPriceLabel->getPosition().y - 20));
	panelbackGround->addChild(label_powercount);
	*/
	///////////////////////////////////base property list
	UIImageView * boundary_touch_first= UIImageView::create();
	boundary_touch_first->setAnchorPoint(ccp(0.5f,0.5f));
	boundary_touch_first->setTexture("res_ui/wupinzhuangbeixiangqing/gaoguang.png");
	boundary_touch_first->setPosition(ccp(panelbackGround->getContentSize().width/2,image_SellPriceType->getPosition().y - 10));
	panelbackGround->addChild(boundary_touch_first);

	const char *str_equipPropertyBase = StringDataManager::getString("equipment_info_property_base");
	UILabel * label_equipProperty_base=UILabel::create();
	label_equipProperty_base->setAnchorPoint(ccp(0,0));
	label_equipProperty_base->setFontSize(16);
	label_equipProperty_base->setText(str_equipPropertyBase);
	label_equipProperty_base->setColor(ccc3(228,173,4));
	label_equipProperty_base->setPosition(ccp(w_space,boundary_touch_first->getPosition().y - 28));
	panelbackGround->addChild(label_equipProperty_base);

	//show activeValue
	boundary_Active= UIImageView::create();
	boundary_Active->setAnchorPoint(ccp(0.5f,0.5f));
	boundary_Active->setTexture("res_ui/wupinzhuangbeixiangqing/gaoguang.png");
	panelbackGround->addChild(boundary_Active);

	const char *str_equipPropertyactivate = StringDataManager::getString("equipment_info_property_activate");
	label_equipProperty_active=UILabel::create();
	label_equipProperty_active->setAnchorPoint(ccp(0,0));
	label_equipProperty_active->setFontSize(16);
	label_equipProperty_active->setColor(ccc3(228,173,4));
	label_equipProperty_active->setText(str_equipPropertyactivate);
	panelbackGround->addChild(label_equipProperty_active);
	
	for (int i=0;i<5;i++)
	{
		const char *str_label = StringDataManager::getString("equip_showActive_label");

 		const char *str_first_value = StringDataManager::getString("equip_showActive_labelValue");

		std::string strings_ = "";
		if (i==0)
		{
			strings_.append(str_first_value);
		}else
		{
			int num = i*5;
			char numStr[50];
			sprintf(numStr,str_label,num);
			strings_.append(numStr);
		}
		
		activePropertyDes=UILabel::create();
		activePropertyDes->setAnchorPoint(ccp(0,0));
		activePropertyDes->setFontSize(16);
		activePropertyDes->setText(strings_.c_str());
		activePropertyDes->setColor(ccc3(255,255,172));
		activePropertyDes->setTag(EQUIPMENTACTIVETAG+100*index+i);
		panelbackGround->addChild(activePropertyDes);
	}
	//show gemValue
	boundary_Gem= UIImageView::create();
	boundary_Gem->setAnchorPoint(ccp(0.5f,0.5f));
	boundary_Gem->setTexture("res_ui/wupinzhuangbeixiangqing/gaoguang.png");
	panelbackGround->addChild(boundary_Gem);

	const char *str_equipPropertygem = StringDataManager::getString("equipment_info_property_gem");
	label_equipProperty_gem=UILabel::create();
	label_equipProperty_gem->setAnchorPoint(ccp(0,0));
	label_equipProperty_gem->setFontSize(16);
	label_equipProperty_gem->setColor(ccc3(228,173,4));
	label_equipProperty_gem->setText(str_equipPropertygem);
	panelbackGround->addChild(label_equipProperty_gem);
	const char *str_ = StringDataManager::getString("equip_showgem_labelhole");
	for (int i=0;i<3;i++)
	{
		gemPropertyDes=UILabel::create();
		gemPropertyDes->setText(str_);
		gemPropertyDes->setAnchorPoint(ccp(0,0));
		gemPropertyDes->setFontSize(16);
		gemPropertyDes->setTag(EQUIPMENTGEMTAG+100*index+i);
		panelbackGround->addChild(gemPropertyDes);
	}

	///refresh value
	for (int i=0;i<goodsInfo->equipmentdetail().properties_size();i++)
	{
		switch (goodsInfo->equipmentdetail().properties(i).type())
		{
		case 1:
			{
				//base property list
				std::string baseName = goodsInfo->equipmentdetail().properties(i).name();
				baseName.append(":");

				basePropertyDes=UILabel::create();
				basePropertyDes->setAnchorPoint(ccp(0,0));
				basePropertyDes->setFontSize(16);
				basePropertyDes->setColor(ccc3(255,255,172));
				basePropertyDes->setText(baseName.c_str());
				basePropertyDes->setPosition(ccp(w_space,label_equipProperty_base->getPosition().y - 20*(baseValueNum+1)));//50
				panelbackGround->addChild(basePropertyDes);

				std::string basePropertyValueStr="";
				for (int propindex=0;propindex<goodsInfo->equipmentdetail().properties(i).value_size();propindex++)
				{
					char basePropertyStr[10];
					sprintf(basePropertyStr,"%d",goodsInfo->equipmentdetail().properties(i).value(propindex));
					if (propindex==0)
					{
						basePropertyValueStr.append(basePropertyStr);
					}else
					{
						basePropertyValueStr.append("(+");
						basePropertyValueStr.append(basePropertyStr);
						basePropertyValueStr.append(")");
					}
				}
				basePropertyValue=UILabel::create();
				basePropertyValue->setAnchorPoint(ccp(0,0));
				basePropertyValue->setFontSize(16);
				basePropertyValue->setColor(ccc3(255,255,172));
				basePropertyValue->setText(basePropertyValueStr.c_str());
				basePropertyValue->setPosition(ccp(basePropertyDes->getPosition().x+basePropertyDes->getContentSize().width+4,basePropertyDes->getPosition().y));
				panelbackGround->addChild(basePropertyValue);
				baseValueNum++;
			}break;
		case 2:
			{
				////star property list
				if (starValueNum==0)
				{
					boundary_Star= UIImageView::create();
					boundary_Star->setAnchorPoint(ccp(0.5f,0.5f));
					boundary_Star->setTexture("res_ui/wupinzhuangbeixiangqing/gaoguang.png");
					boundary_Star->setPosition(ccp(panelbackGround->getContentSize().width/2,basePropertyDes->getPosition().y - 10));
					panelbackGround->addChild(boundary_Star);

					const char *str_equipPropertyStar = StringDataManager::getString("equipment_info_property_star");
					label_equipProperty_star=UILabel::create();
					label_equipProperty_star->setAnchorPoint(ccp(0,0));
					label_equipProperty_star->setFontSize(16);
					label_equipProperty_star->setColor(ccc3(228,173,4));
					label_equipProperty_star->setText(str_equipPropertyStar);
					label_equipProperty_star->setPosition(ccp(w_space,boundary_Star->getPosition().y - 28));
					panelbackGround->addChild(label_equipProperty_star);
				}
			
				std::string starDes_begine = goodsInfo->equipmentdetail().properties(i).name().substr(0,3);
				starPropertyDes=UILabel::create();
				starPropertyDes->setAnchorPoint(ccp(0,0));
				starPropertyDes->setFontSize(16);
				starPropertyDes->setText(starDes_begine.c_str());
				starPropertyDes->setColor(ccc3(255,255,172));
				starPropertyDes->setPosition(ccp(w_space,label_equipProperty_star->getPosition().y -20*(starValueNum+1)));//50
				panelbackGround->addChild(starPropertyDes);

				std::string starDes_end = goodsInfo->equipmentdetail().properties(i).name().substr(3,6);
				starDes_end.append(":");
				UILabel * starPropertyDes_end =UILabel::create();
				starPropertyDes_end->setAnchorPoint(ccp(1,0));
				starPropertyDes_end->setFontSize(16);
				starPropertyDes_end->setText(starDes_end.c_str());
				starPropertyDes_end->setColor(ccc3(255,255,172));
				starPropertyDes_end->setPosition(ccp(w_space+68,starPropertyDes->getPosition().y));//50
				panelbackGround->addChild(starPropertyDes_end);

				std::string starPropertyValueStr="";
				int starPropertySize = goodsInfo->equipmentdetail().properties(i).value_size();
				for (int valueIndex=0;valueIndex<starPropertySize;valueIndex++)
				{
					char basePropertyStr[10];
					sprintf(basePropertyStr,"%d",goodsInfo->equipmentdetail().properties(i).value(valueIndex));
				
					if (valueIndex==0)
					{
						starPropertyValueStr.append(basePropertyStr);
					}else
					{
						starPropertyValueStr.append("(+");
						starPropertyValueStr.append(basePropertyStr);
						starPropertyValueStr.append(")");
					}
				}

				starPropertyValue=UILabel::create();
				starPropertyValue->setAnchorPoint(ccp(0,0));
				starPropertyValue->setFontSize(16);
				starPropertyValue->setColor(ccc3(255,255,172));
				starPropertyValue->setText(starPropertyValueStr.c_str());
				starPropertyValue->setPosition(ccp(starPropertyDes_end->getPosition().x+4,starPropertyDes->getPosition().y));
				panelbackGround->addChild(starPropertyValue);
				starValueNum++;
			}break;
		case 3:
			{
				std::string activeDes_byte = goodsInfo->equipmentdetail().properties(i).name();
				activeDes_byte.append(":");
				UILabel * label_activeDes =(UILabel *)panelbackGround->getChildByTag(EQUIPMENTACTIVETAG+100*index+activeValueNum);
				label_activeDes->setText(activeDes_byte.c_str());

				if (goodsInfo->equipmentdetail().properties(i).name().size() <= 6)//string size is > 6 ,string byte =  4 
				{
					std::string activeDes_begin = goodsInfo->equipmentdetail().properties(i).name().substr(0,3);
					label_activeDes->setText(activeDes_begin.c_str());

					std::string activeDes_end = goodsInfo->equipmentdetail().properties(i).name().substr(3,6);
					activeDes_end.append(":");
					UILabel * label_activeDes_End =UILabel::create();
					label_activeDes_End->setAnchorPoint(ccp(1,0));
					label_activeDes_End->setFontSize(16);
					label_activeDes_End->setText(activeDes_end.c_str());
					label_activeDes_End->setColor(ccc3(255,255,172));
					label_activeDes_End->setTag(EQUIPMENTACTIVE_END_TAG+100*index+activeValueNum); 
					label_activeDes_End->setPosition(ccp(w_space+68,starPropertyDes->getPosition().y));//50
					panelbackGround->addChild(label_activeDes_End);
				}

				int activeProperty =goodsInfo->equipmentdetail().properties(i).value(0);
				char activePropertyStr[10];
				sprintf(activePropertyStr,"%d",activeProperty);

				activePropertyValue=UILabel::create();
				activePropertyValue->setAnchorPoint(ccp(0,0));
				activePropertyValue->setColor(ccc3(255,255,172));
				activePropertyValue->setFontSize(16);
				activePropertyValue->setText(activePropertyStr);
				activePropertyValue->setTag(EQUIPMENTACTIVEVALUETAG+100*index+activeValueNum);
				panelbackGround->addChild(activePropertyValue);
				activeValueNum++;
			}break;
		case 4:
			{
				UILabel * label_gemDes =(UILabel *)panelbackGround->getChildByTag(EQUIPMENTGEMTAG+100*index+gemValueNum);
				int gemProperty =goodsInfo->equipmentdetail().properties(i).value(0);
				if (gemProperty > 0)
				{
					std::string gemIconStr_ = goodsInfo->equipmentdetail().properties(i).description();
					std::string gemIcon ="res_ui/props_icon/";
					gemIcon.append(gemIconStr_);
					gemIcon.append(".png");

					UIImageView * imageStarIcon = UIImageView::create();
					imageStarIcon->setTexture(gemIcon.c_str());
					imageStarIcon->setAnchorPoint(ccp(0,0));
					imageStarIcon->setPosition(ccp(0,0));
					imageStarIcon->setTag(EQUIPMENTGEMICONTAG+ 100*index + gemValueNum);
					imageStarIcon->setScale(0.4f);
					panelbackGround->addChild(imageStarIcon);

					std::string gemDes = goodsInfo->equipmentdetail().properties(i).name();
					label_gemDes->setColor(ccc3(113,227,255));
					label_gemDes->setText(gemDes.c_str());

					char gemPropertyStr[10];
					sprintf(gemPropertyStr,"%d",gemProperty);
					std::string gemPropertyValueStr_ ="";
					gemPropertyValueStr_.append("+");
					gemPropertyValueStr_.append(gemPropertyStr);

					UILabel *gemPropertyValue=UILabel::create();
					gemPropertyValue->setAnchorPoint(ccp(0,0));
					gemPropertyValue->setFontSize(16);
					gemPropertyValue->setText(gemPropertyValueStr_.c_str());
					gemPropertyValue->setColor(ccc3(113,227,255));
					gemPropertyValue->setTag(EQUIPMENTGEMVALUETAG+100*index+gemValueNum);
					panelbackGround->addChild(gemPropertyValue);
				}else
				{
					const char *strings_ = StringDataManager::getString("equip_showgem_labelKaiKong");
					label_gemDes->setText(strings_);
				}
				gemValueNum++;
			}break;
		case 5:
		case 6:
			{
				/////////////////////suit
				if (suitValueNum==0)
				{
					boundary_Suit= UIImageView::create();
					boundary_Suit->setAnchorPoint(ccp(0.5f,0.5f));
					boundary_Suit->setTexture("res_ui/wupinzhuangbeixiangqing/gaoguang.png");
					panelbackGround->addChild(boundary_Suit);

					const char *str_equipSuit = StringDataManager::getString("equipment_info_property_suit");
					label_equipProperty_suit=UILabel::create();
					label_equipProperty_suit->setAnchorPoint(ccp(0,0));
					label_equipProperty_suit->setFontSize(16);
					label_equipProperty_suit->setColor(ccc3(228,173,4));
					label_equipProperty_suit->setText(str_equipSuit);
					panelbackGround->addChild(label_equipProperty_suit);
				}
				//
				UILabel * suitAmountLabel=UILabel::create();
				suitAmountLabel->setAnchorPoint(ccp(0,0));
				suitAmountLabel->setFontSize(16);
				suitAmountLabel->setColor(ccc3(199,199,199));
				//suitAmountLabel->setText(suitDes.c_str());
				suitAmountLabel->setTag(EQUIPMENTSUITNAME+100*index+suitValueNum);
				panelbackGround->addChild(suitAmountLabel);

				std::string suitDes = goodsInfo->equipmentdetail().properties(i).name();
				UILabel *suitPropertyDes=UILabel::create();
				suitPropertyDes->setAnchorPoint(ccp(0,0));
				suitPropertyDes->setFontSize(16);
				suitPropertyDes->setColor(ccc3(199,199,199));
				suitPropertyDes->setText(suitDes.c_str());
				suitPropertyDes->setTag(EQUIPMENTSUITTAG+100*index+suitValueNum);
				panelbackGround->addChild(suitPropertyDes);

				int suitProperty =goodsInfo->equipmentdetail().properties(i).value(0);
				char suitPropertyStr[10];
				sprintf(suitPropertyStr,"%d",suitProperty);
				
				UILabel *suitPropertyValue=UILabel::create();
				suitPropertyValue->setAnchorPoint(ccp(0,0));
				suitPropertyValue->setFontSize(16);
				suitPropertyValue->setText(suitPropertyStr);
				suitPropertyValue->setTag(EQUIPMENTSUITVALUETAG+100*index+suitValueNum);
				panelbackGround->addChild(suitPropertyValue);
				
				suitValueNum++;
			}break;
		}  
	}

	boundary_Active->setPosition(ccp(panelbackGround->getContentSize().width/2,starPropertyValue->getPosition().y - 10));
	label_equipProperty_active->setPosition(ccp(w_space,boundary_Active->getPosition().y - 28));
	for (int i=0;i<activeValueNum;i++)
	{
		UILabel * label_activeDes =(UILabel *)panelbackGround->getChildByTag(EQUIPMENTACTIVETAG+100*index+i);
		label_activeDes->setPosition(ccp(w_space,label_equipProperty_active->getPosition().y -20*(i+1)));

		int x_ = label_activeDes->getPosition().x+ label_activeDes->getContentSize().width;

		if (panelbackGround->getChildByTag(EQUIPMENTACTIVE_END_TAG+100*index+i))
		{
			UILabel * label_activeDes_end =(UILabel *)panelbackGround->getChildByTag(EQUIPMENTACTIVE_END_TAG+100*index+i);
			label_activeDes_end->setPosition(ccp(w_space+68,label_activeDes->getPosition().y));

			x_ = w_space + 68;
		}
		
		UILabel * label_activeValue =(UILabel*)panelbackGround->getChildByTag(EQUIPMENTACTIVEVALUETAG+100*index+i);
		label_activeValue->setPosition(ccp(x_+4,label_activeDes->getPosition().y));
	}

	for(int i=0;i<5-activeValueNum;i++)
	{
		UILabel * label_activeDes =(UILabel *)panelbackGround->getChildByTag(EQUIPMENTACTIVETAG+100*index+activeValueNum+i);
		label_activeDes->setPosition(ccp(w_space,label_equipProperty_active->getPosition().y -20*(activeValueNum+i+1)));
	}

	//boundary_Gem  label_equipProperty_gem
	boundary_Gem->setPosition(ccp(panelbackGround->getContentSize().width/2,activePropertyDes->getPosition().y -10));
	label_equipProperty_gem->setPosition(ccp(w_space,boundary_Gem->getPosition().y -28));
	for (int i=0;i<gemValueNum;i++)
	{
		UIImageView * imageGemIcon = (UIImageView *)panelbackGround->getChildByTag(EQUIPMENTGEMICONTAG+100*index+i);
		UILabel * label_gemDes =(UILabel *)panelbackGround->getChildByTag(EQUIPMENTGEMTAG+100*index+i);
		if (imageGemIcon != NULL)
		{
			imageGemIcon->setPosition(ccp(w_space,label_equipProperty_gem->getPosition().y - 20* (i+1)));
			label_gemDes->setPosition(ccp(imageGemIcon->getPosition().x+imageGemIcon->getContentSize().width *0.4,label_equipProperty_gem->getPosition().y - 20* (i+1)));
		}else
		{
			label_gemDes->setPosition(ccp(w_space,label_equipProperty_gem->getPosition().y - 20* (i+1)));
		}
		UILabel * label_gemValue =(UILabel *)panelbackGround->getChildByTag(EQUIPMENTGEMVALUETAG+100*index+i);
		if (label_gemValue!= NULL)
		{
			label_gemValue->setPosition(ccp(label_gemDes->getPosition().x +label_gemDes->getContentSize().width,label_gemDes->getPosition().y));
		}
	}
	for (int i=0;i<3 - gemValueNum;i++)
	{
		UILabel * label_gemDes =(UILabel *)panelbackGround->getChildByTag(EQUIPMENTGEMTAG+100*index+gemValueNum+i);
		label_gemDes->setPosition(ccp(w_space,label_equipProperty_gem->getPosition().y - 20* (gemValueNum + i+1)));
	}

	///suit
	if (isHavesuibt ==true)
	{
		boundary_Suit->setPosition(ccp(panelbackGround->getContentSize().width/2,gemPropertyDes->getPosition().y - 10));
		label_equipProperty_suit->setPosition(ccp(w_space,boundary_Suit->getPosition().y - 28));
		int selfEquipSuitHighLight = 0;
		//set suit isHighLight
		if (index == 0)
		{
			QuiryUI * quiryui = (QuiryUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagQuiryUI);
			if (quiryui != NULL)
			{
				selfEquipSuitHighLight = quiryui->getRoleEquipAmount()-1;
			}else
			{
				if (isSetSuitColor == true)
				{
					selfEquipSuitHighLight = selfEquipSuit-1;
				}
			}
		}

		if (index == 1)
		{
			selfEquipSuit = 0;
			if (isSetSuitColor == true)
			{
				for (int suitIndex=0;suitIndex<selfEquipVector.size();suitIndex++)
				{
					if (goodsInfo->equipmentdetail().suitid() == selfEquipVector.at(suitIndex)->goods().equipmentdetail().suitid())
					{
						selfEquipSuit++;
					}
				}
				selfEquipSuitHighLight = selfEquipSuit-1;
			}
		}
		int suitAmout_ = selfEquipSuitHighLight+1;
		for (int i=0;i<suitValueNum;i++)
		{
			const char *str_equipSuit = StringDataManager::getString("equipment_info_property_suitDescription");
			char suitAmount[5];
			if (suitAmout_ > i+2 )
			{
				sprintf(suitAmount,"%d",i+2);
			}else
			{
				sprintf(suitAmount,"%d",suitAmout_);
			}
			char suitIndex[5];
			sprintf(suitIndex,"%d",i+2);
			
			std::string suitDes = goodsInfo->name().substr(0,6);
			suitDes.append(str_equipSuit);
			suitDes.append("+(");
			suitDes.append(suitAmount);
			suitDes.append("/");
			suitDes.append(suitIndex);
			suitDes.append("):");

			UILabel * label_suitName =(UILabel *)panelbackGround->getChildByTag(EQUIPMENTSUITNAME+100*index+i);
			label_suitName->setText(suitDes.c_str());
			label_suitName->setPosition(ccp(w_space,label_equipProperty_suit->getPosition().y -20*(i+1)));

			UILabel * label_suitDes =(UILabel *)panelbackGround->getChildByTag(EQUIPMENTSUITTAG+100*index+i);
			label_suitDes->setPosition(ccp(label_suitName->getPosition().x+label_suitName->getContentSize().width+4,label_suitName->getPosition().y));
			UILabel * label_suitValue =(UILabel *)panelbackGround->getChildByTag(EQUIPMENTSUITVALUETAG+100*index+i);
			label_suitValue->setPosition(ccp(label_suitDes->getPosition().x+label_suitDes->getContentSize().width,label_suitDes->getPosition().y ));
			
			if (selfEquipSuitHighLight >0)
			{
				label_suitName->setColor(ccc3(211,5,255));
				label_suitDes->setColor(ccc3(211,5,255));
				label_suitValue->setColor(ccc3(211,5,255));
				selfEquipSuitHighLight--;
			}
		}
	}
}

void GoodsItemInfoBase::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void GoodsItemInfoBase::onExit()
{
	UIScene::onExit();
}

bool GoodsItemInfoBase::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	return resignFirstResponder(pTouch,this,false);
}

void GoodsItemInfoBase::showGeneralEquipMentInfo( CCObject * obj )
{
	UIButton * rolebutton_ =(UIButton *)m_pUiLayer->getWidgetByTag(10);
	UIImageView * roleSelectImage = (UIImageView *)rolebutton_->getChildByTag(1101);
	if (roleSelectImage != NULL)
	{
		roleSelectImage->removeFromParentAndCleanup(true);
	}
	
	int generalNum = GameView::getInstance()->generalsInLineList.size();
	for (int btnIndex =0;btnIndex< generalNum;btnIndex++)
	{
		UIButton * button_ =(UIButton*)m_pUiLayer->getWidgetByTag(btnIndex);
		UIImageView * roleSelectImage = (UIImageView *)button_->getChildByTag(1101);
		if (roleSelectImage != NULL)
		{
			roleSelectImage->removeFromParentAndCleanup(true);
		}
	}
	UIButton * btn =(UIButton *)obj;
	// add the selected flag
	UIImageView * roleSelectButton_ =UIImageView::create();
	roleSelectButton_->setScale9Enable(true);
	roleSelectButton_->setScale9Size(CCSizeMake(142,62));
	roleSelectButton_->setCapInsets(CCRect(15,15,1,1));
	roleSelectButton_->setTexture("res_ui/highlight.png");
	roleSelectButton_->setAnchorPoint(ccp(0.5f,0.5f));
	roleSelectButton_->setPosition(ccp(0,0));
	roleSelectButton_->setTag(1101);
	btn->addChild(roleSelectButton_);
	
	//if package
	PackageScene * packageui = (PackageScene *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);	
	int index_ =btn->getTag();
	bodyPanelInfo->removeAllChildren();

	std::string selectActorPressionStr_ = "";
	int m_level = 0;
	if (index_==10)
	{
		selectActorPressionStr_ = GameView::getInstance()->myplayer->getActiveRole()->profession();
		m_level =  GameView::getInstance()->myplayer->getActiveRole()->level();
		bool isExitequip = false;
		for (int i=0;i<GameView::getInstance()->EquipListItem.size();i++)
		{
			if (goodsInfo_General->equipmentclazz()==GameView::getInstance()->EquipListItem.at(i)->goods().equipmentclazz())
			{
				GoodsInfo * playerEquip = new GoodsInfo();
				playerEquip->CopyFrom(GameView::getInstance()->EquipListItem.at(i)->goods());
				this->showTouchEquipMentInfo(playerEquip,1);// show player equipment goodsinfo
				int width = touchPanelInfo->getContentSize().width + bodyPanelInfo->getContentSize().width;
				if (touchPanelInfo->getContentSize().height > bodyPanelInfo->getContentSize().height)
				{
					touchPanelInfo->setPosition(ccp(0,touchPanelInfo->getContentSize().height));
					bodyPanelInfo->setPosition(ccp(touchPanelInfo->getContentSize().width,touchPanelInfo->getContentSize().height));
					scrollView_info->setInnerContainerSize(CCSizeMake(width, touchPanelInfo->getContentSize().height));
				}else
				{
					touchPanelInfo->setPosition(ccp(0,bodyPanelInfo->getContentSize().height));
					bodyPanelInfo->setPosition(ccp(touchPanelInfo->getContentSize().width,bodyPanelInfo->getContentSize().height));
					scrollView_info->setInnerContainerSize(CCSizeMake(width, bodyPanelInfo->getContentSize().height));
				}
				delete playerEquip;
				isExitequip =true;
			}
		}
		if (isExitequip ==true)
		{
			notEquipment->setVisible(false);
			flag_equip->setVisible(true);
		}else
		{
			notEquipment->setVisible(true);
			flag_equip->setVisible(false);
		}

		if (packageui != NULL)
		{
			packageui->generalList_tableView->selectCell(0);
		}
	}else
	{
		long long generalIndex_ = GameView::getInstance()->generalsInLineList.at(index_)->id();
		
		bool isEquipment = false;
		for (int i=0;i<GameView::getInstance()->generalsInLineDetailList.size();i++)
		{
			long long generalId = GameView::getInstance()->generalsInLineDetailList.at(i)->generalid();
			if (generalIndex_ == generalId)
			{
				selectActorPressionStr_ = GameView::getInstance()->generalsInLineDetailList.at(i)->profession();
				m_level = GameView::getInstance()->generalsInLineDetailList.at(i)->activerole().level();
				for (int j=0;j<GameView::getInstance()->generalsInLineDetailList.at(i)->equipments_size();j++)
				{
					if (goodsInfo_General->equipmentclazz() == GameView::getInstance()->generalsInLineDetailList.at(i)->equipments(j).goods().equipmentclazz())
					{
						GoodsInfo * playerEquip = new GoodsInfo();
						playerEquip->CopyFrom(GameView::getInstance()->generalsInLineDetailList.at(i)->equipments(j).goods());
						this->showTouchEquipMentInfo(playerEquip,1);// show player equipment goodsinfo

						int width = touchPanelInfo->getContentSize().width + bodyPanelInfo->getContentSize().width;
						if (touchPanelInfo->getContentSize().height > bodyPanelInfo->getContentSize().height)
						{
							touchPanelInfo->setPosition(ccp(0,touchPanelInfo->getContentSize().height));
							bodyPanelInfo->setPosition(ccp(touchPanelInfo->getContentSize().width,touchPanelInfo->getContentSize().height));
							scrollView_info->setInnerContainerSize(CCSizeMake(width, touchPanelInfo->getContentSize().height));
						}else
						{
							touchPanelInfo->setPosition(ccp(0,bodyPanelInfo->getContentSize().height));
							bodyPanelInfo->setPosition(ccp(touchPanelInfo->getContentSize().width,bodyPanelInfo->getContentSize().height));
							scrollView_info->setInnerContainerSize(CCSizeMake(width, bodyPanelInfo->getContentSize().height));
						}
						delete playerEquip;

						isEquipment =true;
						break;
					}
				}
			}
		}
		if (isEquipment == false)
		{
			notEquipment->setVisible(true);
			flag_equip->setVisible(false);
		}else
		{
			notEquipment->setVisible(false);
			flag_equip->setVisible(true);
		}
		
		if (packageui != NULL)
		{
			packageui->generalList_tableView->selectCell(index_+1);
		}
	}
	int pressionIndex_ = BasePlayer::getProfessionIdxByName(selectActorPressionStr_.c_str());
	this->setEquipProfessionAndLevelColor(pressionIndex_,m_level);

	this->isTouchSameEquipment();
}

std::string GoodsItemInfoBase::getEquipmentQualityByIndex( int quality )
{
	std::string frameColorPath = "res_ui/";
	switch(quality)
	{
	case 1:
		{
			frameColorPath.append("smdi_white");
		}break;
	case 2:
		{
			frameColorPath.append("smdi_green");
		}break;
	case 3:
		{
			frameColorPath.append("smdi_bule");
		}break;
	case 4:
		{
			frameColorPath.append("smdi_purple");
		}break;
	case 5:
		{
			frameColorPath.append("smdi_orange");
		}break;
	default:
		frameColorPath.append("smdi_white");
	}
	frameColorPath.append(".png");
	return frameColorPath;
}

cocos2d::ccColor3B GoodsItemInfoBase::getEquipmentColorByQuality( int quality )
{
	ccColor3B color_;
	switch(quality)
	{
	case 1:
		{
			color_ =ccc3(255,255,255);
		}break;
	case 2:
		{
			color_=ccc3(136,234,31);
		}break;
	case 3:
		{
			color_=ccc3(15,202,250);
		}break;
	case 4:
		{
			color_=ccc3(255,62,253);
		}break;
	case 5:
		{
			color_=ccc3(250,155,15);
		}break;
	default:
		color_ =ccc3(255,255,255);
	}

	return color_;
}

void GoodsItemInfoBase::getSelfEquipVector(GoodsInfo * goodsInfo,std::vector<CEquipment *> vector_)
{
	selfEquipSuit = 0;
	for (int i=0;i<vector_.size();i++)
	{
		CEquipment * selfEquip = new CEquipment();
		selfEquip->CopyFrom(*vector_.at(i));

		if (goodsInfo->instanceid()==vector_.at(i)->goods().instanceid())
		{
			isSetSuitColor = true;
		}
		
		if (goodsInfo->equipmentdetail().suitid() == vector_.at(i)->goods().equipmentdetail().suitid())
		{
			selfEquipSuit++;
		}
		
		selfEquipVector.push_back(selfEquip);
	}
	
	/*
	PackageScene * packscene = (PackageScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
	if (packscene != NULL )
	{
		if (packscene->selectActorId  == 0)
		{
			this->getRoleEquipmentList();
		}else
		{
			this->getGeneralEquipmentList(packscene->selectActorId);
		}
	}else
	{
		ChatUI * chatui_ = (ChatUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTabChat);
		if (chatui_ != NULL)
		{
			UILayer * layer_ = (UILayer *)chatui_->getChildByTag(CHATUILAYER3);
			BackPackage * backpack_ = (BackPackage *)layer_->getChildByTag(PACKAGEAGE);
			if (backpack_ != NULL)
			{
				if (backpack_->curRoleIndex == 0)
				{
					this->getRoleEquipmentList();
				}else
				{
					long long generalId_ = GameView::getInstance()->generalsInLineDetailList.at(backpack_->curRoleIndex - 1)->generalid();
					this->getGeneralEquipmentList(generalId_);
				}
			}else
			{
				this->getRoleEquipmentList();
			}
		}else
		{
			this->getRoleEquipmentList();
		}
	}
	//
	isRoleEquip_ = false;
	isSetSuitColor = false;
	//是否是套装 套装suitid suitamount
	for (int suitIndex=0;suitIndex<selfEquipVector.size();suitIndex++)
	{
		if (goodsInfo_General->equipmentdetail().suitid() == selfEquipVector.at(suitIndex)->goods().equipmentdetail().suitid())
		{
			selfEquipSuit++;
		}

		if (strcmp(goodsInfo_General->id().c_str(),selfEquipVector.at(suitIndex)->goods().id().c_str())==0)
		{
			isSetSuitColor = true;
		}

		if (goodsInfo_General->equipmentclazz()== selfEquipVector.at(suitIndex)->goods().equipmentclazz())
		{
			isRoleEquip_ =true;
		}
	}
	*/
}

void GoodsItemInfoBase::isEquipOfBody( GoodsInfo * goods )
{
	/*
	isRoleEquip_ = false;
	isSetSuitColor = false;
	for (int i=0;i<selfEquipVector.size();i++)
	{
		if (strcmp(goods->id().c_str(),selfEquipVector.at(i)->goods().id().c_str())==0)
		{
			isSetSuitColor = true;
		}

		if (goods->equipmentclazz()== selfEquipVector.at(i)->goods().equipmentclazz())
		{
			isRoleEquip_ =true;
		}
	}
	*/
}

void GoodsItemInfoBase::setEquipProfessionAndLevelColor( int pression_,int level_ )
{
	UILabel * labelPre = (UILabel *) touchPanelInfo->getChildByName("toucheEquipPression");
	if (pression_ == goodsInfo_General->equipmentdetail().profession())
	{
		labelPre->setColor(ccc3(39,238,194));
	}else
	{
		labelPre->setColor(ccc3(255,0,0));
	}

	UILabel * labelLevel_ = (UILabel *)touchPanelInfo->getChildByName("toucheEquipLevel");
	if (level_ >= goodsInfo_General->equipmentdetail().usergrade())
	{
		labelLevel_->setColor(ccc3(101,255,221));
	}else
	{
		labelLevel_->setColor(ccc3(255,0,0));
	}

	//set button is not touch
	PackageItemInfo * packItem_ = (PackageItemInfo *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoodsItemInfoBase);
	if (packItem_)
	{
		if (pression_ == goodsInfo_General->equipmentdetail().profession() && level_ >= goodsInfo_General->equipmentdetail().usergrade())
		{
			packItem_->setCurEquipIsUseAble(true);
		}else
		{
			packItem_->setCurEquipIsUseAble(false);
		}
	}
}

void GoodsItemInfoBase::isTouchSameEquipment()
{
	flag_toucheSelfEquip->setVisible(false);
	long long goodsINstanceId = goodsInfo_General->instanceid();
	for(int i=0;i<selfEquipVector.size();i++)
	{
		long long selfEquipInstanceId = selfEquipVector.at(i)->goods().instanceid();
		if (goodsINstanceId == selfEquipInstanceId)
		{
			flag_toucheSelfEquip->setVisible(true);
		}
	}
}

void GoodsItemInfoBase::addGeneralButton(long long generalId)
{
	UIButton * roleFrameBtn_= UIButton::create();
	roleFrameBtn_->setTouchEnable(true);
	roleFrameBtn_->setPressedActionEnabled(true);
	roleFrameBtn_->addReleaseEvent(this,coco_releaseselector(GoodsItemInfoBase::showGeneralEquipMentInfo));
	roleFrameBtn_->setAnchorPoint(ccp(0.5f,0.5f));
	roleFrameBtn_->setScale9Enable(true);
	roleFrameBtn_->setScale9Size(CCSizeMake(140,60));
	roleFrameBtn_->setCapInsets(CCRect(15,30,1,1));
	roleFrameBtn_->setPosition(ccp(548+70,330));
	roleFrameBtn_->setTextures("res_ui/kuang0_new.png","res_ui/kuang0_new.png","");
	roleFrameBtn_->setWidgetTag(10);
	m_pUiLayer->addWidget(roleFrameBtn_);

	UIImageView * roleFrameBg_ =UIImageView::create();
	roleFrameBg_->setTexture("res_ui/round.png");
	roleFrameBg_->setAnchorPoint(ccp(0.5f,0.5f));
	roleFrameBg_->setPosition(ccp(-40,0));
	roleFrameBtn_->addChild(roleFrameBg_);

	std::string iconPath_ = BasePlayer::getHeadPathByProfession(GameView::getInstance()->myplayer->getProfession()).c_str();
	UIImageView * roleIconImage_ =UIImageView::create();
	roleIconImage_->setTexture(iconPath_.c_str());
	roleIconImage_->setAnchorPoint(ccp(0.5f,0.5f));
	roleIconImage_->setPosition(ccp(0,0));
	roleIconImage_->setScale(0.8f);
	roleFrameBg_->addChild(roleIconImage_);

	UIImageView * roleLvbackGround = UIImageView::create();
	roleLvbackGround->setAnchorPoint(ccp(0,0));
	roleLvbackGround->setTexture("res_ui/zhezhao80.png");
	roleLvbackGround->setScale9Enable(true);
	roleLvbackGround->setPosition(ccp(-25,-20));
	//roleLvbackGround->setCapInsets(CCRect(5,10,1,4));
	roleLvbackGround->setScale9Size(CCSizeMake(32,14));
	roleFrameBtn_->addChild(roleLvbackGround);

	int roleLevelIndex_ = GameView::getInstance()->myplayer->getActiveRole()->level();
	char roleLevelStr[10];
	sprintf(roleLevelStr,"%d",roleLevelIndex_);
	std::string roleLevelString_ = "LV";
	roleLevelString_.append(roleLevelStr);
	UILabel * roleLvLabel = UILabel::create();
	roleLvLabel->setAnchorPoint(ccp(0,0));
	roleLvLabel->setPosition(ccp(-25,-20));
	roleLvLabel->setText(roleLevelString_.c_str());
	roleLvLabel->setFontSize(12);
	roleFrameBtn_->addChild(roleLvLabel);

	std::string roleNameStr_ = GameView::getInstance()->myplayer->getActiveRole()->rolebase().name();
	UILabel * roleNameLabel = UILabel::create();
	roleNameLabel->setAnchorPoint(ccp(0.5f,0));
	roleNameLabel->setPosition(ccp(-5 + roleFrameBg_->getContentSize().width/2,0));
	roleNameLabel->setText(roleNameStr_.c_str());
	roleNameLabel->setFontSize(18);
	roleFrameBtn_->addChild(roleNameLabel);

	std::string pressionStr_ = GameView::getInstance()->myplayer->getActiveRole()->profession();
	UILabel * rolePressionLabel = UILabel::create();
	rolePressionLabel->setAnchorPoint(ccp(0,0));
	rolePressionLabel->setPosition(ccp(10,-20));
	rolePressionLabel->setText(pressionStr_.c_str());
	rolePressionLabel->setFontSize(18);
	roleFrameBtn_->addChild(rolePressionLabel);
	
	// the image for the selected flag
	UIImageView * roleSelectButton_ =UIImageView::create();
	roleSelectButton_->setScale9Enable(true);
	roleSelectButton_->setScale9Size(CCSizeMake(142,62));
	roleSelectButton_->setCapInsets(CCRect(15,15,1,1));
	roleSelectButton_->setTexture("res_ui/highlight.png");
	roleSelectButton_->setAnchorPoint(ccp(0.5f,0.5f));
	roleSelectButton_->setTag(1101);
	roleSelectButton_->setPosition(ccp(0,0));

	// check if MyPlayer has been selected
	/*
	PackageScene * packscene = (PackageScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
	if (packscene != NULL )
	{
		if (packscene->selectActorId == 0)    // 0 means that MyPlayer is selected
		{
			roleFrameBtn_->addChild(roleSelectButton_);
		}
	}
	else
	{
		roleFrameBtn_->addChild(roleSelectButton_);
	}
	*/
	if (generalId ==0)// 0 means that MyPlayer is selected
	{
		roleFrameBtn_->addChild(roleSelectButton_);
		generalProfession = BasePlayer::getProfessionIdxByName(pressionStr_);
		generalLevel = GameView::getInstance()->myplayer->getActiveRole()->level();
	}
	
	int generalNum = GameView::getInstance()->generalsInLineList.size();
	for (int i=0;i<generalNum;i++)
	{	
		UIButton * generalBtn= UIButton::create();
		generalBtn->setTouchEnable(true);
		generalBtn->setPressedActionEnabled(true);
		generalBtn->addReleaseEvent(this,coco_releaseselector(GoodsItemInfoBase::showGeneralEquipMentInfo));
		generalBtn->setAnchorPoint(ccp(0.5f,0.5f));
		generalBtn->setScale9Enable(true);
		generalBtn->setScale9Size(CCSizeMake(140,60));
		generalBtn->setCapInsets(CCRect(15,30,1,1));
		generalBtn->setPosition(ccp(548+70,269 - 61*i));
		generalBtn->setTextures("res_ui/kuang0_new.png","res_ui/kuang0_new.png","");
		generalBtn->setWidgetTag(i);
		m_pUiLayer->addWidget(generalBtn);

		UIImageView * smaillFrame =UIImageView::create();
		smaillFrame->setTexture("res_ui/round.png");
		smaillFrame->setAnchorPoint(ccp(0.5f,0.5f));
		smaillFrame->setPosition(ccp(-40,0));
		generalBtn->addChild(smaillFrame);

		UIImageView * generalLvbackGround = UIImageView::create();
		generalLvbackGround->setAnchorPoint(ccp(0,0));
		generalLvbackGround->setTexture("res_ui/zhezhao80.png");
		generalLvbackGround->setScale9Enable(true);
		generalLvbackGround->setScale9Size(CCSizeMake(34,14));
		generalLvbackGround->setPosition(ccp(-28,-20));
		generalBtn->addChild(generalLvbackGround);

		int generalLevel_ = GameView::getInstance()->generalsInLineList.at(i)->level();
		char generalLevelStr[10];
		sprintf(generalLevelStr,"%d",generalLevel_);
		std::string generalLevelString_ = "LV";
		generalLevelString_.append(generalLevelStr);
		UILabel * generalLvLabel = UILabel::create();
		generalLvLabel->setAnchorPoint(ccp(0,0));
		generalLvLabel->setPosition(ccp(-25,-20));
		generalLvLabel->setText(generalLevelString_.c_str());
		generalLvLabel->setFontSize(12);
		generalBtn->addChild(generalLvLabel);

		CGeneralBaseMsg * generalMsgFromDb = GeneralsConfigData::s_generalsBaseMsgData[GameView::getInstance()->generalsInLineList.at(i)->modelid()];
		std::string generalNameStr_ = generalMsgFromDb->name();
		int generalIndexQuality = GameView::getInstance()->generalsInLineList.at(i)->currentquality();
		ccColor3B generalColor_ =GameView::getInstance()->getGeneralsColorByQuality(generalIndexQuality);
		UILabel * generalNameLabel = UILabel::create();
		generalNameLabel->setAnchorPoint(ccp(0.5f,0));
		generalNameLabel->setPosition(ccp(smaillFrame->getContentSize().width/2,0 ));
		generalNameLabel->setText(generalNameStr_.c_str());
		generalNameLabel->setFontSize(18);
		generalNameLabel->setColor(generalColor_);
		generalBtn->addChild(generalNameLabel);

		int generalPressionIndex_ = generalMsgFromDb->get_profession();
		std::string generalPressionStr_ = BasePlayer::getProfessionNameIdxByIndex(generalPressionIndex_);

		UILabel * generalPressionLabel = UILabel::create();
		generalPressionLabel->setAnchorPoint(ccp(0,0));
		generalPressionLabel->setPosition(ccp(10,-20));
		generalPressionLabel->setText(generalPressionStr_.c_str());
		generalPressionLabel->setFontSize(18);
		generalBtn->addChild(generalPressionLabel);

		// check if the General is selected
		if (GameView::getInstance()->generalsInLineList.at(i)->id() == generalId)
		{
			generalBtn->addChild(roleSelectButton_);
			generalProfession = BasePlayer::getProfessionIdxByName(generalPressionStr_);
			generalLevel = GameView::getInstance()->generalsInLineList.at(i)->level();
		}

		/*
		if (packscene != NULL )
		{
			long long generalIndex_ = GameView::getInstance()->generalsInLineList.at(i)->id();
			if (packscene->selectActorId == generalIndex_)
			{
				generalBtn->addChild(roleSelectButton_);
				generalProfession = BasePlayer::getProfessionIdxByName(generalPressionStr_);
			}
		}
		*/
		//general icon
		//CGeneralBaseMsg * generalBaseMsgFromDB  = GeneralsConfigData::s_generalsBaseMsgData[GameView::getInstance()->generalsInLineList.at(i)->modelid()];
		std::string sprite_icon_path = "res_ui/generals46X45/";
		sprite_icon_path.append(generalMsgFromDb->get_head_photo());
		sprite_icon_path.append(".png");

		UIImageView * generalIcon_ =UIImageView::create();
		generalIcon_->setTexture(sprite_icon_path.c_str());
		generalIcon_->setAnchorPoint(ccp(0.5f,0.5f));
		generalIcon_->setPosition(ccp(0,0));
		smaillFrame->addChild(generalIcon_);
	}
}

void GoodsItemInfoBase::getRoleEquipmentList()
{
	/*
	// show player equipment goodsinfo
	for (int i=0;i<GameView::getInstance()->EquipListItem.size();i++)
	{
		CEquipment * selfEquip = new CEquipment();
		selfEquip->CopyFrom(*GameView::getInstance()->EquipListItem.at(i));
		selfEquipVector.push_back(selfEquip);
	}
	*/
}

void GoodsItemInfoBase::getGeneralEquipmentList( long long generalId )
{
	/*
	for (int generalindex=0;generalindex <GameView::getInstance()->generalsInLineDetailList.size();generalindex++)
	{
		if (generalId == GameView::getInstance()->generalsInLineDetailList.at(generalindex)->generalid())
		{
			// show general equipment goodsinfo
			int generalEquipVectorSize = GameView::getInstance()->generalsInLineDetailList.at(generalindex)->equipments_size();
			for (int i=0;i<generalEquipVectorSize;i++)
			{
				CEquipment * selfEquip = new CEquipment();
				selfEquip->CopyFrom(GameView::getInstance()->generalsInLineDetailList.at(generalindex)->equipments(i));
				selfEquipVector.push_back(selfEquip);
			}
		}
	}
	*/
}

void GoodsItemInfoBase::callBackQualityDes( CCObject * obj )
{
	std::map<int,std::string>::const_iterator cIter;
	cIter = SystemInfoConfigData::s_systemInfoList.find(17);
	if (cIter == SystemInfoConfigData::s_systemInfoList.end())
	{
		return ;
	}
	else
	{
		if(GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesDetailInfo) == NULL)
		{
			CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
			ShowSystemInfo * showSystemInfo = ShowSystemInfo::create(SystemInfoConfigData::s_systemInfoList[17].c_str());
			GameView::getInstance()->getMainUIScene()->addChild(showSystemInfo,0,kTagStrategiesDetailInfo);
			showSystemInfo->ignoreAnchorPointForPosition(false);
			showSystemInfo->setAnchorPoint(ccp(0.5f, 0.5f));
			showSystemInfo->setPosition(ccp(winSize.width/2, winSize.height/2));
		}
	}
}
