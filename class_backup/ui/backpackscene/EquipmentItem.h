
#ifndef _BACKPACKSCENE_EQUIPMENT_H_
#define _BACKPACKSCENE_EQUIPMENT_H_
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"

USING_NS_CC;
USING_NS_CC_EXT;

class FolderInfo;
class CEquipment;

#define EquipVoidFramePath "res_ui/sdi_none.png"
#define EquipWhiteFramePath "res_ui/sdi_white.png"
#define EquipGreenFramePath "res_ui/sdi_green.png"
#define EquipBlueFramePath "res_ui/sdi_bule.png"
#define EquipPurpleFramePath "res_ui/sdi_purple.png"
#define EquipOrangeFramePath "res_ui/sdi_orange.png"

class EquipmentItem : public UIWidget
{
public:
	EquipmentItem();
	~EquipmentItem();
	static EquipmentItem * create(CEquipment* equipment);
	bool init(CEquipment* equipment);

	CEquipment * curEquipment;
protected:
	int index ;
	cocos2d::extension::UIImageView * imageView_background;

	void EquipmentItemEvent(CCObject * pSender);
	CCPoint autoGetPosition(CCObject * pSender);

	void setRed(bool isRed);
};
#endif;

