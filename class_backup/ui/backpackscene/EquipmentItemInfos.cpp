#include "EquipmentItemInfos.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "PackageItemInfo.h"
#include "PackageScene.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../messageclient/element/CEquipment.h"
#include "../equipMent_ui/EquipMentUi.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/element/CFunctionOpenLevel.h"
#include "AppMacros.h"
#include "../../messageclient/element/MapStarPickAmount.h"
#include "GameAudio.h"
#include "../../utils/GameUtils.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"

EquipmentItemInfos::EquipmentItemInfos()
{
	buttonImagePath = "res_ui/new_button_1.png";
	//pacTabviewSelectindex = 0;
}


EquipmentItemInfos::~EquipmentItemInfos()
{
	delete generalEquip;
}

EquipmentItemInfos * EquipmentItemInfos::create(CEquipment * equipment,long long generalId)
{
	EquipmentItemInfos * equipmentItem_info = new EquipmentItemInfos();
	if (equipmentItem_info && equipmentItem_info->init(equipment,generalId))
	{
		equipmentItem_info->autorelease();
		return equipmentItem_info;
	}
	CC_SAFE_DELETE(equipmentItem_info);
	return NULL;
}

bool EquipmentItemInfos::init(CEquipment * equipment,long long generalId)
{
	GoodsInfo * goods_ =new GoodsInfo();
	goods_->CopyFrom(equipment->goods());

	generalEquip = new CEquipment();
	generalEquip->CopyFrom(*equipment);
	m_generalId = generalId;

	std::vector<CEquipment *> equipVector;
	if (m_generalId != 0)
	{
		for (int i = 0;i<GameView::getInstance()->generalsInLineDetailList.size();i++)
		{
			if (GameView::getInstance()->generalsInLineDetailList.at(i)->generalid() == m_generalId)
			{
				for (int j=0;j<GameView::getInstance()->generalsInLineDetailList.at(i)->equipments_size() ;j++)
				{
					CEquipment * equip_ = new CEquipment();
					equip_->CopyFrom(GameView::getInstance()->generalsInLineDetailList.at(i)->equipments(j));	
					equipVector.push_back(equip_);
				}
			}
		}
	}else
	{
		for (int i=0;i<GameView::getInstance()->EquipListItem.size();i++)
		{
			CEquipment * equip_ = new CEquipment();
			equip_->CopyFrom( *GameView::getInstance()->EquipListItem.at(i));	
			equipVector.push_back(equip_);
		}
	}
	
	
	if (GoodsItemInfoBase::init(goods_,equipVector,m_generalId))
	{
		delete goods_;
		//pick star
		UIButton * Button_pickStar= UIButton::create();
		Button_pickStar->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		Button_pickStar->setTouchEnable(true);
		Button_pickStar->setPressedActionEnabled(true);
		Button_pickStar->addReleaseEvent(this,coco_releaseselector(EquipmentItemInfos::callBackPick));
		Button_pickStar->setAnchorPoint(ccp(0.5f,0.5f));
		Button_pickStar->setScale9Enable(true);
		Button_pickStar->setScale9Size(CCSizeMake(80,43));
		Button_pickStar->setCapInsets(CCRect(18,9,2,23));
		//Button_pickStar->setPosition(ccp(141,25));

		UILabel * Label_pick = UILabel::create();
		Label_pick->setText((const char *)"摘星");
		Label_pick->setFontName(APP_FONT_NAME);
		Label_pick->setFontSize(18);
		Label_pick->setAnchorPoint(ccp(0.5f,0.5f));
		Label_pick->setPosition(ccp(0,0));
		Button_pickStar->addChild(Label_pick);
		m_pUiLayer->addWidget(Button_pickStar);
		//repair equip
		UIButton * Button_repair= UIButton::create();
		Button_repair->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		Button_repair->setTouchEnable(true);
		Button_repair->setPressedActionEnabled(true);
		Button_repair->addReleaseEvent(this,coco_releaseselector(EquipmentItemInfos::callBackRepair));
		Button_repair->setAnchorPoint(ccp(0.5f,0.5f));
		Button_repair->setScale9Enable(true);
		Button_repair->setScale9Size(CCSizeMake(80,43));
		Button_repair->setCapInsets(CCRect(18,9,2,23));
		Button_repair->setPosition(ccp(141,25));
		m_pUiLayer->addWidget(Button_repair);

		UILabel * Label_repair = UILabel::create();
		Label_repair->setText((const char *)"修理");
		Label_repair->setFontName(APP_FONT_NAME);
		Label_repair->setFontSize(18);
		Label_repair->setAnchorPoint(ccp(0.5f,0.5f));
		Label_repair->setPosition(ccp(0,0));
		Button_repair->addChild(Label_repair);
		//卸下按钮
		UIButton * Button_getOff= UIButton::create();
		Button_getOff->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		Button_getOff->setTouchEnable(true);
		Button_getOff->setPressedActionEnabled(true);
		Button_getOff->addReleaseEvent(this,coco_releaseselector(EquipmentItemInfos::getOffEvent));
		Button_getOff->setAnchorPoint(ccp(0.5f,0.5f));
		Button_getOff->setScale9Enable(true);
		Button_getOff->setScale9Size(CCSizeMake(80,43));
		Button_getOff->setCapInsets(CCRect(18,9,2,23));
		//Button_getOff->setPosition(ccp(58,25));
		Button_getOff->setPosition(ccp(55,25));

		UILabel * Label_getOff = UILabel::create();
		Label_getOff->setText((const char *)"卸下");
		Label_getOff->setFontName(APP_FONT_NAME);
		Label_getOff->setFontSize(18);
		Label_getOff->setAnchorPoint(ccp(0.5f,0.5f));
		Label_getOff->setPosition(ccp(0,0));
		Button_getOff->addChild(Label_getOff);
		m_pUiLayer->addWidget(Button_getOff);
		//强化按钮
		UIButton * Button_LVUp= UIButton::create();
		Button_LVUp->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		Button_LVUp->setTouchEnable(true);
		Button_LVUp->setPressedActionEnabled(true);
		Button_LVUp->addReleaseEvent(this,coco_releaseselector(EquipmentItemInfos::lvUpEvent));
		Button_LVUp->setAnchorPoint(ccp(0.5f,0.5f));
		Button_LVUp->setScale9Enable(true);
		Button_LVUp->setScale9Size(CCSizeMake(80,43));
		Button_LVUp->setCapInsets(CCRect(18,9,2,23));
		Button_LVUp->setPosition(ccp(224,25));

		UILabel * Label_LVUp = UILabel::create();
		Label_LVUp->setText((const char *)"强化");
		Label_LVUp->setFontName(APP_FONT_NAME);
		Label_LVUp->setFontSize(18);
		Label_LVUp->setAnchorPoint(ccp(0.5f,0.5f));
		Label_LVUp->setPosition(ccp(0,0));
		Button_LVUp->addChild(Label_LVUp);
		m_pUiLayer->addWidget(Button_LVUp);

		//强化按等级开放
		int roleLevel = GameView::getInstance()->myplayer->getActiveRole()->level();
		int openLevel = 0;
		for (int i = 0;i<FunctionOpenLevelConfigData::s_functionOpenLevelMsg.size();++i)
		{
			if (strcmp("btn_strengthen",FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionId().c_str()) == 0)
			{
				openLevel = FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel();
				break;
			}
		}

		/*
		if (openLevel <= roleLevel)     //可以显示
		{
			Button_LVUp->setVisible(true);
			Button_getOff->setPosition(ccp(58,25));
		}
		else
		{
			Button_LVUp->setVisible(false);
			Button_getOff->setPosition(ccp(141,25));
		}
		*/

		int btn_num = 1;
		if (equipment->goods().equipmentdetail().starlevel() > 0)
		{
			btn_num++;
		}

		if (equipment->goods().equipmentdetail().durable() <= 10)
		{
			btn_num++;
		}

		if (openLevel <= roleLevel)
		{
			btn_num++;
		}

		switch(btn_num)
		{
		case 1:
			{
				Button_pickStar->setVisible(false);
				Button_repair->setVisible(false);
				Button_LVUp->setVisible(false);
				Button_getOff->setVisible(true);
				Button_getOff->setPosition(ccp(141,25));
			}break;
		case 2:
			{
				Button_pickStar->setVisible(false);
				Button_repair->setVisible(false);
				Button_LVUp->setVisible(false);
				Button_getOff->setVisible(true);
				Button_getOff->setPosition(ccp(58,25));

				if (equipment->goods().equipmentdetail().starlevel() > 0)
				{
					Button_pickStar->setVisible(true);
					Button_pickStar->setPosition(ccp(224,25));
				}

				if (equipment->goods().equipmentdetail().durable() <= 10)
				{
					Button_repair->setVisible(true);
					Button_repair->setPosition(ccp(224,25));
				}

				if (openLevel <= roleLevel)
				{
					Button_LVUp->setVisible(true);
					Button_LVUp->setPosition(ccp(224,25));
				}
			}break;
		case 3:
			{
				Button_pickStar->setVisible(false);
				Button_repair->setVisible(false);
				Button_LVUp->setVisible(false);
				Button_getOff->setVisible(true);
				Button_getOff->setPosition(ccp(58,25));

				bool temp_ = false;
				if (equipment->goods().equipmentdetail().starlevel() > 0)
				{
					Button_pickStar->setVisible(true);
					Button_pickStar->setPosition(ccp(141,25));
					temp_ = true;
				}

				if (equipment->goods().equipmentdetail().durable() <= 10)
				{
					Button_repair->setVisible(true);
					if (temp_ == true)
					{
						Button_repair->setPosition(ccp(224,25));
					}else
					{
						Button_repair->setPosition(ccp(141,25));
					}
				}

				if (openLevel <= roleLevel)
				{
					Button_LVUp->setVisible(true);
					Button_LVUp->setPosition(ccp(224,25));
				}
			}break;
		case 4:
			{
				scrollView_info->setSize(CCSizeMake(548,260));
				scrollView_info->setPosition(ccp(0,97));

				Button_getOff->setPosition(ccp(80,70));
				Button_repair->setPosition(ccp(200,70));
				Button_LVUp->setPosition(ccp(80,25));
				Button_pickStar->setPosition(ccp(200,25));
			}break;
		}

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setAnchorPoint(ccp(0,0));
		//this->setContentSize(CCSizeMake(background->getContentSize().width,background->getContentSize().height));


		std::vector<CEquipment *>::iterator iter;
		for (iter = equipVector.begin(); iter!= equipVector.end();iter++)
		{
			delete *iter;
		}
		equipVector.clear();

		return true;
	}
	return false;
}

void EquipmentItemInfos::onEnter()
{
	//GoodsItemInfoBase::onEnter();
	UIScene::onEnter();
	this->openAnim();
}
void EquipmentItemInfos::onExit()
{
	UIScene::onExit();
}

bool EquipmentItemInfos::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
	return this->resignFirstResponder(pTouch,this,false);
}
void EquipmentItemInfos::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
{

}
void EquipmentItemInfos::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
{

}
void EquipmentItemInfos::ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent)
{

}

void EquipmentItemInfos::lvUpEvent(CCObject * pSender)
{
	CCLog("lvUpEvent");
	//关闭详细信息界面
	//CEquipment * equipment = new CEquipment();
	//equipment->CopyFrom(*generalEquip);
	
	this->setVisible(false);
	PackageScene *packageScene = (PackageScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
	//pacTabviewSelectindex  = packageScene->selectTabviewIndex;
	if(packageScene != NULL)
	{
		packageScene->closeEndedEvent(NULL);
	}


	CCActionInterval * action =(CCActionInterval *)CCSequence::create(
		CCDelayTime::create(0.5f),
		CCCallFuncND::create(this,callfuncND_selector(EquipmentItemInfos::showEquipStrength),(void *)generalEquip),
		CCRemoveSelf::create(),
		NULL);
	runAction(action);
	//delete equipment;
}

void EquipmentItemInfos::getOffEvent( CCObject * pSender )
{
	CCLog("EquipmentInfo::getOffEvent");
	
	PackageScene *packageScene = (PackageScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
	if (packageScene->selectActorId != m_generalId)
	{
		int tmpe_selectIndex = 0;
		int generalNum = GameView::getInstance()->generalsInLineList.size();
		for (int i=0;i<generalNum;i++)
		{
			if (m_generalId == GameView::getInstance()->generalsInLineList.at(i)->id())
			{
				tmpe_selectIndex = i+1;
			}
		}
		packageScene->generalList_tableView->selectCell(tmpe_selectIndex);
	}
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1307, (void *)generalEquip->part(),(void *)m_generalId);
	GameUtils::playGameSound(EQUIP_UNLOAD, 2, false);
	//关闭详细信息界面
	this->removeFromParentAndCleanup(true);
	
}

void EquipmentItemInfos::callBackPick( CCObject * obj )
{
	PackageScene *packageScene = (PackageScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
	packageScene->selectGeneralEquipPart = generalEquip->part();
	packageScene->selectGeneralEquipStarLevel =  generalEquip->goods().equipmentdetail().starlevel();
	this->removeFromParentAndCleanup(true);

	const char *strBegin  = StringDataManager::getString("packBackGoodsPickSureBegin");
	const char *strEnd  = StringDataManager::getString("packBackGoodsPickSureEnd");

	MapStarPickAmount * equipStarLevel = StrengthStarConfigData::s_EquipStarAmount[packageScene->selectGeneralEquipStarLevel];
	int starCount_ = equipStarLevel->get_count();
	char str[20];
	sprintf(str,"%d",starCount_);
	std::string string_ = strBegin;
	string_.append(str);
	string_.append(strEnd);
	GameView::getInstance()->showPopupWindow(string_,2,this,coco_selectselector(EquipmentItemInfos::callBackPickSure),NULL);
}

void EquipmentItemInfos::callBackPickSure( CCObject * obj )
{
	PackageScene *packageScene = (PackageScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
	assistEquipStruct assists_={1,packageScene->selectGeneralEquipPart,packageScene->selectActorId,0,2};
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1601,&assists_);

	GameMessageProcessor::sharedMsgProcessor()->sendReq(1608,this);
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1699,this);
}

void EquipmentItemInfos::showEquipStrength(CCNode * pNode,void * equip)
{
	CEquipment * equipment = (CEquipment *)equip;
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	if(GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI) == NULL)
	{
		EquipMentUi * equipui=EquipMentUi::create();
		equipui->ignoreAnchorPointForPosition(false);
		equipui->setAnchorPoint(ccp(0.5f,0.5f));
		equipui->setPosition(ccp(winSize.width/2,winSize.height/2));
		GameView::getInstance()->getMainUIScene()->addChild(equipui,0,ktagEquipMentUI);
		equipui->refreshBackPack(0,-1);

		equipui->generalList->generalList_tableView->selectCell(pacTabviewSelectindex);
 		equipui->generalMainId = m_generalId;
		equipui->addRoleEquipment(equipment);
	}
}

void EquipmentItemInfos::callBackRepair( CCObject * obj )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5211);
	this->removeFromParentAndCleanup(true);
}




