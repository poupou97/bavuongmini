#include "UseMissionPropUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/MissionInfo.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../extensions/CCRichLabel.h"
#include "../extensions/UITab.h"
#include "../../messageclient/protobuf/MissionMessage.pb.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "../../messageclient/element/CNpcInfo.h"
#include "../../legend_engine/GameWorld.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../utils/StaticDataManager.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../utils/GameUtils.h"
#include "../../messageclient/element/CMissionReward.h"
#include "../missionscene/MissionRewardGoodsItem.h"

using namespace CocosDenshion;

UseMissionPropUI::UseMissionPropUI() : 
targetNpcId(0)
{
	rewardIconName[0] = "rewardIcon_0";
	rewardIconName[1] = "rewardIcon_1";
	rewardIconName[2] = "rewardIcon_2";
	rewardIconName[3] = "rewardIcon_3";
	rewardIconName[4] = "rewardIcon_4";
	rewardLabelName[0] = "rewardLabel_0";
	rewardLabelName[1] = "rewardLabel_1";
	rewardLabelName[2] = "rewardLabel_2";
	rewardLabelName[3] = "rewardLabel_3";
	rewardLabelName[4] = "rewardLabel_4";
}


UseMissionPropUI::~UseMissionPropUI()
{
}


UseMissionPropUI* UseMissionPropUI::create(int npcId,const char * des,CMissionReward * missionReward)
{
	UseMissionPropUI *useMissionPropUI = new UseMissionPropUI();
	if (useMissionPropUI && useMissionPropUI->init(npcId,des,missionReward))
	{
		useMissionPropUI->autorelease();
		return useMissionPropUI;
	}
	CC_SAFE_DELETE(useMissionPropUI);
	return NULL;

}

bool UseMissionPropUI::init(int npcId,const char * des,CMissionReward * missionReward)
{
	if (UIScene::init())
	{
		targetNpcId = npcId;

		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		//加载UI
		UIPanel *ppanel = (UIPanel*)GUIReader::shareReader()->widgetFromJsonFile("res_ui/useMissionProp_1.json");
		ppanel->setAnchorPoint(ccp(0.0f,0.0f));
		ppanel->getValidNode()->setContentSize(CCSizeMake(350, 400));
		ppanel->setPosition(CCPointZero);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnable(true);
		m_pUiLayer->addWidget(ppanel);

		UIButton * Button_close = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnable(true);
		Button_close->addReleaseEvent(this, coco_releaseselector(UseMissionPropUI::CloseEvent));
		Button_close->setPressedActionEnabled(true);
		
		UIPanel * panel_mission = (UIPanel*)UIHelper::seekWidgetByName(ppanel,"Panel_renwu");
		panel_mission->setVisible(true);
		Button_getMission = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_lingqu");
		Button_getMission->setTouchEnable(true);
		Button_getMission->setPressedActionEnabled(true);
		Button_getMission->setVisible(true);
		Button_getMission->addReleaseEvent(this,coco_releaseselector(UseMissionPropUI::GetMissionEvent));
		Button_finishMission = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_tijiao");
		Button_finishMission->setVisible(false);

		reward_di_1 = (UIImageView * )UIHelper::seekWidgetByName(ppanel,"ImageView_di1");
		reward_di_2 = (UIImageView * )UIHelper::seekWidgetByName(ppanel,"ImageView_di2");
		reward_di_3 = (UIImageView * )UIHelper::seekWidgetByName(ppanel,"ImageView_di3");
		reward_di_4 = (UIImageView * )UIHelper::seekWidgetByName(ppanel,"ImageView_di4");
		reward_di_1->setVisible(false);
		reward_di_2->setVisible(false);
		reward_di_3->setVisible(false);
		reward_di_4->setVisible(false);

		// load animation
		
		CNpcInfo * npcInfo = GameWorld::NpcInfos[targetNpcId];
		std::string animFileName = "res_ui/npc/";
		if (targetNpcId == 0)//没有任务NPC
		{
			animFileName.append("cz");
		}
		else
		{
			if (npcInfo)
			{
				animFileName.append(npcInfo->icon.c_str());
			}
			else
			{
				animFileName.append("cz");
			}
		}
		animFileName.append(".anm");
		//std::string animFileName = "res_ui/npc/caiwenji.anm";
		CCLegendAnimation *m_pAnim = CCLegendAnimation::create(animFileName);
		if (m_pAnim)
		{
			m_pAnim->setPlayLoop(true);
			m_pAnim->setReleaseWhenStop(false);
			m_pAnim->setScale(1.0f);
			m_pAnim->setPosition(ccp(33,291));
			//m_pAnim->setPlaySpeed(.35f);
			addChild(m_pAnim);
		}

		CCSprite * sprite_anmFrame = CCSprite::create("res_ui/renwua/tietu2.png");
		sprite_anmFrame->setAnchorPoint(ccp(0.5,0.5f));
		sprite_anmFrame->setPosition(ccp(115,335));
		addChild(sprite_anmFrame);


		UIImageView *ImageView_name = (UIImageView*)UIHelper::seekWidgetByName(ppanel,"ImageView_mingzi");

		ccFontDefinition strokeShaodwTextDef;
		strokeShaodwTextDef.m_fontName = std::string("res_ui/font/simhei.ttf");
		ccColor3B tintColorGreen   =  { 54, 92, 7 };
		strokeShaodwTextDef.m_fontFillColor   = tintColorGreen;   // 字体颜色
		strokeShaodwTextDef.m_fontSize = 22;   // 字体大小

		CCLabelTTF* l_nameLabel;
		if (targetNpcId == 0) //没有任务NPC
		{
			l_nameLabel = CCLabelTTF::create(StringDataManager::getString("mission_teachNpcName"),"res_ui/font/simhei.ttf",18);
		}
		else
		{
			if (npcInfo)
			{
				l_nameLabel = CCLabelTTF::create(npcInfo->npcName.c_str(),"res_ui/font/simhei.ttf",18);
			}
			else
			{
				l_nameLabel = CCLabelTTF::create(StringDataManager::getString("mission_teachNpcName"),"res_ui/font/simhei.ttf",18);
			}
		}
		l_nameLabel->setAnchorPoint(ccp(0.5,0.5f));
		l_nameLabel->setPosition(ccp(207,307));
		ccColor3B shadowColor = ccc3(0,0,0);   // black
		l_nameLabel->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		addChild(l_nameLabel);

		UILabelBMFont * lbf_reward_gold  = (UILabelBMFont *)UIHelper::seekWidgetByName(ppanel,"LabelBMFont_goldValue");
		UILabelBMFont * lbf_reward_exp  = (UILabelBMFont *)UIHelper::seekWidgetByName(ppanel,"LabelBMFont_expValue");

		char s_gold[20];
		sprintf(s_gold,"%d",missionReward->gold());
		lbf_reward_gold->setText(s_gold);

		char s_exp[20];
		sprintf(s_exp,"%d",missionReward->exp());
		lbf_reward_exp->setText(s_exp);

		if (missionReward->goods_size() > 0)
		{
			for (int i =0;i<missionReward->goods_size();++i)
			{
				GoodsInfo * goodInfo = new GoodsInfo();
				goodInfo->CopyFrom(missionReward->goods(i).goods());
				int num = missionReward->goods(i).quantity();

				MissionRewardGoodsItem * mrGoodsItem = MissionRewardGoodsItem::create(goodInfo,num);
				mrGoodsItem->setAnchorPoint(ccp(0.5f,0.5f));
				mrGoodsItem->setPosition(ccp(82+58*i,85));
				mrGoodsItem->setName(rewardIconName[i].c_str());
				mrGoodsItem->setScale(0.8f);
				m_pUiLayer->addWidget(mrGoodsItem);

				delete goodInfo;
			}
		}

		//description
		std::string mission_description = des;
		CCRichLabel *label_des = CCRichLabel::createWithString(mission_description.c_str(),CCSizeMake(285, 0),this,NULL,0,18,5);
		CCSize desSize = label_des->getContentSize();
		int height = desSize.height;
		//ccscrollView_description
		CCScrollView *m_contentScrollView = CCScrollView::create(CCSizeMake(290,99));
		m_contentScrollView->setViewSize(CCSizeMake(290, 99));
		m_contentScrollView->ignoreAnchorPointForPosition(false);
		m_contentScrollView->setTouchEnabled(true);
		m_contentScrollView->setDirection(kCCScrollViewDirectionVertical);
		m_contentScrollView->setAnchorPoint(ccp(0,0));
		m_contentScrollView->setPosition(ccp(30,185));
		m_contentScrollView->setBounceable(true);
		m_contentScrollView->setClippingToBounds(true);
		m_pUiLayer->addChild(m_contentScrollView);
		if (height > 99)
		{
			m_contentScrollView->setContentSize(ccp(290,height));
			m_contentScrollView->setContentOffset(ccp(0,99-height));
		}
		else
		{
			m_contentScrollView->setContentSize(ccp(290,99));
		}
		m_contentScrollView->setClippingToBounds(true);

		m_contentScrollView->addChild(label_des);
		label_des->ignoreAnchorPointForPosition(false);
		label_des->setAnchorPoint(ccp(0,1));
		label_des->setPosition(ccp(3,m_contentScrollView->getContentSize().height-3));
		

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(CCSizeMake(350,400));

		return true;
	}
	return false;

}


void UseMissionPropUI::onEnter()
{
	UIScene::onEnter();

	CCFiniteTimeAction*  action = CCSequence::create(
		CCScaleTo::create(0.15f*1/2,1.1f),
		CCScaleTo::create(0.15f*1/3,1.0f),
		NULL);

	runAction(action);

}

void UseMissionPropUI::onExit()
{
	UIScene::onExit();
}


bool UseMissionPropUI::ccTouchBegan(CCTouch *touch, CCEvent * pEvent)
{
	//return this->resignFirstResponder(touch,this,false);
	CCPoint location = touch->getLocation();

	CCPoint pos = this->getPosition();
	CCSize size = this->getContentSize();
	CCPoint anchorPoint = this->getAnchorPointInPoints();
	pos = ccpSub(pos, anchorPoint);
	CCRect rect(pos.x, pos.y, size.width, size.height);
	if(rect.containsPoint(location))
	{
		return true;
	}

	GameMessageProcessor::sharedMsgProcessor()->sendReq(7021, this);
	this->closeAnim();
	return false;
}

void UseMissionPropUI::ccTouchEnded(CCTouch *touch, CCEvent * pEvent)
{
}

void UseMissionPropUI::ccTouchCancelled(CCTouch *touch, CCEvent * pEvent)
{
}

void UseMissionPropUI::ccTouchMoved(CCTouch *touch, CCEvent * pEvent)
{
}

void UseMissionPropUI::CloseEvent( CCObject * pSender )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(7021, this);
	this->closeAnim();
}

void UseMissionPropUI::GetMissionEvent( CCObject * pSender )
{
	GameUtils::playGameSound(MISSION_ACCEPT, 2, false);
	GameMessageProcessor::sharedMsgProcessor()->sendReq(7022, this);
	this->closeAnim();
}

int UseMissionPropUI::getTargetNpcId()
{
	return targetNpcId;
}