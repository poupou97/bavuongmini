#ifndef _BACKPACKSCENE_GOODSITEMINFOBASE_H
#define _BACKPACKSCENE_GOODSITEMINFOBASE_H

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

class GoodsInfo;
class CEquipProperty;
class CEquipment;
class GoodsItemInfoBase:public UIScene
{
public:
	GoodsItemInfoBase();
	~GoodsItemInfoBase();

	static GoodsItemInfoBase *create(GoodsInfo * goodsInfo,std::vector<CEquipment *> curEquipment,long long generalId = 0);
	
	bool init(GoodsInfo * goodsInfo,std::vector<CEquipment *> curEquipment,long long generalId);
	virtual void onEnter();
	virtual void onExit();
	
	void showTouchEquipMentInfo(GoodsInfo * goodsInfo,int index);
	void showTouchGoodsInfo(GoodsInfo * goodsInfo);
	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
protected:
	std::string buttonImagePath;
	enum {
		EQUIPMEGTBASEDESTAG=0,
		EQUIPMEGTBASEVALUETAG=10,

		EQUIPMENTSTARTAG=20,
		EQUIPMENTSTARVALUETAG=30,

		EQUIPMENTACTIVETAG=40,
		EQUIPMENTACTIVE_END_TAG=45,
		EQUIPMENTACTIVEVALUETAG=50,

		EQUIPMENTGEMTAG=60,
		EQUIPMENTGEMVALUETAG=70,
		EQUIPMENTGEMICONTAG = 75,
		EQUIPMENTSUITTAG=80,
		EQUIPMENTSUITVALUETAG=90,
		EQUIPMENTSUITNAME = 100,
		};
public:
	UIPanel * touchPanelInfo;
	UIPanel *bodyPanelInfo;
	cocos2d::extension::UIScrollView * scrollView_info;
	//std::vector<CEquipProperty *> equipmentPropertyV;
public:
	int touchHight;
	//UILayer * layerScene;
	//equipment
	//UIImageView * headImage_equip;
	//UILabel * label_equipName;
	//UILabel * label_profession;
	//UILabel * binding_label;
	//int cur_starLevel;
	//UIImageView* starlevel_All;
	//UIImageView * starlevel_All_copy;
	//UIImageView* starlevel_int;
	//UIImageView* starlevel_half;
	//UILabel * label_uselevel;
	//UILabel * label_powercount;
	//base list
	//UIImageView * boundary_touch_first;
	//UILabel *label_equipProperty_base;
	cocos2d::extension::UILabel *basePropertyDes;
	cocos2d::extension::UILabel *basePropertyValue;
	//star
	cocos2d::extension::UIImageView * boundary_Star;
	cocos2d::extension::UILabel *label_equipProperty_star;
	cocos2d::extension::UILabel *starPropertyDes;
	cocos2d::extension::UILabel *starPropertyValue;
	//active
	cocos2d::extension::UIImageView * boundary_Active;
	cocos2d::extension::UILabel *label_equipProperty_active;
	cocos2d::extension::UILabel *activePropertyDes;
	cocos2d::extension::UILabel *activePropertyValue;
	////gem
	cocos2d::extension::UIImageView * boundary_Gem;
	cocos2d::extension::UILabel *label_equipProperty_gem;
	cocos2d::extension::UILabel *gemPropertyDes;
	//UILabel *gemPropertyValue;
	//suit
	cocos2d::extension::UIImageView * boundary_Suit;
	cocos2d::extension::UILabel *label_equipProperty_suit;

	
	int baseValueNum;
	int starValueNum;
	int activeValueNum;
	int gemValueNum;
	int suitValueNum;
	std::string getEquipmentQualityByIndex(int quality);
	ccColor3B getEquipmentColorByQuality(int quality);
	bool isRoleEquip_;//�Ƿ���װ��
	bool isSetSuitColor;
	void isEquipOfBody(GoodsInfo * goods);
	void getSelfEquipVector(GoodsInfo * goodsInfo,std::vector<CEquipment *> vector_);
	//is touch self equip
	cocos2d::extension::UIImageView * flag_toucheSelfEquip;
	void isTouchSameEquipment();
public:
	///goods
	GoodsInfo * goodsInfo_General;

	std::vector<CEquipment *> curShowEquipment;
	long long curGeneralId;

	cocos2d::extension::UIImageView * headImage_goods;
	cocos2d::extension::UILabel * label_goods_name;
	//UILabel * label_goodsType;
	cocos2d::extension::UILabel * label_goods_uselevel;
	cocos2d::extension::UILabel * label_goodsPrice;
	cocos2d::extension::UILabel * label_description;

	int generalProfession;
	int generalLevel;
	void addGeneralButton(long long generalId);
	void showGeneralEquipMentInfo(CCObject * obj);
	//std::string pression;
	void setEquipProfessionAndLevelColor(int pression_,int level_);

	void getRoleEquipmentList();
	void getGeneralEquipmentList(long long generalId);


	//touch des getEquipmentQualityByIndex
	void callBackQualityDes(CCObject * obj);
private:
	cocos2d::extension::UIImageView * flag_equip;
	cocos2d::extension::UILabel * notEquipment;
	int selfEquipSuit;
	std::vector<CEquipment *> selfEquipVector;
};

#endif