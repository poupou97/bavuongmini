
#ifndef _BACKPACKSCENE_PACKAGEITEM_H_
#define _BACKPACKSCENE_PACKAGEITEM_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"
#include "../../legend_script/CCTutorialIndicator.h"

USING_NS_CC;
USING_NS_CC_EXT;

class FolderInfo;
class GoodsInfo;

typedef enum {
	COMMONITEM,
	VOIDITEM,
	LOCKITEM,
}PacType;

class PackageItem :public UIWidget
{
public:
	PackageItem();
	~PackageItem();
	static PackageItem * create(FolderInfo* folder);
	bool init(FolderInfo* folder);
	static PackageItem * create(GoodsInfo* goods);
	bool initWithGoods(GoodsInfo* goods);
	static PackageItem * createLockedItem(int folderIndex);
	bool initLockedItem(int folderIndex);
	virtual void setLabelNameText(char * str);
	//是否显示数量（默认显示）
	virtual void setNumVisible(bool visible);
	//是否显示绑定标识（默认不显示）
	virtual void setBoundVisible(bool visible);
	//是否显示宝石属性（默认不显示）
	virtual void setBoundJewelPropertyVisible(bool visible);
	//get cur gem property
	std::string getCurGemIconPath(std::string goodsId);

	//背包格子
	FolderInfo * curFolder;
	//副本奖励界面的一项
	GoodsInfo * curGood;
	//////////////////change by liuzhenxing
	void setGray(bool isGray,std::string isShowGoodsNum);
	void setRed(bool isRed);
	void setAvailable(bool isAvailable);

	void BeganToRunCD();

	float getCurProgress();
public:
	//教学穿装备
	UIButton * Btn_pacItemFrame;

	//教学
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction );
	void removeCCTutorialIndicator();
	//教学
	int mTutorialScriptInstanceId;
protected:
	int index ;
	UILabel * Lable_name;
	UILabel * Lable_num;
	PacType cur_type;

	void PackageItemEvent(CCObject * pSender);

	void GoodItemEvent(CCObject *pSender);

	CCPoint autoGetPosition(CCObject * pSender);

	void BtnLockEvent(CCObject * pSender);

	void SureEvent(CCObject * pSender);
	void CancelEvent(CCObject * pSender);

	void drugCoolDownCallBack(CCNode* node);

	long millisecondNow();
	bool isDoubleTouch();
	long lastTouchTime;
private:
	UIImageView *ImageView_packageItem;
	UIImageView *ImageView_bound;

	CCProgressTimer* mProgressTimer_skill;
};

#endif;