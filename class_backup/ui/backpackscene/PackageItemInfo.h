

#ifndef _BACKPACKSCENE_PACKAGEITEMINFO_H
#define _BACKPACKSCENE_PACKAGEITEMINFO_H

#include "../extensions/UIScene.h"
#include "cocos-ext.h"
#include "cocos2d.h"
#include "GoodsItemInfoBase.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../messageclient/element/FolderInfo.h"



USING_NS_CC;
USING_NS_CC_EXT;

class PackageScene;
class LoadSceneState;
class FolderInfo;
class GoodsInfo;
class PacPageView;
class PackageItemInfo : public GoodsItemInfoBase
{
public:
	PackageItemInfo();
	~PackageItemInfo();

	static PackageItemInfo * create(FolderInfo * folder,std::vector<CEquipment *>equipmentVector,long long generalId);
	bool init(FolderInfo * folder,std::vector<CEquipment *>equipmentVector,long long generalId);

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

	void dressOnEvent(CCObject * pSender);
	void sureToDressOn(CCObject * pSender);

	void callBackPick(CCObject * obj);
	void callBackPickSure(CCObject * obj);
	//丢弃
	void deleteEvent(CCObject * pSender);
	//弹出计算器(输入数量后点OK)
	void showCountForDelete(CCObject *pSender);
	void sureToDelete(CCObject * pSender);

	/*add equipment*/
	void showEquipStrength(CCObject * obj);
	void lvUpEvent(CCObject * pSender);
	void shortCutEvent(CCObject *pSender);
	void useEvent(CCObject *pSender);
	void PutInEvent(CCObject * pSender);
	void systhesisEvent(CCObject *pSender);
	void showEquipSysthesisUI(CCNode *pNode,void * folderInfo);
	
	void SellEvent(CCObject *pSender);
	void SellEventNum(CCObject *pSender);
	
	void GetAnnesEvent(CCObject * pSender);
	//void GetAuctionEvent(CCObject * pSender);
	void callBackSearch(CCObject * obj);
	void callBackConsign(CCObject * obj);
	void cancelAuction(CCObject * pSender);
	void showGoodsInfo(CCObject * obj);
	void showGoodsInfoPrivateui(CCObject * obj);
	void showCEquipment(CCObject * obj);

	void showStrengthStone(CCObject * obj);

	void useAllEvent(CCObject *pSender);

	void setCurEquipIsUseAble(bool useAble);

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

private:
	struct assistEquipStruct
	{
		int source_;
		int  pob_;
		long long petid_;
		int index_;
		int playType;
	};
// 	char * curItemName;
// 
// 	UIImageView * ImageView_head;
// 	UILabel * Label_name;
// 	UILabel * Label_type;
// 	UILabel * Label_price;
// 	UILabel * Label_useLevel;
	std::string buttonImagePath;

	//将要丢弃的数量
	int deleteAmount;

public:
	GoodsInfo * curGoods;
	FolderInfo * curFolders;
public:
	//为了教学方便 获取主装备按钮的位置
	//UIButton * btn_addMainEquip;
	UIButton * Button_dressOn;

	//教学
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction );
	void removeCCTutorialIndicator();
	//教学
	int mTutorialScriptInstanceId;
};
#endif;
