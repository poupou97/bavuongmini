#include "GeneralsListForTakeDrug.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "PacPageView.h"
#include "AppMacros.h"
#include "../../ui/generals_ui/RecuriteActionItem.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../gamescene_state/role/ActorUtils.h"
#include "PackageScene.h"
#include "../../ui/extensions/Counter.h"
#include "../../messageclient/element/GoodsInfo.h"

GeneralsListForTakeDrug::GeneralsListForTakeDrug():
curRoleId(-1)
{
}


GeneralsListForTakeDrug::~GeneralsListForTakeDrug()
{
	delete curFolder;
}

GeneralsListForTakeDrug * GeneralsListForTakeDrug::create(FolderInfo * folderInfo)
{
	GeneralsListForTakeDrug * generalsListForTakeDrug = new GeneralsListForTakeDrug();
	if (generalsListForTakeDrug && generalsListForTakeDrug->init(folderInfo))
	{
		generalsListForTakeDrug->autorelease();
		return generalsListForTakeDrug;
	}
	CC_SAFE_DELETE(generalsListForTakeDrug);
	return NULL;
}

bool GeneralsListForTakeDrug::init(FolderInfo * folderInfo)
{
	if (UIScene::init())
	{
		curFolder = new FolderInfo();
		curFolder->CopyFrom(*folderInfo);
		curPackageItemIndex = folderInfo->id();

// 		UIPanel * mainPanel = (UIPanel*)GUIReader::shareReader()->widgetFromJsonFile("res_ui/cure_popupo_1.json");
// 		mainPanel->setAnchorPoint(ccp(0.f,0.f));
// 		mainPanel->setPosition(ccp(0,0));
// 		m_pUiLayer->addWidget(mainPanel);
		UIImageView * bg = UIImageView::create();
		bg->setTexture("gamescene_state/zhujiemian3/renwuduiwu/renwu_di.png");
		bg->setAnchorPoint(CCPointZero);
		bg->setPosition(CCPointZero);
		bg->setScale9Enable(true);
		bg->setCapInsets(CCRectMake(12,12,1,1));
		bg->setScale9Size(CCSizeMake(204,383));
		m_pUiLayer->addWidget(bg);

		//武将列表
		CCTableView * generalList_tableView = CCTableView::create(this,CCSizeMake(188,375));
		generalList_tableView->setDirection(kCCScrollViewDirectionVertical);
		generalList_tableView->setAnchorPoint(ccp(0,0));
		generalList_tableView->setPosition(ccp(8,4));
		generalList_tableView->setContentOffset(ccp(0,0));
		generalList_tableView->setDelegate(this);
		generalList_tableView->setContentOffset(ccp(10,0));
		generalList_tableView->setClippingToBounds(true);
		generalList_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		addChild(generalList_tableView);

		this->setContentSize(CCSizeMake(204,383));
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);

		return true;
	}
	return false;
}

void GeneralsListForTakeDrug::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void GeneralsListForTakeDrug::onExit()
{
	UIScene::onExit();
}

bool GeneralsListForTakeDrug::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return resignFirstResponder(touch,this,false);
}

void GeneralsListForTakeDrug::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void GeneralsListForTakeDrug::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void GeneralsListForTakeDrug::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void GeneralsListForTakeDrug::scrollViewDidScroll( CCScrollView* view )
{

}

void GeneralsListForTakeDrug::scrollViewDidZoom( CCScrollView* view )
{

}

void GeneralsListForTakeDrug::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{
	int index = cell->getIdx();
	curRoleId = GameView::getInstance()->generalsInLineList.at(index)->id();
	
	if (curFolder->goods().clazz() != GOODS_CLASS_WUJIANGJINGYANJIU)  //28 武将经验丹
	{
		PackageScene::IdxAndNum * temp = new PackageScene::IdxAndNum();
		temp->index = curPackageItemIndex;
		temp->num = 1;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1302, (void *)curRoleId,temp);
		delete temp;

		this->closeAnim();
	}
	else
	{
		if(curFolder->quantity() > 1)
		{
			//计算器
			GameView::getInstance()->showCounter(this,callfuncO_selector(GeneralsListForTakeDrug::showCountForGeneralExp),curFolder->quantity());
		}
		else
		{
			PackageScene::IdxAndNum * temp = new PackageScene::IdxAndNum();
			temp->index = curPackageItemIndex;
			temp->num = 1;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1302, (void *)curRoleId,temp);
			delete temp;

			this->closeAnim();
		}
	}

}

cocos2d::CCSize GeneralsListForTakeDrug::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeMake(187,73);
}

cocos2d::extension::CCTableViewCell* GeneralsListForTakeDrug::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

 	cell = GeneralsListForTakeDrugCell::create(GameView::getInstance()->generalsInLineList.at(idx)); 

	return cell;
}

unsigned int GeneralsListForTakeDrug::numberOfCellsInTableView( CCTableView *table )
{
	return GameView::getInstance()->generalsInLineList.size();
}

void GeneralsListForTakeDrug::showCountForGeneralExp( CCObject *pSender )
{
	Counter * counter_ =(Counter *)pSender;
	int amount_;
	if (counter_ != NULL)
	{
		amount_ =counter_->getInputNum();
		PackageScene::IdxAndNum * temp = new PackageScene::IdxAndNum();
		temp->index = curPackageItemIndex;
		temp->num = amount_;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1302, (void *)curRoleId,temp);
		delete temp;

		this->closeAnim();
	}else
	{
	}
}


/////////////////////////////////////////////////////

#define ktag_sprite_bigFrame 456
GeneralsListForTakeDrugCell::GeneralsListForTakeDrugCell()
{

}

GeneralsListForTakeDrugCell::~GeneralsListForTakeDrugCell()
{
	delete curGeneralBaseMsg;
}

GeneralsListForTakeDrugCell* GeneralsListForTakeDrugCell::create( CGeneralBaseMsg * generalBaseMsg )
{
	GeneralsListForTakeDrugCell * generalsListForTakeDrugCell = new GeneralsListForTakeDrugCell();
	if (generalsListForTakeDrugCell && generalsListForTakeDrugCell->init(generalBaseMsg))
	{
		generalsListForTakeDrugCell->autorelease();
		return generalsListForTakeDrugCell;
	}
	CC_SAFE_DELETE(generalsListForTakeDrugCell);
	return NULL;
}

bool GeneralsListForTakeDrugCell::init( CGeneralBaseMsg * generalBaseMsg )
{
	if (CCTableViewCell::init())
	{
		curGeneralBaseMsg = new CGeneralBaseMsg();
		curGeneralBaseMsg->CopyFrom(*generalBaseMsg);

		CGeneralBaseMsg * generalMsgFromDb = GeneralsConfigData::s_generalsBaseMsgData[generalBaseMsg->modelid()];
		//大边框
		if (this->getChildByTag(ktag_sprite_bigFrame))
		{
			this->getChildByTag(ktag_sprite_bigFrame)->removeFromParent();
		}

		CCScale9Sprite * sprite_bigFrame = CCScale9Sprite::create("gamescene_state/zhujiemian3/chongwutouxiang/di_a.png");
		sprite_bigFrame->setAnchorPoint(ccp(0, 0));
		sprite_bigFrame->setPosition(ccp(0, 0));
		sprite_bigFrame->setPreferredSize(CCSizeMake(187,70));
		sprite_bigFrame->setCapInsets(CCRectMake(16,34,1,1));
		sprite_bigFrame->setTag(ktag_sprite_bigFrame);
		sprite_bigFrame->setZOrder(-1);
		addChild(sprite_bigFrame);

		//小边框
		CCScale9Sprite *  smaillFrame = CCScale9Sprite::create("res_ui/LV4_allb.png");
		smaillFrame->setAnchorPoint(ccp(0, 0));
		smaillFrame->setPosition(ccp(10, 8));
		smaillFrame->setCapInsets(CCRectMake(15,15,1,1));
		smaillFrame->setContentSize(CCSizeMake(52,52));
		addChild(smaillFrame);

		//列表中的头像图标
		std::string icon_path = "res_ui/generals46X45/";
		icon_path.append(generalMsgFromDb->get_head_photo());
		icon_path.append(".png");
		CCSprite * sprite_icon = CCSprite::create(icon_path.c_str());
		sprite_icon->setAnchorPoint(ccp(0.5f, 0.5f));
		sprite_icon->setPosition(ccp(smaillFrame->getContentSize().width/2, smaillFrame->getContentSize().height/2));
		smaillFrame->addChild(sprite_icon);

		//等级
		CCScale9Sprite * sprite_lvFrame = CCScale9Sprite::create("res_ui/lv_kuang.png");
		sprite_lvFrame->setAnchorPoint(ccp(0, 0));
		sprite_lvFrame->setPosition(ccp(46, 5));
		addChild(sprite_lvFrame);

		std::string _lv = "LV";
		char s_level [5];
		sprintf(s_level,"%d",generalBaseMsg->level());
		_lv.append(s_level);
		CCLabelTTF * label_level = CCLabelTTF::create(s_level,APP_FONT_NAME,12);
		label_level->setAnchorPoint(ccp(0.5f,0.5f));
		label_level->setPosition(ccp(smaillFrame->getContentSize().width-2,2));
		label_level->setPosition(ccp(sprite_lvFrame->getPosition().x+sprite_lvFrame->getContentSize().width/2,sprite_lvFrame->getPosition().y+sprite_lvFrame->getContentSize().height/2));
		addChild(label_level);
		//军衔
		if (generalBaseMsg->evolution() > 0)
		{
			CCSprite * imageView_rank = ActorUtils::createGeneralRankSprite(generalBaseMsg->evolution());
			imageView_rank->setScale(0.75f);
			imageView_rank->setAnchorPoint(ccp(0.5f,0.5f));
			imageView_rank->setPosition(ccp(59,55));
			addChild(imageView_rank);
		}

		//武将稀有度
		if (generalBaseMsg->rare()>0)
		{
			CCSprite * sprite_star = CCSprite::create(RecuriteActionItem::getStarPathByNum(generalBaseMsg->rare()).c_str());
			sprite_star->setAnchorPoint(ccp(0.5f,0.5f));
			sprite_star->setPosition(ccp(15,15));
			sprite_star->setScale(0.5f);
			addChild(sprite_star);
		}

		//职业
		std::string str_professionIcon = "gamescene_state/zhujiemian3/chongwutouxiang/";
		switch(generalMsgFromDb->get_profession())
		{
		case 1:
			{
				str_professionIcon.append("mj.png");
			}
			break;
		case 2:
			{
				str_professionIcon.append("gm.png");
			}
			break;
		case 3:
			{
				str_professionIcon.append("hj.png");
			}
			break;
		case 4:
			{
				str_professionIcon.append("ss.png");
			}
			break;
		}
		CCSprite *  sprite_profession = CCSprite::create(str_professionIcon.c_str());
		sprite_profession->setAnchorPoint(ccp(0.5f, 0.5f));
		sprite_profession->setPosition(ccp(87, 56));
		addChild(sprite_profession);

		//武将名字
		CCScale9Sprite * sprite_nameFrame = CCScale9Sprite::create("res_ui/zhezhao80_new.png");
		sprite_nameFrame->setAnchorPoint(ccp(0.5f, 0.5f));
		sprite_nameFrame->setPosition(ccp(137, 54));
		sprite_nameFrame->setPreferredSize(CCSizeMake(73,17));
		addChild(sprite_nameFrame);
		CCLabelTTF * label_name = CCLabelTTF::create(generalMsgFromDb->name().c_str(),APP_FONT_NAME,16);
		label_name->setAnchorPoint(ccp(0.5f, 0.5f));
		label_name->setPosition(ccp(137, 53));
		label_name->setColor(GameView::getInstance()->getGeneralsColorByQuality(generalBaseMsg->currentquality()));
		addChild(label_name);
		//生命
		CCLabelTTF * label_hp = CCLabelTTF::create("HP",APP_FONT_NAME,14);
		label_hp->setAnchorPoint(ccp(0.5f, 0.5f));
		label_hp->setPosition(ccp(88, 38));
		addChild(label_hp);
		CCScale9Sprite * imageView_hpBg = CCScale9Sprite::create("gamescene_state/zhujiemian3/chongwutouxiang/di_c.png");
		imageView_hpBg->setPreferredSize(CCSizeMake(76,11)); 
		imageView_hpBg->setAnchorPoint(ccp(0,0.5f));
		imageView_hpBg->setPosition(ccp(99,39));
		addChild(imageView_hpBg);
		CCSprite * imageView_hp = CCSprite::create("gamescene_state/zhujiemian3/chongwutouxiang/hp_2.png");
		imageView_hp->setAnchorPoint(ccp(0,0.5f));
		imageView_hp->setPosition(ccp(99,40));
		addChild(imageView_hp);

		//法力
		CCLabelTTF * label_mp = CCLabelTTF::create("MP",APP_FONT_NAME,14);
		label_mp->setAnchorPoint(ccp(0.5f, 0.5f));
		label_mp->setPosition(ccp(88, 27));
		addChild(label_mp);
		CCScale9Sprite * imageView_mpBg = CCScale9Sprite::create("gamescene_state/zhujiemian3/chongwutouxiang/di_c.png");
		imageView_mpBg->setPreferredSize(CCSizeMake(76,11)); 
		imageView_mpBg->setAnchorPoint(ccp(0,0.5f));
		imageView_mpBg->setPosition(ccp(99,26));
		addChild(imageView_mpBg);
		CCSprite * imageView_mp = CCSprite::create("gamescene_state/zhujiemian3/chongwutouxiang/mp_2.png");
		imageView_mp->setAnchorPoint(ccp(0,0.5f));
		imageView_mp->setPosition(ccp(99,27));
		addChild(imageView_mp);

		int h_cur = 0;
		int h_all = 0;
		int m_cur = 0;
		int m_all = 0;
		for (int i = 0;i<(int)GameView::getInstance()->generalsInLineDetailList.size();++i)
		{
			if (generalBaseMsg->id() == GameView::getInstance()->generalsInLineDetailList.at(i)->generalid())
			{
				h_cur = GameView::getInstance()->generalsInLineDetailList.at(i)->activerole().hp();
				h_all = GameView::getInstance()->generalsInLineDetailList.at(i)->activerole().maxhp();
				m_cur = GameView::getInstance()->generalsInLineDetailList.at(i)->activerole().mp();
				m_all = GameView::getInstance()->generalsInLineDetailList.at(i)->activerole().maxmp();
			}
		}
		float h_scale = (h_cur*1.0f)/(h_all*1.0f);
		if (h_scale > 1.0f)
			h_scale = 1.0f;

		float m_scale = (m_cur*1.0f)/(m_all*1.0f);
		if (m_scale > 1.0f)
			m_scale = 1.0f;

		imageView_hp->setTextureRect(CCRectMake(0,0,imageView_hp->getContentSize().width*h_scale,imageView_hp->getContentSize().height));
		imageView_mp->setTextureRect(CCRectMake(0,0,imageView_mp->getContentSize().width*m_scale,imageView_mp->getContentSize().height));

		//武将死亡
		if (h_cur <= 0)  
		{
			//底框变灰
// 			CCSpriteFrame * spriteFrame = CCSpriteFrame::create("gamescene_state/zhujiemian3/chongwutouxiang/di_b.png",CCRectMake(0,0,180,70));
// 			CCSpriteBatchNode * batchnode = CCSpriteBatchNode::createWithTexture(spriteFrame->getTexture(), 9);
//			sprite_bigFrame->updateWithBatchNode(batchnode, spriteFrame->getRect(), spriteFrame->isRotated(), CCRectMake(150,38,1,1);
			if (this->getChildByTag(ktag_sprite_bigFrame))
			{
				this->getChildByTag(ktag_sprite_bigFrame)->removeFromParent();
			}

			CCScale9Sprite * sprite_bigFrame = CCScale9Sprite::create("gamescene_state/zhujiemian3/chongwutouxiang/di_b.png");
			sprite_bigFrame->setAnchorPoint(ccp(0, 0));
			sprite_bigFrame->setPosition(ccp(0, 0));
			sprite_bigFrame->setPreferredSize(CCSizeMake(187,70));
			sprite_bigFrame->setTag(ktag_sprite_bigFrame);
			sprite_bigFrame->setZOrder(-1);
			addChild(sprite_bigFrame);
			CCLabelTTF * label_died = CCLabelTTF::create(StringDataManager::getString("generals_died"),APP_FONT_NAME,14);
			label_died->setAnchorPoint(ccp(0, 0.5f));
			label_died->setPosition(ccp(108, 13));
			addChild(label_died);
		}


		return true;
	}
	return false;
}

CGeneralBaseMsg* GeneralsListForTakeDrugCell::getCurGeneralBaseMsg()
{
	return curGeneralBaseMsg;
}
