#include "RewardTaskMainUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "SimpleAudioEngine.h"
#include "../../GameAudio.h"
#include "../../utils/StaticDataManager.h"
#include "../../AppMacros.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../utils/GameUtils.h"
#include "../extensions/CCRichLabel.h"
#include "RewardTaskData.h"
#include "RewardTaskItem.h"
#include "../../messageclient/protobuf/MissionMessage.pb.h"
#include "../../ui/generals_ui/RecuriteActionItem.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/CBoardMissionInfo.h"
#include "../missionscene/MissionManager.h"
#include "RewardTaskDetailUI.h"
#include "../../legend_script/CCTeachingGuide.h"

RewardTaskMainUI::RewardTaskMainUI(void)
{
}


RewardTaskMainUI::~RewardTaskMainUI(void)
{
}

RewardTaskMainUI* RewardTaskMainUI::create()
{
	RewardTaskMainUI * rewardTaskMainUI = new RewardTaskMainUI();
	if (rewardTaskMainUI && rewardTaskMainUI->init())
	{
		rewardTaskMainUI->autorelease();
		return rewardTaskMainUI;
	}
	CC_SAFE_DELETE(rewardTaskMainUI);
	return NULL;
}

bool RewardTaskMainUI::init()
{
	if (UIScene::init())
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

		m_base_layer = UILayer::create();
		m_base_layer->ignoreAnchorPointForPosition(false);
		m_base_layer->setAnchorPoint(ccp(.5f,.5f));
		m_base_layer->setPosition(ccp(winSize.width/2,winSize.height/2));
		m_base_layer->setContentSize(CCSizeMake(800,480));
		this->addChild(m_base_layer);	

		m_up_layer = UILayer::create();
		m_up_layer->ignoreAnchorPointForPosition(false);
		m_up_layer->setAnchorPoint(ccp(.5f,.5f));
		m_up_layer->setPosition(ccp(winSize.width/2,winSize.height/2));
		m_up_layer->setContentSize(CCSizeMake(800,480));
		this->addChild(m_up_layer);	

		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winSize);
		mengban->setAnchorPoint(CCPointZero);
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);

		UIPanel *s_panel_worldBossUI = (UIPanel*)GUIReader::shareReader()->widgetFromJsonFile("res_ui/offerReward_1.json");
		s_panel_worldBossUI->setAnchorPoint(ccp(0.5f, 0.5f));
		s_panel_worldBossUI->setPosition(ccp(winSize.width / 2, winSize.height / 2));
		s_panel_worldBossUI->setScale(1.0f);
		s_panel_worldBossUI->setTouchEnable(true);
		m_pUiLayer->addWidget(s_panel_worldBossUI);

		UIButton * btn_close = (UIButton*)UIHelper::seekWidgetByName(s_panel_worldBossUI,"Button_close");
		btn_close->setTouchEnable(true);
		btn_close->setPressedActionEnabled(true);
		btn_close->addReleaseEvent(this, coco_releaseselector(RewardTaskMainUI::CloseEvent));
		//刷新
		UIButton * btn_refresh = (UIButton*)UIHelper::seekWidgetByName(s_panel_worldBossUI,"Button_refresh");
		btn_refresh->setTouchEnable(true);
		btn_refresh->setPressedActionEnabled(true);
		btn_refresh->addReleaseEvent(this, coco_releaseselector(RewardTaskMainUI::RefreshEvent));
		//一键五星
		UIButton * btn_refreshFiveStar = (UIButton*)UIHelper::seekWidgetByName(s_panel_worldBossUI,"Button_refreshFiveStar");
		btn_refreshFiveStar->setTouchEnable(true);
		btn_refreshFiveStar->setPressedActionEnabled(true);
		btn_refreshFiveStar->addReleaseEvent(this, coco_releaseselector(RewardTaskMainUI::RefreshFiveStarEvent));
		
		l_remainTimeToFree = (UILabel*)UIHelper::seekWidgetByName(s_panel_worldBossUI,"Label_remainTimeToFree");
		l_remainTaskNum = (UILabel*)UIHelper::seekWidgetByName(s_panel_worldBossUI,"Label_remainTaskNum");
		l_refreshCostValue = (UILabel*)UIHelper::seekWidgetByName(s_panel_worldBossUI,"Label_refreshCost");
		l_refreshFiveStarCostValue = (UILabel*)UIHelper::seekWidgetByName(s_panel_worldBossUI,"Label_refreshFiveStarCost");
		l_freeThisTime = (UILabel*)UIHelper::seekWidgetByName(s_panel_worldBossUI,"Label_freeThisTime");

		this->schedule(schedule_selector(RewardTaskMainUI::update),1.0f);

		this->setTouchEnabled(false);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(winSize);

		return true;
	}
	return false;
}

void RewardTaskMainUI::onEnter()
{
	UIScene::onEnter();
}

void RewardTaskMainUI::onExit()
{
	UIScene::onExit();
}

bool RewardTaskMainUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return false;
}

void RewardTaskMainUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void RewardTaskMainUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void RewardTaskMainUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void RewardTaskMainUI::update( float dt )
{
	if (RewardTaskData::getInstance()->getTargetNpcId() == 0)
		return;

	if (GameView::getInstance()->missionManager->isOutOfMissionTalkArea(RewardTaskData::getInstance()->getTargetNpcId()))
	{
		RewardTaskDetailUI * td = (RewardTaskDetailUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRewardTaskDetailUI);
		if (td)
		{
			td->closeAnim();
		}
		this->closeAnim();
		return;
	}
	
	if (RewardTaskData::getInstance()->getRemainCDTime() > 0)
	{
		RewardTaskData::getInstance()->setRemainCDTime(RewardTaskData::getInstance()->getRemainCDTime()-1000);
		RewardTaskData::getInstance()->setCurRefreshType(NROM_GOLD);

		l_freeThisTime->setVisible(false);
		l_remainTimeToFree->setVisible(true);
		l_refreshCostValue->setVisible(true);
		
		std::string str_remainTime = RecuriteActionItem::timeFormatToString(RewardTaskData::getInstance()->getRemainCDTime()/1000);
		l_remainTimeToFree->setText(str_remainTime.c_str());
	}
	else
	{
		RewardTaskData::getInstance()->setRemainCDTime(0);
		RewardTaskData::getInstance()->setCurRefreshType(FREE_REFRESH);

		l_freeThisTime->setVisible(true);
		l_remainTimeToFree->setVisible(false);
		l_refreshCostValue->setVisible(false);
	}
}

void RewardTaskMainUI::CloseEvent( CCObject *pSender )
{
	RewardTaskDetailUI * td = (RewardTaskDetailUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRewardTaskDetailUI);
	if (td)
	{
		td->closeAnim();
	}

	this->closeAnim();
}

void RewardTaskMainUI::RefreshFiveMissionPresent()
{
	//delete old
	for(int i = 0;i<5;i++)
	{
		RewardTaskItem * rewardTaskItem = dynamic_cast<RewardTaskItem *>(m_base_layer->getChildByTag(kTag_RewardTaskItem_Base+i));
		if (rewardTaskItem)
		{
			rewardTaskItem->removeFromParent();
		}
	}
	//add new
	for (int i = 0;i<RewardTaskData::getInstance()->p_taskList.size();i++)
	{
		if (i>=5)
			continue;

		RewardTaskItem * rewardTaskItem = RewardTaskItem::create(RewardTaskData::getInstance()->p_taskList.at(i),i);
		rewardTaskItem->setAnchorPoint(ccp(.5f,.5f));
		rewardTaskItem->setPosition(ccp(RewardTastItem_First_Pos_x+RewardTastItem_space*i,RewardTastItem_First_Pos_y));
		rewardTaskItem->setTag(kTag_RewardTaskItem_Base+RewardTaskData::getInstance()->p_taskList.at(i)->index());
		m_base_layer->addChild(rewardTaskItem);
	}
}

void RewardTaskMainUI::RefreshFiveMissionPresentWithAnm()
{
	float newItemDelayTime = 0.f;
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	//delete old
	for(int i = 0;i<5;i++)
	{
		RewardTaskItem * rewardTaskItem = dynamic_cast<RewardTaskItem *>(m_base_layer->getChildByTag(kTag_RewardTaskItem_Base+i));
		if (rewardTaskItem)
		{
			CCSequence * sequence = CCSequence::create(
				CCDelayTime::create(0.1f*i),
				CCEaseBackIn::create(CCMoveTo::create(0.2f,ccp(RewardTastItem_First_Pos_x+RewardTastItem_space*i,RewardTastItem_First_Pos_y-winSize.height))),
				CCRemoveSelf::create(),
			NULL);
			rewardTaskItem->runAction(sequence);
			sequence->setTag(kTag_RefreshAction_deleteOld);

			newItemDelayTime = 0.15f*i+0.15f;
		}
	}
	//add new
	for (int i = 0;i<RewardTaskData::getInstance()->p_taskList.size();i++)
	{
		if (i>=5)
			continue;

		/////////////////////////////////////fourth way////////////////////////////////////////////
		/**************************从上方移入*******************************/
		RewardTaskItem * rewardTaskItem = RewardTaskItem::create(RewardTaskData::getInstance()->p_taskList.at(i),i);
		rewardTaskItem->setAnchorPoint(ccp(.5f,.5f));
		//rewardTaskItem->setPosition(ccp(RewardTastItem_First_Pos_x+RewardTastItem_space*i,RewardTastItem_First_Pos_y));
		rewardTaskItem->setPosition(ccp(RewardTastItem_First_Pos_x+RewardTastItem_space*i,RewardTastItem_First_Pos_y+winSize.height));
		rewardTaskItem->setTag(kTag_RewardTaskItem_Base+RewardTaskData::getInstance()->p_taskList.at(i)->index());
		m_base_layer->addChild(rewardTaskItem);

		CCSequence * sequence = CCSequence::create(
			CCDelayTime::create(newItemDelayTime+0.1f*i),
			CCEaseBackOut::create(CCMoveTo::create(0.2f,ccp(RewardTastItem_First_Pos_x+RewardTastItem_space*i,RewardTastItem_First_Pos_y))),
			NULL);
		rewardTaskItem->runAction(sequence);
		sequence->setTag(kTag_RefreshAction_addNew);
	}
}

void RewardTaskMainUI::RefreshMissionState( int index,int state )
{
	for (int i = 0;i<5;i++)
	{
		RewardTaskItem * rewardTaskItem = (RewardTaskItem*)m_base_layer->getChildByTag(kTag_RewardTaskItem_Base+i);
		if (rewardTaskItem)
		{
			rewardTaskItem->setZOrder(0);
			if (i == index)
			{
				rewardTaskItem->setZOrder(5);
				rewardTaskItem->RefreshBtnMissionState(state,true);
			}
		}
	}
}

void RewardTaskMainUI::RefreshEvent( CCObject * pSender )
{
	bool isActionExist = false;
	for(int i = 0;i<5;i++)
	{
		RewardTaskItem * rewardTaskItem = dynamic_cast<RewardTaskItem *>(m_base_layer->getChildByTag(kTag_RewardTaskItem_Base+i));
		if (rewardTaskItem)
		{
			if (rewardTaskItem->getActionByTag(kTag_RefreshAction_deleteOld) || rewardTaskItem->getActionByTag(kTag_RefreshAction_addNew))
			{
				isActionExist = true;
				break;
			}
		}
	}
	if (isActionExist)
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("RewardTaskItem_missionstate_isRefreshing"));
		return;
	}

	if (RewardTaskData::getInstance()->getRemainCDTime() > 0)
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(7016,(void *)NROM_GOLD);
	}
	else
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(7016,(void *)FREE_REFRESH);
	}
}

void RewardTaskMainUI::RefreshFiveStarEvent( CCObject *pSender )
{
	bool isActionExist = false;
	for(int i = 0;i<5;i++)
	{
		RewardTaskItem * rewardTaskItem = dynamic_cast<RewardTaskItem *>(m_base_layer->getChildByTag(kTag_RewardTaskItem_Base+i));
		if (rewardTaskItem)
		{
			if (rewardTaskItem->getActionByTag(kTag_RefreshAction_deleteOld) || rewardTaskItem->getActionByTag(kTag_RefreshAction_addNew))
			{
				isActionExist = true;
				break;
			}
		}
	}
	if (isActionExist)
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("RewardTaskItem_missionstate_isRefreshing"));
		return;
	}

	GameMessageProcessor::sharedMsgProcessor()->sendReq(7016,(void *)FIVE_STAR);
}

void RewardTaskMainUI::RefreshRemainTaskNum()
{
	std::string str_remainNum ;
	char s_remain [10];
	sprintf(s_remain,"%d",RewardTaskData::getInstance()->getRemainTaskNum());
	str_remainNum.append(s_remain);
	str_remainNum.append("/");
	char s_total [10];
	sprintf(s_total,"%d",RewardTaskData::getInstance()->getAllTTaskNum());
	str_remainNum.append(s_total);
	l_remainTaskNum->setText(str_remainNum.c_str());
}

// void RewardTaskMainUI::RunAddNewAction()
// {
// 	//delete old
// 	for(int i = 0;i<5;i++)
// 	{
// 		RewardTaskItem * rewardTaskItem = dynamic_cast<RewardTaskItem *>(m_base_layer->getChildByTag(kTag_RewardTaskItem_Base+i));
// 		if (rewardTaskItem)
// 		{
// 			rewardTaskItem->removeFromParent();
// 		}
// 	}
// 	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
// 	//add new
// 	for (int i = 0;i<RewardTaskData::getInstance()->p_taskList.size();i++)
// 	{
// 		if (i>=5)
// 			continue;

		/////////////////////////////////////first way////////////////////////////////////////////
		/**************************洗牌的方式*******************************/
		/*
		RewardTaskItem * rewardTaskItem = RewardTaskItem::create(RewardTaskData::getInstance()->p_taskList.at(i),i);
		rewardTaskItem->setAnchorPoint(ccp(.5f,.5f));
		//rewardTaskItem->setPosition(ccp(RewardTastItem_First_Pos_x+RewardTastItem_space*i,RewardTastItem_First_Pos_y));
		rewardTaskItem->setPosition(ccp(RewardTastItem_First_Pos_x+RewardTastItem_space*2,RewardTastItem_First_Pos_y-winSize.height/2));
		rewardTaskItem->setTag(kTag_RewardTaskItem_Base+RewardTaskData::getInstance()->p_taskList.at(i)->index());
		m_base_layer->addChild(rewardTaskItem);

		//begin run action
		CCSequence * sequence = CCSequence::create(
			CCMoveTo::create(0.5f,ccp(RewardTastItem_First_Pos_x+RewardTastItem_space*2,RewardTastItem_First_Pos_y-winSize.height/4)),
			CCRotateTo::create(0.2f,18*(i-2)),
			CCDelayTime::create(0.4f),
			CCMoveTo::create(0.5f,ccp(RewardTastItem_First_Pos_x+RewardTastItem_space*i,RewardTastItem_First_Pos_y)),
			NULL);
		rewardTaskItem->runAction(sequence);
		*/

		/////////////////////////////////////second way////////////////////////////////////////////
		/**************************拍下*******************************/
		/*
		RewardTaskItem * rewardTaskItem = RewardTaskItem::create(RewardTaskData::getInstance()->p_taskList.at(i),i);
		rewardTaskItem->setAnchorPoint(ccp(.5f,.5f));
		//rewardTaskItem->setPosition(ccp(RewardTastItem_First_Pos_x+RewardTastItem_space*i,RewardTastItem_First_Pos_y));
		rewardTaskItem->setPosition(ccp(RewardTastItem_First_Pos_x+RewardTastItem_space*i,RewardTastItem_First_Pos_y));
		rewardTaskItem->setTag(kTag_RewardTaskItem_Base+RewardTaskData::getInstance()->p_taskList.at(i)->index());
		m_base_layer->addChild(rewardTaskItem);
		rewardTaskItem->setVisible(false);
		rewardTaskItem->setScale(1.5f);

		CCSequence * sequence = CCSequence::create(
			CCDelayTime::create(0.3f*i),
			CCShow::create(),
			CCEaseBackIn::create(CCScaleTo::create(0.23f,1.0f,1.0f)),
			NULL);
		rewardTaskItem->runAction(sequence);
		*/

		/////////////////////////////////////third way////////////////////////////////////////////
		/**************************从右侧移入*******************************/
		/*
		RewardTaskItem * rewardTaskItem = RewardTaskItem::create(RewardTaskData::getInstance()->p_taskList.at(i),i);
		rewardTaskItem->setAnchorPoint(ccp(.5f,.5f));
		//rewardTaskItem->setPosition(ccp(RewardTastItem_First_Pos_x+RewardTastItem_space*i,RewardTastItem_First_Pos_y));
		rewardTaskItem->setPosition(ccp(RewardTastItem_First_Pos_x+RewardTastItem_space*i+winSize.width,RewardTastItem_First_Pos_y));
		rewardTaskItem->setTag(kTag_RewardTaskItem_Base+RewardTaskData::getInstance()->p_taskList.at(i)->index());
		m_base_layer->addChild(rewardTaskItem);

		CCSequence * sequence = CCSequence::create(
			CCDelayTime::create(0.4f*i),
			CCEaseBackOut::create(CCMoveTo::create(0.3f,ccp(RewardTastItem_First_Pos_x+RewardTastItem_space*i,RewardTastItem_First_Pos_y))),
			NULL);
		rewardTaskItem->runAction(sequence);
		*/

		/////////////////////////////////////fourth way////////////////////////////////////////////
		/**************************从上方移入*******************************/
/*
		RewardTaskItem * rewardTaskItem = RewardTaskItem::create(RewardTaskData::getInstance()->p_taskList.at(i),i);
		rewardTaskItem->setAnchorPoint(ccp(.5f,.5f));
		//rewardTaskItem->setPosition(ccp(RewardTastItem_First_Pos_x+RewardTastItem_space*i,RewardTastItem_First_Pos_y));
		rewardTaskItem->setPosition(ccp(RewardTastItem_First_Pos_x+RewardTastItem_space*i,RewardTastItem_First_Pos_y+winSize.height));
		rewardTaskItem->setTag(kTag_RewardTaskItem_Base+RewardTaskData::getInstance()->p_taskList.at(i)->index());
		m_base_layer->addChild(rewardTaskItem);

		CCSequence * sequence = CCSequence::create(
			CCDelayTime::create(0.35f*i),
			CCEaseBackOut::create(CCMoveTo::create(0.3f,ccp(RewardTastItem_First_Pos_x+RewardTastItem_space*i,RewardTastItem_First_Pos_y))),
			NULL);
		rewardTaskItem->runAction(sequence);
		*/
//	}
//}


void RewardTaskMainUI::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void RewardTaskMainUI::addCCTutorialIndicator( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height-480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,60,40);
// 	tutorialIndicator->setPosition(ccp(pos.x,pos.y));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	m_up_layer->addChild(tutorialIndicator);

	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,94,43,true);
	tutorialIndicator->setDrawNodePos(ccp(pos.x-56+_w,pos.y-70+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void RewardTaskMainUI::removeCCTutorialIndicator()
{
	CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(m_up_layer->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		CCTeachingGuide* teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}