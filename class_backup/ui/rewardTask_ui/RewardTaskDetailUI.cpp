#include "RewardTaskDetailUI.h"
#include "../../messageclient/element/CBoardMissionInfo.h"
#include "../../AppMacros.h"
#include "../extensions/CCRichLabel.h"
#include "../backpackscene/EquipmentItem.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../missionscene/RewardItem.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "GameView.h"
#include "../../ui/missionscene/MissionManager.h"
#include "../../messageclient/element/MissionInfo.h"
#include "../../utils/GameUtils.h"
#include "GameAudio.h"
#include "RewardTaskData.h"
#include "../../utils/StaticDataManager.h"

RewardTaskDetailUI::RewardTaskDetailUI(void)
{
}


RewardTaskDetailUI::~RewardTaskDetailUI(void)
{
	delete curBoardMissionInfo;
}

RewardTaskDetailUI* RewardTaskDetailUI::create(CBoardMissionInfo * boardMissionInfo)
{
	RewardTaskDetailUI * rewardTaskDetailUI = new RewardTaskDetailUI();
	if (rewardTaskDetailUI && rewardTaskDetailUI->init(boardMissionInfo))
	{
		rewardTaskDetailUI->autorelease();
		return rewardTaskDetailUI;
	}
	CC_SAFE_DELETE(rewardTaskDetailUI);
	return NULL;
}

bool RewardTaskDetailUI::init(CBoardMissionInfo * boardMissionInfo)
{
	if (UIScene::init())
	{
		curBoardMissionInfo = new CBoardMissionInfo();
		curBoardMissionInfo->CopyFrom(*boardMissionInfo);

		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

		m_base_layer = UILayer::create();
		this->addChild(m_base_layer);	

		UIPanel *s_panel_rewardTaskInfo = (UIPanel*)GUIReader::shareReader()->widgetFromJsonFile("res_ui/offerReward_popupo_1.json");
		s_panel_rewardTaskInfo->setTouchEnable(true);
		m_pUiLayer->addWidget(s_panel_rewardTaskInfo);

		UIButton * btn_close = (UIButton*)UIHelper::seekWidgetByName(s_panel_rewardTaskInfo,"Button_close");
		btn_close->setTouchEnable(true);
		btn_close->setPressedActionEnabled(true);
		btn_close->addReleaseEvent(this, coco_releaseselector(RewardTaskDetailUI::CloseEvent));

		btn_getMission = (UIButton*)UIHelper::seekWidgetByName(s_panel_rewardTaskInfo,"Button_getMission");
		btn_getMission->setTouchEnable(true);
		btn_getMission->setPressedActionEnabled(true);
		btn_getMission->addReleaseEvent(this, coco_releaseselector(RewardTaskDetailUI::GetMissionEvent));

		btn_finishMission = (UIButton*)UIHelper::seekWidgetByName(s_panel_rewardTaskInfo,"Button_finishMission");
		btn_finishMission->setTouchEnable(true);
		btn_finishMission->setPressedActionEnabled(true);
		btn_finishMission->addReleaseEvent(this, coco_releaseselector(RewardTaskDetailUI::FinishMissionEvent));

		btn_finishRightNow = (UIButton*)UIHelper::seekWidgetByName(s_panel_rewardTaskInfo,"Button_finishRightNow");
		btn_finishRightNow->setTouchEnable(true);
		btn_finishRightNow->setPressedActionEnabled(true);
		btn_finishRightNow->addReleaseEvent(this, coco_releaseselector(RewardTaskDetailUI::FinishRightNowEvent));

		btn_autoFindPath = (UIButton*)UIHelper::seekWidgetByName(s_panel_rewardTaskInfo,"Button_autoFindPath");
		btn_autoFindPath->setTouchEnable(true);
		btn_autoFindPath->setPressedActionEnabled(true);
		btn_autoFindPath->addReleaseEvent(this, coco_releaseselector(RewardTaskDetailUI::AutoFindPathEvent));

		btn_giveUpMission = (UIButton*)UIHelper::seekWidgetByName(s_panel_rewardTaskInfo,"Button_giveUpMission");
		btn_giveUpMission->setTouchEnable(true);
		btn_giveUpMission->setPressedActionEnabled(true);
		btn_giveUpMission->addReleaseEvent(this, coco_releaseselector(RewardTaskDetailUI::GiveUpMissionEvent));

		refreshBtnState(curBoardMissionInfo->state());

		//task info 
		int scroll_height = 0;
		int _space = 15;
		//name
		CCSprite* sprite_name = CCSprite::create("res_ui/renwubb/renwumingcheng.png");	
		CCLabelTTF* label_name = CCLabelTTF::create(boardMissionInfo->missionname().c_str(),APP_FONT_NAME,18,CCSizeMake(270, 0 ), kCCTextAlignmentLeft);
		int zeroLineHeight = label_name->getContentSize().height+_space;
		//star
		CCSprite* sprite_star= CCSprite::create("res_ui/renwubb/renwuxingji.png");
		char s_star[10];
		sprintf(s_star,"%d",boardMissionInfo->star());
		//CCLabelTTF* label_star = CCLabelTTF::create(s_star,APP_FONT_NAME,18,CCSizeMake(270, 0 ), kCCTextAlignmentLeft);
		int firstLineHeight = zeroLineHeight+sprite_star->getContentSize().height+_space;
		//target
		CCSprite *sprite_target = CCSprite::create("res_ui/renwubb/renwumubiao.png");
		CCRichLabel *label_target = CCRichLabel::createWithString(boardMissionInfo->summary().c_str(),CCSizeMake(325, 0 ),NULL,NULL );
		int secondLineHeight = firstLineHeight + label_target->getContentSize().height+_space;
		//description
		CCSprite *sprite_des = CCSprite::create("res_ui/renwubb/renwuxushu.png");
		CCRichLabel *label_des = CCRichLabel::createWithString(boardMissionInfo->description().c_str(),CCSizeMake(325, 0 ), NULL,NULL,0,18,4);
		int thirdLineHeight = secondLineHeight+label_des->getContentSize().height + _space;
		//rewardSprite
		CCSprite *sprite_award = CCSprite::create("res_ui/renwubb/renwujiangli.png");  
		//reward(gold,exp)
		bool isHaveExp = false;
		bool isHaveGold =  false; 
		CCSprite *sprite_expItemFrame;
		if (boardMissionInfo->rewards().has_exp() && boardMissionInfo->rewards().exp() > 0)
		{
			sprite_expItemFrame = CCSprite::create(EquipOrangeFramePath);

			CCSprite * sprite_goodsItem = CCSprite::create("res_ui/exp.png");
			sprite_goodsItem->setAnchorPoint(ccp(0.5f,0.5f));
			sprite_goodsItem->setPosition(ccp(sprite_expItemFrame->getContentSize().width/2,sprite_expItemFrame->getContentSize().height/2));
			sprite_expItemFrame->addChild(sprite_goodsItem);
			sprite_expItemFrame->setScale(0.8f);

			char s[20];
			sprintf(s,"%d",boardMissionInfo->rewards().exp());
			CCLabelTTF * l_expValue = CCLabelTTF::create(s,APP_FONT_NAME,18);
			l_expValue->setAnchorPoint(ccp(0,0.5f));
			l_expValue->setPosition(ccp(80,28));
			sprite_expItemFrame->addChild(l_expValue);

			isHaveExp = true;
		}

		CCSprite *sprite_goldItemFrame;
		if (boardMissionInfo->rewards().has_gold() && boardMissionInfo->rewards().gold() > 0)
		{
			sprite_goldItemFrame = CCSprite::create(EquipOrangeFramePath);

			CCSprite * sprite_goodsItem = CCSprite::create("res_ui/coins.png");
			sprite_goodsItem->setAnchorPoint(ccp(0.5f,0.5f));
			sprite_goodsItem->setPosition(ccp(sprite_goldItemFrame->getContentSize().width/2,sprite_goldItemFrame->getContentSize().height/2));
			sprite_goldItemFrame->addChild(sprite_goodsItem);
			sprite_goldItemFrame->setScale(0.8f);

			char s[20];
			sprintf(s,"%d",boardMissionInfo->rewards().gold());
			CCLabelTTF * l_goldValue = CCLabelTTF::create(s,APP_FONT_NAME,18);
			l_goldValue->setAnchorPoint(ccp(0,0.5f));
			l_goldValue->setPosition(ccp(80,25));
			sprite_goldItemFrame->addChild(l_goldValue);

			isHaveGold = true;
		}

		int fourthLineHeight;
		if (isHaveExp || isHaveGold)
		{
			fourthLineHeight = thirdLineHeight + 50 + _space;
		}
		else
		{
			fourthLineHeight = thirdLineHeight + _space;
		}
		//reward(goods).
		int fifthLineHeight = fourthLineHeight;
		if (boardMissionInfo->has_rewards())
		{
			for(int i = 0;i<boardMissionInfo->rewards().goods_size();++i)
			{
				CCSize awardSize = CCSizeMake(150,50);
				if (i%2 == 0)
				{
					fifthLineHeight += awardSize.height;
				}
			}
		}

		scroll_height = fifthLineHeight+_space;

		CCScrollView *m_contentScrollView = CCScrollView::create(CCSizeMake(460,225));
		m_contentScrollView->setViewSize(CCSizeMake(460, 225));
		m_contentScrollView->ignoreAnchorPointForPosition(false);
		m_contentScrollView->setTouchEnabled(true);
		m_contentScrollView->setDirection(kCCScrollViewDirectionVertical);
		m_contentScrollView->setAnchorPoint(ccp(0,0));
		m_contentScrollView->setPosition(ccp(50,87));
		m_contentScrollView->setBounceable(true);
		m_contentScrollView->setClippingToBounds(true);
		m_base_layer->addChild(m_contentScrollView);
		if (scroll_height > 225)
		{
			m_contentScrollView->setContentSize(ccp(460,scroll_height));
			m_contentScrollView->setContentOffset(ccp(0,225-scroll_height));  
		}
		else
		{
			m_contentScrollView->setContentSize(ccp(460,225));
		}
		m_contentScrollView->setClippingToBounds(true);

		sprite_name->setAnchorPoint(ccp(0,1.0f));
		sprite_name->setPosition( ccp( 15, m_contentScrollView->getContentSize().height -zeroLineHeight) );
		m_contentScrollView->addChild(sprite_name);
		label_name->setAnchorPoint(ccp(0,1.0f));
		label_name->setPosition( ccp( sprite_name->getContentSize().width+15, m_contentScrollView->getContentSize().height -zeroLineHeight) );
		m_contentScrollView->addChild(label_name);


		sprite_star->setAnchorPoint(ccp(0,1.0f));
		sprite_star->setPosition( ccp( 15, m_contentScrollView->getContentSize().height -firstLineHeight) );
		m_contentScrollView->addChild(sprite_star);
		//label_star->setAnchorPoint(ccp(0,1.0f));
		//label_star->setPosition( ccp( sprite_star->getContentSize().width+15, m_contentScrollView->getContentSize().height -firstLineHeight) );
		//m_contentScrollView->addChild(label_star);
		//星级
		for(int i=0;i<5;i++)
		{
			if (i<curBoardMissionInfo->star())
			{
				CCSprite* sprite_starIcon = CCSprite::create("res_ui/star2.png");
				sprite_starIcon->setAnchorPoint(ccp(0.5f,0.5f));
				sprite_starIcon->setPosition(ccp(sprite_star->getContentSize().width+20+23*i,m_contentScrollView->getContentSize().height -firstLineHeight));
				sprite_starIcon->setScale(.6f);
				m_contentScrollView->addChild(sprite_starIcon);
			}
			else
			{
				CCSprite* sprite_starIcon = CCSprite::create("res_ui/star_off.png");
				sprite_starIcon->setAnchorPoint(ccp(0.5f,0.5f));
				sprite_starIcon->setPosition(ccp(sprite_star->getContentSize().width+20+23*i,m_contentScrollView->getContentSize().height -firstLineHeight));
				sprite_starIcon->setScale(1.0f);
				m_contentScrollView->addChild(sprite_starIcon);
			}
		}

		sprite_target->setAnchorPoint(ccp(0,1.0f));
		sprite_target->setPosition( ccp( 15, m_contentScrollView->getContentSize().height -secondLineHeight+label_target->getContentSize().height-sprite_target->getContentSize().height) );
		m_contentScrollView->addChild(sprite_target);
		label_target->setAnchorPoint(ccp(0,1.0f));
		label_target->setPosition( ccp( sprite_target->getContentSize().width+15, m_contentScrollView->getContentSize().height -secondLineHeight) );
		m_contentScrollView->addChild(label_target);

		sprite_des->setAnchorPoint(ccp(0,1.0f));
		sprite_des->setPosition( ccp( 15, m_contentScrollView->getContentSize().height -thirdLineHeight+label_des->getContentSize().height-sprite_des->getContentSize().height) );
		m_contentScrollView->addChild(sprite_des);
		label_des->setAnchorPoint(ccp(0,1.0f));
		label_des->setPosition( ccp( sprite_des->getContentSize().width+15, m_contentScrollView->getContentSize().height -thirdLineHeight) );
		m_contentScrollView->addChild(label_des);

		sprite_award->setAnchorPoint(ccp(0,1.0f));

		if (isHaveExp || isHaveGold)
		{
			sprite_award->setPosition( ccp( 15, m_contentScrollView->getContentSize().height -fourthLineHeight+25) );
		}
		else
		{
			sprite_award->setPosition( ccp( 15, m_contentScrollView->getContentSize().height -fourthLineHeight-25) );
		}

		m_contentScrollView->addChild(sprite_award);

		if (isHaveExp && isHaveGold)
		{
			sprite_expItemFrame->ignoreAnchorPointForPosition(false);
			sprite_expItemFrame->setAnchorPoint(ccp(0,1.0f));
			sprite_expItemFrame->setPosition( ccp( 120, m_contentScrollView->getContentSize().height -fourthLineHeight) );
			m_contentScrollView->addChild(sprite_expItemFrame);
			sprite_goldItemFrame->ignoreAnchorPointForPosition(false);
			sprite_goldItemFrame->setAnchorPoint(ccp(0,1.0f));
			sprite_goldItemFrame->setPosition( ccp( 261, m_contentScrollView->getContentSize().height -fourthLineHeight) );
			m_contentScrollView->addChild(sprite_goldItemFrame);
		}
		else if (isHaveExp && (!isHaveGold))
		{
			sprite_expItemFrame->ignoreAnchorPointForPosition(false);
			sprite_expItemFrame->setAnchorPoint(ccp(0,1.0f));
			sprite_expItemFrame->setPosition( ccp( 120, m_contentScrollView->getContentSize().height -fourthLineHeight) );
			m_contentScrollView->addChild(sprite_expItemFrame);
		}
		else if (isHaveGold && (!isHaveExp))
		{
			sprite_goldItemFrame->ignoreAnchorPointForPosition(false);
			sprite_goldItemFrame->setAnchorPoint(ccp(0,1.0f));
			sprite_goldItemFrame->setPosition( ccp( 120, m_contentScrollView->getContentSize().height -fourthLineHeight) );
			m_contentScrollView->addChild(sprite_goldItemFrame);
		}
		else
		{

		}

		if (boardMissionInfo->has_rewards())
		{
			for(int i = 0;i<boardMissionInfo->rewards().goods_size();++i)
			{
				GoodsInfo * goodsInfo = new GoodsInfo();
				goodsInfo->CopyFrom(boardMissionInfo->rewards().goods(i).goods());
				RewardItem * rewardItem = RewardItem::create(goodsInfo,boardMissionInfo->rewards().goods(i).quantity());
				m_contentScrollView->addChild(rewardItem);
				delete goodsInfo;
				if(i%2 == 0)
				{
					rewardItem->ignoreAnchorPointForPosition(false);
					rewardItem->setAnchorPoint(ccp(0,1.0f));
					if (isHaveExp || isHaveGold)
					{
						rewardItem->setPosition(ccp(120, m_contentScrollView->getContentSize().height - fourthLineHeight -5 - 5*((int)(i/2)) -  rewardItem->getContentSize().height*((int)(i/2))));
					}
					else
					{
						rewardItem->setPosition(ccp(120, m_contentScrollView->getContentSize().height -fourthLineHeight));
					}
				}
				else
				{
					rewardItem->ignoreAnchorPointForPosition(false);
					rewardItem->setAnchorPoint(ccp(0,1.0f));
					if (isHaveExp || isHaveGold)
					{
						rewardItem->setPosition(ccp(261, m_contentScrollView->getContentSize().height - fourthLineHeight -5 - 5*((int)(i/2)) - rewardItem->getContentSize().height*((int)(i/2))));
					}
					else
					{
						rewardItem->setPosition(ccp(261, m_contentScrollView->getContentSize().height -fourthLineHeight));
					}
				}
			}
		}

		this->setContentSize(s_panel_rewardTaskInfo->getContentSize());
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);

		return true;
	}
	return false;
}

void RewardTaskDetailUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void RewardTaskDetailUI::onExit()
{
	UIScene::onExit();
}

bool RewardTaskDetailUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return true;
}

void RewardTaskDetailUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void RewardTaskDetailUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void RewardTaskDetailUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void RewardTaskDetailUI::update( float dt )
{
}

void RewardTaskDetailUI::CloseEvent( CCObject *pSender )
{
	this->closeAnim();
}

void RewardTaskDetailUI::GetMissionEvent( CCObject *pSender )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(7015,(void *)curBoardMissionInfo->index());
}

void RewardTaskDetailUI::FinishMissionEvent( CCObject *pSender )
{
	for (int i = 0;i<GameView::getInstance()->missionManager->MissionList_MainScene.size();i++)
	{
		if (curBoardMissionInfo->packageid() == GameView::getInstance()->missionManager->MissionList_MainScene.at(i)->missionpackageid())
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(7007, (char *)curBoardMissionInfo->packageid().c_str());
			break;
		}
	}	
}

void RewardTaskDetailUI::FinishRightNowEvent( CCObject *pSender )
{
	char des [100];
	const char *strings = StringDataManager::getString("RewardTask_sureToFinishRightNow");
	sprintf(des,strings,curBoardMissionInfo->quickcompletegold());
	GameView::getInstance()->showPopupWindow(des,2,this,coco_selectselector(RewardTaskDetailUI::SureToFinishRightNow),NULL);	
}

void RewardTaskDetailUI::SureToFinishRightNow( CCObject *pSender )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(7023);
}

void RewardTaskDetailUI::AutoFindPathEvent( CCObject *pSender )
{
	for (int i = 0;i<GameView::getInstance()->missionManager->MissionList_MainScene.size();i++)
	{
		if (curBoardMissionInfo->packageid() == GameView::getInstance()->missionManager->MissionList_MainScene.at(i)->missionpackageid())
		{
			GameView::getInstance()->missionManager->curMissionInfo->CopyFrom(*GameView::getInstance()->missionManager->MissionList_MainScene.at(i));
			GameView::getInstance()->missionManager->addMission(GameView::getInstance()->missionManager->curMissionInfo);
			break;
		}
	}	
}

void RewardTaskDetailUI::GiveUpMissionEvent( CCObject *pSender )
{
	GameView::getInstance()->showPopupWindow(StringDataManager::getString("RewardTask_sureToGiveUpMission"),2,this,coco_selectselector(RewardTaskDetailUI::SureToGiveUpMission),NULL);	
}

void RewardTaskDetailUI::SureToGiveUpMission( CCObject *pSender )
{
	for (int i = 0;i<GameView::getInstance()->missionManager->MissionList_MainScene.size();i++)
	{
		if (curBoardMissionInfo->packageid() == GameView::getInstance()->missionManager->MissionList_MainScene.at(i)->missionpackageid())
		{
			GameUtils::playGameSound(MISSION_CANCEL, 2, false);
			GameView::getInstance()->missionManager->curMissionInfo->CopyFrom(*GameView::getInstance()->missionManager->MissionList_MainScene.at(i));
			GameMessageProcessor::sharedMsgProcessor()->sendReq(7009, this);
			break;
		}
	}	
}

void RewardTaskDetailUI::refreshBtnState( int missionState )
{
	switch(missionState)
	{
	case dispatched:// 已分派
		{
			btn_getMission->setVisible(true);
			btn_finishMission->setVisible(false);
			btn_finishRightNow->setVisible(false);
			btn_autoFindPath->setVisible(false);
			btn_giveUpMission->setVisible(false);
		}
		break;
	case accepted:// 已接受
		{
			btn_getMission->setVisible(false);
			btn_finishMission->setVisible(false);
			btn_finishRightNow->setVisible(true);
			btn_autoFindPath->setVisible(true);
			btn_giveUpMission->setVisible(true);
		}
		break;
	case done://已完成
		{
			btn_getMission->setVisible(false);
			btn_finishMission->setVisible(true);
			btn_finishRightNow->setVisible(false);
			btn_autoFindPath->setVisible(false);
			btn_giveUpMission->setVisible(false);
		}
		break;
	case submitted:// 已提交
		{
			btn_getMission->setVisible(false);
			btn_finishMission->setVisible(false);
			btn_finishRightNow->setVisible(false);
			btn_autoFindPath->setVisible(false);
			btn_giveUpMission->setVisible(false);
		}
		break;
	case abandoned:// 已放弃
		{
			btn_getMission->setVisible(false);
			btn_finishMission->setVisible(false);
			btn_finishRightNow->setVisible(false);
			btn_autoFindPath->setVisible(false);
			btn_giveUpMission->setVisible(false);
		}
		break;
	case submitFailed:// 提交失败
		{
			btn_getMission->setVisible(false);
			btn_finishMission->setVisible(false);
			btn_finishRightNow->setVisible(false);
			btn_autoFindPath->setVisible(false);
			btn_giveUpMission->setVisible(false);
		}
		break;
	case canceled://  已取消
		{
			btn_getMission->setVisible(false);
			btn_finishMission->setVisible(false);
			btn_finishRightNow->setVisible(false);
			btn_autoFindPath->setVisible(false);
			btn_giveUpMission->setVisible(false);
		}
		break;
	default:
		{
			btn_getMission->setVisible(false);
			btn_finishMission->setVisible(false);
			btn_finishRightNow->setVisible(false);
			btn_autoFindPath->setVisible(false);
			btn_giveUpMission->setVisible(false);
		}
	}
}
