#include "RewardTaskItem.h"
#include "../../messageclient/element/CBoardMissionInfo.h"
#include "../../AppMacros.h"
#include "../../messageclient/protobuf/MissionMessage.pb.h"
#include "RewardTaskDetailUI.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../missionscene/MissionManager.h"
#include "../../messageclient/element/MissionInfo.h"
#include "../../utils/StaticDataManager.h"
#include "RewardTaskMainUI.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/ScriptManager.h"

#define  kTagRotateAnm 777

RewardTaskItem::RewardTaskItem():
m_nPos(-1)
{

}

RewardTaskItem::~RewardTaskItem()
{
	delete curBoardMission;
}

RewardTaskItem * RewardTaskItem::create(CBoardMissionInfo * missionInfo,int pos)
{
	RewardTaskItem * rewardTaskItem = new RewardTaskItem();
	if (rewardTaskItem && rewardTaskItem->init(missionInfo,pos))
	{
		rewardTaskItem->autorelease();
		return rewardTaskItem;
	}
	CC_SAFE_DELETE(rewardTaskItem);
	return NULL;
}

bool RewardTaskItem::init(CBoardMissionInfo * missionInfo,int pos)
{
	if (UIScene::init())
	{
		m_nPos = pos;
		curBoardMission = new CBoardMissionInfo();
		curBoardMission->CopyFrom(*missionInfo);

		//底
		UIButton * btn_frame = UIButton::create();
		btn_frame->setTextures("res_ui/kuang01_new.png","res_ui/kuang01_new.png","");
		btn_frame->setTouchEnable(true);
		btn_frame->setScale9Enable(true);
		btn_frame->setScale9Size(CCSizeMake(131,202));
		btn_frame->setCapInsets(CCRectMake(14,30,1,1));
		btn_frame->setAnchorPoint(ccp(.5f,.5f));
		btn_frame->setPosition(ccp(131/2,202/2));
		btn_frame->addReleaseEvent(this,coco_releaseselector(RewardTaskItem::LookOverEvent));
		btn_frame->setPressedActionEnabled(true,0.9f,1.2f);
		m_pUiLayer->addWidget(btn_frame);
		//名字底
		UIImageView* imageView_nameFrame = UIImageView::create();
		imageView_nameFrame->setTexture("res_ui/LV4_diaa.png");
		imageView_nameFrame->setScale9Enable(true);
		imageView_nameFrame->setScale9Size(CCSizeMake(108,49));
		imageView_nameFrame->setCapInsets(CCRectMake(11,11,1,1));
		imageView_nameFrame->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_nameFrame->setPosition(ccp(0,61));
		btn_frame->addChild(imageView_nameFrame);
 		//名字
		UILabel * l_name = UILabel::create();
		l_name->setText(missionInfo->missionname().c_str());
		l_name->setTextAreaSize(CCSizeMake(90,0));
		l_name->setFontName(APP_FONT_NAME);
		l_name->setFontSize(18);
		l_name->setAnchorPoint(ccp(0.5f,0.5f));
		l_name->setPosition(ccp(0,61));
		btn_frame->addChild(l_name);
		//星级
		for(int i=0;i<5;i++)
		{
			UIImageView* imageView_star = UIImageView::create();
			imageView_star->setTexture("");
			imageView_star->setAnchorPoint(ccp(0.5f,0.5f));
			imageView_star->setPosition(ccp(-131/2+20+23*i,19));
			btn_frame->addChild(imageView_star);

			if (i<missionInfo->star())
			{
				imageView_star->setTexture("res_ui/star2.png");
				imageView_star->setScale(.6f);
			}
			else
			{
				imageView_star->setTexture("res_ui/star_off.png");
				imageView_star->setScale(1.0f);
			}
		}
		// exp
		UIImageView* imageView_exp = UIImageView::create();
		imageView_exp->setTexture("res_ui/exp.png");
		imageView_exp->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_exp->setPosition(ccp(28-131/2,-10));
		btn_frame->addChild(imageView_exp);
		imageView_exp->setScale(.8f);
		//exp value
		UILabel * l_expValue = UILabel::create();
		char s_exp [20];
		sprintf(s_exp,"%d",missionInfo->rewards().exp());
		l_expValue->setText(s_exp);
		l_expValue->setFontName(APP_FONT_NAME);
		l_expValue->setFontSize(14);
		l_expValue->setAnchorPoint(ccp(0.f,0.5f));
		l_expValue->setPosition(ccp(imageView_exp->getPosition().x + imageView_exp->getContentSize().width/2,imageView_exp->getPosition().y));
		btn_frame->addChild(l_expValue);

		// gold
		UIImageView* imageView_gold = UIImageView::create();
		imageView_gold->setTexture("res_ui/coins.png");
		imageView_gold->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_gold->setPosition(ccp(34-131/2,-27));
		btn_frame->addChild(imageView_gold);
		imageView_gold->setScale(.8f);
		//gold value
		UILabel * l_goldValue = UILabel::create();
		char s_gold [20];
		sprintf(s_gold,"%d",missionInfo->rewards().gold());
		l_goldValue->setText(s_gold);
		l_goldValue->setFontName(APP_FONT_NAME);
		l_goldValue->setFontSize(14);
		l_goldValue->setAnchorPoint(ccp(0.f,0.5f));
		l_goldValue->setPosition(ccp(imageView_gold->getPosition().x + imageView_gold->getContentSize().width/2+2,imageView_gold->getPosition().y));
		btn_frame->addChild(l_goldValue);

		imageView_mask = UIImageView::create();
		imageView_mask->setTexture("res_ui/zhezhao30.png");
		imageView_mask->setScale9Enable(true);
		imageView_mask->setScale9Size(CCSizeMake(131,202));
		imageView_mask->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_mask->setPosition(ccp(0,0));
		btn_frame->addChild(imageView_mask);
		imageView_mask->setVisible(false);

		//显示任务状态的按钮
		btn_missionState = UIButton::create();
		btn_missionState->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		btn_missionState->setTouchEnable(true);
		btn_missionState->setScale9Enable(true);
		btn_missionState->setScale9Size(CCSizeMake(98,40));
		btn_missionState->setCapInsets(CCRectMake(18,18,1,1));
		btn_missionState->setAnchorPoint(ccp(.5f,.5f));
		btn_missionState->setPosition(ccp(0,3-64));
		btn_missionState->setPressedActionEnabled(true);
		btn_missionState->addReleaseEvent(this,coco_releaseselector(RewardTaskItem::MissionStateEvent));
		btn_frame->addChild(btn_missionState);
		l_missionState = UILabel::create();
		l_missionState->setText("jieshourenwu");
		l_missionState->setFontName(APP_FONT_NAME);
		l_missionState->setFontSize(18);
		l_missionState->setAnchorPoint(ccp(0.5f,0.5f));
		l_missionState->setPosition(ccp(0,0));
		btn_missionState->addChild(l_missionState);

		imageView_missionState = UIImageView::create();
		imageView_missionState->setTexture("");
		imageView_missionState->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_missionState->setPosition(ccp(131/2,37));
		m_pUiLayer->addWidget(imageView_missionState);
		imageView_missionState->setVisible(false);

		RefreshBtnMissionState(curBoardMission->state(),false);

		this->setContentSize(CCSizeMake(131,202));
		return true;
	}
	return false;
}

void RewardTaskItem::MissionStateEvent( CCObject *pSender )
{
	switch(curBoardMission->state())
	{
	case dispatched:// 已分派
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(7015,(void *)curBoardMission->index());
			
			RewardTaskMainUI * rewardTaskMainUI = (RewardTaskMainUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRewardTaskMainUI);
			if (rewardTaskMainUI)
			{
				Script* sc = ScriptManager::getInstance()->getScriptById(rewardTaskMainUI->mTutorialScriptInstanceId);
				if(sc != NULL)
					sc->endCommand(this);
			}
		}
		break;
	case accepted:// 已接受
		{
// 			for (int i = 0;i<GameView::getInstance()->missionManager->MissionList_MainScene.size();i++)
// 			{
// 				if (curBoardMission->packageid() == GameView::getInstance()->missionManager->MissionList_MainScene.at(i)->missionpackageid())
// 				{
// 					GameView::getInstance()->missionManager->curMissionInfo->CopyFrom(*GameView::getInstance()->missionManager->MissionList_MainScene.at(i));
// 					GameView::getInstance()->missionManager->addMission(GameView::getInstance()->missionManager->curMissionInfo);
// 					break;
// 				}
// 			}	

			char des [100];
			const char *strings = StringDataManager::getString("RewardTask_sureToFinishRightNow");
			sprintf(des,strings,curBoardMission->quickcompletegold());
			GameView::getInstance()->showPopupWindow(des,2,this,coco_selectselector(RewardTaskItem::SureToFinishRightNow),NULL);	
		}
		break;
	case done://已完成
		{
			for (int i = 0;i<GameView::getInstance()->missionManager->MissionList_MainScene.size();i++)
			{
				if (curBoardMission->packageid() == GameView::getInstance()->missionManager->MissionList_MainScene.at(i)->missionpackageid())
				{
					GameMessageProcessor::sharedMsgProcessor()->sendReq(7007, (char *)curBoardMission->packageid().c_str());
					break;
				}
			}	
		}
		break;
	case submitted:// 已提交
		{
			
		}
		break;
	case abandoned:// 已放弃
		{
			
		}
		break;
	case submitFailed:// 提交失败
		{
			
		}
		break;
	case canceled://  已取消
		{
			
		}
		break;
	}
}

void RewardTaskItem::LookOverEvent( CCObject *pSender )
{
	//GameMessageProcessor::sharedMsgProcessor()->sendReq(1142,(void*)curPartner->role().rolebase().roleid());
	if (!GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRewardTaskDetailUI))
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		RewardTaskDetailUI * detailUI = RewardTaskDetailUI::create(curBoardMission);
		detailUI->ignoreAnchorPointForPosition(false);
		detailUI->setAnchorPoint(ccp(.5f,.5f));
		detailUI->setPosition(ccp(winSize.width/2,winSize.height/2));
		detailUI->setTag(kTagRewardTaskDetailUI);
		GameView::getInstance()->getMainUIScene()->addChild(detailUI);
	}
}

int RewardTaskItem::getCurPos()
{
	return m_nPos;
}

void RewardTaskItem::RefreshBtnMissionState(int state,bool isAnm)
{
	btn_missionState->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");

	switch(state)
	{
	case dispatched:// 已分派
		{
			curBoardMission->set_state(dispatched);

			btn_missionState->setVisible(true);
			imageView_missionState->setVisible(false);
			imageView_mask->setVisible(false);
			l_missionState->setText(StringDataManager::getString("RewardTaskItem_missionstate_get"));
		}
		break;
	case accepted:// 已接受
		{
			curBoardMission->set_state(accepted);

			btn_missionState->setVisible(true);
			imageView_missionState->setVisible(false);
			imageView_mask->setVisible(false);
			l_missionState->setText(StringDataManager::getString("RewardTaskItem_missionstate_finishRightNow"));
		}
		break;
	case done://已完成
		{
			curBoardMission->set_state(done);

			btn_missionState->setVisible(true);
			btn_missionState->setTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
			imageView_missionState->setVisible(false);
			imageView_mask->setVisible(false);
			l_missionState->setText(StringDataManager::getString("RewardTaskItem_missionstate_finish"));
		}
		break;
	case submitted:// 已提交
		{
			curBoardMission->set_state(submitted);

			btn_missionState->setVisible(false);
			imageView_missionState->setVisible(true);
			imageView_mask->setVisible(true);
			imageView_missionState->setTexture("res_ui/offerReward/dacheng.png");
			if (isAnm)
			{
				imageView_missionState->setScale(3.0f);
				imageView_missionState->setVisible(true);
				CCFiniteTimeAction*  action = CCSequence::create(
					CCEaseElasticIn::create(CCScaleTo::create(0.7f,1.0f)),
					NULL);
				imageView_missionState->runAction(action);
			}
		}
		break;
	case abandoned:// 已放弃
		{
			curBoardMission->set_state(abandoned);
			
			btn_missionState->setVisible(false);
			imageView_missionState->setVisible(true);
			imageView_mask->setVisible(true);
			imageView_missionState->setTexture("res_ui/offerReward/fangqi.png");
			if (isAnm)
			{
				imageView_missionState->setScale(3.0f);
				imageView_missionState->setVisible(true);
				CCFiniteTimeAction*  action = CCSequence::create(
					CCEaseElasticIn::create(CCScaleTo::create(0.7f,1.0f)),
					NULL);
				imageView_missionState->runAction(action);
			}
		}
		break;
	case submitFailed:// 提交失败
		{
			curBoardMission->set_state(submitFailed);
			//l_missionState->setText("tijiaoshibai");
		}
		break;
	case canceled://  已取消
		{
			curBoardMission->set_state(canceled);
			
			btn_missionState->setVisible(false);
			imageView_missionState->setVisible(true);
			imageView_mask->setVisible(true);
			imageView_missionState->setTexture("res_ui/offerReward/fangqi.png");
			if (isAnm)
			{
				imageView_missionState->setScale(3.0f);
				imageView_missionState->setVisible(true);
				CCFiniteTimeAction*  action = CCSequence::create(
					CCEaseElasticIn::create(CCScaleTo::create(0.7f,1.0f)),
					NULL);

				imageView_missionState->runAction(action);
			}
		}
		break;
	}
}

void RewardTaskItem::SureToFinishRightNow( CCObject *pSender )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(7023);
}


