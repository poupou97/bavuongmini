#ifndef _FAMILYAMLS_ 
#define _FAMILYAMLS_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "FamilyUI.h"

class FamilyAlms:public UIScene
{
public:
	FamilyAlms(void);
	~FamilyAlms(void);

	static FamilyAlms * create();
	bool init();
	void onEnter();
	void onExit();

	void callBackInput(CCObject * obj);
	void callBackSurContrbution(CCObject * obj);
	void callBackGetCountNum(CCObject * obj);
	void callBackClose(CCObject * obj);
	int getCurContrGoodsAmount();

	void refreshInfo(bool isDefault);
private:
	FamilyUI * familyui;

	UILabel * labelFamilyLevel;
	UILabel * labelFamilyContrbution;

	int contributionCount;
	UILabel * labelCount;
	UILabel * labelContrbutionOfMine;
	UILabel * familyContrbutionAdd;
	UILabel * familyPersonContrbutionAdd;
};
#endif;

