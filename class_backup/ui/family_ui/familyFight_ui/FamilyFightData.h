#ifndef _UI_FAMILYFIGHTUI_FAMILYFIGHTDATA_H_
#define _UI_FAMILYFIGHTUI_FAMILYFIGHTDATA_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

/////////////////////////////////
/**
 * 家族战逻辑类
 * @author yangjun 
 * @version 0.1.0
 * @date 2014.07.15
 */

class CGuildDailyReward;
class CGuildFightRecord;
class CGuildFightRanking;

class FamilyFightData
{
public:
	FamilyFightData(void);
	~FamilyFightData(void);

	// 通知类型 0:未开始 1：匹配中 2：准备战斗 3：战斗开始 4：战斗结束
	enum FamilyFightStatus {
		status_none = 0,
		status_in_sequence,   //in the the waiting sequence
		status_prepareForFight,
		status_in_fight,
		status_fightEnded
	};


	////////////////////////// 配合使用 /////////////////////////
	// 战场时间状态 
	enum FamilyFightTimeClass{
		timeClass_none = 0,  //休整期
		timeClass_signUp,      //报名期
		timeClass_fight          //战斗期
	};
	// 家族报名状态  true 报名， false 未报名
	static bool isRegisted;
	 // 家族是否被取消战斗 1:取消 0：未取消
	static bool isCancel;
	// 战斗状态
	enum FamilyFightFightClasss {
		fightClass_none = 0,
		fightClass_in_sequence,   //匹配中
		fightClass_startFight,    //战斗开始
		fightClass_endFight   //战斗结束
	};
	///////////////////////////////////////////////////

	struct BossInfo{
		int bossid;
		int pos_x;
		int pos_y;
	};

	struct BossHpInfo{
		int curHp;
		int maxHp;
	};

public: 
	static FamilyFightData * s_familyFightData;
	static FamilyFightData * getInstance();

	void update();

	void setAllToDefault();

	static void setStatus(int status);
	static int getStatus();

	static void setCurrentBattleId(int battleId);
	static int getCurrentBattleId();

	static void setFamilyFightTimeClass(int status);
	static int getFamilyFightTimeClass();

	static void setFamilyFightFightClasss(int status);
	static int getFamilyFightFightClasss();

	static void setFamilyFightRanking(int ranking);
	static int getFamilyFightRanking();

	static void setFamilyFightScore(int score);
	static int getFamilyFightScore();

	static void setFamilyRewardStatus(bool isGet);
	static int getFamilyRewardStatus();

	static void setCurTime(std::string curTime);
	static std::string getCurTime();
	static void setNextTime(std::string nextTime);
	static std::string getNextTime();

	static void setRemainTime(long long remaintime);
	static long long getRemainTime();
	static void setEntryLeftTime(long long remaintime);
	static long long getEntryLeftTime();

	static void setBossOwnerPos(int bossid,int x,int y);
	static BossInfo getBossOwnerPos();
	static void setBossCommonPos(int bossid,int x,int y);
	static BossInfo getBossCommonPos();
	static void setBossEnemyPos(int bossid,int x,int y);
	static BossInfo getBossEnemyPos();

	static void setBossOwnerHpInfo(int curhp,int maxhp);
	static BossHpInfo getBossOwnerHpInfo();
	static void setBossCommonHpInfo(int curhp,int maxhp);
	static BossHpInfo getBossCommonHpInfo();
	static void setBossEnemyHpInfo(int curhp,int maxhp);
	static BossHpInfo getBossEnemyHpInfo();

private:
	static int s_status;
	static int s_currentBattleId;


	// 战场时间状态 
	static int s_familyFightTimeClass;
	// 战斗状态
	static int s_familyFightFightClasss;

	// 家族排名
	static int s_familyFightRanking;
	// 家族积分
	static int s_familyFightScore;	
	//奖励领取状态
	static bool s_familyRewardStatus;

	// 当前战斗时间
	static std::string s_sCurTime;
	// 下场战斗时间
	static std::string s_sNextTime;
	//当前家族战剩余时间（正在战斗中，距当前家族战结束的时间）
	static long long s_nRemainTime;
	//本场进入剩余时间
	static long long s_nEntryLeftTime;

	//我方BOSS位置信息
	static BossInfo s_Boss_Owner_pos;
	//中立BOSS位置信息
	static BossInfo s_Boss_Common_pos;
	//敌方BOSS位置信息
	static BossInfo s_Boss_enemy_pos;

	//我方BOSS血量信息
	static BossHpInfo s_Boss_Owner_hpInfo;
	//中立BOSS血量信息
	static BossHpInfo s_Boss_Common_hpInfo;
	//敌方BOSS血量信息
	static BossHpInfo s_Boss_enemy_hpInfo;


public:
	// 奖励物品
	std::vector<CGuildDailyReward*> familyFightRewardList;
};

#endif
