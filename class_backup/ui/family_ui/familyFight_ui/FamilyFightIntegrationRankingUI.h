#ifndef _UI_FAMILYFIGHTUI_FAMILYFIGHTINTEGRATIONRANKINGUI_H_
#define _UI_FAMILYFIGHTUI_FAMILYFIGHTINTEGRATIONRANKINGUI_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../../extensions/UIScene.h"
#include "../../generals_ui/GeneralsListBase.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CGuildFightRanking;

/////////////////////////////////
/**
 *家族战积分排行榜
 * @author yangjun 
 * @version 0.1.0
 * @date 2014.07.17
 */

class FamilyFightIntegrationRankingUI:public GeneralsListBase, public CCTableViewDataSource, public CCTableViewDelegate
{
public:
	FamilyFightIntegrationRankingUI(void);
	~FamilyFightIntegrationRankingUI(void);

public:
	static FamilyFightIntegrationRankingUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);
private:
	UILayer * m_base_layer;																							// 基调layer
	CCTableView * m_tableView;																						// 主tableView
	UILabelBMFont * lbf_myRanking;

	int lastSelectCellId;
	int selectCellId;

private:
	void initUI();

public:
	// 家族战积分排行榜
	std::vector<CGuildFightRanking*> familyFightRankingList;

	void refreshUI();

	void CloseEvent(CCObject *pSender);
};

#endif

