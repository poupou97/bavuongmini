#include "FamilyFightRewardsUI.h"
#include "GameView.h"
#include "../FamilyUI.h"
#include "../../../gamescene_state/MainScene.h"
#include "../../../utils/StaticDataManager.h"
#include "../../../ui/extensions/ShowSystemInfo.h"
#include "FamilyFightData.h"
#include "../../../gamescene_state/role/MyPlayer.h"
#include "../../../messageclient/element/CGuildBase.h"
#include "../../../messageclient/GameMessageProcessor.h"
#include "../../../messageclient/element/CGuildDailyReward.h"
#include "../../../AppMacros.h"
#include "../../../messageclient/element/GoodsInfo.h"
#include "../../vip_ui/VipRewardCellItem.h"


FamilyFightRewardsUI::FamilyFightRewardsUI()
{
}


FamilyFightRewardsUI::~FamilyFightRewardsUI()
{
}

FamilyFightRewardsUI* FamilyFightRewardsUI::create()
{
	FamilyFightRewardsUI * familyFightRewardsUI = new FamilyFightRewardsUI();
	if (familyFightRewardsUI && familyFightRewardsUI->init())
	{
		familyFightRewardsUI->autorelease();
		return familyFightRewardsUI;
	}
	CC_SAFE_DELETE(familyFightRewardsUI);
	return NULL;
}

bool FamilyFightRewardsUI::init()
{
	if (UIScene::init())
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

		m_base_layer = UILayer::create();
		this->addChild(m_base_layer);	

		//加载UI
		UIPanel * ppanel = (UIPanel *)GUIReader::shareReader()->widgetFromJsonFile("res_ui/familyRankReward_1.json");
		ppanel->setAnchorPoint(ccp(0.0f,0.0f));
		ppanel->setPosition(CCPointZero);	
		ppanel->setTouchEnable(true);
		m_pUiLayer->addWidget(ppanel);

		UIButton * btn_close = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		btn_close->setTouchEnable( true );
		btn_close->setPressedActionEnabled(true);
		btn_close->addReleaseEvent(this,coco_releaseselector(FamilyFightRewardsUI::CloseEvent));

		UIPanel * panel_singCopy =  (UIPanel*)UIHelper::seekWidgetByName(ppanel,"Panel_singCopy");
		panel_singCopy->setVisible(false);
		UIPanel * panel_familyFight =  (UIPanel*)UIHelper::seekWidgetByName(ppanel,"Panel_familyFight");
		panel_familyFight->setVisible(true);

		l_myRanking = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_FRanking");
		char s_ranking [20];
		sprintf(s_ranking,"%d",FamilyFightData::getFamilyFightRanking());
		l_myRanking->setText(s_ranking);

		m_tableView = CCTableView::create(this, CCSizeMake(275,250));
		//m_tableView ->setSelectedEnable(true);   // 支持选中状态的显示
		//m_tableView ->setSelectedScale9Texture("res_ui/highlight.png", CCRectMake(25, 25, 1, 1), ccp(0,0));   // 设置九宫格图片
		m_tableView->setDirection(kCCScrollViewDirectionVertical);
		m_tableView->setAnchorPoint(ccp(0, 0));
		m_tableView->setPosition(ccp(49,35));
		m_tableView->setDelegate(this);
		m_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		m_tableView->setPressedActionEnabled(false);
		m_base_layer->addChild(m_tableView);

		this->setContentSize(ppanel->getContentSize());
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);


		return true;
	}
	return false;
}

void FamilyFightRewardsUI::onEnter()
{
	UIScene::onEnter();
}

void FamilyFightRewardsUI::onExit()
{
	UIScene::onExit();
}

bool FamilyFightRewardsUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return true;
}

void FamilyFightRewardsUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void FamilyFightRewardsUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void FamilyFightRewardsUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void FamilyFightRewardsUI::scrollViewDidScroll( CCScrollView* view )
{

}

void FamilyFightRewardsUI::scrollViewDidZoom( CCScrollView* view )
{

}

void FamilyFightRewardsUI::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{

}

cocos2d::CCSize FamilyFightRewardsUI::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeMake(275,64);
}

cocos2d::extension::CCTableViewCell* FamilyFightRewardsUI::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell = new CCTableViewCell();
	cell->autorelease();

	CGuildDailyReward* guideReward = FamilyFightData::getInstance()->familyFightRewardList.at(idx);
	ccColor3B color_green = ccc3(47,93,13);

	// 底图
	CCScale9Sprite * sprite_bigFrame = CCScale9Sprite::create("res_ui/LV3_dikuang.png");
	sprite_bigFrame->setAnchorPoint(ccp(0, 0));
	sprite_bigFrame->setPosition(ccp(0, 0));
	sprite_bigFrame->setCapInsets(CCRectMake(9,14,1,1));
	sprite_bigFrame->setPreferredSize(CCSizeMake(273,62));
	cell->addChild(sprite_bigFrame);

	if (guideReward->endrank() - guideReward->startrank() == 0)
	{
		if (guideReward->startrank() <= 3 )
		{
			std::string icon_path = "res_ui/rank";
			char s_ranking[10];
			sprintf(s_ranking,"%d",guideReward->startrank());
			icon_path.append(s_ranking);
			icon_path.append(".png");
			CCSprite * sp_ranking = CCSprite::create(icon_path.c_str());
			sp_ranking->setAnchorPoint(ccp(0.5f,0.5f));
			sp_ranking->setPosition(ccp(36,32));
			sp_ranking->setScale(0.95f);
			cell->addChild(sp_ranking);
		}
		else
		{
			char s_ranking[10];
			sprintf(s_ranking,"%d",guideReward->startrank());
			CCLabelTTF * l_ranking = CCLabelTTF::create(s_ranking,APP_FONT_NAME,18);
			l_ranking->setAnchorPoint(ccp(0.5f,0.5f));
			l_ranking->setPosition(ccp(36,32));
			l_ranking->setColor(color_green);
			cell->addChild(l_ranking);
		}
	}
	else
	{
		std::string str_ranking_des ;
		char s_ranking_start[10];
		sprintf(s_ranking_start,"%d",guideReward->startrank());
		char s_ranking_end[10];
		sprintf(s_ranking_end,"%d",guideReward->endrank());
		str_ranking_des.append(s_ranking_start);
		str_ranking_des.append("-");
		str_ranking_des.append(s_ranking_end);
		CCLabelTTF * l_ranking = CCLabelTTF::create(str_ranking_des.c_str(),APP_FONT_NAME,18);
		l_ranking->setAnchorPoint(ccp(0.5f,0.5f));
		l_ranking->setPosition(ccp(36,32));
		l_ranking->setColor(color_green);
		cell->addChild(l_ranking);
	}

	if (guideReward->has_goods())
	{
		GoodsInfo * goodInfo = new GoodsInfo();
		goodInfo->CopyFrom(guideReward->goods());
		int num = 0;
		num = guideReward->number();

		VipRewardCellItem * goodsItem = VipRewardCellItem::create(goodInfo,num);
		goodsItem->ignoreAnchorPointForPosition(false);
		goodsItem->setAnchorPoint(ccp(0.5f,0.5f));
		goodsItem->setPosition(ccp(112,31));
		goodsItem->setScale(0.8f);
		cell->addChild(goodsItem);

		delete goodInfo;
	}

	if (FamilyFightData::getInstance()->getFamilyFightRanking() >= guideReward->startrank() && FamilyFightData::getInstance()->getFamilyFightRanking() <= guideReward->endrank())
	{
		if (!FamilyFightData::getInstance()->getFamilyRewardStatus())
		{
			UILayer * btn_layer = UILayer::create();
			cell->addChild(btn_layer);
			btn_layer->setSwallowsTouches(false);

			UIButton * btn_getReward = UIButton::create();
			btn_getReward->setTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
			btn_getReward->setScale9Enabled(true);
			btn_getReward->setScale9Size(CCSizeMake(103,42));
			btn_getReward->setCapInsets(CCRectMake(18,9,2,23));
			btn_getReward->setTouchEnable(true);
			btn_getReward->setPressedActionEnabled(true);
			btn_getReward->setTag(idx);
			btn_getReward->setAnchorPoint(ccp(.5f,.5f));
			btn_getReward->setPosition(ccp(215,31));
			btn_getReward->addReleaseEvent(this,coco_releaseselector(FamilyFightRewardsUI::getRewardEvent));
			btn_layer->addWidget(btn_getReward);

			UILabel * l_getReward = UILabel::create();
			l_getReward->setText(StringDataManager::getString("btn_lingqu"));
			l_getReward->setAnchorPoint(ccp(.5f,.5f));
			l_getReward->setFontName(APP_FONT_NAME);
			l_getReward->setFontSize(20);
			l_getReward->setPosition(ccp(0,0));
			btn_getReward->addChild(l_getReward);
		}
		else
		{
			CCSprite * sp_di = CCSprite::create("res_ui/zhezhao_btn.png");
			sp_di->setAnchorPoint(ccp(0.5f,0.5f));
			sp_di->setPosition(ccp(215,31));
			cell->addChild(sp_di);
			CCLabelTTF *l_des = CCLabelTTF::create(StringDataManager::getString("vip_reward_Geted"),APP_FONT_NAME,18);
			l_des->setAnchorPoint(ccp(0.5f,0.5f));
			l_des->setPosition(ccp(sp_di->getContentSize().width/2,sp_di->getContentSize().height/2));
			sp_di->addChild(l_des);
		}
	}

	return cell;
}

unsigned int FamilyFightRewardsUI::numberOfCellsInTableView( CCTableView *table )
{
	return FamilyFightData::getInstance()->familyFightRewardList.size();
}

void FamilyFightRewardsUI::CloseEvent( CCObject * pSender )
{
	this->closeAnim();
}

void FamilyFightRewardsUI::getRewardEvent( CCObject * pSender )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1546);
}

void FamilyFightRewardsUI::ReloadTableViewWithOutChangeOffSet()
{
	CCPoint _s = m_tableView->getContentOffset();
	int _h = m_tableView->getContentSize().height + _s.y;
	m_tableView->reloadData();
	CCPoint temp = ccp(_s.x,_h - m_tableView->getContentSize().height);
	m_tableView->setContentOffset(temp); 
}

void FamilyFightRewardsUI::RefreshMyRanking()
{
	char s_ranking [20];
	sprintf(s_ranking,"%d",FamilyFightData::getFamilyFightRanking());
	l_myRanking->setText(s_ranking);
}
