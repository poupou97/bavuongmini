
#ifndef _UI_FAMILYFIGHTUI_BATTLESITUATIONUI_H_
#define _UI_FAMILYFIGHTUI_BATTLESITUATIONUI_H_

#include "../../extensions/UIScene.h"
#include "cocos-ext.h"
#include "cocos2d.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CGuildBattleInfo;

class BattleSituationUI : public UIScene,public CCTableViewDelegate,public CCTableViewDataSource
{
public:
	BattleSituationUI();
	~BattleSituationUI();

	static BattleSituationUI * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);

	void CloseEvent(CCObject *pSender);

	void RefreshOwnData();
	void RefreshOwnDataWithOutChangeOffSet();
	void RefreshEmptyData();
	void RefreshEmptyDataWithOutChangeOffSet();

	void RefreshUI();

private:
	CCTableView* m_tableView_blue;
	CCTableView* m_tableView_red;

	int lastSelectCellId;
	int selectCellId;

	UILayer * u_layer;

	UILabel * l_blueName;
	UILabel * l_redName;
	//UIImageView * ImageView_blueBlood;
	//UIImageView * ImageView_redBlood;
	//UILabel * l_blueBloodValue;
	//UILabel * l_redBloodValue;

	CCProgressTimer * pt_blueBlood;
	CCProgressTimer * pt_redBlood;
	CCLabelTTF * l_blueBloodValue;
	CCLabelTTF * l_redBloodValue;

public:
	CGuildBattleInfo* blueInfo;
	CGuildBattleInfo* redInfo;
};

#endif

