#include "FamilyFightIntegrationRankingUI.h"
#include "../../../loadscene_state/LoadSceneState.h"
#include "SimpleAudioEngine.h"
#include "../../../GameAudio.h"
#include "../../../utils/StaticDataManager.h"
#include "../../../AppMacros.h"
#include "GameView.h"
#include "../../../gamescene_state/GameSceneState.h"
#include "../../../gamescene_state/MainScene.h"
#include "../../../gamescene_state/role/MyPlayer.h"
#include "../../../utils/GameUtils.h"
#include "../../../messageclient/element/CDeadHistory.h"
#include "../../../messageclient/element/CBoss.h"
#include "../../../utils/GameConfig.h"
#include "FamilyFightData.h"
#include "../../../messageclient/element/CGuildFightRecord.h"
#include "../../../messageclient/element/CGuildFightRanking.h"
#include "../../../messageclient/GameMessageProcessor.h"

#define  SelectImageTag 458

FamilyFightIntegrationRankingUI::FamilyFightIntegrationRankingUI(void):
lastSelectCellId(0),
selectCellId(0)
{
	//delete old ranking
	std::vector<CGuildFightRanking*>::iterator iter_ranking;
	for (iter_ranking = familyFightRankingList.begin(); iter_ranking != familyFightRankingList.end(); ++iter_ranking)
	{
		delete *iter_ranking;
	}
	familyFightRankingList.clear();
}


FamilyFightIntegrationRankingUI::~FamilyFightIntegrationRankingUI(void)
{
}

FamilyFightIntegrationRankingUI* FamilyFightIntegrationRankingUI::create()
{
	FamilyFightIntegrationRankingUI * rankingUI = new FamilyFightIntegrationRankingUI();
	if (rankingUI && rankingUI->init())
	{
		rankingUI->autorelease();
		return rankingUI;
	}
	CC_SAFE_DELETE(rankingUI);
	return NULL;
}

bool FamilyFightIntegrationRankingUI::init()
{
	if (UIScene::init())
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		everyPageNum = 20;

		m_base_layer = UILayer::create();
		this->addChild(m_base_layer);	

		initUI();

		return true;
	}
	return false;
}

void FamilyFightIntegrationRankingUI::onEnter()
{
	UIScene::onEnter();
}

void FamilyFightIntegrationRankingUI::onExit()
{
	UIScene::onExit();
}

void FamilyFightIntegrationRankingUI::initUI()
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

	UIImageView *mengban = UIImageView::create();
	mengban->setTexture("res_ui/zhezhao80.png");
	mengban->setScale9Enable(true);
	mengban->setScale9Size(winSize);
	mengban->setAnchorPoint(ccp(.5f,.5f));
	m_pUiLayer->addWidget(mengban);

	//加载UI
	UIPanel * ppanel = (UIPanel *)GUIReader::shareReader()->widgetFromJsonFile("res_ui/familyfamily_1.json");
	ppanel->setAnchorPoint(ccp(0.0f,0.0f));
	ppanel->setPosition(CCPointZero);	
	ppanel->setTouchEnable(true);
	m_pUiLayer->addWidget(ppanel);
	mengban->setPosition(ccp(ppanel->getContentSize().width/2,ppanel->getContentSize().height/2));

	UIButton * btn_close = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
	btn_close->setTouchEnable( true );
	btn_close->setPressedActionEnabled(true);
	btn_close->addReleaseEvent(this,coco_releaseselector(FamilyFightIntegrationRankingUI::CloseEvent));

	UIPanel * panel_history = (UIPanel*)UIHelper::seekWidgetByName(ppanel,"Panel_historyNotes");
	panel_history->setVisible(false);
	UIPanel * panel_ranking = (UIPanel*)UIHelper::seekWidgetByName(ppanel,"Panel_integrationRanking");
	panel_ranking->setVisible(true);

	lbf_myRanking = (UILabelBMFont*)UIHelper::seekWidgetByName(ppanel,"LabelBMFont_rankingValue");
	if (FamilyFightData::getInstance()->getFamilyFightRanking() <= 0)
	{
		lbf_myRanking->setText(StringDataManager::getString("activy_none"));
	}
	else
	{
		char s_myRanking [20];
		sprintf(s_myRanking,"%d",FamilyFightData::getInstance()->getFamilyFightRanking());
		lbf_myRanking->setText(s_myRanking);
	}

	// 主tableView
	m_tableView = CCTableView::create(this, CCSizeMake(666,299));
	//m_tableView ->setSelectedEnable(true);   // 支持选中状态的显示
	//m_tableView ->setSelectedScale9Texture("res_ui/highlight.png", CCRectMake(25, 25, 1, 1), ccp(0,0));   // 设置九宫格图片
	m_tableView->setDirection(kCCScrollViewDirectionVertical);
	m_tableView->setAnchorPoint(ccp(0, 0));
	m_tableView->setPosition(ccp(75,64));
	m_tableView->setDelegate(this);
	m_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
	m_tableView->setPressedActionEnabled(false);
	m_base_layer->addChild(m_tableView);

	this->setContentSize(ppanel->getContentSize());
}


bool FamilyFightIntegrationRankingUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return false;
}

void FamilyFightIntegrationRankingUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void FamilyFightIntegrationRankingUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void FamilyFightIntegrationRankingUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void FamilyFightIntegrationRankingUI::scrollViewDidScroll( CCScrollView* view )
{

}

void FamilyFightIntegrationRankingUI::scrollViewDidZoom( CCScrollView* view )
{

}

void FamilyFightIntegrationRankingUI::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{
	selectCellId = cell->getIdx();
	//取出上次选中状态，更新本次选中状态
	if(table->cellAtIndex(lastSelectCellId))
	{
		if (table->cellAtIndex(lastSelectCellId)->getChildByTag(SelectImageTag))
		{
			table->cellAtIndex(lastSelectCellId)->getChildByTag(SelectImageTag)->setVisible(false);
		}
	}

	if (cell->getChildByTag(SelectImageTag))
	{
		cell->getChildByTag(SelectImageTag)->setVisible(true);
	}

	lastSelectCellId = cell->getIdx();
}

cocos2d::CCSize FamilyFightIntegrationRankingUI::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeMake(665,39);
}

cocos2d::extension::CCTableViewCell* FamilyFightIntegrationRankingUI::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCString *string = CCString::createWithFormat("%d", idx);
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell = new CCTableViewCell();
	cell->autorelease();

	CCScale9Sprite* pHighlightSpr = CCScale9Sprite::create(CCRectMake(5, 5, 1, 1) , "res_ui/mengban_green.png");
	pHighlightSpr->setPreferredSize(CCSize(664,37));
	pHighlightSpr->setAnchorPoint(CCPointZero);
	pHighlightSpr->setPosition(ccp(0,0));
	pHighlightSpr->setTag(SelectImageTag);
	cell->addChild(pHighlightSpr);
	pHighlightSpr->setVisible(false);

	CGuildFightRanking * rankingInfo = familyFightRankingList.at(idx);
	ccColor3B color_green = ccc3(47,93,13);

	// 底图
	CCScale9Sprite * sprite_bigFrame = CCScale9Sprite::create("res_ui/LV3_dikuang.png");
	sprite_bigFrame->setAnchorPoint(ccp(0, 0));
	sprite_bigFrame->setPosition(ccp(1, 0));
	sprite_bigFrame->setCapInsets(CCRectMake(9,14,1,1));
	sprite_bigFrame->setPreferredSize(CCSizeMake(664,37));
	cell->addChild(sprite_bigFrame);

	//排名
	char s_ranking[10];
	sprintf(s_ranking,"%d",rankingInfo->ranking());
	if (rankingInfo->ranking()>0 && rankingInfo->ranking() <= 3)
	{
		std::string icon_path = "res_ui/rank";
		icon_path.append(s_ranking);
		icon_path.append(".png");
		CCSprite * sp_ranking = CCSprite::create(icon_path.c_str());
		sp_ranking->setAnchorPoint(ccp(0.5f,0.5f));
		sp_ranking->setPosition(ccp(43,18));
		sp_ranking->setScale(0.9f);
		cell->addChild(sp_ranking);
	}
	else if(rankingInfo->ranking() <= 10)
	{
		CCLabelBMFont * l_ranking = CCLabelBMFont::create(s_ranking,"res_ui/font/ziti_3.fnt");
		l_ranking->setAnchorPoint(ccp(0.5f,0.5f));
		l_ranking->setPosition(ccp(43,18));
		cell->addChild(l_ranking);
	}
	else
	{
		CCLabelTTF * l_ranking = CCLabelTTF::create(s_ranking,APP_FONT_NAME,16);
		l_ranking->setAnchorPoint(ccp(0.5f,0.5f));
		l_ranking->setPosition(ccp(43,18));
		l_ranking->setColor(color_green);
		cell->addChild(l_ranking);
	}
	
	//家族名字
	CCLabelTTF * l_familyName = CCLabelTTF::create(rankingInfo->guild().guildname().c_str(), APP_FONT_NAME, 15);
	//CCLabelTTF * l_familyName = CCLabelTTF::create(StringDataManager::getString("PleaseDownloadNewClient"), APP_FONT_NAME, 15);
	l_familyName->setColor(color_green);
	l_familyName->setAnchorPoint(ccp(0.5f, 0.5f));
	l_familyName->setPosition(ccp(160, 18));
	cell->addChild(l_familyName);
	//家族等级
	char s_lv[10];
	sprintf(s_lv,"%d",rankingInfo->guild().guildlevel());
	//sprintf(s_lv,"%d",10);
	CCLabelTTF * l_familyLevel = CCLabelTTF::create(s_lv, APP_FONT_NAME, 15);
	l_familyLevel->setColor(color_green);
	l_familyLevel->setAnchorPoint(ccp(0.5f, 0.5f));
	l_familyLevel->setPosition(ccp(246, 18));
	cell->addChild(l_familyLevel);
	//家族组长名字
	CCLabelTTF * l_familyLeaderName = CCLabelTTF::create(rankingInfo->guild().guildleadername().c_str(), APP_FONT_NAME, 15);
	//CCLabelTTF * l_familyLeaderName = CCLabelTTF::create(StringDataManager::getString("CheckVersion"), APP_FONT_NAME, 15);
	l_familyLeaderName->setColor(color_green);
	l_familyLeaderName->setAnchorPoint(ccp(0.0f, 0.5f));
	l_familyLeaderName->setPosition(ccp(320, 18));
	cell->addChild(l_familyLeaderName);

	int countryId = rankingInfo->guild().guildleadercountry();
	if (countryId > 0 && countryId <6)
	{
		std::string countryIdName = "country_";
		char id[2];
		sprintf(id, "%d", countryId);
		countryIdName.append(id);
		std::string iconPathName = "res_ui/country_icon/";
		iconPathName.append(countryIdName);
		iconPathName.append(".png");
		CCSprite * countrySp_ = CCSprite::create(iconPathName.c_str());
		countrySp_->setAnchorPoint(ccp(1.0,0.5f));
		countrySp_->setPosition(ccp(l_familyLeaderName->getPositionX()-1,l_familyLeaderName->getPositionY()));
		countrySp_->setScale(0.7f);
		cell->addChild(countrySp_);
	}

	//vipInfo
	// vip开关控制
	bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
	if (vipEnabled)
	{
		if (rankingInfo->guild().guildleaderviplevel() > 0)
		{
			CCNode *pNode = MainScene::addVipInfoByLevelForNode(rankingInfo->guild().guildleaderviplevel());
			if (pNode)
			{
				cell->addChild(pNode);
				pNode->setPosition(ccp(l_familyLeaderName->getPosition().x+l_familyLeaderName->getContentSize().width+13,l_familyLeaderName->getPosition().y));
			}
		}
	}

	//家族积分
	char s_total[20];
	sprintf(s_total,"%d",rankingInfo->score());
	//sprintf(s_total,"%d",52014);
	CCLabelTTF * l_integration = CCLabelTTF::create(s_total, APP_FONT_NAME, 15);
	l_integration->setColor(color_green);
	l_integration->setAnchorPoint(ccp(0.5f, 0.5f));
	l_integration->setPosition(ccp(478, 18));
	cell->addChild(l_integration);
	//胜率
// 	std::string str_winningpPercentage;
// 	char s_winningPercentage[10];
// 	sprintf(s_winningPercentage,"%d",rankingInfo->winrate());
// 	//sprintf(s_winningPercentage,"%d",85);
// 	str_winningpPercentage.append(s_winningPercentage);
// 	str_winningpPercentage.append("%");
	CCLabelTTF * l_winningpPercentage = CCLabelTTF::create(rankingInfo->winrate().c_str(), APP_FONT_NAME, 15);
	l_winningpPercentage->setColor(color_green);
	l_winningpPercentage->setAnchorPoint(ccp(0.5f, 0.5f));
	l_winningpPercentage->setPosition(ccp(552, 18));
	cell->addChild(l_winningpPercentage);
	//总胜场数
	char s_winNum[10];
	sprintf(s_winNum,"%d",rankingInfo->winnum());
	//sprintf(s_winNum,"%d",32);
	CCLabelTTF * l_winNum = CCLabelTTF::create(s_winNum, APP_FONT_NAME, 15);
	l_winNum->setColor(color_green);
	l_winNum->setAnchorPoint(ccp(0.5f, 0.5f));
	l_winNum->setPosition(ccp(619, 18));
	cell->addChild(l_winNum);
	
	if (this->selectCellId == idx)
	{
		pHighlightSpr->setVisible(true);
	}

	if (getIsCanReq())
	{
		if(idx == familyFightRankingList.size()-4)
		{
			if (generalsListStatus == HaveNext || generalsListStatus == HaveBoth)
			{
				//req for next
				this->isReqNewly = false;
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1534,(void*)(this->s_mCurPage+1),(void*)everyPageNum);
				setIsCanReq(false);
			}
		}
	}

	return cell;
}

unsigned int FamilyFightIntegrationRankingUI::numberOfCellsInTableView( CCTableView *table )
{
	return familyFightRankingList.size();
	//return 3;
}

void FamilyFightIntegrationRankingUI::refreshUI()
{
	lastSelectCellId = 0;
	selectCellId = 0;
	m_tableView->reloadData();
}

void FamilyFightIntegrationRankingUI::CloseEvent( CCObject *pSender )
{
	this->closeAnim();
}
