#include "FamilyFightUI.h"
#include "GameView.h"
#include "../FamilyUI.h"
#include "../../../gamescene_state/MainScene.h"
#include "../../../utils/StaticDataManager.h"
#include "../../../ui/extensions/ShowSystemInfo.h"
#include "FamilyFightData.h"
#include "../../../gamescene_state/role/MyPlayer.h"
#include "../../../messageclient/element/CGuildBase.h"
#include "../../../messageclient/GameMessageProcessor.h"
#include "../../../messageclient/protobuf/ModelMessage.pb.h"
#include "FamilyFightRewardsUI.h"
#include "../../../messageclient/protobuf/GuildMessage.pb.h"
#include "familyFightHistoryNotesUI.h"
#include "FamilyFightIntegrationRankingUI.h"
#include "../../../ui/generals_ui/RecuriteActionItem.h"
#include "../../../legend_script/CCTutorialParticle.h"
#include "../../../messageclient/element/CGuildDailyReward.h"

#define ICON_PATH_BAOMING "res_ui/jiazuzhan/bangming.png"
#define ICON_PATH_QUXIAOBAOMING "res_ui/jiazuzhan/quxiaobaoming.png"
#define ICON_PATH_WEIBAOMING "res_ui/jiazuzhan/weibaoming.png"
#define ICON_PATH_YIBAOMING "res_ui/jiazuzhan/yibaoming.png"
#define ICON_PATH_ZHANQIANZHUNBEI "res_ui/jiazuzhan/zhanqianzhunbei.png"
#define ICON_PATH_JINRUZHANCHANG "res_ui/jiazuzhan/jinruzhanchang.png"
#define ICON_PATH_XIUZHENGQI "res_ui/jiazuzhan/xiuzhengqi.png"
#define ICON_PATH_ZHANDOUQUXIAO "res_ui/jiazuzhan/zhandouquxiao.png"
#define ICON_PATH_BENCHANGJIESHU "res_ui/jiazuzhan/benchangjieshu.png"


FamilyFightUI::FamilyFightUI()
{
}


FamilyFightUI::~FamilyFightUI()
{
}

FamilyFightUI* FamilyFightUI::create()
{
	FamilyFightUI * familyFightUI = new FamilyFightUI();
	if (familyFightUI && familyFightUI->init())
	{
		familyFightUI->autorelease();
		return familyFightUI;
	}
	CC_SAFE_DELETE(familyFightUI);
	return NULL;
}

bool FamilyFightUI::init()
{
	if (UIScene::init())
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

		m_base_layer = UILayer::create();
		this->addChild(m_base_layer);	

		initUI();

		this->schedule( schedule_selector(FamilyFightUI::update), 1.0f); 

		return true;
	}
	return false;
}

void FamilyFightUI::onEnter()
{
	UIScene::onEnter();
}

void FamilyFightUI::onExit()
{
	UIScene::onExit();
}

bool FamilyFightUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return false;
}

void FamilyFightUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void FamilyFightUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void FamilyFightUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void FamilyFightUI::initUI()
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

	if (!GameView::getInstance()->getMainUIScene())
		return;

	FamilyUI* familyUI =  (FamilyUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
	if (!familyUI)
		return;
 
 	UIPanel * s_pl_familyFight= (UIPanel*)UIHelper::seekWidgetByName(familyUI->mainPanel,"Panel_familyFight");

	//记录
	UIButton * btn_record = (UIButton*)UIHelper::seekWidgetByName(s_pl_familyFight,"Button_record");
	btn_record->setTouchEnable(true);
	btn_record->setPressedActionEnabled(true);
	btn_record->addReleaseEvent(this, coco_releaseselector(FamilyFightUI::RecordEvent));
	//积分榜
	UIButton * btn_scoreBoard = (UIButton*)UIHelper::seekWidgetByName(s_pl_familyFight,"Button_leagoe");
	btn_scoreBoard->setTouchEnable(true);
	btn_scoreBoard->setPressedActionEnabled(true);
	btn_scoreBoard->addReleaseEvent(this, coco_releaseselector(FamilyFightUI::ScoreBoardEvent));
	//报名
	btn_signUp = (UIButton*)UIHelper::seekWidgetByName(s_pl_familyFight,"Button_signUp");
	btn_signUp->setTouchEnable(true);
	btn_signUp->setPressedActionEnabled(true);
	btn_signUp->addReleaseEvent(this, coco_releaseselector(FamilyFightUI::SignUpEvent));
	//l_signUp = (UILabel*)UIHelper::seekWidgetByName(s_pl_familyFight,"Label_signUp");
	ImageView_btn_signUpState = (UIImageView*)UIHelper::seekWidgetByName(s_pl_familyFight,"ImageView_btn_curState");

	ImageView_signUp = (UIImageView*)UIHelper::seekWidgetByName(s_pl_familyFight,"ImageView_signUpFrame");
	ImageView_img_signUpState = (UIImageView*)UIHelper::seekWidgetByName(s_pl_familyFight,"ImageView_img_curState");

	l_entryLeftTime = (UILabel*)UIHelper::seekWidgetByName(s_pl_familyFight,"Label_entryLeftTimeValue");

	btn_signUp->setVisible(false);
	ImageView_signUp->setVisible(false);

	//规则
	UIButton * btn_rule = (UIButton*)UIHelper::seekWidgetByName(s_pl_familyFight,"Button_rule");
	btn_rule->setTouchEnable(true);
	btn_rule->setPressedActionEnabled(true);
	btn_rule->addReleaseEvent(this, coco_releaseselector(FamilyFightUI::RuleEvent));
	//领奖
	btn_getReward = (UIButton*)UIHelper::seekWidgetByName(s_pl_familyFight,"Button_accept");
	btn_getReward->setTouchEnable(true);
	btn_getReward->setPressedActionEnabled(true);
	btn_getReward->addReleaseEvent(this, coco_releaseselector(FamilyFightUI::GetRewardEvent));
	//
	//是否有奖励
	bool isExistReward = false;
	if (FamilyFightData::getInstance()->familyFightRewardList.size()>0)
	{
		CGuildDailyReward* guideReward = FamilyFightData::getInstance()->familyFightRewardList.at(FamilyFightData::getInstance()->familyFightRewardList.size()-1);
		if (FamilyFightData::getInstance()->getFamilyFightRanking()>0 && FamilyFightData::getInstance()->getFamilyFightRanking() <= guideReward->endrank())
		{
			isExistReward = true;
		}
	}
	if(isExistReward && (!FamilyFightData::getInstance()->getFamilyRewardStatus()))
	{
		AddParticleForBtnGetReward();
	}
	
	l_familyName = (UILabel*)UIHelper::seekWidgetByName(s_pl_familyFight,"Label_familyName");
	l_familyName->setText(GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().factionname().c_str());

	l_familyRanking = (UILabel*)UIHelper::seekWidgetByName(s_pl_familyFight,"Label_oldFamilyRankingValue");
	l_familyScore = (UILabel*)UIHelper::seekWidgetByName(s_pl_familyFight,"Label_integralValue");
	
	imageView_timeInfo = (UIImageView*)UIHelper::seekWidgetByName(s_pl_familyFight,"ImageView_timeInfo");
	l_time_cur = (UILabel*)UIHelper::seekWidgetByName(s_pl_familyFight,"Label_cur");
	l_time_curValue = (UILabel*)UIHelper::seekWidgetByName(s_pl_familyFight,"Label_curTimeValue");
	l_time_next = (UILabel*)UIHelper::seekWidgetByName(s_pl_familyFight,"Label_next");
	l_time_nextValue = (UILabel*)UIHelper::seekWidgetByName(s_pl_familyFight,"Label_nextTimeValue");
	l_canceledDes = (UILabel*)UIHelper::seekWidgetByName(s_pl_familyFight,"Label_canceledDes");
	l_canceledDes->setTextAreaSize(CCSizeMake(145,0));
	l_canceledDes->setTextHorizontalAlignment(kCCTextAlignmentLeft);
	l_canceledDes->setTextVerticalAlignment(kCCVerticalTextAlignmentCenter);
}

void FamilyFightUI::RecordEvent( CCObject *pSender )
{
 	familyFightHistoryNotesUI * historyNotesUI = (familyFightHistoryNotesUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyFightHistoryNotesUI);
 	if (!historyNotesUI)
 	{
 		CCSize winSize=CCDirector::sharedDirector()->getVisibleSize();
 		historyNotesUI = familyFightHistoryNotesUI::create();
 		historyNotesUI->ignoreAnchorPointForPosition(false);
 		historyNotesUI->setAnchorPoint(ccp(0.5f,0.5f));
 		historyNotesUI->setPosition(ccp(winSize.width/2,winSize.height/2));
 		historyNotesUI->setTag(kTagFamilyFightHistoryNotesUI);
 		GameView::getInstance()->getMainUIScene()->addChild(historyNotesUI);

 		historyNotesUI->isReqNewly = true;
 		GameMessageProcessor::sharedMsgProcessor()->sendReq(1533,(void*)0,(void*)historyNotesUI->everyPageNum);
 	}

}

void FamilyFightUI::ScoreBoardEvent( CCObject *pSender )
{
 	FamilyFightIntegrationRankingUI * rankingUI = (FamilyFightIntegrationRankingUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyFightIntegrationRankingUI);
 	if (!rankingUI)
 	{
 		CCSize winSize=CCDirector::sharedDirector()->getVisibleSize();
 		rankingUI = FamilyFightIntegrationRankingUI::create();
 		rankingUI->ignoreAnchorPointForPosition(false);
 		rankingUI->setAnchorPoint(ccp(0.5f,0.5f));
 		rankingUI->setPosition(ccp(winSize.width/2,winSize.height/2));
 		rankingUI->setTag(kTagFamilyFightIntegrationRankingUI);
 		GameView::getInstance()->getMainUIScene()->addChild(rankingUI);
 
 		rankingUI->isReqNewly = true;
 		GameMessageProcessor::sharedMsgProcessor()->sendReq(1534,(void*)0,(void*)rankingUI->everyPageNum);
 	}

}

void FamilyFightUI::SignUpEvent( CCObject *pSender )
{
	switch(FamilyFightData::getFamilyFightTimeClass())
	{
	case FamilyFightData::timeClass_none:
		{
		}
		break;
	case FamilyFightData::timeClass_signUp:
		{
			if(FamilyFightData::isRegisted)   //已报名
			{
				if (GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().factionposition() == CGuildBase::position_master  ||
					GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().factionposition() == CGuildBase::position_second_master)  //族长或长老
				{
					//取消报名
					GameMessageProcessor::sharedMsgProcessor()->sendReq(1532);
				}
				else
				{
					//已报名
				}
			}
			else                                           //未报名
			{
				if (GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().factionposition() == CGuildBase::position_master  ||
					GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().factionposition() == CGuildBase::position_second_master)  //族长或长老
				{
					//报名
					int needDevote = 0;
					std::map<int,std::string>::const_iterator cIter;
					cIter = VipValueConfig::s_vipValue.find(8);
					if (cIter == VipValueConfig::s_vipValue.end()) // 没找到就是指向END了  
					{

					}
					else
					{
						std::string value = VipValueConfig::s_vipValue[8];
						needDevote = atoi(value.c_str());
					}

					FamilyUI * familyUI = (FamilyUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
					if (familyUI)
					{
						if (familyUI->curFamilyContrbution>=needDevote)
						{
							GameMessageProcessor::sharedMsgProcessor()->sendReq(1531);
						}
						else
						{
							char des [500];
							const char *strings = StringDataManager::getString("FamilyFightUI_notEnoughHonor");
							sprintf(des,strings,needDevote,familyUI->curFamilyContrbution);
							GameView::getInstance()->showPopupWindow(des,2,this,NULL,NULL);	
						}
					}
				}
				else
				{
					//未报名
				}
			}
		}
		break;
	case FamilyFightData::timeClass_fight:
		{
			if(FamilyFightData::isRegisted)   //已报名
			{
				if (FamilyFightData::isCancel)  //匹配失败
				{
					//显示“战斗取消”
				}
				else
				{
					if(FamilyFightData::getFamilyFightFightClasss() == FamilyFightData::fightClass_none)
					{
						//战前准备
					}
					else if (FamilyFightData::getFamilyFightFightClasss() == FamilyFightData::fightClass_in_sequence)
					{
						//战前准备
					}
					else if (FamilyFightData::getFamilyFightFightClasss() == FamilyFightData::fightClass_startFight)
					{
						//进入战场
						GameMessageProcessor::sharedMsgProcessor()->sendReq(1540,(void *)FamilyFightData::getCurrentBattleId());
					}
					else if (FamilyFightData::getFamilyFightFightClasss() == FamilyFightData::fightClass_endFight)
					{
						//本场结束
					}
				}
			}
			else
			{
				//未报名
			}
		}
		break;
	}
}

void FamilyFightUI::RuleEvent( CCObject *pSender )
{
	//家族战规则描述
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	std::map<int,std::string>::const_iterator cIter;
	cIter = SystemInfoConfigData::s_systemInfoList.find(7);
	if (cIter == SystemInfoConfigData::s_systemInfoList.end()) // 没找到就是指向END了  
	{
		return ;
	}
	else
	{
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesDetailInfo) == NULL)
		{
			ShowSystemInfo * showSystemInfo = ShowSystemInfo::create(SystemInfoConfigData::s_systemInfoList[7].c_str(),1.5f);
			GameView::getInstance()->getMainUIScene()->addChild(showSystemInfo,0,kTagStrategiesDetailInfo);
			showSystemInfo->ignoreAnchorPointForPosition(false);
			showSystemInfo->setAnchorPoint(ccp(0.5f, 0.5f));
			showSystemInfo->setPosition(ccp(winSize.width/2, winSize.height/2));
		}
	}
}

void FamilyFightUI::GetRewardEvent( CCObject *pSender )
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyFightRewardUI) == NULL)
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		FamilyFightRewardsUI * familyFightRewardUI = FamilyFightRewardsUI::create();
		familyFightRewardUI->ignoreAnchorPointForPosition(false);
		familyFightRewardUI->setAnchorPoint(ccp(.5f,.5f));
		familyFightRewardUI->setPosition(ccp(winSize.width/2,winSize.height/2));
		familyFightRewardUI->setTag(kTagFamilyFightRewardUI);
		GameView::getInstance()->getMainUIScene()->addChild(familyFightRewardUI);
	}

}

void FamilyFightUI::RefreshBtnSignUpStatus()
{
	switch(FamilyFightData::getFamilyFightTimeClass())
	{
	case FamilyFightData::timeClass_none:
		{
			//显示“休整期”
			btn_signUp->setVisible(false);
			ImageView_signUp->setVisible(true);
			ImageView_img_signUpState->setTexture(ICON_PATH_XIUZHENGQI);
		}
		break;
	case FamilyFightData::timeClass_signUp:
		{
			if(FamilyFightData::isRegisted)   //已报名
			{
				if (GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().factionposition() == CGuildBase::position_master  ||
					GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().factionposition() == CGuildBase::position_second_master)  //族长或长老
				{
					//显示“取消报名”
					btn_signUp->setVisible(true);
					ImageView_btn_signUpState->setTexture(ICON_PATH_QUXIAOBAOMING);
					ImageView_signUp->setVisible(false);
				}
				else
				{
					//显示“已报名”
					btn_signUp->setVisible(false);
					ImageView_signUp->setVisible(true);
					ImageView_img_signUpState->setTexture(ICON_PATH_YIBAOMING);
				}
			}
			else                                           //未报名
			{
				if (GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().factionposition() == CGuildBase::position_master  ||
					GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().factionposition() == CGuildBase::position_second_master)  //族长或长老
				{
					//显示“报名”
					btn_signUp->setVisible(true);
					ImageView_btn_signUpState->setTexture(ICON_PATH_BAOMING);
					ImageView_signUp->setVisible(false);
				}
				else
				{
					//显示“未报名”
					btn_signUp->setVisible(false);
					ImageView_signUp->setVisible(true);
					ImageView_img_signUpState->setTexture(ICON_PATH_WEIBAOMING);
				}
			}
		}
		break;
	case FamilyFightData::timeClass_fight:
		{
			if(FamilyFightData::isRegisted)   //已报名
			{
				if (FamilyFightData::isCancel)  //匹配失败
				{
					//显示“战斗取消”
					btn_signUp->setVisible(false);
					ImageView_signUp->setVisible(true);
					ImageView_img_signUpState->setTexture(ICON_PATH_ZHANDOUQUXIAO);
				}
				else
				{
					if(FamilyFightData::getFamilyFightFightClasss() == FamilyFightData::fightClass_none)
					{
						//显示“战前准备”
						btn_signUp->setVisible(false);
						ImageView_signUp->setVisible(true);
						ImageView_img_signUpState->setTexture(ICON_PATH_ZHANQIANZHUNBEI);
					}
					else if (FamilyFightData::getFamilyFightFightClasss() == FamilyFightData::fightClass_in_sequence)
					{
						//显示“战前准备”
						btn_signUp->setVisible(false);
						ImageView_signUp->setVisible(true);
						ImageView_img_signUpState->setTexture(ICON_PATH_ZHANQIANZHUNBEI);
					}
					else if (FamilyFightData::getFamilyFightFightClasss() == FamilyFightData::fightClass_startFight)
					{
						//显示“进入战场”
						btn_signUp->setVisible(true);
						ImageView_btn_signUpState->setTexture(ICON_PATH_JINRUZHANCHANG);
						ImageView_signUp->setVisible(false);	
					}
					else if (FamilyFightData::getFamilyFightFightClasss() == FamilyFightData::fightClass_endFight)
					{
						//显示“本场结束”
						btn_signUp->setVisible(false);
						ImageView_signUp->setVisible(true);
						ImageView_img_signUpState->setTexture(ICON_PATH_BENCHANGJIESHU);
					}
				}
			}
			else
			{
				//显示“未报名”
				btn_signUp->setVisible(false);
				ImageView_signUp->setVisible(true);
				ImageView_img_signUpState->setTexture(ICON_PATH_WEIBAOMING);
			}
		}
		break;
	}

}

void FamilyFightUI::RefreshFamilyRankingAndScore()
{
	char s_myRanking [20];
	sprintf(s_myRanking,"%d",FamilyFightData::getFamilyFightRanking());
	l_familyRanking->setText(s_myRanking);

	char s_myScore [20];
	sprintf(s_myScore,"%d",FamilyFightData::getFamilyFightScore());
	l_familyScore->setText(s_myScore);
}

void FamilyFightUI::RefreshTimeInfo()
{
	if (FamilyFightData::getFamilyFightTimeClass() != FamilyFightData::timeClass_fight)
	{
		imageView_timeInfo->setVisible(false);
	}
	else
	{
		imageView_timeInfo->setVisible(true);

		if (FamilyFightData::isCancel)              //匹配失败
		{
			l_time_cur->setVisible(false);
			l_time_curValue->setVisible(false);
			l_time_next->setVisible(false);
			l_time_nextValue->setVisible(false);
			l_canceledDes->setVisible(true);
			l_canceledDes->setText(StringDataManager::getString("FamilyFightUI_cancel_today"));
		}
		else
		{
			l_time_cur->setVisible(true);
			l_time_curValue->setVisible(true);
			l_time_curValue->setText(FamilyFightData::getCurTime().c_str());
			l_time_next->setVisible(true);
			l_time_nextValue->setVisible(true);
			l_time_nextValue->setText(FamilyFightData::getNextTime().c_str());

			l_canceledDes->setVisible(false);
		}

	}

	l_time_curValue->setText(FamilyFightData::getCurTime().c_str());
	l_time_nextValue->setText(FamilyFightData::getNextTime().c_str());
	l_canceledDes->setText(StringDataManager::getString("FamilyFightUI_cancel_today"));
}

void FamilyFightUI::update( float dt )
{
	//本场进入剩余时间
	if (FamilyFightData::getFamilyFightTimeClass() != FamilyFightData::timeClass_fight)
	{
		l_entryLeftTime->setVisible(false);
	}
	else
	{
		if (FamilyFightData::getEntryLeftTime() > 0)
		{
			long long remainTime = FamilyFightData::getEntryLeftTime();
			remainTime -= 1000;
			FamilyFightData::setEntryLeftTime(remainTime);
		}

		if (FamilyFightData::getEntryLeftTime() <= 0)
		{
			l_entryLeftTime->setVisible(false);
		}
		else
		{
			l_entryLeftTime->setVisible(true);
			l_entryLeftTime->setText(RecuriteActionItem::timeFormatToString(FamilyFightData::getEntryLeftTime()/1000).c_str());
		}
	}
}

void FamilyFightUI::AddParticleForBtnGetReward()
{
	CCTutorialParticle * tutorialParticle = CCTutorialParticle::create("tuowei0.plist",38,57);
	tutorialParticle->setPosition(ccp(btn_getReward->getPosition().x,btn_getReward->getPosition().y-btn_getReward->getContentSize().height/2));
	tutorialParticle->setTag(CCTUTORIALPARTICLETAG);
	m_base_layer->addChild(tutorialParticle);
}

void FamilyFightUI::RemoveParticleForBtnGetReward()
{
	if (m_base_layer->getChildByTag(CCTUTORIALPARTICLETAG))
	{
		m_base_layer->getChildByTag(CCTUTORIALPARTICLETAG)->removeFromParent();
	}
}
