
#ifndef _FAMILYFIGHTUI_FAMILYFIGHTUI_H_
#define _FAMILYFIGHTUI_FAMILYFIGHTUI_H_

#include "../../extensions/UIScene.h"

class FamilyFightUI : public UIScene
{
public:
	FamilyFightUI();
	~FamilyFightUI();

	static FamilyFightUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	virtual void update(float dt);

	void RecordEvent(CCObject *pSender);
	void ScoreBoardEvent(CCObject *pSender);
	void SignUpEvent(CCObject *pSender);
	void RuleEvent(CCObject *pSender);
	void GetRewardEvent(CCObject *pSender);

	//刷新报名按钮的状态
	void RefreshBtnSignUpStatus();
	//刷新我的家族排名和积分
	void RefreshFamilyRankingAndScore();
	//刷新本场时间和下一场时间信息
	void RefreshTimeInfo();

	void AddParticleForBtnGetReward();
	void RemoveParticleForBtnGetReward();

private:
	UILayer * m_base_layer;									// 基调layer
	//按钮
	UIButton * btn_signUp;
	UIImageView * ImageView_btn_signUpState;
	UIButton * btn_getReward;
	//文本
	UIImageView * ImageView_signUp;
	UIImageView * ImageView_img_signUpState;
	//进入当前战场剩余时间
	UILabel * l_entryLeftTime;
	//右侧显示时间信息
	UIImageView * imageView_timeInfo;
	UILabel * l_time_cur;
	UILabel * l_time_curValue;
	UILabel * l_time_next;
	UILabel * l_time_nextValue;
	UILabel * l_canceledDes;

	UILabel * l_familyName;
	UILabel * l_familyRanking;
	UILabel * l_familyScore;

	void initUI();
};

#endif

