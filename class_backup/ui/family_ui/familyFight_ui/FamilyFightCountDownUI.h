

#ifndef _UI_FAMILYFIGHTUI_FAMILYFIGHTCOUNTDOWNUI_H_
#define _UI_FAMILYFIGHTUI_FAMILYFIGHTCOUNTDOWNUI_H_

#include "../../extensions/UIScene.h"

class FamilyFightCountDownUI : public UIScene
{
public:
	FamilyFightCountDownUI();
	~FamilyFightCountDownUI();

	static FamilyFightCountDownUI * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	virtual void update(float dt);

private:
	int m_nRemainTime;
	//UILabel *l_countDown;
	UIImageView * imageView_countDown;
};

#endif

