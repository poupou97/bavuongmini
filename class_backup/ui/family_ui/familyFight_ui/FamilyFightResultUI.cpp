#include "FamilyFightResultUI.h"
#include "../../../messageclient/GameMessageProcessor.h"
#include "../../../gamescene_state/MainScene.h"
#include "../../../gamescene_state/GameSceneState.h"
#include "GameView.h"
#include "../../../utils/GameUtils.h"
#include "../../../messageclient/protobuf/GuildMessage.pb.h"
#include "../../../utils/StaticDataManager.h"
#include "../../generals_ui/RecuriteActionItem.h"
#include "FamilyFightData.h"
#include "../../../ui/GameUIConstant.h"
#include "../../../gamescene_state/MainAnimationScene.h"
#include "../../../legend_engine/CCLegendAnimation.h"


FamilyFightResultUI::FamilyFightResultUI():
m_nRemainTime(30000),
countDown_startTime(0)
{
}


FamilyFightResultUI::~FamilyFightResultUI()
{
}

FamilyFightResultUI * FamilyFightResultUI::create(Push1543 fightResult)
{
	FamilyFightResultUI * familyFightResultUI = new FamilyFightResultUI();
	if (familyFightResultUI && familyFightResultUI->init(fightResult))
	{
		familyFightResultUI->autorelease();
		return familyFightResultUI;
	}
	CC_SAFE_DELETE(familyFightResultUI);
	return NULL;
}

bool FamilyFightResultUI::init(Push1543 fightResult)
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		countDown_startTime = GameUtils::millisecondNow();
		curResultInfo = fightResult;

		layer_anm = UILayer::create();
		addChild(layer_anm);

		MainScene * mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
		if (!mainscene)
			return false;

		CCActionInterval * action =(CCActionInterval *)CCSequence::create(
			CCDelayTime::create(END_BATTLE_BEFORE_SLOWMOTION_DURATION),
			CCCallFunc::create(this, callfunc_selector(FamilyFightResultUI::startSlowMotion)),
			CCCallFunc::create(this, callfunc_selector(FamilyFightResultUI::addLightAnimation)),
			CCDelayTime::create(END_BATTLE_SLOWMOTION_DURATION),
			CCCallFunc::create(this, callfunc_selector(FamilyFightResultUI::endSlowMotion)),
			//CCCallFunc::create(this, callfunc_selector(FamilyFightResultUI::showUIAnimation)),
			//CCDelayTime::create(1.0f),
			CCCallFunc::create(this,callfunc_selector(FamilyFightResultUI::createAnimation)),
			NULL);
		this->runAction(action);

		this->setContentSize(CCSizeMake(652,346));
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);

		return true;
	}
	return false;
}

void FamilyFightResultUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void FamilyFightResultUI::onExit()
{
	UIScene::onExit();
}

bool FamilyFightResultUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return true;
}

void FamilyFightResultUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void FamilyFightResultUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void FamilyFightResultUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void FamilyFightResultUI::QuitEvent( CCObject *pSender )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1541,(void *)FamilyFightData::getCurrentBattleId());
}

void FamilyFightResultUI::update( float dt )
{
	m_nRemainTime -= 1000;
	if (m_nRemainTime <= 0)
	{
		m_nRemainTime = 0;
		exitFamilyFight();
	}

	char s_remainTime[20];
	sprintf(s_remainTime,"%d",m_nRemainTime/1000);
	l_remainTime->setText(s_remainTime);
}

void FamilyFightResultUI::exitFamilyFight()
{
	//GameMessageProcessor::sharedMsgProcessor()->sendReq(3021);
	this->removeFromParent();
}

void FamilyFightResultUI::createUI()
{
	UIPanel * mainPanel = (UIPanel*)GUIReader::shareReader()->widgetFromJsonFile("res_ui/familyAccounts_1.json");
	mainPanel->setAnchorPoint(ccp(0.f,0.f));
	mainPanel->setPosition(ccp(0,0));
	m_pUiLayer->addWidget(mainPanel);

	UIButton * btn_exit= (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_exit");
	CCAssert(btn_exit != NULL, "should not be nil");
	btn_exit->setTouchEnable(true);
	btn_exit->setPressedActionEnabled(true);
	btn_exit->addReleaseEvent(this,coco_releaseselector(FamilyFightResultUI::QuitEvent));

// 	//结果
// 	UILabel *l_Result = (UILabel *)UIHelper::seekWidgetByName(mainPanel,"Label_ResultValue");
// 	switch(curResultInfo.result())
// 	{
// 	case 0:
// 		{
// 			l_Result->setText(StringDataManager::getString("FamilyFightUI_result_lose"));
// 		}
// 		break;
// 	case 1:
// 		{
// 			l_Result->setText(StringDataManager::getString("FamilyFightUI_result_tie"));
// 		}
// 		break;
// 	case 2:
// 		{
// 			l_Result->setText(StringDataManager::getString("FamilyFightUI_result_win"));
// 		}
// 		break;
// 	}
	//a名称
	UILabel *l_AName = (UILabel *)UIHelper::seekWidgetByName(mainPanel,"Label_aNameValue");
	l_AName->setText(curResultInfo.blueguildname().c_str());
	//aBoss HP
	UILabel *l_ABossHp = (UILabel *)UIHelper::seekWidgetByName(mainPanel,"Label_aBossHpValue");
	std::string str_abosshp;
	char s_abossHp[20];
	sprintf(s_abossHp,"%d",curResultInfo.bluebosshppercent());
	str_abosshp.append(s_abossHp);
	str_abosshp.append("%");
	l_ABossHp->setText(str_abosshp.c_str());
	//aBoss HP
	UIImageView *ImageView_aHp = (UIImageView *)UIHelper::seekWidgetByName(mainPanel,"ImageView_aHp");
	ImageView_aHp->setTextureRect(CCRectMake(0,0,ImageView_aHp->getContentSize().width*curResultInfo.bluebosshppercent()*1.0f/100.f,ImageView_aHp->getContentSize().height));
	//a积分
	UILabel *l_AIntegration = (UILabel *)UIHelper::seekWidgetByName(mainPanel,"Label_aIntegralValue");
	char s_aIntegration[20];
	sprintf(s_aIntegration,"%d",curResultInfo.bluefightpoint());
	l_AIntegration->setText(s_aIntegration);
	//左括号
	UILabel *l_AIntegrationBrace_left = (UILabel *)UIHelper::seekWidgetByName(mainPanel,"Label_aIntegralBrace_left");
	//变化量
	UILabel *l_AIntegrationChangeValue = (UILabel *)UIHelper::seekWidgetByName(mainPanel,"Label_aIntegralChangeValue");
	l_AIntegrationChangeValue->setText("+3");
	//右括号
	UILabel *l_AIntegrationBrace_right = (UILabel *)UIHelper::seekWidgetByName(mainPanel,"Label_aIntegralBrace_right");
	//箭头
	UIImageView * ImageView_aarrow = (UIImageView*)UIHelper::seekWidgetByName(mainPanel,"ImageView_arrowA");
	int allWidth = l_AIntegration->getContentSize().width+l_AIntegrationBrace_left->getContentSize().width+l_AIntegrationChangeValue->getContentSize().width+l_AIntegrationBrace_right->getContentSize().width+ImageView_aarrow->getContentSize().width;
	int centerWidth = allWidth/2;
	if (l_AIntegration->getContentSize().width >= centerWidth)
	{
		l_AIntegration->setPosition(ccp(206-(l_AIntegration->getContentSize().width - centerWidth),204));	
	}
	else
	{
		if (l_AIntegration->getContentSize().width+l_AIntegrationBrace_left->getContentSize().width >= centerWidth)
		{
			l_AIntegration->setPosition(ccp(206-l_AIntegrationBrace_left->getContentSize().width,204));
		}
		else if (l_AIntegration->getContentSize().width+l_AIntegrationBrace_left->getContentSize().width+l_AIntegrationChangeValue->getContentSize().width >= centerWidth)
		{
			l_AIntegration->setPosition(ccp(206-l_AIntegrationBrace_left->getContentSize().width-l_AIntegrationChangeValue->getContentSize().width,204));
		}
		else
		{
			l_AIntegration->setPosition(ccp(206,204));
		}
	}
	l_AIntegrationBrace_left->setPosition(ccp(l_AIntegration->getPosition().x,l_AIntegration->getPosition().y));
	l_AIntegrationChangeValue->setPosition(ccp(l_AIntegrationBrace_left->getPosition().x+l_AIntegrationBrace_left->getContentSize().width,l_AIntegrationBrace_left->getPosition().y));
	l_AIntegrationBrace_right->setPosition(ccp(l_AIntegrationChangeValue->getPosition().x+l_AIntegrationChangeValue->getContentSize().width,l_AIntegrationChangeValue->getPosition().y));
	ImageView_aarrow->setPosition(ccp(l_AIntegrationBrace_right->getPosition().x+l_AIntegrationBrace_right->getContentSize().width,l_AIntegrationBrace_right->getPosition().y));
	//a结果
	UILabel *l_AResult = (UILabel *)UIHelper::seekWidgetByName(mainPanel,"Label_aResultValue");
	switch(curResultInfo.blueresult())
	{
	case 0:
		{
			l_AResult->setText(StringDataManager::getString("FamilyFightUI_result_lose"));
			l_AResult->setColor(ccc3(255,0,6));
			l_AIntegrationChangeValue->setText("-1");
			l_AIntegrationChangeValue->setColor(ccc3(255,0,6));

			ImageView_aarrow->setTexture("res_ui/sarrow_down.png");
		}
		break;
	case 1:
		{
			l_AResult->setText(StringDataManager::getString("FamilyFightUI_result_tie"));
			l_AResult->setColor(ccc3(255,246,0));
			l_AIntegrationChangeValue->setText("+1");
			l_AIntegrationChangeValue->setColor(ccc3(255,246,0));

			ImageView_aarrow->setTexture("res_ui/sarrow_up.png");
		}
		break;
	case 2:
		{
			l_AResult->setText(StringDataManager::getString("FamilyFightUI_result_win"));
			l_AResult->setColor(ccc3(109,255,0));
			l_AIntegrationChangeValue->setText("+3");
			l_AIntegrationChangeValue->setColor(ccc3(109,255,0));

			ImageView_aarrow->setTexture("res_ui/sarrow_up.png");
		}
		break;
	}
	//b名称
	UILabel *l_BName = (UILabel *)UIHelper::seekWidgetByName(mainPanel,"Label_bNameValue");
	l_BName->setText(curResultInfo.redguildname().c_str());
	//bBoss HP
	UILabel *l_BBossHp = (UILabel *)UIHelper::seekWidgetByName(mainPanel,"Label_bBossHpValue");
	std::string str_bbosshp;
	char s_bbossHp[20];
	sprintf(s_bbossHp,"%d",curResultInfo.redbosshppercent());
	str_bbosshp.append(s_bbossHp);
	str_bbosshp.append("%");
	l_BBossHp->setText(str_bbosshp.c_str());
	//aBoss HP
	UIImageView *ImageView_bHp = (UIImageView *)UIHelper::seekWidgetByName(mainPanel,"ImageView_bHp");
	ImageView_bHp->setTextureRect(CCRectMake(0,0,ImageView_bHp->getContentSize().width*curResultInfo.redbosshppercent()*1.0f/100.f,ImageView_bHp->getContentSize().height));
	//b积分
	UILabel *l_BIntegration = (UILabel *)UIHelper::seekWidgetByName(mainPanel,"Label_bIntegralValue");
	char s_bIntegration[20];
	sprintf(s_bIntegration,"%d",curResultInfo.redfightpoint());
	l_BIntegration->setText(s_bIntegration);
	//左括号
	UILabel *l_BIntegrationBrace_left = (UILabel *)UIHelper::seekWidgetByName(mainPanel,"Label_bIntegralBrace_left");
	//变化量
	UILabel *l_BIntegrationChangeValue = (UILabel *)UIHelper::seekWidgetByName(mainPanel,"Label_bIntegralChangeValue");
	l_BIntegrationChangeValue->setText("+3");
	//右括号
	UILabel *l_BIntegrationBrace_right = (UILabel *)UIHelper::seekWidgetByName(mainPanel,"Label_bIntegralBrace_right");
	//箭头
	UIImageView * ImageView_barrow = (UIImageView*)UIHelper::seekWidgetByName(mainPanel,"ImageView_arrowB");
	allWidth = l_BIntegration->getContentSize().width+l_BIntegrationBrace_left->getContentSize().width+l_BIntegrationChangeValue->getContentSize().width+l_BIntegrationBrace_right->getContentSize().width+ImageView_barrow->getContentSize().width;
	centerWidth = allWidth/2;
	if (l_BIntegration->getContentSize().width >= centerWidth)
	{
		l_BIntegration->setPosition(ccp(512-(l_BIntegration->getContentSize().width - centerWidth),204));	
	}
	else
	{
		if (l_BIntegration->getContentSize().width+l_BIntegrationBrace_left->getContentSize().width >= centerWidth)
		{
			l_BIntegration->setPosition(ccp(512-l_BIntegrationBrace_left->getContentSize().width,204));
		}
		else if (l_BIntegration->getContentSize().width+l_BIntegrationBrace_left->getContentSize().width+l_BIntegrationChangeValue->getContentSize().width >= centerWidth)
		{
			l_BIntegration->setPosition(ccp(512-l_BIntegrationBrace_left->getContentSize().width-l_BIntegrationChangeValue->getContentSize().width,204));
		}
		else
		{
			l_BIntegration->setPosition(ccp(512,204));
		}
	}
	l_BIntegrationBrace_left->setPosition(ccp(l_BIntegration->getPosition().x,l_BIntegration->getPosition().y));
	l_BIntegrationChangeValue->setPosition(ccp(l_BIntegrationBrace_left->getPosition().x+l_BIntegrationBrace_left->getContentSize().width,l_BIntegrationBrace_left->getPosition().y));
	l_BIntegrationBrace_right->setPosition(ccp(l_BIntegrationChangeValue->getPosition().x+l_BIntegrationChangeValue->getContentSize().width,l_BIntegrationChangeValue->getPosition().y));
	ImageView_barrow->setPosition(ccp(l_BIntegrationBrace_right->getPosition().x+l_BIntegrationBrace_right->getContentSize().width,l_BIntegrationBrace_right->getPosition().y));

	//b结果
	UILabel *l_BResult = (UILabel *)UIHelper::seekWidgetByName(mainPanel,"Label_bResultValue");
	switch(curResultInfo.redresult())
	{
	case 0:
		{
			l_BResult->setText(StringDataManager::getString("FamilyFightUI_result_lose"));
			l_BResult->setColor(ccc3(255,0,6));
			l_BIntegrationChangeValue->setText("-1");
			l_BIntegrationChangeValue->setColor(ccc3(255,0,6));

			ImageView_barrow->setTexture("res_ui/sarrow_down.png");
		}
		break;
	case 1:
		{
			l_BResult->setText(StringDataManager::getString("FamilyFightUI_result_tie"));
			l_BResult->setColor(ccc3(255,246,0));
			l_BIntegrationChangeValue->setText("+1");
			l_BIntegrationChangeValue->setColor(ccc3(255,246,0));

			ImageView_barrow->setTexture("res_ui/sarrow_up.png");
		}
		break;
	case 2:
		{
			l_BResult->setText(StringDataManager::getString("FamilyFightUI_result_win"));
			l_BResult->setColor(ccc3(109,255,0));
			l_BIntegrationChangeValue->setText("+3");
			l_BIntegrationChangeValue->setColor(ccc3(109,255,0));

			ImageView_barrow->setTexture("res_ui/sarrow_up.png");
		}
		break;
	}

	//战场用时
	UILabel *l_costTime = (UILabel *)UIHelper::seekWidgetByName(mainPanel,"Label_flutterTimeValue");
	l_costTime->setText(RecuriteActionItem::timeFormatToString(curResultInfo.fighttime()/1000).c_str());
	//经验奖励
	UILabel *l_rewardExp = (UILabel *)UIHelper::seekWidgetByName(mainPanel,"Label_awardEXPValue");
	char s_expValue[20];
	sprintf(s_expValue,"%d",curResultInfo.rewardexp());
	l_rewardExp->setText(s_expValue);
	//个人贡献
	UILabel *l_rewardContribution = (UILabel *)UIHelper::seekWidgetByName(mainPanel,"Label_awardContributionValue");
	char s_contribution[20];
	sprintf(s_contribution,"%d",curResultInfo.rewardhonor());
	l_rewardContribution->setText(s_contribution);
	//倒计时
	l_remainTime = (UILabel *)UIHelper::seekWidgetByName(mainPanel,"Label_restTimeValue");
	m_nRemainTime -= (GameUtils::millisecondNow() - countDown_startTime);
	m_nRemainTime -= 1000;

	char s_remainTime[20];
	sprintf(s_remainTime,"%d",m_nRemainTime/1000);
	l_remainTime->setText(s_remainTime);

	this->schedule(schedule_selector(FamilyFightResultUI::update),1.0f);
}

void FamilyFightResultUI::showUIAnimation()
{
//	MainScene * mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
//	if (mainscene != NULL)
//	{
// 		if (m_bIsWin)
// 		{
// 			mainscene->addInterfaceAnm("tzcg/tzcg.anm");
// 		}
// 		else
// 		{
// 			mainscene->addInterfaceAnm("tzsb/tzsb.anm");
// 		}
//	}
//	
//	
//	
}
void FamilyFightResultUI::startSlowMotion()
{
	CCDirector::sharedDirector()->getScheduler()->setTimeScale(0.2f);
}
void FamilyFightResultUI::endSlowMotion()
{
	CCDirector::sharedDirector()->getScheduler()->setTimeScale(1.0f);
}

void FamilyFightResultUI::addLightAnimation()
{
	CCSize size = CCDirector::sharedDirector()->getVisibleSize();
	std::string animFileName = "animation/texiao/renwutexiao/SHANGUANG/shanguang.anm";
	CCLegendAnimation *la_light = CCLegendAnimation::create(animFileName);
	if (la_light)
	{
		la_light->setPlayLoop(false);
		la_light->setReleaseWhenStop(true);
		la_light->setPlaySpeed(5.0f);
		la_light->setScale(0.85f);
		la_light->setPosition(ccp(size.width/2,size.height/2));
		GameView::getInstance()->getMainAnimationScene()->addChild(la_light);
	}
}

void FamilyFightResultUI::createAnimation()
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	UIImageView *mengban = UIImageView::create();
	mengban->setTexture("res_ui/zhezhao80.png");
	mengban->setScale9Enable(true);
	mengban->setScale9Size(winSize);
	mengban->setAnchorPoint(ccp(.5f,.5f));
	m_pUiLayer->addWidget(mengban);
	mengban->setPosition(ccp(652/2,346/2+20));

	//根据动画名称创建动画精灵
	CCArmature *armature ;
	if (curResultInfo.result() == 0)
	{
		//从导出文件异步加载动画
		CCArmatureDataManager::sharedArmatureDataManager()->addArmatureFileInfo("res_ui/uiflash/lostLogo/lostLogo0.png","res_ui/uiflash/lostLogo/lostLogo0.plist","res_ui/uiflash/lostLogo/lostLogo.ExportJson");
		armature = CCArmature::create("lostLogo");
		//播放指定动作
		armature->getAnimation()->playByIndex(0,-1,-1,ANIMATION_NO_LOOP);
	}
	else if (curResultInfo.result() == 1)
	{
		//从导出文件异步加载动画
		CCArmatureDataManager::sharedArmatureDataManager()->addArmatureFileInfo("res_ui/uiflash/drawLogo/drawLogo0.png","res_ui/uiflash/drawLogo/drawLogo0.plist","res_ui/uiflash/drawLogo/drawLogo.ExportJson");
		armature = CCArmature::create("drawLogo");
		//播放指定动作
		armature->getAnimation()->playByIndex(0,-1,-1,ANIMATION_NO_LOOP);
	}
	else
	{	
		//从导出文件异步加载动画
		CCArmatureDataManager::sharedArmatureDataManager()->addArmatureFileInfo("res_ui/uiflash/winLogo/winLogo0.png","res_ui/uiflash/winLogo/winLogo0.plist","res_ui/uiflash/winLogo/winLogo.ExportJson");
		armature = CCArmature::create("winLogo");
		armature->getAnimation()->play("Animation_begin");
		armature->getAnimation()->setMovementEventCallFunc(this,movementEvent_selector(FamilyFightResultUI::ArmatureFinishCallback));  
	}
	//修改属性
	armature->setScale(1.0f);
	//设置动画精灵位置
	armature->setPosition(652/2,346/2+35);
	//添加到当前页面
	this->addChild(armature);

	CCSequence * sequence = CCSequence::create(
		CCDelayTime::create(2.3f),
		CCCallFunc::create(this,callfunc_selector(FamilyFightResultUI::createUI)),
		NULL
		);
	this->runAction(sequence);


	/*******************************************************/
	//胜利背后的佛光
// 	UIImageView * imageView_light = UIImageView::create();
// 	imageView_light->setTexture("res_ui/lightlight.png");
// 	imageView_light->setAnchorPoint(ccp(0.5f,0.5f));
// 	imageView_light->setPosition(ccp(652/2,346/2));
// 	layer_anm->addWidget(imageView_light);
// 	imageView_light->setVisible(false);
	//结果
// 	UIImageView * imageView_result = UIImageView::create();
// 	if (curResultInfo.result() == 0)
// 	{
// 		imageView_result->setTexture("res_ui/jiazuzhan/lost.png");
// 	}
// 	else if (curResultInfo.result() == 1)
// 	{
// 		imageView_result->setTexture("res_ui/jiazuzhan/draw.png");
// 	}
// 	else
// 	{
// 		imageView_result->setTexture("res_ui/jiazuzhan/win.png");
// 	}
// 	imageView_result->setAnchorPoint(ccp(0.5f,0.5f));
// 	imageView_result->setPosition(ccp(652/2,346/2));
// 	imageView_result->setScale(RESULT_UI_RESULTFLAG_BEGINSCALE);
// 	layer_anm->addWidget(imageView_result);

// 	CCSequence * sequence = CCSequence::create(CCScaleTo::create(RESULT_UI_RESULTFLAG_SCALECHANGE_TIME,1.0f,1.0f),
// 		CCDelayTime::create(RESULT_UI_RESULTFLAG_DELAY_TIME),
// 		CCMoveTo::create(RESULT_UI_RESULTFLAG_MOVE_TIME,ccp(652/2,346)),
// 		CCDelayTime::create(0.05f),
// 		CCCallFunc::create(this,callfunc_selector(FamilyFightResultUI::createUI)),
// 		NULL);
// 	imageView_result->runAction(sequence);

// 	if (curResultInfo.result() == 2)
// 	{
// 		CCRotateBy * rotateAnm = CCRotateBy::create(4.0f,360);
// 		CCRepeatForever * repeatAnm = CCRepeatForever::create(CCSequence::create(rotateAnm,NULL));
// 		CCSequence * sequence_light = CCSequence::create(
// 			CCDelayTime::create(RESULT_UI_RESULTFLAG_SCALECHANGE_TIME),
// 			CCShow::create(),
// 			CCDelayTime::create(RESULT_UI_RESULTFLAG_DELAY_TIME),
// 			CCMoveTo::create(RESULT_UI_RESULTFLAG_MOVE_TIME,ccp(652/2,346)),
// 			NULL);
// 		imageView_light->runAction(sequence_light);
// 		imageView_light->runAction(repeatAnm);
// 	}
}

void FamilyFightResultUI::ArmatureFinishCallback( cocos2d::extension::CCArmature *armature, cocos2d::extension::MovementEventType movementType, const char *movementID )
{
	std::string id = movementID;
	if (id.compare("Animation_begin") == 0)
	{
		armature->stopAllActions();
		armature->getAnimation()->play("Animation1_end");
	}
}

