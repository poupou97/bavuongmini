#ifndef FAMILY_FAMILYMANAGE_OPERATORLEADER_H 
#define FAMILY_FAMILYMANAGE_OPERATORLEADER_H

#include "../extensions/UIScene.h"

class FamilyOperatorOfLeader:public UIScene
{
public:
	FamilyOperatorOfLeader(void);
	~FamilyOperatorOfLeader(void);

	static FamilyOperatorOfLeader *create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
// 	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
// 	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
// 	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

	//���� ��� �ټ� ��ɢ
	void callBackConvene(CCObject * obj);
	void callBackCheck(CCObject * obj);
	void callBackCallTogether(CCObject * obj);
	void callBackDismiss(CCObject * obj);
	void callBackDismissSure(CCObject * obj);
	void callBackClose(CCObject * obj);

	CCSize winsize;
	UIButton * button_check;
};

#include "../extensions/RichTextInput.h"
class FamilyChangeNotice:public UIScene
{
public:
	FamilyChangeNotice();
	~FamilyChangeNotice();

	static FamilyChangeNotice *create(int type);
	bool init(int type);

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);

	void callBackSure(CCObject * obj);
	void callBackCancel(CCObject * obj);
public:
	RichTextInputBox * textFamilyNotice;
};

#endif;