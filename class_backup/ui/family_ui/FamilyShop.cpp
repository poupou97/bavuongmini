#include "FamilyShop.h"
#include "FamilyUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "FamilyShopCount.h"
#include "../backpackscene/GoodsItemInfoBase.h"
#include "../../utils/StaticDataManager.h"


FamilyShop::FamilyShop(void)
{
}


FamilyShop::~FamilyShop(void)
{
}

FamilyShop * FamilyShop::create()
{
	FamilyShop * shop =new FamilyShop();
	if (shop && shop->init())
	{
		shop->autorelease();
		return shop;
	}
	CC_SAFE_DELETE(shop);
	return NULL;
}

bool FamilyShop::init()
{
	if (UIScene::init())
	{
		winsize= CCDirector::sharedDirector()->getVisibleSize();
		familyui = (FamilyUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winsize);
		mengban->setAnchorPoint(ccp(0,0));
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);

		UIPanel *mainPanel=(UIPanel *)GUIReader::shareReader()->widgetFromJsonFile("res_ui/family_shop_1.json");
		mainPanel->setAnchorPoint(ccp(0.5f,0.5f));
		mainPanel->setPosition(ccp(winsize.width/2,winsize.height/2));
		m_pUiLayer->addWidget(mainPanel);

		labelContrbution = (UILabel *)UIHelper::seekWidgetByName(mainPanel,"Label_activityValue");
		char contrbutionRem[20];
		sprintf(contrbutionRem,"%d",familyui->remPersonContrbution);
		labelContrbution->setText(contrbutionRem);

		loadLayer= UILayer::create();
		loadLayer->ignoreAnchorPointForPosition(false);
		loadLayer->setAnchorPoint(ccp(0.5f,0.5f));
		loadLayer->setPosition(ccp(winsize.width/2,winsize.height/2));
		loadLayer->setContentSize(CCSizeMake(800,480));
		this->addChild(loadLayer);
		
		UIButton * btnClose = (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_close");
		btnClose->setTouchEnable(true);
		btnClose->setPressedActionEnabled(true);
		btnClose->addReleaseEvent(this,coco_releaseselector(FamilyShop::callBackExit));
		
		tableviewMember = CCTableView::create(this, CCSizeMake(504,260));
		tableviewMember->setDirection(kCCScrollViewDirectionVertical);
		tableviewMember->setAnchorPoint(ccp(0,0));
		tableviewMember->setPosition(ccp(153,109));
		tableviewMember->setDelegate(this);
		tableviewMember->setVerticalFillOrder(kCCTableViewFillTopDown);
		loadLayer->addChild(tableviewMember);
		
// 		UILayer * layer_= UILayer::create();
// 		layer_->ignoreAnchorPointForPosition(false);
// 		layer_->setAnchorPoint(ccp(0.5f,0.5f));
// 		layer_->setPosition(ccp(winsize.width/2,winsize.height/2));
// 		layer_->setContentSize(CCSizeMake(800,480));
// 		this->addChild(layer_,10);
// 
// 		UIImageView * imageBg = UIImageView::create();
// 		imageBg->setScale9Enable(true);
// 		imageBg->setScale9Size(CCSizeMake(515,277));
// 		imageBg->setCapInsets(CCRect(32,32,1,1));
// 		imageBg->setTexture("res_ui/LV5_dikuang_miaobian1.png");
// 		imageBg->setAnchorPoint(ccp(0.5f,0.5f));
// 		imageBg->setPosition(ccp(400,240));
// 		layer_->addWidget(imageBg);

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(winsize);
		return true;
	}
	return false;
}

void FamilyShop::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void FamilyShop::onExit()
{
	UIScene::onExit();
}

void FamilyShop::callBackExit( CCObject * obj )
{
	this->closeAnim();
}

void FamilyShop::scrollViewDidScroll( cocos2d::extension::CCScrollView * view )
{

}

void FamilyShop::scrollViewDidZoom( cocos2d::extension::CCScrollView* view )
{

}

void FamilyShop::tableCellTouched( cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell )
{

}

cocos2d::CCSize FamilyShop::tableCellSizeForIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	return CCSizeMake(200,65);
}

cocos2d::extension::CCTableViewCell* FamilyShop::tableCellAtIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{

	CCString *string = CCString::createWithFormat("%d", idx);
	CCTableViewCell *cell =table->dequeueCell();   // this method must be called

	cell=new CCTableViewCell();
	cell->autorelease();

	int commoditySize = familyui->guildCommodityVector.size();
	if (idx == commoditySize/2)
	{
		if (commoditySize%2 == 0)
		{
			goodsItemCell * goodsItemL =goodsItemCell::create(2*idx);
			goodsItemL->setAnchorPoint(ccp(0.5f,0.5f));
			goodsItemL->setPosition(ccp(0,0));
			cell->addChild(goodsItemL);

			goodsItemCell * goodsItemR =goodsItemCell::create(2*idx+1);
			goodsItemR->setAnchorPoint(ccp(0.5f,0.5f));
			goodsItemR->setPosition(ccp(250,0));
			cell->addChild(goodsItemR);
		}else
		{
			goodsItemCell * goodsItemL =goodsItemCell::create(2*idx);
			goodsItemL->setAnchorPoint(ccp(0.5f,0.5f));
			goodsItemL->setPosition(ccp(0,0));
			cell->addChild(goodsItemL);
		}
	}
	else
	{
		goodsItemCell * goodsItemL =goodsItemCell::create(2*idx);
		goodsItemL->setAnchorPoint(ccp(0.5f,0.5f));
		goodsItemL->setPosition(ccp(0,0));
		cell->addChild(goodsItemL);

		goodsItemCell * goodsItemR =goodsItemCell::create(2*idx+1);
		goodsItemR->setAnchorPoint(ccp(0.5f,0.5f));
		goodsItemR->setPosition(ccp(250,0));
		cell->addChild(goodsItemR);
	}

	return cell;
}

unsigned int FamilyShop::numberOfCellsInTableView( cocos2d::extension::CCTableView *table )
{
	int vectorSize = familyui->guildCommodityVector.size();
	if (vectorSize%2 == 0)
	{
		return vectorSize/2;
	}else
	{
		return vectorSize/2 + 1;
	}
}

void FamilyShop::ccTouchMoved( CCTouch *pTouch, CCEvent *pEvent )
{

}

void FamilyShop::ccTouchEnded( CCTouch *pTouch, CCEvent *pEvent )
{

}

void FamilyShop::ccTouchCancelled( CCTouch *pTouch, CCEvent *pEvent )
{

}
//////////////////////////////////

goodsItemCell::goodsItemCell( void )
{

}

goodsItemCell::~goodsItemCell( void )
{

}

goodsItemCell * goodsItemCell::create( int idx )
{
	goodsItemCell * goods_ =new goodsItemCell();
	if (goods_ && goods_->init(idx))
	{
		goods_->autorelease();
		return goods_;
	}
	CC_SAFE_DELETE(goods_);
	return NULL;
}

bool goodsItemCell::init( int idx )
{
	if (UIScene::init())
	{
		winsize= CCDirector::sharedDirector()->getVisibleSize();
		FamilyUI * familyui = (FamilyUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
		UIImageView * imageBackground = UIImageView::create();
		imageBackground->setTexture("res_ui/kuang0_new.png");
		imageBackground->setScale9Enable(true);
		imageBackground->setScale9Size(CCSizeMake(240,60));
		imageBackground->setAnchorPoint(ccp(0,0));
		imageBackground->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(imageBackground);
		
		std::string bgSp = getColorSpByQuality(familyui->guildCommodityVector.at(idx)->goods().quality());
		UIButton * goodsSpbtn= UIButton::create();
		goodsSpbtn->setTouchEnable(true);
		//goodsSpbtn->setPressedActionEnabled(true);
		goodsSpbtn->addReleaseEvent(this,coco_releaseselector(goodsItemCell::showGoodsInfo));
		goodsSpbtn->setAnchorPoint(ccp(0.5f,0.5f));
		goodsSpbtn->setScale9Enable(true);
		goodsSpbtn->setScale9Size(CCSizeMake(50,50));
		goodsSpbtn->setTag(idx);
		goodsSpbtn->setCapInsets(CCRect(15,30,1,1));
		goodsSpbtn->setPosition(ccp(goodsSpbtn->getSize().width/2+10,imageBackground->getSize().height/2));
		goodsSpbtn->setTextures(bgSp.c_str(),bgSp.c_str(),"");
		imageBackground->addChild(goodsSpbtn);

		std::string iconPath = "res_ui/props_icon/";
		iconPath.append(familyui->guildCommodityVector.at(idx)->goods().icon());
		iconPath.append(".png");

		UIImageView * imageGoods = UIImageView::create();
		imageGoods->setTexture(iconPath.c_str());
		imageGoods->setAnchorPoint(ccp(0.5f,0.5f));
		imageGoods->setPosition(ccp(0,0));
		goodsSpbtn->addChild(imageGoods);

		UILabel * labelName = UILabel::create();
		labelName->setAnchorPoint(ccp(0,0));
		labelName->setPosition(ccp(goodsSpbtn->getPosition().x+goodsSpbtn->getSize().width/2,goodsSpbtn->getPosition().y+5));
		labelName->setText(familyui->guildCommodityVector.at(idx)->goods().name().c_str());
		labelName->setFontSize(18);
		labelName->setColor(GameView::getInstance()->getGoodsColorByQuality(familyui->guildCommodityVector.at(idx)->goods().quality()));
		imageBackground->addChild(labelName);

		int goodsPrice_ = familyui->guildCommodityVector.at(idx)->guildhonor();
		char goodsPriceStr[5];
		sprintf(goodsPriceStr,"%d",goodsPrice_);
		UILabel * labelPrice = UILabel::create();
		labelPrice->setAnchorPoint(ccp(0,0));
		labelPrice->setPosition(ccp(goodsSpbtn->getPosition().x+goodsSpbtn->getSize().width,goodsSpbtn->getPosition().y-20));
		labelPrice->setText(goodsPriceStr);
		labelPrice->setFontSize(18);
		imageBackground->addChild(labelPrice);
		
		UIButton * buyButton= UIButton::create();
		buyButton->setTouchEnable(true);
		buyButton->setPressedActionEnabled(true);
		buyButton->addReleaseEvent(this,coco_releaseselector(goodsItemCell::callBackBuy));
		buyButton->setAnchorPoint(ccp(0.5f,0.5f));
		buyButton->setScale9Enable(true);
		buyButton->setScale9Size(CCSizeMake(80,40));
		buyButton->setCapInsets(CCRect(15,30,1,1));
		buyButton->setTag(idx);
		buyButton->setPosition(ccp(imageBackground->getSize().width -buyButton->getSize().width/2-10 ,imageBackground->getSize().height/2));
		buyButton->setTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
		imageBackground->addChild(buyButton);
		
		const char *stringBuy = StringDataManager::getString("goods_auction_buy");
		UILabel * buyLabel = UILabel::create();
		buyLabel->setAnchorPoint(ccp(0.5f,0.5f));
		buyLabel->setPosition(ccp(0,0));
		buyLabel->setFontSize(18);
		buyLabel->setText(stringBuy);
		buyButton->addChild(buyLabel);

		return true;
	}
	return false;
}

void goodsItemCell::onEnter()
{
	UIScene::onEnter();
}

void goodsItemCell::onExit()
{
	UIScene::onExit();
}

std::string goodsItemCell::getColorSpByQuality( int quality )
{
	std::string iconFramePath;
	switch(quality)
	{
	case 1 :
		{
			iconFramePath = "res_ui/smdi_white.png";
		}
		break;
	case 2 :
		{
			iconFramePath = "res_ui/smdi_green.png";
		}
		break;
	case 3 :
		{
			iconFramePath = "res_ui/smdi_bule.png";
		}
		break;
	case 4 :
		{
			iconFramePath = "res_ui/smdi_purple.png";
		}
		break;
	case 5 :
		{
			iconFramePath = "res_ui/smdi_orange.png";
		}
		break;
	}

	return iconFramePath;
}

void goodsItemCell::callBackBuy( CCObject * obj )
{
	UIButton * btn = (UIButton *)obj;
	int selectId = btn->getTag();
	FamilyUI * familyui = (FamilyUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);

	CGuildCommodity * commodity_ =new CGuildCommodity();
	commodity_->CopyFrom(*familyui->guildCommodityVector.at(selectId));

	if (GameView::getInstance()->getMainUIScene()->getChildByTag(4562) == NULL)
	{
		FamilyShopCount * count_ =FamilyShopCount::create(commodity_);
		count_->ignoreAnchorPointForPosition(false);
		count_->setAnchorPoint(ccp(0.5f,0.5f));
		count_->setPosition(ccp(winsize.width/2,winsize.height/2));
		count_->setTag(4562);
		GameView::getInstance()->getMainUIScene()->addChild(count_);
	}
	
	delete commodity_;
}

void goodsItemCell::showGoodsInfo(CCObject *obj)
{
	UIButton * btn = (UIButton *)obj;
	int selectId = btn->getTag();
	FamilyUI * familyui = (FamilyUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
	GoodsInfo * goods_ =new GoodsInfo();
	goods_->CopyFrom(familyui->guildCommodityVector.at(selectId)->goods());

	GoodsItemInfoBase * goodsInfo = GoodsItemInfoBase::create(goods_,GameView::getInstance()->EquipListItem,0);
	goodsInfo->ignoreAnchorPointForPosition(false);
	goodsInfo->setAnchorPoint(ccp(0.5f,0.5f));
	GameView::getInstance()->getMainUIScene()->addChild(goodsInfo);
	delete goods_;
}


