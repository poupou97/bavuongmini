#include "ManageFamily.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../extensions/CCMoveableMenu.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/CGuildMemberBase.h"
#include "GameView.h"
#include "FamilyUI.h"
#include "../../gamescene_state/GameSceneState.h"
#include "AppMacros.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../gamescene_state/MainScene.h"



ManageFamily::ManageFamily(void)
{
}


ManageFamily::~ManageFamily(void)
{
}

ManageFamily * ManageFamily::create()
{
	ManageFamily * family_=new ManageFamily();
	if (family_ && family_->init())
	{
		family_->autorelease();
		return family_;
	}
	CC_SAFE_DELETE(family_);
	return NULL;
}

bool ManageFamily::init()
{
	if (UIScene::init())
	{
		CCSize winsize= CCDirector::sharedDirector()->getVisibleSize();
		familyui_ =(FamilyUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winsize);
		mengban->setAnchorPoint(ccp(0,0));
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);

		if(LoadSceneLayer::familyManagePanel->getWidgetParent() != NULL)
		{
			LoadSceneLayer::familyManagePanel->removeFromParentAndCleanup(false);
		}

		UIPanel *familypanel=LoadSceneLayer::familyManagePanel;
		familypanel->setAnchorPoint(ccp(0.5f,0.5f));
		familypanel->setPosition(ccp(winsize.width/2,winsize.height/2));
		m_pUiLayer->addWidget(familypanel);

		UILayer *layer= UILayer::create();
		layer->ignoreAnchorPointForPosition(false);
		layer->setAnchorPoint(ccp(0.5f,0.5f));
		layer->setPosition(ccp(winsize.width/2,winsize.height/2));
		layer->setContentSize(CCSizeMake(800,480));
		this->addChild(layer);

		const char * secondStr = StringDataManager::getString("chat_diaoshi_family_jia");
		const char * thirdStr = StringDataManager::getString("chat_diaoshi_family_zu");
		CCArmature * atmature = MainScene::createPendantAnm("",secondStr,thirdStr,"");
		atmature->setPosition(ccp(30,240));
		layer->addChild(atmature);

		tableviewstate = CCTableView::create(this, CCSizeMake(654,360));
		tableviewstate->setDirection(kCCScrollViewDirectionVertical);
		tableviewstate->setAnchorPoint(ccp(0,0));
		tableviewstate->setPosition(ccp(85,45));
		tableviewstate->setDelegate(this);
		tableviewstate->setVerticalFillOrder(kCCTableViewFillTopDown);
		layer->addChild(tableviewstate);
		tableviewstate->reloadData();

		UIButton * btnClose =(UIButton *)UIHelper::seekWidgetByName(familypanel,"Button_close");
		btnClose->setTouchEnable(true);
		btnClose->setPressedActionEnabled(true);
		btnClose->addReleaseEvent(this,coco_releaseselector(ManageFamily::callBackExit));

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(winsize);
		return true;
	}
	return false;
}

void ManageFamily::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void ManageFamily::onExit()
{
	UIScene::onExit();
}

void ManageFamily::callBackExit( CCObject * obj )
{
	this->closeAnim();
}

void ManageFamily::callBackAgree( CCObject * obj )
{
	CCMenuItemSprite * menu_ =(CCMenuItemSprite *)obj;
	int tag_ =menu_->getTag();

	long long roleId =familyui_->vectorApplyGuildMember.at(tag_)->id() ;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1503,(void*)1,(void *)roleId);

	/*
	for (int i=0;i<familyui_->vectorApplyGuildMember.size();i++)
	{
		if (familyui_->vectorApplyGuildMember.at(i)->id()==roleId)
		{
			std::vector<CGuildMemberBase *>::iterator iter= familyui_->vectorApplyGuildMember.begin() + i;
			CGuildMemberBase * applyPlayer=*iter ;
			familyui_->vectorApplyGuildMember.erase(iter);
			delete applyPlayer;
		}
	}
	tableviewstate->reloadData();
	MainScene * mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
	GameView::getInstance()->applyPlayersize = familyui_->vectorApplyGuildMember.size();
	//mainscene_->checkIsNewRemind();
	mainscene_->remindFamilyApply();
	*/
}

void ManageFamily::callBAckRefuse( CCObject * obj )
{
	CCMenuItemSprite * menu_ =(CCMenuItemSprite *)obj;
	int tag_ =menu_->getTag();
	long long roleId =familyui_->vectorApplyGuildMember.at(tag_)->id() ;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1503,(void*)2,(void *)roleId);
	/*
	for (int i=0;i<familyui_->vectorApplyGuildMember.size();i++)
	{
		if (familyui_->vectorApplyGuildMember.at(i)->id()==roleId)
		{
			std::vector<CGuildMemberBase *>::iterator iter= familyui_->vectorApplyGuildMember.begin() + i;
			CGuildMemberBase * applyPlayer=*iter ;
			familyui_->vectorApplyGuildMember.erase(iter);
			delete applyPlayer;
		}
	}
	tableviewstate->reloadData();
	MainScene * mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
	GameView::getInstance()->applyPlayersize = familyui_->vectorApplyGuildMember.size();
	//mainscene_->checkIsNewRemind();
	mainscene_->remindFamilyApply();
	*/
}

void ManageFamily::scrollViewDidScroll( cocos2d::extension::CCScrollView * view )
{

}

void ManageFamily::scrollViewDidZoom( cocos2d::extension::CCScrollView* view )
{

}

void ManageFamily::tableCellTouched( cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell )
{

}

cocos2d::CCSize ManageFamily::tableCellSizeForIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	return CCSizeMake(650,63);
}

cocos2d::extension::CCTableViewCell* ManageFamily::tableCellAtIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	CCString * string =CCString::createWithFormat("%d",idx);	
	CCTableViewCell *cell =table->dequeueCell();
	cell=new CCTableViewCell();
	cell->autorelease();
	////////需要分页
	CCScale9Sprite * spbg = CCScale9Sprite::create("res_ui/kuang0_new.png");
	spbg->setPreferredSize(CCSizeMake(645,60));
	//spbg->setCapInsets(CCRect(15,30,1,1));
	spbg->setCapInsets(CCRect(18,9,2,23));
	spbg->setAnchorPoint(ccp(0,0));
	spbg->setPosition(ccp(0,0));
	cell->addChild(spbg);

	std::string applyerName = familyui_->vectorApplyGuildMember.at(idx)->name();
	CCLabelTTF * labelName= CCLabelTTF::create(applyerName.c_str(),APP_FONT_NAME,16);
	labelName->setAnchorPoint(ccp(0,0));
	labelName->setPosition(ccp(10,25));
	cell->addChild(labelName);

	int vipLevel_ = familyui_->vectorApplyGuildMember.at(idx)->viplevel();
	if (vipLevel_ > 0)
	{
		CCNode * vipNode = MainScene::addVipInfoByLevelForNode(vipLevel_);
		cell->addChild(vipNode);
		vipNode->setPosition(ccp(labelName->getPositionX() + labelName->getContentSize().width + vipNode->getContentSize().width/2,
											labelName->getPositionY()+vipNode->getContentSize().height/2));
	}

	int guiildLevel = familyui_->vectorApplyGuildMember.at(idx)->level();
	char levelStr[5];
	sprintf(levelStr,"%d",guiildLevel);
	CCLabelTTF * labelManage= CCLabelTTF::create(levelStr, APP_FONT_NAME,16);
	labelManage->setAnchorPoint(ccp(0,0));
	labelManage->setPosition(ccp(150,25));
	cell->addChild(labelManage);

 	int pression_ =familyui_->vectorApplyGuildMember.at(idx)->profession();
	std::string professionName = BasePlayer::getProfessionNameIdxByIndex(pression_);
	CCLabelTTF * labelPression= CCLabelTTF::create(professionName.c_str(), APP_FONT_NAME,16);
	labelPression->setAnchorPoint(ccp(0,0));
	labelPression->setPosition(ccp(212,25));
	cell->addChild(labelPression);

	//const char *stringSort_ = StringDataManager::getString("family_sortString_");
	int fight_ = familyui_->vectorApplyGuildMember.at(idx)->fightpoint();
	char fightStr_[20];
	sprintf(fightStr_,"%d",fight_);
	CCLabelTTF * labelRank= CCLabelTTF::create(fightStr_, APP_FONT_NAME,16);
	labelRank->setAnchorPoint(ccp(0,0));
	labelRank->setPosition(ccp(310,25));
	cell->addChild(labelRank);

	const char *strings_check = StringDataManager::getString("family_mamager_agree");
	char * str_check=const_cast<char*>(strings_check);	
	CCScale9Sprite * bg_check = CCScale9Sprite::create("res_ui/new_button_1.png");
	bg_check->setPreferredSize(CCSizeMake(80,36));
	bg_check->setCapInsets(CCRect(15,30,1,1));
	CCMenuItemSprite *btn_check = CCMenuItemSprite::create(bg_check, bg_check, bg_check, this, menu_selector(ManageFamily::callBackAgree));
	btn_check->setTag(idx);
	btn_check->setZoomScale(0.5f);
	CCMoveableMenu *menu_check=CCMoveableMenu::create(btn_check,NULL);
	menu_check->setAnchorPoint(ccp(0,0));
	menu_check->setPosition(ccp(490,33));
	cell->addChild(menu_check);
	CCLabelTTF * label_check= CCLabelTTF::create(str_check,APP_FONT_NAME,16);
	label_check->setAnchorPoint(ccp(0.5f,0.5f));
	label_check->setPosition(ccp(bg_check->getContentSize().width/2,bg_check->getContentSize().height/2));
	btn_check->addChild(label_check);
	
	const char *strings_apply = StringDataManager::getString("family_manager_result");
	char * str_apply=const_cast<char*>(strings_apply);	
	CCScale9Sprite * bg_apply = CCScale9Sprite::create("res_ui/new_button_2.png");
	bg_apply->setPreferredSize(CCSizeMake(80,36));
	bg_apply->setCapInsets(CCRect(15,30,1,1));
	CCMenuItemSprite *btn_apply = CCMenuItemSprite::create(bg_apply, bg_apply, bg_apply, this, menu_selector(ManageFamily::callBAckRefuse));
	btn_apply->setTag(idx);
	btn_apply->setZoomScale(0.5f);
	CCMoveableMenu *menu_apply=CCMoveableMenu::create(btn_apply,NULL);
	menu_apply->setAnchorPoint(ccp(0,0));
	menu_apply->setPosition(ccp(590,33));
	cell->addChild(menu_apply);	
	CCLabelTTF * label_apply= CCLabelTTF::create(str_apply,APP_FONT_NAME,16);
	label_apply->setAnchorPoint(ccp(0.5f,0.5f));
	label_apply->setPosition(ccp(bg_apply->getContentSize().width/2,bg_apply->getContentSize().height/2));
	btn_apply->addChild(label_apply);

	if (idx==familyui_->vectorApplyGuildMember.size()-3)
	{
		if (familyui_->hasNextPageOfManageMember == true)
		{
			familyui_->curPageOfManageFamilyMember ++;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1512,(void *)familyui_->curPageOfManageFamilyMember);
		}
	}

	return cell;
}

unsigned int ManageFamily::numberOfCellsInTableView( cocos2d::extension::CCTableView *table )
{
	return familyui_->vectorApplyGuildMember.size();
}

void ManageFamily::ccTouchMoved( CCTouch *pTouch, CCEvent *pEvent )
{

}

void ManageFamily::ccTouchEnded( CCTouch *pTouch, CCEvent *pEvent )
{

}

void ManageFamily::ccTouchCancelled( CCTouch *pTouch, CCEvent *pEvent )
{

}


