#include "TalkMissionUI.h"
#include "../../messageclient/element/MissionInfo.h"
#include "../../messageclient/element/CNpcInfo.h"
#include "../../legend_engine/GameWorld.h"
#include "../../utils/StaticDataManager.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "MissionManager.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../ui/extensions/CCRichLabel.h"
#include "../../legend_engine/CCLegendAnimation.h"


TalkMissionUI::TalkMissionUI()
:targetNpcId(0)
{
}


TalkMissionUI::~TalkMissionUI()
{
	delete curMissionInfo;
}


void TalkMissionUI::update( float dt )
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkMissionUI) != NULL)
	{
		if(targetNpcId == 0)
			return;

		if (GameView::getInstance()->missionManager->isOutOfMissionTalkArea(this->getTargetNpcId()))
		{
			this->closeAnim();
		}
	}
}

int TalkMissionUI::getTargetNpcId()
{
	return targetNpcId;
}

TalkMissionUI* TalkMissionUI::create(MissionInfo * missionInfo)
{
	TalkMissionUI *talkMissionUI = new TalkMissionUI();
	if (talkMissionUI && talkMissionUI->init(missionInfo))
	{
		talkMissionUI->autorelease();
		return talkMissionUI;
	}
	CC_SAFE_DELETE(talkMissionUI);
	return NULL;

}

bool TalkMissionUI::init(MissionInfo * missionInfo)
{
	if (UIScene::init())
	{
		curMissionInfo = new MissionInfo();
		curMissionInfo->CopyFrom(*missionInfo);

		if (missionInfo->action().has_moveto())
		{
			if (missionInfo->action().moveto().has_targetnpc())
			{
				targetNpcId = missionInfo->action().moveto().targetnpc().npcid();
			}
		}

		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		this->scheduleUpdate();
		//加载UI
		UIPanel *ppanel = (UIPanel *)GUIReader::shareReader()->widgetFromJsonFile("res_ui/renwub_1.json");
		ppanel->setAnchorPoint(ccp(0,0));
		ppanel->getValidNode()->setContentSize(CCSizeMake(350, 400));
		ppanel->setPosition(CCPointZero);
		ppanel->setTouchEnable(true);
		m_pUiLayer->addWidget(ppanel);

		UIButton * Button_close = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnable(true);
		Button_close->addReleaseEvent(this, coco_releaseselector(TalkMissionUI::CloseEvent));
		Button_close->setPressedActionEnabled(true);

		UIButton * Button_enter = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_lingqu");
		Button_enter->setTouchEnable(true);
		Button_enter->addReleaseEvent(this, coco_releaseselector(TalkMissionUI::EnterEvent));
		Button_enter->setPressedActionEnabled(true);

		// load animation
		CNpcInfo * npcInfo = GameWorld::NpcInfos[targetNpcId];
		std::string animFileName = "res_ui/npc/";
		if (targetNpcId == 0)//没有任务NPC
		{
			animFileName.append("cz");
		}
		else
		{
			if (npcInfo)
			{
				animFileName.append(npcInfo->icon.c_str());
			}
			else
			{
				animFileName.append("cz");
			}
		}
		animFileName.append(".anm");
		//std::string animFileName = "res_ui/npc/caiwenji.anm";
		CCLegendAnimation *m_pAnim = CCLegendAnimation::create(animFileName);
		if (m_pAnim)
		{
			m_pAnim->setPlayLoop(true);
			m_pAnim->setReleaseWhenStop(false);
			m_pAnim->setScale(1.0f);
			m_pAnim->setPosition(ccp(33,291));
			//m_pAnim->setPlaySpeed(.35f);
			addChild(m_pAnim);
		}

		CCSprite * sprite_anmFrame = CCSprite::create("res_ui/renwua/tietu2.png");
		sprite_anmFrame->setAnchorPoint(ccp(0.5,0.5f));
		sprite_anmFrame->setPosition(ccp(115,335));
		addChild(sprite_anmFrame);
		UIImageView *ImageView_name = (UIImageView*)UIHelper::seekWidgetByName(ppanel,"ImageView_mingzi");

		ccFontDefinition strokeShaodwTextDef;
		strokeShaodwTextDef.m_fontName = std::string("res_ui/font/simhei.ttf");
		ccColor3B tintColorGreen   =  { 54, 92, 7 };
		strokeShaodwTextDef.m_fontFillColor   = tintColorGreen;   // 字体颜色
		strokeShaodwTextDef.m_fontSize = 22;   // 字体大小
		// 		strokeShaodwTextDef.m_stroke.m_strokeEnabled = true;   // 是否勾边
		// 		strokeShaodwTextDef.m_stroke.m_strokeColor   = blackColor;   // 勾边颜色，黑色
		// 		strokeShaodwTextDef.m_stroke.m_strokeSize    = 2.0;   // 勾边的大小
		/*CCLabelTTF* l_nameLabel = CCLabelTTF::createWithFontDefinition(npcInfo->npcName.c_str(), strokeShaodwTextDef);
		ccColor3B shadowColor = ccc3(0,0,0);   // black
		l_nameLabel->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		l_nameLabel->setAnchorPoint(ccp(0.5,0.5f));
		l_nameLabel->setPosition(ccp(184,359));
		addChild(l_nameLabel);*/
		CCLabelTTF* l_nameLabel;
		if (targetNpcId == 0) //没有任务NPC
		{
			l_nameLabel = CCLabelTTF::create(StringDataManager::getString("mission_teachNpcName"),"res_ui/font/simhei.ttf",18);
		}
		else
		{
			if (npcInfo)
			{
				l_nameLabel = CCLabelTTF::create(npcInfo->npcName.c_str(),"res_ui/font/simhei.ttf",18);
			}
			else
			{
				l_nameLabel = CCLabelTTF::create(StringDataManager::getString("mission_teachNpcName"),"res_ui/font/simhei.ttf",18);
			}
		}
		l_nameLabel->setAnchorPoint(ccp(0.5,0.5f));
		l_nameLabel->setPosition(ccp(207,307));
		ccColor3B shadowColor = ccc3(0,0,0);   // black
		l_nameLabel->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		addChild(l_nameLabel);

		//description
		std::string mission_description = "";
// 		if (missionInfo->has_tip())
// 		{
// 			mission_description.append(missionInfo->tip().dialog());
// 		}
// 		else
// 		{
// 			mission_description.append(missionInfo->dialog());
// 		}
		mission_description.append(missionInfo->action().dialog());
// 		CCLabelTTF * label_des = CCLabelTTF::create(mission_description.c_str(),"res_ui/font/simhei.ttf",18,CCSizeMake(285,0),kCCTextAlignmentLeft,kCCVerticalTextAlignmentTop);
// 		label_des->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
// 		CCSize desSize = label_des->getContentSize();
// 		int height = desSize.height;
		CCRichLabel *label_des = CCRichLabel::createWithString(mission_description.c_str(),CCSizeMake(285, 0),this,NULL,0,18,5);
		CCSize desSize = label_des->getContentSize();
		int height = desSize.height;
		//ccscrollView_description
		CCScrollView *m_contentScrollView = CCScrollView::create(CCSizeMake(290,99));
		m_contentScrollView->setViewSize(CCSizeMake(290, 99));
		m_contentScrollView->ignoreAnchorPointForPosition(false);
		m_contentScrollView->setTouchEnabled(true);
		m_contentScrollView->setDirection(kCCScrollViewDirectionVertical);
		m_contentScrollView->setAnchorPoint(ccp(0,0));
		m_contentScrollView->setPosition(ccp(30,185));
		m_contentScrollView->setBounceable(true);
		m_contentScrollView->setClippingToBounds(true);
		m_pUiLayer->addChild(m_contentScrollView);
		if (height > 99)
		{
			m_contentScrollView->setContentSize(ccp(290,height));
			m_contentScrollView->setContentOffset(ccp(0,99-height));
		}
		else
		{
			m_contentScrollView->setContentSize(ccp(290,99));
		}
		m_contentScrollView->setClippingToBounds(true);

		m_contentScrollView->addChild(label_des);
		label_des->ignoreAnchorPointForPosition(false);
		label_des->setAnchorPoint(ccp(0,1));
		label_des->setPosition(ccp(3,m_contentScrollView->getContentSize().height-3));


		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(CCSizeMake(350,400));

		return true;
	}
	return false;

}


void TalkMissionUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();

	//this->setTouchEnabled(true);
	//this->setContentSize(CCSizeMake(800,480));


}

void TalkMissionUI::onExit()
{
	UIScene::onExit();
}


bool TalkMissionUI::ccTouchBegan(CCTouch *touch, CCEvent * pEvent)
{
	//return this->resignFirstResponder(touch,this,false);
	CCPoint location = touch->getLocation();

	CCPoint pos = this->getPosition();
	CCSize size = this->getContentSize();
	CCPoint anchorPoint = this->getAnchorPointInPoints();
	pos = ccpSub(pos, anchorPoint);
	CCRect rect(pos.x, pos.y, size.width, size.height);
	if(rect.containsPoint(location))
	{
		return true;
	}

	return false;
}

void TalkMissionUI::ccTouchEnded(CCTouch *touch, CCEvent * pEvent)
{
}

void TalkMissionUI::ccTouchCancelled(CCTouch *touch, CCEvent * pEvent)
{
}

void TalkMissionUI::ccTouchMoved(CCTouch *touch, CCEvent * pEvent)
{
}

void TalkMissionUI::CloseEvent( CCObject * pSender )
{
	this->closeAnim();
}

void TalkMissionUI::EnterEvent( CCObject *pSender )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(7012,(void *)getTargetNpcId());
	this->closeAnim();
}
