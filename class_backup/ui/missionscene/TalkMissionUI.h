#ifndef _MISSIONSCENE_TALKMISSIONUI_H_
#define _MISSIONSCENE_TALKMISSIONUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class MissionInfo;

class TalkMissionUI : public UIScene
{
public:
	TalkMissionUI();
	~TalkMissionUI();

	static TalkMissionUI* create(MissionInfo * missionInfo);
	bool init(MissionInfo * missionInfo);

	virtual void update( float dt );

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	void CloseEvent(CCObject *pSender);
	void EnterEvent(CCObject *pSender);

	int getTargetNpcId();

private:
	MissionInfo * curMissionInfo;
	int targetNpcId;

};

#endif
