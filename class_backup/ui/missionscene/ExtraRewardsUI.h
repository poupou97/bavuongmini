#ifndef _MISSIONSCENE_EXTRAREWARDSUI_
#define _MISSIONSCENE_EXTRAREWARDSUI_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CMissionReward;
class CMissionGoods;

class ExtraRewardsUI : public UIScene
{
public:
	ExtraRewardsUI();
	~ExtraRewardsUI();

	static ExtraRewardsUI* create(CMissionReward * missionReward,std::string des);
	bool init(CMissionReward * missionReward,std::string des);

	virtual void update( float dt );

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	void CloseEvent(CCObject *pSender);

private:
};

#endif