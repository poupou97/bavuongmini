#include "HandleMission.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/MissionInfo.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../extensions/CCRichLabel.h"
#include "../extensions/UITab.h"
#include "../../messageclient/protobuf/MissionMessage.pb.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "../../messageclient/element/CNpcInfo.h"
#include "../../legend_engine/GameWorld.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "MissionRewardGoodsItem.h"
#include "../../utils/StaticDataManager.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "MissionManager.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../utils/GameUtils.h"
#include "../../legend_script/CCTeachingGuide.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/Script.h"

using namespace CocosDenshion;

#define HANDLEMISSION_PARTICLEATBUTTON_TAG 21
#define HANDLEMISSION_TUTORIALINDICATOR_TAG 22

HandleMission::HandleMission() : 
curHandleType(getMission),
targetNpcId(0)
{
	rewardIconName[0] = "rewardIcon_0";
	rewardIconName[1] = "rewardIcon_1";
	rewardIconName[2] = "rewardIcon_2";
	rewardIconName[3] = "rewardIcon_3";
	rewardIconName[4] = "rewardIcon_4";
	rewardLabelName[0] = "rewardLabel_0";
	rewardLabelName[1] = "rewardLabel_1";
	rewardLabelName[2] = "rewardLabel_2";
	rewardLabelName[3] = "rewardLabel_3";
	rewardLabelName[4] = "rewardLabel_4";
}


HandleMission::~HandleMission()
{
	delete curMissionInfo;
}

void HandleMission::update( float dt )
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHandleMission) != NULL)
	{
		if (targetNpcId == 0)   //没有目标NPC时不需要检测
			return;

	 	if (GameView::getInstance()->missionManager->isOutOfMissionTalkArea(this->getTargetNpcId()))
	 	{
	 		this->closeAnim();
	 	}
	}
}

HandleMission* HandleMission::create(MissionInfo * missionInfo)
{
	HandleMission *handleMission = new HandleMission();
	if (handleMission && handleMission->init(missionInfo))
	{
		handleMission->autorelease();
		return handleMission;
	}
	CC_SAFE_DELETE(handleMission);
	return NULL;

}

bool HandleMission::init(MissionInfo * missionInfo)
{
	if (UIScene::init())
	{
		curMissionInfo = new MissionInfo();
		curMissionInfo->CopyFrom(*missionInfo);

		if (missionInfo->has_tip())
		{
			targetNpcId = missionInfo->tip().npcid();
		}
		else if (missionInfo->action().has_moveto())
		{
			if (missionInfo->action().moveto().has_targetnpc())
			{
				targetNpcId = missionInfo->action().moveto().targetnpc().npcid();
			}
		}
		else
		{
		}

		GameView::getInstance()->missionManager->m_nTargetNpcId = targetNpcId;

		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		this->scheduleUpdate();
		//加载UI
		if(LoadSceneLayer::TalkWithNpcLayer->getWidgetParent() != NULL)
		{
			LoadSceneLayer::TalkWithNpcLayer->removeFromParentAndCleanup(false);
		}
		UIPanel *ppanel = LoadSceneLayer::TalkWithNpcLayer;
		ppanel->setAnchorPoint(ccp(0.0f,0.0f));
		ppanel->getValidNode()->setContentSize(CCSizeMake(350, 420));
		ppanel->setPosition(CCPointZero);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnable(true);
		m_pUiLayer->addWidget(ppanel);

		UIButton * Button_close = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnable(true);
		Button_close->addReleaseEvent(this, coco_releaseselector(HandleMission::CloseEvent));
		Button_close->setPressedActionEnabled(true);
		
		UIPanel * panel_mission = (UIPanel*)UIHelper::seekWidgetByName(ppanel,"Panel_renwu");
		panel_mission->setVisible(true);
		Button_getMission = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_lingqu");
		Button_getMission->setTouchEnable(true);
		Button_getMission->setPressedActionEnabled(true);
		Button_getMission->addReleaseEvent(this,coco_releaseselector(HandleMission::GetMissionEvent));
		Button_finishMission = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_tijiao");
		Button_finishMission->setTouchEnable(true);
		Button_finishMission->setPressedActionEnabled(true);
		Button_finishMission->addReleaseEvent(this,coco_releaseselector(HandleMission::FinishMissionEvent));

		reward_di_1 = (UIImageView * )UIHelper::seekWidgetByName(ppanel,"ImageView_di1");
		reward_di_2 = (UIImageView * )UIHelper::seekWidgetByName(ppanel,"ImageView_di2");
		reward_di_3 = (UIImageView * )UIHelper::seekWidgetByName(ppanel,"ImageView_di3");
		reward_di_4 = (UIImageView * )UIHelper::seekWidgetByName(ppanel,"ImageView_di4");
		reward_di_1->setVisible(false);
		reward_di_2->setVisible(false);
		reward_di_3->setVisible(false);
		reward_di_4->setVisible(false);

		if(this->getChildByTag(CCTUTORIALINDICATORTAG) == NULL)
		{
			// the particle effect
			//if (GameView::getInstance()->myplayer->getActiveRole()->level() <= 20)
			//{
			//	CCTutorialParticle * tutorialParticle = CCTutorialParticle::create("tuowei0.plist",80,60);
			//	tutorialParticle->setPosition(ccp(Button_getMission->getPosition().x,Button_getMission->getPosition().y-25));
			//	tutorialParticle->setTag(HANDLEMISSION_PARTICLEATBUTTON_TAG); 
			//	this->addChild(tutorialParticle);
			//}

			// the hand
			if (GameView::getInstance()->myplayer->getActiveRole()->level() <= 10)
			{
				CCTutorialIndicator * guideHand = (CCTutorialIndicator*)this->getChildByTag(HANDLEMISSION_TUTORIALINDICATOR_TAG);
				if (guideHand == NULL)
				{
					guideHand = CCTutorialIndicator::create("",ccp(0,0),CCTutorialIndicator::Direction_LU,true,false);
					guideHand->setPosition(ccp(Button_getMission->getPosition().x,Button_getMission->getPosition().y));
					guideHand->setTag(HANDLEMISSION_TUTORIALINDICATOR_TAG);
					guideHand->addHighLightFrameAction(115,52);
					guideHand->addCenterAnm(115,52);
					this->addChild(guideHand);
				}
			}
		}

		// load animation
		CNpcInfo * npcInfo = NULL;
		if (targetNpcId == 0)
		{
			if (missionInfo->action().has_specifiedui())
			{
				if (missionInfo->action().specifiedui().has_para())
				{
					std::map<long long ,CNpcInfo*>::const_iterator cIter;
					cIter = GameWorld::NpcInfos.find(missionInfo->action().specifiedui().para());
					if (cIter == GameWorld::NpcInfos.end()) // 没找到就是指向END了  
					{
						npcInfo = NULL;
					}
					else
					{
						npcInfo = GameWorld::NpcInfos[missionInfo->action().specifiedui().para()];
					}
				}
			}
		}
		else
		{
			std::map<long long ,CNpcInfo*>::const_iterator cIter;
			cIter = GameWorld::NpcInfos.find(targetNpcId);
			if (cIter == GameWorld::NpcInfos.end()) // 没找到就是指向END了  
			{
				npcInfo = NULL;
			}
			else
			{
				npcInfo = GameWorld::NpcInfos[targetNpcId];
			}
		}

		std::string animFileName = "res_ui/npc/";
		if (npcInfo == NULL)//没有任务NPC
		{
			animFileName.append("cz");
		}
		else
		{
			animFileName.append(npcInfo->icon.c_str());
		}
		animFileName.append(".anm");
		//std::string animFileName = "res_ui/npc/caiwenji.anm";
		CCLegendAnimation *m_pAnim = CCLegendAnimation::create(animFileName);
		if (m_pAnim)
		{
			m_pAnim->setPlayLoop(true);
			m_pAnim->setReleaseWhenStop(false);
			m_pAnim->setScale(1.0f);
			m_pAnim->setPosition(ccp(33,312));
			//m_pAnim->setPlaySpeed(.35f);
			addChild(m_pAnim);
		}

		CCSprite * sprite_anmFrame = CCSprite::create("res_ui/renwua/tietu2.png");
		sprite_anmFrame->setAnchorPoint(ccp(0.5,0.5f));
		sprite_anmFrame->setPosition(ccp(116,356));
		addChild(sprite_anmFrame);


		UIImageView *ImageView_name = (UIImageView*)UIHelper::seekWidgetByName(ppanel,"ImageView_mingzi");

		ccFontDefinition strokeShaodwTextDef;
		strokeShaodwTextDef.m_fontName = std::string("res_ui/font/simhei.ttf");
		ccColor3B tintColorGreen   =  { 54, 92, 7 };
		strokeShaodwTextDef.m_fontFillColor   = tintColorGreen;   // 字体颜色
		strokeShaodwTextDef.m_fontSize = 22;   // 字体大小
		// 		strokeShaodwTextDef.m_stroke.m_strokeEnabled = true;   // 是否勾边
		// 		strokeShaodwTextDef.m_stroke.m_strokeColor   = blackColor;   // 勾边颜色，黑色
		// 		strokeShaodwTextDef.m_stroke.m_strokeSize    = 2.0;   // 勾边的大小
		/*CCLabelTTF* l_nameLabel = CCLabelTTF::createWithFontDefinition(npcInfo->npcName.c_str(), strokeShaodwTextDef);
		ccColor3B shadowColor = ccc3(0,0,0);   // black
		l_nameLabel->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		l_nameLabel->setAnchorPoint(ccp(0.5,0.5f));
		l_nameLabel->setPosition(ccp(184,359));
		addChild(l_nameLabel);*/
		CCLabelTTF* l_nameLabel;
		if (npcInfo == NULL) //没有任务NPC
		{
			l_nameLabel = CCLabelTTF::create(StringDataManager::getString("mission_teachNpcName"),"res_ui/font/simhei.ttf",18);
		}
		else
		{
			l_nameLabel = CCLabelTTF::create(npcInfo->npcName.c_str(),"res_ui/font/simhei.ttf",18);
		}
		l_nameLabel->setAnchorPoint(ccp(0.5,0.5f));
		l_nameLabel->setPosition(ccp(207,327));
		ccColor3B shadowColor = ccc3(0,0,0);   // black
		l_nameLabel->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		addChild(l_nameLabel);
// 		UIImageView * rewardImage = UIImageView::create();
// 		rewardImage->setTexture("res_ui/renwua/renwujianglitu.png");
// 		rewardImage->setAnchorPoint(CCPointZero);
// 		rewardImage->setPosition(ccp(132,129));
// 		m_pUiLayer->addWidget(rewardImage);

// 		Button_Handle = UIButton::create();
// 		Button_Handle->setTextures("res_ui/renwua/renwulingqu.png","res_ui/renwua/renwulingqu.png","");
// 		Button_Handle->setAnchorPoint(CCPointZero);
// 		Button_Handle->setTouchEnable(true);
// 		Button_Handle->setPosition(ccp(30,65));
// 		Button_Handle->addReleaseEvent(this,coco_releaseselector(HandleMission::HandleEvent));
// 		m_pUiLayer->addWidget(Button_Handle);

		UILabelBMFont * lbf_reward_gold  = (UILabelBMFont *)UIHelper::seekWidgetByName(ppanel,"LabelBMFont_goldValue");
		UILabelBMFont * lbf_reward_exp  = (UILabelBMFont *)UIHelper::seekWidgetByName(ppanel,"LabelBMFont_expValue");

		if (missionInfo->has_rewards())
		{
			char s_gold[20];
			sprintf(s_gold,"%d",missionInfo->rewards().gold());
			lbf_reward_gold->setText(s_gold);

			char s_exp[20];
			sprintf(s_exp,"%d",missionInfo->rewards().exp());
			lbf_reward_exp->setText(s_exp);

			if (missionInfo->rewards().goods_size()>0)
			{
				for (int i =0;i<missionInfo->rewards().goods_size();++i)
				{
					GoodsInfo * goodInfo = new GoodsInfo();
					goodInfo->CopyFrom(missionInfo->rewards().goods(i).goods());
					int num = missionInfo->rewards().goods(i).quantity();

					MissionRewardGoodsItem * mrGoodsItem = MissionRewardGoodsItem::create(goodInfo,num);
					mrGoodsItem->setAnchorPoint(ccp(0.5f,0.5f));
					mrGoodsItem->setPosition(ccp(82+58*i,106));
					mrGoodsItem->setName(rewardIconName[i].c_str());
					mrGoodsItem->setScale(0.8f);
					m_pUiLayer->addWidget(mrGoodsItem);

					delete goodInfo;
				}
			}
		}

		UILabel * l_phyValue = (UILabel*)(UILabelBMFont *)UIHelper::seekWidgetByName(ppanel,"Label_phyValue");
		l_phyValue->setVisible(false);

		if (missionInfo->missionstate() < 1)
		{
			curHandleType = getMission;
			Button_getMission->setVisible(true);
			Button_finishMission->setVisible(false);
			//Button_Handle->setTextures("res_ui/renwua/renwulingqu.png","res_ui/renwua/renwulingqu.png","");

			if (missionInfo->has_physicalpower() && missionInfo->physicalpower() >0)
			{
				l_phyValue->setVisible(true);
				char  str_phyValue[10];
				sprintf(str_phyValue,"%d",missionInfo->physicalpower());
				l_phyValue->setText(str_phyValue);
			}
		}
		else 
		{
			curHandleType = submitMission;
			Button_getMission->setVisible(false);
			Button_finishMission->setVisible(true);
			//Button_Handle->setTextures("res_ui/renwua/renwuwancheng.png","res_ui/renwua/renwuwancheng.png","");
		}

		//description
		std::string mission_description = "";
		if (missionInfo->has_tip())
		{
			mission_description.append(missionInfo->tip().dialog());
		}
		else
		{
			mission_description.append(missionInfo->dialog());
		}
// 		CCLabelTTF * label_des = CCLabelTTF::create(mission_description.c_str(),"res_ui/font/simhei.ttf",18,CCSizeMake(285,0),kCCTextAlignmentLeft,kCCVerticalTextAlignmentTop);
// 		label_des->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		CCRichLabel *label_des = CCRichLabel::createWithString(mission_description.c_str(),CCSizeMake(285, 0),this,NULL,0,18,5);
		CCSize desSize = label_des->getContentSize();
		int height = desSize.height;
		//ccscrollView_description
		CCScrollView *m_contentScrollView = CCScrollView::create(CCSizeMake(290,99));
		m_contentScrollView->setViewSize(CCSizeMake(290, 99));
		m_contentScrollView->ignoreAnchorPointForPosition(false);
		m_contentScrollView->setTouchEnabled(true);
		m_contentScrollView->setDirection(kCCScrollViewDirectionVertical);
		m_contentScrollView->setAnchorPoint(ccp(0,0));
		m_contentScrollView->setPosition(ccp(30,205));
		m_contentScrollView->setBounceable(true);
		m_contentScrollView->setClippingToBounds(true);
		m_pUiLayer->addChild(m_contentScrollView);
		if (height > 99)
		{
			m_contentScrollView->setContentSize(ccp(290,height));
			m_contentScrollView->setContentOffset(ccp(0,99-height));
		}
		else
		{
			m_contentScrollView->setContentSize(ccp(290,99));
		}
		m_contentScrollView->setClippingToBounds(true);

		m_contentScrollView->addChild(label_des);
		label_des->ignoreAnchorPointForPosition(false);
		label_des->setAnchorPoint(ccp(0,1));
		label_des->setPosition(ccp(3,m_contentScrollView->getContentSize().height-3));
		

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(CCSizeMake(350,420));

		return true;
	}
	return false;

}


void HandleMission::onEnter()
{
	UIScene::onEnter();

	CCFiniteTimeAction*  action = CCSequence::create(
		CCScaleTo::create(0.15f*1/2,1.1f),
		CCScaleTo::create(0.15f*1/3,1.0f),
		NULL);

	runAction(action);

	//this->setTouchEnabled(true);
	//this->setContentSize(CCSizeMake(800,480));


}

void HandleMission::onExit()
{
	UIScene::onExit();
}


bool HandleMission::ccTouchBegan(CCTouch *touch, CCEvent * pEvent)
{
	//return this->resignFirstResponder(touch,this,false);
	CCPoint location = touch->getLocation();

	CCPoint pos = this->getPosition();
	CCSize size = this->getContentSize();
	CCPoint anchorPoint = this->getAnchorPointInPoints();
	pos = ccpSub(pos, anchorPoint);
	CCRect rect(pos.x, pos.y, size.width, size.height);
	if(rect.containsPoint(location))
	{
		return true;
	}

	return false;
}

void HandleMission::ccTouchEnded(CCTouch *touch, CCEvent * pEvent)
{
}

void HandleMission::ccTouchCancelled(CCTouch *touch, CCEvent * pEvent)
{
}

void HandleMission::ccTouchMoved(CCTouch *touch, CCEvent * pEvent)
{
}

void HandleMission::CloseEvent( CCObject * pSender )
{
	this->closeAnim();
}

void HandleMission::GetMissionEvent( CCObject * pSender )
{
	if (curMissionInfo->has_tip())
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("mission_notFinish"));
	}
	else
	{
		Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if(sc != NULL)
			sc->endCommand(pSender);

		GameUtils::playGameSound(MISSION_ACCEPT, 2, false);
		GameMessageProcessor::sharedMsgProcessor()->sendReq(7002, this);
		this->closeAnim();
	}
}

void HandleMission::FinishMissionEvent( CCObject * pSender )
{
	if (curMissionInfo->has_tip())
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("mission_notFinish"));
	}
	else
	{
		Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if(sc != NULL)
			sc->endCommand(pSender);

		GameMessageProcessor::sharedMsgProcessor()->sendReq(7007, (char *)curMissionInfo->missionpackageid().c_str());
		this->closeAnim();
	}
}

int HandleMission::getTargetNpcId()
{
	return targetNpcId;
}

HandleMission::handleType HandleMission::getCurHandlerType()
{
	return curHandleType;
}

void HandleMission::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void HandleMission::addCCTutorialIndicator( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
	if (this->getChildByTag(HANDLEMISSION_PARTICLEATBUTTON_TAG))
	{
		this->getChildByTag(HANDLEMISSION_PARTICLEATBUTTON_TAG)->removeFromParent();
	}

	if (this->getChildByTag(HANDLEMISSION_TUTORIALINDICATOR_TAG))
	{
		this->getChildByTag(HANDLEMISSION_TUTORIALINDICATOR_TAG)->removeFromParent();
	}

	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,70,50);
// 	tutorialIndicator->setPosition(ccp(pos.x+55,pos.y+80));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);
	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,115,52,true);
	tutorialIndicator->setDrawNodePos(ccp(pos.x,pos.y+30+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void HandleMission::removeCCTutorialIndicator()
{
	CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		CCTeachingGuide* teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}

