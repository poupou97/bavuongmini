#include "MissionTalkWithNpc.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../AppMacros.h"
#include "../../messageclient/protobuf/NPCMessage.pb.h"
#include "../../messageclient/element/CNpcDialog.h"
#include "../extensions/CCRichLabel.h"
#include "../../messageclient/element/MissionInfo.h"
#include "../../legend_engine/LegendLevel.h"
#include "../../gamescene_state/role/FunctionNPC.h"
#include "../../GameView.h"
#include "../../ui/missionscene/MissionManager.h"
#include "HandleMission.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../extensions/UITab.h"
#include "../../messageclient/element/CNpcInfo.h"
#include "../../legend_engine/GameWorld.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "AppMacros.h"
#include "../../utils/StaticDataManager.h"
#include "../../legend_engine/CCLegendAnimation.h"

MissionTalkWithNpc::MissionTalkWithNpc()
{
}


MissionTalkWithNpc::~MissionTalkWithNpc()
{
}


void MissionTalkWithNpc::update( float dt )
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkWithNpc) != NULL)
	{
		if (GameView::getInstance()->missionManager->isOutOfMissionTalkArea(this->getCurNpcId()))
		{
			this->closeAnim();
		}
	}
}


MissionTalkWithNpc* MissionTalkWithNpc::create(FunctionNPC* fcNpc)
{
	MissionTalkWithNpc * talkWithNpc = new MissionTalkWithNpc();
	if(talkWithNpc && talkWithNpc->init(fcNpc))
	{
		talkWithNpc->autorelease();
		return talkWithNpc;
	}
	CC_SAFE_DELETE(talkWithNpc);
	return NULL;
}

bool MissionTalkWithNpc::init(FunctionNPC* fcNpc)
{
	if (UIScene::init())
	{
		curNpcId = fcNpc->getRoleId();
		curFunctionNPC = fcNpc;
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		this->scheduleUpdate();
		//加载UI
// 		if(LoadSceneLayer::TalkWithNpcLayer->getWidgetParent() != NULL)
// 		{
// 			LoadSceneLayer::TalkWithNpcLayer->removeFromParentAndCleanup(false);
// 		}
// 		UIPanel *ppanel = LoadSceneLayer::TalkWithNpcLayer;
// 		ppanel->setAnchorPoint(ccp(0.0f,0.0f));
// 		ppanel->getValidNode()->setContentSize(CCSizeMake(350, 420));
// 		ppanel->setPosition(CCPointZero);
// 		ppanel->setScale(1.0f);
// 		ppanel->setTouchEnable(true);
// 		m_pUiLayer->addWidget(ppanel);
// 
// 		UIPanel * panel_mission = (UIPanel*)UIHelper::seekWidgetByName(ppanel,"Panel_renwu");
// 		panel_mission->setVisible(false);

		UIPanel * ppanel = (UIPanel *)GUIReader::shareReader()->widgetFromJsonFile("res_ui/talkWithNpc_1.json");
		ppanel->setAnchorPoint(ccp(0.0f,0.0f));
		ppanel->setPosition(CCPointZero);	
		ppanel->setTouchEnable(true);
		m_pUiLayer->addWidget(ppanel);

		CNpcInfo * npcInfo = GameWorld::NpcInfos[fcNpc->getRoleId()];

		UIButton * button_close = (UIButton *)UIHelper::seekWidgetByName(ppanel,"Button_close");
		button_close->setTouchEnable(true);
		button_close->addReleaseEvent(this,coco_releaseselector(MissionTalkWithNpc::CloseEvent));
		ImageView_headImage = (UIImageView *)UIHelper::seekWidgetByName(ppanel,"ImageView_touxiang");;
		ImageView_name = (UIImageView*)UIHelper::seekWidgetByName(ppanel,"ImageView_mingzi");

		// head anm
		std::string animFileName = "res_ui/npc/";
		animFileName.append(npcInfo->icon.c_str());
		animFileName.append(".anm");
		//std::string animFileName = "res_ui/npc/caiwenji.anm";
		CCLegendAnimation *m_pAnim = CCLegendAnimation::create(animFileName);
		if (m_pAnim)
		{
			m_pAnim->setPlayLoop(true);
			m_pAnim->setReleaseWhenStop(false);
			m_pAnim->setScale(1.0f);
			m_pAnim->setPosition(ccp(33,312));
			//m_pAnim->setPlaySpeed(.35f);
			addChild(m_pAnim);
		}

		CCSprite * sprite_anmFrame = CCSprite::create("res_ui/renwua/tietu2.png");
		sprite_anmFrame->setAnchorPoint(ccp(0.5,0.5f));
		sprite_anmFrame->setPosition(ccp(116,356));
		addChild(sprite_anmFrame);


		UIImageView *ImageView_name = (UIImageView*)UIHelper::seekWidgetByName(ppanel,"ImageView_mingzi");

		ccFontDefinition strokeShaodwTextDef;
		strokeShaodwTextDef.m_fontName = std::string(APP_FONT_NAME);
		ccColor3B tintColorGreen   =  { 54, 92, 7 };
		strokeShaodwTextDef.m_fontFillColor   = tintColorGreen;   // 字体颜色
		strokeShaodwTextDef.m_fontSize = 22;   // 字体大小
		// 		strokeShaodwTextDef.m_stroke.m_strokeEnabled = true;   // 是否勾边
		// 		strokeShaodwTextDef.m_stroke.m_strokeColor   = blackColor;   // 勾边颜色，黑色
		// 		strokeShaodwTextDef.m_stroke.m_strokeSize    = 2.0;   // 勾边的大小
		/*CCLabelTTF* l_nameLabel = CCLabelTTF::createWithFontDefinition(npcInfo->npcName.c_str(),strokeShaodwTextDef);
		ccColor3B shadowColor = ccc3(0,0,0);   // black
		l_nameLabel->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		l_nameLabel->setAnchorPoint(ccp(0.5,0.5f));
		l_nameLabel->setPosition(ccp(184,359));
		addChild(l_nameLabel);*/
		CCLabelTTF* l_nameLabel = CCLabelTTF::create(npcInfo->npcName.c_str(),APP_FONT_NAME,22);
		l_nameLabel->setAnchorPoint(ccp(0.5,0.5f));
		l_nameLabel->setPosition(ccp(207,327));
		ccColor3B shadowColor = ccc3(0,0,0);   // black
		l_nameLabel->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		addChild(l_nameLabel);

		//richLabel_description
// 		CCLabelTTF * label_des = CCLabelTTF::create(npcInfo->welcome.c_str(),APP_FONT_NAME,18,CCSizeMake(285,0),kCCTextAlignmentLeft,kCCVerticalTextAlignmentTop);
// 		label_des->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
// 		CCSize desSize = label_des->getContentSize();
// 		int height = desSize.height;
		CCRichLabel *label_des = CCRichLabel::createWithString(npcInfo->welcome.c_str(),CCSizeMake(285, 0),this,NULL,0,18,5);
		CCSize desSize = label_des->getContentSize();
		int height = desSize.height;
		//ccscrollView_description
		CCScrollView *m_contentScrollView = CCScrollView::create(CCSizeMake(290,99));
		m_contentScrollView->setViewSize(CCSizeMake(290, 99));
		m_contentScrollView->ignoreAnchorPointForPosition(false);
		m_contentScrollView->setTouchEnabled(true);
		m_contentScrollView->setDirection(kCCScrollViewDirectionVertical);
		m_contentScrollView->setAnchorPoint(ccp(0,0));
		m_contentScrollView->setPosition(ccp(30,205));
		m_contentScrollView->setBounceable(true);
		m_contentScrollView->setClippingToBounds(true);
		m_pUiLayer->addChild(m_contentScrollView);
		if (height > 99)
		{
			m_contentScrollView->setContentSize(ccp(290,height));
			m_contentScrollView->setContentOffset(ccp(0,99-height));
		}
		else
		{
			m_contentScrollView->setContentSize(ccp(290,99));
		}
		m_contentScrollView->setClippingToBounds(true);

		m_contentScrollView->addChild(label_des);
		label_des->ignoreAnchorPointForPosition(false);
		label_des->setAnchorPoint(ccp(0,1));
		label_des->setPosition(ccp(3,m_contentScrollView->getContentSize().height-3));


		m_tableView = CCTableView::create(this,CCSizeMake(290,165));
		m_tableView->setPressedActionEnabled(true);
		m_tableView->setDirection(kCCScrollViewDirectionVertical);
		m_tableView->setAnchorPoint(ccp(0,0));
		m_tableView->setPosition(ccp(35,25));
		m_tableView->setDelegate(this);
		m_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		this->addChild(m_tableView);
		
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(CCSizeMake(350,420));

		return true;
	}
	return false;
}


void MissionTalkWithNpc::onEnter()
{
	UIScene::onEnter();

	CCFiniteTimeAction*  action = CCSequence::create(
		CCScaleTo::create(0.15f*1/2,1.1f),
		CCScaleTo::create(0.15f*1/3,1.0f),
		NULL);

	runAction(action);
}

void MissionTalkWithNpc::onExit()
{
	UIScene::onExit();
}


bool MissionTalkWithNpc::ccTouchBegan(CCTouch *touch, CCEvent * pEvent)
{
	//return this->resignFirstResponder(touch,this,false);
	CCPoint location = touch->getLocation();

	CCPoint pos = this->getPosition();
	CCSize size = this->getContentSize();
	CCPoint anchorPoint = this->getAnchorPointInPoints();
	pos = ccpSub(pos, anchorPoint);
	CCRect rect(pos.x, pos.y, size.width, size.height);
	if(rect.containsPoint(location))
	{
		return true;
	}

	return false;
}

void MissionTalkWithNpc::ccTouchEnded(CCTouch *touch, CCEvent * pEvent)
{
}

void MissionTalkWithNpc::ccTouchCancelled(CCTouch *touch, CCEvent * pEvent)
{
}

void MissionTalkWithNpc::ccTouchMoved(CCTouch *touch, CCEvent * pEvent)
{
}



CCSize MissionTalkWithNpc::tableCellSizeForIndex(CCTableView *table, unsigned int idx)
{
	return CCSizeMake(270,42);
}

CCTableViewCell* MissionTalkWithNpc::tableCellAtIndex(CCTableView *table, unsigned int idx)
{
	CCString *string = CCString::createWithFormat("%d", idx);
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell=new CCTableViewCell();
	cell->autorelease();

	CCScale9Sprite * pCellBg = CCScale9Sprite::create("res_ui/new_button_0.png");
	pCellBg->setContentSize(CCSizeMake(270,39));
	pCellBg->setCapInsets(CCRectMake(38,20,1,1));
	pCellBg->setAnchorPoint(ccp(0, 0));
	pCellBg->setPosition(ccp(0, 0));
	cell->addChild(pCellBg);

	//mission type
	std::string str_missionType;
	if (curFunctionNPC->npcMissionList.at(idx)->missionpackagetype() == 1)
	{
		str_missionType.append(StringDataManager::getString("mission_zhu"));
	}
	else if(curFunctionNPC->npcMissionList.at(idx)->missionpackagetype() == 2)
	{
		str_missionType.append(StringDataManager::getString("mission_zhi"));
	}
	else if(curFunctionNPC->npcMissionList.at(idx)->missionpackagetype() == 7)
	{
		str_missionType.append(StringDataManager::getString("mission_jia"));
	}
	else if(curFunctionNPC->npcMissionList.at(idx)->missionpackagetype() == 12)
	{
		str_missionType.append(StringDataManager::getString("mission_xuan"));
	}
	else
	{
		str_missionType.append(StringDataManager::getString("mission_ri"));
	}

	str_missionType.append(curFunctionNPC->npcMissionList.at(idx)->missionname());
	//CCLabelTTF * pMissionName = CCLabelTTF::create(curMissionInfo->description().c_str(),"Arial",16);
	CCLabelTTF * pMissionName = CCLabelTTF::create(str_missionType.c_str(),APP_FONT_NAME,16);
	pMissionName->setAnchorPoint(ccp(0.5f,0.5f));
	pMissionName->setPosition(ccp(pCellBg->getContentSize().width/2,pCellBg->getContentSize().height/2));
	ccColor3B shadowColor = ccc3(0,0,0);   // black
	pMissionName->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
	cell->addChild(pMissionName);

	return cell;

}


unsigned int MissionTalkWithNpc::numberOfCellsInTableView(CCTableView *table)
{
	return curFunctionNPC->npcMissionList.size();
}

void MissionTalkWithNpc::tableCellTouched(CCTableView* table, CCTableViewCell* cell)
{
	//关闭自己
	this->closeAnim();
	//弹出HandelMission
	GameView::getInstance()->missionManager->curMissionInfo->CopyFrom(*curFunctionNPC->npcMissionList.at(cell->getIdx()));
	//GameView::getInstance()->missionManager->addMission(curFunctionNPC->npcMissionList.at(cell->getIdx()));
	UIScene *mainScene = (UIScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene->getChildByTag(kTagHandleMission) == NULL)
	{
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagNpcTalkWindow))
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
			GameView::getInstance()->getMainUIScene()->getChildByTag(kTagNpcTalkWindow)->removeFromParent();
		}
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkWithNpc))
		{
			GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkWithNpc)->removeFromParent();
		}
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkMissionUI))
		{
			GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkMissionUI)->removeFromParent();
		}

		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		HandleMission * getMission = HandleMission::create(curFunctionNPC->npcMissionList.at(cell->getIdx()));
		mainScene->addChild(getMission,0,kTagHandleMission);
		getMission->ignoreAnchorPointForPosition(false);
		getMission->setAnchorPoint(ccp(0,0.5f));
		getMission->setPosition(ccp(0,winSize.height/2));
	}
}

void MissionTalkWithNpc::scrollViewDidScroll(CCScrollView* view )
{
}

void MissionTalkWithNpc::scrollViewDidZoom(CCScrollView* view )
{
}

void MissionTalkWithNpc::CloseEvent( CCObject * pSender )
{
	this->closeAnim();
}

int MissionTalkWithNpc::getCurNpcId()
{
	return curNpcId;
}



