#include "ExtraRewardsUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../backpackscene/PackageItem.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../messageclient/element/CMissionGoods.h"
#include "../../messageclient/element/CMissionReward.h"
#include "../extensions/CCRichLabel.h"
#include "AppMacros.h"
#include "../../gamescene_state/MainScene.h"


ExtraRewardsUI::ExtraRewardsUI()
{
}


ExtraRewardsUI::~ExtraRewardsUI()
{
}

ExtraRewardsUI* ExtraRewardsUI::create(CMissionReward * missionReward,std::string des)
{
	ExtraRewardsUI * extraRewardsUI = new ExtraRewardsUI();
	if (extraRewardsUI && extraRewardsUI->init(missionReward,des))
	{
		extraRewardsUI->autorelease();
		return extraRewardsUI;
	}
	CC_SAFE_DELETE(extraRewardsUI);
	return NULL;
}

bool ExtraRewardsUI::init(CMissionReward * missionReward,std::string des)
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		this->scheduleUpdate(); 
		//����UI
		if(LoadSceneLayer::ExtraRewardsLayer->getWidgetParent() != NULL)
		{
			LoadSceneLayer::ExtraRewardsLayer->removeFromParentAndCleanup(false);
		}
		UIPanel *ppanel = LoadSceneLayer::ExtraRewardsLayer;
		ppanel->setAnchorPoint(ccp(0.0f,0.0f));
		ppanel->getValidNode()->setContentSize(CCSizeMake(354, 226));
		ppanel->setPosition(CCPointZero);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnable(true);
		m_pUiLayer->addWidget(ppanel);

		UIButton * btn_close = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		btn_close->setTouchEnable(true);
		btn_close->addReleaseEvent(this, coco_releaseselector(ExtraRewardsUI::CloseEvent));
		btn_close->setPressedActionEnabled(true);

		UIButton * btn_enter = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_enter");
		btn_enter->setTouchEnable(true);
		btn_enter->addReleaseEvent(this, coco_releaseselector(ExtraRewardsUI::CloseEvent));
		btn_enter->setPressedActionEnabled(true);

		RichLabelDefinition def;
		def.strokeEnabled = false;
		def.fontSize = 22;
		CCRichLabel * l_des = CCRichLabel::create(des.c_str(),CCSizeMake(300,20),def);
		l_des->setAnchorPoint(ccp(0.5f,0.5f));
		l_des->setPosition(ccp(180,178));
		addChild(l_des);

		UIImageView * imageView_exp = (UIImageView *)UIHelper::seekWidgetByName(ppanel,"ImageView_exp");
		UIImageView * imageView_gold = (UIImageView *)UIHelper::seekWidgetByName(ppanel,"ImageView_gold");
		UILabel * l_exp = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_expValue");
		UILabel * l_gold = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_goldValue");

		if (missionReward->goods_size() <= 0)
		{
			imageView_exp->setVisible(true);
			imageView_gold->setVisible(true);
			l_exp->setVisible(true);
			l_gold->setVisible(true);

			char s_exp[20];
			sprintf(s_exp,"%d",missionReward->exp());
			l_exp->setText(s_exp);
			char s_gold[20];
			sprintf(s_gold,"%d",missionReward->gold());
			l_gold->setText(s_gold);
		}
		else
		{
			imageView_exp->setVisible(false);
			imageView_gold->setVisible(false);
			l_exp->setVisible(false);
			l_gold->setVisible(false);

			for(int i = 0;i<missionReward->goods_size();++i)
			{
				GoodsInfo * temp = new GoodsInfo();
				temp->CopyFrom(missionReward->goods(i).goods());
				PackageItem * packageItem = PackageItem::create(temp);
				packageItem->setAnchorPoint(ccp(0,0));
				packageItem->setPosition(ccp(200,80));
				m_pUiLayer->addWidget(packageItem);
				packageItem->setZOrder(20);
				std::string str_name = "rewardItem_";
				char s_index [5];
				sprintf(s_index,"%d",i);
				str_name.append(s_index);
				packageItem->setName(str_name.c_str());

				char s_num[20];
				sprintf(s_num,"%d",missionReward->goods(i).quantity());
				UILabel *Lable_num = UILabel::create();
				Lable_num->setText(s_num);
				Lable_num->setFontName(APP_FONT_NAME);
				Lable_num->setFontSize(13);
				Lable_num->setAnchorPoint(ccp(1.0f,0));
				Lable_num->setPosition(ccp(75,19));
				Lable_num->setName("Label_num");
				packageItem->addChild(Lable_num);
			}

			switch(missionReward->goods_size())
			{
			case 0 :
				{

				}
				break;
			case 1 :
				{
					std::string str_name = "rewardItem_1";
					PackageItem * packageItem = dynamic_cast<PackageItem*>(m_pUiLayer->getWidgetByName(str_name.c_str()));
					if (packageItem)
					{
						packageItem->setPosition(ccp(160,80));
					}
				}
				break;
			case 2 :
				{
					for(int i = 0;i<missionReward->goods_size();++i)
					{
						std::string str_name = "rewardItem_";
						char s_index [5];
						sprintf(s_index,"%d",i);
						str_name.append(s_index);
						PackageItem * packageItem = dynamic_cast<PackageItem*>(m_pUiLayer->getWidgetByName(str_name.c_str()));
						if (packageItem)
						{
							packageItem->setPosition(ccp(75+115*i,80));
						}
					}
				}
				break;
			case 3 :
				{
					for(int i = 0;i<missionReward->goods_size();++i)
					{
						std::string str_name = "rewardItem_";
						char s_index [5];
						sprintf(s_index,"%d",i);
						str_name.append(s_index);
						PackageItem * packageItem = dynamic_cast<PackageItem*>(m_pUiLayer->getWidgetByName(str_name.c_str()));
						if (packageItem)
						{
							packageItem->setPosition(ccp(45+90*i,80));
						}
					}
				}
				break;
			default:
				{

				}
			}
		}

		this->setContentSize(CCSizeMake(354,226));
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		return true;
	}
	return false;
}

void ExtraRewardsUI::update( float dt )
{

}

void ExtraRewardsUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void ExtraRewardsUI::onExit()
{
	UIScene::onExit();
}

bool ExtraRewardsUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return true;
}

void ExtraRewardsUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void ExtraRewardsUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void ExtraRewardsUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void ExtraRewardsUI::CloseEvent( CCObject *pSender )
{
	this->closeAnim();
}
