#include "MissionActions.h"

#include "MissionTalkWithNpc.h"
#include "HandleMission.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/BaseFighter.h"
#include "../../gamescene_state/role/MyPlayerOwnedCommand.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/role/MyPlayerAI.h"
#include "../../gamescene_state/role/MyPlayerSimpleAI.h"
#include "../../gamescene_state/role/BaseFighter.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/CMapInfo.h"
#include "../../messageclient/element/MissionInfo.h"
#include "AppMacros.h"
#include "../../ui/missionscene/MissionManager.h"
#include "../FivePersonInstance/FivePersonInstance.h"
#include "../FivePersonInstance/InstanceFunctionPanel.h"
#include "../FivePersonInstance/InstanceDetailUI.h"
#include "../../gamescene_state/role/PickingActor.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../utils/StaticDataManager.h"
#include "../../utils/pathfinder/AStarPathFinder.h"
#include "TalkMissionUI.h"
#include "../../legend_engine/GameWorld.h"
#include "../../gamescene_state/role/MyPlayerAIConfig.h"
#include "../../gamescene_state/sceneelement/GuideMap.h"
#include "MissionScene.h"
#include "../../ui/ChessBoard_ui/ChessBoardUI.h"
#include "../skillscene/SkillScene.h"
#include "../FivePersonInstance/InstanceMapUI.h"
#include "../generals_ui/GeneralsUI.h"
#include "../generals_ui/RecuriteActionItem.h"
#include "../generals_ui/GeneralsListUI.h"
#include "../generals_ui/GeneralsRecuriteUI.h"
#include "../../utils/GameUtils.h"
#include "../equipMent_ui/EquipMentUi.h"
#include "../Friend_ui/FriendUi.h"
#include "../Friend_ui/FriendWorkList.h"
#include "../Chat_ui/ChatUI.h"
#include "../extensions/UITab.h"
#include "../../ui/rewardTask_ui/RewardTaskMainUI.h"
#include "../searchgeneral/SearchGeneral.h"
#include "../../ui/Active_ui/WorldBoss_ui/WorldBossUI.h"
#include "../extensions/RecruitGeneralCard.h"
#include "../../gamescene_state/EffectDispatch.h"
#include "../../ui/rewardTask_ui/RewardTaskData.h"


MFailAction::MFailAction()
{
}


MFailAction::~MFailAction()
{
}

MFailAction * MFailAction::create(MissionInfo *missionInfo)
{
	MFailAction * mFailAction = new MFailAction();
	if (mFailAction)
	{
		mFailAction->init(missionInfo);
		return mFailAction;
	}
	CC_SAFE_DELETE(mFailAction);
	return false;
}

void MFailAction::init(MissionInfo *missionInfo)
{

}

////////////////////////////////////////////////////////////////////

MHandleViewAction::MHandleViewAction()
{
}

MHandleViewAction::~MHandleViewAction()
{
}

MHandleViewAction * MHandleViewAction::create(MissionInfo * missionInfo)
{
	MHandleViewAction * mHandleViewAction = new MHandleViewAction();
	if (mHandleViewAction)
	{
		mHandleViewAction->init(missionInfo);
		return mHandleViewAction;
	}
	CC_SAFE_DELETE(mHandleViewAction);
	return false;
}

void MHandleViewAction::init(MissionInfo * missionInfo)
{
	UIScene *mainScene = (UIScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene->getChildByTag(kTagHandleMission) == NULL)
	{
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagNpcTalkWindow))
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
			GameView::getInstance()->getMainUIScene()->getChildByTag(kTagNpcTalkWindow)->removeFromParent();
		}
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkWithNpc))
		{
			GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkWithNpc)->removeFromParent();
		}
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkMissionUI))
		{
			GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkMissionUI)->removeFromParent();
		}

		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		HandleMission * getMission = HandleMission::create(missionInfo);
		mainScene->addChild(getMission,0,kTagHandleMission);
		getMission->ignoreAnchorPointForPosition(false);
		getMission->setAnchorPoint(ccp(0,0.5f));
		getMission->setPosition(ccp(0,winSize.height/2));
	}
}

////////////////////////////////////////////////////////////////////

MMonsterKillAction::MMonsterKillAction()
{
}


MMonsterKillAction::~MMonsterKillAction()
{
}


MMonsterKillAction * MMonsterKillAction::create(MissionInfo *missionInfo)
{
	MMonsterKillAction * mMonsterKillAction = new MMonsterKillAction();
	if (mMonsterKillAction)
	{
		mMonsterKillAction->init(missionInfo);
		return mMonsterKillAction;
	}
	CC_SAFE_DELETE(mMonsterKillAction);
	return false;
}

void MMonsterKillAction::init(MissionInfo *missionInfo)
{
	// 清除原来的选中状态
	GameView::getInstance()->myplayer->setLockedActorId(NULL_ROLE_ID);
	// 清除红圈
	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene != NULL)
	{
		for (std::map<long long,GameActor*>::iterator it = GameView::getInstance()->getGameScene()->getActorsMap().begin(); it != GameView::getInstance()->getGameScene()->getActorsMap().end(); ++it)
		{
			GameActor* actor = it->second;
			BaseFighter* bf = dynamic_cast<BaseFighter*>(actor);
			if (bf != NULL)
				bf->removeDangerCircle();
		}
	}

	moveAction = MMoveAction::create(missionInfo);
}


void MMonsterKillAction::doMMonsterKillAction()
{
	CCLOG("doMMonsterKillAction");
	// setup attack command

// 	if(GameView::getInstance()->myplayer->selectEnemy(380))
// 	{
// 		BaseFighter* bf = dynamic_cast<BaseFighter*>(GameView::getInstance()->myplayer->getLockedActor());
// 		CCAssert(bf != NULL, "basefighter should not be null");
// 
// 		MyPlayerCommandAttack* cmd = new MyPlayerCommandAttack();
// 		cmd->skillId = GameView::getInstance()->myplayer->getDefaultSkill();
// 		cmd->skillProcessorId = cmd->skillId;
// 		cmd->sendAttackRequest = true;
// 		cmd->targetId = bf->getRoleId();
// 
// 		GameView::getInstance()->myplayer->getSimpleAI()->startKill(bf->getRoleId());
// 	
// 		GameView::getInstance()->myplayer->setNextCommand(cmd, true);
// 	}
	GameView::getInstance()->myplayer->getMyPlayerAI()->startFight();

	if (GameView::getInstance()->getGameScene())
	{
		if (GameView::getInstance()->getMainUIScene())
		{
			GuideMap * guide_ =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
			if (guide_)
			{
				if (guide_->getBtnHangUp())
				{
					guide_->getBtnHangUp()->setTextures("gamescene_state/zhujiemian3/tubiao/off_hook.png","gamescene_state/zhujiemian3/tubiao/off_hook.png","");
				}
			}
		}
	}
}

bool MMonsterKillAction::isFinishedMove()
{
	return moveAction->isFinishedMove();
}

////////////////////////////////////////////////////////////////////

MMoveAction::MMoveAction():
m_pListener(NULL),
m_pfnSelector(NULL),
distance(200)
{
	targetPos = ccp(0,0);
	targetMapId = "xsc.level";
}

MMoveAction::~MMoveAction()
{
	delete curMissionInfo;
}


MMoveAction * MMoveAction::create(MissionInfo *missionInfo)
{
	MMoveAction * missionMoveBase = new MMoveAction();
	if(missionMoveBase)
	{
		missionMoveBase->init(missionInfo);
		return missionMoveBase;
	}
	CC_SAFE_DELETE(missionMoveBase);
	return NULL;
}

void MMoveAction::init(MissionInfo *missionInfo)
{
	curMissionInfo = new MissionInfo();
	curMissionInfo->CopyFrom(*missionInfo);

	CCPoint pos = ccp(missionInfo->action().moveto().x(),missionInfo->action().moveto().y());
	targetMapId = missionInfo->action().moveto().mapid();
	targetActorId = missionInfo->action().moveto().targetnpc().npcid();
	if (pos.x == 0 && pos.y == 0)
	{
		targetPos = ccp(0,0);
	}
	else
	{
		float x = GameView::getInstance()->getGameScene()->tileToPositionX(pos.x);
		float y = GameView::getInstance()->getGameScene()->tileToPositionY(pos.y);
		targetPos = ccp(x,y);
	}

	GameView::getInstance()->missionManager->isAutoRunForMission = true;

	if (!isFinishedMove())
	{
		runToTarget();
	}
}


// void MMoveAction::update( float dt )
// {
// 	//检测是否寻路完成
// 	//如果完成,继续做任务
// 	if (isFinishedMove())
// 	{
// 		doFinishedMove();
// 	}
// }
// 
// void MMoveAction::doFinishedMove()
// {
// 	if (m_pListener && m_pfnSelector)
// 	{
// 		(m_pListener->*m_pfnSelector)();
// 	}
// }

// void MMoveAction::addFinishedMove( CCObject * pSender,SEL_FinishedMove selector )
// {
// 	m_pListener = pSender;
// 	m_pfnSelector = selector;
// }

bool MMoveAction::isFinishedMove()
{
	if (strcmp(GameView::getInstance()->getMapInfo()->mapid().c_str(),targetMapId.c_str()) == 0)
	{
		CCPoint cp_role = GameView::getInstance()->myplayer->getWorldPosition();
		CCPoint cp_target = targetPos;
		//CCLOG("%f",sqrt(pow((cp_role.x-cp_target.x),2)+pow((cp_role.y-cp_target.y),2)));
		if (ccpDistance(cp_role,cp_target) < 96)
		{
			GameView::getInstance()->myplayer->changeAction(ACT_STAND);
			if (GameView::getInstance()->getGameScene()->getActor(targetActorId) != NULL)
			{
				GameView::getInstance()->myplayer->setLockedActorId(targetActorId);
			}
			
			return true;
		}
		else 
		{
			return false;
		}
	}
	else
	{
		return false;
	}
	
}

//移动到目的地
void MMoveAction::runToTarget()
{
	MyPlayerAIConfig::setAutomaticSkill(0);
	MyPlayerAIConfig::enableRobot(false);
	GameView::getInstance()->myplayer->getMyPlayerAI()->stop();
	if (GameView::getInstance()->getGameScene())
	{
		if (GameView::getInstance()->getMainUIScene())
		{
			GuideMap * guide_ =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
			if (guide_)
			{
				guide_->setAllBtnHangUpTextures("gamescene_state/zhujiemian3/tubiao/on_hook.png");
			}
		}
	}

	GameView::getInstance()->myplayer->getSimpleAI()->stopAll();

	//正在PK时不允许传送，只能跑路
	if(GameView::getInstance()->myplayer->isPKStatus())
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("mission_canNottransport"));
	}

	if (GameView::getInstance()->myplayer->isPKStatus() || curMissionInfo->cantfly())
	{
		if (targetPos.x != 0 && targetPos.y != 0)
		{
			if (MissionManager::getInstance()->CheckIsEnableToRunToTarget(targetMapId.c_str(),targetPos))
			{
				//same map
				if (strcmp(GameView::getInstance()->getMapInfo()->mapid().c_str(),targetMapId.c_str()) == 0)
				{
					CCPoint cp_role = GameView::getInstance()->myplayer->getWorldPosition();
					CCPoint cp_target = targetPos;
					MyPlayerCommandMove* cmd = new MyPlayerCommandMove();
					// NOT reachable, so find the near one
					short tileX = GameView::getInstance()->getGameScene()->positionToTileX(targetPos.x);
					short tileY = GameView::getInstance()->getGameScene()->positionToTileY(targetPos.y);
					if (GameView::getInstance()->getGameScene())
					{
						if(GameView::getInstance()->getGameScene()->isLimitOnGround(tileX, tileY))
						{
							targetPos = GameView::getInstance()->getGameScene()->getNearReachablePoint(targetPos);
						}
					}
					cmd->targetPosition = targetPos;
					cmd->method = MyPlayerCommandMove::method_searchpath;
					bool bSwitchCommandImmediately = true;
					if(GameView::getInstance()->myplayer->isAttacking())
						bSwitchCommandImmediately = false;
					GameView::getInstance()->myplayer->setNextCommand(cmd, bSwitchCommandImmediately);	
				}
				else
				{
					MyPlayerCommandMove* cmd = new MyPlayerCommandMove();
					cmd->targetMapId = targetMapId;
					short tileX = GameView::getInstance()->getGameScene()->positionToTileX(targetPos.x);
					short tileY = GameView::getInstance()->getGameScene()->positionToTileY(targetPos.y);
					cmd->targetPosition = targetPos;
					cmd->method = MyPlayerCommandMove::method_searchpath;
					bool bSwitchCommandImmediately = true;
					if(GameView::getInstance()->myplayer->isAttacking())
						bSwitchCommandImmediately = false;
					GameView::getInstance()->myplayer->setNextCommand(cmd, bSwitchCommandImmediately);	
				}
			}
			else
			{
				GameView::getInstance()->showAlertDialog(StringDataManager::getString("mission_canNotAutoSearchPathNow"));
			}
		}
	}
	else
	{
		if (strcmp(GameView::getInstance()->getMapInfo()->mapid().c_str(),targetMapId.c_str()) == 0)
		{
			//same map
			CCPoint cp_role = GameView::getInstance()->myplayer->getWorldPosition();
			CCPoint cp_target = targetPos;
			if (ccpDistance(cp_role,cp_target) < 64)
			{
				return;
			}
			else
			{
				if (targetPos.x == 0 && targetPos.y == 0)
				{
					return;
				}

				MyPlayerCommandMove* cmd = new MyPlayerCommandMove();
				// NOT reachable, so find the near one
				short tileX = GameView::getInstance()->getGameScene()->positionToTileX(targetPos.x);
				short tileY = GameView::getInstance()->getGameScene()->positionToTileY(targetPos.y);
				if (GameView::getInstance()->getGameScene())
				{
					if(GameView::getInstance()->getGameScene()->isLimitOnGround(tileX, tileY))
					{
						targetPos = GameView::getInstance()->getGameScene()->getNearReachablePoint(targetPos);
					}
				}
				cmd->targetPosition = targetPos;
				cmd->method = MyPlayerCommandMove::method_searchpath;
				bool bSwitchCommandImmediately = true;
				if(GameView::getInstance()->myplayer->isAttacking())
					bSwitchCommandImmediately = false;
				GameView::getInstance()->myplayer->setNextCommand(cmd, bSwitchCommandImmediately);	
			}
		}
		else
		{
			if (targetPos.x == 0 && targetPos.y == 0)   //没有任务目标点的直接寻路过去
			{
				MyPlayerCommandMove* cmd = new MyPlayerCommandMove();
				cmd->targetMapId = targetMapId;
				cmd->method = MyPlayerCommandMove::method_searchpath;
				bool bSwitchCommandImmediately = true;
				if(GameView::getInstance()->myplayer->isAttacking())
					bSwitchCommandImmediately = false;
				GameView::getInstance()->myplayer->setNextCommand(cmd, bSwitchCommandImmediately);	
			}
			else
			{
				GameView::getInstance()->missionManager->AcrossMapTransport(targetMapId,targetPos);
			}
		}
	}
}

float MMoveAction::getDistance()
{
	return distance;
}

bool MMoveAction::isEnableFindPath()
{
	//first check isEnable to run to target
	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if (!scene)
		return false;

	std::string currentMapId = GameView::getInstance()->getMapInfo()->mapid();
	// no cross map
	if(targetMapId == currentMapId)
	{
		// start point
		short start[2] = {0, 0};   // tileY, tileX
		short tileX = scene->positionToTileX(GameView::getInstance()->myplayer->getWorldPosition().x);
		short tileY = scene->positionToTileY(GameView::getInstance()->myplayer->getWorldPosition().y);
		start[0] = tileY;
		start[1] = tileX;
		// out of map, ignore
		if(!scene->checkInMap(tileX, tileY))
		{
			return false;
		}

		// end point
		short end[2] = {0, 0};
		tileX = scene->positionToTileX(targetPos.x);
		tileY = scene->positionToTileY(targetPos.y);
		end[0] = tileY;
		end[1] = tileX;
		// out of map, ignore
		if(!scene->checkInMap(tileX, tileY))
		{
			return false;
		}

		std::vector<short*> *path = scene->getPathFinder()->searchPath(start, end);
		if(path != NULL)
		{
			delete path;
		}
		else
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("mission_canNotAutoSearchPathNow"));
			return false;
		}
	}
	// cross map
	else if(targetMapId != currentMapId)
	{
		std::vector<std::string>* crossMapPaths = GameWorld::searchLevelPath(currentMapId, targetMapId);
		if(crossMapPaths == NULL)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("mission_canNotAutoSearchPathNow"));
			return false;
		}
		else
		{
			CC_SAFE_DELETE(crossMapPaths);
		}

	}

	return true;
}

////////////////////////////////////////////////////////////////////

MOpenViewAction::MOpenViewAction()
{
}


MOpenViewAction::~MOpenViewAction()
{
}


MOpenViewAction * MOpenViewAction::create(MissionInfo * missionInfo)
{
	MOpenViewAction * mOpenViewAction = new MOpenViewAction();
	if (mOpenViewAction)
	{
		mOpenViewAction->init(missionInfo);
		return mOpenViewAction;
	}
	CC_SAFE_DELETE(mOpenViewAction);
	return false;
}

void MOpenViewAction::init(MissionInfo * missionInfo)
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	if (GameView::getInstance()->getMainUIScene())
	{
		switch(missionInfo->action().specifiedui().id())
		{
		case kTagInstanceDetailUI : 
			{
				MainScene* mainUILayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
				CCAssert(mainUILayer != NULL, "should not be nil");

				if (missionInfo->action().specifiedui().para() == FivePersonInstance::getCurrentInstanceId()  && FivePersonInstance::getStatus() == FivePersonInstance::status_in_instance)   //判断当前是否已在该副本中
				{
					if (missionInfo->has_tip())
					{
						GameView::getInstance()->showAlertDialog(missionInfo->tip().dialog().c_str());
					}
				}
				else
				{
					if (mainUILayer->getChildByTag(kTagInstanceMapUI) == NULL)
					{
						InstanceMapUI * _ui = InstanceMapUI::create();
						_ui->ignoreAnchorPointForPosition(false);
						_ui->setAnchorPoint(ccp(0.5f,0.5f));
						_ui->setPosition(ccp(winSize.width/2,winSize.height/2));
						_ui->setTag(kTagInstanceMapUI);
						mainUILayer->addChild(_ui);

						_ui->OpenDetailUI(missionInfo->action().specifiedui().para());
					}
				}
			}
			break;
		case kTagHandleMission : 
			{
				MainScene* mainUILayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
				CCAssert(mainUILayer != NULL, "should not be nil");

				if (mainUILayer->getChildByTag(kTagHandleMission) == NULL)
				{
					if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagNpcTalkWindow))
					{
						GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
						GameView::getInstance()->getMainUIScene()->getChildByTag(kTagNpcTalkWindow)->removeFromParent();
					}
					if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkWithNpc))
					{
						GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkWithNpc)->removeFromParent();
					}
					if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkMissionUI))
					{
						GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkMissionUI)->removeFromParent();
					}

					CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
					HandleMission * getMission = HandleMission::create(missionInfo);
					mainUILayer->addChild(getMission,0,kTagHandleMission);
					getMission->ignoreAnchorPointForPosition(false);
					getMission->setAnchorPoint(ccp(0,0.5f));
					getMission->setPosition(ccp(0,winSize.height/2));
				}
			}
			break;
		case kTagMissionScene : 
			{
				MainScene* mainUILayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
				CCAssert(mainUILayer != NULL, "should not be nil");

				if (mainUILayer->getChildByTag(kTagMissionScene) == NULL)
				{
					MissionScene * missionScene = MissionScene::create();
					mainUILayer->addChild(missionScene,0,kTagMissionScene);
					missionScene->ignoreAnchorPointForPosition(false);
					missionScene->setAnchorPoint(ccp(0,0.5f));
					missionScene->setPosition(ccp(0,winSize.height/2));
				}
			}
			break;
		case kTagTeachRemindSkill:
			{
				EffectStudySkill * skillEffect_ = new EffectStudySkill();
				skillEffect_->setType(type_skillStudy);
				EffectDispatcher::pushEffectToVector(skillEffect_);
			}
			break;
		case kTagTeachRemindGeneral:
			{
				EffectRecruit * recruitGeneral_ = new EffectRecruit();
				recruitGeneral_->setType(type_recruit);
				EffectDispatcher::pushEffectToVector(recruitGeneral_);
			}
			break;
		case kTagChessBoardUI : 
			{
				int nDifficulty = missionInfo->action().specifiedui().para();

				CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
				ChessBoardUI* pTmpChessBoardUI = (ChessBoardUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagChessBoardUI);
				if (NULL == pTmpChessBoardUI)
				{
					ChessBoardUI * pChessBoardUI = ChessBoardUI::create();
					pChessBoardUI->ignoreAnchorPointForPosition(false);
					pChessBoardUI->setAnchorPoint(ccp(0.5f, 0.5f));
					pChessBoardUI->setPosition(ccp(winSize.width / 2, winSize.height / 2));
					pChessBoardUI->setTag(kTagChessBoardUI);
					pChessBoardUI->set_difficulty(nDifficulty);
					pChessBoardUI->initChessBoardData();			// 先设置难度，再调用此接口初始化棋盘

					GameView::getInstance()->getMainUIScene()->addChild(pChessBoardUI);
				}
			}
			break;
		case KTagSkillScene : 
			{
				MainScene* mainUILayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
				CCAssert(mainUILayer != NULL, "should not be nil");

				if (mainUILayer->getChildByTag(KTagSkillScene) == NULL)
				{
					SkillScene * skillScene = SkillScene::create();
					mainUILayer->addChild(skillScene,0,KTagSkillScene);
					skillScene->ignoreAnchorPointForPosition(false);
					skillScene->setAnchorPoint(ccp(0,0.5f));
					skillScene->setPosition(ccp(0,winSize.height/2));
				}
			}
			break;
		case kTagGeneralsUI : 
			{
				MainScene* mainUILayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
				CCAssert(mainUILayer != NULL, "should not be nil");

				if (mainUILayer->getChildByTag(kTagGeneralsUI) == NULL)
				{
					GeneralsUI * generalsUI = (GeneralsUI *)MainScene::GeneralsScene;
					mainUILayer->addChild(generalsUI,0,kTagGeneralsUI);
					generalsUI->ignoreAnchorPointForPosition(false);
					generalsUI->setAnchorPoint(ccp(0.5f, 0.5f));
					generalsUI->setPosition(ccp(winSize.width/2, winSize.height/2));

					//generalsUI->setToDefaultTab();
					generalsUI->setToTabByIndex(1);
					//招募
					for (int i = 0;i<3;++i)
					{
						if (GeneralsUI::generalsRecuriteUI->Layer_recruit->getChildByTag(50+i))
						{
							RecuriteActionItem * tempRecurite = ( RecuriteActionItem *)GeneralsUI::generalsRecuriteUI->Layer_recruit->getChildByTag(50+i);
							tempRecurite->cdTime -= GameUtils::millisecondNow() - tempRecurite->lastChangeTime;
							/*
							if (i == 0)
							{
								CCLOG("MainScene:tempRecurite->cdTime = %ld",tempRecurite->cdTime);
							}
							*/
						}
					}
					//武将列表 
					GeneralsUI::generalsListUI->isReqNewly = true;
					GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
					temp->page = 0;
					temp->pageSize = generalsUI->generalsListUI->everyPageNum;
					temp->type = 1;
					GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
					delete temp;
					//技能列表
					GameMessageProcessor::sharedMsgProcessor()->sendReq(5055, this);
					//阵法列表
					GameMessageProcessor::sharedMsgProcessor()->sendReq(5071, this);
					//已上阵武将列表 
					GeneralsUI::ReqListParameter * temp1 = new GeneralsUI::ReqListParameter();
					temp1->page = 0;
					temp1->pageSize = generalsUI->generalsListUI->everyPageNum;
					temp1->type = 5;
					GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp1);
					delete temp1;
					//mainscene->checkIsNewRemind();
					mainUILayer->remindOfGeneral();
				}
			}
			break;
		case ktagEquipMentUI : 
			{
				MainScene* mainUILayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
				CCAssert(mainUILayer != NULL, "should not be nil");

				if (mainUILayer->getChildByTag(ktagEquipMentUI) == NULL)
				{
					EquipMentUi * equip=EquipMentUi::create();
					equip->ignoreAnchorPointForPosition(false);
					equip->setAnchorPoint(ccp(0.5f,0.5f));
					equip->setPosition(ccp(winSize.width/2,winSize.height/2));
					GameView::getInstance()->getMainUIScene()->addChild(equip,0,ktagEquipMentUI);
					equip->refreshBackPack(0,-1);

					equip->equipTab->setDefaultPanelByIndex(missionInfo->action().specifiedui().para());
					equip->callBackChangeTab(equip->equipTab);
				}
			}
			break;
		case kTagFriendUi : 
			{
				MainScene* mainUILayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
				CCAssert(mainUILayer != NULL, "should not be nil");

				if (mainUILayer->getChildByTag(kTagFriendUi) == NULL)
				{
					FriendUi * friendui=FriendUi::create();
					friendui->ignoreAnchorPointForPosition(false);
					friendui->setAnchorPoint(ccp(0.5f,0.5f));
					friendui->setPosition(ccp(winSize.width/2,winSize.height/2));
					mainUILayer->addChild(friendui,0,kTagFriendUi);

					FriendWorkList::FriendStruct friend1={0,20,0,0};
					GameMessageProcessor::sharedMsgProcessor()->sendReq(2202,&friend1);

					mainUILayer->remindGetFriendPhypower();
				}
			}
			break;
		case kTabChat : 
			{
				MainScene* mainUILayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
				CCAssert(mainUILayer != NULL, "should not be nil");

				if (mainUILayer->getChildByTag(kTabChat) == NULL)
				{
					ChatUI * chat_ui = ChatUI::create();
					chat_ui->ignoreAnchorPointForPosition(false);
					chat_ui->setAnchorPoint(ccp(0.5f, 0.5f));
					chat_ui->setPosition(ccp(winSize.width/2,winSize.height/2));
					mainUILayer->addChild(chat_ui,0,kTabChat);
				}
			}
			break;

		case kTagSearchGeneralUI:
			{
				MainScene* mainUILayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
				CCAssert(mainUILayer != NULL, "should not be nil");

				int nDifficulty = missionInfo->action().specifiedui().para();

				SearchGeneral * search_ = SearchGeneral::create(nDifficulty);
				search_->ignoreAnchorPointForPosition(false);
				search_->setAnchorPoint(ccp(0.5f, 0.5f));
				search_->setPosition(ccp(winSize.width/2,winSize.height/2));
				mainUILayer->addChild(search_);
			}break;
		case kTagWorldBossUI:
			{
				MainScene* mainUILayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
				CCAssert(mainUILayer != NULL, "should not be nil");

				CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
				CCLayer *worldBossUI = (CCLayer*)mainUILayer->getChildByTag(kTagWorldBossUI);
				if(worldBossUI == NULL)
				{
					worldBossUI = WorldBossUI::create();
					GameView::getInstance()->getMainUIScene()->addChild(worldBossUI,0,kTagWorldBossUI);
					worldBossUI->ignoreAnchorPointForPosition(false);
					worldBossUI->setAnchorPoint(ccp(0.5f, 0.5f));
					worldBossUI->setPosition(ccp(winSize.width/2, winSize.height/2));

					GameMessageProcessor::sharedMsgProcessor()->sendReq(5301);
				}
			}break;
		}
	}
}
////////////////////////////////////////////////////////////////////

MPickAction::MPickAction()
{
}


MPickAction::~MPickAction()
{
}


MPickAction * MPickAction::create(MissionInfo * missionInfo)
{
	MPickAction * mPickAction = new MPickAction();
	if (mPickAction)
	{
		mPickAction->init(missionInfo);
		return mPickAction;
	}
	CC_SAFE_DELETE(mPickAction);
	return false;
}

void MPickAction::init(MissionInfo * missionInfo)
{
// 	std::string mapid = missionInfo->action().moveto().mapid();
// 	CCPoint pos = ccp(missionInfo->action().moveto().x(),missionInfo->action().moveto().y());
// 	moveAction = MMoveAction::create(mapid.c_str(),pos);
	moveAction = MMoveAction::create(missionInfo);
}


void MPickAction::doPickAction()
{
	CCLOG("doPickAction");
	if(GameView::getInstance()->myplayer->selectPickActor(380))
	{
		PickingActor* actor = dynamic_cast<PickingActor*>(GameView::getInstance()->myplayer->getLockedActor());
		CCAssert(actor != NULL, "PickingActor should not be null");

		if (actor->getActivied())
		{
			GameView::getInstance()->myplayer->setLockedActorId(actor->getRoleId());

			MyPlayerCommandPick* cmd = new MyPlayerCommandPick();
			//cmd->pickingMaxNumber = 1000;
			cmd->targetId = actor->getRoleId();
			GameView::getInstance()->myplayer->setNextCommand(cmd, true);
		}
		GameView::getInstance()->myplayer->getSimpleAI()->startPick(actor->getRoleId());
	}
	
}

bool MPickAction::isFinishedMove()
{
	return moveAction->isFinishedMove();
}

////////////////////////////////////////////////////////////////////

MTalkWithNpcAction::MTalkWithNpcAction():
isfirst(true)
{
}


MTalkWithNpcAction::~MTalkWithNpcAction()
{
	delete curMissionInfo;
}


MTalkWithNpcAction * MTalkWithNpcAction::create(MissionInfo *missionInfo)
{
	MTalkWithNpcAction * mTalkWithNpcAction = new MTalkWithNpcAction();
	if (mTalkWithNpcAction)
	{
		mTalkWithNpcAction->init(missionInfo);
		return mTalkWithNpcAction;
	}
	CC_SAFE_DELETE(mTalkWithNpcAction);
	return false;
}

void MTalkWithNpcAction::init(MissionInfo *missionInfo)
{
	curMissionInfo = new MissionInfo();
	curMissionInfo->CopyFrom(*missionInfo);
// 	std::string mapid = missionInfo->action().moveto().mapid();
// 	CCPoint pos = ccp(missionInfo->action().moveto().x(),missionInfo->action().moveto().y());
// 	moveAction = MMoveAction::create(mapid.c_str(),pos);
	moveAction = MMoveAction::create(curMissionInfo);
}


void MTalkWithNpcAction::doMTalkWithNpcAction()
{
	CCLOG("doMTalkWithNpcAction");
	//弹出任务人对话窗口.
	if (isfirst)
	{
		UIScene *mainScene = (UIScene*)GameView::getInstance()->getMainUIScene();
		if (mainScene->getChildByTag(kTagHandleMission) == NULL)
		{
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagNpcTalkWindow))
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				GameView::getInstance()->getMainUIScene()->getChildByTag(kTagNpcTalkWindow)->removeFromParent();
			}
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkWithNpc))
			{
				GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkWithNpc)->removeFromParent();
			}
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkMissionUI))
			{
				GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkMissionUI)->removeFromParent();
			}

			CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
			HandleMission * handleMission = HandleMission::create(curMissionInfo);
			mainScene->addChild(handleMission,0,kTagHandleMission);
			handleMission->ignoreAnchorPointForPosition(false);
			handleMission->setAnchorPoint(ccp(0,0.5f));
			handleMission->setPosition(ccp(0,winSize.height/2));
			isfirst = false;
		}
	}
}

bool MTalkWithNpcAction::isFinishedMove()
{
	return moveAction->isFinishedMove();
}


////////////////////////////////////////////////////////////////////

MOpenNpcFunction::MOpenNpcFunction():
isfirst(true)
{
}


MOpenNpcFunction::~MOpenNpcFunction()
{
	delete curMissionInfo;
}


MOpenNpcFunction * MOpenNpcFunction::create(MissionInfo *missionInfo)
{
	MOpenNpcFunction * mOpenNpcFunction = new MOpenNpcFunction();
	if (mOpenNpcFunction)
	{
		mOpenNpcFunction->init(missionInfo);
		return mOpenNpcFunction;
	}
	CC_SAFE_DELETE(mOpenNpcFunction);
	return false;
}

void MOpenNpcFunction::init(MissionInfo *missionInfo)
{
	curMissionInfo = new MissionInfo();
	curMissionInfo->CopyFrom(*missionInfo);
	// 	std::string mapid = missionInfo->action().moveto().mapid();
	// 	CCPoint pos = ccp(missionInfo->action().moveto().x(),missionInfo->action().moveto().y());
	// 	moveAction = MMoveAction::create(mapid.c_str(),pos);
	moveAction = MMoveAction::create(curMissionInfo);
}


void MOpenNpcFunction::doMOpenNpcFunction()
{
	CCLOG("doMOpenNpcFunction");
	//弹出NPC功能面板.
	if (isfirst)
	{
		UIScene *mainScene = (UIScene*)GameView::getInstance()->getMainUIScene();
		if (mainScene)
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1129, (void *)curMissionInfo->action().moveto().targetnpc().npcid());
		}
	}
}

bool MOpenNpcFunction::isFinishedMove()
{
	return moveAction->isFinishedMove();
}


////////////////////////////////////////////////////////////////////

MTalkMission::MTalkMission():
isfirst(true)
{
}


MTalkMission::~MTalkMission()
{
	delete curMissionInfo;
}


MTalkMission * MTalkMission::create(MissionInfo *missionInfo)
{
	MTalkMission * mTalkMission = new MTalkMission();
	if (mTalkMission)
	{
		mTalkMission->init(missionInfo);
		return mTalkMission;
	}
	CC_SAFE_DELETE(mTalkMission);
	return false;
}

void MTalkMission::init(MissionInfo *missionInfo)
{
	curMissionInfo = new MissionInfo();
	curMissionInfo->CopyFrom(*missionInfo);
	// 	std::string mapid = missionInfo->action().moveto().mapid();
	// 	CCPoint pos = ccp(missionInfo->action().moveto().x(),missionInfo->action().moveto().y());
	// 	moveAction = MMoveAction::create(mapid.c_str(),pos);
	moveAction = MMoveAction::create(curMissionInfo);
}


void MTalkMission::doMTalkMission()
{
	CCLOG("MTalkMission");
	//弹出对话面板
	if (isfirst)
	{
		UIScene *mainScene = (UIScene*)GameView::getInstance()->getMainUIScene();
		if (mainScene)
		{
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagNpcTalkWindow))
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				GameView::getInstance()->getMainUIScene()->getChildByTag(kTagNpcTalkWindow)->removeFromParent();
			}
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkWithNpc))
			{
				GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkWithNpc)->removeFromParent();
			}
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHandleMission))
			{
				GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHandleMission)->removeFromParent();
			}
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkMissionUI))
			{
				GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkMissionUI)->removeFromParent();
			}

			CCSize winSize = CCDirector::sharedDirector()->getVisibleSize(); 
			TalkMissionUI * talkMissionUI = TalkMissionUI::create(curMissionInfo);
			mainScene->addChild(talkMissionUI,0,kTagTalkMissionUI);
			talkMissionUI->ignoreAnchorPointForPosition(false);
			talkMissionUI->setAnchorPoint(ccp(0,0.5f));
			talkMissionUI->setPosition(ccp(0,winSize.height/2));
			isfirst = false;
		}
	}
}

bool MTalkMission::isFinishedMove()
{
	return moveAction->isFinishedMove();
}




////////////////////////////////////////////////////////////////////

MMoveToTargetAndOpenUI::MMoveToTargetAndOpenUI():
isfirst(true)
{
}

MMoveToTargetAndOpenUI::~MMoveToTargetAndOpenUI()
{
	delete curMissionInfo;
}


MMoveToTargetAndOpenUI * MMoveToTargetAndOpenUI::create(MissionInfo *missionInfo)
{
	MMoveToTargetAndOpenUI * mOpenUIMission = new MMoveToTargetAndOpenUI();
	if (mOpenUIMission)
	{
		mOpenUIMission->init(missionInfo);
		return mOpenUIMission;
	}
	CC_SAFE_DELETE(mOpenUIMission);
	return false;
}

void MMoveToTargetAndOpenUI::init(MissionInfo *missionInfo)
{
	curMissionInfo = new MissionInfo();
	curMissionInfo->CopyFrom(*missionInfo);
	moveAction = MMoveAction::create(curMissionInfo);
}


void MMoveToTargetAndOpenUI::doMMoveToTargetAndOpenUI()
{
	CCLOG("MMoveToTargetAndOpenUI");
	//弹出界面
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	if (GameView::getInstance()->getMainUIScene())
	{
		switch(curMissionInfo->action().specifiedui().id())
		{
		case kTagRewardTaskMainUI : 
			{
				MainScene* mainUILayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
				CCAssert(mainUILayer != NULL, "should not be nil");

				if (curMissionInfo->action().has_moveto())
				{
					if (curMissionInfo->action().moveto().has_targetnpc())
					{
						RewardTaskData::getInstance()->setTargetNpcId(curMissionInfo->action().moveto().targetnpc().npcid());
					}
				}
				RewardTaskMainUI * rewardTaskMainUI = (RewardTaskMainUI*)mainUILayer->getChildByTag(kTagRewardTaskMainUI);
				if (!rewardTaskMainUI)
				{
					rewardTaskMainUI = RewardTaskMainUI::create();
					rewardTaskMainUI->ignoreAnchorPointForPosition(false);
					rewardTaskMainUI->setAnchorPoint(ccp(0.5f,0.5f));
					rewardTaskMainUI->setPosition(ccp(winSize.width/2,winSize.height/2));
					rewardTaskMainUI->setTag(kTagRewardTaskMainUI);
					mainUILayer->addChild(rewardTaskMainUI);
					GameMessageProcessor::sharedMsgProcessor()->sendReq(7014);
				}
			}
			break;
		}
	}
}

bool MMoveToTargetAndOpenUI::isFinishedMove()
{
	return moveAction->isFinishedMove();
}


