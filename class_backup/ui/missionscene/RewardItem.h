
#ifndef _MISSIONSCENE_REWARDITEM_
#define _MISSIONSCENE_REWARDITEM_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class GoodsInfo;

class RewardItem : public UIScene
{
public:
	RewardItem();
	~RewardItem();

	static RewardItem * create(GoodsInfo* goods,int num);
	bool init(GoodsInfo* goods,int num);

public:
	CCLabelTTF * label_rewardName;
	CCLabelTTF * label_rewardNum;
};

#endif

