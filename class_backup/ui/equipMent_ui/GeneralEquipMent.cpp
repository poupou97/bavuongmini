#include "GeneralEquipMent.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "EquipMentUi.h"
#include "../backpackscene/PackageItemInfo.h"
#include "../../messageclient/element/CEquipment.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/Script.h"
#include "AppMacros.h"
#include "../../legend_script/CCTeachingGuide.h"
#include "../../gamescene_state/role/MyPlayer.h"

GeneralEquipMent::GeneralEquipMent(void)
{
}

GeneralEquipMent::~GeneralEquipMent(void)
{
	delete generalEquipment;
}

GeneralEquipMent * GeneralEquipMent::create(CEquipment * equipment)
{
	GeneralEquipMent *generalEquip =new GeneralEquipMent();
	if (generalEquip &&generalEquip->init(equipment))
	{
		generalEquip->autorelease();
		return generalEquip;
	}
	CC_SAFE_DELETE(generalEquip);
	return NULL;
}

bool GeneralEquipMent::init(CEquipment * equipment)
{
	if (UIScene::init())
	{

		EquipMentUi * equipui =(EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);

		std::string frameColorPath = "res_ui/";
		if (equipment->goods().equipmentdetail().quality() == 1)
		{
			frameColorPath.append("sdi_white");
		}
		else if (equipment->goods().equipmentdetail().quality() == 2)
		{
			frameColorPath.append("sdi_green");
		}
		else if (equipment->goods().equipmentdetail().quality() == 3)
		{
			frameColorPath.append("sdi_bule");
		}
		else if (equipment->goods().equipmentdetail().quality() == 4)
		{
			frameColorPath.append("sdi_purple");
		}
		else if (equipment->goods().equipmentdetail().quality() == 5)
		{
			frameColorPath.append("sdi_orange");
		}
		else
		{
			frameColorPath.append("sdi_white");
		}
		frameColorPath.append(".png");

		Btn_pacItemFrame = UIButton::create();
		Btn_pacItemFrame->setTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
		Btn_pacItemFrame->setTouchEnable(true);
		Btn_pacItemFrame->setPressedActionEnabled(true);
		Btn_pacItemFrame->setAnchorPoint(ccp(0.5f,0.5f));
		Btn_pacItemFrame->setPosition(ccp(0,0));
		Btn_pacItemFrame->setTag(equipment->part());
		Btn_pacItemFrame->addReleaseEvent(this,coco_releaseselector(GeneralEquipMent::showEquipMentInfo));
		m_pUiLayer->addWidget(Btn_pacItemFrame);

		std::string goodsInfoStr ="res_ui/props_icon/";
		goodsInfoStr.append(equipment->goods().icon());
		goodsInfoStr.append(".png");

		UIImageView *equipIcon_ =UIImageView::create();
		equipIcon_->setTexture(goodsInfoStr.c_str());
		equipIcon_->setAnchorPoint(ccp(0.5f,0.5f));
		equipIcon_->setPosition(ccp(0,0));
		Btn_pacItemFrame->addChild(equipIcon_);

		if (equipment->goods().binding() == 1)
		{
			UIImageView *ImageView_bound = UIImageView::create();
			ImageView_bound->setTexture("res_ui/binding.png");
			ImageView_bound->setAnchorPoint(ccp(0,1.0f));
			ImageView_bound->setScale(0.85f);
			ImageView_bound->setPosition(ccp(-25,23));
			Btn_pacItemFrame->addChild(ImageView_bound);
		}


		int starLv_ =equipment->goods().equipmentdetail().starlevel();
		if (starLv_>0)
		{
			char starLvLabel_[5];
			sprintf(starLvLabel_,"%d",starLv_);
			UILabel * label_ =UILabel::create();
			label_->setAnchorPoint(ccp(0,0));
			label_->setPosition(ccp(-23,-23));
			label_->setFontSize(13);
			label_->setText(starLvLabel_);
			Btn_pacItemFrame->addChild(label_);

			UIImageView * imageStar_ =UIImageView::create();
			imageStar_->setTexture("res_ui/star_on.png");
			imageStar_->setScale(0.7f);
			imageStar_->setAnchorPoint(ccp(0,0));
			imageStar_->setPosition(ccp(label_->getContentSize().width-22,label_->getPosition().y));
			Btn_pacItemFrame->addChild(imageStar_);
		}

		int strengthLv_ = equipment->goods().equipmentdetail().gradelevel();
		if (strengthLv_>0)
		{
			char strengthLv[5];
			sprintf(strengthLv,"%d",strengthLv_);
			std::string strengthStr_ = "+";
			strengthStr_.append(strengthLv);

			UILabel * label_StrengthLv = UILabel::create();
			label_StrengthLv->setAnchorPoint(ccp(1,1));
			label_StrengthLv->setText(strengthStr_.c_str());
			label_StrengthLv->setPosition(ccp(23,25));
			label_StrengthLv->setFontSize(13);
			Btn_pacItemFrame->addChild(label_StrengthLv);
		}
	
		if (equipment->goods().equipmentdetail().durable()<=10)
		{
			UIImageView * image_sp = UIImageView::create();
			image_sp->setAnchorPoint(ccp(0.5,0.5f));
			image_sp->setPosition(ccp(0,0));
			image_sp->setTexture("res_ui/mengban_red55.png");
			image_sp->setScale9Enable(true);
			image_sp->setCapInsets(CCRect(5,5,1,1));
			image_sp->setScale9Size(Btn_pacItemFrame->getContentSize());
			Btn_pacItemFrame->addChild(image_sp);

			UILabel * l_des = UILabel::create();
			l_des->setText(StringDataManager::getString("packageitem_notAvailable"));
			l_des->setFontName(APP_FONT_NAME);
			l_des->setFontSize(18);
			l_des->setAnchorPoint(ccp(0.5f,0.5f));
			l_des->setPosition(ccp(0,0));
			Btn_pacItemFrame->addChild(l_des);
		}

		switch(equipment->part())
		{
		case 4 :
			{
				this->setPosition(ccp(438.5f, 358.5f));    //Œ‰∆˜ 4
				equipui->image_arms->setVisible(false);

			}break;
		case 2 :
			{
				this->setPosition(ccp(438.5f, 277.5f));   //“¬∑˛ 2
				equipui->image_clohes->setVisible(false);
			}break;
		case 0 :
			{
				this->setPosition(ccp(701, 358.5f));    //Õ∑ø¯  0
				equipui->image_helmet->setVisible(false);
			}break;
		case 7 :
			{
				this->setPosition(ccp(438.5f, 195));    //Ω‰÷∏7
				equipui->image_ring->setVisible(false);
			}break;
		case 3 :
			{
				this->setPosition(ccp(701, 195));    //—¸¥¯ 3
				equipui->image_accessories->setVisible(false);
			}break;
		case 9 :
			{
				this->setPosition(ccp(701, 277.5f));    //–¨ 9
				equipui->image_shoes->setVisible(false);
			}break;
		}
		generalEquipment =new CEquipment();
		generalEquipment->CopyFrom(*equipment);

		m_mengban_black = UIImageView::create();
		m_mengban_black->setTexture("res_ui/mengban_black65.png");
		m_mengban_black->setScale9Enable(true);
		m_mengban_black->setScale9Size(CCSizeMake(63,63));
		m_mengban_black->setCapInsets(CCRectMake(5,5,1,1));
		m_mengban_black->setAnchorPoint(ccp(0.5f,0.5f));
		m_pUiLayer->addWidget(m_mengban_black);
		m_mengban_black->setVisible(false);

		m_select_state = UIImageView::create();
		m_select_state->setTexture("res_ui/greengou.png");
		m_select_state->setAnchorPoint(ccp(0.5f,0.5f));
		m_select_state->setPosition(ccp(0,m_mengban_black->getContentSize().height/2 - 2));
		m_mengban_black->addChild(m_select_state);
		m_select_state->setVisible(false);


		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(Btn_pacItemFrame->getContentSize());
		return true;
	}
	return false;
}


void GeneralEquipMent::onEnter()
{
	UIScene::onEnter();
}

void GeneralEquipMent::onExit()
{
	UIScene::onExit();
}

void GeneralEquipMent::showEquipMentInfo(CCObject * obj)
{
	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(obj);
	CCSize winsize =CCDirector::sharedDirector()->getVisibleSize();
	UIButton * btn =(UIButton *)obj;
	/*
	EquipMentUi * equipui =(EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
	equipui->addRoleEquipment(generalEquipment);
	*/
	
	GeneralEquipMentInfo *equipInfo =GeneralEquipMentInfo::create(generalEquipment);
	equipInfo->ignoreAnchorPointForPosition(false);
	equipInfo->setAnchorPoint(ccp(0.5f,0.5f));
	equipInfo->setTag(kTagGoodsItemInfoBase);
	//equipInfo->setZOrder(100);
	GameView::getInstance()->getMainUIScene()->addChild(equipInfo);
}

void GeneralEquipMent::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void GeneralEquipMent::addCCTutorialIndicator( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,40,70);
// 	tutorialIndicator->setPosition(ccp(pos.x-40,pos.y+100));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);

	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,63,63,true);
	//tutorialIndicator->setDrawNodePos(ccp(pos.x+27+_w,pos.y+10+_h));//438.5f, 358.5f
	tutorialIndicator->setDrawNodePos(ccp(438.5f+_w, 358.5f+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void GeneralEquipMent::removeCCTutorialIndicator()
{
	CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		CCTeachingGuide* teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}

void GeneralEquipMent::SetCurgeneralEquipGray(bool ptouched)
{
	if (ptouched)
	{
		Btn_pacItemFrame->setTouchEnable(false);
		m_mengban_black->setVisible(true);
		m_select_state->setVisible(true);
	}else
	{
		Btn_pacItemFrame->setTouchEnable(true);
		m_mengban_black->setVisible(false);
		m_select_state->setVisible(false);
	}
}

bool GeneralEquipMent::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return false;
	//return this->resignFirstResponder(touch,this,false);
}

void GeneralEquipMent::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void GeneralEquipMent::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void GeneralEquipMent::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

///////////////////////////////////
GeneralEquipMentInfo::GeneralEquipMentInfo()
{

}

GeneralEquipMentInfo::~GeneralEquipMentInfo()
{
	delete goods_;
	delete curEquip_;
}

GeneralEquipMentInfo * GeneralEquipMentInfo::create( CEquipment * equipment )
{
	GeneralEquipMentInfo * generalEquip =new GeneralEquipMentInfo();
	if (generalEquip && generalEquip->init(equipment))
	{
		generalEquip->autorelease();
		return generalEquip;
	}
	CC_SAFE_DELETE(generalEquip);
	return NULL;

}
bool GeneralEquipMentInfo::init( CEquipment * equipment)
{
	goods_ =new GoodsInfo();
	goods_->CopyFrom(equipment->goods());

	curEquip_ =new CEquipment();
	curEquip_->CopyFrom(*equipment);
	
	if (GoodsItemInfoBase::init(goods_,GameView::getInstance()->EquipListItem,0))
	{
		equipui =(EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
		const char *strings_ = StringDataManager::getString("goods_equipment_main");
		//add main equip
		btn_addMainEquip= UIButton::create();
		btn_addMainEquip->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		btn_addMainEquip->setTouchEnable(true);
		btn_addMainEquip->setPressedActionEnabled(true);
		btn_addMainEquip->addReleaseEvent(this,coco_releaseselector(GeneralEquipMentInfo::mainEquipment));
		btn_addMainEquip->setAnchorPoint(ccp(0.5f,0.5f));
		btn_addMainEquip->setScale9Enable(true);
		btn_addMainEquip->setScale9Size(CCSizeMake(91,43));
		btn_addMainEquip->setCapInsets(CCRect(18,9,2,23));
		btn_addMainEquip->setPosition(ccp(141,33));

		UILabel * Label_mainEquip = UILabel::create();
		Label_mainEquip->setText(strings_);
		Label_mainEquip->setFontName(APP_FONT_NAME);
		Label_mainEquip->setFontSize(18);
		Label_mainEquip->setAnchorPoint(ccp(0.5f,0.5f));
		Label_mainEquip->setPosition(ccp(0,0));
		btn_addMainEquip->addChild(Label_mainEquip);
		m_pUiLayer->addWidget(btn_addMainEquip);
		
		return true;
	}
	return false;
}

void GeneralEquipMentInfo::onEnter()
{
	GoodsItemInfoBase::onEnter();
}

void GeneralEquipMentInfo::onExit()
{
	GoodsItemInfoBase::onExit();
}

bool GeneralEquipMentInfo::ccTouchBegan( CCTouch * pTouch,CCEvent * pEvent )
{
	return this->resignFirstResponder(pTouch,this,false);
}

void GeneralEquipMentInfo::mainEquipment(CCObject * obj)
{
	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(obj);
	
	equipui->generalMainId = equipui->mes_petid;
	
	equipui->addRoleEquipment(curEquip_);
	this->removeFromParentAndCleanup(true);
}

void GeneralEquipMentInfo::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void GeneralEquipMentInfo::addCCTutorialIndicator( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();

	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;

	int temp_x = pos.x+85+_w;
	int temp_y = pos.y+60+_h;

	int openlevel = 0;
	std::map<int,int>::const_iterator cIter;
	cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(26);
	if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end())
	{
	}
	else
	{
		openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[26];
	}
	if (GameView::getInstance()->myplayer->getActiveRole()->level() < openlevel)
	{
		temp_x +=41;
	}
	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,91,43,true);
	tutorialIndicator->setDrawNodePos(ccp(temp_x,temp_y));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void GeneralEquipMentInfo::removeCCTutorialIndicator()
{
// 	CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(m_pUiLayer->getChildByTag(CCTUTORIALINDICATORTAG));
// 	if(tutorialIndicator != NULL)
// 		tutorialIndicator->removeFromParent();

	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		CCTeachingGuide* teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}




