#include "StrengthEquip.h"
#include "GameView.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../backpackscene/GoodsItemInfoBase.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../gamescene_state/MainScene.h"
#include "EquipMentUi.h"
#include "../../utils/StaticDataManager.h"
#include "AppMacros.h"


StrengthEquip::StrengthEquip(void)
{
	m_starLevel = 0;
	m_refineLevel =0;
	m_refineUselevlel =0;
	m_clazz =0;
	m_quality =0;
}


StrengthEquip::~StrengthEquip(void)
{
	
}

StrengthEquip * StrengthEquip::create(FolderInfo* folder,bool isMainequip)
{
	StrengthEquip * equip=new StrengthEquip();
	if (equip && equip->init(folder,isMainequip))
	{
		equip->autorelease();
		return equip;
	}
	CC_SAFE_DELETE(equip);
	return NULL;
}

bool StrengthEquip::init(FolderInfo* folder,bool isMainequip)
{
	if (UIScene::init())
	{
		m_clazz = folder->goods().equipmentclazz();
		m_quality = folder->goods().quality();
		std::string frameColorPath = "res_ui/";
		if (isMainequip == true)
		{
			if (folder->goods().quality() == 1)
			{
				frameColorPath.append("bdi_white");
			}
			else if (folder->goods().quality() == 2)
			{
				frameColorPath.append("bdi_green");
			}
			else if (folder->goods().quality() == 3)
			{
				frameColorPath.append("bdi_blue");
			}
			else if (folder->goods().quality() == 4)
			{
				frameColorPath.append("bdi_purple");
			}
			else if (folder->goods().quality() == 5)
			{
				frameColorPath.append("bdi_orange");
			}
			else
			{
				frameColorPath.append("bdi_white");
			}
		}else
		{
			if (folder->goods().quality() == 1)
			{
				frameColorPath.append("sdi_white");
			}
			else if (folder->goods().quality() == 2)
			{
				frameColorPath.append("sdi_green");
			}
			else if (folder->goods().quality() == 3)
			{
				frameColorPath.append("sdi_bule");
			}
			else if (folder->goods().quality() == 4)
			{
				frameColorPath.append("sdi_purple");
			}
			else if (folder->goods().quality() == 5)
			{
				frameColorPath.append("sdi_orange");
			}
			else
			{
				frameColorPath.append("sdi_white");
			}
		}
		
		frameColorPath.append(".png");

		Btn_pacItemFrame = UIButton::create();
		Btn_pacItemFrame->setTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
		Btn_pacItemFrame->setTouchEnable(true);
		Btn_pacItemFrame->setAnchorPoint(ccp(0.5f,0.5f));
		Btn_pacItemFrame->setPosition(ccp(25,24));
		Btn_pacItemFrame->setWidgetTag(folder->id());
		Btn_pacItemFrame->addReleaseEvent(this,coco_releaseselector(StrengthEquip::showEquipsInfo));
		m_pUiLayer->addWidget(Btn_pacItemFrame);

		std::string goodsInfoStr ="res_ui/props_icon/";
		goodsInfoStr.append(folder->goods().icon());
		goodsInfoStr.append(".png");

		UIImageView *equipIcon_ =UIImageView::create();
		equipIcon_->setTexture(goodsInfoStr.c_str());
		equipIcon_->setAnchorPoint(ccp(0.5f,0.5f));
		equipIcon_->setPosition(ccp(0,0));
		Btn_pacItemFrame->addChild(equipIcon_);

		m_refineUselevlel = folder->goods().uselevel();

		if (folder->goods().equipmentclazz()>0 && folder->goods().equipmentclazz()<11)
		{
			m_starLevel = folder->goods().equipmentdetail().starlevel();
			char starLvLabel_[5];
			sprintf(starLvLabel_,"%d",m_starLevel);
			labelStarlevel=UILabel::create();
			labelStarlevel->setAnchorPoint(ccp(0,0));
			labelStarlevel->setPosition(ccp(-25,-20));
			labelStarlevel->setFontSize(13);
			labelStarlevel->setText(starLvLabel_);
			Btn_pacItemFrame->addChild(labelStarlevel);

			imageStar_ =UIImageView::create();
			imageStar_->setTexture("res_ui/star_on.png");
			imageStar_->setScale(0.5f);
			imageStar_->setAnchorPoint(ccp(0,0));
			imageStar_->setPosition(ccp(labelStarlevel->getPosition().x+ labelStarlevel->getContentSize().width,labelStarlevel->getPosition().y));
			Btn_pacItemFrame->addChild(imageStar_);

			if (this->getStarLevel() <= 0)
			{
				labelStarlevel->setVisible(false);
				imageStar_->setVisible(false);
			}

			m_refineLevel = folder->goods().equipmentdetail().gradelevel();
			char strengthLv[5];
			sprintf(strengthLv,"%d",m_refineLevel);
			std::string strengthStr_ = "+";
			strengthStr_.append(strengthLv);

			label_StrengthLv = UILabel::create();
			label_StrengthLv->setAnchorPoint(ccp(1.0f,0.5f));
			label_StrengthLv->setPosition(ccp(22,15));
			label_StrengthLv->setText(strengthStr_.c_str());
			label_StrengthLv->setFontSize(13);
			Btn_pacItemFrame->addChild(label_StrengthLv);
			
			if (this->getRefineLevel()<=0)
			{
				label_StrengthLv->setVisible(false);
			}
		}else
		{
			EquipMentUi * equip_ =(EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
			char stoneAmountStr[20];
			if (equip_->currType == 0)
			{
				sprintf(stoneAmountStr,"%d",equip_->stoneOfAmountRefine);
			}else
			{
				sprintf(stoneAmountStr,"%d",equip_->stoneOfAmountStar);
			}
			
			UILabel * label_amount = UILabel::create();
			label_amount->setAnchorPoint(ccp(1,1));
			label_amount->setPosition(ccp(21,-10));
			label_amount->setText(stoneAmountStr);
			label_amount->setFontSize(13);
			Btn_pacItemFrame->addChild(label_amount);
		}
		packIndex=folder->id();
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(Btn_pacItemFrame->getContentSize());
		return true;
	}
	return false;
}

void StrengthEquip::onEnter()
{
	UIScene::onEnter();
}

void StrengthEquip::onExit()
{
	UIScene::onExit();
}

void StrengthEquip::showEquipsInfo( CCObject * obj )
{
	UIButton *	btn=(UIButton *)obj;
	int index=btn->getWidgetTag();

	EquipMentUi * equip=(EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
	equip->getBackMainEquip(this->getTag(),index);
	
	/*
	CCSize size= CCDirector::sharedDirector()->getVisibleSize();
	EquipMentUi * equipUi = (EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
	if (equipUi != NULL)
	{
		EquipStrengthInfo * equipInfo = EquipStrengthInfo::create(GameView::getInstance()->AllPacItem.at(index),equipUi->generalEquipVector,equipUi->mes_petid);
		equipInfo->ignoreAnchorPointForPosition(false);
		equipInfo->setAnchorPoint(ccp(0.5f,0.5f));
		equipInfo->setTag(this->getTag());
		GameView::getInstance()->getMainUIScene()->addChild(equipInfo);
	}
	*/
}

void StrengthEquip::setStarLevel( int level_ , bool isAction)
{
	m_starLevel = level_;

	if (level_ >0)
	{
		if (level_ >=20)
		{
			level_ =20;
		}
		char starLvLabel_[10];
		sprintf(starLvLabel_,"%d",level_);
		labelStarlevel->setText(starLvLabel_);
		imageStar_->setPosition(ccp(labelStarlevel->getPosition().x+ labelStarlevel->getContentSize().width,labelStarlevel->getPosition().y));
		labelStarlevel->setVisible(true);
		imageStar_->setVisible(true);

		if (isAction)
		{
			CCSequence * seq_ = CCSequence::create(
				CCScaleTo::create(0.01f,10.0f),
				CCScaleTo::create(0.3f,1.0f),
				NULL);
			labelStarlevel->runAction(seq_);
			
			CCSequence * seq_1 = CCSequence::create(
				CCScaleTo::create(0.01f,5.0f),
				CCScaleTo::create(0.3f,0.5f),
				NULL);
			imageStar_->runAction(seq_1);
		}

	}else
	{
		labelStarlevel->setVisible(false);
		imageStar_->setVisible(false);
	}
}

int StrengthEquip::getStarLevel()
{
	return m_starLevel;
}

void StrengthEquip::setRefineLevel( int level_,bool isAction )
{
	m_refineLevel = level_;
	if (level_ > 0)
	{
		if (level_>=20)
		{
			level_ =20;
		}
		char strengthLv[10];
		sprintf(strengthLv,"%d",level_);
		std::string strengthStr_ = "+";
		strengthStr_.append(strengthLv);
		label_StrengthLv->setText(strengthStr_.c_str());
		label_StrengthLv->setVisible(true);

		if (isAction)
		{
			CCSequence * seq_ = CCSequence::create(
				CCScaleTo::create(0.01f,10.0f),
				CCScaleTo::create(0.3f,1.0f),
				NULL);
			label_StrengthLv->runAction(seq_);
		}
	}else
	{
		label_StrengthLv->setVisible(false);
	}
}

int StrengthEquip::getRefineLevel()
{
	return m_refineLevel;
}

int StrengthEquip::getRefineUseLevel()
{
	return m_refineUselevlel;
}

int StrengthEquip::getClazz()
{
	return m_clazz;
}

int StrengthEquip::getQuality()
{
	return m_quality;
}
////////////////
EquipStrengthInfo::EquipStrengthInfo()
{

}

EquipStrengthInfo::~EquipStrengthInfo()
{

}

EquipStrengthInfo * EquipStrengthInfo::create( FolderInfo* folder,std::vector<CEquipment *>equipVector,long long generalId )
{
	EquipStrengthInfo * equip = new EquipStrengthInfo();
	if (equip && equip->init(folder,equipVector,generalId))
	{
		equip->autorelease();
		return equip;
	}
	CC_SAFE_DELETE(equip);
	return NULL;
}

bool EquipStrengthInfo::init( FolderInfo* folder,std::vector<CEquipment *>equipVector,long long generalId )
{
	GoodsInfo * goods_ =new GoodsInfo();
	goods_->CopyFrom(folder->goods());
	if (GoodsItemInfoBase::init(goods_,equipVector,generalId))
	{
		const char *str_recove = StringDataManager::getString("goods_equip_getBack");
		UIButton * Button_getAnnes= UIButton::create();
		Button_getAnnes->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		Button_getAnnes->setTouchEnable(true);
		Button_getAnnes->setPressedActionEnabled(true);
		Button_getAnnes->addReleaseEvent(this,coco_releaseselector(EquipStrengthInfo::refineMainEquip));
		Button_getAnnes->setAnchorPoint(ccp(0.5f,0.5f));
		Button_getAnnes->setScale9Enable(true);
		Button_getAnnes->setScale9Size(CCSizeMake(80,43));
		Button_getAnnes->setCapInsets(CCRect(18,9,2,23));
		Button_getAnnes->setWidgetTag(folder->id());
		Button_getAnnes->setPosition(ccp(135,25));

		UILabel * Label_getAnnes = UILabel::create();
		Label_getAnnes->setText(str_recove);
		Label_getAnnes->setAnchorPoint(ccp(0.5f,0.5f));
		Label_getAnnes->setFontName(APP_FONT_NAME);
		Label_getAnnes->setFontSize(18);
		Label_getAnnes->setPosition(ccp(0,0));
		Button_getAnnes->addChild(Label_getAnnes);
		m_pUiLayer->addWidget(Button_getAnnes);

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setAnchorPoint(ccp(0,0));
		delete goods_;
		return true;
	}
	return false;
}

void EquipStrengthInfo::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void EquipStrengthInfo::onExit()
{
	UIScene::onExit();
}
void EquipStrengthInfo::refineMainEquip( CCObject * obj )
{
	UIButton * btn= (UIButton*)obj;
	int index =btn->getWidgetTag();
	EquipMentUi * equip=(EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
	equip->getBackMainEquip(this->getTag(),index);
	this->removeFromParentAndCleanup(true);
}

bool EquipStrengthInfo::ccTouchBegan( CCTouch * pTouch,CCEvent * pEvent )
{
	return this->resignFirstResponder(pTouch,this,false);
}
