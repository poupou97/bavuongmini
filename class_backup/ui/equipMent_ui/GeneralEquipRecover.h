#ifndef EQUIP_GENERALEQUIPRECOVER_H
#define EQUIP_GENERALEQUIPRECOVER_H


#include "../extensions/UIScene.h"
class CEquipment;
USING_NS_CC;
USING_NS_CC_EXT;
class GeneralEquipRecover:public UIScene
{
public:
	GeneralEquipRecover(void);
	~GeneralEquipRecover(void);

	static GeneralEquipRecover *create(CEquipment * equipment,bool isMainequip);
	bool init(CEquipment * equipment,bool isMainequip);

	virtual void onEnter();
	virtual void onExit();

	void showEquipInfo(CCObject * obj);
	CEquipment * equip_;
	UIButton *Btn_pacItemFrame;
	UIImageView * m_mengban_black;
	CEquipment * getEquipment();
public:
	int getEquipUseLevel();

	UILabel * labelStarlevel_;
	UIImageView * imageStar_;

	UILabel * label_refineLv;
	void setStarLevel(int level_,bool isAction = false);
	int getStarLevel();
	void setRefineLevel(int level_,bool isAction = false);
	int getRefineLevel();
	int getQuality();
	int getClazz();
private:
	int m_useLevel;

	int m_starLevel;
	int m_refineLevel;

	int m_clazz;
	int m_quality;
};

#endif;