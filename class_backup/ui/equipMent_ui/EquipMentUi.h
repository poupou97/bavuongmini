#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "../../messageclient/protobuf/EquipmentMessage.pb.h"
#include "EquipMentGeneralList.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "../../messageclient/element/CActiveRole.h"
#include "../../legend_script/CCTutorialIndicator.h"

USING_NS_CC;
USING_NS_CC_EXT;

class PacPageView;
class CAdditionProperty;
class EquipmentMessage;
class CEquipHole;
class CEquipment;
class FolderInfo;
class GoodsInfo;
class UITab;
enum
{
	//change  tab type 
	REFININGTYPE = 0,
	STARTYPE = 2,
	GEMTYPE = 1,
	BAPTIZETYPE = 3,
	SYSTHESISTYPE = 4,
	///add main equip tag 
	REFININGMAINEQUIP=11,
	STAREQUIP=12,
	GEMEQUIP=13,
	BAPTIZEEQUIP=14,

	//add refine assisment
	FRISTASSIST=21,
	SECONDASSIST=22,
	THIRDASSIST=23,
	FOURASSIST=24,
	FIVESTRONGTH=25,

	SHOWGEMVALUELAYER = 50,
	
	GENERALREFINEEQUIP =110,
	GENERALSTAREQUIP =120,
	GENERALGEMEQUIP=130,
	GENERALBAPTIZEEQUIP =140,

	GENERALFIRSTASSIST =210,
	GENERALSECONDASSIST =220,
	GENERALTHIRDASSIST =230,
	GENERALFOURASSIST =240,
	GENERALFIVEASSIST = 250,
	//add property to layer
	REFINEPROPERTYLAYER= 800,
	STARPROPERTYLAYER= 810,
	GEMPROPERTYLAYER = 820,
	BAPTIZEPROPERTYLAYER= 830,
	//add stone tag
	STONELAYERTAG =89,
	//add general equipment 
	GENERALEQUIPMENTLIST =90,
	ROLEANMITIONTAG = 299,
	EQUIPMENTROLEANIMATIONTAG = 300,
	CLASS_SYSTHESIS_TAG = 301,
	//end look equip
	REFINELOOKEQUIP = 801,
	GENERALREFINEEQUIPLOOK = 802,

	//end look equip
	STARLOOKEQUIP = 803,
	GENERALSTARQUIPLOOK = 804,
};

enum operatorType {
	// delete  101, getback 102
	operatorGetBack = 101,    
	operatorDelete  = 102,
};

class EquipMentUi:public UIScene
{
public:
	EquipMentUi(void);
	~EquipMentUi(void);

	static EquipMentUi *create();
	bool init();
	void onEnter();
	void onExit();
	int currType;
	CCSize size;
	UILayer * refineLayer;
	UILayer * starPropertyLayer;
	UILayer * gemLayer;
	UILayer * baptizePropertylayer;
	UILayer * synthesisLayer;

	UITab * equipTab;
	UITab * roleAndPack;

	bool isRefreshPacGoods;
	void refreshBackPack(int curtype,int equipStoneLevel);
	void callBackChangeTab(CCObject * obj);
	void callBackPackAndRole(CCObject * obj);

	void refreshCurState();

	void callBackClearUp(CCObject * obj);
	void callBackShop(CCObject * obj);
	void callBackExit(CCObject * obj);

	UILayer * roleLayer;
	UIImageView * image_rolePression;
	UILabel * label_roleLevel;
	UILabelBMFont * label_roleFight;
	UILabelBMFont * label_roleTotalFight;
	EquipMentGeneralList * generalList;
	void refreshRoleInfo(CActiveRole * activeRole,int fight,std::vector<CEquipment *>equipmentsVector);

	UILabel * label_ingot;
	UILabel * label_gold;
	UILabel * label_goldBingding;
	void refreshPlayerMoney();
	void removeAssistAndStone(int operateType);

	UIPanel* equipPanel;
	bool mainEquipIsPac;
	int mainEquipIndexRefine;
	int generalEquipOfPart;
	
	bool refineMainEquip;
	bool starMainEquip;
	bool gemMainEquip;
	bool baptizeMainEquip;

	bool StrengthStone;
	
	void initRefine();
	void initStar();
	void initGem();
	void initBaptize();
	void initSynthesis();
	
	//add all button is des
	void callBackRefineMainDes(CCObject * obj);
	void callBackAssistEquipDes(CCObject * obj);

	void callBackStarMainDes(CCObject * obj);
	void callBackStarAssistDes(CCObject * obj);

	void callBackGemMainDes(CCObject * obj);
	void callBackGemAssistDes(CCObject * obj);


	void callBackBaptizeMainDes(CCObject * obj);

	void isSureEquip(int index);
	//refine
	bool firstAssist;
	bool secondtAssist;
	bool thirdAssist;
	bool fourAssist;
	bool fiveAssist;
	int addAllEx;
	int curExValue;
	int allExValue;
	int refineCost;
	//role equip image 
	UIImageView * image_arms;
	UIImageView * image_clohes;
	UIImageView * image_ring;
	UIImageView * image_helmet;
	UIImageView * image_shoes;
	UIImageView * image_accessories;

	UIImageView * refine_textAreaPutEquipment;
	UILabelBMFont * labelFont_curRefinelevel;
	CCProgressTimer * exPercent_refine;
	UIImageView * exPercent_refineAdd;
	//UILabel * exLabel_refine;
	CCLabelTTF * exLabel_refine;
	UILabelBMFont * labelCost;

	UILabelBMFont * refineBeginFirstLabel;
	UILabelBMFont * refineBeginSecondLabel;
	UILabel * refineBeginFirstvalue;
	UILabel * refineBeginSecondvalue;

	UILabelBMFont * refineEndFirstLabel;
	UILabelBMFont * refineEndSecondLabel;

	UILabel * refineEndFirstvalue;
	UILabel * refineEndSecondvalue;

	UILabel * refineEndFirstvalueAdd;
	UILabel * refineEndSecondvalueAdd;

	int equipLookRefineLevel_;//当前预览的装备的精炼等级
	int m_equipBeginLevel;
	void setEquipBeginLevel(int curLv_);
	int getEquipBeginLevel();
	int curBeginExp;
	void addRoleEquipment(CEquipment * equips);
	void addMainEquipOfGeneral(CEquipment * equips);
	void getBackGeneralMainEquip(int layerTag);
	void addAssistGeneralOfEquip(CEquipment * equips);
	
	long long generalMainId;
	long long curSelectGeneralId;
	long long equipmentInstanceid;
	std::vector<CEquipment *>curMainEquipvector;
	CEquipment * getCurRoleEquipment(int index);

	void addOperationFloder(FolderInfo * floders);
	void addMainEquipment( FolderInfo * floders );
	void addAssistEquipment(FolderInfo * floders,bool isAction = true);
	void getBackMainEquip(int tag,int index);
	bool isCanBaptize(GoodsInfo * goods);

	void refreshExpValue(int refineExvalue,bool isScale,bool isAction = true);
	void getRefineAdditionPropertValue(int equipLookLevel);

	void addExPercentSetPrecentIsFull();
	void addExPercentSetPrecentIsCur();

	void callBackBtn_Refine(CCObject * obj);
	void callBackBtn_addAll(CCObject * obj);
	
	//star
	UIImageView * star_textArea_putEquipment;

	UILabelBMFont * starBeginFirstLabel;
	UILabelBMFont * starBeginSecondLabel;
	UILabelBMFont * starBeginThirdLabel;
	UILabelBMFont * starBeginFourLabel;
	
	UILabel * starBeginFirstvalue;
	UILabel * starBeginSecondvalue;
	UILabel * starBeginThirdvalue;
	UILabel * starBeginFourvalue;

	UILabelBMFont * starEndFirstLabel;
	UILabelBMFont * starEndSecondLabel;
	UILabelBMFont * starEndThirdLabel;
	UILabelBMFont * starFourFourLabel;

	UILabel * starEndFirstvalue;
	UILabel * starEndSecondvalue;
	UILabel * starEndThirdvalue;
	UILabel * starEndFourvalue;

	UILabel * starEndFirstvalueAdd;
	UILabel * starEndSecondvalueAdd;
	UILabel * starEndThirdvalueAdd;
	UILabel * starEndFourvalueAdd;

	std::vector<CAdditionProperty *> additionProperty;
	void refreshEquipProperty(int curValue,int allValue,bool isAction);
	int m_curLuckValue;
	int m_allLuckValue;
	int starCost;
	int starLevelValue;
	UILabel * exLabel_star;
	
	UIImageView * exPercent_star;
	
	int m_curEquipStarEx;
	void setCurEquipExStar(int value_);
	int getCurEquipExStar();

	UILabelBMFont * successRate;
	UILabelBMFont * starCost_label;
	void refreshStarLuckValue(int curluckval,int starCostvale,bool isAction);

	UIButton * btn_upStar;
	UIButton *  btn_allStar;

	int stoneOfIndexRefine;
	int stoneOfAmountRefine;

	int curMainEquipStarLevel;

	int stoneOfIndexStar;
	int stoneOfAmountStar;
	std::vector<CAdditionProperty *> additionProperty_star;
	void refreshEquipProperty_star(int curluckval,int allluckval,int starCostVale,bool isAction = false);

	void callBackStarDes(CCObject * obj);

	void callBackUpstar(CCObject * obj);
	void callBackAllStar(CCObject * obj);
	//待定 （可以整合到一个方法中暂时没有)
	void addStrengthStone();
	void strengthStoneCounter(CCObject * obj);
	////gem
	UIImageView * gem_textArea_putEquipment;
	UIImageView * imageLock1;
	UIImageView * imageLock2;
	UIImageView * imageLock3;

	UILabel * holeLabelFirst;
	UILabel * holeLabelSecond;
	UILabel * holeLabelThird;

	UIImageView * gemBackGround_1;
	UIImageView * gemBackGround_2;
	UIImageView * gemBackGround_3;

	UIButton * gem_buttonPick1;
	UIButton * gem_buttonPick2;
	UIButton * gem_buttonPick3;

	//UILabelBMFont * holeStoneCostLabel;
	//UILabelBMFont * holeCostLabelGem;

	UILabelBMFont * holeStoneAmoumtBingdingLabel;
	UILabelBMFont * holeStoneAmountNotBingdingLabel;

	UICheckBox * useGemBingding;

	UIButton * gem_buttonHole_first;
	UIButton * gem_buttonHole_second;
	UIButton * gem_buttonHole_third;

	int holeFalg;//get already hole id flag
	void callBackHole(CCObject * obj);

	void isSureNotHoleBingding(CCObject * obj);
	void isSureBingdingHoleBingding(CCObject * obj);
	int holeIndex_;
	//void addGemEquipment(std::string icon, std::string name, std::string effert);
	void addGemEquipment(int holeIndex);
	void addGemEquipCurHole(int holeIndex);
	//void equipMentInlaySure(CCObject * obj);

	void equipFirstPickSure(CCObject * obj);
	void equipSecondpickSure(CCObject * obj);
	void equipThirdpickSure(CCObject * obj);

	int getCurHoleStoneAmount(int typeBing);

	std::vector<CEquipHole *>equipmentHoleV;
	void firstButtonEvent(CCObject * obj);
	void secondButtonEvent(CCObject * obj);
	void thirdButtonEvent(CCObject * obj);
	void refreshGemValue(bool backSame);

	void callBackBaptize(CCObject * obj);
	void callBackBaptizeSure(CCObject * obj);
	//////////////////////////baptize
	int aglieLock;
	int intelligenceLock;
	int focusLock;

	int baptizeCost;
	std::vector<int> propertyLock;

	UIImageView * baptize_textAre_putEquipment;
	UILabelBMFont * labelBaptizeCost; 
	UILabelBMFont * labelBaptizeCostStrone;
	UICheckBox * firstPropertyLock;
	UICheckBox * secondPropertyLock;
	UICheckBox * thirdPropertyLock;
	UICheckBox * fourPropertyLock;
	UICheckBox * fivePropertyLock;

	void setBaptizeFirstCheckBox(CCObject * obj,CheckBoxEventType type);
	void setBaptizeSecondCheckBox(CCObject * obj,CheckBoxEventType type);
	void setBaptizeThirdCheckBox(CCObject * obj,CheckBoxEventType type);
	void setBaptizeFourCheckBox(CCObject * obj,CheckBoxEventType type);
	void setBaptizeFiveCheckBox(CCObject * obj,CheckBoxEventType type);

	UILabel * baptizenotBaptize_5;
	UILabel * baptizenotBaptize_10;
	UILabel * baptizenotBaptize_15;
	UILabel * baptizenotBaptize_20;


	UILabelBMFont * baptizeFirstLabel;
	UILabelBMFont * baptizesSecondLabel;
	UILabelBMFont * baptizeThirdLabel;
	UILabelBMFont * baptizeFourLabel;
	UILabelBMFont * baptizeFiveLabel;

	UILabel * baptizeFirstvalue;
	UILabel * baptizeSecondvalue;
	UILabel * baptizeThirdvalue;
	UILabel * baptizeFourvalue;
	UILabel * baptizeFivevalue;

	std::vector<CAdditionProperty *> additionProperty_baptize;
	void refreshEquipProperty_baptize(int cost);
	void equipMentSureBaptize(CCObject * obj);

	UICheckBox * useBaptizeBingding;

	void isSureNotBingingBaptize(CCObject * obj);
	void isSureBingingBaptize(CCObject * obj);
	int getBaptizeClock(int bingdingType);

	void PageScrollToDefault();

public:
	UILayer * m_Layer;
	UIPanel * refiningPanel;
	UIPanel * starPanel;
	UIPanel * gemPanel;
	UIPanel * baptizePanel;
	UIPanel * synthesisPanel;

	UIPanel * rolePanel;
	UIPanel * packPanel;
public:
	UILayer *layerPackageFrame;
	PacPageView * pageView;
	UIImageView* currentPage;
	void CurrentPageViewChanged(CCObject *pSender);
	void refreshGeneralEquipment(std::vector<CEquipment *>equipmentsVector);
	std::vector<CEquipment *> generalEquipVector;

	std::string getBackGroundPathByGoodsQulity(int quality);
private:
	//state 
	UIImageView *image_state_refine_mainEquip;
	UIImageView *image_state_first_assist;
	UIImageView *image_state_second_assist;
	UIImageView *image_state_third_assist;
	UIImageView *image_state_four_assist;
	UIImageView *image_state_five_assist;

	UIImageView *image_state_star_MainEquip;
	UIImageView *image_state_star_stone;

	UIImageView * image_state_gem_mainEquip;

	UIImageView * image_state_baptize_mainEquip;
public:
	//tabview select id
	int tabviewSelectIndex;
	//1601
	int mes_source;
	int mes_pob;
	int mes_assistEquipPob;
	int mes_index;
	int mes_petid;
	//int mes_playType;

	struct assistEquipStruct
	{
		int source_;
		int  pob_;
		long long petid_;
		int index_;
		int playType;
		int binging;
		int equipStarLv;
		bool equipGemLv;
	};
	std::vector<assistEquipStruct> assistVector;

	struct assistStoneStruct
	{
		int source_;
		int index_;
		int amount_;
		int playType_;
		int tag_;
		int binging;
	};
	std::vector<assistStoneStruct> assStoneVector;

	void isSureBingdingRefine(CCObject * obj);
	void isSureBingdingStar(CCObject * obj);
	void isSureBingdingGem(CCObject * obj);
	void isSureBingdingBaptize(CCObject * obj);

	void isSureStarLevelOfRefine(CCObject * obj);
	bool isHaveGemValue(GoodsInfo * goods);

	void setGemBingding(CCObject * obj,CheckBoxEventType type);
	void setBaptizeDingding(CCObject * obj,CheckBoxEventType type);
private:
	bool m_isFirstOpenBackpac;
public:
	//教学
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction );
	void addCCTutorialIndicator2(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction );
	void addCCTutorialIndicatorClose(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction );
	void removeCCTutorialIndicator();
	//教学
	int mTutorialScriptInstanceId;

	int generalWeapTag;
	UIButton * btn_addAll;
	UIButton * btn_Refine;
	UIButton * btn_close;
};

