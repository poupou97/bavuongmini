#ifndef EQUIP_GENERALEQUIPRECOVERSHOW_H
#define EQUIP_GENERALEQUIPRECOVERSHOW_H
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../backpackscene/GoodsItemInfoBase.h"

class CEquipment;
USING_NS_CC;
USING_NS_CC_EXT;
class GeneralEquipShowRecover:public GoodsItemInfoBase
{
public:
	GeneralEquipShowRecover(void);
	~GeneralEquipShowRecover(void);

	static GeneralEquipShowRecover *create(CEquipment * equipment,std::vector<CEquipment *>equipVector,long long generalId);
	bool init(CEquipment * equipment,std::vector<CEquipment *>equipVector,long long generalId);

	virtual void onEnter();
	virtual void onExit();

	void recoverGeneralEquipment(CCObject * obj);
};

#endif;