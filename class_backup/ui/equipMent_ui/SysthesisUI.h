#ifndef _EQUIPMENTUI_SYSTEHSISUI_H_
#define _EQUIPMENTUI_SYSTEHSISUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "../extensions/ControlTree.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CSysthesisInfo;

struct PropInfo 
{
	std::string name;
	std::string icon;
	int quality;
};

class SysthesisUI : public UIScene
{
public:
	SysthesisUI();
	~SysthesisUI();

	struct ReqData
	{
		int mergeid;
		int num;
		bool unbindfirst;
	};

	static SysthesisUI * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//查询物品信息
	static PropInfo getPropInfoFromDBByPropId(const char * id);

	//是否可以合成
	bool isEnableSysthesis(CSysthesisInfo * systhesisInfo);
	//返回可以用来合成的数量
	int getNumEnableSysthesis(CSysthesisInfo * systhesisInfo);
	//是否会合成绑定的
	bool isSysthesisBindingItem(CSysthesisInfo * systhesisInfo);

	void refreshUI(CSysthesisInfo * systhesisInfo);
	void refreshContrelTree();
	//select oneCell by systhesisInfo
	void selectCellBySysthesisInfo(CSysthesisInfo * systhesisInfo);

	void checkBoxEvent(CCObject* pSender);
	void mergeOneEvent(CCObject* pSender);
	void mergeAllEvent(CCObject* pSender);

	//默认选择第一个
	void setToDefault();

	CSysthesisInfo * getCurSysthesisInfo();
	void setCurSysthesisInfo(CSysthesisInfo* sysInfo);
	//add BonusSpecialEffect
	void addBonusSpecialEffect();

	void sureToMergeOne(CCObject *pSender);
	void sureToMergeAll(CCObject *pSender);

	//add test by liuzhenxing for tree
	void callBackTreeEvent(CCObject * obj);
	void callBackSecondTreeEvent(CCObject * obj);

	ControlTree * tree_;
private:
	UILayer * u_layer;
	//CCTableView * m_tableView;

	UILabel * l_mergeName;
	UILabel * l_outputName;
	UIImageView * image_merge_frame;
	UIImageView * image_merge_icon;
	UIImageView * image_output_frame;
	UIImageView * image_output_icon;

	UILabel * l_mergeCurNum;
	UILabel * l_sprit;
	UILabel * l_mergeNeedNum;
	UILabel * l_outputNum;

	UICheckBox * cb_useBound;

	UILabel * l_coin_mergeOneNeed;
	UILabel * l_coin_mergeAllNeed;

	UIButton * btn_mergeOne;
	UIButton * btn_mergeAll;

private:
	CSysthesisInfo * curSysthesisInfo;

	std::map<std::string,PropInfo*>curDataSource;
};

#endif
