
#ifndef EQUIPMENTUI_EQUIPMENTGENERALLIST_H
#define  EQUIPMENTUI_EQUIPMENTGENERALLIST_H

#include "../extensions/UIScene.h"
#include "../generals_ui/GeneralsListBase.h"
USING_NS_CC;
USING_NS_CC_EXT;


class EquipMentGeneralList : public GeneralsListBase,public CCTableViewDelegate,public CCTableViewDataSource
{
public:
	EquipMentGeneralList(void);
	~EquipMentGeneralList(void);

	static EquipMentGeneralList *create();
	bool init();

	virtual void onEnter();
	virtual void onExit();


	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);
	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);

	void ReloadGeneralsTableView();
	void ReloadGeneralsTableViewWithoutChangeOffSet();


public:
	//std::vector<CGeneralBaseMsg *>generalBaseMsgList;
	CCTableView * generalList_tableView;
	CGeneralBaseMsg * curGeneralBaseMsg;
};

#endif;