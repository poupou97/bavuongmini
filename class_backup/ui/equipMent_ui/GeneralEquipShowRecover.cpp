#include "GeneralEquipShowRecover.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../utils/StaticDataManager.h"
#include "EquipMentUi.h"
#include "GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/CEquipment.h"
#include "AppMacros.h"

GeneralEquipShowRecover::GeneralEquipShowRecover(void)
{
}


GeneralEquipShowRecover::~GeneralEquipShowRecover(void)
{
}

GeneralEquipShowRecover * GeneralEquipShowRecover::create( CEquipment * equipment,std::vector<CEquipment *>equipVector,long long generalId )
{
	GeneralEquipShowRecover * generalEquip =new GeneralEquipShowRecover();
	if(generalEquip && generalEquip->init(equipment,equipVector,generalId))
	{
		generalEquip->autorelease();
		return generalEquip;
	}
	CC_SAFE_DELETE(generalEquip);
	return NULL;
}

bool GeneralEquipShowRecover::init( CEquipment * equipment ,std::vector<CEquipment *>equipVector,long long generalId)
{
	GoodsInfo * goods_ =new GoodsInfo();
	goods_->CopyFrom(equipment->goods());
	if (GoodsItemInfoBase::init(goods_,equipVector,generalId))
	{
		const char *str_recove = StringDataManager::getString("goods_auction_getback");
		//ȡ�ظ�����ť
		UIButton * Button_getAnnes= UIButton::create();
		Button_getAnnes->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		Button_getAnnes->setTouchEnable(true);
		Button_getAnnes->setPressedActionEnabled(true);
		Button_getAnnes->addReleaseEvent(this,coco_releaseselector(GeneralEquipShowRecover::recoverGeneralEquipment));
		Button_getAnnes->setAnchorPoint(ccp(0.5f,0.5f));
		Button_getAnnes->setScale9Enable(true);
		Button_getAnnes->setScale9Size(CCSizeMake(91,43));
		Button_getAnnes->setCapInsets(CCRect(18,9,2,23));
		Button_getAnnes->setPosition(ccp(135,30));

		UILabel * Label_getAnnes = UILabel::create();
		Label_getAnnes->setText(str_recove);
		Label_getAnnes->setFontName(APP_FONT_NAME);
		Label_getAnnes->setFontSize(18);
		Label_getAnnes->setAnchorPoint(ccp(0.5f,0.5f));
		Label_getAnnes->setPosition(ccp(0,0));
		Button_getAnnes->addChild(Label_getAnnes);
		m_pUiLayer->addWidget(Button_getAnnes);

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		return true;
	}
	return false;
}

void GeneralEquipShowRecover::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void GeneralEquipShowRecover::onExit()
{
	UIScene::onExit();
}

void GeneralEquipShowRecover::recoverGeneralEquipment( CCObject * obj )
{
	UIButton * btn= (UIButton*)obj;
	int tag =this->getTag();
	EquipMentUi * equip=(EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
	equip->getBackGeneralMainEquip(tag);

	std::vector<CEquipment *>::iterator iter;
	for (iter = equip->curMainEquipvector.begin(); iter != equip->curMainEquipvector.end();++iter)
	{
		delete * iter;
	}
	equip->curMainEquipvector.clear();
	for (int i = 0;i<equip->generalEquipVector.size();i++)
	{
		CEquipment * temp_ =new CEquipment();
		temp_->CopyFrom( *equip->generalEquipVector.at(i));
		equip->curMainEquipvector.push_back(temp_);
	}

	if (equip->refineMainEquip == false)
	{
		equip->generalMainId = equip->mes_petid;
	}
	
	equip->stoneOfAmountRefine = 0;
	this->removeFromParentAndCleanup(true);
}
