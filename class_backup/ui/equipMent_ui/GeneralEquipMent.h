
#ifndef _EQUIPMENT_GENERALEQUIPMENT_H
#define _EQUIPMENT_GENERALEQUIPMENT_H

#include "../extensions/UIScene.h"
#include "../backpackscene/GoodsItemInfoBase.h"
#include "../../legend_script/CCTutorialIndicator.h"
USING_NS_CC;
USING_NS_CC_EXT;
class GoodsInfo;
class EquipMentUi;
class CEquipment;
class GeneralEquipMent:public UIScene
{
public:
	GeneralEquipMent(void);
	~GeneralEquipMent(void);

	static GeneralEquipMent *create(CEquipment * equipment);
	bool init(CEquipment * equipment);
	
	void onEnter();
	void onExit();

	void SetCurgeneralEquipGray(bool ptouched);
	void showEquipMentInfo(CCObject * obj);


	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);
	//教学
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction );
	void removeCCTutorialIndicator();
	//教学
	int mTutorialScriptInstanceId;
	UIButton *Btn_pacItemFrame;
	UIImageView *m_mengban_black;
	UIImageView * m_select_state;
	CEquipment * generalEquipment;
};
///////////////////////////////////////////////////////////
class GeneralEquipMentInfo:public GoodsItemInfoBase
{
public:
	GeneralEquipMentInfo();
	~GeneralEquipMentInfo();

	static GeneralEquipMentInfo *create(CEquipment * equipment);
	bool init(CEquipment * equipment);

	virtual void onEnter();
	virtual void onExit();

	EquipMentUi * equipui;
	virtual bool ccTouchBegan(CCTouch * pTouch,CCEvent * pEvent);

	void mainEquipment(CCObject * obj);
	GoodsInfo * goods_;
	CEquipment * curEquip_;
	int equipPob;
public:
	//为了教学方便 获取朱装备的按钮的位置
	UIButton * btn_addMainEquip;

	//教学
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction );
	void removeCCTutorialIndicator();
	//教学
	int mTutorialScriptInstanceId;

};

#endif;