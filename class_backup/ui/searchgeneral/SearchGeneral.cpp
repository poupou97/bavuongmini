#include "SearchGeneral.h"
#include "../../utils/GameUtils.h"
#include "../../legend_engine/LegendAAnimation.h"
#include "../../legend_engine/LegendAnimationCache.h"
#include "../../legend_engine/CCLegendAnimation.h"
#include "../../utils/StaticDataManager.h"
#include "../../GameView.h"
#include "AppMacros.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/protobuf/MissionMessage.pb.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/MainAnimationScene.h"


#define GENERAL_PATH_NUM 53
#define QUANTITY_PATH_NUM 25
#define ROW_NUM 5
#define SUCCESS_NUM 6


#define LAYERTAG_GENERALICON 255
#define LAYERTAG_SCROLL 155

//game difficult type
#define GAME_TIME_1 (60*1000)
#define SHOWGENERALICON_TIME_1 2//1 s show generalIcon

#define GAME_TIME_2 (50*1000)
#define SHOWGENERALICON_TIME_2 6// 3 s

#define GAME_TIME_3 (30*1000)
#define SHOWGENERALICON_TIME_3 8// 4s

enum {
	kStateStart = 0,
	kStatePlay = 1,
	kStateSuccess = 2,
	kStateFail = 3,
	kStateWait = 4,
};

SearchGeneral::SearchGeneral(void)
{
	m_iState = kStateStart;
	m_iSuccessNum = 0;

	pass_timePrecfect = 100;
	m_touchTime = 0;
}


SearchGeneral::~SearchGeneral(void)
{
}

SearchGeneral * SearchGeneral::create(int diffType)
{
	SearchGeneral * searchGeneral=new SearchGeneral();
	if (searchGeneral && searchGeneral->init(diffType))
	{
		searchGeneral->autorelease();
		return searchGeneral;
	}
	CC_SAFE_DELETE(searchGeneral);
	return NULL;
}

bool SearchGeneral::init(int diffType)
{
	if (UIScene::init())
	{
		m_level = diffType;
		switch(m_level)
		{
		case 1:
			{
				m_showIconTime = GAME_TIME_1;
				m_gameEndTIme = SHOWGENERALICON_TIME_1;
			}break;
		case 2:
			{
				m_showIconTime = GAME_TIME_2;
				m_gameEndTIme = SHOWGENERALICON_TIME_2;
			}break;
		case 3:
			{
				m_showIconTime = GAME_TIME_3;
				m_gameEndTIme = SHOWGENERALICON_TIME_3;
			}break;
		}

		winSize=CCDirector::sharedDirector()->getVisibleSize();

		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("newcommerstory/zhezhao.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winSize);
		mengban->setAnchorPoint(ccp(0,0));
		mengban->setPosition(ccp(0,0));
		mengban->setOpacity(240);
		m_pUiLayer->addWidget(mengban);

		UIPanel * panel_ = (UIPanel*) GUIReader::shareReader()->widgetFromJsonFile("res_ui/takegeneral_1.json");
		panel_->setAnchorPoint(ccp(0.5f,0.5f));
		panel_->setPosition(ccp(winSize.width/2,winSize.height/2));
		m_pUiLayer->addWidget(panel_);

		UILabel * label_des = (UILabel *)UIHelper::seekWidgetByName(panel_,"Label_gameDes");
		label_des->setTextAreaSize(CCSizeMake(128,0));

		m_Layer= UILayer::create();
		m_Layer->ignoreAnchorPointForPosition(false);
		m_Layer->setAnchorPoint(ccp(0.5f,0.5f));
		m_Layer->setPosition(ccp(winSize.width/2,winSize.height/2));
		m_Layer->setContentSize(CCSizeMake(800,480));
		m_Layer->setZOrder(10);
		this->addChild(m_Layer);
		//
		const char * firstStr = StringDataManager::getString("search_diaoshi_sha");
		const char * secondStr = StringDataManager::getString("search_diaoshi_chang");
		const char * thirdStr = StringDataManager::getString("search_diaoshi_dian");
		const char * fourStr = StringDataManager::getString("search_diaoshi_bing");
		CCArmature * atmature = MainScene::createPendantAnm(firstStr,secondStr,thirdStr,fourStr);
		atmature->setPosition(ccp(30,240));
		m_Layer->addChild(atmature);

		//Button_start
		btn_start = (UIButton *)UIHelper::seekWidgetByName(panel_,"Button_start");
		btn_start->setTouchEnable(true);
		btn_start->addReleaseEvent(this, coco_releaseselector(SearchGeneral::callBackStart));
		btn_start->setPressedActionEnabled(true);

		//start button action
		CCActionInterval * action1 = CCScaleTo::create(1.0f,1.1f);
		CCActionInterval * action2 = CCScaleTo::create(1.0f,0.9);

		CCSequence * seq_ = CCSequence::create(action1,action2,NULL);
		btn_start->runAction(CCRepeatForever::create(seq_));

		//Button_close
		UIButton *btn_close = (UIButton *)UIHelper::seekWidgetByName(panel_,"Button_close");
		btn_close->setTouchEnable(true);
		btn_close->addReleaseEvent(this, coco_releaseselector(SearchGeneral::callBackClose));
		btn_close->setPressedActionEnabled(true);
		
		//ImageView_time
		
		//ImageView_generalIcon
		image_icon = (UIImageView *)UIHelper::seekWidgetByName(panel_,"ImageView_generalIcon");

		//Label_searchResult
		label_result = (UILabel*)UIHelper::seekWidgetByName(panel_,"Label_searchResult");

		//label start
		label_star = (UILabel *)UIHelper::seekWidgetByName(panel_,"Label_37");
		label_star->setText(StringDataManager::getString("SearchGeneral_Start"));

		loadingTime = UILoadingBar::create();
		loadingTime->setTexture("res_ui/takegeneral/time2.png");
		loadingTime->setPosition(ccp(751,207));
		loadingTime->setPercent(100);
		loadingTime->setRotation(270);
		m_Layer->addWidget(loadingTime);

		//------------
		generalIConMengban = CCScale9Sprite::create(CCRectMake(7, 7, 1, 1) , "res_ui/zhezhao80.png");
		generalIConMengban->setOpacity(255);
		generalIConMengban->setPreferredSize(CCSize(510,401));
		generalIConMengban->setAnchorPoint(ccp(0,0));
		generalIConMengban->setPosition(ccp(220,41));
		m_Layer->addChild(generalIConMengban,2);
		
		label_mengban_ = CCLabelTTF::create(StringDataManager::getString("SearchGeneral_Start_labelShow"),APP_FONT_NAME,30);
		label_mengban_->setAnchorPoint(ccp(0.5f,0.5f));
		label_mengban_->setPosition(ccp(generalIConMengban->getContentSize().width/2,generalIConMengban->getContentSize().height/2));
		generalIConMengban->addChild(label_mengban_);

		/*
		progress_time = CCProgressTimer::create(CCSprite::create("res_ui/takegeneral/time2.png"));
		progress_time->setType(kCCProgressTimerTypeBar);
		progress_time->setMidpoint(ccp(0,0));
		progress_time->setBarChangeRate(ccp(0,1));
		progress_time->setAnchorPoint(ccp(0,0));
		progress_time->setPosition(ccp(100, 117));
		progress_time->setPercentage(100);
		m_pUiLayer->addChild(progress_time);
		*/

		clipperNode_ = CCClippingNode::create();
 		clipperNode_->setAnchorPoint(ccp(0.5f,0.5f));
 		clipperNode_->setPosition(ccp(475,240));
		clipperNode_->setContentSize(CCSizeMake(510,401));
		m_Layer->addChild(clipperNode_);

		CCDrawNode *stencil = CCDrawNode::create();
		CCPoint rectangle[4];
		rectangle[0] = ccp(0, 0);
		rectangle[1] = ccp(clipperNode_->getContentSize().width, 0);
		rectangle[2] = ccp(clipperNode_->getContentSize().width, clipperNode_->getContentSize().height);
		rectangle[3] = ccp(0, clipperNode_->getContentSize().height);

		ccColor4F white = {1, 1, 1, 1};
		stencil->drawPolygon(rectangle, 4, white, 1, white);
		clipperNode_->setStencil(stencil);

		updateResult();
		initHeadPortrait(NULL,0);
		this->scheduleUpdate();
		
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(winSize);
		return true;
	}
	return false;
}

void SearchGeneral::updateResult(bool isAction)
{
	char aucBuf[50];
	sprintf(aucBuf, "%d / %d %s", m_iSuccessNum, SUCCESS_NUM, StringDataManager::getString("SearchGeneral_SelectResult"));
	label_result->setText(aucBuf);

	if (isAction)
	{
		CCSequence * seq_ = CCSequence::create(
			CCScaleTo::create(0.2f,1.6f),
			//CCScaleTo::create(0.5f,0.5f),
			CCScaleTo::create(0.2f,1.0f),
			NULL);
		label_result->runAction(seq_);
	}
}

void SearchGeneral::initHeadPortrait(CCNode* sender, void* data)
{
	UILayer * layer_GeneralIcon = UILayer::create();
	layer_GeneralIcon->ignoreAnchorPointForPosition(false);
	layer_GeneralIcon->setAnchorPoint(ccp(0,0));
	layer_GeneralIcon->setPosition(ccp(0,0));
	layer_GeneralIcon->setTag(LAYERTAG_GENERALICON);
	clipperNode_->addChild(layer_GeneralIcon);

	char* path[GENERAL_PATH_NUM] = {"BlackMarket", "CaiWenJi", "CaoCao", "CaoPi", "Consignment", 
									"cz", "DaQiao", "DianWei", "DiaoChan", "DiaoXiaoChan", "GanNing", 
									"ChunYuQiong", "GuanYu", "GuoJia", "HuangYueYing", "HuangZhong", 
									"HuaTuo", "LiuBei", "LuSu", "LuXun", "LvBu", "MaChao", 
									"MiFuRen", "MrShuiJing", "NewbieGuide", "PangTong", "shop", 
									"SiMaYi", "SunCe", "SunQuan", "SunShangXiang", "TaiShiCi", "WangYun", 
									"WareHouse", "WenChou", "XiaHouDun", "XiaHouYuan", "XiaoQiao", "XuChu", 
									"XuShu", "XuYou", "YanLiang", "YuanShao", "YuanShu", 
									"ZhangChunHua", "ZhangFei", "ZhangHe", "ZhangJiao", "ZhangLiao", "ZhaoYun", 
									"ZhenJi", "ZhouYu", "ZhuGeLiang"};

	char* output[QUANTITY_PATH_NUM];

	cc_timeval psv;
	CCTime::gettimeofdayCocos2d(&psv, NULL);
	unsigned long long seed = psv.tv_sec*1000 + psv.tv_usec/1000;
	srand(seed);
     
	int size = GENERAL_PATH_NUM;
	for (int i = 0; i < QUANTITY_PATH_NUM; i++) {
		int num = GameUtils::getRandomNum(size);
		output[i] = path[num];
		path[num] = path[size-1];
		size--;
	}
	m_iAnswer = GameUtils::getRandomNum(QUANTITY_PATH_NUM);

	//
	if ((int)data == 1)
	{
		if (m_Layer->getChildByTag(LAYERTAG_SCROLL))
		{
			UILayer *layer_ = (UILayer *)m_Layer->getChildByTag(LAYERTAG_SCROLL);
			layer_->removeFromParentAndCleanup(true);
		}

		UILayer * layer_scroll_ = UILayer::create();
		layer_scroll_->setTag(LAYERTAG_SCROLL);
		m_Layer->addChild(layer_scroll_);

		CCScrollView * scrollView_generalIcon = CCScrollView::create(CCSizeMake(97,65));
		scrollView_generalIcon->setContentSize(CCSizeMake(97,65));
		scrollView_generalIcon->ignoreAnchorPointForPosition(false);
		//scrollView_generalIcon->setTouchEnabled(true);
		scrollView_generalIcon->setDirection(kCCScrollViewDirectionVertical);
		scrollView_generalIcon->setAnchorPoint(ccp(0.5f,0.5f));
		scrollView_generalIcon->setPosition(ccp(136,325));
		scrollView_generalIcon->setBounceable(true);
		scrollView_generalIcon->setClippingToBounds(true);
		layer_scroll_->addChild(scrollView_generalIcon);	
		
		std::string fullPath;
		fullPath.append("res_ui/npc/");
		fullPath.append(output[m_iAnswer]);
		fullPath.append(".anm");

		CCLegendAnimation * pAnswer = CCLegendAnimation::create(fullPath);
		pAnswer->setPlayLoop(true);
		pAnswer->setReleaseWhenStop(false);
		pAnswer->setAnchorPoint(ccp(0.5f,0));
		pAnswer->setPosition(ccp(0,73));
		pAnswer->setTag(100);
		pAnswer->setScale(0.7f);
		scrollView_generalIcon->addChild(pAnswer);

		pAnswer->runAction(CCMoveBy::create(m_gameEndTIme,ccp(0,-73)));
		scrollView_generalIcon->runAction(CCMoveBy::create(m_gameEndTIme,ccp(0,73)));

		std::string textureFile = pAnswer->getAnimationData()->getImagePath(0);
	}
	////
	for(int i=0; i<QUANTITY_PATH_NUM; i++)
	{
		std::string fullPath;
		fullPath.append("res_ui/npc/");
		fullPath.append(output[i]);
		fullPath.append(".anm");

		UIButton * btn = UIButton::create();
		if ((int)data == 1)
		{
			btn->setTouchEnable(true);
		}
		btn->loadTextures("res_ui/takegeneral/carddi.png", "res_ui/takegeneral/carddi.png", "");
		btn->setTag(i);
		if ((int)data == 1)
		{
			btn->setPosition(ccp(55+(i%ROW_NUM)*100+600, 362-(i/ROW_NUM)*80));
		}else
		{
			btn->setPosition(ccp(55+(i%ROW_NUM)*100, 362-(i/ROW_NUM)*80));
		}
		btn->addReleaseEvent(this, coco_releaseselector(SearchGeneral::callBackSelected));
		btn->setPressedActionEnabled(true);
		layer_GeneralIcon->addWidget(btn);
		
		//cardlight
		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/takegeneral/cardlight.png");
		mengban->setAnchorPoint(ccp(0,1));
		mengban->setPosition(ccp( 2-btn->getContentSize().width/2,btn->getContentSize().height/2));
		btn->addChild(mengban);

		CCLegendAnimation* pHeadPortrait = CCLegendAnimation::create(fullPath);
		pHeadPortrait->setPlayLoop(true);
		pHeadPortrait->setReleaseWhenStop(false);
		pHeadPortrait->setAnchorPoint(ccp(0.5f,0.5f));
		pHeadPortrait->setPosition(ccp(-45, -33));
		pHeadPortrait->setScale(0.7f);
		btn->addCCNode(pHeadPortrait);

		//ease out
		if ((int)data == 1)
		{
			CCMoveTo * moveTo_ = CCMoveTo::create(0.3f,ccp(55+(i%ROW_NUM)*100, 362-(i/ROW_NUM)*80));
			
			CCSequence * sequence = CCSequence::create(
				CCDelayTime::create(0.1f),
				CCEaseBackOut::create(moveTo_),
				NULL);

			btn->runAction(sequence);
		}
	}
}

void SearchGeneral::callBackSelected(CCObject * obj)
{
	if (m_iState == kStateSuccess)
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("SearchGeneral_GameIsOver"));
		return;
	}

	if (m_iState == kStateFail)
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("SearchGeneral_GameIsPlayerAgain"));
		return;
	}

	long long currentTime = GameUtils::millisecondNow();
	UIButton *btn = (UIButton *)obj;
	if (this->getFirstBtnTouchTime() != 0 && currentTime - this->getFirstBtnTouchTime() <300)
	{
		return;
	}
	this->setFirstBtnTouchTime(currentTime);

	btn->setTouchEnable(false);

	if(btn->getTag() == m_iAnswer)
	{
		m_iSuccessNum++;
		if(m_iSuccessNum > SUCCESS_NUM)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("SearchGeneral_GameWin"));
		}
		else
		{
			updateResult(true);
			
			if (m_iSuccessNum == SUCCESS_NUM)
			{
				CCSprite * sp_right = CCSprite::create("res_ui/font/right.png");
				sp_right->setAnchorPoint(ccp(0.5f,0.5f));
				sp_right->setPosition(ccp(winSize.width/2,winSize.height/2));
				m_Layer->addChild(sp_right,5);

				CCSequence * seq_ = CCSequence::create(
					CCDelayTime::create(0.5f),
					CCRemoveSelf::create(),
					NULL);
				sp_right->runAction(seq_);

				m_iSuccessNum = 0;
				btn_start->setVisible(true);
				label_star->setText(StringDataManager::getString("SearchGeneral_Finish"));

				CCSequence * seq_1 = CCSequence::create(
					CCDelayTime::create(1.0f),
					CCCallFuncN::create(this,callfuncN_selector(SearchGeneral::setCurState)),
					NULL);
				this->runAction(seq_1);
				//
			}else
			{
				removeHeadPortrait();
				
				CCSprite * sp_right = CCSprite::create("res_ui/font/right.png");
				sp_right->setAnchorPoint(ccp(0.5f,0.5f));
				sp_right->setPosition(ccp(winSize.width/2,winSize.height/2));
				m_Layer->addChild(sp_right,5);

				CCSequence * seq_ = CCSequence::create(
					CCDelayTime::create(0.5f),
					CCRemoveSelf::create(),
					NULL);
				sp_right->runAction(seq_);
			}
		}
	}
	else
	{
		//initHeadPortrait();
		removeHeadPortrait();
		
		CCSprite * sp_wrong = CCSprite::create("res_ui/font/wrong.png");
		sp_wrong->setAnchorPoint(ccp(0.5f,0.5f));
		sp_wrong->setPosition(ccp(winSize.width/2,winSize.height/2));
		m_Layer->addChild(sp_wrong,5);

		CCSequence * seq_ = CCSequence::create(
			CCDelayTime::create(0.5f),
			CCRemoveSelf::create(),
			NULL);

		sp_wrong->runAction(seq_);
	}
}

void SearchGeneral::callBackStart(CCObject * obj)
{
	UIButton* btn = (UIButton*)obj;

	switch(m_iState)
	{
	case kStateStart:
		{
			m_iState = kStatePlay;

			btn->setVisible(false);
			generalIConMengban->setVisible(false);

			//initHeadPortrait();
			removeHeadPortrait();

			struct timeval m_sStartTime;
			gettimeofday(&m_sStartTime, NULL);
			m_dStartTime = m_sStartTime.tv_sec*1000 + m_sStartTime.tv_usec/1000;
		}break;
	case kStateSuccess:
		{
			m_iState = kStateWait;
			
			GameMessageProcessor::sharedMsgProcessor()->sendReq(7013, (void *)m_level, (void *)(com::future::threekingdoms::server::transport::protocol::FIND_GENERAL));
			this->closeAnim();
		}break;
	case kStateFail:
		{
			m_iState = kStatePlay;

			btn->setVisible(false);
			generalIConMengban->setVisible(false);

			updateResult();
			//initHeadPortrait();
			removeHeadPortrait();

			struct timeval m_sStartTime;
			gettimeofday(&m_sStartTime, NULL);
			m_dStartTime = m_sStartTime.tv_sec*1000 + m_sStartTime.tv_usec/1000;
		}break;
	}
}

bool SearchGeneral::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
	return true;
}
void SearchGeneral::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
{

}
void SearchGeneral::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
{

}
void SearchGeneral::ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent)
{

}

void SearchGeneral::update(float dt)
{
	switch(m_iState)
	{
	case kStatePlay:
		{
			struct timeval m_sStartTime;
			gettimeofday(&m_sStartTime, NULL);
			m_dEndedTime = m_sStartTime.tv_sec*1000 + m_sStartTime.tv_usec/1000;
			long double time = m_dEndedTime - m_dStartTime;
			if(time >= m_showIconTime)
			{
				loadingTime->setPercent(0);
				//GameView::getInstance()->showAlertDialog(StringDataManager::getString("SearchGeneral_GameIsGoOnIsFail"));
				btn_start->setVisible(true);
				m_iState = kStateFail;

				m_iSuccessNum = 0;
				label_star->setText(StringDataManager::getString("SearchGeneral_SatrtAgain"));
			}
			else
			{
				long double passTime = m_showIconTime-time;
				int percent = (passTime*100)/m_showIconTime;

				//pass_timePrecfect -= (100/m_showIconTime)*dt;
				loadingTime->setPercent(percent);
			}
		}
		break;
	case kStateWait:
		{
			//I am a lazy state zzzzz
			//loadingTime->setPercent(100);
		}
		break;
	case kStateSuccess:
		{
			UILayer * layer_ = (UILayer *)clipperNode_->getChildByTag(LAYERTAG_GENERALICON);
			if (layer_)
			{
				for(int i=0; i<QUANTITY_PATH_NUM; i++)
				{
					UIButton * btn_ = (UIButton *)layer_->getWidgetByTag(i);
					if (btn_)
					{
						btn_->setTouchEnable(false);
					}
				}
			}

			generalIConMengban->setVisible(true);
			label_mengban_->setString(StringDataManager::getString("SearchGeneral_GameIsGoOn"));
		}break;
	case kStateFail:
		{
			UILayer * layer_ = (UILayer *)clipperNode_->getChildByTag(LAYERTAG_GENERALICON);
			if (layer_)
			{
				for(int i=0; i<QUANTITY_PATH_NUM; i++)
				{
					UIButton * btn_ = (UIButton *)layer_->getWidgetByTag(i);
					if (btn_)
					{
						btn_->setTouchEnable(false);
					}
				}
			}

			generalIConMengban->setVisible(true);
			label_mengban_->setString(StringDataManager::getString("SearchGeneral_GameIsPlayerAgain"));
		}break;
	default:
		break;
	}

}

void SearchGeneral::callBackClose( CCObject * obj )
{
	if (m_iState == kStateSuccess)
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(7013, (void *)m_level, (void *)(com::future::threekingdoms::server::transport::protocol::FIND_GENERAL));
	}
	this->closeAnim();
}

void SearchGeneral::removeHeadPortrait()
{
	UILayer * layer_ = (UILayer *)clipperNode_->getChildByTag(LAYERTAG_GENERALICON);
	if (layer_)
	{
		CCMoveTo * moveTo_ = CCMoveTo::create(0.5f,ccp(-500,0));
		CCSequence * sequence = CCSequence::create(
			CCDelayTime::create(0.2f),
			CCEaseBackOut::create(moveTo_),
			CCRemoveSelf::create(),
			NULL);
		layer_->runAction(sequence);
	}

	if (m_Layer->getChildByTag(LAYERTAG_SCROLL))
	{
		UILayer *layer_ = (UILayer *)m_Layer->getChildByTag(LAYERTAG_SCROLL);
		layer_->removeFromParentAndCleanup(true);
	}
	
	CCActionInterval * action1 =(CCActionInterval *)CCSequence::create(
		CCDelayTime::create(0.5f),
		CCCallFuncND::create(this, callfuncND_selector(SearchGeneral::initHeadPortrait),(void *)1),
		NULL);

	this->runAction(action1);
}

void SearchGeneral::setCurState(CCNode * node)
{
	m_iState = kStateSuccess;

// 	MainScene * mainScene = (MainScene *)GameView::getInstance()->getMainUIScene();
// 	if (NULL != mainScene)
// 	{
// 		mainScene->addInterfaceAnm("gxgg/gxgg.anm");
// 	}

	MainAnimationScene * mainAnmScene = GameView::getInstance()->getMainAnimationScene();
	if (mainAnmScene)
	{
		mainAnmScene->addInterfaceAnm("gxgg/gxgg.anm");
	}
}

void SearchGeneral::setFirstBtnTouchTime( long long time )
{
	m_touchTime = time;
}

long long SearchGeneral::getFirstBtnTouchTime()
{
	return m_touchTime;
}
