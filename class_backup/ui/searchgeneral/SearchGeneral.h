#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../../ui/extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;
class SearchGeneral:public UIScene
{
public:
	SearchGeneral(void);
	~SearchGeneral(void);

	static SearchGeneral *create(int diffType);
	bool init(int diffType);
	
	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);
	virtual void update(float dt);

	void callBackClose(CCObject * obj);
private:
	void callBackSelected(CCObject * obj);
	//void initHeadPortrait(bool isCreate = true);
	void initHeadPortrait(CCNode* sender, void* data);
	void callBackStart(CCObject * obj);
	void updateResult(bool isAction = false);

	void removeHeadPortrait();

	void setCurState(CCNode * node);
private:
	int m_iAnswer;
	int m_iState;
	long double m_dStartTime;
	long double m_dEndedTime;
	int m_iSuccessNum;
	int pass_timePrecfect;
	//
	long long m_showIconTime;
	long long m_gameEndTIme;
	CCSize winSize;

	long long m_touchTime;
	void setFirstBtnTouchTime(long long time);
	long long getFirstBtnTouchTime();
private:
	UIButton *btn_start;

	UILabel * label_star;

	UIImageView * image_icon;
	UILabel* label_result;

	UILoadingBar* loadingTime;

	CCScale9Sprite * generalIConMengban;
	CCLabelTTF * label_mengban_;

	CCClippingNode * clipperNode_;
	CCLayer * layer_Clip;

	UILayer * m_Layer;
private:
	int m_level;
};

