#include "GetFlower.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../extensions/CCRichLabel.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"


GetFlower::GetFlower(void)
{
}


GetFlower::~GetFlower(void)
{
}

GetFlower * GetFlower::create(long long id,std::string name,std::string str)
{
	GetFlower * flower_ = new GetFlower();
	if (flower_ && flower_->init(id,name,str))
	{
		flower_->autorelease();
		return flower_;
	}
	CC_SAFE_DELETE(flower_);
	return NULL;
}

bool GetFlower::init(long long id,std::string name,std::string str)
{
	if (UIScene::init())
	{
		m_playerId = id;
		m_name = name;
		//m_vipLv = 

		//
		winsize =CCSizeMake(800,480);
		UILayer * layer = UILayer::create();
		layer->ignoreAnchorPointForPosition(false);
		layer->setAnchorPoint(ccp(0.5f,0.5f));
		layer->setPosition(ccp(winsize.width/2,winsize.height/2));
		layer->setContentSize(winsize);
		addChild(layer);
		
		CCScale9Sprite * backGroundSp = CCScale9Sprite::create("res_ui/dikuang_new.png");
		backGroundSp->setCapInsets(CCRect(0,0,0,0));
		backGroundSp->setPreferredSize(CCSizeMake(376,198));
		backGroundSp->setAnchorPoint(ccp(0.5f,0.5f));
		backGroundSp->setPosition(ccp(winsize.width/2,winsize.height/2));
		layer->addChild(backGroundSp);

		CCScale9Sprite * backGroundSp_frame = CCScale9Sprite::create("res_ui/LV4_didia.png");
		backGroundSp_frame->setCapInsets(CCRect(13,14,1,1));
		backGroundSp_frame->setPreferredSize(CCSizeMake(300,108));
		backGroundSp_frame->setAnchorPoint(ccp(0.5f,1.0f));
		backGroundSp_frame->setPosition(ccp(backGroundSp->getContentSize().width/2-2,180));
		backGroundSp->addChild(backGroundSp_frame);

		CCRichLabel *labelLink=CCRichLabel::createWithString(str.c_str(),CCSizeMake(290,50),NULL,NULL,0,20,2);
		labelLink->setAnchorPoint(ccp(0,1));
		labelLink->setPosition(ccp(45,backGroundSp->getContentSize().height-30));
		backGroundSp->addChild(labelLink);

		//create button
		UILayer * layerbutton = UILayer::create();
		layerbutton->ignoreAnchorPointForPosition(false);
		layerbutton->setAnchorPoint(ccp(0.5f,0.5f));
		layerbutton->setPosition(ccp(winsize.width/2,winsize.height/2));
		layerbutton->setContentSize(winsize);
		layer->addChild(layerbutton);

		UIButton * buttonPrivate = UIButton::create();
		buttonPrivate->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		buttonPrivate->setTouchEnable(true);
		buttonPrivate->setPressedActionEnabled(true);
		buttonPrivate->addReleaseEvent(this,coco_releaseselector(GetFlower::callbackPrivate));
		buttonPrivate->setAnchorPoint(ccp(0.5f,0.5f));
		buttonPrivate->setScale9Enable(true);
		buttonPrivate->setScale9Size(CCSizeMake(110,43));
		buttonPrivate->setCapInsets(CCRect(18,9,2,23));
		buttonPrivate->setPosition(ccp(layer->getContentSize().width/2 - buttonPrivate->getSize().width + 30,
																layer->getContentSize().height/2 - backGroundSp->getContentSize().height/2 +48));
		layerbutton->addWidget(buttonPrivate);

		UILabel* label_private = UILabel::create();
		label_private->setAnchorPoint(ccp(0.5f,0.5f));
		label_private->setPosition(ccp(0,0));
		label_private->setFontSize(18);
		label_private->setText(StringDataManager::getString("giveFlower_private_"));
		buttonPrivate->addChild(label_private);

		UIButton * buttonThanks = UIButton::create();
		buttonThanks->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		buttonThanks->setTouchEnable(true);
		buttonThanks->setPressedActionEnabled(true);
		buttonThanks->addReleaseEvent(this,coco_releaseselector(GetFlower::callBackThank));
		buttonThanks->setAnchorPoint(ccp(0.5f,0.5f));
		buttonThanks->setScale9Enable(true);
		buttonThanks->setScale9Size(CCSizeMake(110,43));
		buttonThanks->setCapInsets(CCRect(18,9,2,23));
		buttonThanks->setPosition(ccp(layer->getContentSize().width/2 + buttonPrivate->getSize().width - 30,buttonPrivate->getPosition().y));
		layerbutton->addWidget(buttonThanks);

		UILabel* label_thanks = UILabel::create();
		label_thanks->setAnchorPoint(ccp(0.5f,0.5f));
		label_thanks->setPosition(ccp(0,0));
		label_thanks->setFontSize(20);
		label_thanks->setText(StringDataManager::getString("giveFlower_thanks"));
		buttonThanks->addChild(label_thanks);


		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(winsize);
		return true;
	}
	return false;
}

void GetFlower::onEnter()
{
	UIScene::onEnter();
}

void GetFlower::onExit()
{
	UIScene::onExit();
}

void GetFlower::callbackPrivate( CCObject * obj )
{
	int countryId = 0;
	int vipLv = 0;
	int lv = 0;
	int pressionId = 0;

	MainScene * mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
	mainscene_->addPrivateChatUi(m_playerId,m_name,countryId,vipLv,lv,pressionId);

	this->closeAnim();
}

void GetFlower::callBackThank( CCObject * obj )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5141,(void *)m_playerId);

	this->closeAnim();
}
