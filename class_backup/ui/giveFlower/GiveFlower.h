
#ifndef _UI_GIVEFLOWER_H_
#define _UI_GIVEFLOWER_H_

#include "../extensions/UIScene.h"

#define FLOWERTYPE_1 "baihe"
#define FLOWERTYPE_2 "fenmeigui"
#define FLOWERTYPE_3 "hongmeigui"

class RichTextInputBox;
class GiveFlower:public UIScene
{
public:
	GiveFlower(void);
	~GiveFlower(void);

	static GiveFlower * create(long long id,std::string name,int pression,int level,int countryId,int vipLv);
	bool init(long long id,std::string name,int pression,int level,int countryId,int vipLv);

	void onEnter();
	void onExit();

	//
	void callBackFlowerType(CCObject * obj);

	//set flower type
	void setFlowerName(std::string flowerName);
	std::string getFlowerName();
	//set flower  num
	void setFlowerNum(int num);
	int getFlowerNum();

	//model flower num
	void callBackModelFlowerNum(CCObject * obj);
	//use define num
	void callBackDefineFlowerNum(CCObject * obj);

	void showFlowerNum(CCObject * obj);

	void setModelType(int type);
	int getModelType();
	void setModelPublic(CCObject * obj,CheckBoxEventType type);
	void setModelPrivate(CCObject * obj,CheckBoxEventType type);

	//
	void setAllUIcheckTypeBoxIsNull();
	void setAllUIcheckNumBoxIsNull();

	//uicheckbox is select flower type
	void UICheckBoxType1(CCObject * obj,CheckBoxEventType type);
	void UICheckBoxType2(CCObject * obj,CheckBoxEventType type);
	void UICheckBoxType3(CCObject * obj,CheckBoxEventType type);

	//uicheckbox select flower num
	void UICheckBoxNum1(CCObject * obj,CheckBoxEventType type);
	void UICheckBoxNum11(CCObject * obj,CheckBoxEventType type);
	void UICheckBoxNum99(CCObject * obj,CheckBoxEventType type);
	void UICheckBoxNum365(CCObject * obj,CheckBoxEventType type);
	void UICheckBoxNum999(CCObject * obj,CheckBoxEventType type);

	void UIcheckBoxDefineNum(CCObject * obj,CheckBoxEventType type);

	void callBackOpenShop(CCObject * obj);

	void callBackGiveFlower(CCObject * obj);
	void callBackExit(CCObject * obj);

public:
	UICheckBox * checkbox_public;
	UICheckBox * checkbox_private;

	UICheckBox * checkbox_flowerType1;
	UICheckBox * checkbox_flowerType2;
	UICheckBox * checkbox_flowerType3;


	UICheckBox * checkbox_num1;
	UICheckBox * checkbox_num11;
	UICheckBox * checkbox_num99;
	UICheckBox * checkbox_num365;
	UICheckBox * checkbox_num999;

	UICheckBox * checkbox_Define;
public:
	struct GiveFlowerStruct
	{
		long long playerId;
		std::string playerName;
		int flowerNumber;
		std::string flowerName;
		int isShowName;
		std::string flowerSaid;
	};
private:
	CCSize winsize;
	RichTextInputBox * inputFlowerNum;
	RichTextInputBox * inputFlowerSaid;

	UILabel * label_showFlowerNum;

	long long m_playerId;
	std::string m_playerName;

	std::string m_flowerName;
	int m_giveFlowerNum;
	int m_giveFlowerModel;
};

#endif