

#ifndef _OFFLINEARENA_OFFLINEARENCOUNTDOWNUI_
#define _OFFLINEARENA_OFFLINEARENCOUNTDOWNUI_

#include "../extensions/UIScene.h"

class OffLineArenaCountDownUI : public UIScene
{
public:
	OffLineArenaCountDownUI();
	~OffLineArenaCountDownUI();

	static OffLineArenaCountDownUI * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	virtual void update(float dt);

	void showStarCountTime();
private:
	int m_nRemainTime;
	//UILabel *l_countDown;
	UIImageView * imageView_countDown;
};

#endif

