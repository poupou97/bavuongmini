
#ifndef _OFFLINEARENA_RANKLISTUI_
#define _OFFLINEARENA_RANKLISTUI_

#include "../extensions/UIScene.h"
#include "cocos-ext.h"
#include "cocos2d.h"
#include "../generals_ui/GeneralsListBase.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CSimplePartner;

class RankListUI : public GeneralsListBase,public CCTableViewDelegate,public CCTableViewDataSource
{
public:
	RankListUI();
	~RankListUI();

	static RankListUI * create(int myRanking);
	bool init(int myRanking);

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);

	void CloseEvent(CCObject *pSender);
	void MakeFriendEvent(CCObject *pSender);
	void LookOverEvent(CCObject *pSender);

	void RefreshData();
	void RefreshDataWithOutChangeOffSet();
	// LiuLiang++(用于返回字体的颜色值 1、2、3名和其他名)
	ccColor3B getFontColor(int nRank);					
	

private:
	CCTableView* m_tableView;
	//我的排名
	UILabel * l_selfRanking;

	int lastSelectCellId;
	int selectCellId;

public:
	//排行榜
	std::vector<CSimplePartner*> curRankingList;
};

#endif

