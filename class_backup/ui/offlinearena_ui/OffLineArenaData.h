#ifndef _OFFLINEARENA_OFFLINEARENADATA_H_
#define _OFFLINEARENA_OFFLINEARENADATA_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

/////////////////////////////////
/**
 * 竞技场数据类
 * @author yangjun 
 * @version 0.1.0
 * @date 2014.05.30
 */

class OffLineArenaData
{
public:
	OffLineArenaData(void);
	~OffLineArenaData(void);

	enum OLA_Status
	{
		s_none = 0,
		s_inCountDown,
		s_inBattle
	};

public: 
	static OffLineArenaData * s_arenaData;
	static OffLineArenaData * getInstance();

	int getCurStatus();
	void setCurStatus(int _value);

	//我的排名
	int m_nMyRoleRanking;
	int m_nCurStatus;

	//要挑战的玩家排名
	int m_nTargetRanking;
};

#endif
