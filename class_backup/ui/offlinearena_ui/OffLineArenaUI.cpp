#include "OffLineArenaUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameScene.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "RankListUI.h"
#include "HonorShopUI.h"
#include "FightNotesUI.h"
#include "../../messageclient/element/CHonorCommodity.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/CPartner.h"
#include "AppMacros.h"
#include "../../utils/StaticDataManager.h"
#include "../generals_ui/GeneralsUI.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../messageclient/element/CPartner.h"
#include "BuyFightTimesUI.h"
#include "../../gamescene_state/role/ActorUtils.h"
#include "../../gamescene_state/role/GameActorAnimation.h"
#include "../../messageclient/element/CActiveRole.h"
#include "../../utils/GameConfig.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/CCTeachingGuide.h"

OffLineArenaUI::OffLineArenaUI():
m_nCurSelectIndex(-1),
m_nRemainBuytimes(0)
{
}


OffLineArenaUI::~OffLineArenaUI()
{
	std::vector<CPartner*>::iterator iter_parther;
	for (iter_parther = curArenaList.begin(); iter_parther != curArenaList.end(); ++iter_parther)
	{
		delete *iter_parther;
	}
	curArenaList.clear();

	std::vector<CHonorCommodity*>::iterator iter;
	for (iter = curHonorCommodityList.begin(); iter != curHonorCommodityList.end(); ++iter)
	{
		delete *iter;
	}
	curHonorCommodityList.clear();
}

OffLineArenaUI * OffLineArenaUI::create()
{
	OffLineArenaUI * offLineArenaUI = new OffLineArenaUI();
	if(offLineArenaUI && offLineArenaUI->init())
	{
		offLineArenaUI->autorelease();
		return offLineArenaUI;
	}
	CC_SAFE_DELETE(offLineArenaUI);
	return NULL;
}

bool OffLineArenaUI::init()
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();

		u_layer = UILayer::create();
		u_layer->ignoreAnchorPointForPosition(false);
		u_layer->setAnchorPoint(ccp(0.5f,0.5f));
		u_layer->setContentSize(CCSizeMake(800, 480));
		u_layer->setPosition(CCPointZero);
		u_layer->setPosition(ccp(winsize.width/2,winsize.height/2));
		addChild(u_layer);

		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winsize);
		mengban->setAnchorPoint(CCPointZero);
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);

		//加载UI
		if(LoadSceneLayer::OffLineArenaLayer->getWidgetParent() != NULL)
		{
			LoadSceneLayer::OffLineArenaLayer->removeFromParentAndCleanup(false);
		}
		ppanel = LoadSceneLayer::OffLineArenaLayer;
		ppanel->setAnchorPoint(ccp(0.5f,0.5f));
		ppanel->getValidNode()->setContentSize(CCSizeMake(800, 480));
		ppanel->setPosition(CCPointZero);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnable(true);
		ppanel->setPosition(ccp(winsize.width/2,winsize.height/2));
		m_pUiLayer->addWidget(ppanel);

		const char * firstStr = StringDataManager::getString("UIName_jing");
		const char * secondStr = StringDataManager::getString("UIName_ji");
		const char * thirdStr = StringDataManager::getString("UIName_chang");
		CCArmature * atmature = MainScene::createPendantAnm(firstStr,secondStr,thirdStr,"");
		atmature->setPosition(ccp(30,240));
		u_layer->addChild(atmature);

		//关闭按钮
		UIButton * btn_close = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		btn_close->setTouchEnable(true);
		btn_close->setPressedActionEnabled(true);
		btn_close->addReleaseEvent(this, coco_releaseselector(OffLineArenaUI::CloseEvent));
		//排行榜
		UIButton * btn_rankList = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_ranking");
		btn_rankList->setTouchEnable(true);
		btn_rankList->setPressedActionEnabled(true);
		btn_rankList->addReleaseEvent(this, coco_releaseselector(OffLineArenaUI::RankListEvent));
		//荣誉商店
		UIButton * btn_honorShop = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_honorShop");
		btn_honorShop->setTouchEnable(true);
		btn_honorShop->setPressedActionEnabled(true);
		btn_honorShop->addReleaseEvent(this, coco_releaseselector(OffLineArenaUI::HonorShopEvent));
		//战斗记录
		UIButton * btn_fightNotes = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_fightNotes");
		btn_fightNotes->setTouchEnable(true);
		btn_fightNotes->setPressedActionEnabled(true);
		btn_fightNotes->addReleaseEvent(this, coco_releaseselector(OffLineArenaUI::FightNotesEvent));
		//挑战
		btn_challenge = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_dare");
		btn_challenge->setTouchEnable(true);
		btn_challenge->setPressedActionEnabled(true);
		btn_challenge->addReleaseEvent(this, coco_releaseselector(OffLineArenaUI::ChallengeEvent));
		//购买次数
		UIButton * btn_buyNum = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_buyNumber");
		btn_buyNum->setTouchEnable(true);
		btn_buyNum->setPressedActionEnabled(true);
		btn_buyNum->addReleaseEvent(this, coco_releaseselector(OffLineArenaUI::BuyNumEvent));
		//领取奖励
		btn_getRewards = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_takeReward");
		btn_getRewards->setTouchEnable(true);
		btn_getRewards->setPressedActionEnabled(true);
		btn_getRewards->addReleaseEvent(this, coco_releaseselector(OffLineArenaUI::GetRewardsEvent));
		btn_getRewards->setVisible(false);

		imageView_di = (UIImageView*)UIHelper::seekWidgetByName(ppanel,"ImageView_di");
		imageView_ming = (UIImageView*)UIHelper::seekWidgetByName(ppanel,"ImageView_ming");
        l_roleRankingValue = (UILabelBMFont*)UIHelper::seekWidgetByName(ppanel,"LabelBMFont_roleRankValue");
		l_roleHonorValue = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_roleHonorValue");
		m_nRoleHonor = GameView::getInstance()->myplayer->player->honorpoint();
		l_honorValue = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_honor");
		//剩余领取荣誉值时间
		l_remainTimeDes = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_93");
		l_remainTime = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_time");

		l_fightTime = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_fightTimesValue");
		imageView_remainTime = (UIImageView *)UIHelper::seekWidgetByName(ppanel,"ImageView_remainTimes");

		//send req for arenaList
		GameMessageProcessor::sharedMsgProcessor()->sendReq(3000);
		//send req for honorShopList
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1706);

		this->schedule(schedule_selector(OffLineArenaUI::update),1.0f);

		this->setContentSize(winsize);
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);

		return true;
	}
	return false;
}

void OffLineArenaUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void OffLineArenaUI::onExit()
{
	UIScene::onExit();
}

bool OffLineArenaUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return true;
}

void OffLineArenaUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void OffLineArenaUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void OffLineArenaUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void OffLineArenaUI::CloseEvent( CCObject *pSender )
{
	this->closeAnim();
}

void OffLineArenaUI::update( float dt )
{
	m_nRemaintimes -= 1000;
	//剩余领取荣誉值时间
	l_remainTime->setText(timeFormatToString(m_nRemaintimes/1000).c_str());

	if (m_nRemaintimes < 0)
	{
		//可以领取了
		btn_getRewards->setVisible(true);
		l_remainTimeDes->setVisible(false);
		l_remainTime->setVisible(false);
	}
	else
	{
		btn_getRewards->setVisible(false);
		l_remainTimeDes->setVisible(true);
		l_remainTime->setVisible(true);
	}


	m_nRoleHonor = GameView::getInstance()->myplayer->player->honorpoint();
	char s_roleHonor[20];
	sprintf(s_roleHonor,"%d",m_nRoleHonor);
	l_roleHonorValue->setText(s_roleHonor);
}

void OffLineArenaUI::RankListEvent( CCObject *pSender )
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(KTagRankListUI) == NULL)
	{
		CCSize winSize=CCDirector::sharedDirector()->getVisibleSize();
		RankListUI * rankListUI = RankListUI::create(m_nRoleRanking);
		rankListUI->ignoreAnchorPointForPosition(false);
		rankListUI->setAnchorPoint(ccp(0.5f,0.5f));
		rankListUI->setPosition(ccp(winSize.width/2,winSize.height/2));
		rankListUI->setTag(KTagRankListUI);
		GameView::getInstance()->getMainUIScene()->addChild(rankListUI);

		rankListUI->isReqNewly = true;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(3002,(void*)0,(void*)20);
	}
}

void OffLineArenaUI::HonorShopEvent( CCObject *pSender )
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(KTagHonorShopUI) == NULL)
	{
		CCSize winSize=CCDirector::sharedDirector()->getVisibleSize();
		HonorShopUI * honorShopUI = HonorShopUI::create(curHonorCommodityList);
		honorShopUI->ignoreAnchorPointForPosition(false);
		honorShopUI->setAnchorPoint(ccp(0.5f,0.5f));
		honorShopUI->setPosition(ccp(winSize.width/2,winSize.height/2));
		honorShopUI->setTag(KTagHonorShopUI);
		GameView::getInstance()->getMainUIScene()->addChild(honorShopUI);
	}
}

void OffLineArenaUI::FightNotesEvent( CCObject *pSender )
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(KTagFightNotesUI) == NULL)
	{
		CCSize winSize=CCDirector::sharedDirector()->getVisibleSize();
		FightNotesUI * fightNotesUI = FightNotesUI::create();
		fightNotesUI->ignoreAnchorPointForPosition(false);
		fightNotesUI->setAnchorPoint(ccp(0.5f,0.5f));
		fightNotesUI->setPosition(ccp(winSize.width/2,winSize.height/2));
		fightNotesUI->setTag(KTagFightNotesUI);
		GameView::getInstance()->getMainUIScene()->addChild(fightNotesUI);

		GameMessageProcessor::sharedMsgProcessor()->sendReq(3004);
	}
}

void OffLineArenaUI::ChallengeEvent( CCObject *pSender )
{
	if (m_nCurSelectIndex == -1)
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("OffLineArenaUI_PleaseSelectOne"));
	}
	else
	{
		CPartner * curPartner = curArenaList.at(m_nCurSelectIndex);
		if (!curPartner)
			return;

		ReqForChallenge * temp = new ReqForChallenge();
		temp->myRanking = m_nRoleRanking;
		temp->targetId = curPartner->role().rolebase().roleid();
		temp->targetRanking = curPartner->ranking();
		GameMessageProcessor::sharedMsgProcessor()->sendReq(3003,temp);

		Script* sc = ScriptManager::getInstance()->getScriptById(this->mTutorialScriptInstanceId);
		if(sc != NULL)
			sc->endCommand(pSender);

		this->closeAnim();
	}
}

void OffLineArenaUI::BuyNumEvent( CCObject *pSender )
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(KTagBuyFightTimesUI) == NULL)
	{
		CCSize winSize=CCDirector::sharedDirector()->getVisibleSize();
		BuyFightTimesUI * buyFightTimesUI = BuyFightTimesUI::create();
		buyFightTimesUI->ignoreAnchorPointForPosition(false);
		buyFightTimesUI->setAnchorPoint(ccp(0.5f,0.5f));
		buyFightTimesUI->setPosition(ccp(winSize.width/2,winSize.height/2));
		buyFightTimesUI->setTag(KTagBuyFightTimesUI);
		GameView::getInstance()->getMainUIScene()->addChild(buyFightTimesUI);
	}
}

void OffLineArenaUI::GetRewardsEvent( CCObject *pSender )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(3001);
}

void OffLineArenaUI::setRemaintimes( long long _value )
{
	m_nRemaintimes = _value;
}

int OffLineArenaUI::getAllFreeFightTimes()
{
	return m_nAllFreeFighttimes;
}

int OffLineArenaUI::getRemainBuyTimes()
{
	return m_nRemainBuytimes;
}

void OffLineArenaUI::setCurSelectIndex(int _value)
{
	m_nCurSelectIndex = _value;
}

void OffLineArenaUI::RefreshMyRankingConfig(int ranking)
{
	m_nRoleRanking = ranking;
	char s_roleRanking [20];
	sprintf(s_roleRanking,"%d",m_nRoleRanking);
	l_roleRankingValue->setText(s_roleRanking);

	l_roleRankingValue->setPosition(ccp(imageView_di->getPosition().x+imageView_di->getContentSize().width/2*imageView_di->getScale()+l_roleRankingValue->getContentSize().width/2+2,imageView_di->getPosition().y));
	imageView_ming->setPosition(ccp(l_roleRankingValue->getPosition().x+l_roleRankingValue->getContentSize().width/2+imageView_ming->getContentSize().width/2*imageView_ming->getScale()-1,l_roleRankingValue->getPosition().y));
}

int OffLineArenaUI::getMyRoleRankValue()
{
	return m_nRoleRanking;
}

void OffLineArenaUI::RefreshArenaDisplay( std::vector<CPartner *> arenaList )
{
	//refresh data
	//delete old data
	std::vector<CPartner*>::iterator iter_parther;
	for (iter_parther = curArenaList.begin(); iter_parther != curArenaList.end(); ++iter_parther)
	{
		delete *iter_parther;
	}
	curArenaList.clear();
	//add new data
	for(int i =0;i<arenaList.size();++i)
	{
		CPartner * partner = new CPartner();
		partner->CopyFrom(*arenaList.at(i));
		curArenaList.push_back(partner);
	}

	int max = 0;
	if(arenaList.size() <= 5)
	{
		max = arenaList.size();
	}
	else
	{
		max = 5;
	}
	for (int i = 0;i<max;++i)
	{
		if (u_layer->getChildByTag(ArenaRoleInfo_BaseTag+i))
		{
			(u_layer->getChildByTag(ArenaRoleInfo_BaseTag+i))->removeFromParent();
		}

		ArenaRoleInfo * arenaRoleInfo = ArenaRoleInfo::create(arenaList.at(i),i);
		arenaRoleInfo->setAnchorPoint(ccp(0,0));
		arenaRoleInfo->setPosition(ccp(617-139*i,96));//(61+139*i,96)
		arenaRoleInfo->setTag(ArenaRoleInfo_BaseTag+i);
		u_layer->addChild(arenaRoleInfo);

		if (GameView::getInstance()->isOwn(arenaList.at(i)->role().rolebase().roleid()))
		{
			RefreshMyRankingConfig(arenaList.at(i)->ranking());
		}
	}
}

void OffLineArenaUI::RefreshHonorConfig( int honorValue,long long remainTime )
{
	m_nHonor = honorValue;
	m_nRemaintimes = remainTime;
	//每一段时间多少荣誉值
	char s_honorValue[20];
	sprintf(s_honorValue,"%d",m_nHonor);
	l_honorValue->setText(s_honorValue);
	//剩余领取荣誉值时间
	l_remainTime->setText(timeFormatToString(m_nRemaintimes/1000).c_str());

}

void OffLineArenaUI::RefreshFightConfig( int fightTimes,int allFreeTimes,int remainBuyTimes )
{
	m_nAllFreeFighttimes = allFreeTimes;
	m_nRemainBuytimes = remainBuyTimes;
	m_nFighttimes = allFreeTimes-fightTimes;
	//剩余挑战次数
	std::string str_fightTimes;
	char s_remainFightTimesValue[5];
	sprintf(s_remainFightTimesValue,"%d",m_nFighttimes);
	str_fightTimes.append(s_remainFightTimesValue);
	str_fightTimes.append("/");
	char s_allFightTimesValue[5];
	sprintf(s_allFightTimesValue,"%d",m_nAllFreeFighttimes);
	str_fightTimes.append(s_allFightTimesValue);
	l_fightTime->setText(str_fightTimes.c_str());

	float scale_remain = m_nFighttimes*1.0f/allFreeTimes;
	if (scale_remain > 1.0f)
		scale_remain = 1.0f;

	imageView_remainTime->setTextureRect(CCRectMake(0,0,imageView_remainTime->getContentSize().width*scale_remain,imageView_remainTime->getContentSize().height));


	if (m_nAllFreeFighttimes - m_nFighttimes <= 0)
	{

	}
}

void OffLineArenaUI::RefreshSelectState()
{
	for (int i = 0;i<5;++i)
	{
		if (u_layer->getChildByTag(ArenaRoleInfo_BaseTag+i))
		{
			ArenaRoleInfo * arenaRoleInfo = (ArenaRoleInfo*)u_layer->getChildByTag(ArenaRoleInfo_BaseTag+i);
			if (arenaRoleInfo->getCurIndex() != m_nCurSelectIndex)
			{
				arenaRoleInfo->unSelected();
			}
		}
	}

	for (int i = 0;i<5;++i)
	{
		if (u_layer->getChildByTag(ArenaRoleInfo_BaseTag+i))
		{
			ArenaRoleInfo * arenaRoleInfo = (ArenaRoleInfo*)u_layer->getChildByTag(ArenaRoleInfo_BaseTag+i);
			if (arenaRoleInfo->getCurIndex() == m_nCurSelectIndex)
			{
				arenaRoleInfo->selected();
			}
		}
	}
}

std::string OffLineArenaUI::timeFormatToString( long long t )
{
	int int_h = t/60/60;
	int int_m = (t%3600)/60;
	int int_s = (t%3600)%60;

	std::string timeString = "";
	char str_h[10];
	sprintf(str_h,"%d",int_h);
	//timeString = str_h;
	//timeString.append(":");
	if (int_h <= 0)
	{

	}
	else if (int_h > 0 && int_h < 10)
	{
		timeString.append("0");
		timeString.append(str_h);
		timeString.append(":");
	}
	else
	{
		timeString.append(str_h);
		timeString.append(":");
	}

	char str_m[10];
	sprintf(str_m,"%d",int_m);
	//timeString.append(str_m);
	//timeString.append(":");
	if (int_m >= 0 && int_m < 10)
	{
		timeString.append("0");
		timeString.append(str_m);
	}
	else
	{
		timeString.append(str_m);
	}
	timeString.append(":");

	char str_s[10];
	sprintf(str_s,"%d",int_s);
	//timeString.append(str_s);
	if (int_s >= 0 && int_s < 10)
	{
		timeString.append("0");
		timeString.append(str_s);
	}
	else
	{
		timeString.append(str_s);
	}

	return timeString;
}

void OffLineArenaUI::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void OffLineArenaUI::addCCTutorialIndicator1( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height-480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,80,60);
// 	if (direction == CCTutorialIndicator::Direction_LD)
// 	{
// 		tutorialIndicator->setPosition(ccp(pos.x+130+_w,pos.y+200+_h));
// 	}
// 	else if (direction == CCTutorialIndicator::Direction_RD)
// 	{
// 		tutorialIndicator->setPosition(ccp(pos.x+30+_w,pos.y+200+_h));
// 	}
// 	
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);

	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,136,243,true);
	tutorialIndicator->setDrawNodePos(ccp(pos.x+136/2+_w,pos.y+243/2+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void OffLineArenaUI::addCCTutorialIndicator2( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,70,50);
// 	tutorialIndicator->setPosition(ccp(pos.x+60+_w,pos.y+80+_h));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);

	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,135,51,true);
	tutorialIndicator->setDrawNodePos(ccp(pos.x+_w,pos.y+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void OffLineArenaUI::removeCCTutorialIndicator()
{
	CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		CCTeachingGuide* teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}

ArenaRoleInfo * OffLineArenaUI::getTutorialRoleItem()
{
	//从右向左（0-4）
	if (m_nRoleRanking == 1)
		return NULL;

	int willSelectIdx = -1;
	for(int i =0;i<curArenaList.size();++i)
	{
		CPartner * partner = curArenaList.at(i);
		if (GameView::getInstance()->isOwn(partner->role().rolebase().roleid()))
		{
			willSelectIdx = i-1;
			break;
		}
	}

	if (willSelectIdx < 0)
		return NULL;

	ArenaRoleInfo * arenaRoleInfo = (ArenaRoleInfo*)(u_layer->getChildByTag(ArenaRoleInfo_BaseTag+willSelectIdx));
	if (arenaRoleInfo)
	{
		return arenaRoleInfo;		
	}
	else
	{
		return NULL;
	}
}

int OffLineArenaUI::getTutorialRoleItemIdx()
{
	//从右向左（0-4）
	if (m_nRoleRanking == 1)
		return -1;

	int willSelectIdx = -1;
	for(int i =0;i<curArenaList.size();++i)
	{
		CPartner * partner = curArenaList.at(i);
		if (GameView::getInstance()->isOwn(partner->role().rolebase().roleid()))
		{
			willSelectIdx = i-1;
			break;
		}
	}

	if (willSelectIdx < 0)
		return -1;

	return willSelectIdx;
}




//////////////////////////////////////////////////////////////////

#define  kTagRotateAnm 777

ArenaRoleInfo::ArenaRoleInfo():
m_nIndex(-1)
{

}

ArenaRoleInfo::~ArenaRoleInfo()
{
	delete curPartner;
}

ArenaRoleInfo * ArenaRoleInfo::create(CPartner * partner,int index)
{
	ArenaRoleInfo * arenaRoleInfo = new ArenaRoleInfo();
	if (arenaRoleInfo && arenaRoleInfo->init(partner,index))
	{
		arenaRoleInfo->autorelease();
		return arenaRoleInfo;
	}
	CC_SAFE_DELETE(arenaRoleInfo);
	return NULL;
}

bool ArenaRoleInfo::init(CPartner * partner,int index)
{
	if (UIScene::init())
	{
		m_nIndex = index;
		curPartner = new CPartner();
		curPartner->CopyFrom(*partner);

		//边框
		UIButton * btn_bigFrame = UIButton::create();
		btn_bigFrame->setTextures("res_ui/no3_di.png","res_ui/no3_di.png","");
		btn_bigFrame->setScale9Enable(true);
		btn_bigFrame->setScale9Size(CCSizeMake(136,243));
		btn_bigFrame->setCapInsets(CCRectMake(13,13,1,1));
		btn_bigFrame->setAnchorPoint(ccp(0.5f,0.5f));
		btn_bigFrame->setPosition(ccp(136/2,245/2));
		btn_bigFrame->addReleaseEvent(this,coco_releaseselector(ArenaRoleInfo::BigFrameEvent));
		btn_bigFrame->setTouchEnable(true);
		m_pUiLayer->addWidget(btn_bigFrame);

		//底
		imageView_di = UIImageView::create();
		imageView_di->setTexture("res_ui/arena/light_long.png");
		imageView_di->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_di->setPosition(ccp(69,155));
		m_pUiLayer->addWidget(imageView_di);
		//名字
		UILabel * l_name = UILabel::create();
		l_name->setText(partner->role().rolebase().name().c_str());
		l_name->setFontName(APP_FONT_NAME);
		l_name->setFontSize(18);
		l_name->setAnchorPoint(ccp(0.5f,0.5f));
		l_name->setPosition(ccp(69,225));
		m_pUiLayer->addWidget(l_name);

		//角色动画
		if (partner->has_role())
		{
			CActiveRole * activeRole = new CActiveRole();
			activeRole->CopyFrom(partner->role());

			//vipInfo
			// vip开关控制
			bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
			if (vipEnabled)
			{
				if (activeRole->playerbaseinfo().viplevel() > 0)
				{
					l_name->setPosition(ccp(57,225));

					UIWidget *widget = MainScene::addVipInfoByLevelForWidget(activeRole->playerbaseinfo().viplevel());
					m_pUiLayer->addWidget(widget);
					widget->setPosition(ccp(l_name->getPosition().x+l_name->getContentSize().width/2+10,l_name->getPosition().y));
				}
			}

			std::string roleFigureName = ActorUtils::getActorFigureName(*activeRole);
			std::string roleWeaponEffectForRefine = "";
			std::string roleWeaponEffectForStar = "";
			for (int i = 0;i<activeRole->equipmentshowinfo().size();++i)
			{
				if (activeRole->equipmentshowinfo(i).clazz() == 1)
				{
					int profession_ = BasePlayer::getProfessionIdxByName(activeRole->profession());
					int equipRefineLevel_ = activeRole->equipmentshowinfo(i).gradelevel();
					int equipStarLevel_ = activeRole->equipmentshowinfo(i).starlevel();

					roleWeaponEffectForRefine.append(ActorUtils::getWeapEffectByRefineLevel(profession_,equipRefineLevel_));
					roleWeaponEffectForStar.append(ActorUtils::getWeapEffectByStarLevel(profession_,equipStarLevel_));
				}
			}

			GameActorAnimation* pAnim = ActorUtils::createActorAnimation(roleFigureName.c_str(), activeRole->hand().c_str(),
				roleWeaponEffectForRefine.c_str(), roleWeaponEffectForStar.c_str());
			pAnim->setPosition(ccp(69,105));
			addChild(pAnim);
			delete activeRole;
		}
		
		UILayer * u_layer = UILayer::create();
		addChild(u_layer);
		//排名
		std::string str_ranking = StringDataManager::getString("Arena_ranking");
		char s_ranking[20];
		sprintf(s_ranking,"%d",partner->ranking());
		str_ranking.append(s_ranking);
		UILabel * l_ranking = UILabel::create();
		l_ranking->setText(str_ranking.c_str());
		l_ranking->setFontName(APP_FONT_NAME);
		l_ranking->setFontSize(16);
		l_ranking->setAnchorPoint(ccp(0.5f,0.5f));
		l_ranking->setPosition(ccp(69,90));
		u_layer->addWidget(l_ranking);
		//战斗力
		std::string str_fightPoint = StringDataManager::getString("Arena_fightPoint");
		char s_fightPointValue[20];
		sprintf(s_fightPointValue,"%d",partner->role().fightpoint());
		str_fightPoint.append(s_fightPointValue);
		UILabel * l_fightPoint = UILabel::create();
		l_fightPoint->setText(str_fightPoint.c_str());
		l_fightPoint->setFontName(APP_FONT_NAME);
		l_fightPoint->setFontSize(16);
		l_fightPoint->setAnchorPoint(ccp(0.5f,0.5f));
		l_fightPoint->setPosition(ccp(69,72));
		u_layer->addWidget(l_fightPoint);
		
		//荣誉值
		std::string str_honorValue_everyTenMinutes;
		char s_honorValue[20];
		sprintf(s_honorValue,"%d",partner->honorconfig());
		str_honorValue_everyTenMinutes.append(s_honorValue);
		str_honorValue_everyTenMinutes.append(StringDataManager::getString("Arena_honorValue_everyTenMinutes"));
		UILabel * l_honorValue = UILabel::create();
		l_honorValue->setText(str_honorValue_everyTenMinutes.c_str());
		l_honorValue->setFontName(APP_FONT_NAME);
		l_honorValue->setFontSize(16);
		l_honorValue->setAnchorPoint(ccp(0.5f,0.5f));
		l_honorValue->setPosition(ccp(69,54));
		u_layer->addWidget(l_honorValue);

		//查看按钮
		btn_lookOver = UIButton::create();
		btn_lookOver->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		btn_lookOver->setScale9Enable(true);
		btn_lookOver->setScale9Size(CCSizeMake(90,37));
		btn_lookOver->setCapInsets(CCRectMake(18,9,2,23));
		btn_lookOver->setAnchorPoint(ccp(0.5f,0.5f));
		btn_lookOver->setPosition(ccp(69,26));
		btn_lookOver->addReleaseEvent(this,coco_releaseselector(ArenaRoleInfo::LookOverEvent));
		btn_lookOver->setTouchEnable(true);
		btn_lookOver->setPressedActionEnabled(true);
		u_layer->addWidget(btn_lookOver);
		btn_lookOver->setVisible(false);

		UILabel * lookOver = UILabel::create();    
		lookOver->setText(StringDataManager::getString("family_apply_btncheck"));
		lookOver->setAnchorPoint(ccp(0.5f,0.5f));
		lookOver->setFontName(APP_FONT_NAME);
		lookOver->setFontSize(18);
		lookOver->setPosition(ccp(0,0));
		btn_lookOver->addChild(lookOver);

		imageView_select = UIImageView::create();
		imageView_select->setTexture("res_ui/highlight.png");
		imageView_select->setScale9Enable(true);
		imageView_select->setScale9Size(CCSizeMake(136,243));
		imageView_select->setCapInsets(CCRectMake(25,25,1,1));
		imageView_select->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_select->setPosition(ccp(69,245/2));
		m_pUiLayer->addWidget(imageView_select);
		imageView_select->setVisible(false);

		if (GameView::getInstance()->isOwn(partner->role().rolebase().roleid()))
		{
			imageView_di->setTexture("res_ui/arena/light_long_blue.png");
			l_name->setColor(ccc3(0,255,255));
			l_ranking->setColor(ccc3(0,255,255));
			l_fightPoint->setColor(ccc3(0,255,255));
			l_honorValue->setColor(ccc3(0,255,255));
		}
		else
		{
			imageView_di->setTexture("res_ui/arena/light_long.png");
			l_name->setColor(ccc3(0,255,0));
			l_ranking->setColor(ccc3(0,255,0));
			l_fightPoint->setColor(ccc3(0,255,0));
			l_honorValue->setColor(ccc3(0,255,0));
		}

		this->setContentSize(CCSizeMake(138,245));
		return true;
	}
	return false;
}

void ArenaRoleInfo::LookOverEvent( CCObject *pSender )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1142,(void*)curPartner->role().rolebase().roleid());
}

void ArenaRoleInfo::BigFrameEvent( CCObject *pSender )
{
	OffLineArenaUI * offLineAreaUI = (OffLineArenaUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOffLineArenaUI);
	if (!offLineAreaUI)
		return;

	//设置当前选中的位置
	offLineAreaUI->setCurSelectIndex(m_nIndex);

	offLineAreaUI->RefreshSelectState();
}

void ArenaRoleInfo::selected()
{
	if (GameView::getInstance()->isOwn(curPartner->role().rolebase().roleid()))
	{
		btn_lookOver->setVisible(false);
	}
	else
	{
		btn_lookOver->setVisible(true);
	}
	
	imageView_select->setVisible(true);

	//test by yangjun 2014.3.29
	CCRotateBy * rotateAnm = CCRotateBy::create(6.0f,360);
	CCRepeatForever * repeatAnm = CCRepeatForever::create(CCSequence::create(rotateAnm,NULL));
	repeatAnm->setTag(kTagRotateAnm);
	imageView_di->runAction(repeatAnm);

	OffLineArenaUI * offLineArenaUI = (OffLineArenaUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOffLineArenaUI);
	Script* sc = ScriptManager::getInstance()->getScriptById(offLineArenaUI->mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(this);
}

void ArenaRoleInfo::unSelected()
{
	btn_lookOver->setVisible(false);
	imageView_select->setVisible(false);

	//test by yangjun 2014.3.29
	if (imageView_di->getActionByTag(kTagRotateAnm) != NULL)
	{
		imageView_di->stopAllActions();
		//imageView_di->getActionManager()->removeAllActions();
		imageView_di->setRotation(0);
	}
}

int ArenaRoleInfo::getCurIndex()
{
	return m_nIndex;
}
