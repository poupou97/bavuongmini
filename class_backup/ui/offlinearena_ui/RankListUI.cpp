#include "RankListUI.h"
#include "../extensions/UIScene.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "OffLineArenaUI.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/element/CPartner.h"
#include "../../AppMacros.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../utils/StaticDataManager.h"
#include "../../GameView.h"
#include "../../gamescene_state/sceneelement/TargetInfo.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../utils/GameConfig.h"
#include "../../messageclient/element/CSimplePartner.h"

#define  SelectImageTag 458

RankListUI::RankListUI():
lastSelectCellId(0),
selectCellId(0)
{
}


RankListUI::~RankListUI()
{
	std::vector<CSimplePartner*>::iterator iter_ranking;
	for (iter_ranking = curRankingList.begin(); iter_ranking != curRankingList.end(); ++iter_ranking)
	{
		delete *iter_ranking;
	}
	curRankingList.clear();
}

RankListUI * RankListUI::create(int myRanking)
{
	RankListUI * rankListUI = new RankListUI();
	if (rankListUI && rankListUI->init(myRanking))
	{
		rankListUI->autorelease();
		return rankListUI;
	}
	CC_SAFE_DELETE(rankListUI);
	return NULL;
}

bool RankListUI::init(int myRanking)
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		this->scheduleUpdate(); 

		//加载UI
		if(LoadSceneLayer::RankListLayer->getWidgetParent() != NULL)
		{
			LoadSceneLayer::RankListLayer->removeFromParentAndCleanup(false);
		}
		UIPanel *ppanel = LoadSceneLayer::RankListLayer;
		ppanel->setAnchorPoint(ccp(0.0f,0.0f));
		ppanel->getValidNode()->setContentSize(CCSizeMake(608, 417));
		ppanel->setPosition(CCPointZero);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnable(true);
		m_pUiLayer->addWidget(ppanel);
		
		UIPanel * panel_fightNotes = (UIPanel *)UIHelper::seekWidgetByName(ppanel,"Panel_conbatRecord");
		panel_fightNotes->setVisible(false);
		UIPanel * panel_rankList = (UIPanel *)UIHelper::seekWidgetByName(ppanel,"Panel_ranking");
		panel_rankList->setVisible(true);
		UIPanel * panel_honorShop = (UIPanel *)UIHelper::seekWidgetByName(ppanel,"Panel_honorShop");
		panel_honorShop->setVisible(false);

		UIButton *Button_close = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnable(true);
		Button_close->addReleaseEvent(this, coco_releaseselector(RankListUI::CloseEvent));
		Button_close->setPressedActionEnabled(true);

		UIButton * btn_makeFriend = (UIButton*)UIHelper::seekWidgetByName(panel_rankList,"Button_makeFriend");
		btn_makeFriend->setTouchEnable(true);
		btn_makeFriend->addReleaseEvent(this, coco_releaseselector(RankListUI::MakeFriendEvent));
		btn_makeFriend->setPressedActionEnabled(true);
		btn_makeFriend->setVisible(false);

		UIButton *btn_lookOver = (UIButton*)UIHelper::seekWidgetByName(panel_rankList,"Button_view");
		btn_lookOver->setTouchEnable(true);
		btn_lookOver->addReleaseEvent(this, coco_releaseselector(RankListUI::LookOverEvent));
		btn_lookOver->setPressedActionEnabled(true);
		btn_lookOver->setVisible(false);

		l_selfRanking = (UILabel *)UIHelper::seekWidgetByName(panel_rankList,"Label_roleRank");
		//我的排名
		char s_ranking [20];
		sprintf(s_ranking,"%d",myRanking);
		l_selfRanking->setText(s_ranking);
// 		for(int i = 0;i<curRankingList.size();++i)
// 		{
// 			if (GameView::getInstance()->myplayer->getRoleId() == curRankingList.at(i)->role().rolebase().roleid())
// 			{
// 				char s_ranking [20];
// 				sprintf(s_ranking,"%d",curRankingList.at(i)->ranking());
// 				l_selfRanking->setText(s_ranking);
// 				break;
// 			}
// 		}

		m_tableView = CCTableView::create(this,CCSizeMake(517,227));
		m_tableView->setDirection(kCCScrollViewDirectionVertical);
		m_tableView->setAnchorPoint(ccp(0,0));
		m_tableView->setPosition(ccp(45,77));
		m_tableView->setDelegate(this);
		m_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		addChild(m_tableView);

		this->setContentSize(CCSizeMake(608,417));
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);

		return true;
	}
	return false;
}

void RankListUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void RankListUI::onExit()
{
	UIScene::onExit();
}

bool RankListUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return true;
}

void RankListUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void RankListUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void RankListUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void RankListUI::CloseEvent( CCObject *pSender )
{
	this->closeAnim();
}

void RankListUI::scrollViewDidScroll( CCScrollView* view )
{

}

void RankListUI::scrollViewDidZoom( CCScrollView* view )
{

}

void RankListUI::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{
	selectCellId = cell->getIdx();
	//取出上次选中状态，更新本次选中状态
	if(table->cellAtIndex(lastSelectCellId))
	{
		if (table->cellAtIndex(lastSelectCellId)->getChildByTag(SelectImageTag))
		{
			table->cellAtIndex(lastSelectCellId)->getChildByTag(SelectImageTag)->setVisible(false);
		}
	}

	if (cell->getChildByTag(SelectImageTag))
	{
		cell->getChildByTag(SelectImageTag)->setVisible(true);
	}

	lastSelectCellId = cell->getIdx();

	//
	CCSize winSize_ = CCDirector::sharedDirector()->getVisibleSize();
	long long roleId = curRankingList.at(selectCellId)->role().roleid();
	std::string name_ = curRankingList.at(selectCellId)->role().name();
	int countryId_ = curRankingList.at(selectCellId)->role().countryid();
	int vipLv_ = curRankingList.at(selectCellId)->role().viplevel();
	int level_ = curRankingList.at(selectCellId)->role().level();
	int pressionId_ = curRankingList.at(selectCellId)->role().profession();

	TargetInfoList * tarlist = TargetInfoList::create(roleId, name_,countryId_,vipLv_,level_,pressionId_);
	tarlist->ignoreAnchorPointForPosition(false);
	tarlist->setAnchorPoint(ccp(0, 0));
	tarlist->setTag(ktagMainSceneTargetInfo);
	tarlist->setPosition(ccp(winSize_.width/2+ this->getContentSize().width/2 - tarlist->getContentSize().width - 50,
								winSize_.height/2+tarlist->getContentSize().height/2 - 50));
	GameView::getInstance()->getMainUIScene()->addChild(tarlist);
}

cocos2d::CCSize RankListUI::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeMake(510,34);
}

cocos2d::extension::CCTableViewCell* RankListUI::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell = new CCTableViewCell();
	cell->autorelease();

	CSimplePartner * curPartner = curRankingList.at(idx);

	CCScale9Sprite* pHighlightSpr = CCScale9Sprite::create(CCRectMake(5, 5, 1, 1) , "res_ui/mengban_green.png");
	pHighlightSpr->setPreferredSize(CCSize(515,32));
	pHighlightSpr->setAnchorPoint(CCPointZero);
	pHighlightSpr->setPosition(ccp(0,0));
	pHighlightSpr->setTag(SelectImageTag);
	cell->addChild(pHighlightSpr);
	pHighlightSpr->setVisible(false);

	ccColor3B colorFont = getFontColor(curPartner->ranking());

	//大边框
	CCScale9Sprite * sprite_bigFrame = CCScale9Sprite::create("res_ui/LV3_dikuang.png");
	sprite_bigFrame->setAnchorPoint(ccp(0, 0));
	sprite_bigFrame->setPosition(ccp(0, 0));
	sprite_bigFrame->setCapInsets(CCRectMake(9,14,1,1));
	sprite_bigFrame->setPreferredSize(CCSizeMake(515,32));
	cell->addChild(sprite_bigFrame);
	//排名
	char s_ranking[20];
	sprintf(s_ranking,"%d",curPartner->ranking());
	if (curPartner->ranking()>0 && curPartner->ranking() <= 3)
	{
		std::string icon_path = "res_ui/rank";
		icon_path.append(s_ranking);
		icon_path.append(".png");
		CCSprite * sp_ranking = CCSprite::create(icon_path.c_str());
		sp_ranking->setAnchorPoint(ccp(0.5f,0.5f));
		sp_ranking->setPosition(ccp(31,15));
		sp_ranking->setScale(0.95f);
		cell->addChild(sp_ranking);
	}
	else if(curPartner->ranking() <= 10)
	{
		CCLabelBMFont * l_ranking = CCLabelBMFont::create(s_ranking,"res_ui/font/ziti_3.fnt");
		l_ranking->setAnchorPoint(ccp(0.5f,0.5f));
		l_ranking->setPosition(ccp(31,15));
		cell->addChild(l_ranking);
	}
	else
	{
		CCLabelTTF * l_ranking = CCLabelTTF::create(s_ranking,APP_FONT_NAME,18);
		l_ranking->setAnchorPoint(ccp(0.5f,0.5f));
		l_ranking->setPosition(ccp(31,15));
		l_ranking->setColor(colorFont);
		cell->addChild(l_ranking);
	}

	//名字
	CCLabelTTF * l_name = CCLabelTTF::create(curPartner->role().name().c_str(),APP_FONT_NAME,18);
	l_name->setAnchorPoint(ccp(0.0f,0.5f));
	l_name->setPosition(ccp(90,15));
	l_name->setColor(colorFont);
	cell->addChild(l_name);

	int countryId = curPartner->role().countryid();
	if (countryId > 0 && countryId <6)
	{
		std::string countryIdName = "country_";
		char id[2];
		sprintf(id, "%d", countryId);
		countryIdName.append(id);
		std::string iconPathName = "res_ui/country_icon/";
		iconPathName.append(countryIdName);
		iconPathName.append(".png");
		CCSprite * countrySp_ = CCSprite::create(iconPathName.c_str());
		countrySp_->setAnchorPoint(ccp(1.0,0.5f));
		//countrySp_->setPosition(ccp(l_name->getPositionX() - l_name->getContentSize().width/2-1,l_name->getPositionY()));
		countrySp_->setPosition(ccp(l_name->getPositionX()-1,l_name->getPositionY()));
		countrySp_->setScale(0.7f);
		cell->addChild(countrySp_);
	}

	//vipInfo
	// vip开关控制
	bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
	if (vipEnabled)
	{
		if (curPartner->role().viplevel() > 0)
		{
			CCNode *pNode = MainScene::addVipInfoByLevelForNode(curPartner->role().viplevel());
			if (pNode)
			{
				cell->addChild(pNode);
				//pNode->setPosition(ccp(l_name->getPosition().x+l_name->getContentSize().width/2+13,l_name->getPosition().y));
				pNode->setPosition(ccp(l_name->getPosition().x+l_name->getContentSize().width+13,l_name->getPosition().y));
			}
		}
	}

	//职业
	CCLabelTTF * l_profession = CCLabelTTF::create(BasePlayer::getProfessionNameIdxByIndex(curPartner->role().profession()).c_str(),APP_FONT_NAME,18);
	l_profession->setAnchorPoint(ccp(0.5f,0.5f));
	l_profession->setPosition(ccp(225,15));
	l_profession->setColor(colorFont);
	cell->addChild(l_profession);
	//等级
	char s_lv[20];
	sprintf(s_lv,"%d",curPartner->role().level());
	CCLabelTTF * l_lv = CCLabelTTF::create(s_lv,APP_FONT_NAME,18);
	l_lv->setAnchorPoint(ccp(0.5f,0.5f));
	l_lv->setPosition(ccp(291,15));
	l_lv->setColor(colorFont);
	cell->addChild(l_lv);
	//战斗力
	char s_fightPoint[20];
	sprintf(s_fightPoint,"%d",curPartner->role().fightpoint());
	CCLabelTTF * l_fightPoint = CCLabelTTF::create(s_fightPoint,APP_FONT_NAME,18);
	l_fightPoint->setAnchorPoint(ccp(0.5f,0.5f));
	l_fightPoint->setPosition(ccp(375,15));
	l_fightPoint->setColor(colorFont);
	cell->addChild(l_fightPoint);
	//状态
	std::string str_state;
	if (curPartner->online())
	{
		str_state.append(StringDataManager::getString("Arena_roleStype_online"));
	}
	else
	{
		str_state.append(StringDataManager::getString("Arena_roleStype_notOnline"));
	}
	CCLabelTTF * l_state = CCLabelTTF::create(str_state.c_str(),APP_FONT_NAME,18);
	l_state->setAnchorPoint(ccp(0.5f,0.5f));
	l_state->setPosition(ccp(470,15));
	l_state->setColor(colorFont);
	cell->addChild(l_state);


	if (this->selectCellId == idx)
	{
		pHighlightSpr->setVisible(true);
	}

	if (getIsCanReq())
	{
		if(idx == curRankingList.size()-4)
		{
			if (generalsListStatus == HaveNext || generalsListStatus == HaveBoth)
			{
				//req for next
				this->isReqNewly = false;
				GameMessageProcessor::sharedMsgProcessor()->sendReq(3002,(void*)(this->s_mCurPage+1),(void*)20);
				setIsCanReq(false);
			}
		}
	}


	return cell;
}

unsigned int RankListUI::numberOfCellsInTableView( CCTableView *table )
{
	return curRankingList.size();
}

void RankListUI::RefreshData()
{
	m_tableView->reloadData();
}

void RankListUI::MakeFriendEvent( CCObject *pSender )
{
	if (curRankingList.size()>0)
	{
		long long roleId = curRankingList.at(selectCellId)->role().roleid();
		if (GameView::getInstance()->isOwn(roleId))
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("OffLineArenaUI_RankUI_canNotMakeFriendSelf"));
		}
		else
		{
			std::string roleName = curRankingList.at(selectCellId)->role().name();
			TargetInfo::FriendStruct friend1={0,0,roleId,roleName};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend1);
		}
		
	}
	else
	{

	}
}

void RankListUI::LookOverEvent( CCObject *pSender )
{
	if (curRankingList.size()>0)
	{
		long long roleId = curRankingList.at(selectCellId)->role().roleid();
		if (GameView::getInstance()->isOwn(roleId))
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("OffLineArenaUI_RankUI_canNotLookOverSelf"));
		}
		else
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1142,(void *)roleId);
		}
	
	}
	else
	{

	}
}

void RankListUI::RefreshDataWithOutChangeOffSet()
{
	CCPoint _s = m_tableView->getContentOffset();
	int _h = m_tableView->getContentSize().height + _s.y;
	m_tableView->reloadData();
	CCPoint temp = ccp(_s.x,_h - m_tableView->getContentSize().height);
	m_tableView->setContentOffset(temp); 
}

cocos2d::ccColor3B RankListUI::getFontColor( int nRank )
{
	// 默认
	ccColor3B colorFont = ccc3(47, 93, 13);
	if (1 == nRank)
	{
		colorFont = ccc3(155, 84, 0);
	}
	else if (2 == nRank)
	{
		colorFont = ccc3(169, 2, 155);
	}
	else if(3 == nRank)
	{
		colorFont = ccc3(0, 64, 227);
	}

	return colorFont;
}
