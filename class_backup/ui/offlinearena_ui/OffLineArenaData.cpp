#include "OffLineArenaData.h"

OffLineArenaData * OffLineArenaData::s_arenaData = NULL;

OffLineArenaData::OffLineArenaData(void):
m_nMyRoleRanking(-1),
m_nTargetRanking(-1),
m_nCurStatus(s_none)
{
}


OffLineArenaData::~OffLineArenaData(void)
{
}

OffLineArenaData * OffLineArenaData::getInstance()
{
	if (s_arenaData == NULL)
	{
		s_arenaData = new OffLineArenaData();
	}

	return s_arenaData;
}

int OffLineArenaData::getCurStatus()
{
	return m_nCurStatus;
}

void OffLineArenaData::setCurStatus( int _value )
{
	m_nCurStatus = _value;
}
