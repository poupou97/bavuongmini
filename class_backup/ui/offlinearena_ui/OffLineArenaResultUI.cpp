#include "OffLineArenaResultUI.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "OffLineArenaState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/GameSceneState.h"
#include "GameView.h"
#include "../../utils/GameUtils.h"
#include "../../ui/FivePersonInstance/ResultHeadPartItem.h"
#include "../../ui/GameUIConstant.h"
#include "OffLineArenaData.h"
#include "../../gamescene_state/MainAnimationScene.h"
#include "../../legend_engine/CCLegendAnimation.h"


OffLineArenaResultUI::OffLineArenaResultUI():
m_nRemainTime(30000),
m_bIsWin(true),
m_nExp(0),
countDown_startTime(0)
{
}


OffLineArenaResultUI::~OffLineArenaResultUI()
{
}

OffLineArenaResultUI * OffLineArenaResultUI::create( bool isWin,int exp )
{
	OffLineArenaResultUI * offLineArenaResultUI = new OffLineArenaResultUI();
	if (offLineArenaResultUI && offLineArenaResultUI->init(isWin,exp))
	{
		offLineArenaResultUI->autorelease();
		return offLineArenaResultUI;
	}
	CC_SAFE_DELETE(offLineArenaResultUI);
	return NULL;
}

bool OffLineArenaResultUI::init(bool isWin, int exp )
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		m_bIsWin = isWin;
		m_nExp = exp;
		countDown_startTime = GameUtils::millisecondNow();

		MainScene * mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
		if (!mainscene)
			return false;

		CCActionInterval * action =(CCActionInterval *)CCSequence::create(
			CCDelayTime::create(END_BATTLE_BEFORE_SLOWMOTION_DURATION),
			CCCallFunc::create(this, callfunc_selector(OffLineArenaResultUI::startSlowMotion)),
			CCCallFunc::create(this, callfunc_selector(OffLineArenaResultUI::addLightAnimation)),
			CCDelayTime::create(END_BATTLE_SLOWMOTION_DURATION),
			CCCallFunc::create(this, callfunc_selector(OffLineArenaResultUI::endSlowMotion)),
			CCCallFunc::create(this, callfunc_selector(OffLineArenaResultUI::showUIAnimation)),
			CCDelayTime::create(1.8f),
			CCCallFunc::create(this, callfunc_selector(OffLineArenaResultUI::createAnimation)),
			NULL);
		this->runAction(action);

		this->setContentSize(winsize);
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);

		return true;
	}
	return false;
}

void OffLineArenaResultUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void OffLineArenaResultUI::onExit()
{
	UIScene::onExit();
}

bool OffLineArenaResultUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return true;
}

void OffLineArenaResultUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void OffLineArenaResultUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void OffLineArenaResultUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void OffLineArenaResultUI::GetRewardEvent( CCObject *pSender )
{
	exitArena();
}

void OffLineArenaResultUI::update( float dt )
{
	m_nRemainTime -= 1000;
	if (m_nRemainTime <= 0)
	{
		m_nRemainTime = 0;
		exitArena();
	}

	char s_remainTime[20];
	sprintf(s_remainTime,"%d",m_nRemainTime/1000);
	l_remainTime->setText(s_remainTime);
}

void OffLineArenaResultUI::FightAgainEvent( CCObject *pSender )
{
	if (OffLineArenaState::getStatus() == OffLineArenaState::status_in_arena)
	{
		if (OffLineArenaState::getCurrentOpppnentId() != -1)
		{
			//请求再次挑战

		}
	}
}

void OffLineArenaResultUI::exitArena()
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(3021);
	this->removeFromParent();
}

void OffLineArenaResultUI::showRankingAnimation()
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	if (m_bIsWin)  //胜利
	{
		mainPanel->setVisible(true);

		UIPanel * panel_ranking = (UIPanel*)UIHelper::seekWidgetByName(mainPanel,"Panel_ranking");
		panel_ranking->setVisible(true);

		UIPanel * panel_reward = (UIPanel*)UIHelper::seekWidgetByName(mainPanel,"Panel_reward");
		panel_reward->setVisible(false);

		UIWidget * widget = UIWidget::create();
		widget->setAnchorPoint(ccp(5.f,.5f));
		widget->setPosition(ccp(345,131));
		widget->setScale(3.0f);
		panel_ranking->addChild(widget);

		UILabelBMFont * l_curRank = UILabelBMFont::create();
		l_curRank->setFntFile("res_ui/font/ziti_13.fnt");
		char s_rank [20];
		sprintf(s_rank,"%d",OffLineArenaData::getInstance()->m_nTargetRanking);
		l_curRank->setText(s_rank);
		l_curRank->setAnchorPoint(ccp(.5f,.5f));
		l_curRank->setPosition(ccp(0,0));
		widget->addChild(l_curRank);

		UIImageView *imageView_di = UIImageView::create();
		imageView_di->setTexture("res_ui/arena/di.png");
		imageView_di->setAnchorPoint(ccp(1.0f,.5f));
		imageView_di->setPosition(ccp(l_curRank->getPosition().y - l_curRank->getContentSize().width/2,l_curRank->getPosition().y));
		widget->addChild(imageView_di);

		UIImageView *imageView_ming = UIImageView::create();
		imageView_ming->setTexture("res_ui/arena/ming.png");
		imageView_ming->setAnchorPoint(ccp(.0f,.5f));
		imageView_ming->setPosition(ccp(l_curRank->getPosition().y + l_curRank->getContentSize().width/2,l_curRank->getPosition().y));
		widget->addChild(imageView_ming);

		CCSequence * sequence = CCSequence::create(
			CCEaseExponentialIn::create(CCScaleTo::create(0.35f,1.0f)),
			CCDelayTime::create(2.0f),
			CCCallFunc::create(this,callfunc_selector(OffLineArenaResultUI::createUI)),
			NULL);
		widget->runAction(sequence);
	}
	else
	{
		this->createUI();
	}
}

void OffLineArenaResultUI::createUI()
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

	mainPanel->setVisible(true);

	UIPanel * panel_ranking = (UIPanel*)UIHelper::seekWidgetByName(mainPanel,"Panel_ranking");
	panel_ranking->setVisible(false);

	UIPanel * panel_reward = (UIPanel*)UIHelper::seekWidgetByName(mainPanel,"Panel_reward");
	panel_reward->setVisible(true);

	UIButton * btn_getReward= (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_enter");
	CCAssert(btn_getReward != NULL, "should not be nil");
	btn_getReward->setTouchEnable(true);
	btn_getReward->setPressedActionEnabled(true);
	btn_getReward->addReleaseEvent(this,coco_releaseselector(OffLineArenaResultUI::GetRewardEvent));

	//倒计时
	l_remainTime = (UILabel*)UIHelper::seekWidgetByName(mainPanel,"Label_time");	

	m_nRemainTime -= (GameUtils::millisecondNow() - countDown_startTime);
	m_nRemainTime -= 1000;

	char s_remainTime[20];
	sprintf(s_remainTime,"%d",m_nRemainTime/1000);
	l_remainTime->setText(s_remainTime);

	this->schedule(schedule_selector(OffLineArenaResultUI::update),1.0f);

	//展示武将得经验
	int space = 19;
	int width = 66;
	int allsize = GameView::getInstance()->generalsInLineList.size()+1;
	float firstPos_x = 400- (space+width)*0.5f*(allsize-1);

	for (int i = 0;i<allsize;++i)
	{
		if (i >= 6)
			continue;

		ResultHeadPartItem * temp = ResultHeadPartItem::create(m_nExp,i);
		temp->ignoreAnchorPointForPosition(false);
		temp->setAnchorPoint(ccp(0.5f,0.5f));
		temp->setPosition(ccp(firstPos_x+(width+space)*i,245));
		u_layer->addChild(temp);
	}
}

void OffLineArenaResultUI::showUIAnimation()
{
	MainScene * mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	if (mainscene != NULL)
	{
		if (m_bIsWin)
		{
			//mainscene->addInterfaceAnm("tzcg/tzcg.anm");
			MainAnimationScene * mainAnmScene = GameView::getInstance()->getMainAnimationScene();
			if (mainAnmScene)
			{
				mainAnmScene->addInterfaceAnm("tzcg/tzcg.anm");
			}
		}
		else
		{
			//mainscene->addInterfaceAnm("tzsb/tzsb.anm");
			MainAnimationScene * mainAnmScene = GameView::getInstance()->getMainAnimationScene();
			if (mainAnmScene)
			{
				mainAnmScene->addInterfaceAnm("tzsb/tzsb.anm");
			}
		}
	}
}
void OffLineArenaResultUI::startSlowMotion()
{
	CCDirector::sharedDirector()->getScheduler()->setTimeScale(0.2f);
}
void OffLineArenaResultUI::endSlowMotion()
{
	CCDirector::sharedDirector()->getScheduler()->setTimeScale(1.0f);
}

void OffLineArenaResultUI::addLightAnimation()
{
	CCSize size = CCDirector::sharedDirector()->getVisibleSize();
	std::string animFileName = "animation/texiao/renwutexiao/SHANGUANG/shanguang.anm";
	CCLegendAnimation *la_light = CCLegendAnimation::create(animFileName);
	if (la_light)
	{
		la_light->setPlayLoop(false);
		la_light->setReleaseWhenStop(true);
		la_light->setPlaySpeed(5.0f);
		la_light->setScale(0.85f);
		la_light->setPosition(ccp(size.width/2,size.height/2));
		GameView::getInstance()->getMainAnimationScene()->addChild(la_light);
	}
}

void OffLineArenaResultUI::createAnimation()
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	u_layer=UILayer::create();
	u_layer->ignoreAnchorPointForPosition(false);
	u_layer->setAnchorPoint(ccp(0.5f,0.5f));
	u_layer->setPosition(ccp(winSize.width/2,winSize.height/2));
	u_layer->setContentSize(CCSizeMake(UI_DESIGN_RESOLUTION_WIDTH,UI_DESIGN_RESOLUTION_HEIGHT));
	addChild(u_layer);

	UIImageView *mengban = UIImageView::create();
	mengban->setTexture("res_ui/zhezhao80.png");
	mengban->setScale9Enable(true);
	mengban->setScale9Size(winSize);
	mengban->setAnchorPoint(ccp(0,0));
	mengban->setPosition(ccp(0,0));
	m_pUiLayer->addWidget(mengban);

	mainPanel = (UIPanel*)GUIReader::shareReader()->widgetFromJsonFile("res_ui/arena_new_1.json");
	mainPanel->setAnchorPoint(ccp(0.5f,0.5f));
	mainPanel->setPosition(ccp(winSize.width/2,winSize.height/2-20));
	m_pUiLayer->addWidget(mainPanel);
	mainPanel->setVisible(false);

	//根据动画名称创建动画精灵
	CCArmature *armature ;
	if (m_bIsWin)
	{
		//从导出文件异步加载动画
		CCArmatureDataManager::sharedArmatureDataManager()->addArmatureFileInfo("res_ui/uiflash/winLogo/winLogo0.png","res_ui/uiflash/winLogo/winLogo0.plist","res_ui/uiflash/winLogo/winLogo.ExportJson");
		armature = CCArmature::create("winLogo");
		armature->getAnimation()->play("Animation_begin");
		armature->getAnimation()->setMovementEventCallFunc(this,movementEvent_selector(OffLineArenaResultUI::ArmatureFinishCallback));  
	}
	else
	{	
		//从导出文件异步加载动画
		CCArmatureDataManager::sharedArmatureDataManager()->addArmatureFileInfo("res_ui/uiflash/lostLogo/lostLogo0.png","res_ui/uiflash/lostLogo/lostLogo0.plist","res_ui/uiflash/lostLogo/lostLogo.ExportJson");
		armature = CCArmature::create("lostLogo");
		//播放指定动作
		armature->getAnimation()->playByIndex(0,-1,-1,ANIMATION_NO_LOOP);
	}
	//修改属性
	armature->setScale(1.0f);
	//设置动画精灵位置
	armature->setPosition(ccp(UI_DESIGN_RESOLUTION_WIDTH/2,UI_DESIGN_RESOLUTION_HEIGHT/2));
	//添加到当前页面
	u_layer->addChild(armature);

	CCSequence * sequence = CCSequence::create(
		CCDelayTime::create(2.3f),
		CCCallFunc::create(this,callfunc_selector(OffLineArenaResultUI::showRankingAnimation)),
		NULL
		);
	this->runAction(sequence);

	/*************************************************************/
	//胜利背后的佛光
// 	UIImageView * imageView_light = UIImageView::create();
// 	imageView_light->setTexture("res_ui/lightlight.png");
// 	imageView_light->setAnchorPoint(ccp(0.5f,0.5f));
// 	imageView_light->setPosition(ccp(UI_DESIGN_RESOLUTION_WIDTH/2,UI_DESIGN_RESOLUTION_HEIGHT/2));
// 	u_layer->addWidget(imageView_light);
// 	imageView_light->setVisible(false);

	//结果
// 	UIImageView * imageView_result = UIImageView::create();
// 	if (m_bIsWin)
// 	{
// 		imageView_result->setTexture("res_ui/jiazuzhan/win.png");
// 	}
// 	else
// 	{
// 		imageView_result->setTexture("res_ui/jiazuzhan/lost.png");
// 	}
// 	imageView_result->setAnchorPoint(ccp(0.5f,0.5f));
// 	imageView_result->setPosition(ccp(UI_DESIGN_RESOLUTION_WIDTH/2,UI_DESIGN_RESOLUTION_HEIGHT/2));
// 	imageView_result->setScale(RESULT_UI_RESULTFLAG_BEGINSCALE);
// 	u_layer->addWidget(imageView_result);

// 	CCSequence * sequence = CCSequence::create(CCScaleTo::create(RESULT_UI_RESULTFLAG_SCALECHANGE_TIME,1.0f,1.0f),
// 		CCDelayTime::create(RESULT_UI_RESULTFLAG_DELAY_TIME),
// 		CCMoveTo::create(RESULT_UI_RESULTFLAG_MOVE_TIME,ccp(UI_DESIGN_RESOLUTION_WIDTH/2,391)),
// 		CCDelayTime::create(0.05f),
// 		CCCallFunc::create(this,callfunc_selector(OffLineArenaResultUI::showRankingAnimation)),
// 		NULL);
// 	imageView_result->runAction(sequence);

// 	if (m_bIsWin)
// 	{
// 		CCRotateBy * rotateAnm = CCRotateBy::create(4.0f,360);
// 		CCRepeatForever * repeatAnm = CCRepeatForever::create(CCSequence::create(rotateAnm,NULL));
// 		CCSequence * sequence_light = CCSequence::create(
// 			CCDelayTime::create(RESULT_UI_RESULTFLAG_SCALECHANGE_TIME),
// 			CCShow::create(),
// 			CCDelayTime::create(RESULT_UI_RESULTFLAG_DELAY_TIME),
// 			CCMoveTo::create(RESULT_UI_RESULTFLAG_MOVE_TIME,ccp(400,391)),
// 			NULL);
// 		imageView_light->runAction(sequence_light);
// 		imageView_light->runAction(repeatAnm);
// 	}
}

void OffLineArenaResultUI::ArmatureFinishCallback( cocos2d::extension::CCArmature *armature, cocos2d::extension::MovementEventType movementType, const char *movementID )
{
	std::string id = movementID;
	if (id.compare("Animation_begin") == 0)
	{
		armature->stopAllActions();
		armature->getAnimation()->play("Animation1_end");
	}
}


