#include "FightNotesUI.h"
#include "../extensions/UIScene.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/CHistory.h"
#include "../extensions/CCRichLabel.h"
#include "../extensions/CCMoveableMenu.h"
#include "AppMacros.h"
#include "../../utils/StaticDataManager.h"
#include "../../utils/StrUtils.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "OffLineArenaUI.h"
#include "GameView.h"
#include "../../gamescene_state/MainScene.h"

#define  SelectImageTag 458

FightNotesUI::FightNotesUI():
lastSelectCellId(0),
selectCellId(0)
{
}


FightNotesUI::~FightNotesUI()
{
	std::vector<CHistory*>::iterator iter;
	for (iter = curHistoryList.begin(); iter != curHistoryList.end(); ++iter)
	{
		delete *iter;
	}
	curHistoryList.clear();
}

FightNotesUI * FightNotesUI::create()
{
	FightNotesUI * fightNotesUI = new FightNotesUI();
	if (fightNotesUI && fightNotesUI->init())
	{
		fightNotesUI->autorelease();
		return fightNotesUI;
	}
	CC_SAFE_DELETE(fightNotesUI);
	return NULL;
}

bool FightNotesUI::init()
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		this->scheduleUpdate(); 

		//加载UI
		if(LoadSceneLayer::RankListLayer->getWidgetParent() != NULL)
		{
			LoadSceneLayer::RankListLayer->removeFromParentAndCleanup(false);
		}
		UIPanel *ppanel = LoadSceneLayer::RankListLayer;
		ppanel->setAnchorPoint(ccp(0.0f,0.0f));
		ppanel->getValidNode()->setContentSize(CCSizeMake(608, 417));
		ppanel->setPosition(CCPointZero);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnable(true);
		m_pUiLayer->addWidget(ppanel);

		UIPanel * panel_fightNotes = (UIPanel *)UIHelper::seekWidgetByName(ppanel,"Panel_conbatRecord");
		panel_fightNotes->setVisible(true);
		UIPanel * panel_rankList = (UIPanel *)UIHelper::seekWidgetByName(ppanel,"Panel_ranking");
		panel_rankList->setVisible(false);
		UIPanel * panel_honorShop = (UIPanel *)UIHelper::seekWidgetByName(ppanel,"Panel_honorShop");
		panel_honorShop->setVisible(false);

		UIButton *Button_close = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnable(true);
		Button_close->addReleaseEvent(this, coco_releaseselector(FightNotesUI::CloseEvent));
		Button_close->setPressedActionEnabled(true);

		m_tableView = CCTableView::create(this,CCSizeMake(517,290));
		m_tableView->setDirection(kCCScrollViewDirectionVertical);
		m_tableView->setAnchorPoint(ccp(0,0));
		m_tableView->setPosition(ccp(45,40));
		m_tableView->setDelegate(this);
		m_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		addChild(m_tableView);

		this->setContentSize(CCSizeMake(608,417));
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);

		return true;
	}
	return false;
}

void FightNotesUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void FightNotesUI::onExit()
{
	UIScene::onExit();
}

bool FightNotesUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return true;
}

void FightNotesUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void FightNotesUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void FightNotesUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void FightNotesUI::CloseEvent( CCObject *pSender )
{
	this->closeAnim();
}

void FightNotesUI::scrollViewDidScroll( CCScrollView* view )
{

}

void FightNotesUI::scrollViewDidZoom( CCScrollView* view )
{

}

void FightNotesUI::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{
	selectCellId = cell->getIdx();
	//取出上次选中状态，更新本次选中状态
	if(table->cellAtIndex(lastSelectCellId))
	{
		if (table->cellAtIndex(lastSelectCellId)->getChildByTag(SelectImageTag))
		{
			table->cellAtIndex(lastSelectCellId)->getChildByTag(SelectImageTag)->setVisible(false);
		}
	}

	if (cell->getChildByTag(SelectImageTag))
	{
		cell->getChildByTag(SelectImageTag)->setVisible(true);
	}

	lastSelectCellId = cell->getIdx();
}

cocos2d::CCSize FightNotesUI::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeMake(511,45);
}

cocos2d::extension::CCTableViewCell* FightNotesUI::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell = new CCTableViewCell();
	cell->autorelease();

	CHistory * history = curHistoryList.at(idx);

	CCScale9Sprite* pHighlightSpr = CCScale9Sprite::create(CCRectMake(5, 5, 1, 1) , "res_ui/mengban_green.png");
	pHighlightSpr->setPreferredSize(CCSize(515,43));
	pHighlightSpr->setAnchorPoint(CCPointZero);
	pHighlightSpr->setPosition(ccp(0,0));
	pHighlightSpr->setTag(SelectImageTag);
	cell->addChild(pHighlightSpr);
	pHighlightSpr->setVisible(false);

	//大边框
	CCScale9Sprite * sprite_bigFrame = CCScale9Sprite::create("res_ui/LV3_dikuang.png");
	sprite_bigFrame->setAnchorPoint(ccp(0, 0));
	sprite_bigFrame->setPosition(ccp(0, 0));
	sprite_bigFrame->setCapInsets(CCRectMake(9,14,1,1));
	sprite_bigFrame->setPreferredSize(CCSizeMake(515,43));
	cell->addChild(sprite_bigFrame);
	//战斗记录信息
	RichLabelDefinition def;
	def.strokeEnabled = false;
	CCRichLabel * rl_des = CCRichLabel::create(this->getFightNotesDes(history).c_str(),CCSizeMake(350,43),def);
	rl_des->setAnchorPoint(ccp(0,0));
	rl_des->setPosition(ccp(12,2));
	rl_des->setScale(0.8f);
	cell->addChild(rl_des);
	//反击
	CCScale9Sprite * image_strikeBack = CCScale9Sprite::create("res_ui/new_button_2.png");
	image_strikeBack->setPreferredSize(CCSizeMake(80,36));
	image_strikeBack->setCapInsets(CCRect(18,9,2,23));
	CCMenuItemSprite *menuSprite_strikeBack = CCMenuItemSprite::create(image_strikeBack, image_strikeBack, image_strikeBack, this, menu_selector(FightNotesUI::StrikeBackEvent));
	menuSprite_strikeBack->setZoomScale(1.3f);
	menuSprite_strikeBack->setAnchorPoint(ccp(0.5f,0.5f));
	menuSprite_strikeBack->setTag(idx);
	menu_strikeBack = CCMoveableMenu::create(menuSprite_strikeBack, NULL);
	menu_strikeBack->setContentSize(CCSizeMake(80,36));
	menu_strikeBack->setPosition(ccp(382,22));
	sprite_bigFrame->addChild(menu_strikeBack);
	menu_strikeBack->setVisible(false);
	CCLabelTTF * l_strikeBack = CCLabelTTF::create(StringDataManager::getString("Arena_fightNotes_strikeBack"),APP_FONT_NAME,16);
	l_strikeBack->setAnchorPoint(ccp(.5f,.5f));
	l_strikeBack->setPosition(ccp(40,18));
	menuSprite_strikeBack->addChild(l_strikeBack);

	//查看
	CCScale9Sprite * image_lookOver = CCScale9Sprite::create("res_ui/new_button_1.png");
	image_lookOver->setPreferredSize(CCSizeMake(80,36));
	image_lookOver->setCapInsets(CCRect(18,9,2,23));
	CCMenuItemSprite *menuSprite_lookOver = CCMenuItemSprite::create(image_lookOver, image_lookOver, image_lookOver, this, menu_selector(FightNotesUI::LookOverEvent));
	menuSprite_lookOver->setZoomScale(1.3f);
	menuSprite_lookOver->setAnchorPoint(ccp(0.5f,0.5f));
	menuSprite_lookOver->setTag(idx);
	menu_lookOver = CCMoveableMenu::create(menuSprite_lookOver, NULL);
	menu_lookOver->setContentSize(CCSizeMake(80,36));
	menu_lookOver->setPosition(ccp(467,22));
	sprite_bigFrame->addChild(menu_lookOver);
	CCLabelTTF * l_lookOver = CCLabelTTF::create(StringDataManager::getString("friend_check"),APP_FONT_NAME,16);
	l_lookOver->setAnchorPoint(ccp(.5f,.5f));
	l_lookOver->setPosition(ccp(40,18));
	menuSprite_lookOver->addChild(l_lookOver);

	if(history->type() == PASSIVE)
	{
		if (!history->win())
		{
			menu_strikeBack->setVisible(true);
		}
		else
		{
			menu_strikeBack->setVisible(false);
		}
	}
	else
	{
		menu_strikeBack->setVisible(false);
	}

	if (this->selectCellId == idx)
	{
		pHighlightSpr->setVisible(true);
	}

	return cell;
}

unsigned int FightNotesUI::numberOfCellsInTableView( CCTableView *table )
{
	return curHistoryList.size();
}

void FightNotesUI::RefreshData()
{
	m_tableView->reloadData();
}

void FightNotesUI::StrikeBackEvent( CCObject *pSender )
{
	CCMenuItemSprite *menuSprite_lookOver = dynamic_cast<CCMenuItemSprite*>(pSender);
	if (!menuSprite_lookOver)
		return;

	selectCellId = menuSprite_lookOver->getTag();

	if (curHistoryList.size()>0)
	{
		long long roleId = curHistoryList.at(selectCellId)->partnerid();
		GameMessageProcessor::sharedMsgProcessor()->sendReq(3006,(void *)roleId);

		OffLineArenaUI * offLineAreaUI = (OffLineArenaUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOffLineArenaUI);
		if (offLineAreaUI)
		{
			offLineAreaUI->closeAnim();
		}
		this->closeAnim();
	}
	else
	{

	}
}

void FightNotesUI::LookOverEvent( CCObject *pSender )
{
	CCMenuItemSprite *menuSprite_lookOver = dynamic_cast<CCMenuItemSprite*>(pSender);
	if (!menuSprite_lookOver)
		return;

	selectCellId = menuSprite_lookOver->getTag();

	if (curHistoryList.size()>0)
	{
		long long roleId = curHistoryList.at(selectCellId)->partnerid();
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1142,(void *)roleId);
	}
	else
	{

	}
}

std::string FightNotesUI::getFightNotesDes( CHistory * history )
{
	std::string str_des = "";
	//您在
	//str_des.append(StringDataManager::getString("Arena_fightNotes_des_par1"));
	str_des.append(StrUtils::applyColor(StringDataManager::getString("Arena_fightNotes_des_par1"),ccc3(47,93,13)).c_str());
	//2014.1.2
	//str_des.append(history->timestamp().c_str());
	str_des.append(StrUtils::applyColor(history->timestamp().c_str(),ccc3(47,93,13)).c_str());
	//挑战还是被挑战
	if (history->type() == PASSIVE)
	{
		//str_des.append(StringDataManager::getString("Arena_fightNotes_des_par2_1"));
		str_des.append(StrUtils::applyColor(StringDataManager::getString("Arena_fightNotes_des_par2_1"),ccc3(47,93,13)).c_str());

		std::string str_name;
		str_name.append(StringDataManager::getString("Arena_fightNotes_des_par_char1"));
		str_name.append(history->partnername());
		str_name.append(StringDataManager::getString("Arena_fightNotes_des_par_char2"));

		str_des.append(StrUtils::applyColor(str_name.c_str(),ccc3(0,151,39)));
		//str_des.append(StringDataManager::getString("Arena_fightNotes_des_par2_2"));
		str_des.append(StrUtils::applyColor(StringDataManager::getString("Arena_fightNotes_des_par2_2"),ccc3(47,93,13)).c_str());
	}
	else
	{
		//str_des.append(StringDataManager::getString("Arena_fightNotes_des_par2_3"));
		str_des.append(StrUtils::applyColor(StringDataManager::getString("Arena_fightNotes_des_par2_3"),ccc3(47,93,13)).c_str());
		std::string str_name;
		str_name.append(StringDataManager::getString("Arena_fightNotes_des_par_char1"));
		str_name.append(history->partnername());
		str_name.append(StringDataManager::getString("Arena_fightNotes_des_par_char2"));

		str_des.append(StrUtils::applyColor(str_name.c_str(),ccc3(0,151,39)));
	}
	//结果
	//str_des.append(StringDataManager::getString("Arena_fightNotes_des_par3"));
	str_des.append(StrUtils::applyColor(StringDataManager::getString("Arena_fightNotes_des_par3"),ccc3(47,93,13)).c_str());
	if (history->win())
	{
		std::string str_result;
		str_result.append(StringDataManager::getString("Arena_fightNotes_des_par_char1"));
		str_result.append(StringDataManager::getString("Arena_fightNotes_des_par3_1"));
		str_result.append(StringDataManager::getString("Arena_fightNotes_des_par_char2"));
		
		str_des.append(StrUtils::applyColor(str_result.c_str(),ccc3(184,0,0)).c_str());
	}
	else
	{
		std::string str_result;
		str_result.append(StringDataManager::getString("Arena_fightNotes_des_par_char1"));
		str_result.append(StringDataManager::getString("Arena_fightNotes_des_par3_2"));
		str_result.append(StringDataManager::getString("Arena_fightNotes_des_par_char2"));

		str_des.append(StrUtils::applyColor(str_result.c_str(),ccc3(60,65,56)).c_str());
	}
	//排名
	//str_des.append(StringDataManager::getString("Arena_fightNotes_des_par4"));
	str_des.append(StrUtils::applyColor(StringDataManager::getString("Arena_fightNotes_des_par4"),ccc3(47,93,13)).c_str());
	if (history->type() == PASSIVE)   //被挑战（成功排名不变，失败排名下降）
	{
		if(history->win())
		{
			//str_des.append(StringDataManager::getString("Arena_fightNotes_des_par4_3"));
			str_des.append(StrUtils::applyColor(StringDataManager::getString("Arena_fightNotes_des_par4_3"),ccc3(47,93,13)).c_str());
		}
		else
		{
			//str_des.append(StringDataManager::getString("Arena_fightNotes_des_par4_2"));
			str_des.append(StrUtils::applyColor(StringDataManager::getString("Arena_fightNotes_des_par4_2"),ccc3(47,93,13)).c_str());
			char s_ranking [20];
			sprintf(s_ranking,"%d",history->ranking());
			//str_des.append(s_ranking);
			str_des.append(StrUtils::applyColor(s_ranking,ccc3(47,93,13)).c_str());
		}
	}
	else            //主动挑战（成功排名上升，失败排名不变）
	{
		if(history->win())
		{
			//str_des.append(StringDataManager::getString("Arena_fightNotes_des_par4_1"));
			str_des.append(StrUtils::applyColor(StringDataManager::getString("Arena_fightNotes_des_par4_1"),ccc3(47,93,13)).c_str());
			char s_ranking [20];
			sprintf(s_ranking,"%d",history->ranking());
			//str_des.append(s_ranking);
			str_des.append(StrUtils::applyColor(s_ranking,ccc3(47,93,13)).c_str());
		}
		else
		{
			//str_des.append(StringDataManager::getString("Arena_fightNotes_des_par4_3"));
			str_des.append(StrUtils::applyColor(StringDataManager::getString("Arena_fightNotes_des_par4_3"),ccc3(47,93,13)).c_str());
		}
	}
	return str_des;
}
