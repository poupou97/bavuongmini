
#ifndef _OFFLINEARENA_HONORSHOPITEMBUYINFO_H_
#define _OFFLINEARENA_HONORSHOPITEMBUYINFO_H_

#include "../extensions/UIScene.h"

class GoodsInfo;
class CHonorCommodity;

class HonorShopItemBuyInfo : public UIScene
{
public:
	HonorShopItemBuyInfo();
	~HonorShopItemBuyInfo();

	static HonorShopItemBuyInfo * create(CHonorCommodity * honorCommodity);
	bool init(CHonorCommodity * honorCommodity);

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

	void BuyEvent(CCObject * pSender);
	void CloseEvent(CCObject * pSender);

	void getAmount(CCObject * obj);
	void clearNum(CCObject * obj);
	void deleteNum(CCObject * obj);

	void setToDefaultNum();
private:
	UILayer * u_layer;
	CHonorCommodity * curHonorCommodity;
	UITextButton * btn_inputNumValue;
	UILabel * l_constGoldValue;

	//该物品单价
	int basePrice;
	//购买的数量
	int buyNum;
	std::string str_buyNum;
	//购买的总价
	int buyPrice;

	//单价 
	UILabel * l_priceOfOne;
	//购买数量  
	UILabel * l_buyNum;
	//购买数量  
	UILabel * l_totalPrice;
};

#endif

