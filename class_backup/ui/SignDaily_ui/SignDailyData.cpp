#include "SignDailyData.h"
#include "SignDaily.h"
#include "../../messageclient/element/COneSignEverydayGift.h"
#include "../../messageclient/element/GoodsInfo.h"

SignDailyData * SignDailyData::s_signDailyData = NULL;

SignDailyData::SignDailyData(void)
	:m_nIsLogin(0)
	,m_nLoginPlayerLevel(1)
{
}


SignDailyData::~SignDailyData(void)
{
	std::vector<COneSignEverydayGift *>::iterator iter;
	for (iter = m_vector_internt_sign.begin(); iter != m_vector_internt_sign.end(); iter++)
	{
		delete *iter;
	}
	m_vector_internt_sign.clear();

	std::vector<SignDaily *>::iterator iterOnlineGift;
	for (iterOnlineGift = m_vector_native_sign.begin(); iterOnlineGift != m_vector_native_sign.end(); iterOnlineGift++)
	{
		delete *iterOnlineGift;
	}
	m_vector_native_sign.clear();

}

SignDailyData * SignDailyData::instance()
{
	if (NULL == s_signDailyData)
	{
		s_signDailyData = new SignDailyData();
	}

	return s_signDailyData;
}

void SignDailyData::initDataFromIntent()
{
	// 清空本地数据
	std::vector<SignDaily *>::iterator iter;
	for (iter = m_vector_native_sign.begin(); iter != m_vector_native_sign.end(); iter++)
	{
		delete *iter;
	}
	m_vector_native_sign.clear();

	for (unsigned int i=0; i < m_vector_internt_sign.size(); i++)
	{
		SignDaily * signDaily = new SignDaily();
		signDaily->m_nGiftIndex = m_vector_internt_sign.at(i)->number();							// 奖励编号，从0开始
		signDaily->m_nStatus = m_vector_internt_sign.at(i)->status();									// 奖励状态（1.不可领取奖励，2.可领取奖励，3.已领取奖励完毕）

		int nGoodSize = m_vector_internt_sign.at(i)->goods_size();										// 将 goods 添加到 signDaily成员变量中的 vector
		for (int goodSizeIndex = 0; goodSizeIndex < nGoodSize; goodSizeIndex++)
		{
			GoodsInfo * goodsInfo = new GoodsInfo();
			goodsInfo->CopyFrom(m_vector_internt_sign.at(i)->goods(goodSizeIndex));

			signDaily->m_vector_goods.push_back(goodsInfo);
		}

		int nGoodsNumSize = m_vector_internt_sign.at(i)->goodsnumber_size();					// 将 goodsnumber 添加到 signDaily成员变量中的 vector
		for (int goodNumSizeIndex = 0; goodNumSizeIndex < nGoodsNumSize; goodNumSizeIndex++)
		{
			signDaily->m_vector_goodsNum.push_back(m_vector_internt_sign.at(i)->goodsnumber(goodNumSizeIndex));
		}

		m_vector_native_sign.push_back(signDaily);
	}
}

GoodsInfo* SignDailyData::getGoosInfo(unsigned int nGift, int nGiftNum )
{
	for (unsigned int i = 0; i < m_vector_native_sign.size(); i++)
	{
		if (nGift < m_vector_native_sign.size())
		{
			return m_vector_native_sign.at(nGift)->m_vector_goods.at(nGiftNum);
		}
	}

	return NULL;
}

void SignDailyData::setIsLogin( int nIsLogin )
{
	m_nIsLogin = nIsLogin;
}

int SignDailyData::getIsLogin()
{
	return m_nIsLogin;
}

bool SignDailyData::isCanSign()
{
	for (unsigned int i = 0; i < m_vector_internt_sign.size(); i++)
	{
		// 如果可签到的话，返回 true
		if (2 == m_vector_internt_sign.at(i)->status())
		{
			return true;
		}
	}

	return false;
}

void SignDailyData::setLoginPlayerLevel( int nPlayerLevel )
{
	this->m_nLoginPlayerLevel = nPlayerLevel;
}

int SignDailyData::getLoginPalyerLevel()
{
	return m_nLoginPlayerLevel;
}

