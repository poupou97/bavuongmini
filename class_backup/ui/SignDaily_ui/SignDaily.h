#ifndef _UI_SIGNDAILY_SIGNDAILY_H_
#define _UI_SIGNDAILY_SIGNDAILY_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

/////////////////////////////////
/**
 * 签到模块（用于 定义UI界面的元素）
 * @author liuliang
 * @version 0.1.0
 * @date 2014.03.18
 */

class GoodsInfo;

class SignDaily
{
public:
	SignDaily(void);
	~SignDaily(void);

public:
	UIButton * m_btn_sign;									// 签到btn
	UILabel * m_label_btnText;								// 签到btn的text（签到，已领取）

	UIImageView * m_image_red;							// 当前正在进行的签到（红色背景高亮显示）

	UIImageView * m_image_text;							// 连续X天 image
	UILabel * m_label_timeNeed;							// 连续X天	

	UIButton * m_btn_gift1_frame;				// 签到奖励1
	UIImageView * m_image_gift1_icon;			
	UILabel * m_label_gift1_num;

	UIButton * m_btn_gift2_frame;				// 签到奖励2
	UIImageView * m_image_gift2_icon;			
	UILabel * m_label_gift2_num;

	UIButton * m_btn_gift3_frame;				// 签到奖励3
	UIImageView * m_image_gift3_icon;			
	UILabel * m_label_gift3_num;

	UIButton * m_btn_gift4_frame;				// 签到奖励4
	UIImageView * m_image_gift4_icon;			
	UILabel * m_label_gift4_num;

	int m_nGiftIndex;												// 当前奖励编号
	int m_nStatus;													// 奖励领取状态（1.不可领取奖励，2.可领取奖励，3.已领取奖励完毕）
	
	std::vector<GoodsInfo *> m_vector_goods;	// OneSignEverydayGift 结构体中 repeated Goods
	std::vector<int > m_vector_goodsNum;			// OneSignEverydayGift 结构体重 repeated int32

};

#endif

