#include "SignDailyIcon.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "SignDailyData.h"
#include "SignDaily.h"
#include "SignDailyUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"

#define  TAG_TUTORIALPARTICLE 600

SignDailyIcon::SignDailyIcon(void)
	:m_btn_signDailyIcon(NULL)
	,m_layer_signDaily(NULL)
	,m_label_signDailyIcon_text(NULL)
	,m_spirte_fontBg(NULL)
	,m_bIsCanSign(false)
{

}


SignDailyIcon::~SignDailyIcon(void)
{
	
}

SignDailyIcon* SignDailyIcon::create()
{
	SignDailyIcon * signDailyIcon = new SignDailyIcon();
	if (signDailyIcon && signDailyIcon->init())
	{
		signDailyIcon->autorelease();
		return signDailyIcon;
	}
	CC_SAFE_DELETE(signDailyIcon);
	return NULL;
}

bool SignDailyIcon::init()
{
	if (UIScene::init())
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

		// icon
		m_btn_signDailyIcon = UIButton::create();
		m_btn_signDailyIcon->setTouchEnable(true);
		m_btn_signDailyIcon->setPressedActionEnabled(true);
		//m_btn_signDailyIcon->setTextures("gamescene_state/zhujiemian2/tubiao/OnlineAwards.png", "gamescene_state/zhujiemian2/tubiao/OnlineAwards.png","");
		m_btn_signDailyIcon->setTextures("res_ui/meiriqiandao.png", "res_ui/meiriqiandao.png","");
		m_btn_signDailyIcon->setAnchorPoint(ccp(0.5f, 0.5f));
		m_btn_signDailyIcon->setPosition(ccp( winSize.width - 70 - 159 - 80, winSize.height-70));
		m_btn_signDailyIcon->addReleaseEvent(this, coco_cancelselector(SignDailyIcon::callBackBtnSign));

		// label
		m_label_signDailyIcon_text = UILabel::create();
		m_label_signDailyIcon_text->setText("bu ke qian dao");
		m_label_signDailyIcon_text->setColor(ccc3(255, 246, 0));
		m_label_signDailyIcon_text->setFontSize(14);
		m_label_signDailyIcon_text->setAnchorPoint(ccp(0.5f, 0.5f));
		m_label_signDailyIcon_text->setPosition(ccp(m_btn_signDailyIcon->getPosition().x, m_btn_signDailyIcon->getPosition().y - m_btn_signDailyIcon->getContentSize().height / 2 - 5));


		// text_bg
		m_spirte_fontBg = CCScale9Sprite::create(CCRectMake(5, 10, 1, 4) , "res_ui/zhezhao70.png");
		m_spirte_fontBg->setPreferredSize(CCSize(47, 14));
		m_spirte_fontBg->setAnchorPoint(ccp(0.5f, 0.5f));
		m_spirte_fontBg->setPosition(m_label_signDailyIcon_text->getPosition());


		m_layer_signDaily = UILayer::create();
		m_layer_signDaily->setTouchEnabled(true);
		m_layer_signDaily->setAnchorPoint(CCPointZero);
		m_layer_signDaily->setPosition(CCPointZero);
		m_layer_signDaily->setContentSize(winSize);

		m_layer_signDaily->addChild(m_spirte_fontBg, -10);
		m_layer_signDaily->addWidget(m_btn_signDailyIcon);
		m_layer_signDaily->addWidget(m_label_signDailyIcon_text);

		addChild(m_layer_signDaily);

		this->schedule(schedule_selector(SignDailyIcon::update),1.0f);

		this->setContentSize(winSize);

		return true;
	}
	return false;
}

void SignDailyIcon::callBackBtnSign( CCObject *obj )
{

	// 初始化 UI WINDOW数据
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

	SignDailyUI* pTmpSignDailyUI = (SignDailyUI *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagSignDailyUI);
	if (NULL == pTmpSignDailyUI)
	{
		SignDailyUI * pSignDailyUI = SignDailyUI::create();
		pSignDailyUI->ignoreAnchorPointForPosition(false);
		pSignDailyUI->setAnchorPoint(ccp(0.5f, 0.5f));
		pSignDailyUI->setPosition(ccp(winSize.width / 2, winSize.height / 2));
		pSignDailyUI->setTag(kTagSignDailyUI);
		
		GameView::getInstance()->getMainUIScene()->addChild(pSignDailyUI);

		pSignDailyUI->refreshUI();
	}
}

void SignDailyIcon::onEnter()
{
	UIScene::onEnter();
}

void SignDailyIcon::onExit()
{
	UIScene::onExit();
}

void SignDailyIcon::initDataFromIntent()
{
	std::vector<SignDaily *> vector_native_sign = SignDailyData::instance()->m_vector_native_sign;
	int nSignSize = vector_native_sign.size();
	for (int i = 0; i < nSignSize; i++)
	{
		int nGiftIndex = vector_native_sign.at(i)->m_nGiftIndex;
		int nGiftStatus = vector_native_sign.at(i)->m_nStatus;

		if (2 == nGiftStatus)
		{
			m_bIsCanSign = true;

			break;
		}
	}

	refreshIcon();
}

void SignDailyIcon::setIsCanSign( bool bIsCanSign )
{
	this->m_bIsCanSign = bIsCanSign;
}

void SignDailyIcon::update( float dt )
{
	refreshIcon();
}

void SignDailyIcon::refreshIcon()
{
	// 如果 可以签到，则 text显示“可签到”；否则，显示“已签到”
	if (m_bIsCanSign)
	{
		std::string str_text = "";

		const char *str1 = StringDataManager::getString("label_keqiandao");
		str_text.append(str1);

		m_label_signDailyIcon_text->setText(str_text.c_str());

		// 可签到的话，则显示 粒子特效
		CCTutorialParticle* tutorialParticle = (CCTutorialParticle*)this->getChildByTag(TAG_TUTORIALPARTICLE);
		if (NULL == tutorialParticle)
		{
			m_tutorialParticle = CCTutorialParticle::create("tuowei0.plist", m_btn_signDailyIcon->getContentSize().width, m_btn_signDailyIcon->getContentSize().height + 10);
			m_tutorialParticle->setPosition(ccp(m_btn_signDailyIcon->getPosition().x, m_btn_signDailyIcon->getPosition().y - 35));
			m_tutorialParticle->setAnchorPoint(ccp(0.5f, 0.5f));
			m_tutorialParticle->setTag(TAG_TUTORIALPARTICLE);

			this->addChild(m_tutorialParticle);
		}

	}
	else
	{
		std::string str_text = "";

		const char *str2 = StringDataManager::getString("label_yiqiandao");
		str_text.append(str2);

		m_label_signDailyIcon_text->setText(str_text.c_str());

		// 不可签到的话，粒子特效 去除
		CCTutorialParticle* tutorialParticle = (CCTutorialParticle*)this->getChildByTag(TAG_TUTORIALPARTICLE);
		if (NULL != tutorialParticle)
		{
			this->removeChild(m_tutorialParticle);
		}

	}

}

