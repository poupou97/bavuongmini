#ifndef _UI_RANK_RANKUI_H_
#define _UI_RANK_RANKUI_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "../extensions/UITab.h"

USING_NS_CC;
USING_NS_CC_EXT;

/////////////////////////////////
/**
 * 排行榜UI
 * @author liuliang
 * @version 0.1.0
 * @date 2014.04.08
 */

#define  TAG_SCROLLVIEW 50

class CRoleMessage;
class CHomeMessage;
class ControlTree;
class RankUI:public UIScene, public CCTableViewDataSource, public CCTableViewDelegate
{
public:
	RankUI(void);
	~RankUI(void);

public:
	static UIPanel * s_pPanel;
	static RankUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);


	//add scrolle tree by liuzhenxing
	ControlTree * tree_;
	void callBackTreeEvent(CCObject * obj);
	void callBackSecondTreeEvent(CCObject * obj);

	void refreshDataByTreeIndex();
	void refreshDataBySecondTreeIndex(int index);
private:
	UILayer * m_base_layer;																							// 基调layer
	//CCTableView * m_leftView;																						// 左侧tableView
	CCTableView * m_dataView;																						// 数据tableView

	UITab * m_tab_pro;																							// professsion_tab
	UITab * m_tab_country;																						// country_tab
																													

	UIPanel * m_panel_level;																					// 等级
	UIPanel * m_panel_combat;																					// 战斗力
	UIPanel * m_panel_coin;																						// 金币
	UIPanel * m_panel_family;																					// 家族
	UIPanel * m_panel_battleAch;																				// 战功
	UIPanel * m_panel_flower;																					// 鲜花
	

	UILabel * m_label_myRankLabel;																				// 我的排行label
	UILabel * m_label_myRankNum;																				// 我的排行num
	UILabel * m_label_myRankInfo;																				// 没有上榜，加油哦！（用于鲜花榜）

	std::vector<std::string > m_vector_tableView;

	bool m_bHasReqInternet;																							// 是否已经向服务器请求过数据

public:
	void set_hasReqInternet(bool bFlag);
	bool get_hasReqInternet();

	UILayer* getBaseUILayer();

private:
	void initUI();
	void initTimeFlag();				
	void reloadTimeFlag(int nRankType, int nProfession);
	void reloadTimeFlag();

	void judgeTimeFlag();

public:
	void callBackBtnClose(CCObject * obj);
	void callBackBtnFamilyInfo(CCObject * pSender);

	void proTypeTabIndexChangedEvent(CCObject * pSender);
	void countryTypeTabIndexChangedEvent(CCObject * pSender);
	void flowerTypeTabIndexChagedEvent(CCObject * pSender);

	void reloadDataTableView();																						// 更新 scrollView data

	void initDataFromDb(std::vector<CRoleMessage *> vector_roleMessage);				// 从db获取数据到m_vector_rankPlayer
	void initDataFromDb(std::vector<CHomeMessage *> vector_familyMessage);		// 从db获取数据到m_vector_family

	void refreshDataTableView();																						// 刷新dataTableView
	void refreshDataTableViewWithChangeOffSet();															// 刷新dataTableView（有偏移）

	void reloadMyRankLabel(int nRankItemType);											// 按照 排行的类型（职业、国家）

	std::string getProfession(int nProfession);
	std::string getCountry(int nCountry);

	enum
	{
		RANKITEMTYPE_PROFESSION = 0,													// 职业	
		RANKITEMTYPE_COUNTRY,															// 国家
		RANKITEMTYPE_FLOWER,															// 鲜花
	};
};

struct RankDataStruct
{
	int nType;																												//请求类别 0:等级;1:战斗力；2:金币；3:战功；4：家族；5：鲜花
	int nWork;																												//职业 0.全部、1.猛将、2.鬼谋、3.豪杰、4.神射、
	int nPage;																												//第几页
};

struct RankPlayerStruct
{
	int nRank;																												// 排名
	std::string strName;																								// 名称
	int nMount;																											// 等级 or 战斗力 or 金币 or 鲜花
	std::string strProfession;																						// 职业
	std::string strCountry;																							// 国家
	int nCountry;
	int nVipLevel;                                                                                                        // VIP等级
	int nLevel;
	int nFlowerType;

	long long longPlayerId;																							// 数据的id
	int nPageCur;
	int nPageAll;
	int nMyPlayerRank;
	long long longMyPlayerId;																						// 登陆账号的id
	
};

struct RankFamilyStruct
{
	int nRank;																												// 排名
	std::string strFamilyName;																					// 家族名称
	int nLevel;																												// 等级
	int nFamilyMember;																								// 人数
	int nFamilyMemberMax;																							// 家族人数上限
	std::string strCountry;																							// 国家

	int nPageCur;
	int nPageAll;
	int nMyRank;

	long long longFamilyId;
	std::string strCreaterName;
	std::string strAnnouncement;
};

#endif

