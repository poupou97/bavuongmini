#include "RankIcon.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "RankUI.h"
#include "../../gamescene_state/MainScene.h"

RankIcon::RankIcon(void)
	:m_btn_rankIcon(NULL)
	,m_layer_rank(NULL)
	,m_label_rank_text(NULL)
	,m_spirte_fontBg(NULL)
{

}


RankIcon::~RankIcon(void)
{
	
}

RankIcon* RankIcon::create()
{
	RankIcon * rankIcon = new RankIcon();
	if (rankIcon && rankIcon->init())
	{
		rankIcon->autorelease();
		return rankIcon;
	}
	CC_SAFE_DELETE(rankIcon);
	return NULL;
}

bool RankIcon::init()
{
	if (UIScene::init())
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

		// icon
		m_btn_rankIcon = UIButton::create();
		m_btn_rankIcon->setTouchEnable(true);
		m_btn_rankIcon->setPressedActionEnabled(true);

		m_btn_rankIcon->setTextures("gamescene_state/zhujiemian3/tubiao/active.png", "gamescene_state/zhujiemian3/tubiao/active.png","");
		
		m_btn_rankIcon->setAnchorPoint(ccp(0.5f, 0.5f));
		m_btn_rankIcon->setPosition(ccp( winSize.width - 70 - 159 - 80 - 80, winSize.height-70 - 80));
		m_btn_rankIcon->addReleaseEvent(this, coco_cancelselector(RankIcon::callBackRank));

		// label
		const char * str_active = StringDataManager::getString("activy_chartInfo2");

		m_label_rank_text = UILabel::create();
		m_label_rank_text->setText(str_active);
		m_label_rank_text->setColor(ccc3(255, 246, 0));
		m_label_rank_text->setFontSize(14);
		m_label_rank_text->setAnchorPoint(ccp(0.5f, 0.5f));
		m_label_rank_text->setPosition(ccp(m_btn_rankIcon->getPosition().x, m_btn_rankIcon->getPosition().y - m_btn_rankIcon->getContentSize().height / 2 - 5));


		// text_bg
		m_spirte_fontBg = CCScale9Sprite::create(CCRectMake(5, 10, 1, 4) , "res_ui/zhezhao70.png");
		m_spirte_fontBg->setPreferredSize(CCSize(47, 14));
		m_spirte_fontBg->setAnchorPoint(ccp(0.5f, 0.5f));
		m_spirte_fontBg->setPosition(m_label_rank_text->getPosition());


		m_layer_rank = UILayer::create();
		m_layer_rank->setTouchEnabled(true);
		m_layer_rank->setAnchorPoint(CCPointZero);
		m_layer_rank->setPosition(CCPointZero);
		m_layer_rank->setContentSize(winSize);

		m_layer_rank->addWidget(m_btn_rankIcon);

		addChild(m_layer_rank);

		this->setContentSize(winSize);

		return true;
	}
	return false;
}

void RankIcon::callBackRank( CCObject *obj )
{
	// ��ʼ�� UI WINDOW����
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

	RankUI* pTmpRankUI = (RankUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRankUI);
	if (NULL == pTmpRankUI)
	{
		RankUI * pRankUI = RankUI::create();
		pRankUI->ignoreAnchorPointForPosition(false);
		pRankUI->setAnchorPoint(ccp(0.5f, 0.5f));
		pRankUI->setPosition(ccp(winSize.width / 2, winSize.height / 2));
		pRankUI->setTag(kTagRankUI);

		GameView::getInstance()->getMainUIScene()->addChild(pRankUI);
	}
}

void RankIcon::onEnter()
{
	UIScene::onEnter();
}

void RankIcon::onExit()
{
	UIScene::onExit();
}
