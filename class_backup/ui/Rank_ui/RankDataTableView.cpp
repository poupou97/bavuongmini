#include "RankDataTableView.h"
#include "RankUI.h"
#include "../../AppMacros.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "RankData.h"
#include "../../ui/extensions/CCMoveableMenu.h"
#include "../../gamescene_state/sceneelement/TargetInfo.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/GameConfig.h"

const CCSize CCSizeDataCellSize = CCSizeMake(510, 37);

#define PAGE_NUM 10
#define TAG_SELECT_ITEM 600

RankDataTableView * RankDataTableView::s_rankDataTableView = NULL;

RankDataTableView * RankDataTableView::instance()
{
	if (NULL == s_rankDataTableView)
	{
		s_rankDataTableView = new RankDataTableView();
	}

	return s_rankDataTableView;
}

RankDataTableView::RankDataTableView( void )
	:m_nPageCur(0)
	,m_nPageAll(0)
	,m_nPageStatus(TableHaveNext)
	,m_nLastSelectCellId(0)
	,m_nSelectCellId(0)
{

}

RankDataTableView::~RankDataTableView( void )
{
	clearVectorRankPlayer();
	clearVectorRankFamily();
}

void RankDataTableView::scrollViewDidScroll( CCScrollView* view )
{

}

void RankDataTableView::scrollViewDidZoom( CCScrollView* view )
{

}

void RankDataTableView::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{
	// 设置选中时的底图
	m_nSelectCellId = cell->getIdx();

	//取出上次选中状态，更新本次选中状态
	if(table->cellAtIndex(m_nLastSelectCellId))
	{
		if (table->cellAtIndex(m_nLastSelectCellId)->getChildByTag(TAG_SELECT_ITEM))
		{
			table->cellAtIndex(m_nLastSelectCellId)->getChildByTag(TAG_SELECT_ITEM)->setVisible(false);
		}
	}

	if (cell->getChildByTag(TAG_SELECT_ITEM))
	{
		cell->getChildByTag(TAG_SELECT_ITEM)->setVisible(true);
	}

	m_nLastSelectCellId = cell->getIdx();


	// 交互面板逻辑
	RankUI* pRankUI = (RankUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRankUI);
	if (NULL == pRankUI)
	{
		//GameView::getInstance()->showAlertDialog("tableView error");
		return ;
	}
	if (NULL == pRankUI->getBaseUILayer()->getChildByTag(TAG_SCROLLVIEW))
	{
		//GameView::getInstance()->showAlertDialog("tableView error");
		return ;
	}

	CCTableView * dataView = (CCTableView *)pRankUI->getBaseUILayer()->getChildByTag(TAG_SCROLLVIEW);

	float dataViewPositionX = dataView->getPositionX();
	float dataViewPositionY = dataView->getPositionY();

	float rankUIPositionX = pRankUI->getPositionX();
	float rankUIPositionY = pRankUI->getPositionY();

	float dataViewWidth = dataView->getContentSize().width;
	float dataViewHeight = dataView->getContentSize().height;

	float finalPositionX = dataViewPositionX + rankUIPositionX;
	float finalPositionY = dataViewPositionY + rankUIPositionY;

	int nRankType = RankData::instance()->get_rankType();

	long long longPlayerId = 0;
	std::string strPlayerName = "";
	std::string country_ = "";
	int vipLevel = 0;
	int level = 0;
	std::string  pressionStr = "";
	if (0 == nRankType || 1 == nRankType || 2 == nRankType || 3 == nRankType || 5 == nRankType)
	{
		RankPlayerStruct * rankPlayerStruct = m_vector_rankPlayer.at(cell->getIdx());
		longPlayerId = rankPlayerStruct->longPlayerId;
		strPlayerName = rankPlayerStruct->strName;
		country_ = rankPlayerStruct->strCountry;
		vipLevel = rankPlayerStruct->nVipLevel;
		pressionStr = rankPlayerStruct->strProfession;
	}
	else if (4 == nRankType)
	{
		return ;
	}

	if (0 == longPlayerId && "" == strPlayerName)
	{
		//GameView::getInstance()->showAlertDialog("playerId or playerName is error");
		return ;
	}

	RoleBase* base = GameView::getInstance()->myplayer->getActiveRole()->mutable_rolebase();
	long long longRoleId = base->roleid();
	// 如果排行榜中 选中的是自己，则不弹出 交互面板
	if (longPlayerId == longRoleId)
	{
		return ;
	}
	
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

	TargetInfoList * list = (TargetInfoList*)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagMainSceneTargetInfo);
	if (list != NULL)
	{
		list->removeFromParentAndCleanup(true);
	}

	int country_Id = 0;
	int pressionId = BasePlayer::getProfessionIdxByName(pressionStr);
	TargetInfoList * tarlist = TargetInfoList::create(longPlayerId, strPlayerName,country_Id,vipLevel,level,pressionId);
	tarlist->ignoreAnchorPointForPosition(false);
	tarlist->setAnchorPoint(ccp(0, 0));
	tarlist->setTag(ktagMainSceneTargetInfo);
	tarlist->setPosition(ccp(finalPositionX, finalPositionY));
	//tarlist->setPosition(ccp(winSize.width / 2, winSize.height / 2));
	GameView::getInstance()->getMainUIScene()->addChild(tarlist);
}

cocos2d::CCSize RankDataTableView::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeDataCellSize;
}

cocos2d::extension::CCTableViewCell* RankDataTableView::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCString *string = CCString::createWithFormat("%d", idx);
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell = new CCTableViewCell();
	cell->autorelease();


	int nRankType = RankData::instance()->get_rankType();

	if (0 == nRankType || 1 == nRankType || 2 == nRankType || 3 == nRankType || 5 == nRankType)
	{
		if (idx < m_vector_rankPlayer.size())
		{
			initCell(cell, idx, nRankType);
		}

		if (m_vector_rankPlayer.size() > 0)
		{
			int nLastIndex = m_vector_rankPlayer.size() - 1;
			int tmp_pageCur = m_vector_rankPlayer.at(nLastIndex)->nPageCur;
			int tmp_pageAll = m_vector_rankPlayer.at(nLastIndex)->nPageAll;

			this->refreshDataTableStatus(tmp_pageCur, tmp_pageAll);
		}
		else
		{
			GameView::getInstance()->showAlertDialog("page error");
		}

		if (idx == m_vector_rankPlayer.size() - 1)
		{
			if (TableHaveBoth == m_nPageStatus || TableHaveNext == m_nPageStatus)
			{
				// 请求下一页的数据
				int nPage = m_nPageCur + 1;
				if (nPage > m_nPageAll)
				{
					//GameView::getInstance()->showAlertDialog("page error");
					return cell;
				}

				//////////////////////////////////////////////// debug时的提示信息
				/*char str_page[10];
				sprintf(str_page, "%d", nPage);
				std::string str_show = str_page;

				GameView::getInstance()->showAlertDialog(str_show);*/
				/////////////////////////////////////////////////////////////////

				RankDataStruct * rankDataStruct = new RankDataStruct();
				rankDataStruct->nType = RankData::instance()->get_rankType();
				rankDataStruct->nWork = RankData::instance()->get_proOrCountry();
				rankDataStruct->nPage = nPage;

				GameMessageProcessor::sharedMsgProcessor()->sendReq(2300, (void *)rankDataStruct);

				delete rankDataStruct;
			}
		}
	}
	else if (4 == nRankType)
	{
		if (idx < m_vector_rankFamily.size())
		{
			initCell(cell, idx, nRankType);
		}

		if (m_vector_rankFamily.size() > 0)
		{
			int nLastIndex = m_vector_rankFamily.size() - 1;
			int tmp_pageCur = m_vector_rankFamily.at(nLastIndex)->nPageCur;
			int tmp_pageAll = m_vector_rankFamily.at(nLastIndex)->nPageAll;

			this->refreshDataTableStatus(tmp_pageCur, tmp_pageAll);
		}
		else
		{
			//GameView::getInstance()->showAlertDialog("page error");
		}
		

		if (idx == m_vector_rankFamily.size() - 1)
		{
			if (TableHaveBoth == m_nPageStatus || TableHaveNext == m_nPageStatus)
			{
				// 请求下一页的数据
				int nPage = m_nPageCur + 1;
				if (nPage > m_nPageAll)
				{
					//GameView::getInstance()->showAlertDialog("page error");
					return cell;
				}

				/////////////////////////////////////////////////////////////////////
				/*char str_page[10];
				sprintf(str_page, "%d", nPage);
				std::string str_show = str_page;

				GameView::getInstance()->showAlertDialog(str_show);*/
				/////////////////////////////////////////////////////////////////////

				GameMessageProcessor::sharedMsgProcessor()->sendReq(2301, (void *)nPage);
			}
		}
	}


	return cell;
}

unsigned int RankDataTableView::numberOfCellsInTableView( CCTableView *table )
{
	int nRankType = RankData::instance()->get_rankType();

	if (0 == nRankType || 1 == nRankType || 2 == nRankType || 3 == nRankType || 5 == nRankType)
	{
		return m_vector_rankPlayer.size();
	}
	else if (4 == nRankType)
	{
		return m_vector_rankFamily.size();
	}

	return 0;

}

void RankDataTableView::clearVectorRankPlayer()
{
	std::vector<RankPlayerStruct *>::iterator iter;
	for (iter = m_vector_rankPlayer.begin(); iter != m_vector_rankPlayer.end(); iter++)
	{
		delete *iter;
	}
	m_vector_rankPlayer.clear();
}

void RankDataTableView::clearVectorRankFamily()
{
	std::vector<RankFamilyStruct *>::iterator iter;
	for (iter = m_vector_rankFamily.begin(); iter != m_vector_rankFamily.end(); iter++)
	{
		delete *iter;
	}
	m_vector_rankFamily.clear();
}



void RankDataTableView::initCell( CCTableViewCell * cell, unsigned int nIndex, int nRankType)
{
	CCScale9Sprite* pHighlightSpr = CCScale9Sprite::create(CCRectMake(5, 5, 1, 1) , "res_ui/mengban_green.png");
	pHighlightSpr->setPreferredSize(CCSize(CCSizeDataCellSize.width - 4, CCSizeDataCellSize.height - 2));
	pHighlightSpr->setAnchorPoint(CCPointZero);
	pHighlightSpr->setPosition(ccp(2,1));
	pHighlightSpr->setTag(TAG_SELECT_ITEM);
	cell->addChild(pHighlightSpr);
	pHighlightSpr->setVisible(false);

	if (this->m_nSelectCellId == nIndex)
	{
		pHighlightSpr->setVisible(true);
	}

	if (0 == nRankType || 1 == nRankType)
	{
		// 底图
		CCScale9Sprite * sprite_frame = CCScale9Sprite::create("res_ui/LV3_dikuang.png");
		sprite_frame->setContentSize(CCSizeMake(CCSizeDataCellSize.width - 4, CCSizeDataCellSize.height - 2));
		sprite_frame->setCapInsets(CCRectMake(9, 15, 1, 1));
		sprite_frame->setAnchorPoint(CCPointZero);
		sprite_frame->setPosition(ccp(2, 1));
		cell->addChild(sprite_frame);

		ccColor3B color = ccc3(0, 0, 0);
		//ccColor3B colorFont = ccc3(47, 93, 13);

		// 排名
		int nRank = m_vector_rankPlayer.at(nIndex)->nRank;
		ccColor3B colorFont = getFontColor(nRank);

		if (nRank > 10)
		{
			char char_rank[20];
			sprintf(char_rank, "%d", nRank);
			CCLabelTTF * m_label_rank = CCLabelTTF::create(char_rank, APP_FONT_NAME, 18, CCSizeMake(150, 0), kCCTextAlignmentLeft);
			m_label_rank->setColor(colorFont);
			//m_label_rank->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
			m_label_rank->setAnchorPoint(ccp(0.5f, 0.5f));
			m_label_rank->setHorizontalAlignment(kCCTextAlignmentCenter);
			m_label_rank->setPosition(ccp(54, sprite_frame->getContentSize().height / 2));
			cell->addChild(m_label_rank);
		}
		else
		{
			char char_rank[20];
			sprintf(char_rank, "%d", nRank);
			
			UILabelBMFont * labelBMFont = UILabelBMFont::create();
			labelBMFont->setFntFile("res_ui/font/ziti_3.fnt");
			labelBMFont->setText(char_rank);
			labelBMFont->setAnchorPoint(ccp(0.5f, 0.5f));
			labelBMFont->setPosition(ccp(56, sprite_frame->getContentSize().height / 2));

			UILayer * tmpLayer = UILayer::create();
			tmpLayer->addWidget(labelBMFont);
			cell->addChild(tmpLayer);

			// 前三名加皇冠标志
			CCSprite * spriteRank = setRankPic(labelBMFont->getPosition().x, labelBMFont->getPosition().y, nRank);
			if (NULL != spriteRank)
			{
				cell->addChild(spriteRank);
			}
		}

		// 名称
		std::string str_name = m_vector_rankPlayer.at(nIndex)->strName;
		CCLabelTTF * m_label_name = CCLabelTTF::create(str_name.c_str(), APP_FONT_NAME, 18);
		m_label_name->setColor(colorFont);
		//m_label_name->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_name->setAnchorPoint(ccp(0.5f, 0.5f));
		m_label_name->setHorizontalAlignment(kCCTextAlignmentCenter);
		m_label_name->setPosition(ccp(156, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_name);

		// vip开关控制
		bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
		if (true == vipEnabled)
		{
			//VIP等级
			CCNode *pNode = MainScene::addVipInfoByLevelForNode(m_vector_rankPlayer.at(nIndex)->nVipLevel);
			//CCNode *pNode = MainScene::addVipInfoByLevelForNode(5);
			if (pNode)
			{
				cell->addChild(pNode);
				pNode->setPosition(ccp(m_label_name->getPosition().x+m_label_name->getContentSize().width/2+13,m_label_name->getPosition().y));
			}
		}

		// 等级 or 战斗力 or 金币
		int nMount = m_vector_rankPlayer.at(nIndex)->nMount;
		char char_mount[20];
		sprintf(char_mount, "%d", nMount);
		CCLabelTTF * m_label_mount = CCLabelTTF::create(char_mount, APP_FONT_NAME, 18, CCSizeMake(150, 0), kCCTextAlignmentLeft);
		m_label_mount->setColor(colorFont);
		//m_label_mount->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_mount->setAnchorPoint(ccp(0.5f, 0.5f));
		m_label_mount->setHorizontalAlignment(kCCTextAlignmentCenter);
		m_label_mount->setPosition(ccp(260, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_mount);

		// 职业
		std::string str_profession = m_vector_rankPlayer.at(nIndex)->strProfession;
		CCLabelTTF * m_label_profession = CCLabelTTF::create(str_profession.c_str(), APP_FONT_NAME, 18, CCSizeMake(150, 0), kCCTextAlignmentLeft);
		m_label_profession->setColor(colorFont);
		//m_label_profession->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_profession->setAnchorPoint(ccp(0.5f, 0.5f));
		m_label_profession->setHorizontalAlignment(kCCTextAlignmentCenter);
		m_label_profession->setPosition(ccp(360, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_profession);

		// 国家
		std::string str_country = m_vector_rankPlayer.at(nIndex)->strCountry;
		CCLabelTTF * m_label_country = CCLabelTTF::create(str_country.c_str(), APP_FONT_NAME, 18, CCSizeMake(150, 0), kCCTextAlignmentLeft);
		m_label_country->setColor(colorFont);
		//m_label_country->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_country->setAnchorPoint(ccp(0.5f, 0.5f));
		m_label_country->setHorizontalAlignment(kCCTextAlignmentCenter);
		m_label_country->setPosition(ccp(457, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_country);
	}
	else if (2 == nRankType)
	{
		// 底图
		CCScale9Sprite * sprite_frame = CCScale9Sprite::create("res_ui/LV3_dikuang.png");
		sprite_frame->setContentSize(CCSizeMake(CCSizeDataCellSize.width - 4, CCSizeDataCellSize.height - 2));
		sprite_frame->setCapInsets(CCRectMake(9, 15, 1, 1));
		sprite_frame->setAnchorPoint(CCPointZero);
		sprite_frame->setPosition(ccp(2, 1));
		cell->addChild(sprite_frame);

		ccColor3B color = ccc3(0, 0, 0);
		//ccColor3B colorFont = ccc3(47, 93, 13);

		// 排名
		int nRank = m_vector_rankPlayer.at(nIndex)->nRank;
		ccColor3B colorFont = getFontColor(nRank);

		if (nRank > 10)
		{
			char char_rank[20];
			sprintf(char_rank, "%d", nRank);
			CCLabelTTF * m_label_rank = CCLabelTTF::create(char_rank, APP_FONT_NAME, 18, CCSizeMake(150, 0), kCCTextAlignmentLeft);
			m_label_rank->setColor(colorFont);
			//m_label_rank->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
			m_label_rank->setAnchorPoint(ccp(0.5f, 0.5f));
			m_label_rank->setHorizontalAlignment(kCCTextAlignmentCenter);
			m_label_rank->setPosition(ccp(54, sprite_frame->getContentSize().height / 2));
			cell->addChild(m_label_rank);
		}
		else
		{
			char char_rank[20];
			sprintf(char_rank, "%d", nRank);

			UILabelBMFont * labelBMFont = UILabelBMFont::create();
			labelBMFont->setFntFile("res_ui/font/ziti_3.fnt");
			labelBMFont->setText(char_rank);
			labelBMFont->setAnchorPoint(ccp(0.5f, 0.5f));
			labelBMFont->setPosition(ccp(56, sprite_frame->getContentSize().height / 2));

			UILayer * tmpLayer = UILayer::create();
			tmpLayer->addWidget(labelBMFont);
			cell->addChild(tmpLayer);

			// 前三名加皇冠标志
			CCSprite * spriteRank = setRankPic(labelBMFont->getPosition().x, labelBMFont->getPosition().y, nRank);
			if (NULL != spriteRank)
			{
				cell->addChild(spriteRank);
			}
		}

		// 名称
		std::string str_name = m_vector_rankPlayer.at(nIndex)->strName;
		CCLabelTTF * m_label_name = CCLabelTTF::create(str_name.c_str(), APP_FONT_NAME, 18);
		m_label_name->setColor(colorFont);
		//m_label_name->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_name->setAnchorPoint(ccp(0.5f, 0.5f));
		m_label_name->setHorizontalAlignment(kCCTextAlignmentCenter);
		m_label_name->setPosition(ccp(156, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_name);

		// vip开关控制
		bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
		if (true == vipEnabled)
		{
			//VIP等级
			CCNode *pNode = MainScene::addVipInfoByLevelForNode(m_vector_rankPlayer.at(nIndex)->nVipLevel);
			//CCNode *pNode = MainScene::addVipInfoByLevelForNode(5);
			if (pNode)
			{
				cell->addChild(pNode);
				pNode->setPosition(ccp(m_label_name->getPosition().x+m_label_name->getContentSize().width/2+13,m_label_name->getPosition().y));
			}
		}

		// 等级 or 战斗力 or 金币
		int nMount = m_vector_rankPlayer.at(nIndex)->nMount;
		char char_mount[20];
		sprintf(char_mount, "%d", nMount);
		CCLabelTTF * m_label_mount = CCLabelTTF::create(char_mount, APP_FONT_NAME, 18, CCSizeMake(150, 0), kCCTextAlignmentLeft);
		m_label_mount->setColor(colorFont);
		//m_label_mount->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_mount->setAnchorPoint(ccp(0.5f, 0.5f));
		m_label_mount->setHorizontalAlignment(kCCTextAlignmentCenter);
		m_label_mount->setPosition(ccp(275, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_mount);

		// 职业
		std::string str_profession = m_vector_rankPlayer.at(nIndex)->strProfession;
		CCLabelTTF * m_label_profession = CCLabelTTF::create(str_profession.c_str(), APP_FONT_NAME, 18, CCSizeMake(150, 0), kCCTextAlignmentLeft);
		m_label_profession->setColor(colorFont);
		//m_label_profession->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_profession->setAnchorPoint(ccp(0.5f, 0.5f));
		m_label_profession->setHorizontalAlignment(kCCTextAlignmentCenter);
		m_label_profession->setPosition(ccp(380, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_profession);

		// 国家
		std::string str_country = m_vector_rankPlayer.at(nIndex)->strCountry;
		CCLabelTTF * m_label_country = CCLabelTTF::create(str_country.c_str(), APP_FONT_NAME, 18, CCSizeMake(150, 0), kCCTextAlignmentLeft);
		m_label_country->setColor(colorFont);
		//m_label_country->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_country->setAnchorPoint(ccp(0.5f, 0.5f));
		m_label_country->setHorizontalAlignment(kCCTextAlignmentCenter);
		m_label_country->setPosition(ccp(464, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_country);
	}
	else if (3 == nRankType)   // 战功（排名 姓名 战功 等级 职业）
	{
		// 底图
		CCScale9Sprite * sprite_frame = CCScale9Sprite::create("res_ui/LV3_dikuang.png");
		sprite_frame->setContentSize(CCSizeMake(CCSizeDataCellSize.width - 4, CCSizeDataCellSize.height - 2));
		sprite_frame->setCapInsets(CCRectMake(9, 15, 1, 1));
		sprite_frame->setAnchorPoint(CCPointZero);
		sprite_frame->setPosition(ccp(2, 1));
		cell->addChild(sprite_frame);

		ccColor3B color = ccc3(0, 0, 0);
		//ccColor3B colorFont = ccc3(47, 93, 13);

		// 排名
		int nRank = m_vector_rankPlayer.at(nIndex)->nRank;
		ccColor3B colorFont = getFontColor(nRank);

		if (nRank > 10)
		{
			char char_rank[20];
			sprintf(char_rank, "%d", nRank);
			CCLabelTTF * m_label_rank = CCLabelTTF::create(char_rank, APP_FONT_NAME, 18, CCSizeMake(150, 0), kCCTextAlignmentLeft);
			m_label_rank->setColor(colorFont);
			//m_label_rank->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
			m_label_rank->setAnchorPoint(ccp(0.5f, 0.5f));
			m_label_rank->setHorizontalAlignment(kCCTextAlignmentCenter);
			m_label_rank->setPosition(ccp(54, sprite_frame->getContentSize().height / 2));
			cell->addChild(m_label_rank);
		}
		else
		{
			char char_rank[20];
			sprintf(char_rank, "%d", nRank);

			UILabelBMFont * labelBMFont = UILabelBMFont::create();
			labelBMFont->setFntFile("res_ui/font/ziti_3.fnt");
			labelBMFont->setText(char_rank);
			labelBMFont->setAnchorPoint(ccp(0.5f, 0.5f));
			labelBMFont->setPosition(ccp(56, sprite_frame->getContentSize().height / 2));

			UILayer * tmpLayer = UILayer::create();
			tmpLayer->addWidget(labelBMFont);
			cell->addChild(tmpLayer);

			// 前三名加皇冠标志
			CCSprite * spriteRank = setRankPic(labelBMFont->getPosition().x, labelBMFont->getPosition().y, nRank);
			if (NULL != spriteRank)
			{
				cell->addChild(spriteRank);
			}
		}

		// 名称(国家+名字+vip)
	
		// 名字
		std::string str_name = m_vector_rankPlayer.at(nIndex)->strName;
		CCLabelTTF * m_label_name = CCLabelTTF::create(str_name.c_str(), APP_FONT_NAME, 18);
		m_label_name->setColor(colorFont);
		//m_label_name->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_name->setAnchorPoint(ccp(0, 0.5f));
		m_label_name->setHorizontalAlignment(kCCTextAlignmentLeft);
		m_label_name->setPosition(ccp(138, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_name);

		// 国家
		int nCountryId = m_vector_rankPlayer.at(nIndex)->nCountry;
		if (nCountryId > 0 && nCountryId < 5)
		{
			std::string strCountryIdName = "country_";
			char charCountryId[20];
			sprintf(charCountryId, "%d", nCountryId);
			strCountryIdName.append(charCountryId);
			std::string strIconPathName = "res_ui/country_icon/";
			strIconPathName.append(strCountryIdName);
			strIconPathName.append(".png");
			CCSprite * spriteCountry = CCSprite::create(strIconPathName.c_str());
			spriteCountry->setAnchorPoint(ccp(1.0f, 0.5f));
			spriteCountry->setPosition(ccp(m_label_name->getPositionX(), sprite_frame->getContentSize().height / 2));
			spriteCountry->setScale(0.7f);
			cell->addChild(spriteCountry);
		}

		// vip开关控制
		bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
		if (true == vipEnabled)
		{
			//VIP等级
			CCNode *pNode = MainScene::addVipInfoByLevelForNode(m_vector_rankPlayer.at(nIndex)->nVipLevel);
			//CCNode *pNode = MainScene::addVipInfoByLevelForNode(5);
			if (pNode)
			{
				cell->addChild(pNode);
				pNode->setAnchorPoint(ccp(0, 0.5f));
				pNode->setPosition(ccp(m_label_name->getPosition().x + m_label_name->getContentSize().width, m_label_name->getPosition().y));
			}
		}

		// 等级 or 战斗力 or 金币 or 战功
		int nMount = m_vector_rankPlayer.at(nIndex)->nMount;
		char char_mount[20];
		sprintf(char_mount, "%d", nMount);
		CCLabelTTF * m_label_mount = CCLabelTTF::create(char_mount, APP_FONT_NAME, 18, CCSizeMake(150, 0), kCCTextAlignmentLeft);
		m_label_mount->setColor(colorFont);
		//m_label_mount->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_mount->setAnchorPoint(ccp(0.5f, 0.5f));
		m_label_mount->setHorizontalAlignment(kCCTextAlignmentCenter);
		m_label_mount->setPosition(ccp(286, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_mount);
		
		// 等级
		int nLevel = m_vector_rankPlayer.at(nIndex)->nLevel;
		char char_level[20];
		sprintf(char_level, "%d", nLevel);
		CCLabelTTF * m_label_level = CCLabelTTF::create(char_level, APP_FONT_NAME, 18, CCSizeMake(150, 0), kCCTextAlignmentLeft);
		m_label_level->setColor(colorFont);
		m_label_level->setAnchorPoint(ccp(0.5f, 0.5f));
		m_label_level->setHorizontalAlignment(kCCTextAlignmentCenter);
		m_label_level->setPosition(ccp(380, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_level);


		// 职业
		std::string str_profession = m_vector_rankPlayer.at(nIndex)->strProfession;
		CCLabelTTF * m_label_profession = CCLabelTTF::create(str_profession.c_str(), APP_FONT_NAME, 18, CCSizeMake(150, 0), kCCTextAlignmentLeft);
		m_label_profession->setColor(colorFont);
		//m_label_profession->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_profession->setAnchorPoint(ccp(0.5f, 0.5f));
		m_label_profession->setHorizontalAlignment(kCCTextAlignmentCenter);
		m_label_profession->setPosition(ccp(465, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_profession);
	}
	else if (4 == nRankType)
	{
		// 底图
		CCScale9Sprite * sprite_frame = CCScale9Sprite::create("res_ui/LV3_dikuang.png");
		sprite_frame->setContentSize(CCSizeMake(CCSizeDataCellSize.width - 4, CCSizeDataCellSize.height - 2));
		sprite_frame->setCapInsets(CCRectMake(9, 15, 1, 1));
		sprite_frame->setAnchorPoint(CCPointZero);
		sprite_frame->setPosition(ccp(2, 1));
		cell->addChild(sprite_frame);

		ccColor3B color = ccc3(0, 0, 0);
		//ccColor3B colorFont = ccc3(47, 93, 13);

		// 排名
		int nRank = m_vector_rankFamily.at(nIndex)->nRank;
		ccColor3B colorFont = getFontColor(nRank);

		if (nRank > 10)
		{
			char char_rank[20];
			sprintf(char_rank, "%d", nRank);
			CCLabelTTF * m_label_rank = CCLabelTTF::create(char_rank, APP_FONT_NAME, 18, CCSizeMake(150, 0), kCCTextAlignmentLeft);
			m_label_rank->setColor(colorFont);
			//m_label_rank->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
			m_label_rank->setAnchorPoint(ccp(0.5f, 0.5f));
			m_label_rank->setHorizontalAlignment(kCCTextAlignmentCenter);
			m_label_rank->setPosition(ccp(54, sprite_frame->getContentSize().height / 2));
			cell->addChild(m_label_rank);
		}
		else
		{
			char char_rank[20];
			sprintf(char_rank, "%d", nRank);

			UILabelBMFont * labelBMFont = UILabelBMFont::create();
			labelBMFont->setFntFile("res_ui/font/ziti_3.fnt");
			labelBMFont->setText(char_rank);
			labelBMFont->setAnchorPoint(ccp(0.5f, 0.5f));
			labelBMFont->setPosition(ccp(56, sprite_frame->getContentSize().height / 2));			// 上面X坐标为54，此处为56，原因在于一个可设置对齐方式，一个不可以

			UILayer * tmpLayer = UILayer::create();
			tmpLayer->addWidget(labelBMFont);
			cell->addChild(tmpLayer);

			// 前三名加皇冠标志
			CCSprite * spriteRank = setRankPic(labelBMFont->getPosition().x, labelBMFont->getPosition().y, nRank);
			if (NULL != spriteRank)
			{
				cell->addChild(spriteRank);
			}
		}

		// 家族名称
		std::string str_familyName = m_vector_rankFamily.at(nIndex)->strFamilyName;
		CCLabelTTF * m_label_familyName = CCLabelTTF::create(str_familyName.c_str(), APP_FONT_NAME, 18, CCSizeMake(150, 0), kCCTextAlignmentLeft);
		m_label_familyName->setColor(colorFont);
		//m_label_familyName->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_familyName->setAnchorPoint(ccp(0.5f, 0.5f));
		m_label_familyName->setHorizontalAlignment(kCCTextAlignmentCenter);
		m_label_familyName->setPosition(ccp(156, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_familyName);

		// 等级
		int nLevel = m_vector_rankFamily.at(nIndex)->nLevel;
		char char_level[20];
		sprintf(char_level, "%d", nLevel);
		CCLabelTTF * m_label_level = CCLabelTTF::create(char_level, APP_FONT_NAME, 18, CCSizeMake(150, 0), kCCTextAlignmentLeft);
		m_label_level->setColor(colorFont);
		//m_label_level->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_level->setAnchorPoint(ccp(0.5f, 0.5f));
		m_label_level->setHorizontalAlignment(kCCTextAlignmentCenter);
		m_label_level->setPosition(ccp(242, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_level);

		// 人数
		int nFamilyMember = m_vector_rankFamily.at(nIndex)->nFamilyMember;
		char char_familyMember[20];
		sprintf(char_familyMember, "%d", nFamilyMember);
		int nFamilyMemberMax = m_vector_rankFamily.at(nIndex)->nFamilyMemberMax;
		char char_familyMemberMax[20];
		sprintf(char_familyMemberMax, "%d", nFamilyMemberMax);

		std::string str_familyNum = "";
		str_familyNum.append(char_familyMember);
		str_familyNum.append("/");
		str_familyNum.append(char_familyMemberMax);

		CCLabelTTF * m_label_familyNum = CCLabelTTF::create(str_familyNum.c_str(), APP_FONT_NAME, 18, CCSizeMake(150, 0), kCCTextAlignmentLeft);
		m_label_familyNum->setColor(colorFont);
		//m_label_playerNum->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_familyNum->setAnchorPoint(ccp(0.5f, 0.5f));
		m_label_familyNum->setHorizontalAlignment(kCCTextAlignmentCenter);
		m_label_familyNum->setPosition(ccp(315, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_familyNum);

		// 国家
		std::string str_country = m_vector_rankFamily.at(nIndex)->strCountry;
		CCLabelTTF * m_label_country = CCLabelTTF::create(str_country.c_str(), APP_FONT_NAME, 18, CCSizeMake(150, 0), kCCTextAlignmentLeft);
		m_label_country->setColor(colorFont);
		//m_label_country->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_country->setAnchorPoint(ccp(0.5f, 0.5f));
		m_label_country->setHorizontalAlignment(kCCTextAlignmentCenter);
		m_label_country->setPosition(ccp(390, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_country);

		// 查看按钮
		CCScale9Sprite * sprite_btn = CCScale9Sprite::create("res_ui/new_button_2.png");
		sprite_btn->setCapInsets(CCRectMake(18, 9, 2, 23));
		sprite_btn->setContentSize(CCSizeMake(83, 38));

		RankUI* pRankUI = (RankUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRankUI);
		if (NULL != pRankUI)
		{
			CCMenuItemSprite *item = CCMenuItemSprite::create(sprite_btn, sprite_btn, pRankUI, menu_selector(RankUI::callBackBtnFamilyInfo));
			item->setZoomScale(1.2f);
			item->setTag(nRank);			// 以 家族排名 作为tag

			CCMoveableMenu * pSelectMenu = CCMoveableMenu::create(item, NULL);
			pSelectMenu->setAnchorPoint(ccp(0, 0));
			pSelectMenu->setPosition(ccp(470, sprite_frame->getContentSize().height / 2));

			// 查看的text
			const char * char_chakan = StringDataManager::getString("rank_chankan");
			char * str_chakan = const_cast<char *>(char_chakan);
			CCLabelTTF * lable_btnText = CCLabelTTF::create(str_chakan, APP_FONT_NAME, 18, CCSizeMake(200, 0), kCCTextAlignmentLeft);
			lable_btnText->setColor(ccc3(255, 255, 255));
			//lable_btnText->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
			lable_btnText->setAnchorPoint(ccp(0, 0));
			lable_btnText->setPosition(ccp((lable_btnText->getContentSize().width - sprite_btn->getContentSize().width) / 2 - 34, (sprite_btn->getContentSize().height - lable_btnText->getContentSize().height) / 2));
			item->addChild(lable_btnText);

			cell->addChild(pSelectMenu);
		}
	}
	else if (5 == nRankType)
	{
		// 底图
		CCScale9Sprite * sprite_frame = CCScale9Sprite::create("res_ui/LV3_dikuang.png");
		sprite_frame->setContentSize(CCSizeMake(CCSizeDataCellSize.width - 4, CCSizeDataCellSize.height - 2));
		sprite_frame->setCapInsets(CCRectMake(9, 15, 1, 1));
		sprite_frame->setAnchorPoint(CCPointZero);
		sprite_frame->setPosition(ccp(2, 1));
		cell->addChild(sprite_frame);

		ccColor3B color = ccc3(0, 0, 0);
		//ccColor3B colorFont = ccc3(47, 93, 13);

		// 排名
		int nRank = m_vector_rankPlayer.at(nIndex)->nRank;
		ccColor3B colorFont = getFontColor(nRank);

		if (nRank > 10)
		{
			char char_rank[20];
			sprintf(char_rank, "%d", nRank);
			CCLabelTTF * m_label_rank = CCLabelTTF::create(char_rank, APP_FONT_NAME, 18, CCSizeMake(150, 0), kCCTextAlignmentLeft);
			m_label_rank->setColor(colorFont);
			//m_label_rank->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
			m_label_rank->setAnchorPoint(ccp(0.5f, 0.5f));
			m_label_rank->setHorizontalAlignment(kCCTextAlignmentCenter);
			m_label_rank->setPosition(ccp(54, sprite_frame->getContentSize().height / 2));
			cell->addChild(m_label_rank);
		}
		else
		{
			char char_rank[20];
			sprintf(char_rank, "%d", nRank);

			UILabelBMFont * labelBMFont = UILabelBMFont::create();
			labelBMFont->setFntFile("res_ui/font/ziti_3.fnt");
			labelBMFont->setText(char_rank);
			labelBMFont->setAnchorPoint(ccp(0.5f, 0.5f));
			labelBMFont->setPosition(ccp(56, sprite_frame->getContentSize().height / 2));

			UILayer * tmpLayer = UILayer::create();
			tmpLayer->addWidget(labelBMFont);
			cell->addChild(tmpLayer);

			// 前三名加皇冠标志
			CCSprite * spriteRank = setRankPic(labelBMFont->getPosition().x, labelBMFont->getPosition().y, nRank);
			if (NULL != spriteRank)
			{
				cell->addChild(spriteRank);
			}
		}

		// 名称
		std::string str_name = m_vector_rankPlayer.at(nIndex)->strName;
		CCLabelTTF * m_label_name = CCLabelTTF::create(str_name.c_str(), APP_FONT_NAME, 18);
		m_label_name->setColor(colorFont);
		//m_label_name->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_name->setAnchorPoint(ccp(0.5f, 0.5f));
		m_label_name->setHorizontalAlignment(kCCTextAlignmentCenter);
		m_label_name->setPosition(ccp(166, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_name);

		// vip开关控制
		bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
		if (true == vipEnabled)
		{
			//VIP等级
			CCNode *pNode = MainScene::addVipInfoByLevelForNode(m_vector_rankPlayer.at(nIndex)->nVipLevel);
			//CCNode *pNode = MainScene::addVipInfoByLevelForNode(5);
			if (pNode)
			{
				cell->addChild(pNode);
				pNode->setPosition(ccp(m_label_name->getPosition().x+m_label_name->getContentSize().width/2+13,m_label_name->getPosition().y));
			}
		}

		// 鲜花数
		int nMount = m_vector_rankPlayer.at(nIndex)->nMount;
		char char_mount[20];
		sprintf(char_mount, "%d", nMount);
		CCLabelTTF * m_label_mount = CCLabelTTF::create(char_mount, APP_FONT_NAME, 18, CCSizeMake(150, 0), kCCTextAlignmentLeft);
		m_label_mount->setColor(colorFont);
		//m_label_mount->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_mount->setAnchorPoint(ccp(0.5f, 0.5f));
		m_label_mount->setHorizontalAlignment(kCCTextAlignmentCenter);
		m_label_mount->setPosition(ccp(293, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_mount);

		// 职业
		//std::string str_profession = m_vector_rankPlayer.at(nIndex)->strProfession;
		//CCLabelTTF * m_label_profession = CCLabelTTF::create(str_profession.c_str(), APP_FONT_NAME, 18, CCSizeMake(150, 0), kCCTextAlignmentLeft);
		//m_label_profession->setColor(colorFont);
		////m_label_profession->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
		//m_label_profession->setAnchorPoint(ccp(0.5f, 0.5f));
		//m_label_profession->setHorizontalAlignment(kCCTextAlignmentCenter);
		//m_label_profession->setPosition(ccp(360, sprite_frame->getContentSize().height / 2));
		//cell->addChild(m_label_profession);

		// 国家
		std::string str_country = m_vector_rankPlayer.at(nIndex)->strCountry;
		CCLabelTTF * m_label_country = CCLabelTTF::create(str_country.c_str(), APP_FONT_NAME, 18, CCSizeMake(150, 0), kCCTextAlignmentLeft);
		m_label_country->setColor(colorFont);
		//m_label_country->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_country->setAnchorPoint(ccp(0.5f, 0.5f));
		m_label_country->setHorizontalAlignment(kCCTextAlignmentCenter);
		m_label_country->setPosition(ccp(422, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_country);
	}
}

void RankDataTableView::set_pageCur( int nPageCur )
{
	this->m_nPageCur = nPageCur;
}

int RankDataTableView::get_pageCur()
{
	return this->m_nPageCur;
}

void RankDataTableView::set_pageAll( int nPageAll )
{
	this->m_nPageAll = nPageAll;
}

int RankDataTableView::get_pageAll()
{
	return this->m_nPageAll;
}

void RankDataTableView::refreshDataTableStatus( int nPageCur, int nPageAll )
{
	m_nPageCur = nPageCur;
	m_nPageAll = nPageAll;
	if (0 == nPageCur)
	{
		if (nPageCur < nPageAll - 1)
		{
			this->m_nPageStatus = TableHaveNext;
		}
		else
		{
			this->m_nPageStatus = TableHaveNone;
		}
	}
	else
	{
		if (nPageCur < nPageAll - 1)
		{
			this->m_nPageStatus = TableHaveBoth;
		}
		else
		{
			this->m_nPageStatus = TableHaveLast;
		}
	}
}

cocos2d::ccColor3B RankDataTableView::getFontColor( int nRank )
{
	// 默认
	ccColor3B colorFont = ccc3(47, 93, 13);
	if (1 == nRank)
	{
		colorFont = ccc3(155, 84, 0);
	}
	else if (2 == nRank)
	{
		colorFont = ccc3(169, 2, 155);
	}
	else if(3 == nRank)
	{
		colorFont = ccc3(0, 64, 227);
	}

	return colorFont;
}

CCSprite* RankDataTableView::setRankPic( float positionX, float positionY, int nRank)
{
	CCSprite* spriteRank = NULL; 

	if (1 == nRank)
	{
		spriteRank = CCSprite::create("res_ui/rank1.png");
	}
	else if (2 == nRank)
	{
		spriteRank = CCSprite::create("res_ui/rank2.png");
	}
	else if (3 == nRank)
	{
		spriteRank = CCSprite::create("res_ui/rank3.png");
	}

	if (NULL != spriteRank)
	{
		spriteRank->setAnchorPoint(ccp(0.5f, 0.5f));
		spriteRank->ignoreAnchorPointForPosition(false);
		spriteRank->setPosition(ccp(positionX - 2, positionY));
	}

	return spriteRank;
}

void RankDataTableView::setSelectItemDefault()
{
	this->m_nLastSelectCellId = 0;
	this->m_nSelectCellId = 0;
}





