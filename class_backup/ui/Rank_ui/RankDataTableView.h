#ifndef _UI_RANK_RANKDATATABLEVIEW_H_
#define _UI_RANK_RANKDATATABLEVIEW_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

/////////////////////////////////
/**
 * 排行榜模块（tableView）
 * @author liuliang
 * @version 0.1.0
 * @date 2014.04.09
 */
struct RankPlayerStruct;
struct RankFamilyStruct;

class RankDataTableView:public CCTableViewDataSource, public CCTableViewDelegate
{
public:
	static RankDataTableView * s_rankDataTableView;
	static RankDataTableView * instance();

private:
	RankDataTableView(void);
	~RankDataTableView(void);

public:
	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);

public:
	std::vector<RankPlayerStruct *> m_vector_rankPlayer;
	std::vector<RankFamilyStruct *> m_vector_rankFamily;

	void clearVectorRankPlayer();
	void clearVectorRankFamily();

	void refreshDataTableStatus( int nPageCur, int nPageAll);
	ccColor3B getFontColor(int nRank);

	void setSelectItemDefault();

private:
	void initCell(CCTableViewCell * cell, unsigned int nIndex, int nRankType);

	int m_nPageCur;
	int m_nPageAll;
	int m_nPageStatus;

	int m_nLastSelectCellId;
	int m_nSelectCellId;

public:
	void set_pageCur(int nPageCur);
	int get_pageCur();

	void set_pageAll(int nPageAll);
	int get_pageAll();

	// dataTable是否有上一页或下一页
	enum DataTableStatus
	{
		TableHaveLast = 0,					// 上一页
		TableHaveNext,
		TableHaveBoth,
		TableHaveNone,
	};

private:
	CCSprite* setRankPic(float positionX, float positionY, int nRank);
};

#endif

