#ifndef _NPCSCENE_NPCTALKWINDOW_H_
#define _NPCSCENE_NPCTALKWINDOW_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "../../legend_script/CCTutorialIndicator.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CNpcDialog;

class NpcTalkWindow : public UIScene, public CCTableViewDataSource, public CCTableViewDelegate
{
public:
	NpcTalkWindow();
	~NpcTalkWindow();

	virtual void update(float dt);
	static NpcTalkWindow* create(CNpcDialog * npcDialog);
	bool init(CNpcDialog * npcDialog);

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);


	void CloseEvent(CCObject * pSender);

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);

	bool isHasMission(int npcId);

	int getLockedNpcId();

	//教学
	int mTutorialScriptInstanceId;
	int mTutorialIndex;
	//教学
	virtual void registerScriptCommand(int scriptId);
	
	void addCCTutorialIndicatorNpc(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction,int tutorialIndex);
	void removeCCTutorialIndicator();

private:
	CNpcDialog * curNpcDialog;
	CCSize winsize;
	int m_nLockedId;
};
#endif;
