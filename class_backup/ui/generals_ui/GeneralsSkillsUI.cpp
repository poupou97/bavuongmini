#include "GeneralsSkillsUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "GeneralsUI.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "../../messageclient/element/CBaseSkill.h"
#include "../../GameView.h"
#include "TeachAndEvolutionCell.h"
#include "NormalGeneralsHeadItem.h"
#include "../../messageclient/element/CFightWayBase.h"
#include "../extensions/UITab.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/skill/GameFightSkill.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "../../gamescene_state/role/GameActor.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../utils/StaticDataManager.h"
#include "AppMacros.h"
#include "GeneralsShortcutSlot.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../ui/skillscene/SkillScene.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutUIConstant.h"
#include "../../ui/extensions/ShowSystemInfo.h"

#define  GENERALSSKILLSUI_TABALEVIEW_HIGHLIGHT_TAG 456
#define  GENERALSSHORTCUTSLOT_TAG 567
#define  SCROLLVIEW_SKILLDESINFO_TAG 678

bool SortGeneralCellData(GeneralSkillCellData * cellData1,GeneralSkillCellData * cellData2)
{
	if (cellData2->curRealLevel < cellData1->curRealLevel)
	{
		return true;
	}
	else if (cellData2->curRealLevel == cellData1->curRealLevel)
	{
		if (cellData1->curGameFightSkill->getCBaseSkill()->usemodel() < cellData2->curGameFightSkill->getCBaseSkill()->usemodel())
		{
			return true;
		}
		else if (cellData1->curGameFightSkill->getCBaseSkill()->usemodel() == cellData2->curGameFightSkill->getCBaseSkill()->usemodel())
		{
			if (cellData1->curGameFightSkill->getCBaseSkill()->get_quality() < cellData2->curGameFightSkill->getCBaseSkill()->get_quality())      
			{
				return true;
			}
			else if (cellData1->curGameFightSkill->getCBaseSkill()->get_quality() < cellData2->curGameFightSkill->getCBaseSkill()->get_quality())
			{
				return cellData1->curGameFightSkill->getCBaseSkill()->get_complex_order() < cellData2->curGameFightSkill->getCBaseSkill()->get_complex_order();
			}
		}
	}
	return false;
}

GeneralsSkillsUI::GeneralsSkillsUI() :
skillType(Warrior),
curSkillId(""),
selectCellId(0),
lastSelectCellId(0)
{
}

GeneralsSkillsUI::~GeneralsSkillsUI()
{
	std::vector<GeneralSkillCellData*>::iterator _iter;
	for (_iter = DataSourceList.begin(); _iter != DataSourceList.end(); ++_iter)
	{
		delete *_iter;
	}
	DataSourceList.clear();
}
GeneralsSkillsUI* GeneralsSkillsUI::create()
{
	GeneralsSkillsUI * generalsSkillsUI = new GeneralsSkillsUI();
	if (generalsSkillsUI && generalsSkillsUI->init())
	{
		generalsSkillsUI->autorelease();
		return generalsSkillsUI;
	}
	CC_SAFE_DELETE(generalsSkillsUI);
	return NULL;
}

bool GeneralsSkillsUI::init()
{
	if (UIScene::init())
	{
		panel_skills = (UIPanel*)UIHelper::seekWidgetByName(GeneralsUI::ppanel,"Panel_skill");
		panel_skills->setVisible(true);

		Layer_skills = UILayer::create();
		addChild(Layer_skills);

		UIButton* btn_null_skillUpLevel = (UIButton *)UIHelper::seekWidgetByName(panel_skills,"Button_null_slillUpLevel");
		btn_null_skillUpLevel->setTouchEnable(true);
		btn_null_skillUpLevel->addReleaseEvent(this,coco_releaseselector(GeneralsSkillsUI::NullSkillUpEvent));

		btn_levelUp = (UIButton *)UIHelper::seekWidgetByName(panel_skills,"Button_skillUpgrade");
		btn_levelUp->setTouchEnable(true);
		btn_levelUp->setPressedActionEnabled(true);
		btn_levelUp->addReleaseEvent(this,coco_releaseselector(GeneralsSkillsUI::LeveUpEvent));

		l_levelUp = (UILabel *)UIHelper::seekWidgetByName(panel_skills,"Label_skillUpgrade");

	    i_skillFrame = (UIImageView*)UIHelper::seekWidgetByName(panel_skills,"ImageView_skill_di"); 
		i_skillImage = (UIImageView*)UIHelper::seekWidgetByName(panel_skills,"ImageView_skill"); 
		l_skillName = (UILabel*)UIHelper::seekWidgetByName(panel_skills,"Label_SkillName"); 
		l_skillType = (UILabel*)UIHelper::seekWidgetByName(panel_skills,"Label_SkillType"); 
		l_skillLvValue = (UILabel*)UIHelper::seekWidgetByName(panel_skills,"Label_LvValue"); 
		//l_skillQualityValue = (UILabel*)UIHelper::seekWidgetByName(panel_skills,"Label_QualityValue");
		imageView_skillQuality = (UIImageView*)UIHelper::seekWidgetByName(panel_skills,"ImageView_abc");
		l_skillMpValue = (UILabel*)UIHelper::seekWidgetByName(panel_skills,"Label_MpValue"); 
		l_skillCDTimeValue = (UILabel*)UIHelper::seekWidgetByName(panel_skills,"Label_CDTimeValue"); 
		l_skillDistanceValue = (UILabel*)UIHelper::seekWidgetByName(panel_skills,"Label_DistanceValue"); 
		
		const char * normalImage = "res_ui/tab_1_off.png";
		const char * selectImage = "res_ui/tab_1_on.png";
		const char * finalImage = "";
		const char * highLightImage = "res_ui/tab_1_on.png";

		//CCDictionary *strings = CCDictionary::createWithContentsOfFile("res_ui/font/strings.xml");
		//const char *str1  = ((CCString*)strings->objectForKey("profession_mengjiang"))->m_sString.c_str();
		const char *str1 = StringDataManager::getString("profession_mengjiang");
		char* p1 =const_cast<char*>(str1);
		//const char *str2  = ((CCString*)strings->objectForKey("profession_guimou"))->m_sString.c_str();
		const char *str2 = StringDataManager::getString("profession_guimou");
		char* p2 =const_cast<char*>(str2);
		//const char *str3  = ((CCString*)strings->objectForKey("profession_haojie"))->m_sString.c_str();
		const char *str3 = StringDataManager::getString("profession_haojie");
		char* p3 =const_cast<char*>(str3);
		//const char *str4  = ((CCString*)strings->objectForKey("profession_shenshe"))->m_sString.c_str();
		const char *str4 = StringDataManager::getString("profession_shenshe");
		char* p4 =const_cast<char*>(str4);

		char * mainNames[] ={p1,p2,p3,p4};
		m_mainTab  = UITab::createWithText(4,normalImage,selectImage,finalImage,mainNames,HORIZONTAL,5);
		m_mainTab->setAnchorPoint(ccp(0,0));
		m_mainTab->setPosition(ccp(65,412));
		m_mainTab->setHighLightImage((char * )highLightImage);
		m_mainTab->setDefaultPanelByIndex(0);
		m_mainTab->addIndexChangedEvent(this,coco_indexchangedselector(GeneralsSkillsUI::MainIndexChangedEvent));
		m_mainTab->setPressedActionEnabled(true);
		Layer_skills->addWidget(m_mainTab);

		generalsSkillsList_tableView = CCTableView::create(this,CCSizeMake(256,360));
// 		generalsSkillsList_tableView ->setSelectedEnable(true);   // 支持选中状态的显示
// 		generalsSkillsList_tableView ->setSelectedScale9Texture("res_ui/highlight.png", CCRectMake(26, 26, 1, 1), ccp(0,-1)); 
		generalsSkillsList_tableView->setPressedActionEnabled(true);
		generalsSkillsList_tableView->setDirection(kCCScrollViewDirectionVertical);
		generalsSkillsList_tableView->setAnchorPoint(ccp(0,0));
		generalsSkillsList_tableView->setPosition(ccp(72,41));
		generalsSkillsList_tableView->setDelegate(this);
		generalsSkillsList_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		Layer_skills->addChild(generalsSkillsList_tableView);

		Layer_upper = UILayer::create();
		addChild(Layer_upper);
// 
// 		UIImageView * tableView_kuang = UIImageView::create();
// 		tableView_kuang->setTexture("res_ui/LV5_dikuang_miaobian1.png");
// 		tableView_kuang->setScale9Enable(true);
// 		tableView_kuang->setScale9Size(CCSizeMake(243,374));
// 		tableView_kuang->setCapInsets(CCRectMake(30,30,1,1));
// 		tableView_kuang->setAnchorPoint(ccp(0,0));
// 		tableView_kuang->setPosition(ccp(64,35));
// 		Layer_upper->addWidget(tableView_kuang);

		return true;
	}
	return false;
}

void GeneralsSkillsUI::onEnter()
{
	UIScene::onEnter();
}

void GeneralsSkillsUI::onExit()
{
	UIScene::onExit();
}

void GeneralsSkillsUI::scrollViewDidScroll( CCScrollView* view )
{
}

void GeneralsSkillsUI::scrollViewDidZoom( CCScrollView* view )
{
}

void GeneralsSkillsUI::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{
	int i = cell->getIdx();
	curSkillId = DataSourceList.at(i)->curGameFightSkill->getId();

 	selectCellId = cell->getIdx();
// 	CCSize _s = table->getContentOffset();
// 	//取出上次选中状态，更新本次选中状态
// 	//table->reloadData();
 	if(table->cellAtIndex(lastSelectCellId))
 	{
 		if (table->cellAtIndex(lastSelectCellId)->getChildByTag(GENERALSSKILLSUI_TABALEVIEW_HIGHLIGHT_TAG))
 		{
 			table->cellAtIndex(lastSelectCellId)->getChildByTag(GENERALSSKILLSUI_TABALEVIEW_HIGHLIGHT_TAG)->removeFromParent();
 		}
 	}
 	CCScale9Sprite* pHighlightSpr = CCScale9Sprite::create(CCRectMake(26, 26, 1, 1) , "res_ui/highlight.png");
 	pHighlightSpr->setPreferredSize(CCSize(225,66));
 	pHighlightSpr->setAnchorPoint(CCPointZero);
 	pHighlightSpr->setTag(GENERALSSKILLSUI_TABALEVIEW_HIGHLIGHT_TAG);
 	cell->addChild(pHighlightSpr);
 
 	lastSelectCellId = cell->getIdx();
 
// 	table->setContentOffset(_s); 

	RefreshSkillInfo(DataSourceList.at(i)->curGameFightSkill);
	RefreshBtnStatus(DataSourceList.at(i)->curRealLevel);
}

cocos2d::CCSize GeneralsSkillsUI::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeMake(225,65);
}

cocos2d::extension::CCTableViewCell* GeneralsSkillsUI::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called
	if (!cell)
	{
		cell = GeneralSkillCell::create(DataSourceList.at(idx)); 
		if (this->selectCellId == idx)
		{
			GeneralSkillCell * tempCell = dynamic_cast<GeneralSkillCell*>(cell);
			if (tempCell)
			{
				tempCell->setSelectSelf(true);
			}			
		}
	}
	else
	{
		GeneralSkillCell * tempCell = dynamic_cast<GeneralSkillCell*>(cell);
		if (tempCell->getChildByTag(GENERALSSKILLSUI_TABALEVIEW_HIGHLIGHT_TAG))
		{
			tempCell->getChildByTag(GENERALSSKILLSUI_TABALEVIEW_HIGHLIGHT_TAG)->removeFromParent();
		}
		tempCell->RefreshCell(DataSourceList.at(idx));
		if (this->selectCellId == idx)
		{
			tempCell->setSelectSelf(true);		
		}
	}
	return cell;
}

unsigned int GeneralsSkillsUI::numberOfCellsInTableView( CCTableView *table )
{
	return DataSourceList.size();
}


void GeneralsSkillsUI::setVisible( bool visible )
{
	panel_skills->setVisible(visible);
	Layer_skills->setVisible(visible);
	Layer_upper->setVisible(visible);
}

void GeneralsSkillsUI::MainIndexChangedEvent( CCObject *pSender )
{
	switch((int)m_mainTab->getCurrentIndex())
	{
	case 0 :
		{
			skillType = Warrior;
		}
		break;   
	case 1 :
		{
			skillType = Magic;
		}
		break;
	case 2 :
		{
			skillType = Ranger;
		}
		break;
	case 3 :
		{
			skillType = Warlock;
		}
		break;
	}

	UpdateDataByType(skillType);
	sort(DataSourceList.begin(),DataSourceList.end(),SortGeneralCellData);
	this->generalsSkillsList_tableView->reloadData();
	AutoSetOffSet();
	if (DataSourceList.size() > 0)
	{
		curSkillId = DataSourceList.at(0)->curGameFightSkill->getId();
		this->generalsSkillsList_tableView->selectCell(0);
		RefreshSkillInfo(DataSourceList.at(0)->curGameFightSkill);
		RefreshBtnStatus(DataSourceList.at(0)->curRealLevel);
	}

}

void GeneralsSkillsUI::UpdateDataByType( SkillTpye s_t )
{
	std::vector<GeneralSkillCellData*>::iterator iter;
	for (iter = DataSourceList.begin(); iter != DataSourceList.end(); ++iter)
	{
		delete *iter;
	}
	DataSourceList.clear();

	std::map<std::string, CBaseSkill*>& baseSkillData = StaticDataBaseSkill::s_baseSkillData;
	for (std::map<std::string, CBaseSkill*>::iterator it = baseSkillData.begin(); it != baseSkillData.end(); ++it) 
	{
		CBaseSkill* tempBaseSkill = it->second;
		if (tempBaseSkill->get_quality()>0 && tempBaseSkill->get_profession() == s_t)
		{
			GeneralSkillCellData * cellData = new GeneralSkillCellData();
			cellData->curGameFightSkill->initSkill(tempBaseSkill->id(),tempBaseSkill->id(), 1, GameActor::type_pet);
			cellData->curRealLevel = 0;
			DataSourceList.push_back(cellData);
		}
	}

	std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GeneralsGameFightSkillList.begin();
	for ( ; it != GameView::getInstance()->GeneralsGameFightSkillList.end(); ++ it )
	{
		for (int i = 0;i<DataSourceList.size();++i)
		{
			GeneralSkillCellData * cellData = DataSourceList.at(i);
			if (strcmp(cellData->curGameFightSkill->getId().c_str(),it->second->getId().c_str()) == 0)
			{
				cellData->curGameFightSkill->initSkill(it->second->getId(),it->second->getId(), it->second->getLevel(), GameActor::type_pet);
				cellData->curRealLevel =  it->second->getLevel();
			}
		}
	}

	/*
	GeneralsUI * generalsui = (GeneralsUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsUI);
	
	if (s_t == Warrior)
	{
		std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GeneralsGameFightSkillList.begin();
		for ( ; it != GameView::getInstance()->GeneralsGameFightSkillList.end(); ++ it )
		{
			GameFightSkill * gameFightSkill = new GameFightSkill();
			gameFightSkill->initSkill(it->second->getId(), it->second->getId(),it->second->getLevel(), GameActor::type_player);
			
			if(gameFightSkill->getCBaseSkill()->usemodel() == 2)//被动技能
			{
				if (gameFightSkill->getCBaseSkill()->get_profession() == 1)//猛将
				{
					DataSourceList.push_back(gameFightSkill);
				}
			}
			else
			{
				if (gameFightSkill->getCBaseSkill()->get_profession() == 1)//猛将
				{
					DataSourceList.push_back(gameFightSkill);
				}
			}
		}
	}
	else if(s_t == Magic)
	{
		std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GeneralsGameFightSkillList.begin();
		for ( ; it != GameView::getInstance()->GeneralsGameFightSkillList.end(); ++ it )
		{
			GameFightSkill * gameFightSkill = new GameFightSkill();
			gameFightSkill->initSkill(it->second->getId(), it->second->getId(),it->second->getLevel(), GameActor::type_player);

			if(gameFightSkill->getCBaseSkill()->usemodel() == 2)//被动技能
			{
				if (gameFightSkill->getCBaseSkill()->get_profession() == 2)//鬼谋
				{
					DataSourceList.push_back(gameFightSkill);
				}
			}
			else
			{
				if (gameFightSkill->getCBaseSkill()->get_profession() == 2)//鬼谋
				{
					DataSourceList.push_back(gameFightSkill);
				}
			}
		}
	}
	else if (s_t == Ranger)
	{
		std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GeneralsGameFightSkillList.begin();
		for ( ; it != GameView::getInstance()->GeneralsGameFightSkillList.end(); ++ it )
		{
			GameFightSkill * gameFightSkill = new GameFightSkill();
			gameFightSkill->initSkill(it->second->getId(),it->second->getId(), it->second->getLevel(), GameActor::type_player);

			if(gameFightSkill->getCBaseSkill()->usemodel() == 2)//被动技能
			{
				if (gameFightSkill->getCBaseSkill()->get_profession() == 3)//豪杰
				{
					DataSourceList.push_back(gameFightSkill);
				}
			}
			else
			{
				if (gameFightSkill->getCBaseSkill()->get_profession() == 3)//豪杰
				{
					DataSourceList.push_back(gameFightSkill);
				}
			}
		}
	}
	else if(s_t == Warlock)
	{
		std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GeneralsGameFightSkillList.begin();
		for ( ; it != GameView::getInstance()->GeneralsGameFightSkillList.end(); ++ it )
		{
			GameFightSkill * gameFightSkill = new GameFightSkill();
			gameFightSkill->initSkill(it->second->getId(), it->second->getId(),it->second->getLevel(), GameActor::type_player);

			if(gameFightSkill->getCBaseSkill()->usemodel() == 2)//被动技能
			{
				if (gameFightSkill->getCBaseSkill()->get_profession() == 4)//神射
				{
					DataSourceList.push_back(gameFightSkill);
				}
			}
			else
			{
				if (gameFightSkill->getCBaseSkill()->get_profession() == 4)//神射
				{
					DataSourceList.push_back(gameFightSkill);
				}
			}
		}
	}
	*/
}

void GeneralsSkillsUI::LeveUpEvent( CCObject *pSender )
{
	if (strcmp(curSkillId.c_str(),"") == 0)
	{
		//CCDictionary *strings = CCDictionary::createWithContentsOfFile("res_ui/font/strings.xml");
		//const char *str1  = ((CCString*)strings->objectForKey("skillui_peleaseselcetskill"))->m_sString.c_str();
		const char *str1 = StringDataManager::getString("skillui_peleaseselcetskill");
		char* p1 =const_cast<char*>(str1);
		GameView::getInstance()->showAlertDialog(p1);
	}
	else
	{
		const char * _idx = curSkillId.c_str();
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5054,(void *)_idx);
	}
}

void GeneralsSkillsUI::setToDefault()
{	
	skillType = Warrior;
	UpdateDataByType(skillType);
	sort(DataSourceList.begin(),DataSourceList.end(),SortGeneralCellData);
	m_mainTab->setDefaultPanelByIndex(0);
	this->generalsSkillsList_tableView->reloadData();
	AutoSetOffSet();
	if (DataSourceList.size() > 0)
	{
		curSkillId = DataSourceList.at(0)->curGameFightSkill->getId();
		this->generalsSkillsList_tableView->selectCell(0);
		RefreshSkillInfo(DataSourceList.at(0)->curGameFightSkill);
		RefreshBtnStatus(DataSourceList.at(0)->curRealLevel);
	}

}

void GeneralsSkillsUI::RefreshData()
{
	UpdateDataByType(skillType);
	sort(DataSourceList.begin(),DataSourceList.end(),SortGeneralCellData);

// 	CCSize _s = this->generalsSkillsList_tableView->getContentOffset();
// 	//取出上次选中状态，更新本次选中状态
// 	this->generalsSkillsList_tableView->reloadData();
// 	this->generalsSkillsList_tableView->setContentOffset(_s); 
	this->generalsSkillsList_tableView->reloadData();
	AutoSetOffSet();

	for (int i = 0;i<DataSourceList.size();++i)
	{
		if (curSkillId == DataSourceList.at(i)->curGameFightSkill->getId())
		{
			RefreshSkillInfo(DataSourceList.at(i)->curGameFightSkill);
			RefreshBtnStatus(DataSourceList.at(i)->curRealLevel);
		}
	}
}

void GeneralsSkillsUI::RefreshSkillInfo( GameFightSkill * gameFightSkill )
{
// 	//i_skillFrame; 
// 	
// 	std::string skillIconPath = "res_ui/jineng_icon/";
// 
// 	if (gameFightSkill->getId() != "")
// 	{
// 		if (strcmp(gameFightSkill->getId().c_str(),FightSkill_SKILL_ID_WARRIORDEFAULTATTACK) == 0
// 			||strcmp(gameFightSkill->getId().c_str(),FightSkill_SKILL_ID_MAGICDEFAULTATTACK) == 0
// 			||strcmp(gameFightSkill->getId().c_str(),FightSkill_SKILL_ID_RANGERDEFAULTATTACK) == 0
// 			||strcmp(gameFightSkill->getId().c_str(),FightSkill_SKILL_ID_WARLOCKDEFAULTATTACK) == 0)
// 		{
// 			skillIconPath.append("jineng_2");
// 		}
// 		else
// 		{
// 			skillIconPath.append(gameFightSkill->getCBaseSkill()->icon());
// 		}
// 
// 		skillIconPath.append(".png");
// 	}
// 	else
// 	{
// 		skillIconPath = "gamescene_state/jinengqu/suibian.png";
// 	}
// 	i_skillImage->setTexture(skillIconPath.c_str()) ;

	if (Layer_skills->getChildByTag(GENERALSSHORTCUTSLOT_TAG))
	{
		Layer_skills->getChildByTag(GENERALSSHORTCUTSLOT_TAG)->removeFromParent();
	}

	GeneralsShortcutSlot * generalsShortCutSlot = GeneralsShortcutSlot::create(gameFightSkill);
	generalsShortCutSlot->ignoreAnchorPointForPosition(false);
	generalsShortCutSlot->setAnchorPoint(ccp(0.5f,0.5f));
	generalsShortCutSlot->setPosition(ccp(393,355));
	generalsShortCutSlot->setTouchEnabled(false);
	Layer_skills->addChild(generalsShortCutSlot,0,GENERALSSHORTCUTSLOT_TAG);

	l_skillName->setText(gameFightSkill->getCBaseSkill()->name().c_str());

	//主动/被动
	if (gameFightSkill->getCBaseSkill()->usemodel() == 0)
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_zhudongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		l_skillType->setText(skillinfo_zhudongjineng);
	}
	else if (gameFightSkill->getCBaseSkill()->usemodel() == 2)
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_beidongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		l_skillType->setText(skillinfo_zhudongjineng);
	}
	else
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_zhudongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		l_skillType->setText(skillinfo_zhudongjineng);
	}

	char s_level[20];
	sprintf(s_level,"%d",gameFightSkill->getCFightSkill()->level());
	l_skillLvValue->setText(s_level);
	//l_skillQualityValue->setText("");
	switch(gameFightSkill->getCBaseSkill()->get_quality())
	{
	case 1:
		{
			imageView_skillQuality->setTexture("res_ui/o_white.png");
		}
		break;
	case 2:
		{
			imageView_skillQuality->setTexture("res_ui/o_green.png");
		}
		break;
	case 3:
		{
			imageView_skillQuality->setTexture("res_ui/o_blue.png");
		}
		break;
	case 4:
		{
			imageView_skillQuality->setTexture("res_ui/o_purple.png");
		}
		break;
	case 5:
		{
			imageView_skillQuality->setTexture("res_ui/o_orange.png");
		}
		break;
	default:
		imageView_skillQuality->setTexture("res_ui/o_white.png");
	}
	char s_mp[20];
	sprintf(s_mp,"%d",gameFightSkill->getCFightSkill()->mp());
	l_skillMpValue->setText(s_mp);
	char s_intervaltime[20];
	sprintf(s_intervaltime,"%d",gameFightSkill->getCFightSkill()->intervaltime()/1000);
	l_skillCDTimeValue->setText(s_intervaltime);
	char s_distance[20];
	sprintf(s_distance,"%d",gameFightSkill->getCFightSkill()->distance());
	l_skillDistanceValue ->setText(s_distance);

	refreshSkillDes(gameFightSkill->getCFightSkill()->description(),gameFightSkill->getCFightSkill()->get_next_description());

	if(Layer_skills->getWidgetByName("panel_levelInfo"))
	{
		Layer_skills->getWidgetByName("panel_levelInfo")->removeFromParent();
	}
	UIPanel * panel_levelInfo  = UIPanel::create();
	Layer_skills->addWidget(panel_levelInfo);
	panel_levelInfo->setName("panel_levelInfo");

	/////////////////////////////////////////////////////
	int m_required_level = 0;
	int m_required_point = 0;
	std::string m_str_pre_skill = "";
	std::string m_pre_skill_id = "";
	int m_pre_skill_level = 0;
	int m_required_gold= 0;
	int m_required_skillBookValue = 0;
	if (gameFightSkill->getLevel() == 0)
	{
		//等级
		if (gameFightSkill->getCFightSkill()->get_required_level()>0)
		{
			m_required_level = gameFightSkill->getCFightSkill()->get_required_level();
		}
		//技能点
		if (gameFightSkill->getCFightSkill()->get_required_point()>0)
		{
			m_required_point = gameFightSkill->getCFightSkill()->get_required_point();
		}
		//XXX技能
		if (gameFightSkill->getCFightSkill()->get_pre_skill() != "")
		{
			CBaseSkill * temp = StaticDataBaseSkill::s_baseSkillData[gameFightSkill->getCFightSkill()->get_pre_skill()];
			m_str_pre_skill.append(temp->name().c_str());
			m_pre_skill_id.append(temp->id());

			m_pre_skill_level = gameFightSkill->getCFightSkill()->get_pre_skill_level();
		}
		//金币
		if (gameFightSkill->getCFightSkill()->get_required_gold()>0)
		{
			m_required_gold = gameFightSkill->getCFightSkill()->get_required_gold();
		}
		//技能书
		if (gameFightSkill->getCFightSkill()->get_required_num()>0)
		{
			m_required_skillBookValue = gameFightSkill->getCFightSkill()->get_required_num();
		}
	}
	else
	{
		//等级
		if (gameFightSkill->getCFightSkill()->get_next_required_level()>0)
		{
			m_required_level = gameFightSkill->getCFightSkill()->get_next_required_level();
		}
		//技能点
		if (gameFightSkill->getCFightSkill()->get_next_required_point()>0)
		{
			m_required_point = gameFightSkill->getCFightSkill()->get_next_required_point();
		}
		//XXX技能
		if (gameFightSkill->getCFightSkill()->get_next_pre_skill() != "")
		{
			CBaseSkill * temp = StaticDataBaseSkill::s_baseSkillData[gameFightSkill->getCFightSkill()->get_next_pre_skill()];
			m_str_pre_skill.append(temp->name());
			m_pre_skill_id.append(temp->id());

			m_pre_skill_level = gameFightSkill->getCFightSkill()->get_next_pre_skill_level();
		}
		//金币
		if (gameFightSkill->getCFightSkill()->get_next_required_gold()>0)
		{
			m_required_gold = gameFightSkill->getCFightSkill()->get_next_required_gold();
		}
		//金技能书
		if (gameFightSkill->getCFightSkill()->get_next_required_num()>0)
		{
			m_required_skillBookValue = gameFightSkill->getCFightSkill()->get_next_required_num();
		}
	}

	/////////////////////////////////////////////////////
	int lineNum = 0;
	//主角等级
	if (m_required_level>0)
	{
		lineNum++;
		UILabel * l_playerLv = UILabel::create();
		l_playerLv->setText(StringDataManager::getString("skillinfo_zhujuedengji"));
		l_playerLv->setFontName(APP_FONT_NAME);
		l_playerLv->setFontSize(16);
		l_playerLv->setColor(ccc3(47,93,13));
		l_playerLv->setAnchorPoint(ccp(0,0));
		l_playerLv->setPosition(ccp(330,118-lineNum*20));
		l_playerLv->setName("l_playerLv");
		panel_levelInfo->addChild(l_playerLv);

		UILabel * l_playerLv_colon = UILabel::create();
		l_playerLv_colon->setText(StringDataManager::getString("skillinfo_maohao"));
		l_playerLv_colon->setFontName(APP_FONT_NAME);
		l_playerLv_colon->setFontSize(16);
		l_playerLv_colon->setColor(ccc3(47,93,13));
		l_playerLv_colon->setAnchorPoint(ccp(0,0));
		l_playerLv_colon->setPosition(ccp(394,118-lineNum*20));
		l_playerLv_colon->setName("l_playerLv_colon");
		panel_levelInfo->addChild(l_playerLv_colon);

		char s_next_required_level[20];
		sprintf(s_next_required_level,"%d",m_required_level);
		UILabel * l_playerLvValue = UILabel::create();
		l_playerLvValue->setText(s_next_required_level);
		l_playerLvValue->setFontName(APP_FONT_NAME);
		l_playerLvValue->setFontSize(16);
		l_playerLvValue->setColor(ccc3(35,93,13));
		l_playerLvValue->setAnchorPoint(ccp(0,0));
		l_playerLvValue->setPosition(ccp(404,118-lineNum*20));
		panel_levelInfo->addChild(l_playerLvValue);
		// 
		if (GameView::getInstance()->myplayer->getActiveRole()->level()<m_required_level)  //人物等级未达到技能升级要求
		{
		 	l_playerLv->setColor(ccc3(212,59,59));
			l_playerLv_colon->setColor(ccc3(212,59,59));
		 	l_playerLvValue->setColor(ccc3(212,59,59));
		}
	}
	//技能点
	if (m_required_point>0)
	{
		lineNum++;

		UIWidget * widget_skillPoint = UIWidget::create();
		panel_levelInfo->addChild(widget_skillPoint);
		widget_skillPoint->setName("widget_skillPoint");
		widget_skillPoint->setAnchorPoint(ccp(0,0));
		widget_skillPoint->setPosition(ccp(330,118-lineNum*20));

		std::string str_skillPoint = StringDataManager::getString("skillinfo_jinengdian");
		for(int i = 0;i<3;i++)
		{
			std::string str_name = "l_skillPoint";
			char s_index[5];
			sprintf(s_index,"%d",i);
			str_name.append(s_index);
			UILabel * l_skillPoint = UILabel::create();
			l_skillPoint->setText(str_skillPoint.substr(i*3,3).c_str());
			l_skillPoint->setFontName(APP_FONT_NAME);
			l_skillPoint->setFontSize(16);
			l_skillPoint->setColor(ccc3(47,93,13));
			l_skillPoint->setAnchorPoint(ccp(0,0));
			l_skillPoint->setName(str_name.c_str());
			widget_skillPoint->addChild(l_skillPoint);
			l_skillPoint->setPosition(ccp(24*i,0));
		}

		UILabel * l_skillPoint_colon = UILabel::create();
		l_skillPoint_colon->setText(StringDataManager::getString("skillinfo_maohao"));
		l_skillPoint_colon->setFontName(APP_FONT_NAME);
		l_skillPoint_colon->setFontSize(16);
		l_skillPoint_colon->setColor(ccc3(47,93,13));
		l_skillPoint_colon->setAnchorPoint(ccp(0,0));
		l_skillPoint_colon->setPosition(ccp(394,118-lineNum*20));
		l_skillPoint_colon->setName("l_skillPoint_colon");
		panel_levelInfo->addChild(l_skillPoint_colon);

		char s_required_point[20];
		sprintf(s_required_point,"%d",m_required_point);
		UILabel * l_skillPointValue = UILabel::create();
		l_skillPointValue->setText(s_required_point);
		l_skillPointValue->setFontName(APP_FONT_NAME);
		l_skillPointValue->setFontSize(16);
		l_skillPointValue->setColor(ccc3(47,93,13));
		l_skillPointValue->setAnchorPoint(ccp(0,0));
		l_skillPointValue->setPosition(ccp(404,118-lineNum*20));
		panel_levelInfo->addChild(l_skillPointValue);

		if(GameView::getInstance()->myplayer->player->skillpoint()<m_required_point)//技能点不足
		{
			for(int i = 0;i<3;i++)
			{
				std::string str_name = "l_skillPoint";
				char s_index[5];
				sprintf(s_index,"%d",i);
				str_name.append(s_index);
				UILabel * l_skillPoint = (UILabel*)widget_skillPoint->getChildByName(str_name.c_str());
				if (l_skillPoint)
				{
					l_skillPoint->setColor(ccc3(212,59,59));
				}
			}
			l_skillPoint_colon->setColor(ccc3(212,59,59));
			l_skillPointValue->setColor(ccc3(212,59,59));
		}
	}
	//XXX技能
	if (m_str_pre_skill != "")
	{
		lineNum++;
		UIWidget * widget_pre_skill = UIWidget::create();
		panel_levelInfo->addChild(widget_pre_skill);
		widget_pre_skill->setName("widget_pre_skill");
		widget_pre_skill->setAnchorPoint(ccp(0,0));
		widget_pre_skill->setPosition(ccp(330,118-lineNum*20));
		if (m_str_pre_skill.size()/3 < 4)
		{
			for(int i = 0;i<m_str_pre_skill.size()/3;i++)
			{
				std::string str_name = "l_preSkill";
				char s_index[5];
				sprintf(s_index,"%d",i);
				str_name.append(s_index);

				UILabel * l_preSkill = UILabel::create();
				l_preSkill->setText(m_str_pre_skill.substr(i*3,3).c_str());
				l_preSkill->setFontName(APP_FONT_NAME);
				l_preSkill->setFontSize(16);
				l_preSkill->setColor(ccc3(47,93,13));
				l_preSkill->setAnchorPoint(ccp(0,0));
				widget_pre_skill->addChild(l_preSkill);
				l_preSkill->setName(str_name.c_str());
				if (m_str_pre_skill.size()/3 == 1)
				{
					l_preSkill->setPosition(ccp(0,0));
				}
				else if (m_str_pre_skill.size()/3 == 2)
				{
					l_preSkill->setPosition(ccp(48*i,0));
				}
				else if (m_str_pre_skill.size()/3 == 3)
				{
					l_preSkill->setPosition(ccp(24*i,0));
				}
			}
		}
		else
		{
			UILabel * l_preSkill = UILabel::create();
			l_preSkill->setText(m_str_pre_skill.c_str());
			l_preSkill->setFontName(APP_FONT_NAME);
			l_preSkill->setFontSize(16);
			l_preSkill->setColor(ccc3(47,93,13));
			l_preSkill->setAnchorPoint(ccp(0,0));
			l_preSkill->setPosition(ccp(0,0));
			widget_pre_skill->addChild(l_preSkill);
			l_preSkill->setName("l_preSkill");
		}

		UILabel * l_preSkill_colon = UILabel::create();
		l_preSkill_colon->setText(StringDataManager::getString("skillinfo_maohao"));
		l_preSkill_colon->setFontName(APP_FONT_NAME);
		l_preSkill_colon->setFontSize(16);
		l_preSkill_colon->setColor(ccc3(47,93,13));
		l_preSkill_colon->setAnchorPoint(ccp(0,0));
		l_preSkill_colon->setPosition(ccp(394,118-lineNum*20));
		l_preSkill_colon->setName("l_preSkill_colon");
		panel_levelInfo->addChild(l_preSkill_colon);

		char s_pre_skill_level[20];
		sprintf(s_pre_skill_level,"%d",m_pre_skill_level);
		UILabel * l_preSkillValue = UILabel::create();
		l_preSkillValue->setText(s_pre_skill_level);
		l_preSkillValue->setFontName(APP_FONT_NAME);
		l_preSkillValue->setFontSize(16);
		l_preSkillValue->setColor(ccc3(47,93,13));
		l_preSkillValue->setAnchorPoint(ccp(0,0));
		l_preSkillValue->setPosition(ccp(404,118-lineNum*20));
		l_preSkillValue->setName("l_preSkillValue");
		panel_levelInfo->addChild(l_preSkillValue);

		std::map<std::string,GameFightSkill*>::const_iterator cIter;
		cIter = GameView::getInstance()->GameFightSkillList.find(m_pre_skill_id);
		if (cIter == GameView::getInstance()->GameFightSkillList.end()) // 没找到就是指向END了  
		{
		}
		else
		{
			GameFightSkill * gameFightSkillStuded = GameView::getInstance()->GameFightSkillList[m_pre_skill_id];
			if (gameFightSkillStuded)
			{
				if (gameFightSkillStuded->getLevel() < m_pre_skill_level)
				{
					if (m_str_pre_skill.size()/3 < 4)
					{
						for(int i = 0;i<m_str_pre_skill.size()/3;i++)
						{
							std::string str_name = "l_preSkill";
							char s_index[5];
							sprintf(s_index,"%d",i);
							str_name.append(s_index);

							UILabel * l_preSkill = (UILabel*)widget_pre_skill->getChildByName(str_name.c_str());
							if (l_preSkill)
							{
								l_preSkill->setColor(ccc3(212,59,59));
							}
						}
					}
					else
					{
						UILabel * l_preSkill = (UILabel*)widget_pre_skill->getChildByName("l_preSkill");
						if (l_preSkill)
						{
							l_preSkill->setColor(ccc3(212,59,59));
						}
					}
					l_preSkill_colon->setColor(ccc3(212,59,59));
					l_preSkillValue->setColor(ccc3(212,59,59));
				}
			}
		}
	}
	//金币
	if (m_required_gold>0)
	{
		lineNum++;
		 
		UIWidget * widget_gold = UIWidget::create();
		panel_levelInfo->addChild(widget_gold);
		widget_gold->setPosition(ccp(330,118-lineNum*20));

		std::string str_gold = StringDataManager::getString("skillinfo_jinbi");
		for(int i = 0;i<2;i++)
		{
			std::string str_name = "l_gold";
			char s_index[5];
			sprintf(s_index,"%d",i);
			str_name.append(s_index);
			UILabel * l_gold = UILabel::create();
			l_gold->setText(str_gold.substr(i*3,3).c_str());
			l_gold->setFontName(APP_FONT_NAME);
			l_gold->setFontSize(16);
			l_gold->setColor(ccc3(47,93,13));
			l_gold->setAnchorPoint(ccp(0,0));
			l_gold->setName(str_name.c_str());
			widget_gold->addChild(l_gold);
			l_gold->setPosition(ccp(48*i,0));
		}

		UILabel * l_gold_colon = UILabel::create();
		l_gold_colon->setText(StringDataManager::getString("skillinfo_maohao"));
		l_gold_colon->setFontName(APP_FONT_NAME);
		l_gold_colon->setFontSize(16);
		l_gold_colon->setColor(ccc3(47,93,13));
		l_gold_colon->setAnchorPoint(ccp(0,0));
		l_gold_colon->setPosition(ccp(394,118-lineNum*20));
		l_gold_colon->setName("l_gold_colon");
		panel_levelInfo->addChild(l_gold_colon);

		char s_required_gold[20];
		sprintf(s_required_gold,"%d",m_required_gold);
		UILabel * l_goldValue = UILabel::create();
		l_goldValue->setText(s_required_gold);
		l_goldValue->setFontName(APP_FONT_NAME);
		l_goldValue->setFontSize(16);
		l_goldValue->setColor(ccc3(47,93,13));
		l_goldValue->setAnchorPoint(ccp(0,0));
		l_goldValue->setPosition(ccp(404,118-lineNum*20));
		panel_levelInfo->addChild(l_goldValue);

 	 	if(GameView::getInstance()->getPlayerGold()<m_required_gold)//金币不足
 	 	{
			for(int i = 0;i<2;i++)
			{
				std::string str_name = "l_gold";
				char s_index[5];
				sprintf(s_index,"%d",i);
				str_name.append(s_index);
				UILabel * l_gold = (UILabel*)widget_gold->getChildByName(str_name.c_str());
				if (l_gold)
				{
					l_gold->setColor(ccc3(212,59,59));
				}
			}
			l_gold_colon->setColor(ccc3(212,59,59));
 	 		l_goldValue->setColor(ccc3(212,59,59));
 	 	}
	}
	//技能书
	if (m_required_skillBookValue>0)
	{
		lineNum++;

		UIWidget * widget_skillBook = UIWidget::create();
		panel_levelInfo->addChild(widget_skillBook);
		widget_skillBook->setName("widget_skillBook");
		widget_skillBook->setPosition(ccp(330,118-lineNum*20));

		std::string str_skillbook = GeneralsSkillsUI::getSkillQualityStr(gameFightSkill->getCBaseSkill()->get_quality());
		str_skillbook.append(StringDataManager::getString("skillinfo_jinengshu"));
		UILabel * l_skillbook = UILabel::create();
		l_skillbook->setText(str_skillbook.c_str());
		l_skillbook->setFontName(APP_FONT_NAME);
		l_skillbook->setFontSize(16);
		l_skillbook->setColor(ccc3(47,93,13));
		l_skillbook->setAnchorPoint(ccp(0,0));
		l_skillbook->setName("l_skillbook");
		widget_skillBook->addChild(l_skillbook);
		l_skillbook->setPosition(ccp(0,0));

		UILabel * l_skillbook_colon = UILabel::create();
		l_skillbook_colon->setText(StringDataManager::getString("skillinfo_maohao"));
		l_skillbook_colon->setFontName(APP_FONT_NAME);
		l_skillbook_colon->setFontSize(16);
		l_skillbook_colon->setColor(ccc3(47,93,13));
		l_skillbook_colon->setAnchorPoint(ccp(0,0));
		l_skillbook_colon->setPosition(ccp(394,118-lineNum*20));
		l_skillbook_colon->setName("l_skillbook_colon");
		panel_levelInfo->addChild(l_skillbook_colon);

		char s_required_skillBook[20];
		sprintf(s_required_skillBook,"%d",m_required_skillBookValue);
		UILabel * l_skillBookValue = UILabel::create();
		l_skillBookValue->setText(s_required_skillBook);
		l_skillBookValue->setFontName(APP_FONT_NAME);
		l_skillBookValue->setFontSize(16);
		l_skillBookValue->setColor(ccc3(47,93,13));
		l_skillBookValue->setAnchorPoint(ccp(0,0));
		l_skillBookValue->setPosition(ccp(404,118-lineNum*20));
		panel_levelInfo->addChild(l_skillBookValue);

		int propNum = 0;
		for (int i = 0;i<GameView::getInstance()->AllPacItem.size();++i)
		{
			FolderInfo * folder = GameView::getInstance()->AllPacItem.at(i);
			if (!folder->has_goods())
				continue;

			if (folder->goods().id() == gameFightSkill->getCFightSkill()->get_required_prop())
			{
				propNum += folder->quantity();
			}
		}
		if(propNum<gameFightSkill->getCFightSkill()->get_required_num())//技能书不足
		{
			l_skillbook->setColor(ccc3(212,59,59));
			l_skillbook_colon->setColor(ccc3(212,59,59));
			l_skillBookValue->setColor(ccc3(212,59,59));
		}
	}
}

std::string GeneralsSkillsUI::getCurSkillId()
{
	return curSkillId;
}

void GeneralsSkillsUI::RefreshBtnStatus( int curRealLevel )
{
	if (curRealLevel > 0)
	{
		l_levelUp->setText(StringDataManager::getString("generals_strategies_upgrade"));
	}
	else
	{
		l_levelUp->setText(StringDataManager::getString("generals_strategies_study"));
	}
}

void GeneralsSkillsUI::AutoSetOffSet()
{
	int selectIndex = -1;
	for (int i = 0;i<DataSourceList.size();i++)
	{
		if (curSkillId == DataSourceList.at(i)->curGameFightSkill->getId())
		{
			selectIndex = i;
			break;
		}
	}

	if (selectIndex == -1)
		return;

	//strategiesList_tableView->selectCell(selectIndex);
	this->generalsSkillsList_tableView->selectCell(selectIndex);

	if (DataSourceList.size()*tableCellSizeForIndex(generalsSkillsList_tableView,0).height < generalsSkillsList_tableView->getViewSize().height)
	{
	}
	else
	{
		if ((DataSourceList.size()-selectIndex)*tableCellSizeForIndex(generalsSkillsList_tableView,0).height  <= generalsSkillsList_tableView->getViewSize().height)  //是最后几个
		{
			generalsSkillsList_tableView->setContentOffset(ccp(0,0));
		}
		else
		{
			int beginHeight = generalsSkillsList_tableView->getViewSize().height - DataSourceList.size()*tableCellSizeForIndex(generalsSkillsList_tableView,0).height;
			generalsSkillsList_tableView->setContentOffset(ccp(0,beginHeight+selectIndex*tableCellSizeForIndex(generalsSkillsList_tableView,0).height));
		}
	}
}

void GeneralsSkillsUI::NullSkillUpEvent( CCObject * pSender )
{
	std::map<int,std::string>::const_iterator cIter;
	cIter = SystemInfoConfigData::s_systemInfoList.find(19);
	if (cIter == SystemInfoConfigData::s_systemInfoList.end())
	{
		return ;
	}
	else
	{
		if(GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesDetailInfo) == NULL)
		{
			CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
			ShowSystemInfo * showSystemInfo = ShowSystemInfo::create(SystemInfoConfigData::s_systemInfoList[19].c_str());
			GameView::getInstance()->getMainUIScene()->addChild(showSystemInfo,0,kTagStrategiesDetailInfo);
			showSystemInfo->ignoreAnchorPointForPosition(false);
			showSystemInfo->setAnchorPoint(ccp(0.5f, 0.5f));
			showSystemInfo->setPosition(ccp(winSize.width/2, winSize.height/2));
		}
	}
}

void GeneralsSkillsUI::refreshSkillDes( std::string curSkillDes,std::string nextSkillDes )
{
	//skill des text
	if (Layer_skills->getWidgetByTag(SCROLLVIEW_SKILLDESINFO_TAG))
	{
		UIScrollView * scroll_ = (UIScrollView *)Layer_skills->getWidgetByTag(SCROLLVIEW_SKILLDESINFO_TAG);
		scroll_->removeFromParentAndCleanup(true);
	}

	UIScrollView * scrollView_info = UIScrollView::create();
	scrollView_info->setTouchEnable(true);
	scrollView_info->setDirection(SCROLLVIEW_DIR_VERTICAL);
	scrollView_info->setSize(CCSizeMake(264,303));
	scrollView_info->setPosition(ccp(495,97));
	scrollView_info->setBounceEnabled(true);
	scrollView_info->setTag(SCROLLVIEW_SKILLDESINFO_TAG);
	Layer_skills->addWidget(scrollView_info);

	ccColor3B color_ = ccc3(47,93,13);

	UIImageView * mo_1 = UIImageView::create();
	mo_1->setTexture("res_ui/mo_2.png");
	mo_1->setAnchorPoint(ccp(0,0));
	mo_1->setScale(0.7f);
	scrollView_info->addChild(mo_1);

	UILabelBMFont * labelBM_1 = UILabelBMFont::create();
	labelBM_1->setAnchorPoint(ccp(0,0));
	labelBM_1->setText(StringDataManager::getString("robotui_skill_des_setSkill"));
	labelBM_1->setFntFile("res_ui/font/ziti_3.fnt");
	scrollView_info->addChild(labelBM_1);

	UILabel * label_des=UILabel::create();
	label_des->setText(curSkillDes.c_str());
	label_des->setAnchorPoint(ccp(0,0));
	label_des->setFontName(APP_FONT_NAME);
	label_des->setFontSize(18);
	label_des->setColor(color_);
	label_des->setTextAreaSize(CCSizeMake(240,0));
	scrollView_info->addChild(label_des);

	UIImageView * mo_2 = UIImageView::create();
	mo_2->setTexture("res_ui/mo_2.png");
	mo_2->setAnchorPoint(ccp(0,0));
	mo_2->setScale(0.7f);
	scrollView_info->addChild(mo_2);

	UILabelBMFont * labelBM_2 = UILabelBMFont::create();
	labelBM_2->setAnchorPoint(ccp(0,0));
	labelBM_2->setText(StringDataManager::getString("robotui_nextSkill_des_setSkill"));
	labelBM_2->setFntFile("res_ui/font/ziti_3.fnt");
	scrollView_info->addChild(labelBM_2);

	UILabel * label_next_des=UILabel::create();
	label_next_des->setText(nextSkillDes.c_str());
	label_next_des->setAnchorPoint(ccp(0,0));
	label_next_des->setFontName(APP_FONT_NAME);
	label_next_des->setFontSize(18);
	label_next_des->setColor(color_);
	label_next_des->setTextAreaSize(CCSizeMake(240,0));
	scrollView_info->addChild(label_next_des);

	int h_ = mo_1->getContentSize().height * 2;
	int temp_min_h = 100;

	if (label_des->getContentSize().height > temp_min_h)
	{
		h_+= label_des->getContentSize().height;
	}else
	{
		h_+= temp_min_h;
	}

	if (label_next_des->getContentSize().height > temp_min_h)
	{
		h_+= label_next_des->getContentSize().height;
	}else
	{
		h_+= temp_min_h;
	}


	int innerWidth = scrollView_info->getRect().size.width;
	int innerHeight = h_;
	if (h_ < scrollView_info->getRect().size.height)
	{
		innerHeight = scrollView_info->getRect().size.height;
	}

	scrollView_info->setInnerContainerSize(CCSizeMake(innerWidth,innerHeight));

	mo_1->setPosition(ccp(0,innerHeight - mo_1->getContentSize().height ));
	labelBM_1->setPosition(ccp(10,mo_1->getPosition().y+2));
	label_des->setPosition(ccp(0,mo_1->getPosition().y - label_des->getContentSize().height));

	mo_2->setPosition(ccp(0,label_des->getPosition().y - 30));
	labelBM_2->setPosition(ccp(10,mo_2->getPosition().y+2));
	label_next_des->setPosition(ccp(0,mo_2->getPosition().y - label_next_des->getContentSize().height));
}

std::string GeneralsSkillsUI::getSkillQualityStr( int skillQuality )
{
	std::string temp = "";
	switch(skillQuality)
	{
	case 0:
		{
			temp.append(StringDataManager::getString("generals_skill_white"));
		}
		break;
	case 1:
		{
			temp.append(StringDataManager::getString("generals_skill_white"));
		}
		break;
	case 2:
		{
			temp.append(StringDataManager::getString("generals_skill_green"));
		}
		break;
	case 3:
		{
			temp.append(StringDataManager::getString("generals_skill_blue"));
		}
		break;
	case 4:
		{
			temp.append(StringDataManager::getString("generals_skill_purple"));
		}
		break;
	case 5:
		{
			temp.append(StringDataManager::getString("generals_skill_orange"));
		}
		break;
	default:
		temp.append(StringDataManager::getString("generals_skill_white"));
	}

	return temp;
}

//////////////////////////////////////////////////////////////////////////////

GeneralSkillCellData::GeneralSkillCellData()
{
	curGameFightSkill = new GameFightSkill();
}

GeneralSkillCellData::~GeneralSkillCellData()
{
	CC_SAFE_DELETE(curGameFightSkill);
}


//////////////////////////////////////////////////////////////////////////////

#define  KTAG_Sprite_SkillVariety 50

GeneralSkillCell::GeneralSkillCell()
{

}

GeneralSkillCell::~GeneralSkillCell()
{
}

GeneralSkillCell* GeneralSkillCell::create( GeneralSkillCellData * cellData )
{
	GeneralSkillCell * generalSkillCell = new GeneralSkillCell();
	if(generalSkillCell && generalSkillCell->init(cellData))
	{
		generalSkillCell->autorelease();
		return generalSkillCell;
	}
	CC_SAFE_DELETE(generalSkillCell);
}

bool GeneralSkillCell::init( GeneralSkillCellData * cellData )
{
	if(CCTableViewCell::init())
	{
		CCScale9Sprite * sprite_bigFrame = CCScale9Sprite::create("res_ui/kuang01_new.png");
		sprite_bigFrame->setAnchorPoint(ccp(0, 0));
		sprite_bigFrame->setPosition(ccp(0, 0));
		sprite_bigFrame->setContentSize(CCSizeMake(225,63));
		sprite_bigFrame->setCapInsets(CCRectMake(15,31,1,1));
		this->addChild(sprite_bigFrame);

		//小边框
		CCSprite *  smaillFrame = CCSprite::create("res_ui/jinengdi.png");
		smaillFrame->setAnchorPoint(ccp(0, 0));
		smaillFrame->setPosition(ccp(19,5));
		smaillFrame->setScale(0.91f);
		this->addChild(smaillFrame);

		CCScale9Sprite *sprite_nameFrame = CCScale9Sprite::create("res_ui/LV4_diaa.png");
		sprite_nameFrame->setAnchorPoint(ccp(0, 0));
		sprite_nameFrame->setPosition(ccp(83, 17));
		sprite_nameFrame->setCapInsets(CCRectMake(11,11,1,1));
		sprite_nameFrame->setContentSize(CCSizeMake(130,30));
		sprite_bigFrame->addChild(sprite_nameFrame);

		std::string skillIconPath = "res_ui/jineng_icon/";

		if (cellData->curGameFightSkill->getId() != "")
		{
			skillIconPath.append(cellData->curGameFightSkill->getCBaseSkill()->icon());
			skillIconPath.append(".png");
		}
		else
		{
			skillIconPath = "gamescene_state/zhujiemian3/jinengqu/suibian.png";
		}

		CBaseSkill * baseSkill = StaticDataBaseSkill::s_baseSkillData[cellData->curGameFightSkill->getId()];
		CCAssert(baseSkill != NULL, "baseSkill should not be null");

		sprite_IconImage = CCSprite::create(skillIconPath.c_str());
		if(sprite_IconImage != NULL)
		{
			sprite_IconImage->setAnchorPoint(ccp(0.5f,0.5f));
			sprite_IconImage->setPosition(ccp(45,31));
			this->addChild(sprite_IconImage);

			CCSprite *  sprite_lvFrame = CCSprite::create("res_ui/jineng/jineng_zhezhao.png");
			sprite_lvFrame->setAnchorPoint(ccp(0.5f,0.f));
			sprite_lvFrame->setPosition(ccp(45/2+1,0));
			sprite_IconImage->addChild(sprite_lvFrame);

			std::string str_lv = "";
			char s_curLv [5];
			sprintf(s_curLv,"%d",cellData->curGameFightSkill->getLevel());
			str_lv.append(s_curLv);
			str_lv.append("/");
			char max_lv [5];
			sprintf(max_lv,"%d",cellData->curGameFightSkill->getCBaseSkill()->maxlevel());
			str_lv.append(max_lv);

			l_lv = CCLabelTTF::create(str_lv.c_str(),APP_FONT_NAME,12);
			l_lv->setAnchorPoint(ccp(0.5f,0.f));
			l_lv->setPosition(ccp(sprite_lvFrame->getPosition().x,sprite_lvFrame->getPosition().y));
			sprite_lvFrame->addChild(l_lv);
		}

		// 	CCSprite * sprite_highLight = CCSprite::create(HIGHLIGHT1);
		// 	sprite_highLight->setAnchorPoint(ccp(0.5f, 0.5f));
		// 	sprite_highLight->setPosition(ccp(27,28));
		// 	sprite_frame->addChild(sprite_highLight);

		//skill variety icon
		if (this->getChildByTag(KTAG_Sprite_SkillVariety))
		{
			this->getChildByTag(KTAG_Sprite_SkillVariety)->removeFromParent();
		}
		if (baseSkill->usemodel() == 0)
		{
			CCNode* sprite_skillVariety = SkillScene::GetSkillVarirtySprite(baseSkill->id().c_str());
			if (sprite_skillVariety)
			{
				sprite_skillVariety->setAnchorPoint(ccp(0.5f,0.5f));
				sprite_skillVariety->setPosition(ccp(60,29));
				sprite_skillVariety->setScale(.85f);
				this->addChild(sprite_skillVariety);
				sprite_skillVariety->setTag(KTAG_Sprite_SkillVariety);
			}
		}

		label_skillName = CCLabelTTF::create(cellData->curGameFightSkill->getCBaseSkill()->name().c_str(),APP_FONT_NAME,18);
		label_skillName->setAnchorPoint(ccp(0, 0.5f));
		label_skillName->setPosition(ccp(100,31));
		// 	ccColor3B shadowColor = ccc3(0,0,0);   // black
		// 	label_skillName->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		this->addChild(label_skillName);

		if (baseSkill->get_quality() == 1)
		{
			label_skillName->setColor(ccc3(255,255,255));
		}
		else if (baseSkill->get_quality()  == 2)
		{
			label_skillName->setColor(ccc3(0,255,0));
		}
		else if (baseSkill->get_quality()  == 3)
		{
			label_skillName->setColor(ccc3(0,255,255));
		}
		else if (baseSkill->get_quality()  == 4)
		{
			label_skillName->setColor(ccc3(255,0,228));
		}
		else if (baseSkill->get_quality()  == 5)
		{
			label_skillName->setColor(ccc3(254,124,0));
		}
		else
		{
			label_skillName->setColor(ccc3(255,255,255));
		}

//  	 	if (this->selectCellId == idx)
//  	 	{
//  	 		CCScale9Sprite* pHighlightSpr = CCScale9Sprite::create(CCRectMake(26, 26, 1, 1) , "res_ui/highlight.png");
//  	 		pHighlightSpr->setPreferredSize(CCSize(225,66));
//  	 		pHighlightSpr->setAnchorPoint(CCPointZero);
//  	 		pHighlightSpr->setTag(GENERALSSKILLSUI_TABALEVIEW_HIGHLIGHT_TAG);
//  	 		this->addChild(pHighlightSpr);
//  	 	}

		sprite_blackFrame = CCSprite::create(PROGRESS_SKILL);
		sprite_blackFrame->setAnchorPoint(ccp(0.5f,0.5f));
		sprite_blackFrame->setPosition(ccp(45,31));
		sprite_blackFrame->setScale(1.0f);
		this->addChild(sprite_blackFrame,1);

		CCSprite * sprite_lock = CCSprite::create("res_ui/suo_2.png");
		sprite_lock->setAnchorPoint(ccp(0.5f,0.5f));
		sprite_lock->setPosition(ccp(sprite_blackFrame->getContentSize().width/2,sprite_blackFrame->getContentSize().height/2));
		sprite_lock->setScale(0.85f);
		sprite_blackFrame->addChild(sprite_lock);

		if (cellData->curRealLevel > 0)
		{
			sprite_blackFrame->setVisible(false);
		}
		else
		{
			sprite_blackFrame->setVisible(true);
		}

		return true;
	}
	return false;
}

void GeneralSkillCell::RefreshCell( GeneralSkillCellData * cellData )
{
	std::string skillIconPath = "res_ui/jineng_icon/";

	if (cellData->curGameFightSkill->getId() != "")
	{
		skillIconPath.append(cellData->curGameFightSkill->getCBaseSkill()->icon());
		skillIconPath.append(".png");
	}
	else
	{
		skillIconPath = "gamescene_state/zhujiemian3/jinengqu/suibian.png";
	}

	CBaseSkill * baseSkill = StaticDataBaseSkill::s_baseSkillData[cellData->curGameFightSkill->getId()];
	CCAssert(baseSkill != NULL, "baseSkill should not be null");

	CCTexture2D* texture = CCTextureCache::sharedTextureCache()->addImage(skillIconPath.c_str());
	sprite_IconImage->setTexture(texture);
	std::string str_lv = "";
	char s_curLv [5];
	sprintf(s_curLv,"%d",cellData->curGameFightSkill->getLevel());
	str_lv.append(s_curLv);
	str_lv.append("/");
	char max_lv [5];
	sprintf(max_lv,"%d",cellData->curGameFightSkill->getCBaseSkill()->maxlevel());
	str_lv.append(max_lv);
	l_lv->setString(str_lv.c_str());

	//skill variety icon
	if (this->getChildByTag(KTAG_Sprite_SkillVariety))
	{
		this->getChildByTag(KTAG_Sprite_SkillVariety)->removeFromParent();
	}
	if (baseSkill->usemodel() == 0)
	{
		CCNode* sprite_skillVariety = SkillScene::GetSkillVarirtySprite(baseSkill->id().c_str());
		if (sprite_skillVariety)
		{
			sprite_skillVariety->setAnchorPoint(ccp(0.5f,0.5f));
			sprite_skillVariety->setPosition(ccp(60,29));
			sprite_skillVariety->setScale(.85f);
			this->addChild(sprite_skillVariety);
			sprite_skillVariety->setTag(KTAG_Sprite_SkillVariety);
		}
	}

	label_skillName->setString(cellData->curGameFightSkill->getCBaseSkill()->name().c_str());
	if (baseSkill->get_quality() == 1)
	{
		label_skillName->setColor(ccc3(255,255,255));
	}
	else if (baseSkill->get_quality()  == 2)
	{
		label_skillName->setColor(ccc3(0,255,0));
	}
	else if (baseSkill->get_quality()  == 3)
	{
		label_skillName->setColor(ccc3(0,255,255));
	}
	else if (baseSkill->get_quality()  == 4)
	{
		label_skillName->setColor(ccc3(255,0,228));
	}
	else if (baseSkill->get_quality()  == 5)
	{
		label_skillName->setColor(ccc3(254,124,0));
	}
	else
	{
		label_skillName->setColor(ccc3(255,255,255));
	}

	if (cellData->curRealLevel > 0)
	{
		sprite_blackFrame->setVisible(false);
	}
	else
	{
		sprite_blackFrame->setVisible(true);
	}

	this->setScale(1.0f);
}

void GeneralSkillCell::setSelectSelf( bool isvisible )
{
	if (isvisible)
	{
		CCScale9Sprite* pHighlightSpr = CCScale9Sprite::create(CCRectMake(26, 26, 1, 1) , "res_ui/highlight.png");
		pHighlightSpr->setPreferredSize(CCSize(225,66));
		pHighlightSpr->setAnchorPoint(CCPointZero);
		pHighlightSpr->setTag(GENERALSSKILLSUI_TABALEVIEW_HIGHLIGHT_TAG);
		this->addChild(pHighlightSpr);
	}
	else
	{
		if (this->getChildByTag(GENERALSSKILLSUI_TABALEVIEW_HIGHLIGHT_TAG))
		{
			this->getChildByTag(GENERALSSKILLSUI_TABALEVIEW_HIGHLIGHT_TAG)->removeFromParent();
		}
	}
}
