#ifndef  _GENERALSUI_GENERALSTEACHUI_H_
#define _GENERALSUI_GENERALSTEACHUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "../../messageclient/protobuf/RecruitMessage.pb.h"
#include "./GeneralsListBase.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CGeneralBaseMsg;
class CGeneralDetail;
class NormalGeneralsHeadItemBase;

/////////////////////////////////
/**
 * 武将界面下的 武将传功类（吞吃相同卡牌）
 * @author yangjun
 * @version 0.1.0
 * @date 2013.12.4
 */

class GeneralsTeachUI : public GeneralsListBase,public CCTableViewDelegate,public CCTableViewDataSource
{
public:
	GeneralsTeachUI();
	~GeneralsTeachUI();

	struct headSlotInfo{
		bool isHave;
		CCPoint loc;
		int tag;
	};

	static GeneralsTeachUI* create(CGeneralBaseMsg * generalsBaseMsg);
	bool init(CGeneralBaseMsg * generalsBaseMsg);

	virtual void onEnter();
	virtual void onExit();

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);

	headSlotInfo* getEmptyItem();
	//是否已经添加到传功位置
	bool isAddedToTeach(long long _id);
	void addToTeach(CGeneralBaseMsg * generalsBaseMsg,headSlotInfo* headslotinfo);

	void TeachEvent(CCObject *pSender);
	//一键传功
	void ConvenientTeachEvent(CCObject *pSender);
	void ResetEvent(CCObject *pSender);
	void BackEvent(CCObject *pSender);

	void BeginTeachAnm();

	//进度条走到头，然后调用（有下一级）
	void ProgressToFinishCall();
	//进度条没走到头，然后调用（没有下一级）
	void ProgressNotFinishCall();
	void ResetCallFucTime();
	//进度条最后一次走到头，此时需要设置预览条的长度
	void FinalProgressToFinishCall(CCNode * pNode,void * scale);

private:
	UIPanel * panel_teach; //传功面板
	UILayer * Layer_teach;
	UILayer * Layer_upper;

	headSlotInfo* headSlotInfo1;
	headSlotInfo* headSlotInfo2;
	headSlotInfo* headSlotInfo3;
	headSlotInfo* headSlotInfo4;

	CCTableView * generalList_tableView;	

	UILabel * l_curTeachExp;
	UILabel * l_constValue;

	CCLabelTTF * l_CapsExp_value;
	CCProgressTimer *sp_exp_cur;
	CCProgressTimer *sp_exp_preView;

	//调用ProgressToFinishCall的次数
	int m_nCallProgressFunctionTimes;

	std::string nextTeachValue;

	//预测提升的等级
	int m_nPreViewUpgradeLevel;


	/////////////////////PreView Info////////////////////////////
	//int m_nPreViewLevel;
	//int m_nPreViewExp;

	UILabelBMFont * lbf_hp;
	UILabelBMFont * lbf_max_attack;
	UILabelBMFont * lbf_max_magic_attack;
	UILabel * l_hpValue_cur;
	UILabel * l_hpValue_next;
	UILabel * l_attackValue_cur;
	UILabel * l_attackValue_next;

	UIImageView * image_jiantou_first;
	UIImageView * image_jiantou_second;
	//////////////////////////////////////////////////////////////

	UIImageView * ImageView_MainGeneralFrame;

public:
	long long curTeachGeneralId ;
	CGeneralBaseMsg * curTeachGeneral;
	CGeneralBaseMsg * oldTeachGeneral;
	CGeneralDetail * curTeachGeneralDetail;
	CGeneralDetail * oldTeachGeneralDetail;
	std::vector<CGeneralBaseMsg *> generalsTeachList;

	CGeneralBaseMsg * curVictimGeneralBaseMsg;

	int curExpValue;
	int costGoldValue;

	//传功的牺牲品（最对四个）
	std::vector<long long> victimList;

	//刷新武将列表
	void RefreshGeneralsList();
	//刷新武将列表(tableView 的偏移量不变)
	void RefreshGeneralsListWithOutChangeOffSet();

	void RefreshGeneralsAddedPropertyCur(CGeneralDetail * generalDetail);
	void RefreshGeneralsAddedPropertyPreView();

	void setGeneralsAddedPropertyToDefault(CGeneralDetail * generalDetail);

	void createTeachHeadItem(CGeneralBaseMsg * generalsBaseMsg);
	void refreshTeachHeadItem(CGeneralBaseMsg * generalsBaseMsg);
	void refreshEvolutionHeadFrame(CGeneralBaseMsg * generalsBaseMsg);
	//传功完成后，把界面置为初始
	void setToDefault();

	virtual bool isVisible();
	virtual void setVisible(bool visible);

	void AddBonusSpecialEffect();

	//add AptitudePopupEffect
	void AddAptitudePopupEffect();
};

#endif

