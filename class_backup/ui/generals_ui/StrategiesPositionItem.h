
#ifndef _GENERALSUI_STRATEGIESPOSITIONITEM
#define _GENERALSUI_STRATEGIESPOSITIONITEM

#include "../extensions/UIScene.h"
/////////////////////////////////
/**
 * 武将界面下的“阵法界面”中的阵位信息类
 * @author yangjun
 * @version 0.1.0
 * @date 2013.12.16
 */

class CGeneralBaseMsg;

class StrategiesPositionItem : public UIScene
{
public:
	StrategiesPositionItem();
	~StrategiesPositionItem();

	static StrategiesPositionItem * create(CGeneralBaseMsg * generalBaseMsg);
	bool init(CGeneralBaseMsg * generalBaseMsg);

	static StrategiesPositionItem * create(int index);
	bool init(int index);

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	void RefreshGeneralsStatus(int _status);

	void RefreshAddedProperty();

private:
	void FightStatusEvent(CCObject *pSender);
	void HeadFrameEvent(CCObject *pSender);

private:
	UILabel * l_fightStatus;
	UIButton * btn_fightStatus;
	UILabel * l_additive;
public:
	//1-5
	int positionIndex;
	//休息 0，掠阵 1，出战 2
	int fightStatus;
	//该位置是否有武将
	bool isHaveGeneral;
	//当前武将id
	long long genenralsId;

};

#endif