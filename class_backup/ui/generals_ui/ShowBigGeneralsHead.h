
#ifndef _GENERALSUI_SHOWBIGGENERALSHEAD_H_
#define _GENERALSUI_SHOWBIGGENERALSHEAD_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CCLegendAnimation;

#define BigGeneralsFramePath_White    "res_ui/generals_dia.png"
#define BigGeneralsFramePath_Green   "res_ui/generals_diagreen.png"
#define BigGeneralsFramePath_Blue      "res_ui/generals_diablue.png"
#define BigGeneralsFramePath_Purpe    "res_ui/generals_diapurple.png"
#define BigGeneralsFramePath_Orange  "res_ui/generals_diaorange.png"

/////////////////////////////////
/**
 * 武将大卡牌展示效果
 * @author yangjun
 * @version 0.1.0
 * @date 2014.8.7
 */

class CGeneralBaseMsg;
class CGeneralDetail;

class ShowBigGeneralsHead :public UIScene
{
public:
	ShowBigGeneralsHead();
	~ShowBigGeneralsHead();

	enum CreateType
	{
		CT_withGeneralBaseMsg = 0,
		CT_withModleId,
		CT_withModleIdAndQuality,
	};

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	static ShowBigGeneralsHead * create(CGeneralBaseMsg * generalBaseMsg);
	static ShowBigGeneralsHead * create(int generalModleId);
	static ShowBigGeneralsHead * create(int generalModleId,int currentquality);
	bool init(CGeneralBaseMsg * generalBaseMsg);
	bool init(int generalModleId);
	bool init(int generalModleId,int currentquality);

	std::string getBigHeadFramePath( int quality );

	//beginAction
	void showBeginAction();

public:
	UIImageView * frame;
	UIImageView * black_namebg;
	UILabel * label_name;
	UIImageView * imageView_head;
	UIImageView * imageView_star;

	int m_nCreateType;

	CGeneralBaseMsg * m_pGeneralBaseMsg;
	int m_nGeneralModleId;
	int m_nCurrentquality;
};

#endif