#ifndef  _GENERALSUI_GENERALSLISTBASE_H_
#define _GENERALSUI_GENERALSLISTBASE_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "../../messageclient/protobuf/RecruitMessage.pb.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CGeneralBaseMsg;
class CGeneralDetail;

/////////////////////////////////
/**
 * 武将界面下的“我的武将”中的武将列表类
 * @author yangjun
 * @version 0.1.0
 * @date 2013.12.3
 */

class GeneralsListBase : public UIScene
{
public:
	GeneralsListBase();
	~GeneralsListBase();
	static GeneralsListBase* create();
	bool init();

	//武将的状态
	enum GeneralsFightStatus{
		NoBattle = 0,
		HoldTheLine,
		InBattle 
	};

	//武将列表是否有上一页或下一页
	enum GeneralsListStatus{
		HaveLast = 0,
		HaveNext,
		HaveBoth,
		HaveNone,
	};

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	//更新List的状态
	//page   当前页数
	//allPageNum   总页数
	void RefreshGeneralsListStatus( int page, int allPageNum);

	bool getIsCanReq();
	void setIsCanReq(bool _value);

private:
	//服务器是否已经返回（已经返回数据才能继续发请求）
	bool isCanReq;

public:
	GeneralsListStatus  generalsListStatus;
	int everyPageNum;

	int s_mCurPage;
	int s_mAllPageNum;

	//是重新请求新数据还是继续添加第二页...
	bool isReqNewly;

};

#endif

