#include "TenTimesRecuriteResultUI.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "GeneralsUI.h"
#include "GameView.h"
#include "../../messageclient/element/CActionDetail.h"
#include "GeneralsHeadItemBase.h"
#include "GeneralsRecuriteUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "GeneralsListUI.h"
#include "../../legend_engine/CCLegendAnimation.h"
#include "../../gamescene_state/GameSceneEffects.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "RecuriteActionItem.h"
#include "../../ui/extensions/RecruitGeneralCard.h"


TenTimesRecuriteResultUI::TenTimesRecuriteResultUI()
{
}


TenTimesRecuriteResultUI::~TenTimesRecuriteResultUI()
{
}

TenTimesRecuriteResultUI* TenTimesRecuriteResultUI::create()
{
	TenTimesRecuriteResultUI * tenTimesRecuriteResultUI = new TenTimesRecuriteResultUI();
	if (tenTimesRecuriteResultUI && tenTimesRecuriteResultUI->init())
	{
		tenTimesRecuriteResultUI->autorelease();
		return tenTimesRecuriteResultUI;
	}
	CC_SAFE_DELETE(tenTimesRecuriteResultUI);
	return NULL;
}

bool TenTimesRecuriteResultUI::init()
{
	if (UIScene::init())
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		this->scheduleUpdate();

		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winSize);
		mengban->setAnchorPoint(ccp(0.5f,0.5f));
		mengban->setPosition(ccp(722/2,438/2));
		m_pUiLayer->addWidget(mengban);

		//加载UI
		if(LoadSceneLayer::TenTimesRecuriteResultLayer->getWidgetParent() != NULL)
		{
			LoadSceneLayer::TenTimesRecuriteResultLayer->removeFromParentAndCleanup(false);
		}
		//十连抽结果面板
		panel_tenTimesRecruit = LoadSceneLayer::TenTimesRecuriteResultLayer;
		panel_tenTimesRecruit->setAnchorPoint(ccp(0.0f,0.0f));
		panel_tenTimesRecruit->getValidNode()->setContentSize(CCSizeMake(722, 438));
		panel_tenTimesRecruit->setPosition(CCPointZero);
		panel_tenTimesRecruit->setTouchEnable(true);
		m_pUiLayer->addWidget(panel_tenTimesRecruit);

		panel_btn = (UIPanel*)UIHelper::seekWidgetByName(panel_tenTimesRecruit,"Panel_btn");
		panel_btn->setVisible(true);

		btn_close = (UIButton*)UIHelper::seekWidgetByName(panel_tenTimesRecruit,"Button_quit");
		btn_close->setVisible(false);
		btn_close->setTouchEnable(true);
		btn_close->setPressedActionEnabled(true);
		btn_close->addReleaseEvent(this, coco_releaseselector(TenTimesRecuriteResultUI::CloseEvent));

		//十连抽结果层
		Layer_tenTimesRecruit = UILayer::create();
		addChild(Layer_tenTimesRecruit);

// 		UIButton * Btn_close = (UIButton*)UIHelper::seekWidgetByName(panel_tenTimesRecruit,"Button_close");
// 		Btn_close->setTouchEnable(true);
// 		Btn_close->setPressedActionEnabled(true);
// 		Btn_close->addReleaseEvent(this, coco_releaseselector(TenTimesRecuriteResultUI::CloseEvent));
		//十连抽返回
		UIButton * Btn_tenTimes_back = (UIButton*)UIHelper::seekWidgetByName(panel_tenTimesRecruit,"Button_back");
		Btn_tenTimes_back->setTouchEnable(true);
		Btn_tenTimes_back->setPressedActionEnabled(true);
		Btn_tenTimes_back->addReleaseEvent(this, coco_releaseselector(TenTimesRecuriteResultUI::TenTimesBackEvent));
		//十连抽再来一次
		UIButton * Btn_tenTimes_again = (UIButton*)UIHelper::seekWidgetByName(panel_tenTimesRecruit,"Button_again");
		Btn_tenTimes_again->setTouchEnable(true);
		Btn_tenTimes_again->setPressedActionEnabled(true);
		Btn_tenTimes_again->addReleaseEvent(this, coco_releaseselector(TenTimesRecuriteResultUI::TenTimesAgainEvent));

		lable_tenTimes_gold = (UILabel*)UIHelper::seekWidgetByName(panel_tenTimesRecruit,"Label_IngotValue");

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(CCSizeMake(722,438));

		return true;
	}
	return false;
}

void TenTimesRecuriteResultUI::onEnter()
{
	UIScene::onEnter();
	//this->openAnim();
}

void TenTimesRecuriteResultUI::onExit()
{
	UIScene::onExit();
}

bool TenTimesRecuriteResultUI::ccTouchBegan(CCTouch *touch, CCEvent * pEvent)
{
	//return this->resignFirstResponder(touch,this,false);
	return true;
}

void TenTimesRecuriteResultUI::ccTouchEnded(CCTouch *touch, CCEvent * pEvent)
{
}

void TenTimesRecuriteResultUI::ccTouchCancelled(CCTouch *touch, CCEvent * pEvent)
{
}

void TenTimesRecuriteResultUI::ccTouchMoved(CCTouch *touch, CCEvent * pEvent)
{
}

void TenTimesRecuriteResultUI::CloseEvent(CCObject * pSender)
{
	//this->closeAnim();
	this->removeFromParent();
}


void TenTimesRecuriteResultUI::TenTimesBackEvent(CCObject *pSender)
{
	//this->closeAnim();
	this->removeFromParent();
}

void TenTimesRecuriteResultUI::TenTimesAgainEvent(CCObject *pSender)
{
	GeneralsUI::generalsRecuriteUI->TenTimesRecuritEvent(pSender);
}

void TenTimesRecuriteResultUI::RefreshTenTimesRecruitData( std::vector<CGeneralDetail *> temp )
{
	for (int i =0;i<GameView::getInstance()->actionDetailList.size();++i)
	{
		if (GameView::getInstance()->actionDetailList.at(i)->type() == TEN_TIMES)
		{
			char s[20];
			sprintf(s,"%d",GameView::getInstance()->actionDetailList.at(i)->gold());
			this->lable_tenTimes_gold->setText(s);
		}
	}


	for (int i = 0;i<10;++i)
	{
	 	if (Layer_tenTimesRecruit->getChildByTag(TenTimeRecruitResultBaseTag+i))
	 	{
	 		Layer_tenTimesRecruit->getChildByTag(TenTimeRecruitResultBaseTag+i)->removeFromParent();
	 	}
	}
	 
	for (int i = 0;i<temp.size();++i)
	{
//  		GeneralsHeadItemBase * generalsHeadItemBase = GeneralsHeadItemBase::create(temp.at(i)->modelid());
//  		generalsHeadItemBase->setAnchorPoint(ccp(0.5f,0.5f));
//  		generalsHeadItemBase->setTag(TenTimeRecruitResultBaseTag+i);
// 		generalsHeadItemBase->showSilhouetteEffect();
//  		Layer_tenTimesRecruit->addChild(generalsHeadItemBase);
// 
// 		CCActionInterval * action =(CCActionInterval *)CCSequence::create(
// 			CCTintTo::create(0.5f, 255,255,255),
// 			CCCallFunc::create(generalsHeadItemBase,callfunc_selector(GeneralsHeadItemBase::showBonusEffect)),
// 			NULL);
// 		generalsHeadItemBase->runAction(action);
// 
// 		ccBezierConfig bezier;
//  		if (i<5)
//  		{
//  			generalsHeadItemBase->setPosition(ccp(60 + i*121,233));
//  		}
//  		else
//  		{
//  			generalsHeadItemBase->setPosition(ccp(60 + (i%5)*121,84));
// 		}
// 
// 		if (generalsHeadItemBase->getLegendHead())
// 		{
// 			CCFiniteTimeAction * action = CCSequence::create(
// 				CCDelayTime::create(1.0f),
// 				CCTintTo::create(0.3f, 255,255,255),
// 				NULL);
// 			generalsHeadItemBase->getLegendHead()->runAction(action);
// 		}
// 		
// 		CCFiniteTimeAction * action1 = CCSequence::create(
// 			CCDelayTime::create(1.0f),
// 			CCTintTo::create(0.3f, 255,255,255),
// 			NULL);
// 		generalsHeadItemBase->label_name->runAction(action1);
// 
// 		CCFiniteTimeAction * action2 = CCSequence::create(
// 			CCDelayTime::create(1.0f),
// 			CCTintTo::create(0.3f, 255,255,255),
// 			NULL);
// 		generalsHeadItemBase->imageView_star->runAction(action2);
// 
// 		if (generalsHeadItemBase->getFiveStarAnm())
// 		{
// 			CCFiniteTimeAction * action3 = CCSequence::create(
// 				CCDelayTime::create(1.0f),
// 				CCTintTo::create(0.3f, 255,255,255),
// 				NULL);
// 			generalsHeadItemBase->getFiveStarAnm()->runAction(action3);
// 		}
// 
// 
		GeneralCardItem * generalsHeadItem = GeneralCardItem::create(temp.at(i));
		generalsHeadItem->setAnchorPoint(ccp(0.5f,0.5f));
		generalsHeadItem->setTag(TenTimeRecruitResultBaseTag+i);
		Layer_tenTimesRecruit->addChild(generalsHeadItem);
		if (i<5)
		{
			generalsHeadItem->setPosition(ccp(60 + i*121,233));
		}
		else
		{
			generalsHeadItem->setPosition(ccp(60 + (i%5)*121,84));
		}
// 
// 		UIImageView* imageGeneral_bg = UIImageView::create();
// 		imageGeneral_bg->setTexture("res_ui/generals_white.png");
// 		imageGeneral_bg->setAnchorPoint(ccp(0.5f,0.5f));
// 		imageGeneral_bg->setTag(TenTimeRecruitResultBaseTag+i);
// 		Layer_tenTimesRecruit->addWidget(imageGeneral_bg);
// 		if (i<5)
// 		{
// 			imageGeneral_bg->setPosition(ccp(60 + i*121,233));
// 		}
// 		else
// 		{
// 			imageGeneral_bg->setPosition(ccp(60 + (i%5)*121,84));
// 		}
// 
// 		//generalInfo
// 		CGeneralBaseMsg * generalBaseInfo  = GeneralsConfigData::s_generalsBaseMsgData[temp.at(i)->modelid()];
// 		UIImageView * imageGeneral_Icon = UIImageView::create();
// 		imageGeneral_Icon->setTexture(generalBaseInfo->get_half_photo().c_str());
// 		imageGeneral_Icon->setAnchorPoint(ccp(0.5f,0));
// 		imageGeneral_Icon->setPosition(ccp(0,63-imageGeneral_bg->getContentSize().height/2));
// 		imageGeneral_bg->addChild(imageGeneral_Icon);
// 
// 		std::string generalName_ = generalBaseInfo->name();
// 		UILabel * Label_generalName=UILabel::create();
// 		Label_generalName->setText(generalName_.c_str());
// 		Label_generalName->setAnchorPoint(ccp(0.5f,0.5f));
// 		Label_generalName->setPosition(ccp(0,-imageGeneral_bg->getContentSize().height/2+Label_generalName->getContentSize().height*3 - 5));
// 		Label_generalName->setFontSize(20);
// 		imageGeneral_bg->addChild(Label_generalName);
// 
// 		//rare
// 		std::string generalRare_ = RecuriteActionItem::getStarPathByNum(generalBaseInfo->rare());
// 		imageGeneral_Star = UIImageView::create();
// 		imageGeneral_Star->setTexture(generalRare_.c_str());
// 		imageGeneral_Star->setAnchorPoint(ccp(0.5f,0.5f));
// 		imageGeneral_Star->setPosition(ccp(imageGeneral_Star->getContentSize().width/2 - imageGeneral_bg->getContentSize().width/2+13,
// 			Label_generalName->getPosition().y+imageGeneral_Star->getContentSize().height));
// 		imageGeneral_Star->setScale(2.0f);
// 		imageGeneral_bg->addChild(imageGeneral_Star);
// 		imageGeneral_Star->setVisible(false);
// 
// 		//profession
// 		std::string generalProfess_ = getGeneralProfessionIconPath(generalBaseInfo->get_profession());
// 		imageGeneral_profession = UIImageView::create();
// 		imageGeneral_profession->setTexture(generalProfess_.c_str());
// 		imageGeneral_profession->setAnchorPoint(ccp(0.5f,0.5f));
// 		imageGeneral_profession->setPosition(ccp(imageGeneral_bg->getContentSize().width/2 - imageGeneral_profession->getContentSize().width/2 - 14,
// 			imageGeneral_Star->getPosition().y));
// 		imageGeneral_profession->setScale(2.0f);
// 		imageGeneral_bg->addChild(imageGeneral_profession);
// 		imageGeneral_profession->setVisible(false);
// 			
// 		//begin action
// 		imageGeneral_bg->setScale(1.0f);
// 		imageGeneral_bg->setColor(ccc3(0,0,0));
// 
// 		CCActionInterval * action =(CCActionInterval *)CCSequence::create(
// 			CCTintTo::create(0.5f, 255,255,255),
// 			CCCallFunc::create(this,callfunc_selector(RecruitGeneralCard::createParticleSys)),
// 			CCDelayTime::create(0.5f),
// 			CCCallFunc::create(this, callfunc_selector(RecruitGeneralCard::lightActions)),
// 			NULL);
// 		imageGeneral_bg->runAction(action);
// 
// 		//head imageView action
// 		imageGeneral_Icon->setColor(ccc3(0,0,0));
// 		CCFiniteTimeAction * head_action = CCSequence::create(
// 			CCDelayTime::create(1.0f),
// 			CCTintTo::create(0.3f, 255,255,255),
// 			NULL);
// 		imageGeneral_Icon->runAction(head_action);
// 		//name label action
// 		Label_generalName->setColor(ccc3(0,0,0));
// 		CCFiniteTimeAction * nameLabel_action = CCSequence::create(
// 			CCDelayTime::create(1.0f),
// 			CCTintTo::create(0.3f, 255,255,255),
// 			NULL);
// 		Label_generalName->runAction(nameLabel_action);

 	}
}

void TenTimesRecuriteResultUI::RefreshBtnPresent( bool isPresent )
{
	panel_btn->setVisible(isPresent);
	btn_close->setVisible(!isPresent);
}


/////////////////////////////////////////////////////////////////////////////////////////

#define kTag_head_anm 10
GeneralCardItem::GeneralCardItem(void)
{
}


GeneralCardItem::~GeneralCardItem(void)
{
}

GeneralCardItem * GeneralCardItem::create(CGeneralDetail * generalInfo)
{
	GeneralCardItem * recruitCard = new GeneralCardItem();
	if (recruitCard && recruitCard->init(generalInfo))
	{
		recruitCard->autorelease();
		return recruitCard;
	}
	CC_SAFE_DELETE(recruitCard);
	return NULL;
}

bool GeneralCardItem::init(CGeneralDetail * generalInfo)
{
	if (UIScene::init())
	{
		winSize =CCDirector::sharedDirector()->getVisibleSize();

		u_layer = UILayer::create();
		m_pUiLayer->addChild(u_layer);

		imageGeneral_bg = UIImageView::create();
		imageGeneral_bg->setTexture("res_ui/generals_white.png");
		imageGeneral_bg->setAnchorPoint(ccp(0.5f,0.5f));
		imageGeneral_bg->setPosition(ccp(imageGeneral_bg->getContentSize().width/2,imageGeneral_bg->getContentSize().height/2));
		u_layer->addWidget(imageGeneral_bg);

		//generalInfo
 		CGeneralBaseMsg * generalBaseInfo  = GeneralsConfigData::s_generalsBaseMsgData[generalInfo->modelid()];
// 		imageGeneral_icon = UIImageView::create();
// 		imageGeneral_icon->setTexture(generalBaseInfo->get_half_photo().c_str());
// 		imageGeneral_icon->setAnchorPoint(ccp(0.5f,0));
// 		imageGeneral_icon->setPosition(ccp(0,63-imageGeneral_bg->getContentSize().height/2));
// 		imageGeneral_bg->addChild(imageGeneral_icon);

		std::string animFileName = "res_ui/generals/";
		animFileName.append(generalBaseInfo->get_half_photo());
		animFileName.append(".anm");
		CCLegendAnimation* imageGeneral_icon = CCLegendAnimation::create(animFileName);
		if (imageGeneral_icon)
		{
			imageGeneral_icon->setPlayLoop(true);
			imageGeneral_icon->setReleaseWhenStop(false);
			imageGeneral_icon->setScale(.7f);	
			imageGeneral_icon->setPosition(ccp(-49,-imageGeneral_bg->getContentSize().height/2+26));
			imageGeneral_bg->addCCNode(imageGeneral_icon);
		}

		std::string generalName_ = generalBaseInfo->name();
		UILabel* Label_generalName=UILabel::create();
		Label_generalName->setText(generalName_.c_str());
		Label_generalName->setAnchorPoint(ccp(0.5f,0.5f));
		Label_generalName->setPosition(ccp(0,18-imageGeneral_bg->getContentSize().height/2));
		imageGeneral_bg->addChild(Label_generalName);

		//rare
		std::string generalRare_ = RecuriteActionItem::getStarPathByNum(generalBaseInfo->rare());
		imageGeneral_Star = UIImageView::create();
		imageGeneral_Star->setTexture(generalRare_.c_str());
		imageGeneral_Star->setAnchorPoint(ccp(0.5f,0.5f));
		imageGeneral_Star->setPosition(ccp(imageGeneral_Star->getContentSize().width/2 - imageGeneral_bg->getContentSize().width/2+1,
			Label_generalName->getPosition().y+imageGeneral_Star->getContentSize().height/2+3));
		imageGeneral_Star->setScale(0.8f);
		imageGeneral_bg->addChild(imageGeneral_Star);

		//profession
		std::string generalProfess_ = RecruitGeneralCard::getGeneralProfessionIconPath(generalBaseInfo->get_profession());
		imageGeneral_profession = UIImageView::create();
		imageGeneral_profession->setTexture(generalProfess_.c_str());
		imageGeneral_profession->setAnchorPoint(ccp(0.5f,0.5f));
		imageGeneral_profession->setPosition(ccp(imageGeneral_bg->getContentSize().width/2 - imageGeneral_profession->getContentSize().width/2-1 ,
			imageGeneral_Star->getPosition().y));
		imageGeneral_profession->setScale(0.8f);
		imageGeneral_bg->addChild(imageGeneral_profession);

		//begin action
		imageGeneral_bg->setScale(1.0f);
		imageGeneral_bg->setColor(ccc3(0,0,0));
		imageGeneral_Star->setScale(0.8f);
		imageGeneral_Star->setColor(ccc3(0,0,0));
		imageGeneral_profession->setScale(0.8f);
		imageGeneral_profession->setColor(ccc3(0,0,0));

		CCActionInterval * action =(CCActionInterval *)CCSequence::create(
			CCTintTo::create(0.5f, 255,255,255),
			CCCallFunc::create(this,callfunc_selector(GeneralCardItem::showBonusEffect)),
			NULL);
		imageGeneral_bg->runAction(action);

		//head imageView action
		imageGeneral_icon->setColor(ccc3(0,0,0));
		CCFiniteTimeAction * head_action = CCSequence::create(
			CCDelayTime::create(1.0f),
			CCTintTo::create(0.3f, 255,255,255),
			NULL);
		imageGeneral_icon->runAction(head_action);
		//name label action
		Label_generalName->setColor(ccc3(0,0,0));
		CCFiniteTimeAction * nameLabel_action = CCSequence::create(
			CCDelayTime::create(1.0f),
			CCTintTo::create(0.3f, 255,255,255),
			NULL);
		Label_generalName->runAction(nameLabel_action);
		//rare action
		imageGeneral_Star->setColor(ccc3(0,0,0));
		CCFiniteTimeAction * general_Star_action = CCSequence::create(
			CCDelayTime::create(1.0f),
			CCTintTo::create(0.3f, 255,255,255),
			NULL);
		imageGeneral_Star->runAction(general_Star_action);
		//profession action
		imageGeneral_profession->setColor(ccc3(0,0,0));
		CCFiniteTimeAction * general_profession_action = CCSequence::create(
			CCDelayTime::create(1.0f),
			CCTintTo::create(0.3f, 255,255,255),
			NULL);
		imageGeneral_profession->runAction(general_profession_action);

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(imageGeneral_bg->getContentSize());
		return true;
	}
	return false;
}

void GeneralCardItem::onEnter()
{
	UIScene::onEnter();
}

void GeneralCardItem::onExit()
{
	UIScene::onExit();
}

bool GeneralCardItem::ccTouchBegan(CCTouch *touch, CCEvent * pEvent)
{
	return false;
}

void GeneralCardItem::ccTouchEnded(CCTouch *touch, CCEvent * pEvent)
{
}

void GeneralCardItem::ccTouchCancelled(CCTouch *touch, CCEvent * pEvent)
{
}

void GeneralCardItem::ccTouchMoved(CCTouch *touch, CCEvent * pEvent)
{
}

void GeneralCardItem::generalStarActions()
{
	imageGeneral_Star->setVisible(true);
	CCActionInterval * action =(CCActionInterval *)CCSequence::create(
		CCScaleTo::create(0.2f,0.8f,0.8f),
		CCCallFunc::create(this, callfunc_selector(GeneralCardItem::generalProfessionActions)),
		NULL);

	imageGeneral_Star->runAction(action);
}

void GeneralCardItem::generalProfessionActions()
{
	imageGeneral_profession->setVisible(true);
	imageGeneral_profession->runAction(CCScaleTo::create(0.2f,0.8f,0.8f));
}

void GeneralCardItem::showBonusEffect()
{
	CCNodeRGBA* pNode = BonusSpecialEffect::create();
	pNode->setPosition(ccp(this->getContentSize().width/2, this->getContentSize().height/2+20));
	pNode->setScale(0.7f);
	addChild(pNode);
}

void GeneralCardItem::showStarAndProfession()
{
	this->generalStarActions();
}
