#include "GeneralsSkillListUI.h"
#include "../../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "../../../loadscene_state/LoadSceneState.h"
#include "../../../gamescene_state/skill/GameFightSkill.h"
#include "../GeneralsSkillsUI.h"
#include "../GeneralsUI.h"
#include "../../../GameView.h"
#include "../../../gamescene_state/GameSceneState.h"
#include "../../../gamescene_state/MainScene.h"
#include "../../../gamescene_state/role/GameActor.h"
#include "../GeneralsListUI.h"
#include "../../../messageclient/element/CGeneralBaseMsg.h"
#include "../../../messageclient/GameMessageProcessor.h"
#include "AppMacros.h"
#include "../GeneralsShortcutSlot.h"
#include "../../../ui/skillscene/SkillScene.h"
#include "../../../utils/StaticDataManager.h"
#include "../../../ui/extensions/ShowSystemInfo.h"
#include "../../../gamescene_state/role/MyPlayer.h"
#include "../../../messageclient/element/FolderInfo.h"

#define GENERALSSHORTCUTSLOT_TAG 785 


GeneralsSkillListUI::GeneralsSkillListUI():
curSkillId(""),
curSlotIndex(-1)
{
}


GeneralsSkillListUI::~GeneralsSkillListUI()
{
	std::vector<GameFightSkill*>::iterator iter;
	for (iter = DataSourceList.begin(); iter != DataSourceList.end(); ++iter)
	{
		delete *iter;
	}
	DataSourceList.clear();
}

GeneralsSkillListUI* GeneralsSkillListUI::create(GeneralsSkillsUI::SkillTpye skill_type,int index)
{
	GeneralsSkillListUI * generalsSkillListUI = new GeneralsSkillListUI();
	if (generalsSkillListUI && generalsSkillListUI->init(skill_type,index))
	{
		generalsSkillListUI->autorelease();
		return generalsSkillListUI;
	}
	CC_SAFE_DELETE(generalsSkillListUI);
	return NULL;
}

bool GeneralsSkillListUI::init(GeneralsSkillsUI::SkillTpye skill_type,int index)
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		//this->scheduleUpdate();

		curSlotIndex = index;
		UpdateDataByType(skill_type,index);

		if (DataSourceList.size()<=0)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("generals_skill_notStudied"));
			return false;
		}

		//加载UI
		if(LoadSceneLayer::GeneralsSkillListLayer->getWidgetParent() != NULL)
		{
			LoadSceneLayer::GeneralsSkillListLayer->removeFromParentAndCleanup(false);
		}
		UIPanel *ppanel = LoadSceneLayer::GeneralsSkillListLayer;
		ppanel->setAnchorPoint(ccp(0.0f,0.0f));
		ppanel->getValidNode()->setContentSize(CCSizeMake(703, 416));
		ppanel->setPosition(CCPointZero);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnable(true);
		m_pUiLayer->addWidget(ppanel);

		Layer_skills = UILayer::create();
		addChild(Layer_skills);

		UIButton * Button_close = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnable(true);
		Button_close->addReleaseEvent(this, coco_releaseselector(GeneralsSkillListUI::CloseEvent));
		Button_close->setPressedActionEnabled(true);

		UIButton * btn_equip = (UIButton *)UIHelper::seekWidgetByName(ppanel,"Button_set");
		btn_equip->setTouchEnable(true);
		btn_equip->setPressedActionEnabled(true);
		btn_equip->addReleaseEvent(this,coco_releaseselector(GeneralsSkillListUI::EquipEvent));

		UIButton* btn_null_skillUpLevel = (UIButton *)UIHelper::seekWidgetByName(ppanel,"Button_null_skillEquip");
		btn_null_skillUpLevel->setTouchEnable(true);
		btn_null_skillUpLevel->addReleaseEvent(this,coco_releaseselector(GeneralsSkillListUI::NullSkillDesEvent));


		//i_skillFrame = (UIImageView*)UIHelper::seekWidgetByName(ppanel,"ImageView_skill_di"); 
		//i_skillImage = (UIImageView*)UIHelper::seekWidgetByName(ppanel,"ImageView_skill"); 
		l_skillName = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_SkillName"); 
		l_skillType = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_SkillType"); 
		l_skillLvValue = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_LvValue"); 
		imageView_skillQuality = (UIImageView*)UIHelper::seekWidgetByName(ppanel,"ImageView_abc");
		imageView_skillQuality->setVisible(true);
		l_skillQualityValue = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_QualityValue");
		l_skillMpValue = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_MpValue"); 
		l_skillCDTimeValue = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_CDTimeValue"); 
		l_skillDistanceValue = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_DistanceValue"); 
		//t_skillDescription = (UITextArea*)UIHelper::seekWidgetByName(ppanel,"TextArea_SkillRarity");
		//t_skillNextDescription = (UITextArea*)UIHelper::seekWidgetByName(ppanel,"TextArea_NextSkillRarity");

		generalsSkillsList_tableView = CCTableView::create(this,CCSizeMake(230,356));
		generalsSkillsList_tableView ->setSelectedEnable(true);   // 支持选中状态的显示
		generalsSkillsList_tableView ->setSelectedScale9Texture("res_ui/highlight.png", CCRectMake(26, 26, 1, 1), ccp(0,-1)); 
		generalsSkillsList_tableView->setPressedActionEnabled(true);
		generalsSkillsList_tableView->setDirection(kCCScrollViewDirectionVertical);
		generalsSkillsList_tableView->setAnchorPoint(ccp(0,0));
		generalsSkillsList_tableView->setPosition(ccp(30,29));
		generalsSkillsList_tableView->setDelegate(this);
		generalsSkillsList_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		Layer_skills->addChild(generalsSkillsList_tableView);

		Layer_upper = UILayer::create();
		addChild(Layer_upper);

// 		UIImageView * tableView_kuang = UIImageView::create();
// 		tableView_kuang->setTexture("res_ui/LV5_dikuang_miaobian1.png");
// 		tableView_kuang->setScale9Enable(true);
// 		tableView_kuang->setScale9Size(CCSizeMake(243,374));
// 		tableView_kuang->setCapInsets(CCRectMake(30,30,1,1));
// 		tableView_kuang->setAnchorPoint(ccp(0,0));
// 		tableView_kuang->setPosition(ccp(19,21));
// 		Layer_upper->addWidget(tableView_kuang);

		if (DataSourceList.size()>0)
		{
			curSkillId = DataSourceList.at(0)->getId();
			RefreshSkillInfo(DataSourceList.at(0));
		}

		this->setContentSize(CCSizeMake(703,416));
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);

		return true;
	}
	return false;
}

void GeneralsSkillListUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void GeneralsSkillListUI::onExit()
{
	UIScene::onExit();
}

bool GeneralsSkillListUI::ccTouchBegan(CCTouch *touch, CCEvent * pEvent)
{
	return true;
}

void GeneralsSkillListUI::ccTouchEnded(CCTouch *touch, CCEvent * pEvent)
{
}

void GeneralsSkillListUI::ccTouchCancelled(CCTouch *touch, CCEvent * pEvent)
{
}

void GeneralsSkillListUI::ccTouchMoved(CCTouch *touch, CCEvent * pEvent)
{
}

void GeneralsSkillListUI::CloseEvent( CCObject * pSender )
{
	this->closeAnim();
}

void GeneralsSkillListUI::scrollViewDidScroll( CCScrollView* view )
{
}

void GeneralsSkillListUI::scrollViewDidZoom( CCScrollView* view )
{
}

void GeneralsSkillListUI::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{
	int i = cell->getIdx();
	curSkillId = DataSourceList.at(i)->getId();
	RefreshSkillInfo(DataSourceList.at(i));
}

cocos2d::CCSize GeneralsSkillListUI::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeMake(221,65);
}

cocos2d::extension::CCTableViewCell* GeneralsSkillListUI::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

		cell = new CCTableViewCell();
		cell->autorelease();

		CCScale9Sprite * sprite_bigFrame = CCScale9Sprite::create("res_ui/kuang01_new.png");
		sprite_bigFrame->setAnchorPoint(ccp(0, 0));
		sprite_bigFrame->setPosition(ccp(0, 0));
		sprite_bigFrame->setContentSize(CCSizeMake(220,63));
		sprite_bigFrame->setCapInsets(CCRectMake(15,31,1,1));
		cell->addChild(sprite_bigFrame);

		//小边框
// 		CCScale9Sprite *  smaillFrame = CCScale9Sprite::create("res_ui/LV4_allb.png");
// 		smaillFrame->setAnchorPoint(ccp(0, 0));
// 		smaillFrame->setPosition(ccp(18,5));
// 		smaillFrame->setCapInsets(CCRectMake(15,15,1,1));
// 		smaillFrame->setContentSize(CCSizeMake(52,52));
// 		cell->addChild(smaillFrame);
		CCSprite *  smaillFrame = CCSprite::create("res_ui/jinengdi.png");
		smaillFrame->setAnchorPoint(ccp(0, 0));
		smaillFrame->setPosition(ccp(19,5));
		smaillFrame->setScale(0.91f);
		cell->addChild(smaillFrame);

		CCScale9Sprite *sprite_nameFrame = CCScale9Sprite::create("res_ui/LV4_diaa.png");
		sprite_nameFrame->setAnchorPoint(ccp(0, 0));
		sprite_nameFrame->setPosition(ccp(83, 17));
		sprite_nameFrame->setCapInsets(CCRectMake(11,11,1,1));
		sprite_nameFrame->setContentSize(CCSizeMake(130,30));
		sprite_bigFrame->addChild(sprite_nameFrame);

		std::string skillIconPath = "res_ui/jineng_icon/";

		if (DataSourceList.at(idx)->getId() != "")
		{
// 			if (strcmp(DataSourceList.at(idx)->getId().c_str(),FightSkill_SKILL_ID_WARRIORDEFAULTATTACK) == 0
// 				||strcmp(DataSourceList.at(idx)->getId().c_str(),FightSkill_SKILL_ID_MAGICDEFAULTATTACK) == 0
// 				||strcmp(DataSourceList.at(idx)->getId().c_str(),FightSkill_SKILL_ID_RANGERDEFAULTATTACK) == 0
// 				||strcmp(DataSourceList.at(idx)->getId().c_str(),FightSkill_SKILL_ID_WARLOCKDEFAULTATTACK) == 0)
// 			{
// 				skillIconPath.append("jineng_2");
// 			}
// 			else
// 			{
				skillIconPath.append(DataSourceList.at(idx)->getCBaseSkill()->icon());
			//}

			skillIconPath.append(".png");
		}
		else
		{
			skillIconPath = "gamescene_state/zhujiemian3/jinengqu/suibian.png";
		}

		CCSprite *  sprite_image = CCSprite::create(skillIconPath.c_str());
		if(sprite_image != NULL)
		{
			sprite_image->setAnchorPoint(ccp(0.5f,0.5f));
			sprite_image->setPosition(ccp(45,31));
			cell->addChild(sprite_image);

			CCSprite *  sprite_lvFrame = CCSprite::create("res_ui/jineng/jineng_zhezhao.png");
			sprite_lvFrame->setAnchorPoint(ccp(0.5f,0.f));
			sprite_lvFrame->setPosition(ccp(45/2+1,0));
			sprite_image->addChild(sprite_lvFrame);

			std::string str_lv = "";
			char s_curLv [5];
			sprintf(s_curLv,"%d",DataSourceList.at(idx)->getLevel());
			str_lv.append(s_curLv);
			str_lv.append("/");
			char max_lv [5];
			sprintf(max_lv,"%d",DataSourceList.at(idx)->getCBaseSkill()->maxlevel());
			str_lv.append(max_lv);

			CCLabelTTF * l_lv = CCLabelTTF::create(str_lv.c_str(),APP_FONT_NAME,12);
			l_lv->setAnchorPoint(ccp(0.5f,0.f));
			l_lv->setPosition(ccp(sprite_lvFrame->getPosition().x,sprite_lvFrame->getPosition().y));
			sprite_lvFrame->addChild(l_lv);
		}

		//skill variety icon
		CCNode * sprite_skillVariety = SkillScene::GetSkillVarirtySprite(DataSourceList.at(idx)->getId().c_str());
		if (sprite_skillVariety)
		{
			sprite_skillVariety->setAnchorPoint(ccp(0.5f,0.5f));
			sprite_skillVariety->setPosition(ccp(60,29));
			sprite_skillVariety->setScale(.85f);
			cell->addChild(sprite_skillVariety);
		}

		CCLabelTTF * label_skillName = CCLabelTTF::create(DataSourceList.at(idx)->getCBaseSkill()->name().c_str(),APP_FONT_NAME,18);
		label_skillName->setAnchorPoint(ccp(0, 0.5f));
		label_skillName->setPosition(ccp(100,31));
// 		ccColor3B shadowColor = ccc3(0,0,0);   // black
// 		label_skillName->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		cell->addChild(label_skillName);

		if (DataSourceList.at(idx)->getCBaseSkill()->get_quality() == 1)
		{
			label_skillName->setColor(ccc3(255,255,255));
		}
		else if (DataSourceList.at(idx)->getCBaseSkill()->get_quality()  == 2)
		{
			label_skillName->setColor(ccc3(0,255,0));
		}
		else if (DataSourceList.at(idx)->getCBaseSkill()->get_quality()  == 3)
		{
			label_skillName->setColor(ccc3(0,255,255));
		}
		else if (DataSourceList.at(idx)->getCBaseSkill()->get_quality()  == 4)
		{
			label_skillName->setColor(ccc3(255,0,228));
		}
		else if (DataSourceList.at(idx)->getCBaseSkill()->get_quality()  == 5)
		{
			label_skillName->setColor(ccc3(254,124,0));
		}
		else
		{
			label_skillName->setColor(ccc3(255,255,255));
		}
	
	return cell;
}

unsigned int GeneralsSkillListUI::numberOfCellsInTableView( CCTableView *table )
{
	return  DataSourceList.size();
}

void GeneralsSkillListUI::EquipEvent( CCObject *pSender )
{
	ReqData * temp = new ReqData();
 	temp->generalsid = GeneralsUI::generalsListUI->getCurGeneralBaseMsg()->id();
	temp->skillid = curSkillId;
	temp->index = curSlotIndex;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5053,temp);
	delete temp;
}

void GeneralsSkillListUI::RefreshSkillInfo( GameFightSkill * gameFightSkill )
{
// 	if (Layer_skills->getChildByTag(GENERALSSHORTCUTSLOT_TAG))
// 	{
// 		Layer_skills->getChildByTag(GENERALSSHORTCUTSLOT_TAG)->removeFromParent();
// 	}
// 
// 	GeneralsShortcutSlot * generalsShortCutSlot = GeneralsShortcutSlot::create(gameFightSkill);
// 	generalsShortCutSlot->ignoreAnchorPointForPosition(false);
// 	generalsShortCutSlot->setAnchorPoint(ccp(0.5f,0.5f));
// 	generalsShortCutSlot->setPosition(ccp(341,346));
// 	generalsShortCutSlot->setTouchEnabled(false);
// 	Layer_skills->addChild(generalsShortCutSlot,0,GENERALSSHORTCUTSLOT_TAG);
// 
// 	l_skillName->setText(gameFightSkill->getCBaseSkill()->name().c_str());
// 	char s_level[5];
// 	sprintf(s_level,"%d",gameFightSkill->getCFightSkill()->level());
// 	l_skillLvValue->setText(s_level);
// 	l_skillQualityValue->setText("");
// 	char s_mp[20];
// 	sprintf(s_mp,"%d",gameFightSkill->getCFightSkill()->mp());
// 	l_skillMpValue->setText(s_mp);
// 	char s_intervaltime[5];
// 	sprintf(s_intervaltime,"%d",gameFightSkill->getCFightSkill()->intervaltime()/1000);
// 	l_skillCDTimeValue->setText(s_intervaltime);
// 	char s_distance[5];
// 	sprintf(s_distance,"%d",gameFightSkill->getCFightSkill()->distance());
// 	l_skillDistanceValue ->setText(s_distance);
// 	t_skillDescription->setText(gameFightSkill->getCFightSkill()->description().c_str());
// 	t_skillNextDescription->setText(gameFightSkill->getCFightSkill()->nextleveldescription().c_str());


	if (Layer_skills->getChildByTag(GENERALSSHORTCUTSLOT_TAG))
	{
		Layer_skills->getChildByTag(GENERALSSHORTCUTSLOT_TAG)->removeFromParent();
	}

	GeneralsShortcutSlot * generalsShortCutSlot = GeneralsShortcutSlot::create(gameFightSkill);
	generalsShortCutSlot->ignoreAnchorPointForPosition(false);
	generalsShortCutSlot->setAnchorPoint(ccp(0.5f,0.5f));
	generalsShortCutSlot->setPosition(ccp(342,336));
	generalsShortCutSlot->setTouchEnabled(false);
	Layer_skills->addChild(generalsShortCutSlot,0,GENERALSSHORTCUTSLOT_TAG);

	l_skillName->setText(gameFightSkill->getCBaseSkill()->name().c_str());

	//主动/被动
	if (gameFightSkill->getCBaseSkill()->usemodel() == 0)
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_zhudongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		l_skillType->setText(skillinfo_zhudongjineng);
	}
	else if (gameFightSkill->getCBaseSkill()->usemodel() == 2)
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_beidongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		l_skillType->setText(skillinfo_zhudongjineng);
	}
	else
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_zhudongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		l_skillType->setText(skillinfo_zhudongjineng);
	}

	char s_level[20];
	sprintf(s_level,"%d",gameFightSkill->getCFightSkill()->level());
	l_skillLvValue->setText(s_level);
	//l_skillQualityValue->setText("");
	switch(gameFightSkill->getCBaseSkill()->get_quality())
	{
	case 1:
		{
			imageView_skillQuality->setTexture("res_ui/o_white.png");
		}
		break;
	case 2:
		{
			imageView_skillQuality->setTexture("res_ui/o_green.png");
		}
		break;
	case 3:
		{
			imageView_skillQuality->setTexture("res_ui/o_blue.png");
		}
		break;
	case 4:
		{
			imageView_skillQuality->setTexture("res_ui/o_purple.png");
		}
		break;
	case 5:
		{
			imageView_skillQuality->setTexture("res_ui/o_orange.png");
		}
		break;
	default:
		imageView_skillQuality->setTexture("res_ui/o_white.png");
	}
	char s_mp[20];
	sprintf(s_mp,"%d",gameFightSkill->getCFightSkill()->mp());
	l_skillMpValue->setText(s_mp);
	char s_intervaltime[20];
	sprintf(s_intervaltime,"%d",gameFightSkill->getCFightSkill()->intervaltime()/1000);
	l_skillCDTimeValue->setText(s_intervaltime);
	char s_distance[20];
	sprintf(s_distance,"%d",gameFightSkill->getCFightSkill()->distance());
	l_skillDistanceValue ->setText(s_distance);
	//t_skillDescription->setText(gameFightSkill->getCFightSkill()->description().c_str());
	//t_skillNextDescription->setText(gameFightSkill->getCFightSkill()->get_next_description().c_str());
	this->showSkillDes(gameFightSkill->getCFightSkill()->description(),gameFightSkill->getCFightSkill()->get_next_description());

	if(Layer_skills->getWidgetByName("panel_levelInfo"))
	{
		Layer_skills->getWidgetByName("panel_levelInfo")->removeFromParent();
	}
	UIPanel * panel_levelInfo  = UIPanel::create();
	Layer_skills->addWidget(panel_levelInfo);
	panel_levelInfo->setName("panel_levelInfo");

	/////////////////////////////////////////////////////
	int m_required_level = 0;
	int m_required_point = 0;
	std::string m_str_pre_skill = "";
	std::string m_pre_skill_id = "";
	int m_pre_skill_level = 0;
	int m_required_gold= 0;
	int m_required_skillBookValue = 0;
	if (gameFightSkill->getLevel() == 0)
	{
		//等级
		if (gameFightSkill->getCFightSkill()->get_required_level()>0)
		{
			m_required_level = gameFightSkill->getCFightSkill()->get_required_level();
		}
		//技能点
		if (gameFightSkill->getCFightSkill()->get_required_point()>0)
		{
			m_required_point = gameFightSkill->getCFightSkill()->get_required_point();
		}
		//XXX技能
		if (gameFightSkill->getCFightSkill()->get_pre_skill() != "")
		{
			CBaseSkill * temp = StaticDataBaseSkill::s_baseSkillData[gameFightSkill->getCFightSkill()->get_pre_skill()];
			m_str_pre_skill.append(temp->name().c_str());
			m_pre_skill_id.append(temp->id());

			m_pre_skill_level = gameFightSkill->getCFightSkill()->get_pre_skill_level();
		}
		//金币
		if (gameFightSkill->getCFightSkill()->get_required_gold()>0)
		{
			m_required_gold = gameFightSkill->getCFightSkill()->get_required_gold();
		}
		//技能书
		if (gameFightSkill->getCFightSkill()->get_required_num()>0)
		{
			m_required_skillBookValue = gameFightSkill->getCFightSkill()->get_required_num();
		}
	}
	else
	{
		//等级
		if (gameFightSkill->getCFightSkill()->get_next_required_level()>0)
		{
			m_required_level = gameFightSkill->getCFightSkill()->get_next_required_level();
		}
		//技能点
		if (gameFightSkill->getCFightSkill()->get_next_required_point()>0)
		{
			m_required_point = gameFightSkill->getCFightSkill()->get_next_required_point();
		}
		//XXX技能
		if (gameFightSkill->getCFightSkill()->get_next_pre_skill() != "")
		{
			CBaseSkill * temp = StaticDataBaseSkill::s_baseSkillData[gameFightSkill->getCFightSkill()->get_next_pre_skill()];
			m_str_pre_skill.append(temp->name());
			m_pre_skill_id.append(temp->id());

			m_pre_skill_level = gameFightSkill->getCFightSkill()->get_next_pre_skill_level();
		}
		//金币
		if (gameFightSkill->getCFightSkill()->get_next_required_gold()>0)
		{
			m_required_gold = gameFightSkill->getCFightSkill()->get_next_required_gold();
		}
		//金技能书
		if (gameFightSkill->getCFightSkill()->get_next_required_num()>0)
		{
			m_required_skillBookValue = gameFightSkill->getCFightSkill()->get_next_required_num();
		}
	}

	/////////////////////////////////////////////////////
	int lineNum = 0;
	//主角等级
	if (m_required_level>0)
	{
		lineNum++;
		UILabel * l_playerLv = UILabel::create();
		l_playerLv->setText(StringDataManager::getString("skillinfo_zhujuedengji"));
		l_playerLv->setFontName(APP_FONT_NAME);
		l_playerLv->setFontSize(16);
		l_playerLv->setColor(ccc3(47,93,13));
		l_playerLv->setAnchorPoint(ccp(0,0));
		l_playerLv->setPosition(ccp(283,102-lineNum*20));//(330,118)
		l_playerLv->setName("l_playerLv");
		panel_levelInfo->addChild(l_playerLv);

		UILabel * l_playerLv_colon = UILabel::create();
		l_playerLv_colon->setText(StringDataManager::getString("skillinfo_maohao"));
		l_playerLv_colon->setFontName(APP_FONT_NAME);
		l_playerLv_colon->setFontSize(16);
		l_playerLv_colon->setColor(ccc3(47,93,13));
		l_playerLv_colon->setAnchorPoint(ccp(0,0));
		l_playerLv_colon->setPosition(ccp(347,102-lineNum*20));
		l_playerLv_colon->setName("l_playerLv_colon");
		panel_levelInfo->addChild(l_playerLv_colon);

		char s_next_required_level[20];
		sprintf(s_next_required_level,"%d",m_required_level);
		UILabel * l_playerLvValue = UILabel::create();
		l_playerLvValue->setText(s_next_required_level);
		l_playerLvValue->setFontName(APP_FONT_NAME);
		l_playerLvValue->setFontSize(16);
		l_playerLvValue->setColor(ccc3(35,93,13));
		l_playerLvValue->setAnchorPoint(ccp(0,0));
		l_playerLvValue->setPosition(ccp(356,102-lineNum*20));
		panel_levelInfo->addChild(l_playerLvValue);
		// 
		if (GameView::getInstance()->myplayer->getActiveRole()->level()<m_required_level)  //人物等级未达到技能升级要求
		{
			l_playerLv->setColor(ccc3(212,59,59));
			l_playerLv_colon->setColor(ccc3(212,59,59));
			l_playerLvValue->setColor(ccc3(212,59,59));
		}
	}
	//技能点
	if (m_required_point>0)
	{
		lineNum++;

		UIWidget * widget_skillPoint = UIWidget::create();
		panel_levelInfo->addChild(widget_skillPoint);
		widget_skillPoint->setName("widget_skillPoint");
		widget_skillPoint->setAnchorPoint(ccp(0,0));
		widget_skillPoint->setPosition(ccp(283,102-lineNum*20));

		std::string str_skillPoint = StringDataManager::getString("skillinfo_jinengdian");
		for(int i = 0;i<3;i++)
		{
			std::string str_name = "l_skillPoint";
			char s_index[5];
			sprintf(s_index,"%d",i);
			str_name.append(s_index);
			UILabel * l_skillPoint = UILabel::create();
			l_skillPoint->setText(str_skillPoint.substr(i*3,3).c_str());
			l_skillPoint->setFontName(APP_FONT_NAME);
			l_skillPoint->setFontSize(16);
			l_skillPoint->setColor(ccc3(47,93,13));
			l_skillPoint->setAnchorPoint(ccp(0,0));
			l_skillPoint->setName(str_name.c_str());
			widget_skillPoint->addChild(l_skillPoint);
			l_skillPoint->setPosition(ccp(24*i,0));
		}

		UILabel * l_skillPoint_colon = UILabel::create();
		l_skillPoint_colon->setText(StringDataManager::getString("skillinfo_maohao"));
		l_skillPoint_colon->setFontName(APP_FONT_NAME);
		l_skillPoint_colon->setFontSize(16);
		l_skillPoint_colon->setColor(ccc3(47,93,13));
		l_skillPoint_colon->setAnchorPoint(ccp(0,0));
		l_skillPoint_colon->setPosition(ccp(347,102-lineNum*20));
		l_skillPoint_colon->setName("l_skillPoint_colon");
		panel_levelInfo->addChild(l_skillPoint_colon);

		char s_required_point[20];
		sprintf(s_required_point,"%d",m_required_point);
		UILabel * l_skillPointValue = UILabel::create();
		l_skillPointValue->setText(s_required_point);
		l_skillPointValue->setFontName(APP_FONT_NAME);
		l_skillPointValue->setFontSize(16);
		l_skillPointValue->setColor(ccc3(47,93,13));
		l_skillPointValue->setAnchorPoint(ccp(0,0));
		l_skillPointValue->setPosition(ccp(356,102-lineNum*20));
		panel_levelInfo->addChild(l_skillPointValue);

		if(GameView::getInstance()->myplayer->player->skillpoint()<m_required_point)//技能点不足
		{
			for(int i = 0;i<3;i++)
			{
				std::string str_name = "l_skillPoint";
				char s_index[5];
				sprintf(s_index,"%d",i);
				str_name.append(s_index);
				UILabel * l_skillPoint = (UILabel*)widget_skillPoint->getChildByName(str_name.c_str());
				if (l_skillPoint)
				{
					l_skillPoint->setColor(ccc3(212,59,59));
				}
			}
			l_skillPoint_colon->setColor(ccc3(212,59,59));
			l_skillPointValue->setColor(ccc3(212,59,59));
		}
	}
	//XXX技能
	if (m_str_pre_skill != "")
	{
		lineNum++;
		UIWidget * widget_pre_skill = UIWidget::create();
		panel_levelInfo->addChild(widget_pre_skill);
		widget_pre_skill->setName("widget_pre_skill");
		widget_pre_skill->setAnchorPoint(ccp(0,0));
		widget_pre_skill->setPosition(ccp(283,102-lineNum*20));
		if (m_str_pre_skill.size()/3 < 4)
		{
			for(int i = 0;i<m_str_pre_skill.size()/3;i++)
			{
				std::string str_name = "l_preSkill";
				char s_index[5];
				sprintf(s_index,"%d",i);
				str_name.append(s_index);

				UILabel * l_preSkill = UILabel::create();
				l_preSkill->setText(m_str_pre_skill.substr(i*3,3).c_str());
				l_preSkill->setFontName(APP_FONT_NAME);
				l_preSkill->setFontSize(16);
				l_preSkill->setColor(ccc3(47,93,13));
				l_preSkill->setAnchorPoint(ccp(0,0));
				widget_pre_skill->addChild(l_preSkill);
				l_preSkill->setName(str_name.c_str());
				if (m_str_pre_skill.size()/3 == 1)
				{
					l_preSkill->setPosition(ccp(0,0));
				}
				else if (m_str_pre_skill.size()/3 == 2)
				{
					l_preSkill->setPosition(ccp(48*i,0));
				}
				else if (m_str_pre_skill.size()/3 == 3)
				{
					l_preSkill->setPosition(ccp(24*i,0));
				}
			}
		}
		else
		{
			UILabel * l_preSkill = UILabel::create();
			l_preSkill->setText(m_str_pre_skill.c_str());
			l_preSkill->setFontName(APP_FONT_NAME);
			l_preSkill->setFontSize(16);
			l_preSkill->setColor(ccc3(47,93,13));
			l_preSkill->setAnchorPoint(ccp(0,0));
			l_preSkill->setPosition(ccp(0,0));
			widget_pre_skill->addChild(l_preSkill);
			l_preSkill->setName("l_preSkill");
		}

		UILabel * l_preSkill_colon = UILabel::create();
		l_preSkill_colon->setText(StringDataManager::getString("skillinfo_maohao"));
		l_preSkill_colon->setFontName(APP_FONT_NAME);
		l_preSkill_colon->setFontSize(16);
		l_preSkill_colon->setColor(ccc3(47,93,13));
		l_preSkill_colon->setAnchorPoint(ccp(0,0));
		l_preSkill_colon->setPosition(ccp(347,102-lineNum*20));
		l_preSkill_colon->setName("l_preSkill_colon");
		panel_levelInfo->addChild(l_preSkill_colon);

		char s_pre_skill_level[20];
		sprintf(s_pre_skill_level,"%d",m_pre_skill_level);
		UILabel * l_preSkillValue = UILabel::create();
		l_preSkillValue->setText(s_pre_skill_level);
		l_preSkillValue->setFontName(APP_FONT_NAME);
		l_preSkillValue->setFontSize(16);
		l_preSkillValue->setColor(ccc3(47,93,13));
		l_preSkillValue->setAnchorPoint(ccp(0,0));
		l_preSkillValue->setPosition(ccp(356,102-lineNum*20));
		l_preSkillValue->setName("l_preSkillValue");
		panel_levelInfo->addChild(l_preSkillValue);

		std::map<std::string,GameFightSkill*>::const_iterator cIter;
		cIter = GameView::getInstance()->GameFightSkillList.find(m_pre_skill_id);
		if (cIter == GameView::getInstance()->GameFightSkillList.end()) // 没找到就是指向END了  
		{
		}
		else
		{
		 	GameFightSkill * gameFightSkillStuded = GameView::getInstance()->GameFightSkillList[m_pre_skill_id];
		 	if (gameFightSkillStuded)
		 	{
		 		if (gameFightSkillStuded->getLevel() < m_pre_skill_level)
		 		{
					if (m_str_pre_skill.size()/3 < 4)
					{
						for(int i = 0;i<m_str_pre_skill.size()/3;i++)
						{
							std::string str_name = "l_preSkill";
							char s_index[5];
							sprintf(s_index,"%d",i);
							str_name.append(s_index);

							UILabel * l_preSkill = (UILabel*)widget_pre_skill->getChildByName(str_name.c_str());
							if (l_preSkill)
							{
								l_preSkill->setColor(ccc3(212,59,59));
							}
						}
					}
					else
					{
						UILabel * l_preSkill = (UILabel*)widget_pre_skill->getChildByName("l_preSkill");
						if (l_preSkill)
						{
							l_preSkill->setColor(ccc3(212,59,59));
						}
					}
					l_preSkill_colon->setColor(ccc3(212,59,59));
		 			l_preSkillValue->setColor(ccc3(212,59,59));
		 		}
		 	}
		}
	}
	//金币
	if (m_required_gold>0)
	{
		lineNum++;

		UIWidget * widget_gold = UIWidget::create();
		panel_levelInfo->addChild(widget_gold);
		widget_gold->setPosition(ccp(283,102-lineNum*20));

		std::string str_gold = StringDataManager::getString("skillinfo_jinbi");
		for(int i = 0;i<2;i++)
		{
			std::string str_name = "l_gold";
			char s_index[5];
			sprintf(s_index,"%d",i);
			str_name.append(s_index);
			UILabel * l_gold = UILabel::create();
			l_gold->setText(str_gold.substr(i*3,3).c_str());
			l_gold->setFontName(APP_FONT_NAME);
			l_gold->setFontSize(16);
			l_gold->setColor(ccc3(47,93,13));
			l_gold->setAnchorPoint(ccp(0,0));
			l_gold->setName(str_name.c_str());
			widget_gold->addChild(l_gold);
			l_gold->setPosition(ccp(48*i,0));
		}

		UILabel * l_gold_colon = UILabel::create();
		l_gold_colon->setText(StringDataManager::getString("skillinfo_maohao"));
		l_gold_colon->setFontName(APP_FONT_NAME);
		l_gold_colon->setFontSize(16);
		l_gold_colon->setColor(ccc3(47,93,13));
		l_gold_colon->setAnchorPoint(ccp(0,0));
		l_gold_colon->setPosition(ccp(347,102-lineNum*20));
		l_gold_colon->setName("l_gold_colon");
		panel_levelInfo->addChild(l_gold_colon);

		char s_required_gold[20];
		sprintf(s_required_gold,"%d",m_required_gold);
		UILabel * l_goldValue = UILabel::create();
		l_goldValue->setText(s_required_gold);
		l_goldValue->setFontName(APP_FONT_NAME);
		l_goldValue->setFontSize(16);
		l_goldValue->setColor(ccc3(47,93,13));
		l_goldValue->setAnchorPoint(ccp(0,0));
		l_goldValue->setPosition(ccp(356,102-lineNum*20));
		panel_levelInfo->addChild(l_goldValue);

		if(GameView::getInstance()->getPlayerGold()<m_required_gold)//金币不足
		{
			for(int i = 0;i<2;i++)
			{
				std::string str_name = "l_gold";
				char s_index[5];
				sprintf(s_index,"%d",i);
				str_name.append(s_index);
				UILabel * l_gold = (UILabel*)widget_gold->getChildByName(str_name.c_str());
				if (l_gold)
				{
					l_gold->setColor(ccc3(212,59,59));
				}
			}
			l_gold_colon->setColor(ccc3(212,59,59));
			l_goldValue->setColor(ccc3(212,59,59));
		}
	}
	//技能书
	if (m_required_skillBookValue>0)
	{
		lineNum++;

		UIWidget * widget_skillBook = UIWidget::create();
		panel_levelInfo->addChild(widget_skillBook);
		widget_skillBook->setName("widget_skillBook");
		widget_skillBook->setPosition(ccp(283,102-lineNum*20));

		std::string str_skillbook = GeneralsSkillsUI::getSkillQualityStr(gameFightSkill->getCBaseSkill()->get_quality());
		str_skillbook.append(StringDataManager::getString("skillinfo_jinengshu"));
		UILabel * l_skillbook = UILabel::create();
		l_skillbook->setText(str_skillbook.c_str());
		l_skillbook->setFontName(APP_FONT_NAME);
		l_skillbook->setFontSize(16);
		l_skillbook->setColor(ccc3(47,93,13));
		l_skillbook->setAnchorPoint(ccp(0,0));
		l_skillbook->setName("l_skillbook");
		widget_skillBook->addChild(l_skillbook);
		l_skillbook->setPosition(ccp(0,0));

		UILabel * l_skillbook_colon = UILabel::create();
		l_skillbook_colon->setText(StringDataManager::getString("skillinfo_maohao"));
		l_skillbook_colon->setFontName(APP_FONT_NAME);
		l_skillbook_colon->setFontSize(16);
		l_skillbook_colon->setColor(ccc3(47,93,13));
		l_skillbook_colon->setAnchorPoint(ccp(0,0));
		l_skillbook_colon->setPosition(ccp(347,102-lineNum*20));
		l_skillbook_colon->setName("l_skillbook_colon");
		panel_levelInfo->addChild(l_skillbook_colon);

		char s_required_skillBook[20];
		sprintf(s_required_skillBook,"%d",m_required_skillBookValue);
		UILabel * l_skillBookValue = UILabel::create();
		l_skillBookValue->setText(s_required_skillBook);
		l_skillBookValue->setFontName(APP_FONT_NAME);
		l_skillBookValue->setFontSize(16);
		l_skillBookValue->setColor(ccc3(47,93,13));
		l_skillBookValue->setAnchorPoint(ccp(0,0));
		l_skillBookValue->setPosition(ccp(356,102-lineNum*20));
		panel_levelInfo->addChild(l_skillBookValue);

		int propNum = 0;
		for (int i = 0;i<GameView::getInstance()->AllPacItem.size();++i)
		{
			FolderInfo * folder = GameView::getInstance()->AllPacItem.at(i);
			if (!folder->has_goods())
				continue;

			if (folder->goods().id() == gameFightSkill->getCFightSkill()->get_required_prop())
			{
				propNum += folder->quantity();
			}
		}
		if(propNum<gameFightSkill->getCFightSkill()->get_required_num())//技能书不足
		{
			l_skillbook->setColor(ccc3(212,59,59));
			l_skillbook_colon->setColor(ccc3(212,59,59));
			l_skillBookValue->setColor(ccc3(212,59,59));
		}
	}
}

void GeneralsSkillListUI::UpdateDataByType( GeneralsSkillsUI::SkillTpye s_t,int index )
{
	std::vector<GameFightSkill*>::iterator iter;
	for (iter = DataSourceList.begin(); iter != DataSourceList.end(); ++iter)
	{
		delete *iter;
	}
	DataSourceList.clear();

	GeneralsUI * generalsui = (GeneralsUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsUI);

	if (s_t == GeneralsSkillsUI::Warrior)
	{
		std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GeneralsGameFightSkillList.begin();
		for ( ; it != GameView::getInstance()->GeneralsGameFightSkillList.end(); ++ it )
		{
			GameFightSkill * gameFightSkill = new GameFightSkill();
			gameFightSkill->initSkill(it->second->getId(),it->second->getId(),it->second->getLevel(), GameActor::type_player);

			if (gameFightSkill->getCBaseSkill()->get_profession() == 1)//猛将
			{
				if (index == 0)  
				{
					if(gameFightSkill->getCBaseSkill()->usemodel() == 0)//主动技能
					{
						DataSourceList.push_back(gameFightSkill);
					}
				}
				else
				{
					if(gameFightSkill->getCBaseSkill()->usemodel() == 2)//被动技能
					{
						DataSourceList.push_back(gameFightSkill);
					}
				}
			}
		}
	}
	else if(s_t == GeneralsSkillsUI::Magic)
	{
		std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GeneralsGameFightSkillList.begin();
		for ( ; it != GameView::getInstance()->GeneralsGameFightSkillList.end(); ++ it )
		{
			GameFightSkill * gameFightSkill = new GameFightSkill();
			gameFightSkill->initSkill(it->second->getId(), it->second->getId(),it->second->getLevel(), GameActor::type_player);

			if (gameFightSkill->getCBaseSkill()->get_profession() == 2)//鬼谋
			{
				if (index == 0)  
				{
					if(gameFightSkill->getCBaseSkill()->usemodel() == 0)//主动技能
					{
						DataSourceList.push_back(gameFightSkill);
					}
				}
				else
				{
					if(gameFightSkill->getCBaseSkill()->usemodel() == 2)//被动技能
					{
						DataSourceList.push_back(gameFightSkill);
					}
				}
			}
		}
	}
	else if (s_t == GeneralsSkillsUI::Ranger)
	{
		std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GeneralsGameFightSkillList.begin();
		for ( ; it != GameView::getInstance()->GeneralsGameFightSkillList.end(); ++ it )
		{
			GameFightSkill * gameFightSkill = new GameFightSkill();
			gameFightSkill->initSkill(it->second->getId(), it->second->getId(),it->second->getLevel(), GameActor::type_player);

			if (gameFightSkill->getCBaseSkill()->get_profession() == 3)//豪杰
			{
				if (index == 0)  
				{
					if(gameFightSkill->getCBaseSkill()->usemodel() == 0)//主动技能
					{
						DataSourceList.push_back(gameFightSkill);
					}
				}
				else
				{
					if(gameFightSkill->getCBaseSkill()->usemodel() == 2)//被动技能
					{
						DataSourceList.push_back(gameFightSkill);
					}
				}
			}
		}
	}
	else if(s_t == GeneralsSkillsUI::Warlock)
	{
		std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GeneralsGameFightSkillList.begin();
		for ( ; it != GameView::getInstance()->GeneralsGameFightSkillList.end(); ++ it )
		{
			GameFightSkill * gameFightSkill = new GameFightSkill();
			gameFightSkill->initSkill(it->second->getId(), it->second->getId(),it->second->getLevel(), GameActor::type_player);

			if (gameFightSkill->getCBaseSkill()->get_profession() == 4)//神射
			{
				if (index == 0)  
				{
					if(gameFightSkill->getCBaseSkill()->usemodel() == 0)//主动技能
					{
						DataSourceList.push_back(gameFightSkill);
					}
				}
				else
				{
					if(gameFightSkill->getCBaseSkill()->usemodel() == 2)//被动技能
					{
						DataSourceList.push_back(gameFightSkill);
					}
				}
			}
		}
	}
}

void GeneralsSkillListUI::showSkillDes( std::string curSkillDes,std::string nextSkillDes )
{
	//skill des text
	if (m_pUiLayer->getWidgetByTag(123456))
	{
		UIScrollView * scroll_ = (UIScrollView *)m_pUiLayer->getWidgetByTag(123456);
		scroll_->removeFromParentAndCleanup(true);
	}

	UIScrollView * scrollView_info = UIScrollView::create();
	scrollView_info->setTouchEnable(true);
	scrollView_info->setDirection(SCROLLVIEW_DIR_VERTICAL);
	scrollView_info->setSize(CCSizeMake(264,303));
	scrollView_info->setPosition(ccp(425,75));
	scrollView_info->setBounceEnabled(true);
	scrollView_info->setTag(123456);
	m_pUiLayer->addWidget(scrollView_info);

	ccColor3B color_ = ccc3(47,93,13);

	UIImageView * mo_1 = UIImageView::create();
	mo_1->setTexture("res_ui/mo_2.png");
	mo_1->setAnchorPoint(ccp(0,0));
	mo_1->setScale(0.7f);
	scrollView_info->addChild(mo_1);

	UILabelBMFont * labelBM_1 = UILabelBMFont::create();
	labelBM_1->setAnchorPoint(ccp(0,0));
	labelBM_1->setText(StringDataManager::getString("robotui_skill_des_setSkill"));
	labelBM_1->setFntFile("res_ui/font/ziti_3.fnt");
	scrollView_info->addChild(labelBM_1);

	UILabel * label_des=UILabel::create();
	label_des->setText(curSkillDes.c_str());
	label_des->setAnchorPoint(ccp(0,0));
	label_des->setFontSize(16);
	label_des->setColor(color_);
	label_des->setTextAreaSize(CCSizeMake(240,0));
	scrollView_info->addChild(label_des);

	UIImageView * mo_2 = UIImageView::create();
	mo_2->setTexture("res_ui/mo_2.png");
	mo_2->setAnchorPoint(ccp(0,0));
	mo_2->setScale(0.7f);
	scrollView_info->addChild(mo_2);

	UILabelBMFont * labelBM_2 = UILabelBMFont::create();
	labelBM_2->setAnchorPoint(ccp(0,0));
	labelBM_2->setText(StringDataManager::getString("robotui_nextSkill_des_setSkill"));
	labelBM_2->setFntFile("res_ui/font/ziti_3.fnt");
	scrollView_info->addChild(labelBM_2);

	UILabel * label_next_des=UILabel::create();
	label_next_des->setText(nextSkillDes.c_str());
	label_next_des->setAnchorPoint(ccp(0,0));
	label_next_des->setFontSize(16);
	label_next_des->setColor(color_);
	label_next_des->setTextAreaSize(CCSizeMake(240,0));
	scrollView_info->addChild(label_next_des);

	int h_ = mo_1->getContentSize().height * 2;
	int temp_min_h = 100;

	if (label_des->getContentSize().height > temp_min_h)
	{
		h_+= label_des->getContentSize().height;
	}else
	{
		h_+= temp_min_h;
	}

	if (label_next_des->getContentSize().height > temp_min_h)
	{
		h_+= label_next_des->getContentSize().height;
	}else
	{
		h_+= temp_min_h;
	}


	int innerWidth = scrollView_info->getRect().size.width;
	int innerHeight = h_;
	if (h_ < scrollView_info->getRect().size.height)
	{
		innerHeight = scrollView_info->getRect().size.height;
	}

	scrollView_info->setInnerContainerSize(CCSizeMake(innerWidth,innerHeight));

	mo_1->setPosition(ccp(0,innerHeight - mo_1->getContentSize().height ));
	labelBM_1->setPosition(ccp(10,mo_1->getPosition().y+2));
	label_des->setPosition(ccp(0,mo_1->getPosition().y - label_des->getContentSize().height));

	mo_2->setPosition(ccp(0,label_des->getPosition().y - 30));
	labelBM_2->setPosition(ccp(10,mo_2->getPosition().y+2));
	label_next_des->setPosition(ccp(0,mo_2->getPosition().y - label_next_des->getContentSize().height));
}

void GeneralsSkillListUI::NullSkillDesEvent( CCObject * pSender )
{
	std::map<int,std::string>::const_iterator cIter;
	cIter = SystemInfoConfigData::s_systemInfoList.find(19);
	if (cIter == SystemInfoConfigData::s_systemInfoList.end())
	{
		return ;
	}
	else
	{
		if(GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesDetailInfo) == NULL)
		{
			CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
			ShowSystemInfo * showSystemInfo = ShowSystemInfo::create(SystemInfoConfigData::s_systemInfoList[19].c_str());
			GameView::getInstance()->getMainUIScene()->addChild(showSystemInfo,0,kTagStrategiesDetailInfo);
			showSystemInfo->ignoreAnchorPointForPosition(false);
			showSystemInfo->setAnchorPoint(ccp(0.5f, 0.5f));
			showSystemInfo->setPosition(ccp(winSize.width/2, winSize.height/2));
		}
	}
}
