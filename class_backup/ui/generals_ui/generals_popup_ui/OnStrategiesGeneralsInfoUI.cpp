#include "OnStrategiesGeneralsInfoUI.h"
#include "../../../loadscene_state/LoadSceneState.h"
#include "GameView.h"
#include "../TeachAndEvolutionCell.h"
#include "../GeneralsUI.h"
#include "../../../messageclient/GameMessageProcessor.h"
#include "../../../messageclient/element/CGeneralBaseMsg.h"


OnStrategiesGeneralsInfoUI::OnStrategiesGeneralsInfoUI():
selectItemIndex(-1),
curGeneralId(-1),
isHaveGeneral(false),
selectGeneralId(-1)
{
}


OnStrategiesGeneralsInfoUI::~OnStrategiesGeneralsInfoUI()
{
}

OnStrategiesGeneralsInfoUI * OnStrategiesGeneralsInfoUI::create(int positionIndex)
{
	OnStrategiesGeneralsInfoUI * onStrategiesGeneralsInfoUI = new OnStrategiesGeneralsInfoUI();
	if (onStrategiesGeneralsInfoUI && onStrategiesGeneralsInfoUI->init(positionIndex))
	{
		onStrategiesGeneralsInfoUI->autorelease();
		return onStrategiesGeneralsInfoUI;
	}
	CC_SAFE_DELETE(onStrategiesGeneralsInfoUI);
	return NULL;
}

OnStrategiesGeneralsInfoUI * OnStrategiesGeneralsInfoUI::create(int positionIndex,long long generalsID)
{
	OnStrategiesGeneralsInfoUI * onStrategiesGeneralsInfoUI = new OnStrategiesGeneralsInfoUI();
	if (onStrategiesGeneralsInfoUI && onStrategiesGeneralsInfoUI->init(positionIndex,generalsID))
	{
		onStrategiesGeneralsInfoUI->autorelease();
		return onStrategiesGeneralsInfoUI;
	}
	CC_SAFE_DELETE(onStrategiesGeneralsInfoUI);
	return NULL;
}

bool OnStrategiesGeneralsInfoUI::init(int positionIndex)
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		this->scheduleUpdate();

		selectItemIndex = positionIndex;
		isHaveGeneral = false;

		//加载UI
		if(LoadSceneLayer::OnStrategiesGeneralsInfoLayer->getWidgetParent() != NULL)
		{
			LoadSceneLayer::OnStrategiesGeneralsInfoLayer->removeFromParentAndCleanup(false);
		}

		UIPanel *ppanel = LoadSceneLayer::OnStrategiesGeneralsInfoLayer;
		ppanel->setAnchorPoint(ccp(0.0f,0.0f));
		ppanel->setPosition(CCPointZero);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnable(true);
		m_pUiLayer->addWidget(ppanel);
// 
// 		Button_relax = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_rest");
// 		Button_relax->setTouchEnable(true);
// 		Button_relax->addReleaseEvent(this, coco_releaseselector(OnStrategiesGeneralsInfoUI::RelaxEvent));
// 		Button_relax->setPressedActionEnabled(true);
// 
// 		if (isHaveGeneral)
// 		{
// 			Button_relax->setVisible(true);
// 		}
// 		else
// 		{
// 			Button_relax->setVisible(false);
// 		}

		//refresh data
		RefreshDataSource();

		//武将列表
		generalList_tableView = CCTableView::create(this,CCSizeMake(261,377));
// 		generalList_tableView ->setSelectedEnable(true);   // 支持选中状态的显示
// 		generalList_tableView ->setSelectedScale9Texture("res_ui/highlight.png", CCRectMake(26, 26, 1, 1), ccp(0,5)); 
// 		generalList_tableView->setPressedActionEnabled(true);
		generalList_tableView->setDirection(kCCScrollViewDirectionVertical);
		generalList_tableView->setAnchorPoint(ccp(0,0));
		generalList_tableView->setPosition(ccp(31,32));
		generalList_tableView->setContentOffset(ccp(0,0));
		generalList_tableView->setDelegate(this);
		generalList_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		addChild(generalList_tableView);

		this->setTouchEnabled(true);
		this->setContentSize(ppanel->getContentSize());
		this->setTouchMode(kCCTouchesOneByOne);

		return true;
	}
	return false;
}

bool OnStrategiesGeneralsInfoUI::init(int positionIndex,long long generalsID)
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		this->scheduleUpdate();

		selectItemIndex = positionIndex;
		isHaveGeneral = true;
		curGeneralId = generalsID;

		//加载UI
		if(LoadSceneLayer::OnStrategiesGeneralsInfoLayer->getWidgetParent() != NULL)
		{
			LoadSceneLayer::OnStrategiesGeneralsInfoLayer->removeFromParentAndCleanup(false);
		}

		UIPanel *ppanel = LoadSceneLayer::OnStrategiesGeneralsInfoLayer;
		ppanel->setAnchorPoint(ccp(0.0f,0.0f));
		ppanel->getValidNode()->setContentSize(CCSizeMake(318, 436));
		ppanel->setPosition(CCPointZero);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnable(true);
		m_pUiLayer->addWidget(ppanel);

// 		Button_relax = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_rest");
// 		Button_relax->setTouchEnable(true);
// 		Button_relax->addReleaseEvent(this, coco_releaseselector(OnStrategiesGeneralsInfoUI::RelaxEvent));
// 		Button_relax->setPressedActionEnabled(true);
// 
// 		if (isHaveGeneral)
// 		{
// 			Button_relax->setVisible(true);
// 		}
// 		else
// 		{
// 			Button_relax->setVisible(false);
// 		}

		//refresh data
		RefreshDataSource();
		//武将列表
		generalList_tableView = CCTableView::create(this,CCSizeMake(261,377));
		generalList_tableView->setDirection(kCCScrollViewDirectionVertical);
		generalList_tableView->setAnchorPoint(ccp(0,0));
		generalList_tableView->setPosition(ccp(32,32));
		generalList_tableView->setContentOffset(ccp(0,0));
		generalList_tableView->setDelegate(this);
		generalList_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		addChild(generalList_tableView);

		this->setTouchEnabled(true);
		this->setContentSize(CCSizeMake(318,436));
		this->setTouchMode(kCCTouchesOneByOne);

		return true;
	}
	return false;
}

void OnStrategiesGeneralsInfoUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void OnStrategiesGeneralsInfoUI::onExit()
{
	UIScene::onExit();
	std::vector<CGeneralBaseMsg *>::iterator iter_generalslistforfightway;
	for (iter_generalslistforfightway=generalsListForFightWay.begin();iter_generalslistforfightway !=generalsListForFightWay.end();++iter_generalslistforfightway)
	{
		delete *iter_generalslistforfightway;
	}
	generalsListForFightWay.clear();
}

bool OnStrategiesGeneralsInfoUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return resignFirstResponder(touch,this,false);
}

void OnStrategiesGeneralsInfoUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void OnStrategiesGeneralsInfoUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void OnStrategiesGeneralsInfoUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void OnStrategiesGeneralsInfoUI::RelaxEvent( CCObject * pSender )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5056,(void *)curGeneralId,(void *)0);

	this->closeAnim();
}

void OnStrategiesGeneralsInfoUI::scrollViewDidScroll( CCScrollView* view )
{

}

void OnStrategiesGeneralsInfoUI::scrollViewDidZoom( CCScrollView* view )
{

}

void OnStrategiesGeneralsInfoUI::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{
	int i = cell->getIdx();
	GeneralsListCell * tempCell = dynamic_cast<GeneralsListCell*>(cell);

	//替换
 	long long _id = generalsListForFightWay.at(i)->id();
 	GameMessageProcessor::sharedMsgProcessor()->sendReq(5073,(void *)_id,(void *)selectItemIndex);

	this->closeAnim();

// 	int i = cell->getIdx();
// 	if (generalsListStatus == HaveNone)
// 	{
// 		//替换
// 		long long _id = GameView::getInstance()->generalsListForFightWay.at(i)->id();
// 		GameMessageProcessor::sharedMsgProcessor()->sendReq(5073,(void *)_id,(void *)selectItemIndex);
// 	}
// 	else if (generalsListStatus == HaveLast)
// 	{
// 		if (i == 0)
// 		{
// 			//req for last
// 			GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
// 			temp->page = this->s_mCurPage-1;
// 			temp->pageSize = this->everyPageNum;
// 			temp->type = 4;
// 			GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
// 			delete temp;
// 		}
// 		else
// 		{
// 			//替换
// 			long long _id = GameView::getInstance()->generalsListForFightWay.at(i-1)->id();
// 			GameMessageProcessor::sharedMsgProcessor()->sendReq(5073,(void *)_id,(void *)selectItemIndex);
// 		}
// 	}
// 	else if (generalsListStatus == HaveNext)
// 	{
// 		if (i == GameView::getInstance()->generalsListForFightWay.size())
// 		{
// 			//req for next
// 			GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
// 			temp->page = this->s_mCurPage+1;
// 			temp->pageSize = this->everyPageNum;
// 			temp->type = 4;
// 			GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
// 			delete temp;
// 		}
// 		else
// 		{
// 			//替换
// 			long long _id = GameView::getInstance()->generalsListForFightWay.at(i)->id();
// 			GameMessageProcessor::sharedMsgProcessor()->sendReq(5073,(void *)_id,(void *)selectItemIndex);
// 		}
// 	}
// 	else if (generalsListStatus == HaveBoth)
// 	{
// 		if (i == 0)
// 		{
// 			//req for lase
// 			GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
// 			temp->page = this->s_mCurPage-1;
// 			temp->pageSize = this->everyPageNum;
// 			temp->type = 4;
// 			GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
// 			delete temp;
// 		}
// 		else if (i == GameView::getInstance()->generalsListForFightWay.size()+1)
// 		{
// 			//req for next
// 			GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
// 			temp->page = this->s_mCurPage+1;
// 			temp->pageSize = this->everyPageNum;
// 			temp->type = 4;
// 			GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
// 			delete temp;
// 		}
// 		else
// 		{
// 			//替换
// 			long long _id = GameView::getInstance()->generalsListForFightWay.at(i-1)->id();
// 			GameMessageProcessor::sharedMsgProcessor()->sendReq(5073,(void *)_id,(void *)selectItemIndex);
// 		}
// 	}

}

cocos2d::CCSize OnStrategiesGeneralsInfoUI::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeMake(260,72);
}

cocos2d::extension::CCTableViewCell* OnStrategiesGeneralsInfoUI::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell = TeachAndEvolutionCell::create(generalsListForFightWay.at(idx)); 
// 	if(idx == generalsListForFightWay.size()-3)
// 	{
// 		if (generalsListStatus == HaveNext || generalsListStatus == HaveBoth)
// 		{
// 			//req for next
// 			this->isReqNewly = false;
// 			GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
// 			temp->page = this->s_mCurPage+1;
// 			temp->pageSize = this->everyPageNum;
// 			temp->type = 4;
// 			GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
// 			delete temp;
// 		}
// 	}

	return cell;
}

unsigned int OnStrategiesGeneralsInfoUI::numberOfCellsInTableView( CCTableView *table )
{
	return generalsListForFightWay.size();
// 	if (generalsListStatus == HaveNone)
// 	{
// 		return GameView::getInstance()->generalsListForFightWay.size();
// 	}
// 	else if (generalsListStatus == HaveLast ||generalsListStatus == HaveNext)
// 	{
// 		return GameView::getInstance()->generalsListForFightWay.size()+1;
// 	}
// 	else if (generalsListStatus == HaveBoth)
// 	{
// 		return GameView::getInstance()->generalsListForFightWay.size()+2;
// 	}
// 	else
// 	{
// 		return 0;
// 	}
}


void OnStrategiesGeneralsInfoUI::RefreshGeneralsList()
{
	this->generalList_tableView->reloadData();
}

void OnStrategiesGeneralsInfoUI::RefreshGeneralsListWithOutChangeOffSet()
{
	CCPoint _s = generalList_tableView->getContentOffset();
	int _h = generalList_tableView->getContentSize().height + _s.y;
	generalList_tableView->reloadData();
	CCPoint temp = ccp(_s.x,_h - generalList_tableView->getContentSize().height);
	generalList_tableView->setContentOffset(temp); 
}

void OnStrategiesGeneralsInfoUI::RefreshDataSource()
{
	//delete old
	std::vector<CGeneralBaseMsg *>::iterator iter_generalslistforfightway;
	for (iter_generalslistforfightway=generalsListForFightWay.begin();iter_generalslistforfightway !=generalsListForFightWay.end();++iter_generalslistforfightway)
	{
		delete *iter_generalslistforfightway;
	}
	generalsListForFightWay.clear();
	//add new 
	for(int i = 0;i<GameView::getInstance()->generalsInLineList.size();++i)
	{
		if (curGeneralId == -1)
		{
			CGeneralBaseMsg * temp = new CGeneralBaseMsg();
			temp->CopyFrom(*GameView::getInstance()->generalsInLineList.at(i));
			generalsListForFightWay.push_back(temp);
		}
		else
		{
			if (curGeneralId != GameView::getInstance()->generalsInLineList.at(i)->id())
			{
				CGeneralBaseMsg * temp = new CGeneralBaseMsg();
				temp->CopyFrom(*GameView::getInstance()->generalsInLineList.at(i));
				generalsListForFightWay.push_back(temp);
			}
		}
	}
}
