#include "GeneralsFateInfoUI.h"
#include "../../../loadscene_state/LoadSceneState.h"
#include "../../../messageclient/element/CGeneralDetail.h"
#include "../GeneralsHeadItemBase.h"
#include "../../../utils/StaticDataManager.h"
#include "../../../messageclient/element/CGeneralBaseMsg.h"
#include "../../../messageclient/element/CFateBaseMsg.h"
#include "AppMacros.h"
#include "../NormalGeneralsHeadItem.h"

GeneralsFateInfoUI::GeneralsFateInfoUI()
{
}


GeneralsFateInfoUI::~GeneralsFateInfoUI()
{
}

GeneralsFateInfoUI * GeneralsFateInfoUI::create(CGeneralDetail * generalsDetail)
{
	GeneralsFateInfoUI * generalsFateInfoUI = new GeneralsFateInfoUI();
	if (generalsFateInfoUI && generalsFateInfoUI->init(generalsDetail))
	{
		generalsFateInfoUI->autorelease();
		return generalsFateInfoUI;
	}
	CC_SAFE_DELETE(generalsFateInfoUI);
	return NULL;
}

bool GeneralsFateInfoUI::init(CGeneralDetail * generalsDetail)
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		this->scheduleUpdate();

		if (generalsDetail->generalid() == 0)
			return true;

		//加载UI
		if(LoadSceneLayer::GeneralsFateInfoLayer->getWidgetParent() != NULL)
		{
			LoadSceneLayer::GeneralsFateInfoLayer->removeFromParentAndCleanup(false);
		}
		ppanel = LoadSceneLayer::GeneralsFateInfoLayer;

		ppanel->setAnchorPoint(ccp(0.0f,0.0f));
		ppanel->getValidNode()->setContentSize(CCSizeMake(389, 420));
		ppanel->setPosition(CCPointZero);
		ppanel->setTouchEnable(true);
		m_pUiLayer->addWidget(ppanel);

		UIButton * btn_close = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		btn_close->setTouchEnable( true );
		btn_close->setPressedActionEnabled(true);
		btn_close->addReleaseEvent(this,coco_releaseselector(GeneralsFateInfoUI::CloseEvent));

		//从数据库读武将介绍，头像，半身像等数据
		CGeneralBaseMsg * generalBaseMsgFromDb = GeneralsConfigData::s_generalsBaseMsgData[generalsDetail->modelid()];

		l_fateName_1 = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_super_skill_1");
		l_fateName_1->setText("");
		l_fateName_2 = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_super_skill_2");
		l_fateName_2->setText("");
		l_fateName_3 = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_super_skill_3");
		l_fateName_3->setText("");
		l_fateName_4 = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_super_skill_4");
		l_fateName_4->setText("");
		l_fateName_5 = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_super_skill_5");
		l_fateName_5->setText("");

// 		t_fateDes_1 = (UITextArea *)UIHelper::seekWidgetByName(l_fateName_1,"TextArea_SkillContent");
// 		t_fateDes_1->setText("");
// 		t_fateDes_2 = (UITextArea *)UIHelper::seekWidgetByName(l_fateName_2,"TextArea_SkillContent");
// 		t_fateDes_2->setText("");
// 		t_fateDes_3 = (UITextArea *)UIHelper::seekWidgetByName(l_fateName_3,"TextArea_SkillContent");
// 		t_fateDes_3->setText("");
// 		t_fateDes_4 = (UITextArea *)UIHelper::seekWidgetByName(l_fateName_4,"TextArea_SkillContent");
// 		t_fateDes_4->setText("");
// 		t_fateDes_5 = (UITextArea *)UIHelper::seekWidgetByName(l_fateName_5,"TextArea_SkillContent");
// 		t_fateDes_5->setText("");
		l_fateDes_1 = CCLabelTTF::create("",APP_FONT_NAME,15,CCSizeMake(229,0),kCCTextAlignmentLeft,kCCVerticalTextAlignmentTop);
		l_fateDes_1->setAnchorPoint(ccp(0,1.0f));
		l_fateDes_1->setPosition(ccp(121,222));
		l_fateDes_1->setColor(ccc3(47,93,13));
		addChild(l_fateDes_1);
		l_fateDes_2 = CCLabelTTF::create("",APP_FONT_NAME,15,CCSizeMake(229,0),kCCTextAlignmentLeft,kCCVerticalTextAlignmentTop);
		l_fateDes_2->setAnchorPoint(ccp(0,1.0f));
		l_fateDes_2->setPosition(ccp(121,185));
		l_fateDes_2->setColor(ccc3(47,93,13));
		addChild(l_fateDes_2);
		l_fateDes_3 = CCLabelTTF::create("",APP_FONT_NAME,15,CCSizeMake(229,0),kCCTextAlignmentLeft,kCCVerticalTextAlignmentTop);
		l_fateDes_3->setAnchorPoint(ccp(0,1.0f));
		l_fateDes_3->setPosition(ccp(121,150));
		l_fateDes_3->setColor(ccc3(47,93,13));
		addChild(l_fateDes_3);
		l_fateDes_4 = CCLabelTTF::create("",APP_FONT_NAME,15,CCSizeMake(229,0),kCCTextAlignmentLeft,kCCVerticalTextAlignmentTop);
		l_fateDes_4->setAnchorPoint(ccp(0,1.0f));
		l_fateDes_4->setPosition(ccp(121,113));
		l_fateDes_4->setColor(ccc3(47,93,13));
		addChild(l_fateDes_4);
		l_fateDes_5 = CCLabelTTF::create("",APP_FONT_NAME,15,CCSizeMake(229,0),kCCTextAlignmentLeft,kCCVerticalTextAlignmentTop);
		l_fateDes_5->setAnchorPoint(ccp(0,1.0f));
		l_fateDes_5->setPosition(ccp(121,77));
		l_fateDes_5->setColor(ccc3(47,93,13));
		addChild(l_fateDes_5);

		RefreshFateInfo(generalsDetail);

		NormalGeneralsHeadItemBase * headItem = NormalGeneralsHeadItemBase::create(generalsDetail);
		headItem->setAnchorPoint(ccp(0,0));
		headItem->setPosition(ccp(30,245));
		addChild(headItem);

		CCLabelTTF * l_description = CCLabelTTF::create(generalBaseMsgFromDb->get_description().c_str(),APP_FONT_NAME,16,CCSizeMake(195,0),kCCTextAlignmentLeft);
		int height = l_description->getContentSize().height;
		ccColor3B shadowColor = ccc3(0,0,0);   // black
		l_description->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);

		CCScrollView *m_contentScrollView = CCScrollView::create(CCSizeMake(198,120));
		m_contentScrollView->setViewSize(CCSizeMake(198, 120));
		m_contentScrollView->ignoreAnchorPointForPosition(false);
		m_contentScrollView->setTouchEnabled(true);
		m_contentScrollView->setDirection(kCCScrollViewDirectionVertical);
		m_contentScrollView->setAnchorPoint(ccp(0,0));
		m_contentScrollView->setPosition(ccp(151,258));
		m_contentScrollView->setBounceable(true);
		m_contentScrollView->setClippingToBounds(true);
		m_pUiLayer->addChild(m_contentScrollView);
// 		UIScrollView * m_scrollView = UIScrollView::create();
// 		m_scrollView->setTouchEnable(true);
// 		m_scrollView->setDirection(SCROLLVIEW_DIR_VERTICAL);
// 		m_scrollView->setSize(CCSizeMake(204,125));
// 		m_scrollView->setPosition(ccp(133,244));
// 		m_pUiLayer->addWidget(m_scrollView);

		if (height > 120)
		{
			m_contentScrollView->setContentSize(ccp(198,height));
			m_contentScrollView->setContentOffset(ccp(0,120-height));
		}
		else
		{
			m_contentScrollView->setContentSize(ccp(198,120));
		}

		m_contentScrollView->addChild(l_description);
		l_description->ignoreAnchorPointForPosition(false);
		l_description->setAnchorPoint(ccp(0,1));
		l_description->setPosition(ccp(4,m_contentScrollView->getContentSize().height));


		this->setContentSize(CCSizeMake(389,420));
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);

		return true;
	}
	return false;
}

void GeneralsFateInfoUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void GeneralsFateInfoUI::onExit()
{
	UIScene::onExit();
}

bool GeneralsFateInfoUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return true;
}

void GeneralsFateInfoUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void GeneralsFateInfoUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void GeneralsFateInfoUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void GeneralsFateInfoUI::CloseEvent( CCObject * pSender )
{
	this->closeAnim();
}

void GeneralsFateInfoUI::RefreshFateInfo( CGeneralDetail * generalsDetail )
{
	if (generalsDetail->fates_size()>0)
	{
		for (int i = 0;i<generalsDetail->fates_size();++i)
		{
			CFateBaseMsg * temp = FateBaseMsgConfigData::s_fateBaseMsg[generalsDetail->fates(i).fateid()];
			if (temp == NULL)
				return;

			if (i == 0)
			{
				l_fateName_1->setText(generalsDetail->fates(i).fatename().c_str());
				l_fateDes_1->setString(temp->get_description().c_str());
			}
			else if(i == 1)
			{
				l_fateName_2->setText(generalsDetail->fates(i).fatename().c_str());
				l_fateDes_2->setString(temp->get_description().c_str());
			}
			else if(i == 2)
			{
				l_fateName_3->setText(generalsDetail->fates(i).fatename().c_str());
				l_fateDes_3->setString(temp->get_description().c_str());
			}
			else if(i == 3)
			{
				l_fateName_4->setText(generalsDetail->fates(i).fatename().c_str());
				l_fateDes_4->setString(temp->get_description().c_str());
			}
			else if(i == 4)
			{
				l_fateName_5->setText(generalsDetail->fates(i).fatename().c_str());
				l_fateDes_5->setString(temp->get_description().c_str());
			}
			else if(i == 5)
			{
			}
		}
	}
}

