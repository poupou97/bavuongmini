#include "StrategiesUpgradeUI.h"
#include "../../../loadscene_state/LoadSceneState.h"
#include "../../../messageclient/element/CFightWayBase.h"
#include "../../../utils/StaticDataManager.h"
#include "../../../messageclient/element/CFightWayGrowingUp.h"
#include "../../../messageclient/GameMessageProcessor.h"
#include "GameView.h"
#include "../GeneralsUI.h"
#include "../GeneralsStrategiesUI.h"
#include "../../../legend_script/CCTutorialIndicator.h"
#include "../../../legend_script/Script.h"
#include "../../../legend_script/ScriptManager.h"
#include "../../../legend_script/CCTeachingGuide.h"
#include "../../../gamescene_state/MainScene.h"

#define  Max_Level 10

StrategiesUpgradeUI::StrategiesUpgradeUI()
{
}


StrategiesUpgradeUI::~StrategiesUpgradeUI()
{
	delete curFightWayBase;
}

StrategiesUpgradeUI * StrategiesUpgradeUI::create(int selectFightWayId)
{
	StrategiesUpgradeUI * strategiesUpgradeUI = new StrategiesUpgradeUI();
	if (strategiesUpgradeUI && strategiesUpgradeUI->init(selectFightWayId))
	{
		strategiesUpgradeUI->autorelease();
		return strategiesUpgradeUI;
	}
	CC_SAFE_DELETE(strategiesUpgradeUI);
	return NULL;
}

bool StrategiesUpgradeUI::init(int selectFightWayId)
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		this->scheduleUpdate(); 
		curFightWayBase = new CFightWayBase();		
		for (int i = 0;i<FightWayConfigData::s_fightWayBase.size();++i)
		{
			if (selectFightWayId == FightWayConfigData::s_fightWayBase.at(i)->fightwayid())
			{
				curFightWayBase->CopyFrom(*FightWayConfigData::s_fightWayBase.at(i));
				curFightWayBase->set_name(FightWayConfigData::s_fightWayBase.at(i)->get_name());
				curFightWayBase->set_kValue(FightWayConfigData::s_fightWayBase.at(i)->get_kValue());
				curFightWayBase->set_icon(FightWayConfigData::s_fightWayBase.at(i)->get_icon());
				curFightWayBase->set_description(FightWayConfigData::s_fightWayBase.at(i)->get_description());
				break;
			}
		}
		if (GeneralsUI::generalsStrategiesUI)
		{
			for (int i = 0;i<GeneralsUI::generalsStrategiesUI->generalsStrategiesList.size();++i)
			{
				if (selectFightWayId == GeneralsUI::generalsStrategiesUI->generalsStrategiesList.at(i)->fightwayid())
				{
					curFightWayBase->CopyFrom(*GeneralsUI::generalsStrategiesUI->generalsStrategiesList.at(i));
					break;
				}
			}
		}
		//����UI
		if(LoadSceneLayer::StrategiesUpgradeLayer->getWidgetParent() != NULL)
		{
			LoadSceneLayer::StrategiesUpgradeLayer->removeFromParentAndCleanup(false);
		}
		UIPanel *ppanel = LoadSceneLayer::StrategiesUpgradeLayer;
		ppanel->setAnchorPoint(ccp(0.0f,0.0f));
		ppanel->getValidNode()->setContentSize(CCSizeMake(350, 400));
		ppanel->setPosition(CCPointZero);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnable(true);
		m_pUiLayer->addWidget(ppanel);

		Button_close = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnable(true);
		Button_close->addReleaseEvent(this, coco_releaseselector(StrategiesUpgradeUI::CloseEvent));
		Button_close->setPressedActionEnabled(true);

		Button_upgrade = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_upgrade");
		Button_upgrade->setTouchEnable(true);
		Button_upgrade->addReleaseEvent(this, coco_releaseselector(StrategiesUpgradeUI::UpgradeEvent));
		Button_upgrade->setPressedActionEnabled(true);

		l_name = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_nameValue");
		l_level = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_lvValue");

		l_costGoldValue = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_ingot");
		l_costConsumeValue = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_consume_value");

		//��˫״̬��������Ϣ
		UITextArea * t_musouDes = (UITextArea*)UIHelper::seekWidgetByName(ppanel,"TextArea_consume");
		t_musouDes->setText(curFightWayBase->get_description().c_str());

		l_property1_l = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_CurProperty_1");
		l_property1_r = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_MaXProperty_1");
		l_property2_l = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_CurProperty_2");
		l_property2_r = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_MaxProperty_2");
		l_property3_l = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_CurProperty_3");
		l_property3_r = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_MaxProperty_3");
		l_property4_l = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_CurProperty_4");
		l_property4_r = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_MaxProperty_4");
		l_property5_l = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_CurProperty_5");
		l_property5_r = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_MaxProperty_5");
		
		RefreshPropertyInfo(curFightWayBase);

		l_upgrade = (UILabel *)UIHelper::seekWidgetByName(Button_upgrade,"Label_upgrade");
		RefreshButtonStatus();

		this->setContentSize(CCSizeMake(454,452));
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);

		this->setTouchPriority(-1);
		m_pUiLayer->setTouchPriority(-2);

		return true;
	}
	return false;
}

void StrategiesUpgradeUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void StrategiesUpgradeUI::onExit()
{
	UIScene::onExit();
}

bool StrategiesUpgradeUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return true;
}

void StrategiesUpgradeUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void StrategiesUpgradeUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void StrategiesUpgradeUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void StrategiesUpgradeUI::CloseEvent( CCObject *pSender )
{
	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(pSender);

	this->closeAnim();
}

void StrategiesUpgradeUI::UpgradeEvent( CCObject *pSender )
{
	if (curFightWayBase->level() >= Max_Level)
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("generals_strategies_maxLevel"));
	}
	else
	{
		CFightWayGrowingUp * nextFightWayGrowingUp;
		if (curFightWayBase->level()>0)
		{
			if (curFightWayBase->level() == 10)
			{
				nextFightWayGrowingUp = FightWayGrowingUpConfigData::s_fightWayGrowingUp[curFightWayBase->fightwayid()]->at(10);
			}
			else
			{
				nextFightWayGrowingUp = FightWayGrowingUpConfigData::s_fightWayGrowingUp[curFightWayBase->fightwayid()]->at(curFightWayBase->level()+1);
			}
		}
		else
		{
			nextFightWayGrowingUp = FightWayGrowingUpConfigData::s_fightWayGrowingUp[curFightWayBase->fightwayid()]->at(1);
		}

		GameMessageProcessor::sharedMsgProcessor()->sendReq(5074,(void *)curFightWayBase->fightwayid());

		Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if(sc != NULL)
			sc->endCommand(pSender);
	}
}

void StrategiesUpgradeUI::RefreshButtonStatus()
{
	//CCDictionary *strings = CCDictionary::createWithContentsOfFile("res_ui/font/strings.xml");
	if (curFightWayBase->level()>0)
	{
		//const char *str_upgrade  = ((CCString*)strings->objectForKey("generals_strategies_upgrade"))->m_sString.c_str();
		const char *str_upgrade = StringDataManager::getString("generals_strategies_upgrade");
		l_upgrade->setText(str_upgrade);
	}
	else
	{
		//const char *str_study  = ((CCString*)strings->objectForKey("generals_strategies_study"))->m_sString.c_str();
		const char *str_study = StringDataManager::getString("generals_strategies_study");
		l_upgrade->setText(str_study);
	}
}

void StrategiesUpgradeUI::RefreshPropertyInfo( CFightWayBase * fightWayBase )
{
	CFightWayGrowingUp * curFightWayGrowingUp;
	CFightWayGrowingUp * nextFightWayGrowingUp;
	if (fightWayBase->level()>0)
	{
		if (fightWayBase->level() == 10)
		{
			curFightWayGrowingUp = FightWayGrowingUpConfigData::s_fightWayGrowingUp[fightWayBase->fightwayid()]->at(fightWayBase->level());
			nextFightWayGrowingUp = FightWayGrowingUpConfigData::s_fightWayGrowingUp[fightWayBase->fightwayid()]->at(fightWayBase->level());
		}
		else
		{
			curFightWayGrowingUp = FightWayGrowingUpConfigData::s_fightWayGrowingUp[fightWayBase->fightwayid()]->at(fightWayBase->level());
			nextFightWayGrowingUp = FightWayGrowingUpConfigData::s_fightWayGrowingUp[fightWayBase->fightwayid()]->at(fightWayBase->level()+1);
		}
	}
	else
	{
		curFightWayGrowingUp = FightWayGrowingUpConfigData::s_fightWayGrowingUp[fightWayBase->fightwayid()]->at(1);
		nextFightWayGrowingUp = FightWayGrowingUpConfigData::s_fightWayGrowingUp[fightWayBase->fightwayid()]->at(1);
	}

	l_name->setText(fightWayBase->get_name().c_str());
	std::string str_lv = "LV";
	char s_lv[10];
	if (fightWayBase->level() <= 1)
	{
		sprintf(s_lv,"%d",1);
	}
	else
	{
		sprintf(s_lv,"%d",fightWayBase->level());
	}
	str_lv.append(s_lv);
	l_level->setText(str_lv.c_str());

	//CCDictionary *strings = CCDictionary::createWithContentsOfFile("res_ui/font/strings.xml");
	//1��λ���ԣ���
	std::string str_property1_l = "generals_strategies_property_";
	char s_grid1_property_l[10];
	sprintf(s_grid1_property_l,"%d",curFightWayGrowingUp->get_grid1_property());
	str_property1_l.append(s_grid1_property_l);
	//const char *shuxing_1_l  = ((CCString*)strings->objectForKey(str_property1_l))->m_sString.c_str();
	const char *shuxing_1_l = StringDataManager::getString(str_property1_l.c_str());
	std::string property1_l_value;
	property1_l_value.append(shuxing_1_l);
	property1_l_value.append("+");
	if (fightWayBase->level()>0)
	{
		char s_grid1_value[10];
		sprintf(s_grid1_value,"%.1f",curFightWayGrowingUp->get_grid1_value());
		property1_l_value.append(s_grid1_value);
	}
	else
	{
		property1_l_value.append("0");
	}
	property1_l_value.append("%");
	l_property1_l->setText(property1_l_value.c_str());
	//1��λ���ԣ��ң�
	std::string str_property1_r = "generals_strategies_property_";
	char s_grid1_property_r[10];
	sprintf(s_grid1_property_r,"%d",nextFightWayGrowingUp->get_grid1_property());
	str_property1_r.append(s_grid1_property_r);
	//const char *shuxing_1_r  = ((CCString*)strings->objectForKey(str_property1_r))->m_sString.c_str();
	const char *shuxing_1_r = StringDataManager::getString(str_property1_r.c_str());
	std::string property1_r_value;
	property1_r_value.append(shuxing_1_r);
	property1_r_value.append("+");

	char s_grid1_value_r[10];
	sprintf(s_grid1_value_r,"%.1f",nextFightWayGrowingUp->get_grid1_value());
	property1_r_value.append(s_grid1_value_r);

	property1_r_value.append("%");
	l_property1_r->setText(property1_r_value.c_str());


	//2��λ���ԣ���
	std::string str_property2_l = "generals_strategies_property_";
	char s_grid2_property_l[10];
	sprintf(s_grid2_property_l,"%d",curFightWayGrowingUp->get_grid2_property());
	str_property2_l.append(s_grid2_property_l);
	//const char *shuxing_2_l  = ((CCString*)strings->objectForKey(str_property2_l))->m_sString.c_str();
	const char *shuxing_2_l = StringDataManager::getString(str_property2_l.c_str());
	std::string property2_l_value;
	property2_l_value.append(shuxing_2_l);
	property2_l_value.append("+");
	if (fightWayBase->level()>0)
	{
		char s_grid2_value_l[10];
		sprintf(s_grid2_value_l,"%.1f",curFightWayGrowingUp->get_grid2_value());
		property2_l_value.append(s_grid2_value_l);
	}
	else
	{
		property2_l_value.append("0");
	}
	property2_l_value.append("%");
	l_property2_l->setText(property2_l_value.c_str());
	//2��λ���ԣ��ң�
	std::string str_property2_r = "generals_strategies_property_";
	char s_grid2_property_r[10];
	sprintf(s_grid2_property_r,"%d",nextFightWayGrowingUp->get_grid2_property());
	str_property2_r.append(s_grid2_property_r);
	//const char *shuxing_2_r  = ((CCString*)strings->objectForKey(str_property2_r))->m_sString.c_str();
	const char *shuxing_2_r = StringDataManager::getString(str_property2_r.c_str());
	std::string property2_r_value;
	property2_r_value.append(shuxing_2_r);
	property2_r_value.append("+");
	char s_grid2_value_r[10];
	sprintf(s_grid2_value_r,"%.1f",nextFightWayGrowingUp->get_grid2_value());
	property2_r_value.append(s_grid2_value_r);
	property2_r_value.append("%");
	l_property2_r->setText(property2_r_value.c_str());

	//3��λ���ԣ���
	std::string str_property3_l = "generals_strategies_property_";
	char s_grid3_property_l[10];
	sprintf(s_grid3_property_l,"%d",curFightWayGrowingUp->get_grid3_property());
	str_property3_l.append(s_grid3_property_l);
	//const char *shuxing_3_l  = ((CCString*)strings->objectForKey(str_property3_l))->m_sString.c_str();
	const char *shuxing_3_l = StringDataManager::getString(str_property3_l.c_str());
	std::string property3_l_value;
	property3_l_value.append(shuxing_3_l);
	property3_l_value.append("+");
	if (fightWayBase->level()>0)
	{
		char s_grid3_value_l[10];
		sprintf(s_grid3_value_l,"%.1f",curFightWayGrowingUp->get_grid3_value());
		property3_l_value.append(s_grid3_value_l);
	}
	else
	{
		property3_l_value.append("0");
	}
	property3_l_value.append("%");
	l_property3_l->setText(property3_l_value.c_str());
	//3��λ���ԣ��ң�
	std::string str_property3_r = "generals_strategies_property_";
	char s_grid3_property_r[10];
	sprintf(s_grid3_property_r,"%d",nextFightWayGrowingUp->get_grid3_property());
	str_property3_r.append(s_grid3_property_r);
	//const char *shuxing_3_r  = ((CCString*)strings->objectForKey(str_property3_r))->m_sString.c_str();
	const char *shuxing_3_r = StringDataManager::getString(str_property3_r.c_str());
	std::string property3_r_value;
	property3_r_value.append(shuxing_3_r);
	property3_r_value.append("+");
	char s_grid3_value[10];
	sprintf(s_grid3_value,"%.1f",nextFightWayGrowingUp->get_grid3_value());
	property3_r_value.append(s_grid3_value);
	property3_r_value.append("%");
	l_property3_r->setText(property3_r_value.c_str());


	//4��λ���ԣ���
	std::string str_property4_l = "generals_strategies_property_";
	char s_grid4_property_l[10];
	sprintf(s_grid4_property_l,"%d",curFightWayGrowingUp->get_grid4_property());
	str_property4_l.append(s_grid4_property_l);
	//const char *shuxing_4_l  = ((CCString*)strings->objectForKey(str_property4_l))->m_sString.c_str();
	const char *shuxing_4_l = StringDataManager::getString(str_property4_l.c_str());
	std::string property4_l_value;
	property4_l_value.append(shuxing_4_l);
	property4_l_value.append("+");
	if (fightWayBase->level()>0)
	{
		char s_grid4_value_l[10];
		sprintf(s_grid4_value_l,"%.1f",curFightWayGrowingUp->get_grid4_value());
		property4_l_value.append(s_grid4_value_l);
	}
	else
	{
		property4_l_value.append("0");
	}
	property4_l_value.append("%");
	l_property4_l->setText(property4_l_value.c_str());
	//4��λ���ԣ��ң�
	std::string str_property4_r = "generals_strategies_property_";
	char s_grid4_property_r[10];
	sprintf(s_grid4_property_r,"%d",nextFightWayGrowingUp->get_grid4_property());
	str_property4_r.append(s_grid4_property_r);
	//const char *shuxing_4_r  = ((CCString*)strings->objectForKey(str_property4_r))->m_sString.c_str();
	const char *shuxing_4_r = StringDataManager::getString(str_property4_r.c_str());
	std::string property4_r_value;
	property4_r_value.append(shuxing_4_r);
	property4_r_value.append("+");
	char s_grid4_value_r[10];
	sprintf(s_grid4_value_r,"%.1f",nextFightWayGrowingUp->get_grid4_value());
	property4_r_value.append(s_grid4_value_r);
	property4_r_value.append("%");
	l_property4_r->setText(property4_r_value.c_str());


	//5��λ���ԣ���
	std::string str_property5_l = "generals_strategies_property_";
	char s_grid5_property_l[10];
	sprintf(s_grid5_property_l,"%d",curFightWayGrowingUp->get_grid5_property());
	str_property5_l.append(s_grid5_property_l);
	//const char *shuxing_5_l  = ((CCString*)strings->objectForKey(str_property5_l))->m_sString.c_str();
	const char *shuxing_5_l = StringDataManager::getString(str_property5_l.c_str());
	std::string property5_l_value;
	property5_l_value.append(shuxing_5_l);
	property5_l_value.append("+");
	if (fightWayBase->level()>0)
	{
		char s_grid5_value_l[10];
		sprintf(s_grid5_value_l,"%.1f",curFightWayGrowingUp->get_grid5_value());
		property5_l_value.append(s_grid5_value_l);
	}
	else
	{
		property5_l_value.append("0");
	}
	property5_l_value.append("%");
	l_property5_l->setText(property5_l_value.c_str());
	//5��λ���ԣ��ң�
	std::string str_property5_r = "generals_strategies_property_";
	char s_grid5_property_r[10];
	sprintf(s_grid5_property_r,"%d",nextFightWayGrowingUp->get_grid5_property());
	str_property5_r.append(s_grid5_property_r);
	//const char *shuxing_5_r  = ((CCString*)strings->objectForKey(str_property5_r))->m_sString.c_str();
	const char *shuxing_5_r = StringDataManager::getString(str_property5_r.c_str());
	std::string property5_r_value;
	property5_r_value.append(shuxing_5_r);
	property5_r_value.append("+");
	char s_grid5_value_r[10];
	sprintf(s_grid5_value_r,"%.1f",nextFightWayGrowingUp->get_grid5_value());
	property5_r_value.append(s_grid5_value_r);
	property5_r_value.append("%");
	l_property5_r->setText(property5_r_value.c_str());

	//���ĵĽ�Һ�����
	char s_required_experience[20];
	sprintf(s_required_experience,"%d",nextFightWayGrowingUp->get_required_experience());
	l_costConsumeValue->setText(s_required_experience);
	char s_required_gold[20];
	sprintf(s_required_gold,"%d",nextFightWayGrowingUp->get_required_gold());
	l_costGoldValue->setText(s_required_gold);
}

void StrategiesUpgradeUI::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void StrategiesUpgradeUI::addCCTutorialIndicator1( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,70,60);
// 	tutorialIndicator->setPosition(ccp(pos.x-40,pos.y+95));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);
	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,100,43,true);
	tutorialIndicator->setDrawNodePos(ccp(pos.x+174+_w,pos.y+14+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void StrategiesUpgradeUI::addCCTutorialIndicator2( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,20,40);
// 	tutorialIndicator->setPosition(ccp(pos.x-60,pos.y-75));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);
	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,40,40,true);
	tutorialIndicator->setDrawNodePos(ccp(pos.x+174+_w,pos.y+14+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void StrategiesUpgradeUI::removeCCTutorialIndicator()
{
	CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		CCTeachingGuide* teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}
