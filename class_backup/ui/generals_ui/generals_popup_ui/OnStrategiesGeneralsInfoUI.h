
#ifndef _GENERALSPOPUPUI_ONSTRATEGIESGENERALSINFOUI_H_
#define _GENERALSPOPUPUI_ONSTRATEGIESGENERALSINFOUI_H_

#include "../../extensions/UIScene.h"
#include "../GeneralsListBase.h"

/////////////////////////////////
/**
 * 替换上阵时的武将列表类
 * @author yangjun
 * @version 0.1.0
 * @date 2013.12.16
 */
class CGeneralBaseMsg;

class OnStrategiesGeneralsInfoUI : public UIScene,public CCTableViewDelegate,public CCTableViewDataSource
{
public:
	OnStrategiesGeneralsInfoUI();
	~OnStrategiesGeneralsInfoUI();

	static OnStrategiesGeneralsInfoUI * create(int positionIndex);
	bool init(int positionIndex);
	
	static OnStrategiesGeneralsInfoUI * create(int positionIndex,long long generalsID);
	bool init(int positionIndex,long long generalsID);

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);

	void RelaxEvent(CCObject * pSender);

	//刷新列表数据
	void RefreshDataSource();
	//刷新武将列表
	void RefreshGeneralsList();
	//刷新武将列表(tableView 的偏移量不变)
	void RefreshGeneralsListWithOutChangeOffSet();

public:
	// 阵法弹出界面列表信息
	std::vector<CGeneralBaseMsg * > generalsListForFightWay;

	int selectItemIndex ;
	long long curGeneralId;
	//该位置是否有武将
	bool isHaveGeneral;
	//选中的武将ID
	long long selectGeneralId;

	CCTableView * generalList_tableView;

	UIButton * Button_relax;
};

#endif
