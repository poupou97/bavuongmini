#include "GeneralsSkillInfoUI.h"
#include "../../../loadscene_state/LoadSceneState.h"
#include "GameView.h"
#include "../../../gamescene_state/MainScene.h"
#include "GeneralsSkillListUI.h"
#include "../../../messageclient/element/CBaseSkill.h"
#include "../../../messageclient/element/CFightSkill.h"
#include "../../../gamescene_state/skill/GameFightSkill.h"
#include "../../../messageclient/GameMessageProcessor.h"
#include "../../../utils/StaticDataManager.h"
#include "../../../gamescene_state/role/MyPlayer.h"
#include "../../skillscene/SkillScene.h"
#include "../GeneralsUI.h"
#include "../GeneralsListUI.h"
#include "../../../messageclient/element/CGeneralBaseMsg.h"
#include "../../../legend_script/CCTutorialParticle.h"
#include "../../../legend_script/CCTutorialIndicator.h"
#include "../../../legend_script/ScriptManager.h"
#include "../../../legend_script/Script.h"
#include "AppMacros.h"
#include "../../../messageclient/element/FolderInfo.h"
#include "../GeneralsSkillsUI.h"

#define  kTag_ScrollView 30

GeneralsSkillInfoUI::GeneralsSkillInfoUI():
skillSlotIndex(-1),
curSkillId("")
{
}


GeneralsSkillInfoUI::~GeneralsSkillInfoUI()
{
}

GeneralsSkillInfoUI * GeneralsSkillInfoUI::create(GameFightSkill * gameFightSkill,int slotIndex)
{
	GeneralsSkillInfoUI * generalsSkillInfoUI = new GeneralsSkillInfoUI();
	if (generalsSkillInfoUI && generalsSkillInfoUI->init(gameFightSkill,slotIndex))
	{
		generalsSkillInfoUI->autorelease();
		return generalsSkillInfoUI;
	}
	CC_SAFE_DELETE(generalsSkillInfoUI);
	return NULL;
}

bool GeneralsSkillInfoUI::init(GameFightSkill * gameFightSkill,int slotIndex)
{
	if (UIScene::init())
	{
		skillSlotIndex = slotIndex;
		curSkillId = gameFightSkill->getId();

		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		this->scheduleUpdate();
		//加载UI
		if(LoadSceneLayer::GeneralsSkillInfoLayer->getWidgetParent() != NULL)
		{
			LoadSceneLayer::GeneralsSkillInfoLayer->removeFromParentAndCleanup(false);
		}
		ppanel = LoadSceneLayer::GeneralsSkillInfoLayer;

		ppanel->setAnchorPoint(ccp(0.0f,0.0f));
		ppanel->getValidNode()->setContentSize(CCSizeMake(288, 377));
		ppanel->setPosition(CCPointZero);
		ppanel->setTouchEnable(true);
		m_pUiLayer->addWidget(ppanel);

		UIButton * btn_close = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		btn_close->setTouchEnable( true );
		btn_close->setPressedActionEnabled(true);
		btn_close->addReleaseEvent(this,coco_releaseselector(GeneralsSkillInfoUI::CloseEvent));
		btn_close->setVisible(false);

		UILabel * L_skillName = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_skill_name");
		L_skillName->setText(gameFightSkill->getCBaseSkill()->name().c_str());
		//无双
		btn_superSkill = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_SuperSkill");
		btn_superSkill->setTouchEnable( true );
		btn_superSkill->setPressedActionEnabled(true);
		btn_superSkill->addReleaseEvent(this,coco_releaseselector(GeneralsSkillInfoUI::SuperSkillEvent));

		if(gameFightSkill->getCBaseSkill()->usemodel() == 0)//主动技能
		{
			btn_superSkill->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
			btn_superSkill->setTouchEnable(true);
		}
		else
		{
			btn_superSkill->setTextures("res_ui/new_button_001.png","res_ui/new_button_001.png","");
			btn_superSkill->setTouchEnable(false);
		}


		//升级
		UIButton * btn_lvUp = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_upgrade");
		btn_lvUp->setTouchEnable( true );
		btn_lvUp->setPressedActionEnabled(true);
		btn_lvUp->addReleaseEvent(this,coco_releaseselector(GeneralsSkillInfoUI::LvUpEvent));
		//替换
		UIButton * btn_replace = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_shift");
		btn_replace->setTouchEnable( true );
		btn_replace->setPressedActionEnabled(true);
		btn_replace->addReleaseEvent(this,coco_releaseselector(GeneralsSkillInfoUI::ReplaceEvent));

		RefreshSkillInfo(gameFightSkill);

		this->setContentSize(CCSizeMake(288,377));
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);

		return true;
	}
	return false;
}

void GeneralsSkillInfoUI::CloseEvent( CCObject *pSender )
{
	this->closeAnim();
}

void GeneralsSkillInfoUI::SuperSkillEvent( CCObject *pSender )
{
	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(pSender);

	SkillScene::ReqForPreeless * temp = new SkillScene::ReqForPreeless();
	temp->skillid = curSkillId;
	temp->grid = skillSlotIndex;
	temp->roleId = GeneralsUI::generalsListUI->getCurGeneralBaseMsg()->id();
	temp->roleType = GameActor::type_pet;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5075,temp);
	delete temp;
}

void GeneralsSkillInfoUI::LvUpEvent( CCObject *pSender )
{
	//开启等级
	int openlevel = 0;
	std::map<int,int>::const_iterator cIter;
	cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(24);
	if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // 没找到就是指向END了  
	{
	}
	else
	{
		openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[24];
	}

	if (openlevel > GameView::getInstance()->myplayer->getActiveRole()->level())
	{
		std::string str_des = StringDataManager::getString("feature_will_be_open_function");
		char str_level[20];
		sprintf(str_level,"%d",openlevel);
		str_des.append(str_level);
		str_des.append(StringDataManager::getString("feature_will_be_open_open"));
		GameView::getInstance()->showAlertDialog(str_des.c_str());
		return;
	}

	if (strcmp(curSkillId.c_str(),"") == 0)
	{
		//CCDictionary *strings = CCDictionary::createWithContentsOfFile("res_ui/font/strings.xml");
		//const char *str1  = ((CCString*)strings->objectForKey("skillui_peleaseselcetskill"))->m_sString.c_str();
		const char *str1 = StringDataManager::getString("skillui_peleaseselcetskill");
		char* p1 =const_cast<char*>(str1);
		GameView::getInstance()->showAlertDialog(p1);
	}
	else
	{
		const char * _idx = curSkillId.c_str();
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5054,(void *)_idx);
	}
}

void GeneralsSkillInfoUI::ReplaceEvent( CCObject *pSender )
{
	//开启等级
	int openlevel = 0;
	std::map<int,int>::const_iterator cIter;
	cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(24);
	if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // 没找到就是指向END了  
	{
	}
	else
	{
		openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[24];
	}

	if (openlevel > GameView::getInstance()->myplayer->getActiveRole()->level())
	{
		std::string str_des = StringDataManager::getString("feature_will_be_open_function");
		char str_level[20];
		sprintf(str_level,"%d",openlevel);
		str_des.append(str_level);
		str_des.append(StringDataManager::getString("feature_will_be_open_open"));
		GameView::getInstance()->showAlertDialog(str_des.c_str());
		return;
	}

	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsSkillListUI) == NULL)
	{
		this->removeFromParent();
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		GeneralsSkillListUI * generalsSkillListUI = GeneralsSkillListUI::create(skill_profession,skillSlotIndex);
		if (generalsSkillListUI)
		{
			generalsSkillListUI->ignoreAnchorPointForPosition(false);
			generalsSkillListUI->setAnchorPoint(ccp(0.5f,0.5f));
			generalsSkillListUI->setPosition(ccp(winSize.width/2,winSize.height/2));
			GameView::getInstance()->getMainUIScene()->addChild(generalsSkillListUI,0,kTagGeneralsSkillListUI);
		}
	}
}

void GeneralsSkillInfoUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void GeneralsSkillInfoUI::onExit()
{
	UIScene::onExit();
}

bool GeneralsSkillInfoUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	//return true;
	return resignFirstResponder(touch,this,false);
}

void GeneralsSkillInfoUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void GeneralsSkillInfoUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void GeneralsSkillInfoUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{
}

void GeneralsSkillInfoUI::setSkillProfession( GeneralsSkillsUI::SkillTpye skill_prof )
{
	this->skill_profession = skill_prof;
}

GeneralsSkillsUI::SkillTpye GeneralsSkillInfoUI::getSkillProfession()
{
	return this->skill_profession;
}

std::string GeneralsSkillInfoUI::getProfessionBySkill( std::string skillid )
{
	const char *str1 = StringDataManager::getString("profession_mengjiang");
	char* profession_mengjiang =const_cast<char*>(str1);
	const char *str2 = StringDataManager::getString("profession_guimou");
	char* profession_guimou =const_cast<char*>(str2);
	const char *str3 = StringDataManager::getString("profession_haojie");
	char* profession_haojie =const_cast<char*>(str3);
	const char *str4 = StringDataManager::getString("profession_shenshe");
	char* profession_shenshe =const_cast<char*>(str4);

	int profession_index = 0;
	std::map<std::string,CBaseSkill*>::const_iterator cIter;
	cIter = StaticDataBaseSkill::s_baseSkillData.find(skillid);
	if (cIter == StaticDataBaseSkill::s_baseSkillData.end()) // 没找到就是指向END了  
	{
		CCAssert(false,"skill not found");
	}
	else
	{
		profession_index = cIter->second->get_profession();
	}

	switch(profession_index)
	{
	case 1:
		{
			return profession_mengjiang;
		}
		break;
	case 2:
		{
			return profession_guimou;
		}
		break;
	case 3:
		{
			return profession_haojie;
		}
		break;
	case 4:
		{
			return profession_shenshe;
		}
		break;
	}

// 	std::string tempid1 = skillid.substr(0,1);
// 	if (strcmp(tempid1.c_str(),"P") == 0)  //被动技能
// 	{
// 		std::string tempid3 = skillid.substr(1,1);
// 		if (strcmp(tempid3.c_str(),"A") == 0)
// 		{
// 			return profession_mengjiang;
// 		}
// 		else if (strcmp(tempid3.c_str(),"B") == 0)
// 		{
// 			return profession_guimou;
// 		}
// 		else if (strcmp(tempid3.c_str(),"C") == 0)
// 		{
// 			return profession_haojie;
// 		}
// 		else if (strcmp(tempid3.c_str(),"D") == 0)
// 		{
// 			return profession_shenshe;
// 		}
// 	}
// 	else
// 	{
// 		std::string tempid2 = skillid.substr(0,1);
// 		if (strcmp(tempid2.c_str(),"A") == 0)
// 		{
// 			return profession_mengjiang;
// 		}
// 		else if (strcmp(tempid2.c_str(),"B") == 0)
// 		{
// 			return profession_guimou;
// 		}
// 		else if (strcmp(tempid2.c_str(),"C") == 0)
// 		{
// 			return profession_haojie;
// 		}
// 		else if (strcmp(tempid2.c_str(),"D") == 0)
// 		{
// 			return profession_shenshe;
// 		}
// 	}
}


void GeneralsSkillInfoUI::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void GeneralsSkillInfoUI::addCCTutorialIndicator1( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
	// 	UITutorialIndicator * tutorialIndicator = UITutorialIndicator::create();
	// 	tutorialIndicator->setPosition(pos);
	// 	tutorialIndicator->setTag(UITUTORIALINDICATORTAG);
	// 	u_layer->addWidget(tutorialIndicator);
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,60,50);
	//tutorialIndicator->setPosition(ccp(pos.x+40+_w,pos.y+65+_h));
	tutorialIndicator->setPosition(ccp(pos.x+40,pos.y+65));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	this->addChild(tutorialIndicator);

	CCTutorialParticle * tutorialParticle = CCTutorialParticle::create("tuowei0.plist",60,50);
	//tutorialParticle->setPosition(ccp(pos.x+5+_w,pos.y-15+_h));
	tutorialParticle->setPosition(ccp(pos.x+5,pos.y-15));
	tutorialParticle->setTag(CCTUTORIALPARTICLETAG);
	this->addChild(tutorialParticle);
}

void GeneralsSkillInfoUI::removeCCTutorialIndicator()
{
	CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	CCTutorialParticle* tutorialParticle = dynamic_cast<CCTutorialParticle*>(this->getChildByTag(CCTUTORIALPARTICLETAG));
	if(tutorialParticle != NULL)
		tutorialParticle->removeFromParent();
}

void GeneralsSkillInfoUI::RefreshSkillInfo( GameFightSkill * gameFightSkill )
{
	if (curSkillId != gameFightSkill->getId())
		return;

	//create scrollerView
	UIScrollView * scrollView_skillInfo = (UIScrollView*)m_pUiLayer->getWidgetByTag(kTag_ScrollView);
	if (scrollView_skillInfo == NULL)
	{
		scrollView_skillInfo = UIScrollView::create();
		scrollView_skillInfo->setTouchEnable(true);
		scrollView_skillInfo->setDirection(SCROLLVIEW_DIR_VERTICAL);
		scrollView_skillInfo->setSize(CCSizeMake(240,238));
		scrollView_skillInfo->setPosition(ccp(24,84));
		scrollView_skillInfo->setBounceEnabled(true);
		scrollView_skillInfo->setTag(kTag_ScrollView);
		m_pUiLayer->addWidget(scrollView_skillInfo);
	}
	//create elements
	int scroll_height = 0;
	//技能名称
	UIImageView * image_nameFrame = (UIImageView *)scrollView_skillInfo->getChildByName("image_nameFrame");
	if (image_nameFrame == NULL)
	{
		image_nameFrame = UIImageView::create();
		image_nameFrame->setTexture("res_ui/moji_0.png");
		image_nameFrame->setScale(0.6f);
		image_nameFrame->setAnchorPoint(ccp(0,0));
		image_nameFrame->setName("image_nameFrame");
		scrollView_skillInfo->addChild(image_nameFrame);
	}

	UILabel * l_name = (UILabel *)scrollView_skillInfo->getChildByName("l_name");
	if (l_name == NULL)
	{
		l_name = UILabel::create();
		l_name->setText(gameFightSkill->getCBaseSkill()->name().c_str());
		l_name->setFontName(APP_FONT_NAME);
		l_name->setFontSize(18);
		l_name->setColor(ccc3(196,255,68));
		l_name->setAnchorPoint(ccp(0,0));
		l_name->setName("l_name");
		scrollView_skillInfo->addChild(l_name);
	}
	else
	{
		l_name->setText(gameFightSkill->getCBaseSkill()->name().c_str());
	}

	int zeroLineHeight = image_nameFrame->getContentSize().height;
	scroll_height = zeroLineHeight;

	//first line
	UIImageView * image_first_line = (UIImageView *)scrollView_skillInfo->getChildByName("image_first_line");
	if (image_first_line == NULL)
	{
		image_first_line = UIImageView::create();
		image_first_line->setTexture("res_ui/henggang.png");
		image_first_line->setScaleX(0.9f);
		image_first_line->setAnchorPoint(ccp(0,0));
		image_first_line->setName("image_first_line");
		scrollView_skillInfo->addChild(image_first_line);
	}
	int zeroLineHeight_1 = zeroLineHeight + image_first_line->getContentSize().height+6;
	scroll_height = zeroLineHeight_1;
	//主动/被动
	std::string str_type = "";
	if (gameFightSkill->getCBaseSkill()->usemodel() == 0)
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_zhudongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		str_type.append(skillinfo_zhudongjineng);
	}
	else if (gameFightSkill->getCBaseSkill()->usemodel() == 2)
	{
		const char *beidongjineng = StringDataManager::getString("skillinfo_beidongjineng");
		char* skillinfo_beidongjineng =const_cast<char*>(beidongjineng);
		str_type.append(skillinfo_beidongjineng);
	}
	else
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_zhudongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		str_type.append(skillinfo_zhudongjineng);
	}
	UILabel * l_type = (UILabel *)scrollView_skillInfo->getChildByName("l_type");
	if (l_type == NULL)
	{
		l_type = UILabel::create();
		l_type->setText(str_type.c_str());
		l_type->setFontName(APP_FONT_NAME);
		l_type->setFontSize(16);
		l_type->setColor(ccc3(212,255,151));
		l_type->setAnchorPoint(ccp(0,0));
		l_type->setName("l_type");
		scrollView_skillInfo->addChild(l_type);
	}
	else
	{
		l_type->setText(str_type.c_str());
	}
	//MP消耗
	UILabel * l_mp = (UILabel *)scrollView_skillInfo->getChildByName("l_mp");
	if (l_mp == NULL)
	{
		l_mp = UILabel::create();
		l_mp->setText(StringDataManager::getString("skillinfo_mofaxiaohao"));
		l_mp->setFontName(APP_FONT_NAME);
		l_mp->setFontSize(16);
		l_mp->setColor(ccc3(212,255,151));
		l_mp->setAnchorPoint(ccp(0,0));
		l_mp->setName("l_mp");
		scrollView_skillInfo->addChild(l_mp);
	}
	char s_mp[5];
	sprintf(s_mp,"%d",gameFightSkill->getCFightSkill()->mp());
	UILabel * l_mpValue = (UILabel *)scrollView_skillInfo->getChildByName("l_mpValue");
	if (l_mpValue == NULL)
	{
		l_mpValue = UILabel::create();
		l_mpValue->setText(s_mp);
		l_mpValue->setFontName(APP_FONT_NAME);
		l_mpValue->setFontSize(16);
		l_mpValue->setColor(ccc3(212,255,151));
		l_mpValue->setAnchorPoint(ccp(0,0));
		l_mpValue->setName("l_mpValue");
		scrollView_skillInfo->addChild(l_mpValue);
	}
	else
	{
		l_mpValue->setText(s_mp);
	}
	int firstLineHeight = zeroLineHeight_1 + l_type->getContentSize().height;
	scroll_height = firstLineHeight;
	//等级
	UILabel * l_lv = (UILabel *)scrollView_skillInfo->getChildByName("l_lv");
	if (l_lv == NULL)
	{
		l_lv = UILabel::create();
		l_lv->setText(StringDataManager::getString("skillinfo_dengji"));
		l_lv->setFontName(APP_FONT_NAME);
		l_lv->setFontSize(16);
		l_lv->setColor(ccc3(212,255,151));
		l_lv->setAnchorPoint(ccp(0,0));
		l_lv->setName("l_lv");
		scrollView_skillInfo->addChild(l_lv);
	}

	char s_curLevel[5];
	sprintf(s_curLevel,"%d",gameFightSkill->getCFightSkill()->level());
	UILabel * l_lvValue = (UILabel *)scrollView_skillInfo->getChildByName("l_lvValue");
	if (l_lvValue == NULL)
	{
		l_lvValue = UILabel::create();
		l_lvValue->setText(s_curLevel);
		l_lvValue->setFontName(APP_FONT_NAME);
		l_lvValue->setFontSize(16);
		l_lvValue->setColor(ccc3(212,255,151));
		l_lvValue->setAnchorPoint(ccp(0,0));
		l_lvValue->setName("l_lvValue");
		scrollView_skillInfo->addChild(l_lvValue);
	}
	else
	{
		l_lvValue->setText(s_curLevel);
	}
	//CD时间 
	UILabel * l_cd = (UILabel *)scrollView_skillInfo->getChildByName("l_cd");
	if (l_cd == NULL)
	{
		l_cd = UILabel::create();
		l_cd->setText(StringDataManager::getString("skillinfo_lengqueshijian"));
		l_cd->setFontName(APP_FONT_NAME);
		l_cd->setFontSize(16);
		l_cd->setColor(ccc3(212,255,151));
		l_cd->setAnchorPoint(ccp(0,0));
		l_cd->setName("l_cd");
		scrollView_skillInfo->addChild(l_cd);
	}
	char s_intervaltime[5];
	sprintf(s_intervaltime,"%d",gameFightSkill->getCFightSkill()->intervaltime()/1000);
	UILabel * l_cdValue = (UILabel *)scrollView_skillInfo->getChildByName("l_cdValue");
	if (l_cdValue == NULL)
	{
		l_cdValue = UILabel::create();
		l_cdValue->setText(s_intervaltime);
		l_cdValue->setFontName(APP_FONT_NAME);
		l_cdValue->setFontSize(16);
		l_cdValue->setColor(ccc3(212,255,151));
		l_cdValue->setAnchorPoint(ccp(0,0));
		l_cdValue->setName("l_cdValue");
		scrollView_skillInfo->addChild(l_cdValue);
	}
	else
	{
		l_cdValue->setText(s_intervaltime);
	}
	int secondLineHeight = firstLineHeight + l_lv->getContentSize().height;
	scroll_height = secondLineHeight;
	//职业
	UILabel * l_pro = (UILabel *)scrollView_skillInfo->getChildByName("l_pro");
	if (l_pro == NULL)
	{
		l_pro = UILabel::create();
		l_pro->setText(StringDataManager::getString("skillinfo_zhiye"));
		l_pro->setFontName(APP_FONT_NAME);
		l_pro->setFontSize(16);
		l_pro->setColor(ccc3(212,255,151));
		l_pro->setAnchorPoint(ccp(0,0));
		l_pro->setName("l_pro");
		scrollView_skillInfo->addChild(l_pro);
	}
	UILabel * l_proValue = (UILabel *)scrollView_skillInfo->getChildByName("l_proValue");
	if (l_proValue == NULL)
	{
		l_proValue = UILabel::create();
		l_proValue->setText(GameView::getInstance()->myplayer->getActiveRole()->profession().c_str());
		l_proValue->setFontName(APP_FONT_NAME);
		l_proValue->setFontSize(16);
		l_proValue->setColor(ccc3(212,255,151));
		l_proValue->setAnchorPoint(ccp(0,0));
		l_proValue->setName("l_proValue");
		scrollView_skillInfo->addChild(l_proValue);
	}
	else
	{
		l_proValue->setText(GameView::getInstance()->myplayer->getActiveRole()->profession().c_str());
	}
	//施法距离
	UILabel * l_dis = (UILabel *)scrollView_skillInfo->getChildByName("l_dis");
	if (l_dis == NULL)
	{
		l_dis = UILabel::create();
		l_dis->setText(StringDataManager::getString("skillinfo_shifajuli"));
		l_dis->setFontName(APP_FONT_NAME);
		l_dis->setFontSize(16);
		l_dis->setColor(ccc3(212,255,151));
		l_dis->setAnchorPoint(ccp(0,0));
		l_dis->setName("l_dis");
		scrollView_skillInfo->addChild(l_dis);
	}
	char s_distance[5];
	sprintf(s_distance,"%d",gameFightSkill->getCFightSkill()->distance());
	UILabel * l_disValue = (UILabel *)scrollView_skillInfo->getChildByName("l_disValue");
	if (l_disValue == NULL)
	{
		l_disValue = UILabel::create();
		l_disValue->setText(s_distance);
		l_disValue->setFontName(APP_FONT_NAME);
		l_disValue->setFontSize(16);
		l_disValue->setColor(ccc3(212,255,151));
		l_disValue->setAnchorPoint(ccp(0,0));
		l_disValue->setName("l_disValue");
		scrollView_skillInfo->addChild(l_disValue);
	}
	else
	{
		l_disValue->setText(s_distance);
	}
	int thirdLineHeight = secondLineHeight + l_pro->getContentSize().height;
	scroll_height = thirdLineHeight;
	//简介
	UILabel * l_des = (UILabel *)scrollView_skillInfo->getChildByName("l_des");
	if (l_des == NULL)
	{
		l_des = UILabel::create();
		l_des->setText(gameFightSkill->getCFightSkill()->description().c_str());
		l_des->setFontName(APP_FONT_NAME);
		l_des->setFontSize(16);
		l_des->setTextAreaSize(CCSizeMake(230, 0 ));
		l_des->setColor(ccc3(30,255,0));
		l_des->setAnchorPoint(ccp(0,0));
		l_des->setName("l_des");
		scrollView_skillInfo->addChild(l_des);
	}
	else
	{
		l_des->setText(gameFightSkill->getCFightSkill()->description().c_str());
	}
	int fourthLineHeight = thirdLineHeight+l_des->getContentSize().height;
	scroll_height = fourthLineHeight;

	//next info
	if (scrollView_skillInfo->getChildByName("panel_nextInfo"))
	{
		scrollView_skillInfo->getChildByName("panel_nextInfo")->removeFromParent();
	}
	UIPanel * panel_nextInfo = UIPanel::create();
	panel_nextInfo->setName("panel_nextInfo");
	scrollView_skillInfo->addChild(panel_nextInfo);

	int fifthLineHeight = 0;
	int sixthLineHeight = 0;
	int seventhLineHeight = 0;
	int seventhLineHeight_1 = 0;
	int eighthLineHeight = 0;
	int ninthLineHeight = 0;
	int tenthLineHeight = 0;
	int eleventhLineHeight = 0;
	int twelfthLineHeight = 0;

	std::string m_str_next_des = "";
	int m_required_level = 0;
	int m_required_point = 0;
	std::string m_str_pre_skill = "";
	std::string m_pre_skill_id = "";
	int m_required_pre_skil_level = 0;
	int m_required_gold = 0;
	int m_required_skillBookValue = 0;

	if (gameFightSkill->getLevel()<gameFightSkill->getCBaseSkill()->maxlevel())
	{
		//get data
		m_str_next_des.append(gameFightSkill->getCFightSkill()->get_next_description());
		//主角等级
		m_required_level = gameFightSkill->getCFightSkill()->get_next_required_level();
		//技能点
		m_required_point = gameFightSkill->getCFightSkill()->get_next_required_point();
		//XXX技能
		if (gameFightSkill->getCFightSkill()->get_next_pre_skill() != "")
		{
			CBaseSkill * temp = StaticDataBaseSkill::s_baseSkillData[gameFightSkill->getCFightSkill()->get_next_pre_skill()];
			m_str_pre_skill.append(temp->name().c_str());
			m_pre_skill_id.append(temp->id());

			m_required_pre_skil_level = gameFightSkill->getCFightSkill()->get_next_pre_skill_level();
		}
		//金币
		m_required_gold = gameFightSkill->getCFightSkill()->get_next_required_gold();
		//技能书
		m_required_skillBookValue = gameFightSkill->getCFightSkill()->get_next_required_num();

		//create ui
		//下一级
		UILabel * l_nextLv = UILabel::create();
		l_nextLv->setText(StringDataManager::getString("skillinfo_xiayiji"));
		l_nextLv->setFontName(APP_FONT_NAME);
		l_nextLv->setFontSize(16);
		l_nextLv->setColor(ccc3(0,255,255));
		l_nextLv->setAnchorPoint(ccp(0,0));
		l_nextLv->setName("l_nextLv");
		panel_nextInfo->addChild(l_nextLv);

		fifthLineHeight = fourthLineHeight+l_nextLv->getContentSize().height*2;
		scroll_height = fifthLineHeight;
		/////////////////////////////////////////////////////
		UILabel * l_nextLvDes = UILabel::create();
		l_nextLvDes->setText(gameFightSkill->getCFightSkill()->get_next_description().c_str());
		l_nextLvDes->setFontName(APP_FONT_NAME);
		l_nextLvDes->setFontSize(16);
		l_nextLvDes->setTextAreaSize(CCSizeMake(230, 0 ));
		l_nextLvDes->setColor(ccc3(0,255,255));
		l_nextLvDes->setAnchorPoint(ccp(0,0));
		l_nextLvDes->setName("l_nextLvDes");
		panel_nextInfo->addChild(l_nextLvDes);

		sixthLineHeight = fifthLineHeight+l_nextLvDes->getContentSize().height;
		scroll_height = sixthLineHeight;

		//升级需求
		UIImageView * image_upgradeNeed = UIImageView::create();
		image_upgradeNeed->setTexture("res_ui/moji_0.png");
		image_upgradeNeed->setScale(0.6f);
		image_upgradeNeed->setAnchorPoint(ccp(0,0));
		image_upgradeNeed->setName("image_upgradeNeed");
		panel_nextInfo->addChild(image_upgradeNeed);

		UILabel * l_upgradeNeed = UILabel::create();
		l_upgradeNeed->setText(StringDataManager::getString("skillinfo_xuexixuqiu"));
		l_upgradeNeed->setFontName(APP_FONT_NAME);
		l_upgradeNeed->setFontSize(18);
		l_upgradeNeed->setColor(ccc3(196,255,68));
		l_upgradeNeed->setAnchorPoint(ccp(0,0));
		l_upgradeNeed->setName("l_upgradeNeed");
		panel_nextInfo->addChild(l_upgradeNeed);

		seventhLineHeight = sixthLineHeight + image_upgradeNeed->getContentSize().height;
		scroll_height = seventhLineHeight;


		//////////////////////////////////////////////////////
		//
		UIImageView * image_second_line = UIImageView::create();
		image_second_line->setTexture("res_ui/henggang.png");
		image_second_line->setScale(0.9f);
		image_second_line->setAnchorPoint(ccp(0,0));
		image_second_line->setName("image_second_line");
		panel_nextInfo->addChild(image_second_line);

		seventhLineHeight_1 = seventhLineHeight + image_second_line->getContentSize().height+7;
		scroll_height = seventhLineHeight_1;

		//主角等级
		if (m_required_level > 0)
		{
			UILabel * l_playerLv = UILabel::create();
			l_playerLv->setText(StringDataManager::getString("skillinfo_zhujuedengji"));
			l_playerLv->setFontName(APP_FONT_NAME);
			l_playerLv->setFontSize(16);
			l_playerLv->setColor(ccc3(212,255,151));
			l_playerLv->setAnchorPoint(ccp(0,0));
			l_playerLv->setName("l_playerLv");
			panel_nextInfo->addChild(l_playerLv);

			UILabel * l_playerLv_colon = UILabel::create();
			l_playerLv_colon->setText(":");
			l_playerLv_colon->setFontName(APP_FONT_NAME);
			l_playerLv_colon->setFontSize(16);
			l_playerLv_colon->setColor(ccc3(212,255,151));
			l_playerLv_colon->setAnchorPoint(ccp(0,0));
			l_playerLv_colon->setName("l_playerLv_colon");
			panel_nextInfo->addChild(l_playerLv_colon);

			char s_required_level[5];
			sprintf(s_required_level,"%d",m_required_level);
			UILabel * l_playerLvValue = UILabel::create();
			l_playerLvValue->setText(s_required_level);
			l_playerLvValue->setFontName(APP_FONT_NAME);
			l_playerLvValue->setFontSize(16);
			l_playerLvValue->setTextAreaSize(CCSizeMake(230, 0 ));
			l_playerLvValue->setColor(ccc3(212,255,151));
			l_playerLvValue->setAnchorPoint(ccp(0,0));
			l_playerLvValue->setName("l_playerLvValue");
			panel_nextInfo->addChild(l_playerLvValue);

			eighthLineHeight = seventhLineHeight_1 + l_playerLv->getContentSize().height;
			scroll_height = eighthLineHeight;

			if (GameView::getInstance()->myplayer->getActiveRole()->level()<m_required_level)  //人物等级未达到技能升级要求
			{
				l_playerLv->setColor(ccc3(255,51,51));
				l_playerLv_colon->setColor(ccc3(255,51,51));
				l_playerLvValue->setColor(ccc3(255,51,51));
			}
		}
		else
		{
			eighthLineHeight = seventhLineHeight_1;
			scroll_height = eighthLineHeight;
		}
		//技能点
		if (m_required_point > 0)
		{
			UIWidget * widget_skillPoint = UIWidget::create();
			panel_nextInfo->addChild(widget_skillPoint);
			widget_skillPoint->setName("widget_skillPoint");

			std::string str_skillPoint = StringDataManager::getString("skillinfo_jinengdian");
			for(int i = 0;i<3;i++)
			{
				std::string str_name = "l_skillPoint";
				char s_index[5];
				sprintf(s_index,"%d",i);
				str_name.append(s_index);
				UILabel * l_skillPoint = UILabel::create();
				l_skillPoint->setText(str_skillPoint.substr(i*3,3).c_str());
				l_skillPoint->setFontName(APP_FONT_NAME);
				l_skillPoint->setFontSize(16);
				l_skillPoint->setColor(ccc3(212,255,151));
				l_skillPoint->setAnchorPoint(ccp(0,0));
				l_skillPoint->setName(str_name.c_str());
				widget_skillPoint->addChild(l_skillPoint);
				l_skillPoint->setPosition(ccp(24*i,0));
			}

			UILabel * l_skillPoint_colon = UILabel::create();
			l_skillPoint_colon->setText(":");
			l_skillPoint_colon->setFontName(APP_FONT_NAME);
			l_skillPoint_colon->setFontSize(16);
			l_skillPoint_colon->setColor(ccc3(212,255,151));
			l_skillPoint_colon->setAnchorPoint(ccp(0,0));
			l_skillPoint_colon->setName("l_skillPoint_colon");
			panel_nextInfo->addChild(l_skillPoint_colon);

			char s_required_point[10];
			sprintf(s_required_point,"%d",m_required_point);
			UILabel * l_skillPointValue = UILabel::create();
			l_skillPointValue->setText(s_required_point);
			l_skillPointValue->setFontName(APP_FONT_NAME);
			l_skillPointValue->setFontSize(16);
			l_skillPointValue->setTextAreaSize(CCSizeMake(230, 0 ));
			l_skillPointValue->setColor(ccc3(212,255,151));
			l_skillPointValue->setAnchorPoint(ccp(0,0));
			l_skillPointValue->setName("l_skillPointValue");
			panel_nextInfo->addChild(l_skillPointValue);

			ninthLineHeight = eighthLineHeight + l_skillPointValue->getContentSize().height;
			scroll_height = ninthLineHeight;

			if (GameView::getInstance()->myplayer->player->skillpoint()<m_required_point)  //人物技能点不足
			{
				for(int i = 0;i<3;i++)
				{
					std::string str_name = "l_skillPoint";
					char s_index[5];
					sprintf(s_index,"%d",i);
					str_name.append(s_index);
					UILabel * l_skillPoint = (UILabel*)widget_skillPoint->getChildByName(str_name.c_str());
					if (l_skillPoint)
					{
						l_skillPoint->setColor(ccc3(255,51,51));
					}
				}
				l_skillPoint_colon->setColor(ccc3(255,51,51));
				l_skillPointValue->setColor(ccc3(255,51,51));
			}
		}
		else
		{
			ninthLineHeight = eighthLineHeight;
			scroll_height = ninthLineHeight;
		}
		//XXX技能
		if (m_str_pre_skill != "")
		{
			UIWidget * widget_pre_skill = UIWidget::create();
			panel_nextInfo->addChild(widget_pre_skill);
			widget_pre_skill->setName("widget_pre_skill");
			if (m_str_pre_skill.size()/3 < 4)
			{
				for(int i = 0;i<m_str_pre_skill.size()/3;i++)
				{
					std::string str_name = "l_preSkill";
					char s_index[5];
					sprintf(s_index,"%d",i);
					str_name.append(s_index);

					UILabel * l_preSkill = UILabel::create();
					l_preSkill->setText(m_str_pre_skill.substr(i*3,3).c_str());
					l_preSkill->setFontName(APP_FONT_NAME);
					l_preSkill->setFontSize(16);
					l_preSkill->setColor(ccc3(212,255,151));
					l_preSkill->setAnchorPoint(ccp(0,0));
					widget_pre_skill->addChild(l_preSkill);
					l_preSkill->setName(str_name.c_str());
					if (m_str_pre_skill.size()/3 == 1)
					{
						l_preSkill->setPosition(ccp(0,0));
					}
					else if (m_str_pre_skill.size()/3 == 2)
					{
						l_preSkill->setPosition(ccp(48*i,0));
					}
					else if (m_str_pre_skill.size()/3 == 3)
					{
						l_preSkill->setPosition(ccp(24*i,0));
					}
				}
			}
			else
			{
				UILabel * l_preSkill = UILabel::create();
				l_preSkill->setText(m_str_pre_skill.c_str());
				l_preSkill->setFontName(APP_FONT_NAME);
				l_preSkill->setFontSize(16);
				l_preSkill->setColor(ccc3(212,255,151));
				l_preSkill->setAnchorPoint(ccp(0,0));
				l_preSkill->setPosition(ccp(0,0));
				widget_pre_skill->addChild(l_preSkill);
				l_preSkill->setName("l_preSkill");
			}

			UILabel * l_preSkill_colon = UILabel::create();
			l_preSkill_colon->setText(":");
			l_preSkill_colon->setFontName(APP_FONT_NAME);
			l_preSkill_colon->setFontSize(16);
			l_preSkill_colon->setColor(ccc3(212,255,151));
			l_preSkill_colon->setAnchorPoint(ccp(0,0));
			l_preSkill_colon->setName("l_preSkill_colon");
			panel_nextInfo->addChild(l_preSkill_colon);

			char s_pre_skill_level[5];
			sprintf(s_pre_skill_level,"%d",m_required_pre_skil_level);
			UILabel * l_preSkillValue = UILabel::create();
			l_preSkillValue->setText(s_pre_skill_level);
			l_preSkillValue->setFontName(APP_FONT_NAME);
			l_preSkillValue->setFontSize(16);
			l_preSkillValue->setTextAreaSize(CCSizeMake(230, 0 ));
			l_preSkillValue->setColor(ccc3(212,255,151));
			l_preSkillValue->setAnchorPoint(ccp(0,0));
			l_preSkillValue->setName("l_preSkillValue");
			panel_nextInfo->addChild(l_preSkillValue);

			tenthLineHeight = ninthLineHeight + l_preSkillValue->getContentSize().height;
			scroll_height = tenthLineHeight;

			std::map<std::string,GameFightSkill*>::const_iterator cIter;
			cIter = GameView::getInstance()->GameFightSkillList.find(m_pre_skill_id);
			if (cIter == GameView::getInstance()->GameFightSkillList.end()) // 没找到就是指向END了  
			{
				GameFightSkill * gameFightSkillStuded = GameView::getInstance()->GameFightSkillList[m_pre_skill_id];
				if (gameFightSkillStuded)
				{
					if (gameFightSkillStuded->getLevel() < m_required_pre_skil_level)
					{
						if (m_str_pre_skill.size()/3 < 4)
						{
							for(int i = 0;i<m_str_pre_skill.size()/3;i++)
							{
								std::string str_name = "l_preSkill";
								char s_index[5];
								sprintf(s_index,"%d",i);
								str_name.append(s_index);

								UILabel * l_preSkill = (UILabel*)widget_pre_skill->getChildByName(str_name.c_str());
								if (l_preSkill)
								{
									l_preSkill->setColor(ccc3(255,51,51));
								}
							}
						}
						else
						{
							UILabel * l_preSkill = (UILabel*)widget_pre_skill->getChildByName("l_preSkill");
							if (l_preSkill)
							{
								l_preSkill->setColor(ccc3(255,51,51));
							}
						}
						l_preSkill_colon->setColor(ccc3(255,51,51));
						l_preSkillValue->setColor(ccc3(255,51,51));
					}
				}
			}
		}
		else
		{
			tenthLineHeight = ninthLineHeight;
			scroll_height = tenthLineHeight;
		}
		//金币
		if (m_required_gold > 0)
		{
			UIWidget * widget_gold = UIWidget::create();
			panel_nextInfo->addChild(widget_gold);
			widget_gold->setName("widget_gold");

			std::string str_gold = StringDataManager::getString("skillinfo_jinbi");
			for(int i = 0;i<2;i++)
			{
				std::string str_name = "l_gold";
				char s_index[5];
				sprintf(s_index,"%d",i);
				str_name.append(s_index);
				UILabel * l_gold = UILabel::create();
				l_gold->setText(str_gold.substr(i*3,3).c_str());
				l_gold->setFontName(APP_FONT_NAME);
				l_gold->setFontSize(16);
				l_gold->setColor(ccc3(212,255,151));
				l_gold->setAnchorPoint(ccp(0,0));
				l_gold->setName(str_name.c_str());
				widget_gold->addChild(l_gold);
				l_gold->setPosition(ccp(48*i,0));
			}

			UILabel * l_gold_colon = UILabel::create();
			l_gold_colon->setText(":");
			l_gold_colon->setFontName(APP_FONT_NAME);
			l_gold_colon->setFontSize(16);
			l_gold_colon->setColor(ccc3(212,255,151));
			l_gold_colon->setAnchorPoint(ccp(0,0));
			l_gold_colon->setName("l_gold_colon");
			panel_nextInfo->addChild(l_gold_colon);

			char s_required_gold[20];
			sprintf(s_required_gold,"%d",m_required_gold);
			UILabel * l_goldValue = UILabel::create();
			l_goldValue->setText(s_required_gold);
			l_goldValue->setFontName(APP_FONT_NAME);
			l_goldValue->setFontSize(16);
			l_goldValue->setColor(ccc3(212,255,151));
			l_goldValue->setAnchorPoint(ccp(0,0));
			l_goldValue->setName("l_goldValue");
			panel_nextInfo->addChild(l_goldValue);

			UIImageView * image_gold = UIImageView::create();
			image_gold->setTexture("res_ui/coins.png");
			image_gold->setAnchorPoint(ccp(0,0));
			image_gold->setName("image_gold");
			image_gold->setScale(0.85f);
			panel_nextInfo->addChild(image_gold);

			eleventhLineHeight = tenthLineHeight + l_goldValue->getContentSize().height;
			scroll_height = eleventhLineHeight;

			if(GameView::getInstance()->getPlayerGold()<m_required_gold)//金币不足
			{
				for(int i = 0;i<2;i++)
				{
					std::string str_name = "l_gold";
					char s_index[5];
					sprintf(s_index,"%d",i);
					str_name.append(s_index);
					UILabel * l_gold = (UILabel*)widget_gold->getChildByName(str_name.c_str());
					if (l_gold)
					{
						l_gold->setColor(ccc3(255,51,51));
					}
				}
				l_gold_colon->setColor(ccc3(255,51,51));
				l_goldValue->setColor(ccc3(255,51,51));
			}
		}
		else
		{
			eleventhLineHeight = tenthLineHeight;
			scroll_height = eleventhLineHeight;
		}
		//技能书
		if (m_required_skillBookValue>0)
		{
			UIWidget * widget_skillBook = UIWidget::create();
			panel_nextInfo->addChild(widget_skillBook);
			widget_skillBook->setName("widget_skillBook");

			std::string str_skillbook = GeneralsSkillsUI::getSkillQualityStr(gameFightSkill->getCBaseSkill()->get_quality());
			str_skillbook.append(StringDataManager::getString("skillinfo_jinengshu"));
			UILabel * l_skillbook = UILabel::create();
			l_skillbook->setText(str_skillbook.c_str());
			l_skillbook->setFontName(APP_FONT_NAME);
			l_skillbook->setFontSize(16);
			l_skillbook->setColor(ccc3(212,255,151));
			l_skillbook->setAnchorPoint(ccp(0,0));
			l_skillbook->setName("l_skillbook");
			widget_skillBook->addChild(l_skillbook);
			l_skillbook->setPosition(ccp(0,0));

			UILabel * l_skillbook_colon = UILabel::create();
			l_skillbook_colon->setText(":");
			l_skillbook_colon->setFontName(APP_FONT_NAME);
			l_skillbook_colon->setFontSize(16);
			l_skillbook_colon->setColor(ccc3(212,255,151));
			l_skillbook_colon->setAnchorPoint(ccp(0,0));
			l_skillbook_colon->setName("l_skillbook_colon");
			panel_nextInfo->addChild(l_skillbook_colon);

			char s_required_skillBook[20];
			sprintf(s_required_skillBook,"%d",m_required_skillBookValue);
			UILabel * l_skillBookValue = UILabel::create();
			l_skillBookValue->setText(s_required_skillBook);
			l_skillBookValue->setFontName(APP_FONT_NAME);
			l_skillBookValue->setFontSize(16);
			l_skillBookValue->setColor(ccc3(212,255,151));
			l_skillBookValue->setAnchorPoint(ccp(0,0));
			panel_nextInfo->addChild(l_skillBookValue);
			l_skillBookValue->setName("l_skillBookValue");

			twelfthLineHeight = eleventhLineHeight + l_skillBookValue->getContentSize().height;
			scroll_height = twelfthLineHeight;

			int propNum = 0;
			for (int i = 0;i<GameView::getInstance()->AllPacItem.size();++i)
			{
				FolderInfo * folder = GameView::getInstance()->AllPacItem.at(i);
				if (!folder->has_goods())
					continue;

				if (folder->goods().id() == gameFightSkill->getCFightSkill()->get_required_prop())
				{
					propNum += folder->quantity();
				}
			}
			if(propNum<gameFightSkill->getCFightSkill()->get_required_num())//技能书不足
			{
				l_skillbook->setColor(ccc3(255,51,51));
				l_skillbook_colon->setColor(ccc3(255,51,51));
				l_skillBookValue->setColor(ccc3(255,51,51));
			}
		}
		else
		{
			twelfthLineHeight = eleventhLineHeight;
			scroll_height = twelfthLineHeight;
		}
	}

	int innerWidth = scrollView_skillInfo->getRect().size.width;
	int innerHeight = scroll_height;
	if (scroll_height < scrollView_skillInfo->getRect().size.height)
	{
		innerHeight = scrollView_skillInfo->getRect().size.height;
	}
	scrollView_skillInfo->setInnerContainerSize(CCSizeMake(innerWidth,innerHeight));

	image_nameFrame->setPosition(ccp(8,innerHeight - zeroLineHeight));
	l_name->setPosition(ccp(15,innerHeight - zeroLineHeight));
	image_first_line->setPosition(ccp(5,innerHeight - zeroLineHeight_1+5));
	l_type->setPosition(ccp(8,innerHeight - firstLineHeight));
	l_mp->setPosition(ccp(105,innerHeight - firstLineHeight));
	l_mpValue->setPosition(ccp(105+l_mp->getContentSize().width+2,innerHeight - firstLineHeight));
	l_lv->setPosition(ccp(8,innerHeight - secondLineHeight));
	l_lvValue->setPosition(ccp(8+l_lv->getContentSize().width+2,innerHeight - secondLineHeight));
	l_cd->setPosition(ccp(105,innerHeight - secondLineHeight));
	l_cdValue->setPosition(ccp(105+l_cd ->getContentSize().width+2,innerHeight - secondLineHeight));
	l_pro->setPosition(ccp(8,innerHeight - thirdLineHeight));
	l_proValue->setPosition(ccp(8+l_pro->getContentSize().width+2,innerHeight - thirdLineHeight));
	l_dis->setPosition(ccp(105,innerHeight - thirdLineHeight));
	l_disValue->setPosition(ccp(105+l_dis->getContentSize().width+2,innerHeight - thirdLineHeight));

	if (gameFightSkill->getCBaseSkill()->usemodel() == 2)
	{
		l_mp->setVisible(false);
		l_mpValue->setVisible(false);
		l_cd->setVisible(false);
		l_cdValue->setVisible(false);
		l_dis->setVisible(false);
		l_disValue->setVisible(false);
	}
	else
	{
		l_mp->setVisible(true);
		l_mpValue->setVisible(true);
		l_cd->setVisible(true);
		l_cdValue->setVisible(true);
		l_dis->setVisible(true);
		l_disValue->setVisible(true);
	}

	l_des->setPosition(ccp(8,innerHeight - fourthLineHeight));
	if (gameFightSkill->getLevel()<gameFightSkill->getCBaseSkill()->maxlevel())
	{
		if (panel_nextInfo->getChildByName("l_nextLv"))
		{
			panel_nextInfo->getChildByName("l_nextLv")->setPosition(ccp(8,innerHeight - fifthLineHeight));
		}

		if (panel_nextInfo->getChildByName("l_nextLvDes"))
		{
			panel_nextInfo->getChildByName("l_nextLvDes")->setPosition(ccp(8,innerHeight - sixthLineHeight));
		}

		if (panel_nextInfo->getChildByName("image_upgradeNeed"))
		{
			panel_nextInfo->getChildByName("image_upgradeNeed")->setPosition(ccp(8,innerHeight - seventhLineHeight));
		}

		if (panel_nextInfo->getChildByName("l_upgradeNeed"))
		{
			panel_nextInfo->getChildByName("l_upgradeNeed")->setPosition(ccp(15,innerHeight - seventhLineHeight));
		}

		if (panel_nextInfo->getChildByName("image_second_line"))
		{
			panel_nextInfo->getChildByName("image_second_line")->setPosition(ccp(5,innerHeight - seventhLineHeight_1+5));
		}

		int temp_width = 64;

		//主角等级
		if (m_required_level>0)
		{
			if (panel_nextInfo->getChildByName("l_playerLv"))
			{
				panel_nextInfo->getChildByName("l_playerLv")->setPosition(ccp(8,innerHeight - eighthLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_playerLv_colon"))
			{
				panel_nextInfo->getChildByName("l_playerLv_colon")->setPosition(ccp(76,innerHeight - eighthLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_playerLvValue"))
			{
				panel_nextInfo->getChildByName("l_playerLvValue")->setPosition(ccp(85,innerHeight - eighthLineHeight));
			}
		}
		//技能点
		if (m_required_point>0)
		{
			if (panel_nextInfo->getChildByName("widget_skillPoint"))
			{
				panel_nextInfo->getChildByName("widget_skillPoint")->setPosition(ccp(8,innerHeight - ninthLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_skillPoint_colon"))
			{
				panel_nextInfo->getChildByName("l_skillPoint_colon")->setPosition(ccp(76,innerHeight - ninthLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_skillPointValue"))
			{
				panel_nextInfo->getChildByName("l_skillPointValue")->setPosition(ccp(85,innerHeight - ninthLineHeight));
			}
		}
		//XXX技能
		if (m_str_pre_skill != "")
		{
			if (panel_nextInfo->getChildByName("widget_pre_skill"))
			{
				panel_nextInfo->getChildByName("widget_pre_skill")->setPosition(ccp(8,innerHeight - tenthLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_preSkill_colon"))
			{
				panel_nextInfo->getChildByName("l_preSkill_colon")->setPosition(ccp(76,innerHeight - tenthLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_preSkillValue"))
			{
				panel_nextInfo->getChildByName("l_preSkillValue")->setPosition(ccp(85,innerHeight - tenthLineHeight));
			}
		}
		//金币
		if (m_required_gold > 0)
		{
			if (panel_nextInfo->getChildByName("widget_gold"))
			{
				panel_nextInfo->getChildByName("widget_gold")->setPosition(ccp(8,innerHeight - eleventhLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_gold_colon"))
			{
				panel_nextInfo->getChildByName("l_gold_colon")->setPosition(ccp(76,innerHeight - eleventhLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_goldValue"))
			{
				panel_nextInfo->getChildByName("l_goldValue")->setPosition(ccp(85,innerHeight - eleventhLineHeight));
			}
			if (panel_nextInfo->getChildByName("image_gold"))
			{
				panel_nextInfo->getChildByName("image_gold")->setPosition(ccp(88+panel_nextInfo->getChildByName("l_goldValue")->getContentSize().width,innerHeight - eleventhLineHeight));
			}
		}
		//技能书
		if(m_required_skillBookValue > 0)
		{
			if (panel_nextInfo->getChildByName("widget_skillBook"))
			{
				panel_nextInfo->getChildByName("widget_skillBook")->setPosition(ccp(8,innerHeight - twelfthLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_skillbook_colon"))
			{
				panel_nextInfo->getChildByName("l_skillbook_colon")->setPosition(ccp(76,innerHeight - twelfthLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_skillBookValue"))
			{
				panel_nextInfo->getChildByName("l_skillBookValue")->setPosition(ccp(85,innerHeight - twelfthLineHeight));
			}
		}
	}
	scrollView_skillInfo->jumpToTop();

	/*
	if (this->getChildByTag(kTag_ScrollView) != NULL)
	{
		this->getChildByTag(kTag_ScrollView)->removeFromParent();
	}

	int scroll_height = 0;
	//技能名称
	CCSprite * s_nameFrame = CCSprite::create("res_ui/moji_0.png");
	s_nameFrame->setScaleY(0.6f);
	CCLabelTTF * l_name = CCLabelTTF::create(gameFightSkill->getCBaseSkill()->name().c_str(),APP_FONT_NAME,18);
	l_name->setColor(ccc3(196,255,68));
	int zeroLineHeight = s_nameFrame->getContentSize().height;
	scroll_height = zeroLineHeight;

	//
	CCSprite * s_first_line = CCSprite::create("res_ui/henggang.png");
	s_first_line->setScaleX(0.9f);
	int zeroLineHeight_1 = zeroLineHeight + s_first_line->getContentSize().height+6;
	scroll_height = zeroLineHeight_1;

	//主动/被动
	CCLabelTTF * l_type;
	if (gameFightSkill->getCBaseSkill()->usemodel() == 0)
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_zhudongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		l_type = CCLabelTTF::create(skillinfo_zhudongjineng,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
	}
	else if (gameFightSkill->getCBaseSkill()->usemodel() == 2)
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_beidongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		l_type = CCLabelTTF::create(skillinfo_zhudongjineng,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
	}
	else
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_zhudongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		l_type = CCLabelTTF::create(skillinfo_zhudongjineng,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
	}
	l_type->setColor(ccc3(212,255,151));
	//等级
	//const char *dengji  = ((CCString*)strings->objectForKey("skillinfo_dengji"))->m_sString.c_str();
	const char *dengji = StringDataManager::getString("skillinfo_dengji");
	char* skillinfo_dengji =const_cast<char*>(dengji);
	CCLabelTTF * l_lv = CCLabelTTF::create(skillinfo_dengji,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
	l_lv->setColor(ccc3(212,255,151));
	char s_level[5];
	sprintf(s_level,"%d",gameFightSkill->getCFightSkill()->level());
	CCLabelTTF * l_lvValue = CCLabelTTF::create(s_level,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
	l_lvValue->setColor(ccc3(212,255,151));

	int firstLineHeight = zeroLineHeight_1 + l_type->getContentSize().height;
	scroll_height = firstLineHeight;
	///////////////////////2////////////////////////////
	//MP消耗
	//const char *mofaxiaohao  = ((CCString*)strings->objectForKey("skillinfo_mofaxiaohao"))->m_sString.c_str();
	const char *mofaxiaohao = StringDataManager::getString("skillinfo_mofaxiaohao");
	char* skillinfo_mofaxiaohao =const_cast<char*>(mofaxiaohao);
	CCLabelTTF * l_mp = CCLabelTTF::create(skillinfo_mofaxiaohao,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
	l_mp->setColor(ccc3(212,255,151));
	char s_mp[20];
	sprintf(s_mp,"%d",gameFightSkill->getCFightSkill()->mp());
	CCLabelTTF * l_mpValue = CCLabelTTF::create(s_mp,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
	l_mpValue->setColor(ccc3(212,255,151));
	//CD时间 
	//const char *lengqueshijian  = ((CCString*)strings->objectForKey("skillinfo_lengqueshijian"))->m_sString.c_str();
	const char *lengqueshijian = StringDataManager::getString("skillinfo_lengqueshijian");
	char* skillinfo_lengqueshijian =const_cast<char*>(lengqueshijian);
	CCLabelTTF * l_cd = CCLabelTTF::create(skillinfo_lengqueshijian,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
	l_cd->setColor(ccc3(212,255,151));
	char s_intervaltime[20];
	sprintf(s_intervaltime,"%d",gameFightSkill->getCFightSkill()->intervaltime()/1000);
	CCLabelTTF * l_cdValue = CCLabelTTF::create(s_intervaltime,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
	l_cdValue->setColor(ccc3(212,255,151));

	int secondLineHeight = firstLineHeight + l_mp->getContentSize().height;
	scroll_height = secondLineHeight;
	///////////////////////3////////////////////////////
	//施法距离
	//const char *shifajuli  = ((CCString*)strings->objectForKey("skillinfo_shifajuli"))->m_sString.c_str();
	const char *shifajuli = StringDataManager::getString("skillinfo_shifajuli");
	char* skillinfo_shifajuli =const_cast<char*>(shifajuli);
	CCLabelTTF * l_dis = CCLabelTTF::create(skillinfo_shifajuli,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
	l_dis->setColor(ccc3(212,255,151));
	char s_distance[20];
	sprintf(s_distance,"%d",gameFightSkill->getCFightSkill()->distance());
	CCLabelTTF * l_disValue = CCLabelTTF::create(s_distance,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
	l_disValue->setColor(ccc3(212,255,151));
	//职业
	//const char *zhiye  = ((CCString*)strings->objectForKey("skillinfo_zhiye"))->m_sString.c_str();
	const char *zhiye = StringDataManager::getString("skillinfo_zhiye");
	char* skillinfo_zhiye =const_cast<char*>(zhiye);
	CCLabelTTF * l_pro = CCLabelTTF::create(skillinfo_zhiye,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
	l_pro->setColor(ccc3(212,255,151));
	CCLabelTTF * l_proValue = CCLabelTTF::create(this->getProfessionBySkill(gameFightSkill->getId()).c_str(),APP_FONT_NAME,16,CCSizeMake(100, 0 ), kCCTextAlignmentLeft);
	l_proValue->setColor(ccc3(212,255,151));

	int thirdLineHeight = secondLineHeight + l_dis->getContentSize().height;
	scroll_height = thirdLineHeight;
	////////////////////4///////////////////////////////
	//简介
	CCLabelTTF * t_des = CCLabelTTF::create(gameFightSkill->getCFightSkill()->description().c_str(),APP_FONT_NAME,16,CCSizeMake(235, 0 ), kCCTextAlignmentLeft);
	t_des->setColor(ccc3(30,255,0));

	int fourthLineHeight = thirdLineHeight+t_des->getContentSize().height;
	scroll_height = fourthLineHeight;
	/////////////////////////////////////////////////////
	int fifthLineHeight = 0;
	int sixthLineHeight = 0;
	int seventhLineHeight = 0;
	int seventhLineHeight_1 = 0;
	int eighthLineHeight = 0;
	int ninthLineHeight = 0;
	int tenthLineHeight = 0;
	int eleventhLineHeight = 0;
	int twelfthLineHeight = 0;
	if (gameFightSkill->getLevel()<gameFightSkill->getCBaseSkill()->maxlevel())
	{
		//下一级
		//CCDictionary *strings = CCDictionary::createWithContentsOfFile("res_ui/font/strings.xml");
		//const char *xiayiji  = ((CCString*)strings->objectForKey("skillinfo_xiayiji"))->m_sString.c_str();
		const char *xiayiji = StringDataManager::getString("skillinfo_xiayiji");
		char* skillinfo_xiayiji =const_cast<char*>(xiayiji);
		l_nextLv = CCLabelTTF::create(skillinfo_xiayiji,APP_FONT_NAME,16,CCSizeMake(100, 0 ), kCCTextAlignmentLeft);
		l_nextLv->setColor(ccc3(0,255,255));

		fifthLineHeight = fourthLineHeight+l_nextLv->getContentSize().height*2;
		scroll_height = fifthLineHeight;
		/////////////////////////////////////////////////////
		t_nextLvDes = CCLabelTTF::create(gameFightSkill->getCFightSkill()->get_next_description().c_str(),APP_FONT_NAME,16,CCSizeMake(235, 0 ), kCCTextAlignmentLeft);
		t_nextLvDes->setColor(ccc3(0,255,255));

		sixthLineHeight = fifthLineHeight+t_nextLvDes->getContentSize().height;
		scroll_height = sixthLineHeight;
		/////////////////////////////////////////////////////
		//升级需求
		s_upgradeNeed = CCSprite::create("res_ui/moji_0.png");
		s_upgradeNeed->setScaleY(0.6f);
		//const char *shenjixuqiu  = ((CCString*)strings->objectForKey("skillinfo_shengjixuqiu"))->m_sString.c_str();
		const char *shenjixuqiu = StringDataManager::getString("skillinfo_shengjixuqiu");
		char* skillinfo_shenjixuqiu =const_cast<char*>(shenjixuqiu);
		l_upgradeNeed = CCLabelTTF::create(skillinfo_shenjixuqiu,APP_FONT_NAME,18);
		l_upgradeNeed->setColor(ccc3(196,255,68));
		seventhLineHeight = sixthLineHeight + s_upgradeNeed->getContentSize().height;
		scroll_height = seventhLineHeight;

		//////////////////////////////////////////////////////
		//
		s_second_line = CCSprite::create("res_ui/henggang.png");
		s_second_line->setScaleX(0.9f);
		seventhLineHeight_1 = seventhLineHeight + s_first_line->getContentSize().height+7;
		scroll_height = seventhLineHeight_1;

		/////////////////////////////////////////////////////
		//主角等级
		if (gameFightSkill->getCFightSkill()->get_next_required_level()>0)
		{
						UILabel * l_playerLv = UILabel::create();
			l_playerLv->setText(StringDataManager::getString("skillinfo_zhujuedengji"));
			l_playerLv->setFontName(APP_FONT_NAME);
			l_playerLv->setFontSize(16);
			l_playerLv->setColor(ccc3(212,255,151));
			l_playerLv->setAnchorPoint(ccp(0,0));
			l_playerLv->setName("l_playerLv");
			panel_nextInfo->addChild(l_playerLv);

			UILabel * l_playerLv_colon = UILabel::create();
			l_playerLv_colon->setText(":");
			l_playerLv_colon->setFontName(APP_FONT_NAME);
			l_playerLv_colon->setFontSize(16);
			l_playerLv_colon->setColor(ccc3(212,255,151));
			l_playerLv_colon->setAnchorPoint(ccp(0,0));
			l_playerLv_colon->setName("l_playerLv_colon");
			panel_nextInfo->addChild(l_playerLv_colon);

			char s_required_level[5];
			sprintf(s_required_level,"%d",m_required_level);
			UILabel * l_playerLvValue = UILabel::create();
			l_playerLvValue->setText(s_required_level);
			l_playerLvValue->setFontName(APP_FONT_NAME);
			l_playerLvValue->setFontSize(16);
			l_playerLvValue->setTextAreaSize(CCSizeMake(230, 0 ));
			l_playerLvValue->setColor(ccc3(212,255,151));
			l_playerLvValue->setAnchorPoint(ccp(0,0));
			l_playerLvValue->setName("l_playerLvValue");
			panel_nextInfo->addChild(l_playerLvValue);

			eighthLineHeight = seventhLineHeight_1 + l_playerLv->getContentSize().height;
			scroll_height = eighthLineHeight;

			if (GameView::getInstance()->myplayer->getActiveRole()->level()<m_required_level)  //人物等级未达到技能升级要求
			{
				l_playerLv->setColor(ccc3(255,51,51));
				l_playerLv_colon->setColor(ccc3(255,51,51));
				l_playerLvValue->setColor(ccc3(255,51,51));
			}



			const char *zhujuedengji = StringDataManager::getString("skillinfo_zhujuedengji");
			char* skillinfo_zhujuedengji =const_cast<char*>(zhujuedengji);
			l_playerLv = CCLabelTTF::create(skillinfo_zhujuedengji,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
			l_playerLv->setColor(ccc3(212,255,151));
			char s_next_required_level[20];
			sprintf(s_next_required_level,"%d",gameFightSkill->getCFightSkill()->get_next_required_level());
			l_playerLvValue = CCLabelTTF::create(s_next_required_level,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
			l_playerLvValue->setColor(ccc3(212,255,151));

			eighthLineHeight = seventhLineHeight_1 + l_playerLv->getContentSize().height;
			scroll_height = eighthLineHeight;

			if (GameView::getInstance()->myplayer->getActiveRole()->level()<gameFightSkill->getCFightSkill()->get_next_required_level())  //人物等级未达到技能升级要求
			{
				l_playerLv->setColor(ccc3(255,51,51));
				l_playerLvValue->setColor(ccc3(255,51,51));
			}
		}
		else
		{
			eighthLineHeight = seventhLineHeight_1;
			scroll_height = eighthLineHeight;
		}
		//技能点
		if (gameFightSkill->getCFightSkill()->get_next_required_point()>0)
		{
			//CCDictionary *strings = CCDictionary::createWithContentsOfFile("res_ui/font/strings.xml");
			//const char *jinengdian  = ((CCString*)strings->objectForKey("skillinfo_jinengdian"))->m_sString.c_str();
			const char *jinengdian = StringDataManager::getString("skillinfo_jinengdian");
			char* skillinfo_jinengdian =const_cast<char*>(jinengdian);
			l_skillPoint = CCLabelTTF::create(skillinfo_jinengdian,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
			l_skillPoint->setColor(ccc3(212,255,151));

			char s_next_required_point[20];
			sprintf(s_next_required_point,"%d",gameFightSkill->getCFightSkill()->get_next_required_point());
			l_skillPointValue = CCLabelTTF::create(s_next_required_point,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
			l_skillPointValue->setColor(ccc3(212,255,151));

			ninthLineHeight = eighthLineHeight + l_skillPoint->getContentSize().height;
			scroll_height = ninthLineHeight;

			if(GameView::getInstance()->myplayer->player->skillpoint()<gameFightSkill->getCFightSkill()->get_next_required_point())//技能点不足
			{
				l_skillPoint->setColor(ccc3(255,51,51));
				l_skillPointValue->setColor(ccc3(255,51,51));
			}
		}
		else
		{
			ninthLineHeight = eighthLineHeight;
			scroll_height = ninthLineHeight;
		}
		//XXX技能
		if (gameFightSkill->getCFightSkill()->get_pre_skill() != "")
		{
			std::string skillname = "";
			CBaseSkill * temp = StaticDataBaseSkill::s_baseSkillData[gameFightSkill->getCFightSkill()->get_pre_skill()];
			skillname.append(temp->name().c_str());
			skillname.append(": ");
			l_preSkill = CCLabelTTF::create(skillname.c_str(),APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
			l_preSkill->setColor(ccc3(212,255,151));
			char s_pre_skill_level[20];
			sprintf(s_pre_skill_level,"%d",gameFightSkill->getCFightSkill()->get_pre_skill_level());
			l_preSkillValue = CCLabelTTF::create(s_pre_skill_level,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
			l_preSkillValue->setColor(ccc3(212,255,151));

			tenthLineHeight = ninthLineHeight + l_preSkill->getContentSize().height;
			scroll_height = tenthLineHeight;

			GameFightSkill * gameFightSkillStuded = GameView::getInstance()->GameFightSkillList[gameFightSkill->getCFightSkill()->get_pre_skill()];
			if (gameFightSkillStuded)
			{
				if (gameFightSkillStuded->getLevel() < gameFightSkill->getCFightSkill()->get_next_pre_skill_level())
				{
					l_preSkill->setColor(ccc3(255,51,51));
					l_preSkillValue->setColor(ccc3(255,51,51));
				}
			}

		}
		else
		{
			tenthLineHeight = ninthLineHeight;
			scroll_height = tenthLineHeight;
		}
		//金币
		if (gameFightSkill->getCFightSkill()->get_next_required_gold()>0)
		{
			//CCDictionary *strings = CCDictionary::createWithContentsOfFile("res_ui/font/strings.xml");
			//const char *jinbi  = ((CCString*)strings->objectForKey("skillinfo_jinbi"))->m_sString.c_str();
			const char *jinbi = StringDataManager::getString("skillinfo_jinbi");
			char* skillinfo_jinbi =const_cast<char*>(jinbi);
			l_gold = CCLabelTTF::create(skillinfo_jinbi,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
			l_gold->setColor(ccc3(212,255,151));
			char s_next_required_gold[20];
			sprintf(s_next_required_gold,"%d",gameFightSkill->getCFightSkill()->get_next_required_gold());
			l_goldValue = CCLabelTTF::create(s_next_required_gold,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
			l_goldValue->setColor(ccc3(212,255,151));

			eleventhLineHeight = tenthLineHeight + l_gold->getContentSize().height;
			scroll_height = eleventhLineHeight;

			if(GameView::getInstance()->getPlayerGold()<gameFightSkill->getCFightSkill()->get_next_required_gold())//金币不足
			{
				l_gold->setColor(ccc3(255,51,51));
				l_goldValue->setColor(ccc3(255,51,51));
			}

		}
		else
		{
			eleventhLineHeight = tenthLineHeight;
			scroll_height = eleventhLineHeight;
		}
		//技能书
		if (gameFightSkill->getCFightSkill()->get_next_required_num()>0)
		{
			const char *jinengshu = StringDataManager::getString("skillinfo_jinengshu");
			char* skillinfo_jinengshu =const_cast<char*>(jinengshu);
			l_prop = CCLabelTTF::create(skillinfo_jinengshu,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
			l_prop->setColor(ccc3(212,255,151));
			char s_required_skillBook[20];
			sprintf(s_required_skillBook,"%d",gameFightSkill->getCFightSkill()->get_next_required_num());
			l_propValue = CCLabelTTF::create(s_required_skillBook,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
			l_propValue->setColor(ccc3(212,255,151));

			twelfthLineHeight = eleventhLineHeight + l_prop->getContentSize().height;
			scroll_height = twelfthLineHeight;

			int propNum = 0;
			for (int i = 0;i<GameView::getInstance()->AllPacItem.size();++i)
			{
				FolderInfo * folder = GameView::getInstance()->AllPacItem.at(i);
				if (!folder->has_goods())
					continue;

				if (folder->goods().id() == gameFightSkill->getCFightSkill()->get_required_prop())
				{
					propNum += folder->quantity();
				}
			}
			if(propNum<gameFightSkill->getCFightSkill()->get_next_required_num())//技能书不足
			{
				l_prop->setColor(ccc3(212,59,59));
				l_propValue->setColor(ccc3(212,59,59));
			}
		}
	}

	CCScrollView * m_scrollView = CCScrollView::create(CCSizeMake(240,238));
	m_scrollView->setViewSize(CCSizeMake(240, 238));
	m_scrollView->ignoreAnchorPointForPosition(false);
	m_scrollView->setTouchEnabled(true);
	m_scrollView->setDirection(kCCScrollViewDirectionVertical);
	m_scrollView->setAnchorPoint(ccp(0,0));
	m_scrollView->setPosition(ccp(24,84));
	m_scrollView->setBounceable(true);
	m_scrollView->setClippingToBounds(true);
	m_scrollView->setTag(kTag_ScrollView);
	addChild(m_scrollView);
	if (scroll_height > 239)
	{
		m_scrollView->setContentSize(CCSizeMake(240,scroll_height));
		m_scrollView->setContentOffset(ccp(0,238-scroll_height));  
	}
	else
	{
		m_scrollView->setContentSize(ccp(240,238));
	}
	m_scrollView->setClippingToBounds(true);

	s_nameFrame->ignoreAnchorPointForPosition(false);
	s_nameFrame->setAnchorPoint(ccp(0,0));
	m_scrollView->addChild(s_nameFrame);
	s_nameFrame->setPosition(ccp(8,m_scrollView->getContentSize().height - zeroLineHeight));

	m_scrollView->addChild(l_name);
	l_name->setPosition(ccp(15,m_scrollView->getContentSize().height - zeroLineHeight));

	m_scrollView->addChild(s_first_line);
	s_first_line->setPosition(ccp(5,m_scrollView->getContentSize().height - zeroLineHeight_1+5));

	l_type->ignoreAnchorPointForPosition( false );  
	l_type->setAnchorPoint( ccp( 0, 0 ) );  
	m_scrollView->addChild(l_type);
	l_type->setPosition(ccp(8,m_scrollView->getContentSize().height - firstLineHeight));

	l_mp->ignoreAnchorPointForPosition( false );  
	l_mp->setAnchorPoint( ccp( 0, 0 ) );  
	m_scrollView->addChild(l_mp);
	l_mp->setPosition(ccp(105,m_scrollView->getContentSize().height - firstLineHeight));

	l_mpValue->ignoreAnchorPointForPosition( false );  
	l_mpValue->setAnchorPoint( ccp( 0.5f, 0.5f ) );  
	m_scrollView->addChild(l_mpValue);
	l_mpValue->setPosition(ccp(105+l_mp->getContentSize().width+2,m_scrollView->getContentSize().height - firstLineHeight));

	l_lv->ignoreAnchorPointForPosition( false );  
	l_lv->setAnchorPoint( ccp( 0, 0 ) );  
	m_scrollView->addChild(l_lv);
	l_lv->setPosition(ccp(8,m_scrollView->getContentSize().height - secondLineHeight));

	l_lvValue->ignoreAnchorPointForPosition( false );  
	l_lvValue->setAnchorPoint( ccp( 0, 0 ) );  
	m_scrollView->addChild(l_lvValue);
	l_lvValue->setPosition(ccp(8+l_lv->getContentSize().width+2,m_scrollView->getContentSize().height - secondLineHeight));

	l_cd->ignoreAnchorPointForPosition( false );  
	l_cd->setAnchorPoint( ccp( 0, 0 ) );  
	m_scrollView->addChild(l_cd);
	l_cd->setPosition(ccp(105,m_scrollView->getContentSize().height - secondLineHeight));

	l_cdValue->ignoreAnchorPointForPosition( false );  
	l_cdValue->setAnchorPoint( ccp( 0, 0 ) );  
	m_scrollView->addChild(l_cdValue);
	l_cdValue->setPosition(ccp(105+l_cd ->getContentSize().width+2,m_scrollView->getContentSize().height - secondLineHeight));

	l_pro->ignoreAnchorPointForPosition( false );  
	l_pro->setAnchorPoint( ccp( 0, 0 ) );  
	m_scrollView->addChild(l_pro);
	l_pro->setPosition(ccp(8,m_scrollView->getContentSize().height - thirdLineHeight));

	l_proValue->ignoreAnchorPointForPosition( false );  
	l_proValue->setAnchorPoint( ccp( 0, 0 ) );  
	m_scrollView->addChild(l_proValue);
	l_proValue->setPosition(ccp(8+l_pro->getContentSize().width+2,m_scrollView->getContentSize().height - thirdLineHeight));

	l_dis->ignoreAnchorPointForPosition( false );  
	l_dis->setAnchorPoint( ccp( 0, 0 ) );  
	m_scrollView->addChild(l_dis);
	l_dis->setPosition(ccp(105,m_scrollView->getContentSize().height - thirdLineHeight));

	l_disValue->ignoreAnchorPointForPosition( false );  
	l_disValue->setAnchorPoint( ccp( 0, 0 ) );  
	m_scrollView->addChild(l_disValue);
	l_disValue->setPosition(ccp(105+l_dis->getContentSize().width+2,m_scrollView->getContentSize().height - thirdLineHeight));

	if (gameFightSkill->getCBaseSkill()->usemodel() == 2)
	{
		l_mp->setVisible(false);
		l_mpValue->setVisible(false);
		l_cd->setVisible(false);
		l_cdValue->setVisible(false);
		l_dis->setVisible(false);
		l_disValue->setVisible(false);
	}

	t_des->ignoreAnchorPointForPosition( false );  
	t_des->setAnchorPoint( ccp( 0, 0 ) );  
	m_scrollView->addChild(t_des);
	t_des->setPosition(ccp(8,m_scrollView->getContentSize().height - fourthLineHeight));

	if (gameFightSkill->getLevel()<gameFightSkill->getCBaseSkill()->maxlevel())
	{
		l_nextLv->ignoreAnchorPointForPosition( false );  
		l_nextLv->setAnchorPoint( ccp( 0, 0 ) );  
		m_scrollView->addChild(l_nextLv);
		l_nextLv->setPosition(ccp(8,m_scrollView->getContentSize().height - fifthLineHeight));

		t_nextLvDes->ignoreAnchorPointForPosition( false );  
		t_nextLvDes->setAnchorPoint( ccp( 0, 0 ) );  
		m_scrollView->addChild(t_nextLvDes);
		t_nextLvDes->setPosition(ccp(8,m_scrollView->getContentSize().height - sixthLineHeight));

		s_upgradeNeed->ignoreAnchorPointForPosition( false );  
		s_upgradeNeed->setAnchorPoint( ccp( 0, 0 ) );  
		m_scrollView->addChild(s_upgradeNeed);
		s_upgradeNeed->setPosition(ccp(8,m_scrollView->getContentSize().height - seventhLineHeight));

		m_scrollView->addChild(l_upgradeNeed);
		l_upgradeNeed->setPosition(ccp(15,m_scrollView->getContentSize().height - seventhLineHeight));

		m_scrollView->addChild(s_second_line);
		s_second_line->setPosition(ccp(5,m_scrollView->getContentSize().height - seventhLineHeight_1+5));

		//主角等级
		if (gameFightSkill->getCFightSkill()->get_next_required_level()>0)
		{
			l_playerLv->ignoreAnchorPointForPosition( false );  
			l_playerLv->setAnchorPoint( ccp( 0, 0 ) );  
			m_scrollView->addChild(l_playerLv);
			l_playerLv->setPosition(ccp(8,m_scrollView->getContentSize().height - eighthLineHeight));

			l_playerLvValue->ignoreAnchorPointForPosition( false );  
			l_playerLvValue->setAnchorPoint( ccp( 0, 0 ) );  
			m_scrollView->addChild(l_playerLvValue);
			l_playerLvValue->setPosition(ccp(8+l_playerLv->getContentSize().width+5,m_scrollView->getContentSize().height - eighthLineHeight));
		}
		//技能点
		if (gameFightSkill->getCFightSkill()->get_next_required_point()>0)
		{
			l_skillPoint->ignoreAnchorPointForPosition( false );  
			l_skillPoint->setAnchorPoint( ccp( 0, 0 ) );  
			m_scrollView->addChild(l_skillPoint);
			l_skillPoint->setPosition(ccp(8,m_scrollView->getContentSize().height - ninthLineHeight));

			l_skillPointValue->ignoreAnchorPointForPosition( false );  
			l_skillPointValue->setAnchorPoint( ccp( 0, 0 ) );  
			m_scrollView->addChild(l_skillPointValue);
			l_skillPointValue->setPosition(ccp(8+l_skillPoint->getContentSize().width+5,m_scrollView->getContentSize().height - ninthLineHeight));
		}
		//XXX技能
		if (gameFightSkill->getCFightSkill()->get_pre_skill() != "")
		{
			l_preSkill->ignoreAnchorPointForPosition( false );  
			l_preSkill->setAnchorPoint( ccp( 0, 0 ) );  
			m_scrollView->addChild(l_preSkill);
			l_preSkill->setPosition(ccp(8,m_scrollView->getContentSize().height - tenthLineHeight));

			l_preSkillValue->ignoreAnchorPointForPosition( false );  
			l_preSkillValue->setAnchorPoint( ccp( 0, 0 ) );  
			m_scrollView->addChild(l_preSkillValue);
			l_preSkillValue->setPosition(ccp(8+l_preSkill->getContentSize().width+5,m_scrollView->getContentSize().height - tenthLineHeight));
		}
		//金币
		if (gameFightSkill->getCFightSkill()->get_next_required_gold()>0)
		{
			l_gold->ignoreAnchorPointForPosition( false );  
			l_gold->setAnchorPoint( ccp( 0, 0 ) );  
			m_scrollView->addChild(l_gold);
			l_gold->setPosition(ccp(8,m_scrollView->getContentSize().height - eleventhLineHeight));

			l_goldValue->ignoreAnchorPointForPosition( false );  
			l_goldValue->setAnchorPoint( ccp( 0, 0 ) );  
			m_scrollView->addChild(l_goldValue);
			l_goldValue->setPosition(ccp(8+l_gold->getContentSize().width+5,m_scrollView->getContentSize().height - eleventhLineHeight));
		}
		//技能书
		if (gameFightSkill->getCFightSkill()->get_next_required_num()>0)
		{
			l_prop->ignoreAnchorPointForPosition( false );  
			l_prop->setAnchorPoint( ccp( 0, 0 ) );  
			m_scrollView->addChild(l_prop);
			l_prop->setPosition(ccp(8,m_scrollView->getContentSize().height - twelfthLineHeight));

			l_propValue->ignoreAnchorPointForPosition( false );  
			l_propValue->setAnchorPoint( ccp( 0, 0 ) );  
			m_scrollView->addChild(l_propValue);
			l_propValue->setPosition(ccp(8+l_prop->getContentSize().width+5,m_scrollView->getContentSize().height - twelfthLineHeight));
		}
	}
	*/
}


