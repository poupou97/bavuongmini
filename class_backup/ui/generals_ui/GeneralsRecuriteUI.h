#ifndef  _GENERALSUI_GENERALSRECURITEUI_H_
#define _GENERALSUI_GENERALSRECURITEUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../../messageclient/protobuf/RecruitMessage.pb.h"
#include "GeneralsListBase.h"
#include "../extensions/UIScene.h"
#include "../../legend_script/CCTutorialIndicator.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CGeneralBaseMsg;
class CGeneralDetail;
class CActionDetail;

/////////////////////////////////
/**
 * 武将界面下的“点将台”中的招募类
 * @author yangjun
 * @version 0.1.0
 * @date 2013.12.9
 */

class GeneralsRecuriteUI : public UIScene
{
public:
	GeneralsRecuriteUI();
	~GeneralsRecuriteUI();

	struct ReqData{
		int actionType;
		int recruitType;
	};

	enum RecuriteActionType{
		BASE = 1,
		BETTER = 2,
		BEST = 3,
		TEN_TIMES = 4,
	};

	
	static GeneralsRecuriteUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

public:
	void CloseEvent(CCObject *pSender);
	void NormalRecuritEvent(CCObject* pSender);
	void GoodRecuritEvent(CCObject* pSender);
	void WonderfulRecuritEvent(CCObject* pSender);
	void TenTimesRecuritEvent(CCObject* pSender);

	void NullGoodEvent(CCObject *pSender);
	void NullBetterEvent(CCObject *pSender);
	void NullBestEvent(CCObject *pSender);

	void createFiveBestGeneral();   /**  创建招贤馆的五张牛逼卡牌  **/
	void RefreshRecruitData();       /**  刷新招贤馆的三种招募的基本信息  **/
	void ReloadNormalType(CActionDetail * actionDetail);	

	//单次招募成功后显示
	void presentRecruiteResult(CGeneralDetail * generalDetail,int actionType ,int recruitGeneralCardType,bool isPresentBtn = true);
	//详情
	void DetailInfoEvent(CCObject *pSender);
	//再来一次
	void RecuriteAgainEvent(CCObject *pSender);
	
	//十连抽招募成功后显示
	void presentTenTimesRecruiteResult(std::vector<CGeneralDetail *> temp,bool isPresentBtn = true);

	virtual void setVisible(bool visible);

	RecuriteActionType getCurRecuriteActionType();


	//教学
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction );
	void removeCCTutorialIndicator();

public:
	UIPanel * panel_recruit; //招募面板
	UILayer * Layer_recruit;
	UILayer *Layer_upper;

	//普通招募
    cocos2d::extension::UIButton * Button_normalRecurit;
	//优秀招募
	cocos2d::extension::UIButton * Button_goodRecurit;
	//极品招募
	cocos2d::extension::UIButton * Button_wonderfulRecurit;
	//十顾茅庐
	cocos2d::extension::UIButton * Button_tenTimesRecurit;

private:
	RecuriteActionType curActionType;

	//单词招募成功后下发的相关信息
	CGeneralDetail * m_pCurGeneralDetail;
	int m_nActionType;
	bool m_bIsPresentBtn;

	//教学
	int mTutorialScriptInstanceId;
};


/////////////////////////////////
/**
 * 跑马灯（用于武将招募界面）
 * @author yangjun
 * @version 0.1.0
 * @date 2014.08.5
 */

class GeneralRecuritMarquee :public CCNode
{
public:
	GeneralRecuritMarquee();
	~GeneralRecuritMarquee();

	virtual void update(float dt);

	static GeneralRecuritMarquee * create(int width, int height);
	bool init(int width, int height);

private:
	CCPoint m_layerRecurite1Pos;
	CCPoint m_layerRecurite2Pos;
	int m_nWidth;
	int m_nHeight;

	CCLayer * l_recurite_1;
	CCLayer * l_recurite_2;
};


#endif

