#include "SingleRecuriteResultUI.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "GeneralsUI.h"
#include "GeneralsRecuriteUI.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "generals_popup_ui/GeneralsFateInfoUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/CShortCut.h"
#include "GeneralsShortcutSlot.h"
#include "NormalGeneralsHeadItem.h"
#include "RecuriteActionItem.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/protobuf/RecruitMessage.pb.h"
#include "../../messageclient/element/CActionDetail.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/Script.h"
#include "GeneralsListUI.h"
#include "../../gamescene_state/GameSceneEffects.h"
#include "../../legend_engine/CCLegendAnimation.h"

#define HeadItemTag 20
#define ShortSlotBaseTag 40

SingleRecuriteResultUI::SingleRecuriteResultUI()
{
	//curGeneralDetail = new CGeneralDetail();

	slotPos[0].x = 153;
	slotPos[0].y = 138;
	slotPos[1].x = 200;
	slotPos[1].y = 70;
	slotPos[2].x = 250;
	slotPos[2].y = 138;
	slotPos[3].x = 294;
	slotPos[3].y = 70;
	slotPos[4].x = 347;
	slotPos[4].y = 138;
}


SingleRecuriteResultUI::~SingleRecuriteResultUI()
{
	delete curGeneralDetail;
}

SingleRecuriteResultUI* SingleRecuriteResultUI::create()
{
	SingleRecuriteResultUI * singleRecuriteResultUI = new SingleRecuriteResultUI();
	if (singleRecuriteResultUI && singleRecuriteResultUI->init())
	{
		singleRecuriteResultUI->autorelease();
		return singleRecuriteResultUI;
	}
	CC_SAFE_DELETE(singleRecuriteResultUI);
	return NULL;
}

bool SingleRecuriteResultUI::init()
{
	if (UIScene::init())
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		this->scheduleUpdate();

		curGeneralDetail = new CGeneralDetail();
		//加载UI
		if(LoadSceneLayer::SingleRecuriteResultLayer->getWidgetParent() != NULL)
		{
			LoadSceneLayer::SingleRecuriteResultLayer->removeFromParentAndCleanup(false);
		}
		//ppanel = (UIPanel*)LoadSceneLayer::s_pPanel->copy();
		panel_generalsInfo = LoadSceneLayer::SingleRecuriteResultLayer;
		panel_generalsInfo->setAnchorPoint(ccp(0.0f,0.0f));
		panel_generalsInfo->getValidNode()->setContentSize(CCSizeMake(676, 419));
		panel_generalsInfo->setPosition(CCPointZero);
		panel_generalsInfo->setTouchEnable(true);
		m_pUiLayer->addWidget(panel_generalsInfo);

		panel_btn = (UIPanel*)UIHelper::seekWidgetByName(panel_generalsInfo,"Panel_btn");
		panel_btn->setVisible(true);

		UIScrollView * muScrollView = (UIScrollView*)UIHelper::seekWidgetByName(panel_generalsInfo,"ScrollView_property");
		muScrollView->setTouchEnable(true);
		muScrollView->setBounceEnabled(true);
		//招募成功后的详细信息
// 		panel_generalsInfo = (UIPanel*)UIHelper::seekWidgetByName(GeneralsUI::ppanel,"Panel_get_generals");
// 		panel_generalsInfo->setVisible(false);
		//招募成功后的层
		Layer_generalsInfo = UILayer::create();
		addChild(Layer_generalsInfo);
		Btn_close = (UIButton*)UIHelper::seekWidgetByName(panel_generalsInfo,"Button_close");
		Btn_close->setTouchEnable(true);
		Btn_close->setPressedActionEnabled(true);
		Btn_close->addReleaseEvent(this, coco_releaseselector(SingleRecuriteResultUI::CloseEvent));
		//招募成功后返回
		UIButton * Btn_recruitBack = (UIButton*)UIHelper::seekWidgetByName(panel_generalsInfo,"Button_back");
		Btn_recruitBack->setTouchEnable(true);
		Btn_recruitBack->setPressedActionEnabled(true);
		Btn_recruitBack->addReleaseEvent(this, coco_releaseselector(SingleRecuriteResultUI::RecruiteBackEvent));
		//招募成功后再抽一次（对应的）
		UIButton * Btn_recruitAgain = (UIButton*)UIHelper::seekWidgetByName(panel_generalsInfo,"Button_again");
		Btn_recruitAgain->setTouchEnable(true);
		Btn_recruitAgain->setPressedActionEnabled(true);
		Btn_recruitAgain->addReleaseEvent(this, coco_releaseselector(SingleRecuriteResultUI::RecruiteAgainEvent));

		//UIButton * Btn_Back = (UIButton *)UIHelper::seekWidgetByName(panel_generalsInfo,"Label_job"); 
		l_result_profession = (UILabel*)UIHelper::seekWidgetByName(panel_generalsInfo,"Label_job"); //职业
		l_result_combatPower = (UILabel*)UIHelper::seekWidgetByName(panel_generalsInfo,"Label_job_combat"); //战力
		l_result_hp = (UILabel*)UIHelper::seekWidgetByName(panel_generalsInfo,"Label_hp"); //血量值
        l_result_hp->setStrokeEnabled(true);
		Image_result_hp = (UIImageView*)UIHelper::seekWidgetByName(panel_generalsInfo,"ImageView_hp"); //血量
		l_result_mp = (UILabel*)UIHelper::seekWidgetByName(panel_generalsInfo,"Label_mp"); //法力值
        l_result_mp->setStrokeEnabled(true);
		Image_result_mp = (UIImageView*)UIHelper::seekWidgetByName(panel_generalsInfo,"ImageView_mp"); //法力
		l_result_powerValue = (UILabel*)UIHelper::seekWidgetByName(panel_generalsInfo,"Label_power");//力量
		l_result_aglieValue = (UILabel*)UIHelper::seekWidgetByName(panel_generalsInfo,"Label_aglie");  //敏捷
		l_result_intelligenceValue = (UILabel*)UIHelper::seekWidgetByName(panel_generalsInfo,"Label_intelligence");  //智力
		l_result_focusValue = (UILabel*)UIHelper::seekWidgetByName(panel_generalsInfo,"Label_focus");  //专注
		l_result_phyAttackValue = (UILabel*)UIHelper::seekWidgetByName(panel_generalsInfo,"Label_physical_attacks");
		l_result_magicAttackValue = (UILabel*)UIHelper::seekWidgetByName(panel_generalsInfo,"Label_magic_attacks");
		l_result_phyDenValue = (UILabel*)UIHelper::seekWidgetByName(panel_generalsInfo,"Label_power_physical_defense");
		l_result_magicDenValue = (UILabel*)UIHelper::seekWidgetByName(panel_generalsInfo,"Label_power_magic_defense");
		l_result_hitValue = (UILabel*)UIHelper::seekWidgetByName(panel_generalsInfo,"Label_hit");  //命中
		l_result_dodgeValue = (UILabel*)UIHelper::seekWidgetByName(panel_generalsInfo,"Label_dodge"); //闪避
		l_result_critValue = (UILabel*)UIHelper::seekWidgetByName(panel_generalsInfo,"Label_crit");  //暴击
		l_result_critDamageValue = (UILabel*)UIHelper::seekWidgetByName(panel_generalsInfo,"Label_crit_damage");//暴击伤害
		l_result_atkSpeedValue = (UILabel*)UIHelper::seekWidgetByName(panel_generalsInfo,"Label_attack_speed");

		btn_fate_1 = (UIButton*)UIHelper::seekWidgetByName(panel_generalsInfo,"Button_fate_1");
		btn_fate_1->setTouchEnable(true);
		btn_fate_1->setPressedActionEnabled(true);
		btn_fate_1->addReleaseEvent(this,coco_releaseselector(SingleRecuriteResultUI::FateEvent));
		btn_fate_2 = (UIButton*)UIHelper::seekWidgetByName(panel_generalsInfo,"Button_fate_2");
		btn_fate_2->setTouchEnable(true);
		btn_fate_2->setPressedActionEnabled(true);
		btn_fate_2->addReleaseEvent(this,coco_releaseselector(SingleRecuriteResultUI::FateEvent));
		btn_fate_3 = (UIButton*)UIHelper::seekWidgetByName(panel_generalsInfo,"Button_fate_3");
		btn_fate_3->setTouchEnable(true);
		btn_fate_3->setPressedActionEnabled(true);
		btn_fate_3->addReleaseEvent(this,coco_releaseselector(SingleRecuriteResultUI::FateEvent));
		btn_fate_4 = (UIButton*)UIHelper::seekWidgetByName(panel_generalsInfo,"Button_fate_6");
		btn_fate_4->setTouchEnable(true);
		btn_fate_4->setPressedActionEnabled(true);
		btn_fate_4->addReleaseEvent(this,coco_releaseselector(SingleRecuriteResultUI::FateEvent));
		btn_fate_5 = (UIButton*)UIHelper::seekWidgetByName(panel_generalsInfo,"Button_fate_5");
		btn_fate_5->setTouchEnable(true);
		btn_fate_5->setPressedActionEnabled(true);
		btn_fate_5->addReleaseEvent(this,coco_releaseselector(SingleRecuriteResultUI::FateEvent));
		btn_fate_6 = (UIButton*)UIHelper::seekWidgetByName(panel_generalsInfo,"Button_fate_4");
		btn_fate_6->setTouchEnable(true);
		btn_fate_6->setPressedActionEnabled(true);
		btn_fate_6->addReleaseEvent(this,coco_releaseselector(SingleRecuriteResultUI::FateEvent));

		l_fate_1 = (UILabel *)UIHelper::seekWidgetByName(panel_generalsInfo,"Label_fate_1");
		l_fate_1->setText("");
		l_fate_2 = (UILabel *)UIHelper::seekWidgetByName(panel_generalsInfo,"Label_fate_2");
		l_fate_2->setText("");
		l_fate_3 = (UILabel *)UIHelper::seekWidgetByName(panel_generalsInfo,"Label_fate_3");
		l_fate_3->setText("");
		l_fate_4 = (UILabel *)UIHelper::seekWidgetByName(panel_generalsInfo,"Label_fate_6");
		l_fate_4->setText("");
		l_fate_5 = (UILabel *)UIHelper::seekWidgetByName(panel_generalsInfo,"Label_fate_5");
		l_fate_5->setText("");
		l_fate_6 = (UILabel *)UIHelper::seekWidgetByName(panel_generalsInfo,"Label_fate_4");
		l_fate_6->setText("");

		lable_singleCost_gold = (UILabel*)UIHelper::seekWidgetByName(panel_generalsInfo,"Label_IngotValue");

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(CCSizeMake(676,419));

		return true;
	}
	return false;
}

void SingleRecuriteResultUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void SingleRecuriteResultUI::onExit()
{
	UIScene::onExit();
}

bool SingleRecuriteResultUI::ccTouchBegan(CCTouch *touch, CCEvent * pEvent)
{
	//return this->resignFirstResponder(touch,this,false);
	return true;
}

void SingleRecuriteResultUI::ccTouchEnded(CCTouch *touch, CCEvent * pEvent)
{
}

void SingleRecuriteResultUI::ccTouchCancelled(CCTouch *touch, CCEvent * pEvent)
{
}

void SingleRecuriteResultUI::ccTouchMoved(CCTouch *touch, CCEvent * pEvent)
{
}

void SingleRecuriteResultUI::CloseEvent(CCObject * pSender)
{
	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(pSender);

	this->closeAnim();
}

void SingleRecuriteResultUI::RecruiteBackEvent( CCObject *pSender )
{
	this->closeAnim();
}

void SingleRecuriteResultUI::RecruiteAgainEvent( CCObject *pSender )
{
	if (GeneralsUI::generalsRecuriteUI->getCurRecuriteActionType() == BASE)
	{
		GeneralsRecuriteUI::ReqData* tempdata = new GeneralsRecuriteUI::ReqData();
		tempdata->actionType = BASE;
		if (GeneralsUI::generalsRecuriteUI->Layer_recruit->getChildByTag(BASERECURITEACTIONITEMTAG))
		{
			RecuriteActionItem * temp = (RecuriteActionItem *)GeneralsUI::generalsRecuriteUI->Layer_recruit->getChildByTag(BASERECURITEACTIONITEMTAG);
			if (temp->state == RecuriteActionItem::Free)
			{
				tempdata->recruitType = FREE;
			}
			else
			{
				tempdata->recruitType = GOLD;
			}
		}

		GameMessageProcessor::sharedMsgProcessor()->sendReq(5061, tempdata);
		delete tempdata;
	}
	else if (GeneralsUI::generalsRecuriteUI->getCurRecuriteActionType() == BETTER)
	{
		GeneralsRecuriteUI::ReqData* tempdata = new GeneralsRecuriteUI::ReqData();
		tempdata->actionType = BETTER;
		if (GeneralsUI::generalsRecuriteUI->Layer_recruit->getChildByTag(BETTERRECURITEACTIONITEMTAG))
		{
			RecuriteActionItem * temp = (RecuriteActionItem *)GeneralsUI::generalsRecuriteUI->Layer_recruit->getChildByTag(BETTERRECURITEACTIONITEMTAG);
			if (temp->state == RecuriteActionItem::Free)
			{
				tempdata->recruitType = FREE;
			}
			else
			{
				tempdata->recruitType = GOLD;
			}
		}

		GameMessageProcessor::sharedMsgProcessor()->sendReq(5061, tempdata);
		delete tempdata;
	}
	else if (GeneralsUI::generalsRecuriteUI->getCurRecuriteActionType() == BEST)
	{
		GeneralsRecuriteUI::ReqData* tempdata = new GeneralsRecuriteUI::ReqData();
		tempdata->actionType = BEST;
		if (GeneralsUI::generalsRecuriteUI->Layer_recruit->getChildByTag(BESTRECURITEACTIONITEMTAG))
		{
			RecuriteActionItem * temp = (RecuriteActionItem *)GeneralsUI::generalsRecuriteUI->Layer_recruit->getChildByTag(BESTRECURITEACTIONITEMTAG);
			if (temp->state == RecuriteActionItem::Free)
			{
				tempdata->recruitType = FREE;
			}
			else
			{
				tempdata->recruitType = GOLD;
			}
		}

		GameMessageProcessor::sharedMsgProcessor()->sendReq(5061, tempdata);
		delete tempdata;
	}
}

void SingleRecuriteResultUI::RefreshRecruiteCost(int actionType)
{
	for (int i =0;i<GameView::getInstance()->actionDetailList.size();++i)
	{
		if (GameView::getInstance()->actionDetailList.at(i)->type() == actionType)
		{
			char s_gold[20];
			sprintf(s_gold,"%d",GameView::getInstance()->actionDetailList.at(i)->gold());
			this->lable_singleCost_gold->setText(s_gold);
		}
	}
}

void SingleRecuriteResultUI::RefreshRecruiteResult( CGeneralDetail * generalDetail )
{
	curGeneralDetail->CopyFrom(*generalDetail);

	l_result_profession->setText(generalDetail->profession().c_str()); //职业

	char s_fightpoint[30];
	sprintf(s_fightpoint,"%d",generalDetail->fightpoint());
	l_result_combatPower->setText(s_fightpoint) ; //战力

	std::string hpValue = "";
	char s_hp[30];
	sprintf(s_hp,"%d",generalDetail->activerole().hp());
	hpValue.append(s_hp);
	hpValue.append("/");
	char s_maxhp[30];
	sprintf(s_maxhp,"%d",generalDetail->activerole().maxhp());
	hpValue.append(s_maxhp);
	l_result_hp->setText(hpValue.c_str());//血量值

	float hpScaleValue = (generalDetail->activerole().hp()*1.0f)/(generalDetail->activerole().maxhp()*1.0f);
	//Image_result_hp->setScale(hpScaleValue);
	if (hpScaleValue >= 1.0f)
		hpScaleValue = 1.0f;

	Image_result_hp->setTextureRect(CCRectMake(0,0,Image_result_hp->getContentSize().width*hpScaleValue,Image_result_hp->getContentSize().height));


	std::string mpValue = "";
	char s_mp[30];
	sprintf(s_mp,"%d",generalDetail->activerole().mp());
	mpValue.append(s_mp);
	mpValue.append("/");
	char s_maxmp[30];
	sprintf(s_maxmp,"%d",generalDetail->activerole().maxmp());
	mpValue.append(s_maxmp);
	l_result_mp->setText(mpValue.c_str());//法力值

	float mpScaleValue = (generalDetail->activerole().mp()*1.0f)/(generalDetail->activerole().maxmp()*1.0f);
	//Image_result_mp->setScale(mpScaleValue);
	if (mpScaleValue >= 1.0f)
		mpScaleValue = 1.0f;

	Image_result_mp->setTextureRect(CCRectMake(0,0,Image_result_mp->getContentSize().width*mpScaleValue,Image_result_mp->getContentSize().height));

	char s_strength[30];
	sprintf(s_strength,"%d",generalDetail->aptitude().strength());
	l_result_powerValue->setText(s_strength) ; //力量

	char s_dexterity[30];
	sprintf(s_dexterity,"%d",generalDetail->aptitude().dexterity());
	l_result_aglieValue->setText(s_dexterity) ;  //敏捷

	char s_intelligence[30];
	sprintf(s_intelligence,"%d",generalDetail->aptitude().intelligence());
	l_result_intelligenceValue->setText(s_intelligence) ;  //智力

	char s_focus[30];
	sprintf(s_focus,"%d",generalDetail->aptitude().focus());
	l_result_focusValue->setText(s_focus) ;  //专注

	std::string phyAttackValue = "";
	char s_minattack[30];
	sprintf(s_minattack,"%d",generalDetail->aptitude().minattack());
	phyAttackValue.append(s_minattack);
	phyAttackValue.append("-");
	char s_maxattack[30];
	sprintf(s_maxattack,"%d",generalDetail->aptitude().maxattack());
	phyAttackValue.append(s_maxattack);
	l_result_phyAttackValue->setText(phyAttackValue.c_str());

	std::string magicAttackValue = "";
	char s_minmagicattack[30];
	sprintf(s_minmagicattack,"%d",generalDetail->aptitude().minmagicattack());
	magicAttackValue.append(s_minmagicattack);
	magicAttackValue.append("-");
	char s_maxmagicattack[30];
	sprintf(s_maxmagicattack,"%d",generalDetail->aptitude().maxmagicattack());
	magicAttackValue.append(s_maxmagicattack);
	l_result_magicAttackValue->setText(magicAttackValue.c_str());

	std::string phyDenValue = "";
	char s_mindefend[30];
	sprintf(s_mindefend,"%d",generalDetail->aptitude().mindefend());
	phyDenValue.append(s_mindefend);
	phyDenValue.append("-");
	char s_maxdefend[30];
	sprintf(s_maxdefend,"%d",generalDetail->aptitude().maxdefend());
	phyDenValue.append(s_maxdefend);
	l_result_phyDenValue->setText(phyDenValue.c_str());

	std::string magicDenValue = "";
	char s_minmagicdefend[30];
	sprintf(s_minmagicdefend,"%d",generalDetail->aptitude().minmagicdefend());
	magicDenValue.append(s_minmagicdefend);
	magicDenValue.append("-");
	char s_maxmagicdefend[30];
	sprintf(s_maxmagicdefend,"%d",generalDetail->aptitude().maxmagicdefend());
	magicDenValue.append(s_maxmagicdefend);
	l_result_magicDenValue->setText(magicDenValue.c_str());

	char s_aptitude[30];
	sprintf(s_aptitude,"%d",generalDetail->aptitude().hit());
	l_result_hitValue->setText(s_aptitude); //命中
	char s_dodge[30];
	sprintf(s_dodge,"%d",generalDetail->aptitude().dodge());
	l_result_dodgeValue->setText(s_dodge); //闪避
	char s_crit[30];
	sprintf(s_crit,"%d",generalDetail->aptitude().crit());
	l_result_critValue->setText(s_crit); //暴击
	char s_critdamage[30];
	sprintf(s_critdamage,"%d",generalDetail->aptitude().critdamage());
	l_result_critDamageValue->setText(s_critdamage); //暴击伤害
	char s_attackspeed[30];
	sprintf(s_attackspeed,"%d",generalDetail->aptitude().attackspeed());
	l_result_atkSpeedValue->setText(s_attackspeed); //攻速

	setSkillTypeByGeneralsDetail(generalDetail);
	RefreshGeneralsFate(generalDetail);
//	RefreshGeneralsSkill(generalDetail);
	RefreshGeneralsHead(generalDetail);
}


void SingleRecuriteResultUI::FateEvent( CCObject *pSender )
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsFateInfoUI) == NULL)
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		GeneralsFateInfoUI * generalsFateInfoUI = GeneralsFateInfoUI::create(curGeneralDetail);
		generalsFateInfoUI->ignoreAnchorPointForPosition(false);
		generalsFateInfoUI->setAnchorPoint(ccp(0.5f,0.5f));
		generalsFateInfoUI->setPosition(ccp(winsize.width/2,winsize.height/2));
		GameView::getInstance()->getMainUIScene()->addChild(generalsFateInfoUI,0,kTagGeneralsFateInfoUI);
	}
}

void SingleRecuriteResultUI::RefreshGeneralsFate( CGeneralDetail * generalDetail )
{
	l_fate_1->setText("");
	l_fate_2->setText("");
	l_fate_3->setText("");
	l_fate_4->setText("");
	l_fate_5->setText("");
	l_fate_6->setText("");

	btn_fate_1->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
	btn_fate_2->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
	btn_fate_3->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
	btn_fate_4->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
	btn_fate_5->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
	btn_fate_6->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");

	if (generalDetail->fates_size()>0)
	{
		for (int i = 0;i<generalDetail->fates_size();++i)
		{
			if (i == 0)
			{
				l_fate_1->setText(generalDetail->fates(i).fatename().c_str());
				if (generalDetail->fates(i).actived())
				{
					btn_fate_1->setTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				}
			}
			else if(i == 1)
			{
				l_fate_2->setText(generalDetail->fates(i).fatename().c_str());
				if (generalDetail->fates(i).actived())
				{
					btn_fate_2->setTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				}
			}
			else if(i == 2)
			{
				l_fate_3->setText(generalDetail->fates(i).fatename().c_str());
				if (generalDetail->fates(i).actived())
				{
					btn_fate_3->setTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				}
			}
			else if(i == 3)
			{
				l_fate_4->setText(generalDetail->fates(i).fatename().c_str());
				if (generalDetail->fates(i).actived())
				{
					btn_fate_4->setTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				}
			}
			else if(i == 4)
			{
				l_fate_5->setText(generalDetail->fates(i).fatename().c_str());
				if (generalDetail->fates(i).actived())
				{
					btn_fate_5->setTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				}
			}
			else if(i == 5)
			{
				l_fate_6->setText(generalDetail->fates(i).fatename().c_str());
				if (generalDetail->fates(i).actived())
				{
					btn_fate_6->setTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				}
			}
		}
	}
}

void SingleRecuriteResultUI::RefreshGeneralsSkill( CGeneralDetail * generalDetail )
{
	for (int i = 0;i<5;++i)
	{
		if (Layer_generalsInfo->getChildByTag(ShortSlotBaseTag+i))
		{
			Layer_generalsInfo->getChildByTag(ShortSlotBaseTag+i)->removeFromParent();
		}
	}

	if (generalDetail->shortcuts_size()>0)
	{
		for (int i = 0;i<generalDetail->shortcuts_size();++i)
		{
			CShortCut * shortcut = new CShortCut();
			shortcut->CopyFrom(generalDetail->shortcuts(i));
			GeneralsShortcutSlot * genentalsShortcutSlot = GeneralsShortcutSlot::create(generalDetail->rare(),shortcut);
			delete shortcut;
			genentalsShortcutSlot->ignoreAnchorPointForPosition(false);
			genentalsShortcutSlot->setAnchorPoint(ccp(0.5f,0.5f));
			genentalsShortcutSlot->setPosition(slotPos[i]);
			genentalsShortcutSlot->setTag(ShortSlotBaseTag+i);
			genentalsShortcutSlot->setSkillProfession(curSkillType);
			genentalsShortcutSlot->setTouchEnabled(false);
			Layer_generalsInfo->addChild(genentalsShortcutSlot);
		}
		for (int i = generalDetail->shortcuts_size();i<5;++i)
		{
			GeneralsShortcutSlot * genentalsShortcutSlot = GeneralsShortcutSlot::create(generalDetail->rare(),i);
			genentalsShortcutSlot->ignoreAnchorPointForPosition(false);
			genentalsShortcutSlot->setAnchorPoint(ccp(0.5f,0.5f));
			genentalsShortcutSlot->setPosition(slotPos[i]);
			genentalsShortcutSlot->setTag(ShortSlotBaseTag+i);
			genentalsShortcutSlot->setSkillProfession(curSkillType);
			genentalsShortcutSlot->setTouchEnabled(false);
			Layer_generalsInfo->addChild(genentalsShortcutSlot);
		}
	}
	else
	{
		for (int i = 0;i<5;++i)
		{
			GeneralsShortcutSlot * genentalsShortcutSlot = GeneralsShortcutSlot::create(generalDetail->rare(),i);
			genentalsShortcutSlot->ignoreAnchorPointForPosition(false);
			genentalsShortcutSlot->setAnchorPoint(ccp(0.5f,0.5f));
			genentalsShortcutSlot->setPosition(slotPos[i]);
			genentalsShortcutSlot->setTag(ShortSlotBaseTag+i);
			genentalsShortcutSlot->setSkillProfession(curSkillType);
			genentalsShortcutSlot->setTouchEnabled(false);
			Layer_generalsInfo->addChild(genentalsShortcutSlot);
		}
	}
}

void SingleRecuriteResultUI::RefreshGeneralsHead( CGeneralDetail * generalDetail )
{
	if (Layer_generalsInfo->getChildByTag(HeadItemTag))
	{
		Layer_generalsInfo->getChildByTag(HeadItemTag)->removeFromParent();
	}

	GeneralsHeadItemBase * headItem = GeneralsHeadItemBase::create(curGeneralDetail->modelid());
	headItem->setAnchorPoint(ccp(0,0));
	headItem->setPosition(ccp(120,239));
	headItem->setTag(HeadItemTag);
	Layer_generalsInfo->addChild(headItem);
	
	
	
	/*********************************    first  ************************************/
// 	GeneralsHeadItemBase * headItem = GeneralsHeadItemBase::create(curGeneralDetail->modelid());
// 	headItem->setAnchorPoint(ccp(0.5f,0.5f));
// 	headItem->setPosition(ccp(70,190));
// 	headItem->setTag(HeadItemTag);
// 	headItem->setScale(0.01f);
// 	Layer_generalsInfo->addChild(headItem);
// 
// 	ccBezierConfig bezier;
// 	bezier.controlPoint_1 = ccp(70,190);
// 	bezier.controlPoint_2 = ccp(384,300);
// 	bezier.endPosition = ccp(174,280);
// 	CCFiniteTimeAction * sequence = CCSequence::create(
// 		CCSpawn::create(CCBezierTo::create(1.0f,bezier),CCScaleTo::create(1.0f,1.3f),CCRotateBy::create(1.0f,720,0),NULL),
// 		CCSpawn::create(CCDelayTime::create(0.5f),CCCallFunc::create(this,callfunc_selector(SingleRecuriteResultUI::createParticleSys)),NULL),
// 		CCSpawn::create(CCMoveTo::create(0.15f,ccp(120,239)),CCScaleTo::create(0.15f,1.0f),NULL),
// 		NULL);
// 	headItem->runAction(sequence);

	/*********************************    second  ************************************/
	/*
	GeneralsHeadItemBase * headItem = GeneralsHeadItemBase::create(curGeneralDetail->modelid());
	headItem->setAnchorPoint(ccp(0,0));
	headItem->setPosition(ccp(120,239));
	headItem->setTag(HeadItemTag);
	headItem->showSilhouetteEffect();
	Layer_generalsInfo->addChild(headItem);

	CCFiniteTimeAction * sequence = CCSequence::create(
		CCDelayTime::create(0.4f),
		CCCallFunc::create(this,callfunc_selector(SingleRecuriteResultUI::createParticleSys)),
		NULL);
	headItem->runAction(sequence);

	if (headItem->getLegendHead())
	{
		CCFiniteTimeAction * action = CCSequence::create(
			CCDelayTime::create(0.8f),
			CCTintTo::create(0.3f, 255,255,255),
			NULL);
		headItem->getLegendHead()->runAction(action);
	}

	CCFiniteTimeAction * action1 = CCSequence::create(
		CCDelayTime::create(0.8f),
		CCTintTo::create(0.3f, 255,255,255),
		NULL);
	headItem->label_name->runAction(action1);

	CCFiniteTimeAction * action2 = CCSequence::create(
		CCDelayTime::create(0.8f),
		CCTintTo::create(0.3f, 255,255,255),
		NULL);
	headItem->imageView_star->runAction(action2);

	if (headItem->getFiveStarAnm())
	{
		CCFiniteTimeAction * action3 = CCSequence::create(
			CCDelayTime::create(0.8f),
			CCTintTo::create(0.3f, 255,255,255),
			NULL);
		headItem->getFiveStarAnm()->runAction(action3);
	}
	*/
}

void SingleRecuriteResultUI::setSkillTypeByGeneralsDetail( CGeneralDetail *generalsDetail )
{
	if(generalsDetail->profession() == PROFESSION_MJ_NAME)
	{
		curSkillType = GeneralsSkillsUI::Warrior;
	}
	else if (generalsDetail->profession() == PROFESSION_GM_NAME)
	{
		curSkillType = GeneralsSkillsUI::Magic;
	}
	else if (generalsDetail->profession()  == PROFESSION_HJ_NAME)
	{
		curSkillType = GeneralsSkillsUI::Ranger;
	}
	else if (generalsDetail->profession()  == PROFESSION_SS_NAME)
	{
		curSkillType = GeneralsSkillsUI::Warlock;
	}
}

void SingleRecuriteResultUI::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void SingleRecuriteResultUI::addCCTutorialIndicator1( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,20,40);
	tutorialIndicator->setPosition(ccp(pos.x-60,pos.y-75));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	this->addChild(tutorialIndicator);
}

void SingleRecuriteResultUI::removeCCTutorialIndicator()
{
	CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();
}

void SingleRecuriteResultUI::createParticleSys()
{
	//CCParticleSystem* particleEffect = CCParticleSystemQuad::create("animation/texiao/particledesigner/shanshuo3.plist");
	//particleEffect->setPositionType(kCCPositionTypeGrouped);
	//particleEffect->setPosition(ccp(225,262));
	//particleEffect->setScale(1.3f);
	//addChild(particleEffect);
	//particleEffect->setVisible(false);
	//CCFiniteTimeAction * action = CCSequence::create(
	//	CCDelayTime::create(0.0f),
	//	CCShow::create(),
	//	NULL);
	//particleEffect->runAction(action);

	//CCParticleSystem* particleEffect1 = CCParticleSystemQuad::create("animation/texiao/particledesigner/shanshuo1.plist");
	//particleEffect1->setPositionType(kCCPositionTypeGrouped);
	//particleEffect1->setPosition(ccp(174+35-45,280+160-30));
	//particleEffect1->setScale(1.3f);
	//addChild(particleEffect1);
	//particleEffect1->setVisible(false);
	//CCFiniteTimeAction * action1 = CCSequence::create(
	//	CCDelayTime::create(0.15f),
	//	CCShow::create(),
	//	NULL);
	//particleEffect1->runAction(action1);
	//CCParticleSystem* particleEffect2 = CCParticleSystemQuad::create("animation/texiao/particledesigner/shanshuo2.plist");
	//particleEffect2->setPositionType(kCCPositionTypeGrouped);
	//particleEffect2->setPosition(ccp(174-20-45,280+80-30));
	//particleEffect2->setScale(1.3f);
	//addChild(particleEffect2);
	//particleEffect2->setVisible(false);
	//CCFiniteTimeAction * action2 = CCSequence::create(
	//	CCDelayTime::create(0.4f),
	//	CCShow::create(),
	//	NULL);
	//particleEffect2->runAction(action2);
	//CCParticleSystem* particleEffect3 = CCParticleSystemQuad::create("animation/texiao/particledesigner/shanshuo3.plist");
	//particleEffect3->setPositionType(kCCPositionTypeGrouped);
	//particleEffect3->setPosition(ccp(174+120-45,280+120-30));
	//particleEffect3->setScale(1.3f);
	//addChild(particleEffect3);
	//particleEffect3->setVisible(false);
	//CCFiniteTimeAction * action3 = CCSequence::create(
	//	CCDelayTime::create(0.7f),
	//	CCShow::create(),
	//	NULL);
	//particleEffect3->runAction(action3);

	// Bonus Special Effect
	CCNodeRGBA* pNode = BonusSpecialEffect::create();
	pNode->setPosition(ccp(175, 315));
	addChild(pNode);
}

void SingleRecuriteResultUI::RefreshBtnPresent( bool isPresent )
{
	panel_btn->setVisible(isPresent);
}
