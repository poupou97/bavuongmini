
#include "NormalGeneralsHeadItem.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "GeneralsHeadItemBase.h"
#include "../../legend_engine/CCLegendAnimation.h"


NormalGeneralsHeadItemBase::NormalGeneralsHeadItemBase()
{

}

NormalGeneralsHeadItemBase::~NormalGeneralsHeadItemBase()
{

}

NormalGeneralsHeadItemBase * NormalGeneralsHeadItemBase::create(CGeneralBaseMsg * generalBaseMsg)
{
	NormalGeneralsHeadItemBase * normalGeneralsHeadItemBase = new NormalGeneralsHeadItemBase();
	if (normalGeneralsHeadItemBase && normalGeneralsHeadItemBase->initWithGeneralBaseMsg(generalBaseMsg))
	{
		normalGeneralsHeadItemBase->autorelease();
		return normalGeneralsHeadItemBase;
	}
	CC_SAFE_DELETE(normalGeneralsHeadItemBase);
	return NULL;
}

NormalGeneralsHeadItemBase * NormalGeneralsHeadItemBase::create( int generalId )
{
	NormalGeneralsHeadItemBase * normalGeneralsHeadItemBase = new NormalGeneralsHeadItemBase();
	if (normalGeneralsHeadItemBase && normalGeneralsHeadItemBase->initWithGeneralId(generalId))
	{
		normalGeneralsHeadItemBase->autorelease();
		return normalGeneralsHeadItemBase;
	}
	CC_SAFE_DELETE(normalGeneralsHeadItemBase);
	return NULL;
}

NormalGeneralsHeadItemBase * NormalGeneralsHeadItemBase::create( CGeneralDetail * generalDetail )
{
	NormalGeneralsHeadItemBase * normalGeneralsHeadItemBase = new NormalGeneralsHeadItemBase();
	if (normalGeneralsHeadItemBase && normalGeneralsHeadItemBase->initWithGeneralDetail(generalDetail))
	{
		normalGeneralsHeadItemBase->autorelease();
		return normalGeneralsHeadItemBase;
	}
	CC_SAFE_DELETE(normalGeneralsHeadItemBase);
	return NULL;
}

bool NormalGeneralsHeadItemBase::initWithGeneralBaseMsg(CGeneralBaseMsg * generalBaseMsg)
{
	if (GeneralsHeadItemBase::init(generalBaseMsg))
	{
		u_layer = UILayer::create();
		addChild(u_layer);
// 
// 		//等级底图
// 		UIImageView * imageView_lvFrame = UIImageView::create();
// 		imageView_lvFrame->setTexture("res_ui/zhezhao80.png");
// 		imageView_lvFrame->setScale9Enable(true);
// 		imageView_lvFrame->setScale9Size(CCSizeMake(43,15));
// 		imageView_lvFrame->setAnchorPoint(ccp(1.0f,0));
// 		imageView_lvFrame->setPosition(ccp(102,25));
// 		u_layer->addWidget(imageView_lvFrame);
// 		//等级
// 		std::string _lv = "LV";
// 		char s_level[5];
// 		sprintf(s_level,"%d",generalBaseMsg->level());
// 		_lv.append(s_level);
// 		UILabel * label_level = UILabel::create();
// 		label_level->setText(_lv.c_str());
// 		label_level->setAnchorPoint(ccp(0.5f,0.5f));
// 		label_level->setPosition(ccp(-20,8));
// 		imageView_lvFrame->addChild(label_level);
		//军衔
		if (generalBaseMsg->evolution() > 0)
		{
			std::string rankPathBase = "res_ui/rank/rank";
			char s_evolution[5];
			sprintf(s_evolution,"%d",generalBaseMsg->evolution());
			rankPathBase.append(s_evolution);
			rankPathBase.append(".png");
			UIImageView * imageView_rank = UIImageView::create();
			imageView_rank->setTexture(rankPathBase.c_str());
			imageView_rank->setAnchorPoint(ccp(.5f,.5f));
			imageView_rank->setPosition(ccp(101-14,137-14));
			imageView_rank->setName("imageView_rank");
			u_layer->addWidget(imageView_rank);
		}

		this->setContentSize(CCSizeMake(108,147));

		return true;
	}
	return false;
}

bool NormalGeneralsHeadItemBase::initWithGeneralId( int generalModleId )
{
	if (GeneralsHeadItemBase::init(generalModleId))
	{
		u_layer = UILayer::create();
		addChild(u_layer);

// 		//等级底图
// 		UIImageView * imageView_lvFrame = UIImageView::create();
// 		imageView_lvFrame->setTexture("res_ui/zhezhao80.png");
// 		imageView_lvFrame->setScale9Enable(true);
// 		imageView_lvFrame->setScale9Size(CCSizeMake(43,15));
// 		imageView_lvFrame->setAnchorPoint(ccp(1.0f,0));
// 		imageView_lvFrame->setPosition(ccp(102,25));
// 		u_layer->addWidget(imageView_lvFrame);
// 		//等级
// 		std::string _lv = "LV";
// 		char s_lv[5];
// 		sprintf(s_lv,"%d",1);
// 		_lv.append(s_lv);
// 		UILabel * label_level = UILabel::create();
// 		label_level->setText(_lv.c_str());
// 		label_level->setAnchorPoint(ccp(0.5f,0.5f));
// 		label_level->setPosition(ccp(-20,8));
// 		imageView_lvFrame->addChild(label_level);
		//军衔
// 		std::string rankPathBase = "res_ui/rank/rank";
// 		char s_rank[5];
// 		sprintf(s_rank,"%d",1);
// 		rankPathBase.append(s_rank);
// 		rankPathBase.append(".png");
// 		UIImageView * imageView_rank = UIImageView::create();
// 		imageView_rank->setTexture(rankPathBase.c_str());
// 		imageView_rank->setAnchorPoint(ccp(.5f,.5f));
// 		imageView_rank->setPosition(ccp(101-14,137-14));
// 		imageView_rank->setName("imageView_rank");
// 		u_layer->addWidget(imageView_rank);

		this->setContentSize(CCSizeMake(108,147));

		return true;
	}
	return false;
}

bool NormalGeneralsHeadItemBase::initWithGeneralDetail( CGeneralDetail * generalDetail )
{
	if (GeneralsHeadItemBase::init(generalDetail->modelid(),generalDetail->currentquality()))
	{
		u_layer = UILayer::create();
		addChild(u_layer);

		//等级
// 		std::string _lv = "LV";
// 		char * s = new char [5];
// 		sprintf(s,"%d",level);
// 		_lv.append(s);
// 		UILabel * label_level = UILabel::create();
// 		label_level->setText(_lv.c_str());
// 		delete [] s;
// 		label_level->setAnchorPoint(ccp(1.0f,0));
// 		label_level->setPosition(ccp(imageView_head->getContentSize().width,46));
// 		m_pUiLayer->addWidget(label_level);
		//军衔
		if (generalDetail->evolution() > 0)
		{
			std::string rankPathBase = "res_ui/rank/rank";
			char s_evolution[5];
			sprintf(s_evolution,"%d",generalDetail->evolution());
			rankPathBase.append(s_evolution);
			rankPathBase.append(".png");
			UIImageView * imageView_rank = UIImageView::create();
			imageView_rank->setTexture(rankPathBase.c_str());
			imageView_rank->setAnchorPoint(ccp(.5f,.5f));
			imageView_rank->setPosition(ccp(101-14,137-14));
			imageView_rank->setName("imageView_rank");
			u_layer->addWidget(imageView_rank);
		}

		this->setContentSize(CCSizeMake(108,147));

		return true;
	}
	return false;
}

void NormalGeneralsHeadItemBase::playAnmOfEvolution()
{
	UIImageView* imageView_rank = (UIImageView*)u_layer->getWidgetByName("imageView_rank");
	if (imageView_rank)
	{
		imageView_rank->setScale(3.0f);
		CCAction* action = CCSequence::create(
			CCDelayTime::create(0.8f),
			CCScaleTo::create(0.2f,1.0f),
			NULL);
		imageView_rank->runAction(action);
	}
}
