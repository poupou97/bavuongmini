
#include "GeneralsHeadItemBase.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../utils/StaticDataManager.h"
#include "GeneralsUI.h"
#include "GameView.h"
#include "RecuriteActionItem.h"
#include "../../legend_engine/CCLegendAnimation.h"
#include "ShowBigGeneralsHead.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/extensions/RecruitGeneralCard.h"

#define KTag_FiveStar_Anm 345
#define kTagLegendAnm_Head 10  

GeneralsHeadItemBase::GeneralsHeadItemBase():
m_nCreateType(-1)
{
	m_pGeneralBaseMsg = new CGeneralBaseMsg();
}

GeneralsHeadItemBase::~GeneralsHeadItemBase()
{
	delete m_pGeneralBaseMsg;
}


void GeneralsHeadItemBase::onEnter()
{
	UIScene::onEnter();
}

void GeneralsHeadItemBase::onExit()
{
	UIScene::onExit();
}


GeneralsHeadItemBase * GeneralsHeadItemBase::create(CGeneralBaseMsg * generalBaseMsg)
{
	GeneralsHeadItemBase * generalsHeadItemBase = new GeneralsHeadItemBase();
	if (generalsHeadItemBase && generalsHeadItemBase->init(generalBaseMsg))
	{
		generalsHeadItemBase->autorelease();
		return generalsHeadItemBase;
	}
	CC_SAFE_DELETE(generalsHeadItemBase);
	return NULL;
}

GeneralsHeadItemBase * GeneralsHeadItemBase::create( int generalModleId)
{
	GeneralsHeadItemBase * generalsHeadItemBase = new GeneralsHeadItemBase();
	if (generalsHeadItemBase && generalsHeadItemBase->init(generalModleId))
	{
		generalsHeadItemBase->autorelease();
		return generalsHeadItemBase;
	}
	CC_SAFE_DELETE(generalsHeadItemBase);
	return NULL;
}

GeneralsHeadItemBase * GeneralsHeadItemBase::create( int generalModleId,int currentquality )
{
	GeneralsHeadItemBase * generalsHeadItemBase = new GeneralsHeadItemBase();
	if (generalsHeadItemBase && generalsHeadItemBase->init(generalModleId,currentquality))
	{
		generalsHeadItemBase->autorelease();
		return generalsHeadItemBase;
	}
	CC_SAFE_DELETE(generalsHeadItemBase);
	return NULL;
}

bool GeneralsHeadItemBase::init(CGeneralBaseMsg * generalBaseMsg)
{
	if (UIScene::init())
	{
		m_nCreateType = CT_withGeneralBaseMsg;
		m_pGeneralBaseMsg->CopyFrom(*generalBaseMsg);

		layer_up = UILayer::create();
		addChild(layer_up);

		CGeneralBaseMsg * generalMsgFromDb = GeneralsConfigData::s_generalsBaseMsgData[generalBaseMsg->modelid()];

		//边框
		frame = UIButton::create();
		frame->setTextures(GeneralsUI::getBigHeadFramePath(generalBaseMsg->currentquality()).c_str(),GeneralsUI::getBigHeadFramePath(generalBaseMsg->currentquality()).c_str(),"");
		frame->setScale9Enable(true);
		frame->setScale9Size(CCSizeMake(108,147));
		frame->setAnchorPoint(CCPointZero);
		frame->setPosition(ccp(0,0));
		frame->setTouchEnable(false);
		frame->addReleaseEvent(this,coco_releaseselector(GeneralsHeadItemBase::FrameEvent));
		m_pUiLayer->addWidget(frame);

		//头像
// 		std::string icon_path = "res_ui/generals/";
// 		icon_path.append(generalMsgFromDb->get_half_photo());
// 		icon_path.append(".png");
// 		imageView_head = UIImageView::create();
// 		imageView_head->setTexture(icon_path.c_str());
// 		imageView_head->setAnchorPoint(ccp(0.5f,0));
// 		imageView_head->setPosition(ccp(frame->getSize().width/2,27));
// 		m_pUiLayer->addWidget(imageView_head);

		std::string animFileName = "res_ui/generals/";
		animFileName.append(generalMsgFromDb->get_half_photo());
		animFileName.append(".anm");
		//std::string animFileName = "res_ui/generals/caiwenji.anm";
		la_head = CCLegendAnimation::create(animFileName);
		if (la_head)
		{
			la_head->setPlayLoop(true);
			la_head->setReleaseWhenStop(false);
			la_head->setScale(.7f);	
			la_head->setPosition(ccp(5,27));
			la_head->setTag(kTagLegendAnm_Head);
			m_pUiLayer->addChild(la_head);
		}

		//名字黑底
//  		black_namebg = UIImageView::create(); 
//  		black_namebg->setTexture("res_ui/zhezhao80.png");
//  		black_namebg->setScale9Enable(true);
//  		black_namebg->setScale9Size(CCSizeMake(98,18));
//  		black_namebg->setAnchorPoint(CCPointZero);
//  		black_namebg->setPosition(ccp(0,0));
//  		m_pUiLayer->addWidget(black_namebg);
		//名字
		label_name = UILabel::create();
		label_name->setText(generalMsgFromDb->name().c_str());
		label_name->setAnchorPoint(ccp(0.5f,0.5f));
		label_name->setPosition(ccp(frame->getSize().width/2,18));
		label_name->setColor(GameView::getInstance()->getGeneralsColorByQuality(generalBaseMsg->currentquality()));
		layer_up->addWidget(label_name);

		//稀有度
		if (generalBaseMsg->rare()>0)
		{
			imageView_star = UIImageView::create();
			imageView_star->setTexture(RecuriteActionItem::getStarPathByNum(generalBaseMsg->rare()).c_str());
			imageView_star->setAnchorPoint(CCPointZero);
			imageView_star->setScale(0.8f);
			imageView_star->setPosition(ccp(6,27));
			imageView_star->setName("ImageView_Star");
			layer_up->addWidget(imageView_star);

			if (generalBaseMsg->rare() >= 5)
			{
				// load animation
				std::string animFileName = "animation/ui/wj/wj2.anm";
				CCLegendAnimation *m_pAnim = CCLegendAnimation::create(animFileName);
				m_pAnim->setPlayLoop(true);
				m_pAnim->setReleaseWhenStop(false);
 				m_pAnim->setScale(.8f);
 				m_pAnim->setPosition(ccp(6,27));
 				m_pAnim->setPlaySpeed(.35f);
				m_pAnim->setTag(KTag_FiveStar_Anm);
				layer_up->addChild(m_pAnim);
			}
		}
// 		for( int  i = 0; i<generalBaseMsg->rare();++i)
// 		{
// 			UIImageView * star = UIImageView::create();
// 			star->setTexture("res_ui/star_on.png");
// 			star->setAnchorPoint(CCPointZero);
// 			star->setScale(0.8f);
// 			star->setPosition(ccp(6+19*i,28));
// 			m_pUiLayer->addWidget(star);
// 		}

		//profession
		std::string generalProfess_ = RecruitGeneralCard::getGeneralProfessionIconPath(generalMsgFromDb->get_profession());
		imageView_prefession = UIImageView::create();
		imageView_prefession->setTexture(generalProfess_.c_str());
		imageView_prefession->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_prefession->setPosition(ccp(frame->getSize().width - imageView_prefession->getContentSize().width/2-2 ,
			42));
		imageView_prefession->setScale(0.8f);
		layer_up->addWidget(imageView_prefession);
		imageView_prefession->setName("imageView_prefession");

		this->setContentSize(CCSizeMake(108,147));
		return true;
	}
	return false;
}

bool GeneralsHeadItemBase::init( int generalModleId )
{
	if (UIScene::init())
	{
		m_nCreateType = CT_withModleId;
		m_nGeneralModleId = generalModleId;

		layer_up = UILayer::create();
		addChild(layer_up);

		CGeneralBaseMsg* generalBaseMsg = GeneralsConfigData::s_generalsBaseMsgData[generalModleId];
		//边框
		frame = UIButton::create();
		frame->setTextures(GeneralsUI::getBigHeadFramePath(1).c_str(),GeneralsUI::getBigHeadFramePath(1).c_str(),"");    //默认是1级（1-10为白色）
		frame->setScale9Enable(true);
		frame->setScale9Size(CCSizeMake(108,147));
		frame->setAnchorPoint(CCPointZero);
		frame->setPosition(ccp(0,0));
		frame->setTouchEnable(false);
		frame->addReleaseEvent(this,coco_releaseselector(GeneralsHeadItemBase::FrameEvent));
		m_pUiLayer->addWidget(frame);
		//名字黑底
// 		black_namebg = UIImageView::create(); 
// 		black_namebg->setTexture("res_ui/zidi.png");
// 		black_namebg->setScale9Enable(true);
// 		black_namebg->setScale9Size(CCSizeMake(98,18));
// 		black_namebg->setAnchorPoint(CCPointZero);
// 		black_namebg->setPosition(ccp(0,0));
// 		m_pUiLayer->addWidget(black_namebg);
		//名字
		label_name = UILabel::create();
		label_name->setText(generalBaseMsg->name().c_str());
		//label_name->setText("asd");
		label_name->setAnchorPoint(ccp(0.5f,0.5f));
		label_name->setPosition(ccp(frame->getSize().width/2,18));
		label_name->setColor(GameView::getInstance()->getGeneralsColorByQuality(1));
		layer_up->addWidget(label_name);
		//头像
// 		std::string icon_path = "res_ui/generals/";
// 		icon_path.append(generalBaseMsg->get_half_photo());
// 		//icon_path.append("guanyu");
// 		icon_path.append(".png");
// 		imageView_head = UIImageView::create();
// 		imageView_head->setTexture(icon_path.c_str());
// 		imageView_head->setAnchorPoint(ccp(0.5f,0));
// 		imageView_head->setPosition(ccp(frame->getSize().width/2,27));
// 		m_pUiLayer->addWidget(imageView_head);

		std::string animFileName = "res_ui/generals/";
		animFileName.append(generalBaseMsg->get_half_photo());
		animFileName.append(".anm");
		//std::string animFileName = "res_ui/generals/caiwenji.anm";
		la_head = CCLegendAnimation::create(animFileName);
		if (la_head)
		{
			la_head->setPlayLoop(true);
			la_head->setReleaseWhenStop(false);
			la_head->setScale(.7f);	
			la_head->setPosition(ccp(5,27));
			la_head->setTag(kTagLegendAnm_Head);
			m_pUiLayer->addChild(la_head);
		}
		//稀有度
		if (generalBaseMsg->rare()>0)
		{
			imageView_star = UIImageView::create();
			imageView_star->setTexture(RecuriteActionItem::getStarPathByNum(generalBaseMsg->rare()).c_str());
			imageView_star->setAnchorPoint(CCPointZero);
			imageView_star->setScale(0.8f);
			imageView_star->setPosition(ccp(6,27));
			imageView_star->setName("ImageView_Star");
			layer_up->addWidget(imageView_star);

			if (generalBaseMsg->rare() >= 5)
			{
				// load animation
				std::string animFileName = "animation/ui/wj/wj2.anm";
				CCLegendAnimation *m_pAnim = CCLegendAnimation::create(animFileName);
				m_pAnim->setPlayLoop(true);
				m_pAnim->setReleaseWhenStop(true);
				m_pAnim->setScale(.8f);
				m_pAnim->setPosition(ccp(6,27));
				m_pAnim->setPlaySpeed(.35f);
				m_pAnim->setTag(KTag_FiveStar_Anm);
				layer_up->addChild(m_pAnim);
			}
		}
		//for( int  i = 0; i<5;++i)
// 		for( int  i = 0; i<generalBaseMsg->rare();++i)
// 		{
// 			UIImageView * star = UIImageView::create();
// 			star->setTexture("res_ui/star_on.png");
// 			star->setAnchorPoint(CCPointZero);
// 			star->setScale(0.8f);
// 			star->setPosition(ccp(6+19*i,28));
// 			m_pUiLayer->addWidget(star);
// 		}
		//profession
		std::string generalProfess_ = RecruitGeneralCard::getGeneralProfessionIconPath(generalBaseMsg->get_profession());
		imageView_prefession = UIImageView::create();
		imageView_prefession->setTexture(generalProfess_.c_str());
		imageView_prefession->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_prefession->setPosition(ccp(frame->getSize().width - imageView_prefession->getContentSize().width/2-2 ,
			42));
		imageView_prefession->setScale(0.8f);
		layer_up->addWidget(imageView_prefession);
		imageView_prefession->setName("imageView_prefession");

		this->setContentSize(CCSizeMake(108,147));
		return true;
	}
	return false;
}

bool GeneralsHeadItemBase::init( int generalModleId,int currentquality )
{
	if (UIScene::init())
	{
		m_nCreateType = CT_withModleIdAndQuality;
		m_nGeneralModleId = generalModleId;
		m_nCurrentquality = currentquality;

		layer_up = UILayer::create();
		addChild(layer_up);

		CGeneralBaseMsg* generalBaseMsg = GeneralsConfigData::s_generalsBaseMsgData[generalModleId];
		//边框
		frame = UIButton::create();
		frame->setTextures(GeneralsUI::getBigHeadFramePath(currentquality).c_str(),GeneralsUI::getBigHeadFramePath(currentquality).c_str(),"");    //（1-10为白色）
		frame->setScale9Enable(true);
		frame->setScale9Size(CCSizeMake(108,147));
		frame->setAnchorPoint(CCPointZero);
		frame->setPosition(ccp(0,0));
		frame->setTouchEnable(false);
		frame->addReleaseEvent(this,coco_releaseselector(GeneralsHeadItemBase::FrameEvent));
		m_pUiLayer->addWidget(frame);

		//名字
		label_name = UILabel::create();
		label_name->setText(generalBaseMsg->name().c_str());
		//label_name->setText("asd");
		label_name->setAnchorPoint(ccp(0.5f,0.5f));
		label_name->setPosition(ccp(frame->getSize().width/2,18));
		label_name->setColor(GameView::getInstance()->getGeneralsColorByQuality(1));
		layer_up->addWidget(label_name);
		//头像
// 		std::string icon_path = "res_ui/generals/";
// 		icon_path.append(generalBaseMsg->get_half_photo());
// 		//icon_path.append("guanyu");
// 		icon_path.append(".png");
// 		imageView_head = UIImageView::create();
// 		imageView_head->setTexture(icon_path.c_str());
// 		imageView_head->setAnchorPoint(ccp(0.5f,0));
// 		imageView_head->setPosition(ccp(frame->getSize().width/2,27));
// 		m_pUiLayer->addWidget(imageView_head);

		std::string animFileName = "res_ui/generals/";
		animFileName.append(generalBaseMsg->get_half_photo());
		animFileName.append(".anm");
		//std::string animFileName = "res_ui/generals/caiwenji.anm";
		la_head = CCLegendAnimation::create(animFileName);
		if (la_head)
		{
			la_head->setPlayLoop(true);
			la_head->setReleaseWhenStop(false);
			la_head->setScale(.7f);	
			la_head->setPosition(ccp(5,27));
			la_head->setTag(kTagLegendAnm_Head);
			m_pUiLayer->addChild(la_head);
		}
		//稀有度
		if (generalBaseMsg->rare()>0)
		{
			imageView_star = UIImageView::create();
			imageView_star->setTexture(RecuriteActionItem::getStarPathByNum(generalBaseMsg->rare()).c_str());
			imageView_star->setAnchorPoint(CCPointZero);
			imageView_star->setScale(0.8f);
			imageView_star->setPosition(ccp(6,27));
			imageView_star->setName("ImageView_Star");
			layer_up->addWidget(imageView_star);

			if (generalBaseMsg->rare() >= 5)
			{
				// load animation
				std::string animFileName = "animation/ui/wj/wj2.anm";
				CCLegendAnimation *m_pAnim = CCLegendAnimation::create(animFileName);
				m_pAnim->setPlayLoop(true);
				m_pAnim->setReleaseWhenStop(true);
				m_pAnim->setScale(.8f);
				m_pAnim->setPosition(ccp(6,27));
				m_pAnim->setPlaySpeed(.35f);
				m_pAnim->setTag(KTag_FiveStar_Anm);
				layer_up->addChild(m_pAnim);
			}
		}
		//for( int  i = 0; i<5;++i)
// 		for( int  i = 0; i<generalBaseMsg->rare();++i)
// 		{
// 			UIImageView * star = UIImageView::create();
// 			star->setTexture("res_ui/star_on.png");
// 			star->setAnchorPoint(CCPointZero);
// 			star->setScale(0.8f);
// 			star->setPosition(ccp(6+19*i,28));
// 			m_pUiLayer->addWidget(star);
// 		}
		//profession
		std::string generalProfess_ = RecruitGeneralCard::getGeneralProfessionIconPath(generalBaseMsg->get_profession());
		imageView_prefession = UIImageView::create();
		imageView_prefession->setTexture(generalProfess_.c_str());
		imageView_prefession->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_prefession->setPosition(ccp(frame->getSize().width - imageView_prefession->getContentSize().width/2-2 ,
			42));
		imageView_prefession->setScale(0.8f);
		layer_up->addWidget(imageView_prefession);
		imageView_prefession->setName("imageView_prefession");

		this->setContentSize(CCSizeMake(108,147));
		return true;
	}
	return false;
}

void GeneralsHeadItemBase::showSilhouetteEffect()
{
	//imageView_head->setColor(ccc3(0,0,0));
	if (m_pUiLayer->getChildByTag(kTagLegendAnm_Head))
	{
		CCLegendAnimation * anm = (CCLegendAnimation*)m_pUiLayer->getChildByTag(kTagLegendAnm_Head);
		anm->setColor(ccc3(0,0,0));
	}

	//label_name->setVisible(false);
	label_name->setColor(ccc3(0,0,0));
	if (layer_up->getWidgetByName("ImageView_Star"))
	{
		layer_up->getWidgetByName("ImageView_Star")->setColor(ccc3(0,0,0));
	}

	if (layer_up->getChildByTag(KTag_FiveStar_Anm))
	{
		CCLegendAnimation * anm = (CCLegendAnimation*)layer_up->getChildByTag(KTag_FiveStar_Anm);
		anm->setColor(ccc3(0,0,0));
	}
}

void GeneralsHeadItemBase::removeSilhouetteEffect()
{
	//imageView_head->setColor(ccc3(255,255,255));
	if (m_pUiLayer->getChildByTag(kTagLegendAnm_Head))
	{
		CCLegendAnimation * anm = (CCLegendAnimation*)m_pUiLayer->getChildByTag(kTagLegendAnm_Head);
		anm->setColor(ccc3(0,0,0));
	}

	//label_name->setVisible(true);
	label_name->setColor(ccc3(255,255,255));
	if (layer_up->getWidgetByName("ImageView_Star"))
	{
		layer_up->getWidgetByName("ImageView_Star")->setColor(ccc3(255,255,255));
	}

	if (layer_up->getChildByTag(KTag_FiveStar_Anm))
	{
		CCLegendAnimation * anm = (CCLegendAnimation*)layer_up->getChildByTag(KTag_FiveStar_Anm);
		anm->setColor(ccc3(255,255,255));
	}
}

void GeneralsHeadItemBase::FrameEvent( CCObject *pSender )
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	switch(m_nCreateType)
	{
	case CT_withGeneralBaseMsg:
		{
			ShowBigGeneralsHead * bigHead = (ShowBigGeneralsHead*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShowBigGeneralsHead);
			if (bigHead == NULL)
			{
				bigHead = ShowBigGeneralsHead::create(m_pGeneralBaseMsg);
				GameView::getInstance()->getMainUIScene()->addChild(bigHead,0,kTagShowBigGeneralsHead);
				bigHead->ignoreAnchorPointForPosition(false);
				bigHead->setAnchorPoint(ccp(0.5f, 0.5f));
				bigHead->setPosition(ccp(winSize.width/2, winSize.height/2));
				bigHead->showBeginAction();
			}
		}
		break;
	case CT_withModleId:
		{
			ShowBigGeneralsHead * bigHead = (ShowBigGeneralsHead*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShowBigGeneralsHead);
			if (bigHead == NULL)
			{
				bigHead = ShowBigGeneralsHead::create(m_nGeneralModleId);
				GameView::getInstance()->getMainUIScene()->addChild(bigHead,0,kTagShowBigGeneralsHead);
				bigHead->ignoreAnchorPointForPosition(false);
				bigHead->setAnchorPoint(ccp(0.5f, 0.5f));
				bigHead->setPosition(ccp(winSize.width/2, winSize.height/2));
				bigHead->showBeginAction();
			}
		}
		break;
	case CT_withModleIdAndQuality:
		{
			ShowBigGeneralsHead * bigHead = (ShowBigGeneralsHead*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShowBigGeneralsHead);
			if (bigHead == NULL)
			{
				bigHead = ShowBigGeneralsHead::create(m_nGeneralModleId,m_nCurrentquality);
				GameView::getInstance()->getMainUIScene()->addChild(bigHead,0,kTagShowBigGeneralsHead);
				bigHead->ignoreAnchorPointForPosition(false);
				bigHead->setAnchorPoint(ccp(0.5f, 0.5f));
				bigHead->setPosition(ccp(winSize.width/2, winSize.height/2));
				bigHead->showBeginAction();
			}
		}
		break;
	}
}

void GeneralsHeadItemBase::setCanShowBigCard( bool isCan )
{
	frame->setTouchEnable(isCan);
}

CCLegendAnimation * GeneralsHeadItemBase::getLegendHead()
{
	if (m_pUiLayer->getChildByTag(kTagLegendAnm_Head))
	{
		CCLegendAnimation * anm = (CCLegendAnimation*)m_pUiLayer->getChildByTag(kTagLegendAnm_Head);
		return anm;
	}
	else
	{
		return NULL;
	}
}

CCLegendAnimation * GeneralsHeadItemBase::getFiveStarAnm()
{
	if (layer_up->getChildByTag(KTag_FiveStar_Anm))
	{
		CCLegendAnimation * anm = (CCLegendAnimation*)layer_up->getChildByTag(KTag_FiveStar_Anm);
		return anm;
	}
	else
	{
		return NULL;
	}
}
