

#ifndef _GENERALSUI_GENERALSSHORTCUTSLOT_H_
#define _GENERALSUI_GENERALSSHORTCUTSLOT_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "GeneralsSkillsUI.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CShortCut;
class GameFightSkill;

#define GENERALSKILLEMPTYPATH "res_ui/round_none.png"

#define GENERALSKILLVoidFramePath "res_ui/round_none.png"
#define GENERALSKILLWhiteFramePath "res_ui/round_white.png"
#define GENERALSKILLGreenFramePath "res_ui/round_green.png"
#define GENERALSKILLBlueFramePath "res_ui/round_blue.png"
#define GENERALSKILLPurpleFramePath "res_ui/round_purple.png"
#define GENERALSKILLOrangeFramePath "res_ui/round_orange.png"

class GeneralsShortcutSlot : public UIScene 
{
public:
	GeneralsShortcutSlot();
	~GeneralsShortcutSlot();
	//类型：主动技能，被动技能
	enum SkillType{
		Actived,
		Passived
	};
	//状态：未开启，已开启，已装配
	enum SkillState
	{
		NotOpen,
		Opened,
		Used
	};

	static GeneralsShortcutSlot* create(int quality,CShortCut * shortcut);
	bool init(int quality,CShortCut * shortcut);

	static GeneralsShortcutSlot* create(int quality,int slotIndex);
	bool init(int quality,int slotIndex);

	static GeneralsShortcutSlot* create(GameFightSkill * gameFightSkill);
	bool init(GameFightSkill * gameFightSkill);

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

	void UpdateSkillType(int slotIndex);
	void UpdateSkillState(int slotIndex,int quality);
	void UpdateSkillState(CShortCut * shortcut,int quality);

	void DoThing(CCObject * pSender);

	void setSkillProfession(GeneralsSkillsUI::SkillTpye st);
	GeneralsSkillsUI::SkillTpye getSkillProfession();

	int getIndex();
	virtual void setTouchEnabled(bool value);

private:
	SkillType skillType;
	SkillState skillState;

	UIButton * btn_bgFrame;

	GeneralsSkillsUI::SkillTpye skill_profession;

	std::string skillId;
	int index;
};

#endif

