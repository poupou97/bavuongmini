#include "TeachAndEvolutionCell.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../utils/StaticDataManager.h"
#include "GeneralsUI.h"
#include "GameView.h"
#include "AppMacros.h"
#include "RecuriteActionItem.h"
#include "../../gamescene_state/role/ActorUtils.h"

#define kTag_Icon_Evolution 44
#define kTag_Icon_Rare 45

TeachAndEvolutionCell::TeachAndEvolutionCell()
{
}


TeachAndEvolutionCell::~TeachAndEvolutionCell()
{
}

TeachAndEvolutionCell* TeachAndEvolutionCell::create(CGeneralBaseMsg * generalBaseMsg)
{
	TeachAndEvolutionCell * teachAndEvolutionCell = new TeachAndEvolutionCell();
	if (teachAndEvolutionCell && teachAndEvolutionCell->init(generalBaseMsg))
	{
		teachAndEvolutionCell->autorelease();
		return teachAndEvolutionCell;
	}
	CC_SAFE_DELETE(teachAndEvolutionCell);
	return NULL;
}

bool TeachAndEvolutionCell::init(CGeneralBaseMsg * generalBaseMsg)
{
	if (CCTableViewCell::init())
	{
		CGeneralBaseMsg * generalMsgFromDb = GeneralsConfigData::s_generalsBaseMsgData[generalBaseMsg->modelid()];
		curGeneralBaseMsg = generalBaseMsg;
		//大边框
		sprite_bigFrame = CCScale9Sprite::create("res_ui/kuang01_new.png");
		sprite_bigFrame->setAnchorPoint(ccp(0, 0));
		sprite_bigFrame->setPosition(ccp(0, 0));
		sprite_bigFrame->setCapInsets(CCRectMake(15,31,1,1));
		sprite_bigFrame->setContentSize(CCSizeMake(237,69));
		addChild(sprite_bigFrame);
		//小边框
		CCScale9Sprite *smaillFrame = CCScale9Sprite::create("res_ui/LV4_allb.png");
		smaillFrame->setAnchorPoint(ccp(0, 0));
		smaillFrame->setPosition(ccp(12, 8));
		smaillFrame->setCapInsets(CCRectMake(15,15,1,1));
		smaillFrame->setContentSize(CCSizeMake(52,52));
		sprite_bigFrame->addChild(smaillFrame);

		CCScale9Sprite *sprite_nameFrame = CCScale9Sprite::create("res_ui/LV4_diaa.png");
		sprite_nameFrame->setAnchorPoint(ccp(0, 0));
		sprite_nameFrame->setPosition(ccp(83, 19));
		sprite_nameFrame->setCapInsets(CCRectMake(11,11,1,1));
		sprite_nameFrame->setContentSize(CCSizeMake(146,30));
		sprite_bigFrame->addChild(sprite_nameFrame);

		//带颜色的小边框
// 		CCRect cr = CCRect(9.5f,9.5f,6.0f,6.0f);
// 		CCScale9Sprite *sprite_smallFrame = CCScale9Sprite::create(GeneralsUI::getSmallHeadFramePath(generalBaseMsg->currentquality()).c_str());
// 		sprite_smallFrame->setAnchorPoint(ccp(0, 0));
// 		sprite_smallFrame->setPosition(ccp(12,12));
// 		sprite_smallFrame->setPreferredSize(CCSizeMake(51,61));
// 		sprite_smallFrame->setCapInsets(cr);
// 		sprite_bigFrame->addChild(sprite_smallFrame);

		//列表中的头像图标
		std::string icon_path = "res_ui/generals46X45/";
		icon_path.append(generalMsgFromDb->get_head_photo());
		icon_path.append(".png");
		sprite_icon = CCSprite::create(icon_path.c_str());
		sprite_icon->setAnchorPoint(ccp(0.5f, 0.5f));
		sprite_icon->setPosition(ccp(38, 33));
		sprite_bigFrame->addChild(sprite_icon);
// 		//等级
		CCSprite * sprite_lvFrame = CCSprite::create("res_ui/lv_kuang.png");
		sprite_lvFrame->setAnchorPoint(ccp(0, 0));
		sprite_lvFrame->setPosition(ccp(49, 4));
		sprite_bigFrame->addChild(sprite_lvFrame);
		//武将等级
		std::string _lv = "LV";
		char general_lv[5];
		sprintf(general_lv,"%d",generalBaseMsg->level());
		_lv.append(general_lv);
		label_lv = CCLabelTTF::create(general_lv,APP_FONT_NAME,12);
		ccColor3B shadowColor = ccc3(0,0,0);   // black
		label_lv->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		label_lv->setAnchorPoint(ccp(0.5f,0.5f));
		label_lv->setPosition(ccp(63,11));
		sprite_bigFrame->addChild(label_lv);

		//军衔
		if (generalBaseMsg->evolution() > 0)
		{
			CCSprite * imageView_rank = ActorUtils::createGeneralRankSprite(generalBaseMsg->evolution());
			imageView_rank->setAnchorPoint(ccp(1.0f,1.0f));
			imageView_rank->setPosition(ccp(74,66));
			imageView_rank->setScale(0.75f);
			imageView_rank->setTag(kTag_Icon_Evolution);
			sprite_bigFrame->addChild(imageView_rank);
		}
		//稀有度
		if (generalBaseMsg->rare()>0)
		{
			CCSprite * imageView_star = CCSprite::create(RecuriteActionItem::getStarPathByNum(generalBaseMsg->rare()).c_str());
			imageView_star->setAnchorPoint(ccp(0.5f,0.5f));
			imageView_star->setPosition(ccp(15,15));
			imageView_star->setScale(0.5f);
			imageView_star->setTag(kTag_Icon_Rare);
			sprite_bigFrame->addChild(imageView_star);
		}

		//武将名字
		label_name = CCLabelTTF::create(generalMsgFromDb->name().c_str(),APP_FONT_NAME,16);
		label_name->setAnchorPoint(ccp(0.0f, 0.5f));
		label_name->setPosition(ccp(93, 33));
		label_name->setColor(GameView::getInstance()->getGeneralsColorByQuality(generalBaseMsg->currentquality()));
		sprite_bigFrame->addChild(label_name);
		//武将品阶
		std::string general_rank = GeneralsUI::getQuality(generalBaseMsg->currentquality());
		label_rank = CCLabelTTF::create(general_rank.c_str(),APP_FONT_NAME,16);
		label_rank->setAnchorPoint(ccp(0, 0.5f));
		label_rank->setPosition(ccp(160, 33));
		label_rank->setColor(GameView::getInstance()->getGeneralsColorByQuality(generalBaseMsg->currentquality()));
		sprite_bigFrame->addChild(label_rank);

// 		if (generalBaseMsg->rare()>0)
// 		{
// 			CCSprite * temp = CCSprite::create(RecuriteActionItem::getStarPathByNum(generalBaseMsg->rare()).c_str());
// 			temp->setAnchorPoint(ccp(0.5f,0.5f));
// 			temp->setPosition(ccp(232,35));
// 			sprite_bigFrame->addChild(temp);
// 		}

		sprite_bigFrame_mengban = CCScale9Sprite::create("res_ui/mengban_black65.png");
		sprite_bigFrame_mengban->setAnchorPoint(ccp(0, 0));
		sprite_bigFrame_mengban->setPosition(ccp(0, 0));
		sprite_bigFrame_mengban->setPreferredSize(CCSizeMake(237,69));
		addChild(sprite_bigFrame_mengban);
		sprite_bigFrame_mengban->setVisible(false);
		
		return true;
	}
	return false;
}

CGeneralBaseMsg* TeachAndEvolutionCell::getCurGeneralBaseMsg()
{
	return this->curGeneralBaseMsg;
}

void TeachAndEvolutionCell::setGray( bool able )
{
	sprite_bigFrame_mengban->setVisible(able);
}

void TeachAndEvolutionCell::refreshCell( CGeneralBaseMsg * generalBaseMsg )
{
	CGeneralBaseMsg * generalMsgFromDb = GeneralsConfigData::s_generalsBaseMsgData[generalBaseMsg->modelid()];
	curGeneralBaseMsg = generalBaseMsg;

	//列表中的头像图标
	std::string icon_path = "res_ui/generals46X45/";
	icon_path.append(generalMsgFromDb->get_head_photo());
	icon_path.append(".png");
	CCTexture2D * texture = CCTextureCache::sharedTextureCache()->addImage(icon_path.c_str());
	sprite_icon->setTexture(texture);

	//武将等级
	std::string _lv = "LV";
	char general_lv[5];
	sprintf(general_lv,"%d",generalBaseMsg->level());
	_lv.append(general_lv);
	label_lv->setString(general_lv);
	//军衔
	if (sprite_bigFrame->getChildByTag(kTag_Icon_Evolution))
	{
		sprite_bigFrame->getChildByTag(kTag_Icon_Evolution)->removeFromParent();
	}
	if (generalBaseMsg->evolution() > 0)
	{
		CCSprite * imageView_rank = ActorUtils::createGeneralRankSprite(generalBaseMsg->evolution());
		imageView_rank->setAnchorPoint(ccp(1.0f,1.0f));
		imageView_rank->setPosition(ccp(74,67));
		imageView_rank->setScale(0.75f);
		imageView_rank->setTag(kTag_Icon_Evolution);
		sprite_bigFrame->addChild(imageView_rank);
	}
	//稀有度
	if (sprite_bigFrame->getChildByTag(kTag_Icon_Rare))
	{
		sprite_bigFrame->getChildByTag(kTag_Icon_Rare)->removeFromParent();
	}
	if (generalBaseMsg->rare()>0)
	{
		CCSprite * imageView_star = CCSprite::create(RecuriteActionItem::getStarPathByNum(generalBaseMsg->rare()).c_str());
		imageView_star->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_star->setPosition(ccp(15,15));
		imageView_star->setScale(0.5f);
		imageView_star->setTag(kTag_Icon_Rare);
		sprite_bigFrame->addChild(imageView_star);
	}

	//武将名字
	label_name->setString(generalMsgFromDb->name().c_str());
	label_name->setColor(GameView::getInstance()->getGeneralsColorByQuality(generalBaseMsg->currentquality()));
	//武将品阶
	std::string general_rank = GeneralsUI::getQuality(generalBaseMsg->currentquality());
	label_rank->setString(general_rank.c_str());
	label_rank->setColor(GameView::getInstance()->getGeneralsColorByQuality(generalBaseMsg->currentquality()));

	setGray(false);
}



