#ifndef  _GENERALSUI_GENERALSSKILLSUI_H_
#define _GENERALSUI_GENERALSSKILLSUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "../../messageclient/protobuf/RecruitMessage.pb.h"
#include "./GeneralsListBase.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CGeneralBaseMsg;
class CGeneralDetail;
class NormalGeneralsHeadItemBase;
class CFightWayBase;
class UITab;
class GameFightSkill;


///////////////////////////////////////////////////////////////////////
class GeneralSkillCellData
{
public:
	GeneralSkillCellData();
	~GeneralSkillCellData();

public:
	GameFightSkill * curGameFightSkill;
	int curRealLevel;
};


/////////////////////////////////
/**
 * 武将界面下的 武将技能类
 * @author yangjun
 * @version 0.1.0
 * @date 2013.12.9
 */

class GeneralsSkillsUI : public UIScene,public CCTableViewDelegate,public CCTableViewDataSource
{
public:
	GeneralsSkillsUI();
	~GeneralsSkillsUI();

	enum SkillTpye{
		Warrior = 1,
		Magic,
		Ranger,
		Warlock
	};

	static GeneralsSkillsUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);

	void LeveUpEvent(CCObject * pSender);
	void NullSkillUpEvent(CCObject * pSender);

private:
	int lastSelectCellId;
	int selectCellId;

	UIPanel * panel_skills; //技能面板
	UILayer * Layer_skills;
	UILayer * Layer_upper;

	cocos2d::extension::UIButton *btn_levelUp;
	cocos2d::extension::UILabel *l_levelUp;

	CCTableView * generalsSkillsList_tableView;	
	SkillTpye skillType ;

	UITab * m_mainTab;

	std::vector<GeneralSkillCellData *>DataSourceList;

	std::string curSkillId;

	cocos2d::extension::UIImageView* i_skillFrame;
	cocos2d::extension::UIImageView* i_skillImage;
	cocos2d::extension::UILabel* l_skillName;
	cocos2d::extension::UILabel* l_skillType;
	cocos2d::extension::UILabel* l_skillLvValue ;
	//UILabel* l_skillQualityValue;
	cocos2d::extension::UIImageView * imageView_skillQuality;
	cocos2d::extension::UILabel* l_skillMpValue;
	cocos2d::extension::UILabel* l_skillCDTimeValue ;
	cocos2d::extension::UILabel* l_skillDistanceValue;

private:
	void MainIndexChangedEvent(CCObject *pSender);
	void UpdateDataByType(SkillTpye s_t);
	//自动设置列表的偏移量
	void AutoSetOffSet();

	void refreshSkillDes(std::string curSkillDes,std::string nextSkillDes);

public:
	void setToDefault();
	void RefreshData();
	virtual void setVisible(bool visible);

	void RefreshSkillInfo(GameFightSkill * gameFightSkill);
	void RefreshBtnStatus(int curRealLevel);
	std::string getCurSkillId();

	static std::string getSkillQualityStr(int skillQuality);
};


/////////////////////////////////
/**
 * 武将界面下的“武将技能"中的武将列表项
 * @author yangjun
 * @version 0.1.0
 * @date 2014.11.28
 */

class GeneralSkillCell : public CCTableViewCell
{
public:
	GeneralSkillCell();
	~GeneralSkillCell();
	static GeneralSkillCell* create(GeneralSkillCellData * cellData);
	bool init(GeneralSkillCellData * cellData);

	void RefreshCell(GeneralSkillCellData * cellData);
	void setSelectSelf(bool isvisible);
private:
	CCSprite *  sprite_IconImage;
	CCLabelTTF * l_lv;
	CCLabelTTF * label_skillName;

	CCSprite* sprite_blackFrame;
};
#endif

