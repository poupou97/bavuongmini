#ifndef  _GENERALSUI_TEACHANDEVOLUTIONCELL_H_
#define _GENERALSUI_TEACHANDEVOLUTIONCELL_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "../../messageclient/protobuf/RecruitMessage.pb.h"
#include "./GeneralsListBase.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CGeneralBaseMsg;
class CGeneralDetail;

class TeachAndEvolutionCell : public CCTableViewCell
{
public:
	TeachAndEvolutionCell();
	~TeachAndEvolutionCell();
	static TeachAndEvolutionCell* create(CGeneralBaseMsg * generalBaseMsg);
	bool init(CGeneralBaseMsg * generalBaseMsg);

	void refreshCell(CGeneralBaseMsg * generalBaseMsg);

	CGeneralBaseMsg* getCurGeneralBaseMsg();

	//�Ƿ���
	void setGray(bool able);

private:

private:
	CGeneralBaseMsg * curGeneralBaseMsg; 

	CCScale9Sprite * sprite_bigFrame;
	CCScale9Sprite * sprite_bigFrame_mengban;

	CCSprite * sprite_icon;
	CCLabelTTF * label_lv;
	CCLabelTTF * label_name;
	CCLabelTTF * label_rank;
};

#endif

