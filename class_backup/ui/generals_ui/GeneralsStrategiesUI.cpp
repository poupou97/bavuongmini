#include "GeneralsStrategiesUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "GeneralsUI.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "GameView.h"
#include "NormalGeneralsHeadItem.h"
#include "../../messageclient/element/CFightWayBase.h"
#include "StrategiesPositionItem.h"
#include "generals_popup_ui/StrategiesUpgradeUI.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/StaticDataManager.h"
#include "../../utils/GameUtils.h"
#include "../../messageclient/element/CFightWayGrowingUp.h"
#include "GeneralsListBase.h"
#include "AppMacros.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/ScriptManager.h"
#include "../extensions/ShowSystemInfo.h"
#include "../../legend_script/CCTeachingGuide.h"

#define PositionItemBaseTag 55
#define M_SCROLLVIEWTAG 120
#define GENERALSSTRATRGIESUI_TABALEVIEW_HIGHLIGHT_TAG 456

//排序规则优先级为：（1）已启用阵法；（2）已学会阵法；（3）等级高的阵法；（4）按【排序编号】排；
bool SortFightWay(CFightWayBase * fightWay1 , CFightWayBase *fightWay2)
{
	if (fightWay1->activeflag() == 1)      //已启用阵法
	{
		return true;
	}
	else if (fightWay2->activeflag() == 1)      //已启用阵法
	{
		return false;
	}
	else
	{
		if (fightWay1->level() == fightWay2->level())
		{
			return (fightWay1->get_sortingOrder()> fightWay2->get_sortingOrder());
		}
		else
		{
			return (fightWay1->level()> fightWay2->level());
		}
	}
	return false;
}

GeneralsStrategiesUI::GeneralsStrategiesUI():
curSelectFightWayId(-1),
activeFightWayId(-1),
selectCellIndex(0),
curFightWayStatus(NotStuded),
lastSelectCellId(0)
{
	slotPos[0].x = 347;
	slotPos[0].y = 279;
	slotPos[1].x = 481;
	slotPos[1].y = 279;
	slotPos[2].x = 347;
	slotPos[2].y = 105;
	slotPos[3].x = 481;
	slotPos[3].y = 105;
	slotPos[4].x = 616;
	slotPos[4].y = 105;
}

GeneralsStrategiesUI::~GeneralsStrategiesUI()
{
	std::vector<CFightWayBase*>::iterator iter_generalStrategies;
	for (iter_generalStrategies = generalsStrategiesList.begin(); iter_generalStrategies != generalsStrategiesList.end(); ++iter_generalStrategies)
	{
		delete *iter_generalStrategies;
	}
	generalsStrategiesList.clear();

}
GeneralsStrategiesUI* GeneralsStrategiesUI::create()
{
	GeneralsStrategiesUI * generalsStrategiesUI = new GeneralsStrategiesUI();
	if (generalsStrategiesUI && generalsStrategiesUI->init())
	{
		generalsStrategiesUI->autorelease();
		return generalsStrategiesUI;
	}
	CC_SAFE_DELETE(generalsStrategiesUI);
	return NULL;
}

bool GeneralsStrategiesUI::init()
{
	if (UIScene::init())
	{
		setStrategiesListToDefault();

		sort(generalsStrategiesList.begin(),generalsStrategiesList.end(),SortFightWay);

		//武将阵法面板
		panel_strategies = (UIPanel*)UIHelper::seekWidgetByName(GeneralsUI::ppanel,"Panel_array");
		panel_strategies->setVisible(true);
		//武将阵法层
		Layer_strategies = UILayer::create();
		addChild(Layer_strategies);
		//一键休息
		UIButton *btn_reset = (UIButton*)UIHelper::seekWidgetByName(panel_strategies,"Button_reset");
		btn_reset->setTouchEnable(true);
		btn_reset->setPressedActionEnabled(true);
		btn_reset->addReleaseEvent(this,coco_releaseselector(GeneralsStrategiesUI::StrategiesResetEvent));
		btn_reset->setVisible(false);
		//升级阵法
		btn_levelUpStrategies = (UIButton*)UIHelper::seekWidgetByName(panel_strategies,"Button_upgrade");   
		btn_levelUpStrategies->setTouchEnable(true);
		btn_levelUpStrategies->setPressedActionEnabled(true);
		btn_levelUpStrategies->addReleaseEvent(this,coco_releaseselector(GeneralsStrategiesUI::StrategiesLeveUpEvent));

		l_upgrade = (UILabel *)UIHelper::seekWidgetByName(btn_levelUpStrategies,"Label_upgrade");   

		//启用阵法
		btn_openStrategies = (UIButton*)UIHelper::seekWidgetByName(panel_strategies,"Button_use");  
		btn_openStrategies->setTouchEnable(true);
		btn_openStrategies->setPressedActionEnabled(true);
		btn_openStrategies->addReleaseEvent(this,coco_releaseselector(GeneralsStrategiesUI::StrategiesUseEvent));
		//关闭阵法
		btn_closeStrategies = (UIButton*)UIHelper::seekWidgetByName(panel_strategies,"Button_notUse");  
		btn_closeStrategies->setTouchEnable(true);
		btn_closeStrategies->setPressedActionEnabled(true);
		btn_closeStrategies->addReleaseEvent(this,coco_releaseselector(GeneralsStrategiesUI::StrategiesNotUseEvent));

		UIButton * btn_detail = (UIButton*)UIHelper::seekWidgetByName(panel_strategies,"Button_detail");  
		btn_detail->setTouchEnable(true);
		btn_detail->setPressedActionEnabled(true);
		btn_detail->addReleaseEvent(this,coco_releaseselector(GeneralsStrategiesUI::StrategiesDetailEvent));

		l_strategieName = (UILabel *)UIHelper::seekWidgetByName(panel_strategies,"Label_name");  
		l_addPropDes = (UILabel *)UIHelper::seekWidgetByName(panel_strategies,"Label_buff");  

		//阵法列表
		strategiesList_tableView = CCTableView::create(this,CCSizeMake(261,401));
		strategiesList_tableView->setDirection(kCCScrollViewDirectionVertical);
		strategiesList_tableView->setAnchorPoint(ccp(0,0));
		strategiesList_tableView->setPosition(ccp(74,41));
		strategiesList_tableView->setDelegate(this);
		strategiesList_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		Layer_strategies->addChild(strategiesList_tableView);

		Layer_upper = UILayer::create();
		addChild(Layer_upper);

// 		UIImageView * tableView_kuang = UIImageView::create();
// 		tableView_kuang->setTexture("res_ui/LV5_dikuang_miaobian1.png");
// 		tableView_kuang->setScale9Enable(true);
// 		tableView_kuang->setScale9Size(CCSizeMake(265,417));
// 		tableView_kuang->setCapInsets(CCRectMake(30,30,1,1));
// 		tableView_kuang->setAnchorPoint(ccp(0,0));
// 		tableView_kuang->setPosition(ccp(63,33));
// 		Layer_upper->addWidget(tableView_kuang);

		//设置默认值
		RefreshListToDefault();

		return true;
	}
	return false;
}

void GeneralsStrategiesUI::StrategiesResetEvent( CCObject * pSender )
{
	for (int i =0;i<GameView::getInstance()->generalsInLineList.size();++i)
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5056,(void *)GameView::getInstance()->generalsInLineList.at(i)->id(),(void *)0);
	}
}

void GeneralsStrategiesUI::StrategiesLeveUpEvent( CCObject * pSender )
{
	if (curSelectFightWayId > 0)
	{
		//GameMessageProcessor::sharedMsgProcessor()->sendReq(5074,(void *)curSelectFightWayId);
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesUpGradeUI) == NULL)
		{
			CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
			StrategiesUpgradeUI * strategiesUpgradeUI = StrategiesUpgradeUI::create(curSelectFightWayId);
			strategiesUpgradeUI->ignoreAnchorPointForPosition(false);
			strategiesUpgradeUI->setAnchorPoint(ccp(0.5f,0.5f));
			strategiesUpgradeUI->setPosition(ccp(winSize.width/2,winSize.height/2));
			GameView::getInstance()->getMainUIScene()->addChild(strategiesUpgradeUI,0,kTagStrategiesUpGradeUI);

			Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
			if(sc != NULL)
				sc->endCommand(pSender);
		}
	}
	else
	{
		//CCDictionary *strings = CCDictionary::createWithContentsOfFile("res_ui/font/strings.xml");
		//const char *str1  = ((CCString*)strings->objectForKey("generals_strategies_pleaseSelectStrategies"))->m_sString.c_str();
		const char *str1 = StringDataManager::getString("generals_strategies_pleaseSelectStrategies");
		char* p1 =const_cast<char*>(str1);
		GameView::getInstance()->showAlertDialog(p1);
	}
}

void GeneralsStrategiesUI::StrategiesUseEvent( CCObject * pSender )
{
	if (curSelectFightWayId > 0)
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5072,(void *)curSelectFightWayId,(void *)1);

		Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if(sc != NULL)
			sc->endCommand(pSender);
	}
	else
	{
		//CCDictionary *strings = CCDictionary::createWithContentsOfFile("res_ui/font/strings.xml");
		//const char *str1  = ((CCString*)strings->objectForKey("generals_strategies_pleaseSelectStrategies"))->m_sString.c_str();
		const char *str1 = StringDataManager::getString("generals_strategies_pleaseSelectStrategies");
		char* p1 =const_cast<char*>(str1);
		GameView::getInstance()->showAlertDialog(p1);
	}
}

void GeneralsStrategiesUI::StrategiesNotUseEvent( CCObject * pSender )
{
	if (curSelectFightWayId > 0)
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5072,(void *)curSelectFightWayId,(void *)0);
	}
	else
	{
		//CCDictionary *strings = CCDictionary::createWithContentsOfFile("res_ui/font/strings.xml");
		//const char *str1  = ((CCString*)strings->objectForKey("generals_strategies_pleaseSelectStrategies"))->m_sString.c_str();
		const char *str1 = StringDataManager::getString("generals_strategies_pleaseSelectStrategies");
		char* p1 =const_cast<char*>(str1);
		GameView::getInstance()->showAlertDialog(p1);
	}
}

void GeneralsStrategiesUI::StrategiesDetailEvent(CCObject * pSender)
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesDetailInfo))
		return;

// 	StrategiesDetailInfo *strategiesDetailInfo = StrategiesDetailInfo::create(curSelectFightWayId);
// 	GameView::getInstance()->getMainUIScene()->addChild(strategiesDetailInfo,0,kTagStrategiesDetailInfo);
// 
// 	strategiesDetailInfo->ignoreAnchorPointForPosition(false);
// 	strategiesDetailInfo->setAnchorPoint(ccp(0.5f, 0.5f));
// 	strategiesDetailInfo->setPosition(ccp(winSize.width/2, winSize.height/2));

	//阵法信息描述
	std::map<int,std::string>::const_iterator cIter;
	cIter = SystemInfoConfigData::s_systemInfoList.find(1);
	if (cIter == SystemInfoConfigData::s_systemInfoList.end()) // 没找到就是指向END了  
	{
		return ;
	}
	else
	{
		if(GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesDetailInfo) == NULL)
		{
			ShowSystemInfo * showSystemInfo = ShowSystemInfo::create(SystemInfoConfigData::s_systemInfoList[1].c_str());
			GameView::getInstance()->getMainUIScene()->addChild(showSystemInfo,0,kTagStrategiesDetailInfo);
			showSystemInfo->ignoreAnchorPointForPosition(false);
			showSystemInfo->setAnchorPoint(ccp(0.5f, 0.5f));
			showSystemInfo->setPosition(ccp(winSize.width/2, winSize.height/2));
		}
	}
}

void GeneralsStrategiesUI::onEnter()
{
	UIScene::onEnter();
}

void GeneralsStrategiesUI::onExit()
{
	UIScene::onExit();
}

void GeneralsStrategiesUI::scrollViewDidScroll( CCScrollView* view )
{
}

void GeneralsStrategiesUI::scrollViewDidZoom( CCScrollView* view )
{
}

void GeneralsStrategiesUI::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{
	int i = cell->getIdx();
	curSelectFightWayId = generalsStrategiesList.at(i)->fightwayid();

	//选中状态坐标 
	selectCellIndex = cell->getIdx();
	CCSize _s = table->getContentOffset();
	table->reloadData();
	table->setContentOffset(_s); 

	//设置阵法当前状态
	if (generalsStrategiesList.at(i)->level()==0)
	{
		curFightWayStatus = NotStuded;
	}
	else 
	{
		if (generalsStrategiesList.at(i)->level()==10)
		{
			curFightWayStatus = Highest;
		}
		else
		{
			curFightWayStatus = Studed;
		}
	}

	//阵法名称
	l_strategieName->setText(generalsStrategiesList.at(i)->get_name().c_str());
	//设置描述以及属性加成
	RefreshStrategiesInfo(generalsStrategiesList.at(i));
	RefreshButtonState(generalsStrategiesList.at(i));

	if(table->cellAtIndex(lastSelectCellId))
	{
		if (table->cellAtIndex(lastSelectCellId)->getChildByTag(GENERALSSTRATRGIESUI_TABALEVIEW_HIGHLIGHT_TAG))
		{
			table->cellAtIndex(lastSelectCellId)->getChildByTag(GENERALSSTRATRGIESUI_TABALEVIEW_HIGHLIGHT_TAG)->removeFromParent();
		}
	}

	CCScale9Sprite* pHighlightSpr = CCScale9Sprite::create(CCRectMake(26, 26, 1, 1) , "res_ui/highlight.png");
	pHighlightSpr->setPreferredSize(CCSize(245,65));
	pHighlightSpr->setAnchorPoint(CCPointZero);
	pHighlightSpr->setTag(GENERALSSTRATRGIESUI_TABALEVIEW_HIGHLIGHT_TAG);
	cell->addChild(pHighlightSpr);

	lastSelectCellId = cell->getIdx();
}

cocos2d::CCSize GeneralsStrategiesUI::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeMake(245,65);
}

cocos2d::extension::CCTableViewCell* GeneralsStrategiesUI::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell = new CCTableViewCell();
	cell->autorelease();
	//大边框
	CCScale9Sprite * sprite_bigFrame = CCScale9Sprite::create("res_ui/kuang01_new.png");
	sprite_bigFrame->setAnchorPoint(ccp(0, 0));
	sprite_bigFrame->setPosition(ccp(0, 0));
	sprite_bigFrame->setCapInsets(CCRectMake(15,31,1,1));
	sprite_bigFrame->setContentSize(CCSizeMake(244,63));
	cell->addChild(sprite_bigFrame);
	//名字小边框
	CCScale9Sprite *sprite_nameFrame = CCScale9Sprite::create("res_ui/LV4_diaa.png");
	sprite_nameFrame->setAnchorPoint(ccp(0, 0));
	sprite_nameFrame->setPosition(ccp(83, 17));
	sprite_nameFrame->setCapInsets(CCRectMake(11,11,1,1));
	sprite_nameFrame->setContentSize(CCSizeMake(146,30));
	sprite_bigFrame->addChild(sprite_nameFrame);

	//头像小边框
	CCScale9Sprite *sprite_headFrame = CCScale9Sprite::create("res_ui/LV4_allb.png");
	sprite_headFrame->setAnchorPoint(ccp(0.5f, 0.5f));
	sprite_headFrame->setPosition(ccp(41, sprite_bigFrame->getContentSize().height/2));
	sprite_headFrame->setCapInsets(CCRectMake(12,12,1,1));
	sprite_headFrame->setContentSize(CCSizeMake(52,52));
	sprite_bigFrame->addChild(sprite_headFrame);

	if (this->selectCellIndex == idx)
	{
		CCScale9Sprite* pHighlightSpr = CCScale9Sprite::create(CCRectMake(26, 26, 1, 1) , "res_ui/highlight.png");
		pHighlightSpr->setPreferredSize(CCSize(244,65));
		pHighlightSpr->setAnchorPoint(CCPointZero);
		pHighlightSpr->setTag(GENERALSSTRATRGIESUI_TABALEVIEW_HIGHLIGHT_TAG);
		cell->addChild(pHighlightSpr);
	}

	std::string str_iconPath = "res_ui/MatrixMethod/";
	str_iconPath.append(generalsStrategiesList.at(idx)->get_icon());
	str_iconPath.append(".png");
	CCSprite *  sprite_image = CCSprite::create(str_iconPath.c_str());
	sprite_image->setAnchorPoint(ccp(0.5f,0.5f));
	sprite_image->setPosition(ccp(41,sprite_bigFrame->getContentSize().height/2));
	sprite_bigFrame->addChild(sprite_image);

	CCLabelTTF * label_stratrgiesName = CCLabelTTF::create(generalsStrategiesList.at(idx)->get_name().c_str(),APP_FONT_NAME,15);
	label_stratrgiesName->setAnchorPoint(ccp(0.5f, 0.5f));
	label_stratrgiesName->setPosition(ccp(117,sprite_bigFrame->getContentSize().height/2));
	ccColor3B shadowColor = ccc3(0,0,0);   // black
	label_stratrgiesName->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
	sprite_bigFrame->addChild(label_stratrgiesName);

	CCScale9Sprite* sprite_mengban = CCScale9Sprite::create(CCRectMake(1, 1, 1, 1) , "res_ui/zhezhao50.png");
	sprite_mengban->setPreferredSize(CCSize(43,43));
	sprite_mengban->setAnchorPoint(ccp(0.5f,0.5f));
	sprite_mengban->setPosition(ccp(41,sprite_bigFrame->getContentSize().height/2));
	sprite_bigFrame->addChild(sprite_mengban);

	CCSprite *  sprite_disabled = CCSprite::create("res_ui/suo_2.png");
	sprite_disabled->setAnchorPoint(ccp(0.5f,0.5f));
	sprite_disabled->setPosition(ccp(41,sprite_bigFrame->getContentSize().height/2));
	sprite_disabled->setScale(0.8f);
	sprite_bigFrame->addChild(sprite_disabled);

	std::string str_level = "LV ";
	char s_level[5];
	if (generalsStrategiesList.at(idx)->level() <= 1)
	{
		sprintf(s_level,"%d",1);
	}
	else
	{
		sprintf(s_level,"%d",generalsStrategiesList.at(idx)->level());
	}
	str_level.append(s_level);
	CCLabelTTF * label_stratrgiesLv = CCLabelTTF::create(str_level.c_str(),APP_FONT_NAME,15);
	label_stratrgiesLv->setAnchorPoint(ccp(0, 0.5f));
	label_stratrgiesLv->setPosition(ccp(173,sprite_bigFrame->getContentSize().height/2));
	label_stratrgiesLv->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
	sprite_bigFrame->addChild(label_stratrgiesLv);

	//启用标识
	CCSprite *  sprite_flag = CCSprite::create("res_ui/wujiang/StartUsing.png");
	sprite_flag->setAnchorPoint(ccp(0.5,0.5f));
	sprite_flag->setPosition(ccp(15,sprite_bigFrame->getContentSize().height/2+12));
	sprite_bigFrame->addChild(sprite_flag);
	sprite_flag->setVisible(false);

	if (generalsStrategiesList.at(idx)->level() <= 0)
	{
		sprite_mengban->setVisible(true);
		sprite_disabled->setVisible(true);
	}
	else
	{
		sprite_mengban->setVisible(false);
		sprite_disabled->setVisible(false);
	}

	if (generalsStrategiesList.at(idx)->activeflag() == 0)
	{
		GameUtils::addGray((CCSprite*)sprite_bigFrame);
		sprite_flag->setVisible(false);
	}
	else 
	{
		GameUtils::removeGray((CCSprite*)sprite_bigFrame);
		sprite_flag->setVisible(true);
	}

	return cell;
}

unsigned int GeneralsStrategiesUI::numberOfCellsInTableView( CCTableView *table )
{
	return generalsStrategiesList.size();
}

void GeneralsStrategiesUI::RefreshGeneralsStrategiesList()
{
	sort(generalsStrategiesList.begin(),generalsStrategiesList.end(),SortFightWay);
	this->strategiesList_tableView->reloadData();
	AutoSetOffSet();
}

void GeneralsStrategiesUI::setVisible( bool visible )
{
	panel_strategies->setVisible(visible);
	Layer_strategies->setVisible(visible);
	Layer_upper->setVisible(visible);
}


bool GeneralsStrategiesUI::isVisible()
{
	return panel_strategies->isVisible();
}


int GeneralsStrategiesUI::getActiveFightWay()
{
	return this->activeFightWayId;
}

void GeneralsStrategiesUI::setActiveFightWay( int _id )
{
	this->activeFightWayId = _id;
}

void GeneralsStrategiesUI::setActiveIndicator()
{
	if (this->activeFightWayId > 0)
	{

	}
}

int GeneralsStrategiesUI::getCurSelectFightWayId()
{
	return curSelectFightWayId;
}

void GeneralsStrategiesUI::RefreshData()
{
	this->RefreshGeneralsStrategiesList();
}

void GeneralsStrategiesUI::RefreshAllGeneralsInLine()
{
	for (int i = 0;i<5;++i)
	{
		if (Layer_strategies->getChildByTag(PositionItemBaseTag + i+1))
		{
			Layer_strategies->getChildByTag(PositionItemBaseTag + i+1)->removeFromParent();
		}
	}

	for(int i = 0;i<5;++i)
	{
		bool ishavegeneral = false;
		for (int j = 0;j<GameView::getInstance()->generalsInLineList.size();++j)
		{
			if (GameView::getInstance()->generalsInLineList.at(j)->order() == i+1)
			{
				StrategiesPositionItem * positionItem = StrategiesPositionItem::create(GameView::getInstance()->generalsInLineList.at(j));
				positionItem->ignoreAnchorPointForPosition(false);
				positionItem->setAnchorPoint(ccp(0,0));
				positionItem->setPosition(slotPos[i]);
				Layer_strategies->addChild(positionItem,0,PositionItemBaseTag+i+1);
				ishavegeneral = true;
				break;
			}
		}

		if (!ishavegeneral)
		{
			StrategiesPositionItem * positionItem = StrategiesPositionItem::create(i+1);
			positionItem->ignoreAnchorPointForPosition(false);
			positionItem->setAnchorPoint(ccp(0,0));
			positionItem->setPosition(slotPos[i]);
			Layer_strategies->addChild(positionItem,0,PositionItemBaseTag+i+1);
		}
	}
}

void GeneralsStrategiesUI::RefreshOneGeneralsInLine(CGeneralBaseMsg * generalBaseMsg)
{	
	for (int i = 0;i<6;i++)
	{
		StrategiesPositionItem * temp = (StrategiesPositionItem * )Layer_strategies->getChildByTag(PositionItemBaseTag + i);
		if (!temp)
			continue;

		if (temp->genenralsId == generalBaseMsg->id())
		{
			int index = temp->positionIndex;
			temp->removeFromParent();

			StrategiesPositionItem * positionItem = StrategiesPositionItem::create(index);
			positionItem->ignoreAnchorPointForPosition(false);
			positionItem->setAnchorPoint(ccp(0,0));
			positionItem->setPosition(slotPos[index-1]);
			Layer_strategies->addChild(positionItem,0,PositionItemBaseTag+index);
		}
	}

	if (Layer_strategies->getChildByTag(PositionItemBaseTag + generalBaseMsg->order()))
	{
		Layer_strategies->getChildByTag(PositionItemBaseTag + generalBaseMsg->order())->removeFromParent();
	}
 
	StrategiesPositionItem * positionItem = StrategiesPositionItem::create(generalBaseMsg);
	positionItem->ignoreAnchorPointForPosition(false);
	positionItem->setAnchorPoint(ccp(0,0));
	positionItem->setPosition(slotPos[generalBaseMsg->order()-1]);
	Layer_strategies->addChild(positionItem,0,PositionItemBaseTag+generalBaseMsg->order());
}

void GeneralsStrategiesUI::RefreshStrategiesInfo( CFightWayBase * fightWayBase )
{
	//阵法名称
	//l_strategieName->setText(fightWayBase->get_name().c_str());

	CFightWayGrowingUp * fightWayGrowingUp;
	if (fightWayBase->level()>0)
	{
		fightWayGrowingUp = FightWayGrowingUpConfigData::s_fightWayGrowingUp[fightWayBase->fightwayid()]->at(fightWayBase->level());
	}
	else
	{
		fightWayGrowingUp = FightWayGrowingUpConfigData::s_fightWayGrowingUp[fightWayBase->fightwayid()]->at(1);
	}

	//龙魂状态的描述信息
	std::string str_property = "";
	str_property.append("generals_strategies_property_");
	char s_complex_property[10];
	sprintf(s_complex_property,"%d",fightWayGrowingUp->get_complex_property());
	str_property.append(s_complex_property);
	const char *shuxing = StringDataManager::getString(str_property.c_str());

	std::string property_value = shuxing;
	property_value.append("+");
	int general_property_all = 0;
	for(int i = 0;i<GameView::getInstance()->generalsInLineList.size();++i)
	{
		CGeneralBaseMsg * temp = GameView::getInstance()->generalsInLineList.at(i);
		general_property_all += (temp->currentquality() + temp->evolution() + temp->rare());
	}
	int complex_value = 0;
	//if (fightWayBase->level()>0)
	//{
		complex_value = fightWayGrowingUp->get_complex_value();
	//}
	//else
	//{
	//}
	int countLevel = 1;
	if (fightWayBase->level() > 1)
	{
		countLevel = fightWayBase->level();
	}
	float final_result = 0.02*general_property_all*countLevel*complex_value;
	char str_final_result[20];
	sprintf(str_final_result,"%.f",final_result);
	property_value.append(str_final_result);
	property_value.append("%");
	l_addPropDes->setText(property_value.c_str());

	//刷新各个阵位的属性加成信息
	for (int i = 0;i<5;++i)
	{
		if (Layer_strategies->getChildByTag(PositionItemBaseTag + i+1))
		{
			StrategiesPositionItem * positionItem = (StrategiesPositionItem *)Layer_strategies->getChildByTag(PositionItemBaseTag + i+1);
			positionItem->RefreshAddedProperty();
		}
	}
}


void GeneralsStrategiesUI::RefreshButtonState( CFightWayBase * fightWayBase )
{
	if (fightWayBase->level()>0)
	{
		btn_openStrategies->setVisible(true);
		btn_openStrategies->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		btn_openStrategies->setTouchEnable(true);
		btn_closeStrategies->setVisible(false);

		const char *str_upgrade = StringDataManager::getString("generals_strategies_upgradeFightWay");
		l_upgrade->setText(str_upgrade);
	}
	else
	{
		btn_openStrategies->setVisible(true);
		btn_openStrategies->setTextures("res_ui/new_button_001.png","res_ui/new_button_001.png","");
		btn_openStrategies->setTouchEnable(false);
		btn_closeStrategies->setVisible(false);

		const char *str_study = StringDataManager::getString("generals_strategies_studyFightWay");
		l_upgrade->setText(str_study);
	}

	//设置开启/关闭按钮
	if(fightWayBase->activeflag() == 1)
	{
		btn_openStrategies->setVisible(false);
		btn_closeStrategies->setVisible(true);
	}
	else
	{
		btn_openStrategies->setVisible(true);
		btn_closeStrategies->setVisible(false);
	}
}

void GeneralsStrategiesUI::RefreshListToDefault()
{
	if (generalsStrategiesList.size()<=0)
		return;

	for (int i = 0;i<generalsStrategiesList.size();++i)
	{
		if (generalsStrategiesList.at(i)->activeflag() == 1)
		{
			activeFightWayId = generalsStrategiesList.at(i)->fightwayid();
		}
	}

	selectCellIndex = 0;
	curSelectFightWayId = generalsStrategiesList.at(0)->fightwayid();

	if (generalsStrategiesList.at(selectCellIndex)->level()==0)
	{
		curFightWayStatus = NotStuded;
	}
	else 
	{
		if (generalsStrategiesList.at(selectCellIndex)->level()==10)
		{
			curFightWayStatus = Highest;
		}
		else
		{
			curFightWayStatus = Studed;
		}
	}

	l_strategieName->setText(generalsStrategiesList.at(selectCellIndex)->get_name().c_str());
	RefreshStrategiesInfo(generalsStrategiesList.at(selectCellIndex));
	RefreshButtonState(generalsStrategiesList.at(selectCellIndex));

	sort(generalsStrategiesList.begin(),generalsStrategiesList.end(),SortFightWay);
	strategiesList_tableView->reloadData();
	AutoSetOffSet();
}

void GeneralsStrategiesUI::RefreshListWithoutChangeOffSet()
{
	for (int i = 0;i<generalsStrategiesList.size();++i)
	{
		if (generalsStrategiesList.at(i)->activeflag() == 1)
		{
			activeFightWayId = generalsStrategiesList.at(i)->fightwayid();
			//selectCellIndex = i;
			//curSelectFightWayId = generalsStrategiesList.at(i)->fightwayid();
		}
	}

	if (activeFightWayId > 0)
	{

	}
	else
	{
		if (generalsStrategiesList.size()>0)
		{
			selectCellIndex = 0;
			curSelectFightWayId = generalsStrategiesList.at(0)->fightwayid();
		}
	}

	if (generalsStrategiesList.at(selectCellIndex)->level()==0)
	{
		curFightWayStatus = NotStuded;
	}
	else 
	{
		if (generalsStrategiesList.at(selectCellIndex)->level()==10)
		{
			curFightWayStatus = Highest;
		}
		else
		{
			curFightWayStatus = Studed;
		}
	}

	RefreshStrategiesInfo(generalsStrategiesList.at(selectCellIndex));
	RefreshButtonState(generalsStrategiesList.at(selectCellIndex));


	sort(generalsStrategiesList.begin(),generalsStrategiesList.end(),SortFightWay);
	CCPoint _pt = strategiesList_tableView->getContentOffset();
	strategiesList_tableView->reloadData();
	strategiesList_tableView->setContentOffset(_pt);
}

void GeneralsStrategiesUI::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void GeneralsStrategiesUI::addCCTutorialIndicator1( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,70,60);
// 	tutorialIndicator->setPosition(ccp(pos.x-40,pos.y+95));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	Layer_upper->addChild(tutorialIndicator);

	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,100,43,true);
	tutorialIndicator->setDrawNodePos(ccp(pos.x+_w,pos.y+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void GeneralsStrategiesUI::removeCCTutorialIndicator()
{
	CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(Layer_upper->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		CCTeachingGuide* teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}

void GeneralsStrategiesUI::RefreshAllInfo( CFightWayBase * fightWayBase )
{
	if(curSelectFightWayId != fightWayBase->fightwayid())
		return;

	//设置阵法当前状态
	if (fightWayBase->level()==0)
	{
		curFightWayStatus = NotStuded;
	}
	else 
	{
		if (fightWayBase->level()==10)
		{
			curFightWayStatus = Highest;
		}
		else
		{
			curFightWayStatus = Studed;
		}
	}
	//设置描述以及属性加成
	RefreshStrategiesInfo(fightWayBase);
	RefreshButtonState(fightWayBase);

	//设置学习/升级按钮的显示
	//CCDictionary *strings = CCDictionary::createWithContentsOfFile("res_ui/font/strings.xml");
// 	if (curFightWayStatus == NotStuded)
// 	{
// 		//const char *str_study  = ((CCString*)strings->objectForKey("generals_strategies_studyFightWay"))->m_sString.c_str();
// 		const char *str_study = StringDataManager::getString("generals_strategies_studyFightWay");
// 		l_upgrade->setText(str_study);
// 	}
// 	else
// 	{
// 		//const char *str_upgrade  = ((CCString*)strings->objectForKey("generals_strategies_upgradeFightWay"))->m_sString.c_str();
// 		const char *str_upgrade = StringDataManager::getString("generals_strategies_upgradeFightWay");
// 		l_upgrade->setText(str_upgrade);
// 	}


}

void GeneralsStrategiesUI::AutoSetOffSet()
{
	int selectIndex = -1;
	for (int i = 0;i<generalsStrategiesList.size();i++)
	{
		if (curSelectFightWayId == generalsStrategiesList.at(i)->fightwayid())
		{
			selectIndex = i;
			break;
		}
	}

	if (selectIndex == -1)
		return;

	strategiesList_tableView->selectCell(selectIndex);

	if (generalsStrategiesList.size()*tableCellSizeForIndex(strategiesList_tableView,0).height < strategiesList_tableView->getViewSize().height)
	{
		//m_tableView->setContentOffset(ccp(0,0));
	}
	else
	{
		if ((generalsStrategiesList.size()-selectIndex)*tableCellSizeForIndex(strategiesList_tableView,0).height  <= strategiesList_tableView->getViewSize().height)  //是最后几个
		{
			strategiesList_tableView->setContentOffset(ccp(0,0));
		}
		else
		{
			int beginHeight = strategiesList_tableView->getViewSize().height - generalsStrategiesList.size()*tableCellSizeForIndex(strategiesList_tableView,0).height;
			strategiesList_tableView->setContentOffset(ccp(0,beginHeight+selectIndex*tableCellSizeForIndex(strategiesList_tableView,0).height));
		}
	}
}

void GeneralsStrategiesUI::setStrategiesListToDefault()
{
	if (generalsStrategiesList.size()>0)
	{
		std::vector<CFightWayBase*>::iterator iter_generalStrategies;
		for (iter_generalStrategies = generalsStrategiesList.begin(); iter_generalStrategies != generalsStrategiesList.end(); ++iter_generalStrategies)
		{
			delete *iter_generalStrategies;
		}
		generalsStrategiesList.clear();
	}

	for (int i = 0;i<FightWayConfigData::s_fightWayBase.size();++i)
	{
		CFightWayBase * temp = new CFightWayBase();
		temp->CopyFrom(*FightWayConfigData::s_fightWayBase.at(i));
		temp->set_name(FightWayConfigData::s_fightWayBase.at(i)->get_name());
		temp->set_kValue(FightWayConfigData::s_fightWayBase.at(i)->get_kValue());
		temp->set_icon(FightWayConfigData::s_fightWayBase.at(i)->get_icon());
		temp->set_description(FightWayConfigData::s_fightWayBase.at(i)->get_description());
		generalsStrategiesList.push_back(temp);
	}
}
