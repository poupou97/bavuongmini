#ifndef  _GENERALSUI_GENERALSSTRATEGIESUI_H_
#define _GENERALSUI_GENERALSSTRATEGIESUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "../../messageclient/protobuf/RecruitMessage.pb.h"
#include "../../legend_script/CCTutorialIndicator.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CGeneralBaseMsg;
class CGeneralDetail;
class NormalGeneralsHeadItemBase;
class CFightWayBase;

#define GeneralsStrategiesPropertyBase "generals_strategies_property_"

/////////////////////////////////
/**
 * 武将界面下的 阵法类
 * @author yangjun
 * @version 0.1.0
 * @date 2013.12.4
 */

class GeneralsStrategiesUI : public UIScene,public CCTableViewDelegate,public CCTableViewDataSource
{
public:
	GeneralsStrategiesUI();
	~GeneralsStrategiesUI();

	enum PropertyType{
		fightway_strength = 1,
        fightway_dexterity,
        fightway_intelligence,
        fightway_focus,
		fightway_hp_capacity = 11,
		fightway_mp_capacity,
		fightway_min_attack,
		fightway_max_attack,
		fightway_min_magic_attack,
		fightway_max_magic_attack,
		fightway_min_defend,
		fightway_max_defend,
		fightway_min_magic_defend,
		fightway_max_magic_defend,
		fightway_hit,
		fightway_dodge,
		fightway_crit,
        fightway_crit_damage,
		fightway_attack_speed,
		fightway_move_speed,
	};

	enum FightWayStatus{
		NotStuded = 0,  //未学会
	    Studed,              //已学未使用
		Highest               //满级
	};

	static GeneralsStrategiesUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);

	void StrategiesResetEvent(CCObject * pSender);
	void StrategiesLeveUpEvent(CCObject * pSender);
	void StrategiesUseEvent(CCObject * pSender);
	void StrategiesNotUseEvent(CCObject * pSender);
	//详细说明
	void StrategiesDetailEvent(CCObject * pSender);

	int getActiveFightWay();
	void setActiveFightWay(int _id);

	void setActiveIndicator();

	void RefreshStrategiesInfo(CFightWayBase * fightWayBase);
	void RefreshButtonState(CFightWayBase * fightWayBase);

	//升级完刷新数据
	void RefreshAllInfo(CFightWayBase * fightWayBase);

	void setStrategiesListToDefault();

private:
	int lastSelectCellId;

	UIPanel * panel_strategies; //阵法面板
	UILayer * Layer_strategies;
	UILayer * Layer_upper;
	CCPoint slotPos[5] ;

	UILabel *l_upgrade;
	UIButton *btn_closeStrategies;

	UILabel * l_strategieName;
	UILabel * l_addPropDes;

	//选中的Cell的Index
	int selectCellIndex;

	CCTableView * strategiesList_tableView;
	//当前激活阵法id
	int activeFightWayId ;
	//当前选中阵法id
	int curSelectFightWayId;
	//当前选中阵法状态
	FightWayStatus curFightWayStatus;

public:
	//阵法列表
	std::vector<CFightWayBase *> generalsStrategiesList;
	//上阵武将列表
	//std::vector<CGeneralBaseMsg *>generalsInLineList;

	int getCurSelectFightWayId();
	//刷新阵法界面信息
	void RefreshData();
	//刷新阵法列表
	void RefreshGeneralsStrategiesList();
	//刷新所有上阵武将信息
	void RefreshAllGeneralsInLine();
	//刷新单个上阵武将信息
	void RefreshOneGeneralsInLine(CGeneralBaseMsg * generalBaseMsg);
	//自动设置列表的偏移量
	void AutoSetOffSet();

	void RefreshListToDefault();
	void RefreshListWithoutChangeOffSet();

	virtual bool isVisible();
	virtual void setVisible(bool visible);

	//教学
	virtual void registerScriptCommand(int scriptId);
	//学习阵法
	void addCCTutorialIndicator1(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction);
	void removeCCTutorialIndicator();

public:
	UIButton *btn_levelUpStrategies;
	UIButton *btn_openStrategies;
private:
	//教学
	int mTutorialScriptInstanceId;
};

#endif

