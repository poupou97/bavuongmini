#ifndef _INSTANCE_INSTANCEFUNCTIONPANEL_H_
#define _INSTANCE_INSTANCEFUNCTIONPANEL_H_

#include "../../ui/extensions/uiscene.h"
#include "cocos-ext.h"
#include "cocos2d.h"

class InstanceFunctionPanel :public UIScene
{
public:
	InstanceFunctionPanel();
	~InstanceFunctionPanel();

	static InstanceFunctionPanel* create();
	bool init();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);

	virtual void onEnter();
	virtual void onExit();

	void callFriendCallback(CCObject* pSender);
	void exitInstanceCallback(CCObject* pSender);

	void sureToExit(CCObject *pSender);

private:

};

#endif;

