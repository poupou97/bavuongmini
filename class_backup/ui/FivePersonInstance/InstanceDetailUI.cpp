#include "InstanceDetailUI.h"

#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/CFivePersonInstance.h"
#include "../../utils/StaticDataManager.h"
#include "../extensions/UITab.h"
#include "../../GameView.h"
#include "InstanceRewardUI.h"
#include "../../gamescene_state/MainScene.h"
#include "AppMacros.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/Script.h"
#include "FivePersonInstance.h"
#include "../../utils/GameUtils.h"
#include "../../legend_script/CCTeachingGuide.h"
#include "../../ui/backpackscene/PackageItem.h"

#define UISCENE_MAIN_SCENE_TAG 1000
#define INSTANCEDETAILUI_TABALEVIEW_HIGHLIGHT_TAG 456
#define M_SCROLLVIEWTAG_DES 567
#define M_SCROLLVIEWTAG_EXPLAIN 568

#define kTag_StarItem_Base 900
#define kTag_RewardItem_Base 800

InstanceDetailUI::InstanceDetailUI():
curInstanceId(-1)
{
}

InstanceDetailUI::~InstanceDetailUI()
{
}

InstanceDetailUI * InstanceDetailUI::create(int instanceId)
{
	InstanceDetailUI * _ui=new InstanceDetailUI();
	if (_ui && _ui->init(instanceId))
	{
		_ui->autorelease();
		return _ui;
	}
	CC_SAFE_DELETE(_ui);
	return NULL;
}

bool InstanceDetailUI::init(int instanceId)
{
	if (UIScene::init())
	{
		curInstanceId = instanceId;
		CCSize size=CCDirector::sharedDirector()->getVisibleSize();

		layer=UILayer::create();
		addChild(layer);

		if(LoadSceneLayer::instanceDetailUiPanel->getWidgetParent() != NULL)
		{
			LoadSceneLayer::instanceDetailUiPanel->removeFromParentAndCleanup(false);
		}

		UIPanel *mainPanel=LoadSceneLayer::instanceDetailUiPanel;
		mainPanel->setAnchorPoint(ccp(0,0));
		mainPanel->setPosition(ccp(0,0));
		mainPanel->setWidgetTag(UISCENE_MAIN_SCENE_TAG);
		m_pUiLayer->addWidget(mainPanel);

		btnSignUp= (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_SignUp");
		CCAssert(btnSignUp != NULL, "should not be nil");
		btnSignUp->setTouchEnable(true);
		btnSignUp->setPressedActionEnabled(true);
		btnSignUp->setVisible(true); 
		btnSignUp->addReleaseEvent(this,coco_releaseselector(InstanceDetailUI::signUpCallback));

		btnCancelSign= (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_CancelSign");
		CCAssert(btnCancelSign != NULL, "should not be nil");
		btnCancelSign->setTouchEnable(true);
		btnCancelSign->setPressedActionEnabled(true);
		btnCancelSign->setVisible(false);   // default value, invisible
		btnCancelSign->addReleaseEvent(this,coco_releaseselector(InstanceDetailUI::cancelSignCallback));

		if (FivePersonInstance::getStatus() == FivePersonInstance::status_in_sequence)
		{
			btnSignUp->setVisible(false);
			btnCancelSign->setVisible(true);
		}
		else
		{
			btnSignUp->setVisible(true);
			btnCancelSign->setVisible(false);
		}

// 		UIButton * btnCreateTeam= (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_CreatTeam");
// 		CCAssert(btnCreateTeam != NULL, "should not be nil");
// 		btnCreateTeam->setTouchEnable(true);
// 		btnCreateTeam->setPressedActionEnabled(true);
// 		btnCreateTeam->addReleaseEvent(this,coco_releaseselector(InstanceDetailUI::createTeamCallback));
	
		btnEnter= (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_enter");
		CCAssert(btnEnter != NULL, "should not be nil");
		btnEnter->setTouchEnable(true);
		btnEnter->setPressedActionEnabled(true);
		btnEnter->addReleaseEvent(this,coco_releaseselector(InstanceDetailUI::enterInstanceCallback));

		UIButton * btn_Exit= (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_close");
		btn_Exit->setTouchEnable(true);
		btn_Exit->setPressedActionEnabled(true);
		btn_Exit->addReleaseEvent(this,coco_releaseselector(InstanceDetailUI::callBackExit));

		UIButton * btnReward= (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_reward");
		CCAssert(btnReward != NULL, "should not be nil");
		btnReward->setTouchEnable(true);
		btnReward->setPressedActionEnabled(true);
		btnReward->addReleaseEvent(this,coco_releaseselector(InstanceDetailUI::btnRewardCallback));

		l_instanceName = (UILabelBMFont*)UIHelper::seekWidgetByName(mainPanel,"Label_InstanceName");
		CCAssert(l_instanceName != NULL, "should not be nil");
		l_open_lv = (UILabel*)UIHelper::seekWidgetByName(mainPanel,"Label_OpenLv_value");
		CCAssert(l_open_lv != NULL, "should not be nil");
		l_consume = (UILabel*)UIHelper::seekWidgetByName(mainPanel,"Label_consume_value");
		CCAssert(l_consume != NULL, "should not be nil");
		l_open_time = (UILabel*)UIHelper::seekWidgetByName(mainPanel,"Label_OpenTime_value");
		CCAssert(l_open_time != NULL, "should not be nil");
		l_difficult = (UILabel*)UIHelper::seekWidgetByName(mainPanel,"Label_difficult_value");
		CCAssert(l_difficult != NULL, "should not be nil");
		l_suggest = (UILabel*)UIHelper::seekWidgetByName(mainPanel,"Label_suggestValue");
		CCAssert(l_suggest != NULL, "should not be nil");
		l_combat = (UILabel*)UIHelper::seekWidgetByName(mainPanel,"Label_combat_value");
		CCAssert(l_combat != NULL, "should not be nil");
		
		//副本介绍
		int scroll_height = 0;
		std::string instanceDes = "";
		const char *str_fubenjieshao = StringDataManager::getString("fivePerson_fubenjieshao");
		instanceDes.append(str_fubenjieshao);
		l_des_head = CCLabelTTF::create(instanceDes.c_str(),APP_FONT_NAME,18,CCSizeMake(290,0),kCCTextAlignmentLeft,kCCVerticalTextAlignmentTop);
		l_des_head->setColor(ccc3(47,93,13));
		int zeroLineHeight = l_des_head->getContentSize().height;
		scroll_height = zeroLineHeight;
		
		l_des = CCLabelTTF::create("",APP_FONT_NAME,18,CCSizeMake(290,0),kCCTextAlignmentLeft,kCCVerticalTextAlignmentTop);
		l_des->setColor(ccc3(47,93,13));
		int firstLineHeight = zeroLineHeight+l_des->getContentSize().height;
		scroll_height = firstLineHeight+10;

		//副本说明
		std::string instanceExplain = "";
		const char *str_fubenshuoming = StringDataManager::getString("fivePerson_fubenshuoiming");
		instanceExplain.append(str_fubenshuoming);
		l_info_head = CCLabelTTF::create(instanceExplain.c_str(),APP_FONT_NAME,18,CCSizeMake(290,0),kCCTextAlignmentLeft,kCCVerticalTextAlignmentTop);
		l_info_head->setColor(ccc3(47,93,13));
		int secondLineHeight =  firstLineHeight+l_info_head->getContentSize().height;
		scroll_height = secondLineHeight;

		//副本说明描述
		l_info = CCLabelTTF::create("",APP_FONT_NAME,18,CCSizeMake(290,0),kCCTextAlignmentLeft,kCCVerticalTextAlignmentTop);
		l_info->setColor(ccc3(47,93,13));
		int thirdLineHeight = secondLineHeight + l_info->getContentSize().height;
		scroll_height = thirdLineHeight+10;

		m_scrollView_des = CCScrollView::create(CCSizeMake(325,294));
		m_scrollView_des->setViewSize(CCSizeMake(325, 294));
		m_scrollView_des->ignoreAnchorPointForPosition(false);
		m_scrollView_des->setTouchEnabled(true);
		m_scrollView_des->setDirection(kCCScrollViewDirectionVertical);
		m_scrollView_des->setAnchorPoint(ccp(0,0));
		m_scrollView_des->setPosition(ccp(388,80));
		m_scrollView_des->setBounceable(true);
		m_scrollView_des->setClippingToBounds(true);
		m_scrollView_des->setTag(M_SCROLLVIEWTAG_DES);
		layer->addChild(m_scrollView_des);
		if (scroll_height > 294)
		{
			m_scrollView_des->setContentSize(CCSizeMake(325,scroll_height));
			m_scrollView_des->setContentOffset(ccp(0,294-scroll_height));  
		}
		else
		{
			m_scrollView_des->setContentSize(ccp(325,294));
		}
		m_scrollView_des->setClippingToBounds(true);

		m_scrollView_des->addChild(l_des_head);
		l_des_head->setPosition(ccp(36,m_scrollView_des->getContentSize().height - zeroLineHeight-7));

		m_scrollView_des->addChild(l_des);
		l_des->setPosition(ccp(36,m_scrollView_des->getContentSize().height - firstLineHeight-7));

		m_scrollView_des->addChild(l_info_head);
		l_info_head->setPosition(ccp(36,m_scrollView_des->getContentSize().height - secondLineHeight-7));

		m_scrollView_des->addChild(l_info);
		l_info->setPosition(ccp(36,m_scrollView_des->getContentSize().height - thirdLineHeight-7));

		RefreshInstanceInfo(instanceId);

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(mainPanel->getContentSize());
		return true;
	}
	return false;
}

void InstanceDetailUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void InstanceDetailUI::onExit()
{
	UIScene::onExit();
}

bool InstanceDetailUI::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
	// convert the touch point to OpenGL coordinates
// 	CCPoint location = pTouch->getLocation();
// 
// 	CCPoint pos = this->getPosition();
// 	CCSize size = this->getContentSize();
// 	CCPoint anchorPoint = this->getAnchorPointInPoints();
// 	pos = ccpSub(pos, anchorPoint);
// 	CCRect rect(pos.x, pos.y, size.width, size.height);
// 	if(rect.containsPoint(location))
// 	{
// 		return true;
// 	}
// 
// 	return false;
	return true;
}
void InstanceDetailUI::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
{

}
void InstanceDetailUI::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
{

}
void InstanceDetailUI::ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent)
{

}

void InstanceDetailUI::callBackExit(CCObject * pSender)
{
	this->closeAnim();
// 	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
// 
// 	CCMoveTo * moveTo = CCMoveTo::create(.3f,ccp(winSize.width+this->getContentSize().width,winSize.height/2));
// 	CCEaseBackIn * easeIn = CCEaseBackIn::create(moveTo);
// 	CCSequence * sequence = CCSequence::create(easeIn,CCRemoveSelf::create(),NULL);
// 	this->runAction(sequence);
}

void InstanceDetailUI::signUpCallback(CCObject* pSender)
{
	// request to instance waiting sequence
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1920,(void *)curInstanceId);
}
void InstanceDetailUI::cancelSignCallback(CCObject* pSender)
{
	if (FivePersonInstance::getSignedUpId() != -1)
	{
		// request to cancel the waiting sequence
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1922,(void*)FivePersonInstance::getSignedUpId());
	}
}

void InstanceDetailUI::enterInstanceCallback(CCObject* pSender)
{
	// request to enter instance directly
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1921,(void *)curInstanceId);

	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(pSender);

	this->closeAnim();
}

void InstanceDetailUI::createTeamCallback(CCObject* pSender)
{
}


void InstanceDetailUI::btnRewardCallback( CCObject* pSender )
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagInstanceRewardUI) == NULL)
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		InstanceRewardUI * instanceRewardUI = InstanceRewardUI::create();
		GameView::getInstance()->getMainUIScene()->addChild(instanceRewardUI,0,kTagInstanceRewardUI);
		instanceRewardUI->ignoreAnchorPointForPosition(false);
		instanceRewardUI->setAnchorPoint(ccp(0.5f,0.5f));
		instanceRewardUI->setPosition(ccp(winSize.width/2,winSize.height/2));
	}
}

void InstanceDetailUI::RefreshInstanceInfo( int instanceId )
{
	CFivePersonInstance * fivePersonInstance;
	bool isExist = false;
	for (int i = 0;i<FivePersonInstanceConfigData::s_fivePersonInstance.size();i++)
	{
		fivePersonInstance = FivePersonInstanceConfigData::s_fivePersonInstance.at(i);
		if (instanceId == fivePersonInstance->get_id())
		{
			curInstanceId = instanceId;
			isExist = true;
			break;
		}
	}

	if (!isExist)
		return;

	//请求当前选中副本的奖励信息
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1926,(void *)instanceId); 
	
	//star info
	for (int i = 0;i<5;i++)
	{
		if (layer->getWidgetByTag(kTag_StarItem_Base+i))
		{
			layer->getWidgetByTag(kTag_StarItem_Base+i)->removeFromParent();
		}
	}
	int star = 0;
	std::map<int, int>::iterator it = FivePersonInstance::getInstance()->s_m_copyStar.begin();
	for ( ; it != FivePersonInstance::getInstance()->s_m_copyStar.end(); ++ it )
	{
		if (it->first == instanceId)
		{
			star = it->second;
			break;
		}
	}
	for (int i = 0;i<5;i++)
	{
		UIImageView * pIcon = UIImageView::create();
		if (i<star)
		{
			pIcon->setTexture("res_ui/star_on.png");
		}
		else
		{
			pIcon->setTexture("res_ui/star_off.png");
		}
		pIcon->setScale(1.0f);
		pIcon->setAnchorPoint(ccp(0.5f, 0.5f));
		pIcon->setPosition(ccp(240 +25*i, 400));
		pIcon->setTag(kTag_StarItem_Base+i);
		layer->addWidget(pIcon);
	}

	//名字
	l_instanceName->setText(fivePersonInstance->get_name().c_str());
	//开放等级
	std::string open_lv = "";
	char s_open_level[10];
	sprintf(s_open_level,"%d",fivePersonInstance->get_open_level());
	open_lv.append(s_open_level);
	const char *str_ji = StringDataManager::getString("fivePerson_ji");
	open_lv.append(str_ji);
	l_open_lv->setText(open_lv.c_str());
	//消耗：体力5
	std::string consume = "";
	const char *str_tili = StringDataManager::getString("fivePerson_tili");
	consume.append(str_tili);
	char s_consume[10];
	sprintf(s_consume,"%d",fivePersonInstance->get_consume());
	consume.append(s_consume);
	l_consume->setText(consume.c_str());
	//开放时间：0:0:0-23:59:59
	std::string openTime = "";
	openTime.append(fivePersonInstance->get_start_time().substr(0,5));
	openTime.append("-");
	openTime.append(fivePersonInstance->get_end_time().substr(0,5));
	l_open_time->setText(openTime.c_str());
	//副本难度：普通/困难
	std::string str_difficult = "";
	if (fivePersonInstance->get_difficult() == 1)
	{
		str_difficult.append(StringDataManager::getString("fivePerson_difficult_normal"));
	}
	else
	{
		str_difficult.append(StringDataManager::getString("fivePerson_difficult_hard"));
	}

	l_difficult->setText(str_difficult.c_str());
	//建议人数：建议2人
	std::string str_suggest = "";
	//const char *str_jianyi = StringDataManager::getString("fivePerson_jianyi");
	//str_suggest.append(str_jianyi);
	char s_suggest_num[10];
	sprintf(s_suggest_num,"%d",fivePersonInstance->get_suggest_num());
	str_suggest.append(s_suggest_num);
	const char *str_ren = StringDataManager::getString("fivePerson_ren");
	str_suggest.append(str_ren);
	l_suggest->setText(str_suggest.c_str());

	char s_combat_value[10];
	sprintf(s_combat_value,"%d",fivePersonInstance->get_fight_point());
	l_combat->setText(s_combat_value);


	int scroll_height = 0;
	//副本介绍
	int zeroLineHeight = l_des_head->getContentSize().height;

	std::string str_des;
	str_des.append(fivePersonInstance->get_description().c_str());
	l_des->setString(str_des.c_str());
	int firstLineHeight = zeroLineHeight+l_des->getContentSize().height;

	// 	//副本说明
	int secondLineHeight = firstLineHeight+l_info_head->getContentSize().height;

	//副本说明描述
	std::string str_info ;
	std::map<int,std::string>::const_iterator cIter;
	cIter = SystemInfoConfigData::s_systemInfoList.find(4);
	if (cIter == SystemInfoConfigData::s_systemInfoList.end()) // 没找到就是指向END了  
	{
		return ;
	}
	else
	{
		str_info.append(SystemInfoConfigData::s_systemInfoList[4].c_str());
	}
	l_info->setString(str_info.c_str());
	int thirdLineHeight = secondLineHeight + l_info->getContentSize().height;
	scroll_height = thirdLineHeight+30;

	if (scroll_height > 294)
	{
		m_scrollView_des->setContentSize(CCSizeMake(325,scroll_height));
		m_scrollView_des->setContentOffset(ccp(0,294-scroll_height));  
	}
	else
	{
		m_scrollView_des->setContentSize(ccp(325,294));
	}
	m_scrollView_des->setClippingToBounds(true);
	l_des_head->setPosition(ccp(36,m_scrollView_des->getContentSize().height - zeroLineHeight-10));
	l_des->setPosition(ccp(36,m_scrollView_des->getContentSize().height - firstLineHeight-10));
	l_info_head->setPosition(ccp(36,m_scrollView_des->getContentSize().height - secondLineHeight-20));
	l_info->setPosition(ccp(36,m_scrollView_des->getContentSize().height - thirdLineHeight-20));

}

void InstanceDetailUI::RefreshRewardsInfo()
{
	for (int i = 0;i<8;i++)
	{
		if (layer->getWidgetByTag(kTag_RewardItem_Base+i))
		{
			layer->getWidgetByTag(kTag_RewardItem_Base+i)->removeFromParent();
		}
	}

	for (int i = 0;i<FivePersonInstance::getInstance()->s_m_rewardGoods.size();i++)
	{
		if (i >= 8)
			break;

		PackageItem * packageItem = PackageItem::create(FivePersonInstance::getInstance()->s_m_rewardGoods.at(i));
		packageItem->setAnchorPoint(ccp(0,0));
		packageItem->setTag(kTag_RewardItem_Base+i);
		//packageItem->setPosition(ccp(CoordinateVector.at(idx%m_nElementNumEveryPage).x, CoordinateVector.at(idx%m_nElementNumEveryPage).y));
		layer->addWidget(packageItem);
		packageItem->setBoundVisible(true);

		if (i < 4)
		{
			packageItem->setPosition(ccp(57+86*i,162));
		}
		else
		{
			packageItem->setPosition(ccp(57+86*(i%4),85));
		}
	}
}

void InstanceDetailUI::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void InstanceDetailUI::addCCTutorialIndicator( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,60,55);
// 	tutorialIndicator->setPosition(ccp(pos.x-45,pos.y+95));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	layer->addChild(tutorialIndicator);
	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,110,49,true);
	tutorialIndicator->setDrawNodePos(ccp(pos.x+21+_w,pos.y+17+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}


void InstanceDetailUI::removeCCTutorialIndicator()
{
	CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(layer->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		CCTeachingGuide* teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}

bool InstanceDetailUI::isFinishAction()
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	if (this->getPosition().x == winSize.width/2)
		return true;

	return false;
}

