#ifndef _UI_INSTANCE_INSTANCEEND_H_
#define _UI_INSTANCE_INSTANCEEND_H_

#include "../extensions/UIScene.h"
#include "../backpackscene/PacPageView.h"

#define  RewardCardItemBaseTag 720

class CFivePersonInstanceEndInfo;
class CRewardProp;

class InstanceEndUI : public UIScene
{
public:
	InstanceEndUI();
	~InstanceEndUI();

	struct Coordinate
	{
		float x;
		float y;
	};

	static InstanceEndUI *create(CFivePersonInstanceEndInfo * fivePersonInstanceEndInfo);
	bool init(CFivePersonInstanceEndInfo * fivePersonInstanceEndInfo);
	virtual void onEnter();
	virtual void onExit();

	void callBackExit(CCObject * pSender);

	void exitInstanceCallback(CCObject* pSender);

	void update(float dt);

	void setPayForCardValue();
	void setRemainNumValue(int _value);
	int getRemainNumValue();

	void setAllNumValue(int _value);
	int getAllNumValue();

	//开始展示牌
	void showAllRewarsCard();
	//把所有牌都翻到背面
	void AllCardTurnToBack();
	//开始洗牌
	void BeginShuffle();

	void createAnimation();
	void createUI();

	UILayer * getULayer();

	void ArmatureFinishCallback(cocos2d::extension::CCArmature *armature, cocos2d::extension::MovementEventType movementType, const char *movementID);

protected:
	void showUIAnimation();
	void startSlowMotion();

	void addLightAnimation();
public:
	void endSlowMotion();
	
	// star end action callBack (++ by LiuLiang)
	void starActionCallBack();

private:
	std::vector<CRewardProp*>curRewardPropList;
	std::vector<Coordinate> Coordinate_card;

	UIPanel *panel_1;
	UIPanel *panel_2;
	UILayer * layer_1;
	UILayer * layer_2;

	UILayer * layer_base;

	UIImageView * imageView_Win;

	int remainTime;
	UILabel * l_remainTime_panel1;
	UILabel * l_remainTime_panel2;
	long long countDown_startTime;

	//free num
	UILabel * l_allFreeNum;
	int m_nAllNum;
	//remain num
	UILabel * l_remainNum;
	int m_nRemainNum;

	UILabel * l_payForCardValue;
	UILabel * l_IngotValue;

	CFivePersonInstanceEndInfo * curFivePersonInstanceEndInfo;

	UILayer * u_layer;

	UIButton * btn_close;
	//显示关闭按钮
	void PresentBtnClose();
	//开始教学
	void BeginToturial();

public:
	void addToturial();
	void removeToturial();
};

#endif