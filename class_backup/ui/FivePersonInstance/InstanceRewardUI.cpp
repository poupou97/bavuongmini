#include "InstanceRewardUI.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../backpackscene/PacPageView.h"
#include "../backpackscene/PackageItem.h"
#include "FivePersonInstance.h"


InstanceRewardUI::InstanceRewardUI():
m_nElementNumEveryPage(16),
m_nPageNum(0),
m_nSpace(28),
m_nMaxPageNum(12)
{
	//coordinate for packageitem
	for (int m = 3;m>=0;m--)
	{
		for (int n = 3;n>=0;n--)
		{
			int _m = 3-m;
			int _n = 3-n;

			Coordinate temp ;
			temp.x = 5+_n*86;
			temp.y = 0 +m*77;

			CoordinateVector.push_back(temp);
		}
	}
	//packageItem name
	for (int i=0 ;i<16; ++i)
	{
		std::string pacBaseName = "rewardGood_";
		char num[4];
		int tmpID = i;
		if(tmpID < 10)
			sprintf(num, "0%d", tmpID);
		else
			sprintf(num, "%d", tmpID);
		std::string number = num;
		pacBaseName.append(num);
		rewardGoodItem_name[i] = pacBaseName;
	}
}


InstanceRewardUI::~InstanceRewardUI()
{
// 	std::vector<GoodsInfo*>::iterator iter;
// 	for (iter = rewardGoods.begin(); iter != rewardGoods.end(); ++iter)
// 	{
// 		delete *iter;
// 	}
// 	rewardGoods.clear();

	CoordinateVector.clear();
}

void InstanceRewardUI::onEnter()
{
	UIScene::onEnter();
}

void InstanceRewardUI::onExit()
{
	UIScene::onExit();
}

bool InstanceRewardUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	//return resignFirstResponder(touch,this,false);
	return true;
}

void InstanceRewardUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void InstanceRewardUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void InstanceRewardUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

InstanceRewardUI * InstanceRewardUI::create()
{
	InstanceRewardUI * instanceRewardUI = new InstanceRewardUI();
	if (instanceRewardUI && instanceRewardUI->init())
	{
		instanceRewardUI->autorelease();
		return instanceRewardUI;
	}
	CC_SAFE_DELETE(instanceRewardUI);
	return NULL;
}

bool InstanceRewardUI::init()
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();

		this->scheduleUpdate();

		u_layer = UILayer::create();
		addChild(u_layer);

		//加载UI
		UIPanel *ppanel = (UIPanel*)GUIReader::shareReader()->widgetFromJsonFile("res_ui/instance_reward_1.json");
		ppanel->setAnchorPoint(ccp(0.f,0.f));
		ppanel->getValidNode()->setContentSize(CCSizeMake(410, 450));
		ppanel->setPosition(CCPointZero);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnable(true);
		m_pUiLayer->addWidget(ppanel);

		//关闭按钮
		UIButton * Button_close = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnable(true);
		Button_close->setPressedActionEnabled(true);
		Button_close->addReleaseEvent(this, coco_releaseselector(InstanceRewardUI::CloseEvent));

		ReloadData();

		this->setTouchEnabled(true);
		this->setContentSize(CCSizeMake(450,450));
		this->setTouchMode(kCCTouchesOneByOne);

		return true;
	}
	return false;
}


void InstanceRewardUI::ReloadData()
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	// Create the page view
	m_pageView = UIPageView::create();
	m_pageView->setTouchEnable(true);
	m_pageView->setSize(CCSizeMake(350, 313));
	m_pageView->setPosition(ccp(50,62));
	m_pageView->setName("pageView");
	m_pageView->setWidgetTag(kTagPageView);
// 
// 	if (this->rewardGoods.size()%m_nElementNumEveryPage == 0)
// 	{
// 		m_nPageNum = this->rewardGoods.size()/m_nElementNumEveryPage;
// 	}
// 	else
// 	{
// 		m_nPageNum = this->rewardGoods.size()/m_nElementNumEveryPage + 1;
// 	}
// 	
// 	if (m_nPageNum > m_nMaxPageNum)
// 		m_nPageNum = m_nMaxPageNum;
// 
// 	int idx = 0;
// 	for (int i = 0; i < m_nPageNum; ++i)
// 	{
// 		UIPanel* panel = UIPanel::create();
// 		panel->setSize(CCSizeMake(350, 313));
// 		panel->setTouchEnable(true);
// 		if (this->rewardGoods.size()<=m_nElementNumEveryPage*(i+1))
// 		{
// 			if (m_nElementNumEveryPage*i < this->rewardGoods.size())
// 			{
// 				for (int m = m_nElementNumEveryPage*i;m<this->rewardGoods.size();m++)
// 				{
// 					PackageItem * packageItem = PackageItem::create(this->rewardGoods.at(m));
// 					packageItem->setAnchorPoint(ccp(0,0));
// 					packageItem->setPosition(ccp(CoordinateVector.at(idx%m_nElementNumEveryPage).x, CoordinateVector.at(idx%m_nElementNumEveryPage).y));
// 					packageItem->setName(rewardGoodItem_name[idx%m_nElementNumEveryPage].c_str());
// 					panel->addChild(packageItem);
// 					packageItem->setBoundVisible(true);
// 					idx++;
// 				}
// 			}
// 			else
// 			{
// 			}
// 		}
// 		else
// 		{
// 			for (int m = m_nElementNumEveryPage*i;m<m_nElementNumEveryPage*(i+1);m++)
// 			{
// 				PackageItem * packageItem = PackageItem::create(this->rewardGoods.at(m));
// 				packageItem->setAnchorPoint(ccp(0,0));
// 				packageItem->setPosition(ccp(CoordinateVector.at(idx%m_nElementNumEveryPage).x, CoordinateVector.at(idx%m_nElementNumEveryPage).y));
// 				packageItem->setName(rewardGoodItem_name[idx%m_nElementNumEveryPage].c_str());
// 				panel->addChild(packageItem);
// 				packageItem->setBoundVisible(true);
// 				idx++;
// 			}
// 		}
// 		m_pageView->addPage(panel);
// 
// 	}
	m_pageView->addPageTurningEvent(this,coco_PageView_PageTurning_selector(InstanceRewardUI::CurrentPageViewChanged));
	u_layer->addWidget(m_pageView);

	ReloadDataByIndex(0);

	first_point_x = 0.f;
	if (m_nPageNum %2 == 0)
	{
		first_point_x = 225-m_nSpace*((m_nPageNum-1)*1.0f/2.0f);
	}
	else
	{
		first_point_x = 225-m_nSpace*(m_nPageNum/2);
	}
	for (int i = 0;i<m_nPageNum;++i)
	{
		UIImageView *imageView_point_off = UIImageView::create();
		imageView_point_off->setTexture("res_ui/dian_off.png");
		imageView_point_off->setAnchorPoint(ccp(.5f,.5f));
		imageView_point_off->setPosition(ccp(first_point_x+i*m_nSpace,40));
		u_layer->addWidget(imageView_point_off);
	}

	UIImageView * imageView_select = UIImageView::create();
	imageView_select->setTexture("res_ui/dian_on.png");
	imageView_select->setAnchorPoint(ccp(.5f,.5f));
	imageView_select->setPosition(ccp(first_point_x+1,39));
	imageView_select->setName("imageView_select");
	u_layer->addWidget(imageView_select);

// 	UIImageView * imageView_frame = UIImageView::create();
// 	imageView_frame->setTexture("res_ui/LV5_dikuang_miaobian1.png");
// 	imageView_frame->setScale9Enabled(true);
// 	imageView_frame->setScale9Size(CCSizeMake(363,323));
// 	imageView_frame->setCapInsets(CCRectMake(32,32,1,1));
// 	imageView_frame->setAnchorPoint(ccp(0.f,0.f));
// 	imageView_frame->setPosition(ccp(44,53));
// 	imageView_frame->setName("imageView_select");
// 	u_layer->addWidget(imageView_frame);
}

void InstanceRewardUI::CloseEvent( CCObject *pSender )
{
	this->closeAnim();
}

void InstanceRewardUI::CurrentPageViewChanged( CCObject* pSender )
{
	if (u_layer->getWidgetByName("imageView_select"))
	{
		u_layer->getWidgetByName("imageView_select")->setPosition(ccp(first_point_x+m_pageView->getCurPageIndex()*m_nSpace+1,39));
	}

	ReloadDataByIndex(m_pageView->getCurPageIndex());
}

void InstanceRewardUI::ReloadDataByIndex( int index )
{
	if (FivePersonInstance::getInstance()->s_m_rewardGoods.size()%m_nElementNumEveryPage == 0)
	{
		m_nPageNum = FivePersonInstance::getInstance()->s_m_rewardGoods.size()/m_nElementNumEveryPage;
	}
	else
	{
		m_nPageNum = FivePersonInstance::getInstance()->s_m_rewardGoods.size()/m_nElementNumEveryPage + 1;
	}

	if (m_nPageNum > m_nMaxPageNum)
		m_nPageNum = m_nMaxPageNum;

	if (index >= m_nPageNum)
		return;

	int idx = 0;
	if(!m_pageView->getPage(index))
	{
		UIPanel* panel = UIPanel::create();
		panel->setSize(CCSizeMake(350, 313));
		panel->setTouchEnable(true);
		if (FivePersonInstance::getInstance()->s_m_rewardGoods.size()<=m_nElementNumEveryPage*(index+1))
		{
			if (m_nElementNumEveryPage*index < FivePersonInstance::getInstance()->s_m_rewardGoods.size())
			{
				for (int m = m_nElementNumEveryPage*index;m<FivePersonInstance::getInstance()->s_m_rewardGoods.size();m++)
				{
					PackageItem * packageItem = PackageItem::create(FivePersonInstance::getInstance()->s_m_rewardGoods.at(m));
					packageItem->setAnchorPoint(ccp(0,0));
					packageItem->setPosition(ccp(CoordinateVector.at(idx%m_nElementNumEveryPage).x, CoordinateVector.at(idx%m_nElementNumEveryPage).y));
					packageItem->setName(rewardGoodItem_name[idx%m_nElementNumEveryPage].c_str());
					panel->addChild(packageItem);
					packageItem->setBoundVisible(true);
					idx++;
				}
			}
			else
			{
			}
		}
		else
		{
			for (int m = m_nElementNumEveryPage*index;m<m_nElementNumEveryPage*(index+1);m++)
			{
				PackageItem * packageItem = PackageItem::create(FivePersonInstance::getInstance()->s_m_rewardGoods.at(m));
				packageItem->setAnchorPoint(ccp(0,0));
				packageItem->setPosition(ccp(CoordinateVector.at(idx%m_nElementNumEveryPage).x, CoordinateVector.at(idx%m_nElementNumEveryPage).y));
				packageItem->setName(rewardGoodItem_name[idx%m_nElementNumEveryPage].c_str());
				panel->addChild(packageItem);
				packageItem->setBoundVisible(true);
				idx++;
			}
		}
		m_pageView->insertPage(panel,index);
	}

	if (index == 0)   //第一页
	{
		int t_idx_next = 0;
		int next_index = index + 1;
		if(!m_pageView->getPage(next_index))
		{
			UIPanel* panel = UIPanel::create();
			panel->setSize(CCSizeMake(350, 313));
			panel->setTouchEnable(true);
			if (FivePersonInstance::getInstance()->s_m_rewardGoods.size()<=m_nElementNumEveryPage*(next_index+1))
			{
				if (m_nElementNumEveryPage*next_index < FivePersonInstance::getInstance()->s_m_rewardGoods.size())
				{
					for (int m = m_nElementNumEveryPage*next_index;m<FivePersonInstance::getInstance()->s_m_rewardGoods.size();m++)
					{
						PackageItem * packageItem = PackageItem::create(FivePersonInstance::getInstance()->s_m_rewardGoods.at(m));
						packageItem->setAnchorPoint(ccp(0,0));
						packageItem->setPosition(ccp(CoordinateVector.at(t_idx_next%m_nElementNumEveryPage).x, CoordinateVector.at(t_idx_next%m_nElementNumEveryPage).y));
						packageItem->setName(rewardGoodItem_name[t_idx_next%m_nElementNumEveryPage].c_str());
						panel->addChild(packageItem);
						packageItem->setBoundVisible(true);
						t_idx_next++;
					}
				}
				else
				{
				}
			}
			else
			{
				for (int m = m_nElementNumEveryPage*next_index;m<m_nElementNumEveryPage*(next_index+1);m++)
				{
					PackageItem * packageItem = PackageItem::create(FivePersonInstance::getInstance()->s_m_rewardGoods.at(m));
					packageItem->setAnchorPoint(ccp(0,0));
					packageItem->setPosition(ccp(CoordinateVector.at(t_idx_next%m_nElementNumEveryPage).x, CoordinateVector.at(t_idx_next%m_nElementNumEveryPage).y));
					packageItem->setName(rewardGoodItem_name[t_idx_next%m_nElementNumEveryPage].c_str());
					panel->addChild(packageItem);
					packageItem->setBoundVisible(true);
					t_idx_next++;
				}
			}
			m_pageView->insertPage(panel,next_index);
		}
	}
	else if(index == m_nPageNum-1) //最后一页
	{
		int t_idx_last = 0;
		int last_index = index - 1;
		if(!m_pageView->getPage(last_index))
		{
			UIPanel* panel = UIPanel::create();
			panel->setSize(CCSizeMake(350, 313));
			panel->setTouchEnable(true);
			if (FivePersonInstance::getInstance()->s_m_rewardGoods.size()<=m_nElementNumEveryPage*(last_index+1))
			{
				if (m_nElementNumEveryPage*last_index < FivePersonInstance::getInstance()->s_m_rewardGoods.size())
				{
					for (int m = m_nElementNumEveryPage*last_index;m<FivePersonInstance::getInstance()->s_m_rewardGoods.size();m++)
					{
						PackageItem * packageItem = PackageItem::create(FivePersonInstance::getInstance()->s_m_rewardGoods.at(m));
						packageItem->setAnchorPoint(ccp(0,0));
						packageItem->setPosition(ccp(CoordinateVector.at(t_idx_last%m_nElementNumEveryPage).x, CoordinateVector.at(t_idx_last%m_nElementNumEveryPage).y));
						packageItem->setName(rewardGoodItem_name[t_idx_last%m_nElementNumEveryPage].c_str());
						panel->addChild(packageItem);
						packageItem->setBoundVisible(true);
						t_idx_last++;
					}
				}
				else
				{
				}
			}
			else
			{
				for (int m = m_nElementNumEveryPage*last_index;m<m_nElementNumEveryPage*(last_index+1);m++)
				{
					PackageItem * packageItem = PackageItem::create(FivePersonInstance::getInstance()->s_m_rewardGoods.at(m));
					packageItem->setAnchorPoint(ccp(0,0));
					packageItem->setPosition(ccp(CoordinateVector.at(t_idx_last%m_nElementNumEveryPage).x, CoordinateVector.at(t_idx_last%m_nElementNumEveryPage).y));
					packageItem->setName(rewardGoodItem_name[t_idx_last%m_nElementNumEveryPage].c_str());
					panel->addChild(packageItem);
					packageItem->setBoundVisible(true);
					t_idx_last++;
				}
			}
			m_pageView->insertPage(panel,last_index);
		}
	}
	else  //中间的某一页
	{
		//加载前一页
		int t_idx_last = 0;
		int last_index = index - 1;
		if(!m_pageView->getPage(last_index))
		{
			UIPanel* panel = UIPanel::create();
			panel->setSize(CCSizeMake(350, 313));
			panel->setTouchEnable(true);
			if (FivePersonInstance::getInstance()->s_m_rewardGoods.size()<=m_nElementNumEveryPage*(last_index+1))
			{
				if (m_nElementNumEveryPage*last_index < FivePersonInstance::getInstance()->s_m_rewardGoods.size())
				{
					for (int m = m_nElementNumEveryPage*last_index;m<FivePersonInstance::getInstance()->s_m_rewardGoods.size();m++)
					{
						PackageItem * packageItem = PackageItem::create(FivePersonInstance::getInstance()->s_m_rewardGoods.at(m));
						packageItem->setAnchorPoint(ccp(0,0));
						packageItem->setPosition(ccp(CoordinateVector.at(t_idx_last%m_nElementNumEveryPage).x, CoordinateVector.at(t_idx_last%m_nElementNumEveryPage).y));
						packageItem->setName(rewardGoodItem_name[t_idx_last%m_nElementNumEveryPage].c_str());
						panel->addChild(packageItem);
						packageItem->setBoundVisible(true);
						t_idx_last++;
					}
				}
				else
				{
				}
			}
			else
			{
				for (int m = m_nElementNumEveryPage*last_index;m<m_nElementNumEveryPage*(last_index+1);m++)
				{
					PackageItem * packageItem = PackageItem::create(FivePersonInstance::getInstance()->s_m_rewardGoods.at(m));
					packageItem->setAnchorPoint(ccp(0,0));
					packageItem->setPosition(ccp(CoordinateVector.at(t_idx_last%m_nElementNumEveryPage).x, CoordinateVector.at(t_idx_last%m_nElementNumEveryPage).y));
					packageItem->setName(rewardGoodItem_name[t_idx_last%m_nElementNumEveryPage].c_str());
					panel->addChild(packageItem);
					packageItem->setBoundVisible(true);
					t_idx_last++;
				}
			}
			m_pageView->insertPage(panel,last_index);
		}
		
		//加载后一页
		int t_idx_next = 0;
		int next_index = index + 1;
		if(!m_pageView->getPage(next_index))
		{
			UIPanel* panel = UIPanel::create();
			panel->setSize(CCSizeMake(350, 313));
			panel->setTouchEnable(true);
			if (FivePersonInstance::getInstance()->s_m_rewardGoods.size()<=m_nElementNumEveryPage*(next_index+1))
			{
				if (m_nElementNumEveryPage*next_index < FivePersonInstance::getInstance()->s_m_rewardGoods.size())
				{
					for (int m = m_nElementNumEveryPage*next_index;m<FivePersonInstance::getInstance()->s_m_rewardGoods.size();m++)
					{
						PackageItem * packageItem = PackageItem::create(FivePersonInstance::getInstance()->s_m_rewardGoods.at(m));
						packageItem->setAnchorPoint(ccp(0,0));
						packageItem->setPosition(ccp(CoordinateVector.at(t_idx_next%m_nElementNumEveryPage).x, CoordinateVector.at(t_idx_next%m_nElementNumEveryPage).y));
						packageItem->setName(rewardGoodItem_name[t_idx_next%m_nElementNumEveryPage].c_str());
						panel->addChild(packageItem);
						packageItem->setBoundVisible(true);
						t_idx_next++;
					}
				}
				else
				{
				}
			}
			else
			{
				for (int m = m_nElementNumEveryPage*next_index;m<m_nElementNumEveryPage*(next_index+1);m++)
				{
					PackageItem * packageItem = PackageItem::create(FivePersonInstance::getInstance()->s_m_rewardGoods.at(m));
					packageItem->setAnchorPoint(ccp(0,0));
					packageItem->setPosition(ccp(CoordinateVector.at(t_idx_next%m_nElementNumEveryPage).x, CoordinateVector.at(t_idx_next%m_nElementNumEveryPage).y));
					packageItem->setName(rewardGoodItem_name[t_idx_next%m_nElementNumEveryPage].c_str());
					panel->addChild(packageItem);
					packageItem->setBoundVisible(true);
					t_idx_next++;
				}
			}
			m_pageView->insertPage(panel,next_index);
		}
	}
}
