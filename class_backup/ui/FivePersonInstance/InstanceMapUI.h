#ifndef _UI_INSTANCE_INSTANCEMAPUI_H_
#define _UI_INSTANCE_INSTANCEMAPUI_H_

#include "../extensions/UIScene.h"
#include "../../legend_script/CCTutorialIndicator.h"

class UITab;
class CFivePersonInstance;

class InstanceMapUI : public UIScene
{
public:
	InstanceMapUI();
	~InstanceMapUI();

	static InstanceMapUI *create();
	bool init();
	virtual void onEnter();
	virtual void onExit();

	void CloseEvent(CCObject * pSender);

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	void BtnInfoEvent(CCObject * pSender);
	void OpenDetailUI(int instanceId);
	void AddPhyEvent(CCObject * pSender);

	void RefreshInstanceInfo(CFivePersonInstance * fivePersonInstance);
	//
	void RefreshInstanceStar();
	//present selected instanceIcon in center As much as possible
	void ScrollInstanceToCenter(int instanceId);
	void callBackScroll();

protected:
	UILayer * layer;
	UIDragPanel * p_MyDragPanel;
	UIImageView *ImageView_select;

	UIImageView *m_pImageView_phy;
	UILabelBMFont * m_pLabelBMF_phyValue;
	UIButton * btn_addPhy;

	int curInstanceId;
	CCPoint curInstancePos;

	UIPanel *panel_tutorial;

public:
	//教学
	int mTutorialScriptInstanceId;
	virtual void registerScriptCommand(int scriptId);
	//选第一个副本
	void addCCTutorialIndicator(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction  );
	void removeCCTutorialIndicator();

	UIButton * btn_firstInstance;
	UIButton * btn_secondInstance;
	UIButton * btn_thirdInstance;
	UIButton * btn_fouthInstance;
};

#endif