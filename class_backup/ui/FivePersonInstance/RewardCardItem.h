
#ifndef _FIVEPERSONINSTANCE_REWARDCARDITEM_H_
#define _FIVEPERSONINSTANCE_REWARDCARDITEM_H_

#include "../extensions/UIScene.h"

class CRewardProp;

class RewardCardItem : public UIScene
{
public:
	RewardCardItem();
	~RewardCardItem();

	static RewardCardItem * create(CRewardProp *rewardProp , int timeDouble);
	bool init(CRewardProp *rewardProp , int timeDouble);

	void BackCradEvent(CCObject * pSender);
	void FrontCradEvent(CCObject * pSender);

	void FlopAnimationToFront();
	void FlopAnimationToBack();

	void setBtnTouchEnabled();
	void presentDesLable();

	void RefreshCardInfo(CRewardProp *rewardProp,std::string roleName,long long roleId);
	void RefreshCostValue(std::string costValue,std::string iconPath);

private:
	UIButton * btn_backCardFrame;
	UILabel * l_des;
	UILabel * l_costValue;
	UIImageView * image_unit;
	UIButton * btn_frontCardFrame;
	UIImageView * imageView_frame;
	UIImageView * imageView_icon;
	UILabel * l_rewardName;
	UILabel * l_roleName;

	//格子位置
	int grid;
	//当前显示的正面(true)还是背面(false)
	bool isFrontOrBack;
	//是否已经翻开
	bool isFloped;
};

#endif