#include "CCMoveableMenu.h"


CCMoveableMenu::CCMoveableMenu(void)
{
	moved_=false;
}


CCMoveableMenu::~CCMoveableMenu(void)
{
}

CCMoveableMenu * CCMoveableMenu::create()
{
	CCMoveableMenu * moveMenu=new CCMoveableMenu();
	moveMenu->init();
	moveMenu->autorelease();
	return moveMenu;
}

CCMoveableMenu* CCMoveableMenu::create( CCMenuItem* item, ... )
{
	va_list args;
	va_start(args,item);

	CCMoveableMenu * pRet= new CCMoveableMenu();

	CCArray* pArray = NULL;
	if( item )
	{
		pArray = CCArray::create(item, NULL);
		CCMenuItem *i = va_arg(args, CCMenuItem*);
		while(i)
		{
			pArray->addObject(i);
			i = va_arg(args, CCMenuItem*);
		}
	}

	if(pRet)
	{
		pRet->initWithArray(pArray);
		pRet->autorelease();
	}
	return pRet;
}


bool CCMoveableMenu::init()
{
	if (CCMenu::init())
	{
		return true;
	}
	return false;
}

void CCMoveableMenu::registerWithTouchDispatcher()
{
	CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this,0,false);
}
bool CCMoveableMenu::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
// 	if(visibleRect_.size.width&&visibleRect_.size.height)
// 	{
// 		if(!visibleRect_.containsPoint(pTouch->getLocation()))
// 			return false;
// 
// 	}
	moved_=false;
	mBeginPoint = pTouch->getLocation();
	return CCMenu::ccTouchBegan(pTouch,pEvent);
}

void CCMoveableMenu::ccTouchMoved( CCTouch *pTouch, CCEvent *pEvent )
{
	// must move enough distance
	if(ccpDistance(mBeginPoint, pTouch->getLocation()) > 10.0f)
		moved_=true;
	CCMenu::ccTouchMoved(pTouch,pEvent);
}

void CCMoveableMenu::ccTouchEnded( CCTouch *pTouch, CCEvent *pEvent )
{
	if(!moved_)
	{
		CCMenu::ccTouchEnded(pTouch,pEvent);
	}
	else
	{
		m_eState = kCCMenuStateWaiting;
		if (m_pSelectedItem)
		{
			m_pSelectedItem->unselected();
		}

	}
}
