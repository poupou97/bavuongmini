#include "GetSkillTeach.h"
#include "../../utils/StaticDataManager.h"
#include "../../GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/element/CBaseSkill.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "../../gamescene_state/sceneelement/HeadMenu.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutUIConstant.h"


GetSkillTeach::GetSkillTeach(void)
{
}


GetSkillTeach::~GetSkillTeach(void)
{
}

GetSkillTeach * GetSkillTeach::create(std::string skillId,std::string skillName,std::string skillIcon)
{
	GetSkillTeach * skill_ =new GetSkillTeach();
	if (skill_ && skill_->init(skillId,skillName,skillIcon))
	{
		skill_->autorelease();
		return skill_;
	}
	CC_SAFE_DELETE(skill_);
	return NULL;
}

bool GetSkillTeach::init(std::string skillId,std::string skillName,std::string skillIcon)
{
	if ( UIScene::init())
	{
		m_skillId = skillId;
		m_skillName = skillName;
		m_skillIcon = skillIcon;
		
		winSize = CCDirector::sharedDirector()->getVisibleSize();

		mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winSize);
		mengban->setAnchorPoint(ccp(0.5f,0.5f));
		mengban->setPosition(ccp(winSize.width/2,winSize.height/2));
		m_pUiLayer->addWidget(mengban);

		u_layer=UILayer::create();
		u_layer->ignoreAnchorPointForPosition(false);
		u_layer->setAnchorPoint(ccp(0.5f,0.5f));
		u_layer->setPosition(ccp(winSize.width/2,winSize.height/2));
		u_layer->setContentSize(CCSizeMake(800,480));
		addChild(u_layer);

		CCArmatureDataManager::sharedArmatureDataManager()->addArmatureFileInfo("res_ui/uiflash/get_jineng/get_jineng0.png","res_ui/uiflash/get_jineng/get_jineng0.plist","res_ui/uiflash/get_jineng/get_jineng.ExportJson");
		CCArmatureDataManager::sharedArmatureDataManager()->addArmatureFileInfo("res_ui/uiflash/get_jineng/get_jineng1.png","res_ui/uiflash/get_jineng/get_jineng1.plist","res_ui/uiflash/get_jineng/get_jineng.ExportJson");
		CCArmature * armature = CCArmature::create("get_jineng");
		armature->getAnimation()->play("Animation1");
		armature->getAnimation()->setMovementEventCallFunc(this,movementEvent_selector(GetSkillTeach::ArmatureFinishCallback));  
		armature->setPosition(ccp(400,240));
		u_layer->addChild(armature);

		std::string iconPath = "";
		iconPath.append(m_skillIcon);
		iconPath.append(".png");
		
		CCSpriteDisplayData displayData_icon;
		displayData_icon.setParam(iconPath.c_str());
		CCBone * bone_skillIcon = (CCBone *)armature->getBone("skill_icon");
		bone_skillIcon->addDisplay(&displayData_icon,1);
		bone_skillIcon->changeDisplayByIndex(1,true);
		
		std::string namePath = "";
		namePath.append(m_skillIcon);
		namePath.append("_name.png");

		CCSpriteDisplayData displayData_name;
		displayData_name.setParam(namePath.c_str());
		CCBone * bone_skillName = (CCBone *)armature->getBone("skill_name");
		bone_skillName->addDisplay(&displayData_name,1);
		bone_skillName->changeDisplayByIndex(1,true);

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(winSize);
		return true;
	}
	return false;
}

void GetSkillTeach::onEnter()
{
	UIScene::onEnter();
}

void GetSkillTeach::onExit()
{
	UIScene::onExit();
}

void GetSkillTeach::callBackStudySkill( CCObject * obj )
{
	this->closeAnim();
	int skillUpLevel = 1;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1126,(void *)skillUpLevel,(void *)m_skillId.c_str());
}

void GetSkillTeach::ArmatureFinishCallback( cocos2d::extension::CCArmature *armature, cocos2d::extension::MovementEventType movementType, const char *movementID )
{
	std::string id = movementID;
	if (id.compare("Animation1") == 0)
	{
		armature->stopAllActions();
		armature->getAnimation()->play("Animation1_end");

		UIButton * Btn_study = UIButton::create();
		Btn_study->setTextures("res_ui/creating_a_role/button_di2.png", "res_ui/creating_a_role/button_di2.png","");
		Btn_study->setTouchEnable(true);
		Btn_study->setPressedActionEnabled(true);
		Btn_study->setAnchorPoint(ccp(0.5f,0.5f));
		Btn_study->setPosition(ccp(400,75));
		Btn_study->addReleaseEvent(this,coco_releaseselector(GetSkillTeach::callBackSkillAction));
		u_layer->addWidget(Btn_study);

		const char * str = StringDataManager::getString("popupWindow_sure");
		UILabel * label_ = UILabel::create();
		label_->setText(str);
		label_->setAnchorPoint(ccp(0.5f,0.5f));
		label_->setPosition(ccp(0,0));
		label_->setFontSize(20);
		Btn_study->addChild(label_);
	}
}

void GetSkillTeach::callBackSkillAction( CCObject * obj )
{
	CCSize winSize_ = CCDirector::sharedDirector()->getVisibleSize();

	mengban->runAction(CCFadeOut::create(0.8f));
	
	
	std::string skillIconPath = "res_ui/jineng_icon/";
	skillIconPath.append(m_skillIcon);
	skillIconPath.append(".png");

	CCSprite* pSpriteIcon = CCSprite::create(skillIconPath.c_str());
	pSpriteIcon->setPosition(ccp(winSize_.width/2,winSize_.height/2+pSpriteIcon->getContentSize().height*1.2f));
	pSpriteIcon->setAnchorPoint(ccp(0.5f, 0.5f));
	pSpriteIcon->setScale(1.3f);
	GameView::getInstance()->getMainUIScene()->addChild(pSpriteIcon);
	
	MainScene * mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
	ShortcutLayer * shortLayey_ = (ShortcutLayer *)mainscene_->getChildByTag(kTagShortcutLayer);
	if (shortLayey_ != NULL)
	{
		CCPoint shortPoint = shortLayey_->getChildByTag(shortLayey_->shortcurTag[1]+100)->getPosition();
		int offx = shortPoint.x;
		int offy = shortPoint.y ;
		if (mainscene_->isHeadMenuOn)
		{
			offy = offy + mainscene_->headMenu->getContentSize().height + 11;
		}

		CCActionInterval * action =(CCActionInterval *)CCSequence::create(
			CCDelayTime::create(0.5f),
			CCSpawn::create(
			CCMoveTo::create(0.8f,ccp(offx ,offy)),
			CCRotateBy::create(0.8f,360),
			CCScaleTo::create(0.8f,1.1f),
			NULL),
			CCCallFuncO::create(this,callfuncO_selector(GetSkillTeach::callBackStudySkill),NULL),
			CCRemoveSelf::create(),
			NULL);
		pSpriteIcon->runAction(action);
	}
}
