#ifndef _MENUNODE_CCMOVEABLEMENU_H_
#define _MENUNODE_CCMOVEABLEMENU_H_

#include "cocos2d.h"
USING_NS_CC;

/*
* when you want put a CCMenu into the CCScrollView or CCTabelView,
* please use this CCMoveableMenu instead of CCMenu
*/
class CCMoveableMenu : public CCMenu
{
public:
	CCMoveableMenu(void);
	~CCMoveableMenu(void);

	static CCMoveableMenu * create();
	static CCMoveableMenu* create(CCMenuItem* item, ...);
	bool init();
	virtual void registerWithTouchDispatcher();
	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);

private:
	bool moved_;
	CCPoint mBeginPoint;   // record the point when ccTouchBegan()
};

#endif;