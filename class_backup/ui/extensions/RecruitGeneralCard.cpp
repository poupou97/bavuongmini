#include "RecruitGeneralCard.h"
#include "../../gamescene_state/MainScene.h"
#include "GameView.h"
#include "../../legend_engine/CCLegendAnimation.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../utils/StaticDataManager.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "../generals_ui/RecuriteActionItem.h"
#include "../../gamescene_state/GameSceneEffects.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/ScriptManager.h"
#include "../../messageclient/element/CActionDetail.h"
#include "../../legend_script/CCTeachingGuide.h"

#define kTag_head_anm 10
RecruitGeneralCard::RecruitGeneralCard(void):
isRunTutorials(false)
{
	m_generalId = 0;
}


RecruitGeneralCard::~RecruitGeneralCard(void)
{
}

RecruitGeneralCard * RecruitGeneralCard::create(int curType,int generalModelId)
{
	RecruitGeneralCard * recruitCard = new RecruitGeneralCard();
	if (recruitCard && recruitCard->init(curType,generalModelId))
	{
		recruitCard->autorelease();
		return recruitCard;
	}
	CC_SAFE_DELETE(recruitCard);
	return NULL;
}

bool RecruitGeneralCard::init(int curType,int generalModelId)
{
	if (UIScene::init())
	{
		winsize =CCDirector::sharedDirector()->getVisibleSize();
		
		m_type = curType;

		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winsize);
		mengban->setAnchorPoint(ccp(0.5f,0.5f));
		mengban->setPosition(ccp(383/2,379/2));
		m_pUiLayer->addWidget(mengban);
		
		u_layer = UILayer::create();
		u_layer->setAnchorPoint(ccp(0.0f,0.0f));
		u_layer->setPosition(ccp(0,0));
		u_layer->setContentSize(CCSizeMake(383,379));
		m_pUiLayer->addChild(u_layer);
		//
		image_light_a = UIImageView::create();
		image_light_a->setTexture("res_ui/lightlighta.png");
		image_light_a->setAnchorPoint(ccp(0.5f,0.5f));
		image_light_a->setPosition(ccp(192,198));
		image_light_a->setScale(0);
		u_layer->addWidget(image_light_a);
		
		image_light_b = UIImageView::create();
		image_light_b->setTexture("res_ui/lightlightb.png");
		image_light_b->setAnchorPoint(ccp(0.5f,0.5f));
		image_light_b->setPosition(ccp(192,198));
		image_light_b->setScale(0);
		u_layer->addWidget(image_light_b);

		imageGeneral_bg = UIImageView::create();
		imageGeneral_bg->setTexture("res_ui/generals_dia.png");
		imageGeneral_bg->setAnchorPoint(ccp(0.5f,0.5f));
		imageGeneral_bg->setPosition(ccp(192,198));
		imageGeneral_bg->setScale(4.0f);
		u_layer->addWidget(imageGeneral_bg);

		image_up_ = UIImageView::create();
		image_up_->setTexture("res_ui/wujiang/nihuode.png");
		image_up_->setAnchorPoint(ccp(0.5f,0.5f));
		image_up_->setPosition(ccp(192,imageGeneral_bg->getPosition().y+imageGeneral_bg->getContentSize().height/2+image_up_->getContentSize().height -10));
		u_layer->addWidget(image_up_);
		image_up_->setVisible(false);

		//generalInfo
		CGeneralBaseMsg * generalBaseInfo;
		switch(curType)
		{
		case kTypeFirstRecurite:
			{
				 for (int i =0;i<GameView::getInstance()->generalBaseMsgList.size();i++)
				 {
					 if (GameView::getInstance()->generalBaseMsgList.at(i)->fightstatus() == 0)
					 {
						 m_generalId = GameView::getInstance()->generalBaseMsgList.at(i)->id();
						 int generalModle_ = GameView::getInstance()->generalBaseMsgList.at(i)->modelid();
						 generalBaseInfo  = GeneralsConfigData::s_generalsBaseMsgData[generalModle_];
						 break;
					 }
				 }
			}
			break;
		case kTypeCommonRecuriteGeneral:
			{
				generalBaseInfo = GeneralsConfigData::s_generalsBaseMsgData[generalModelId];
			}
			break;
		case kTypeUsePropRecuriteGeneral:
			{
				generalBaseInfo = GeneralsConfigData::s_generalsBaseMsgData[generalModelId];
			}
			break;
		}

		CCAssert(generalBaseInfo != NULL, "generalBaseInfo should not be null, please check db");

		std::string generalIconPath_ =  getGeneralIconPath(generalBaseInfo->get_half_photo());
		UIImageView * imageGeneral_Icon = UIImageView::create();
		imageGeneral_Icon->setTexture(generalIconPath_.c_str());
		imageGeneral_Icon->setAnchorPoint(ccp(0.5f,0));
		imageGeneral_Icon->setPosition(ccp(0,63-imageGeneral_bg->getContentSize().height/2));
		imageGeneral_bg->addChild(imageGeneral_Icon);

		std::string generalName_ = generalBaseInfo->name();
		UILabel * Label_generalName=UILabel::create();
		Label_generalName->setText(generalName_.c_str());
		Label_generalName->setAnchorPoint(ccp(0.5f,0.5f));
		Label_generalName->setPosition(ccp(0,-imageGeneral_bg->getContentSize().height/2+Label_generalName->getContentSize().height*3 - 5));
		Label_generalName->setFontSize(20);
		imageGeneral_bg->addChild(Label_generalName);

		//rare
		std::string generalRare_ = RecuriteActionItem::getStarPathByNum(generalBaseInfo->rare());
		imageGeneral_Star = UIImageView::create();
		imageGeneral_Star->setTexture(generalRare_.c_str());
		imageGeneral_Star->setAnchorPoint(ccp(0.5f,0.5f));
		imageGeneral_Star->setPosition(ccp(imageGeneral_Star->getContentSize().width/2 - imageGeneral_bg->getContentSize().width/2+13,
			Label_generalName->getPosition().y+imageGeneral_Star->getContentSize().height));
		imageGeneral_Star->setScale(2.0f);
		imageGeneral_bg->addChild(imageGeneral_Star);
		imageGeneral_Star->setVisible(false);

		//profession
		std::string generalProfess_ = getGeneralProfessionIconPath(generalBaseInfo->get_profession());
		imageGeneral_profession = UIImageView::create();
		imageGeneral_profession->setTexture(generalProfess_.c_str());
		imageGeneral_profession->setAnchorPoint(ccp(0.5f,0.5f));
		imageGeneral_profession->setPosition(ccp(imageGeneral_bg->getContentSize().width/2 - imageGeneral_profession->getContentSize().width/2 - 14,
			imageGeneral_Star->getPosition().y));
		imageGeneral_profession->setScale(2.0f);
		imageGeneral_bg->addChild(imageGeneral_profession);
		imageGeneral_profession->setVisible(false);

		switch(curType)
		{
		case kTypeFirstRecurite:
			{
				image_down_ = UIImageView::create();
				image_down_->setTexture("res_ui/wujiang/daizi.png");
				image_down_->setAnchorPoint(ccp(0.5f,0.5f));
				image_down_->setPosition(ccp(u_layer->getContentSize().width/2,
										imageGeneral_bg->getPosition().y - imageGeneral_bg->getContentSize().height/2 - image_down_->getContentSize().height/2+5));
				u_layer->addWidget(image_down_);
				image_down_->setVisible(false);

				UIButton * button_getGeneral = UIButton::create();
				button_getGeneral->setTextures("res_ui/wujiang/woyuanzhuisuini.png","res_ui/wujiang/woyuanzhuisuini.png","");
				button_getGeneral->setTouchEnable(true);
				button_getGeneral->setPressedActionEnabled(true);
				button_getGeneral->addReleaseEvent(this,coco_releaseselector(RecruitGeneralCard::callBackShowGeneral));
				button_getGeneral->setAnchorPoint(ccp(0.5f,0.5f));
				button_getGeneral->setPosition(ccp(0,0));
				image_down_->addChild(button_getGeneral);
			}break;
		case kTypeCommonRecuriteGeneral:
			{
				button_first = UIButton::create();
				button_first->setTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				button_first->setScale9Enable(true);
				button_first->setScale9Size(CCSizeMake(110,46));
				button_first->setCapInsets(CCRect(18,9,2,23));
				button_first->setTouchEnable(true);
				button_first->setPressedActionEnabled(true);
				button_first->addReleaseEvent(this,coco_releaseselector(RecruitGeneralCard::callbackFirstButton));
				button_first->setAnchorPoint(ccp(0.5f,0.5f));
				button_first->setPosition(ccp(70,27));
				u_layer->addWidget(button_first);
				button_first->setVisible(false);

				const char * firstString_ = StringDataManager::getString("generals_Recurite_button_des");
				label_first = UILabel::create();
				label_first->setText(firstString_);
				label_first->setAnchorPoint(ccp(0.5f,0.5f));
				label_first->setPosition(ccp(0,0));
				label_first->setFontSize(20);
				button_first->addChild(label_first);

				button_second = UIButton::create();
				button_second->setTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				button_second->setScale9Enable(true);
				button_second->setScale9Size(CCSizeMake(110,46));
				button_second->setCapInsets(CCRect(18,9,2,23));
				button_second->setTouchEnable(true);
				button_second->setPressedActionEnabled(true);
				button_second->addReleaseEvent(this,coco_releaseselector(RecruitGeneralCard::callBackSecondButton));
				button_second->setAnchorPoint(ccp(0.5f,0.5f));
				button_second->setPosition(ccp(192,27));
				u_layer->addWidget(button_second);
				button_second->setVisible(false);

				const char * secondString_ = StringDataManager::getString("generals_Recurite_button_close");
				label_second = UILabel::create();
				label_second->setText(secondString_);
				label_second->setAnchorPoint(ccp(0.5f,0.5f));
				label_second->setPosition(ccp(0,0));
				label_second->setFontSize(20);
				button_second->addChild(label_second);

				button_third = UIButton::create();
				button_third->setTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				button_third->setScale9Enable(true);
				button_third->setScale9Size(CCSizeMake(110,46));
				button_third->setCapInsets(CCRect(18,9,2,23));
				button_third->setTouchEnable(true);
				button_third->setPressedActionEnabled(true);
				button_third->addReleaseEvent(this,coco_releaseselector(RecruitGeneralCard::callBackThirdButton));
				button_third->setAnchorPoint(ccp(0.5f,0.5f));
				button_third->setPosition(ccp(314,27));
				u_layer->addWidget(button_third);
				button_third->setVisible(false);

				const char * thirdString_ = StringDataManager::getString("generals_Recurite_button_recuriteAgain");
				label_third = UILabel::create();
				label_third->setText(thirdString_);
				label_third->setAnchorPoint(ccp(0.5f,0.5f));
				label_third->setPosition(ccp(0,0));
				label_third->setFontSize(20);
				button_third->addChild(label_third);

				//cost 
				UIImageView * imageView_goldInGot = UIImageView::create();
				imageView_goldInGot->setTexture("res_ui/ingot.png");
				imageView_goldInGot->setAnchorPoint(ccp(0.5f,0.5f));
				imageView_goldInGot->setPosition(ccp(290,65));
				imageView_goldInGot->setName("imageView_goldInGot");
				u_layer->addWidget(imageView_goldInGot);
				imageView_goldInGot->setVisible(false);
				UILabel* label_goldInGotValue = UILabel::create();
				label_goldInGotValue->setText("900");
				label_goldInGotValue->setAnchorPoint(ccp(0,0.5f));
				label_goldInGotValue->setPosition(ccp(imageView_goldInGot->getContentSize().width/2+10,0));
				label_goldInGotValue->setFontSize(16);
				label_goldInGotValue->setName("label_goldInGotValue");
				imageView_goldInGot->addChild(label_goldInGotValue);
			}
			break;
		case kTypeUsePropRecuriteGeneral:
			{
				button_first = UIButton::create();
				button_first->setTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				button_first->setScale9Enable(true);
				button_first->setScale9Size(CCSizeMake(120,46));
				button_first->setCapInsets(CCRect(18,9,2,23));
				button_first->setTouchEnable(true);
				button_first->setPressedActionEnabled(true);
				button_first->addReleaseEvent(this,coco_releaseselector(RecruitGeneralCard::callbackFirstButton));
				button_first->setAnchorPoint(ccp(0.5f,0.5f));
				button_first->setPosition(ccp(113,27));
				u_layer->addWidget(button_first);
				button_first->setVisible(false);

				const char * firstString_ = StringDataManager::getString("generals_Recurite_button_des");
				label_first = UILabel::create();
				label_first->setText(firstString_);
				label_first->setAnchorPoint(ccp(0.5f,0.5f));
				label_first->setPosition(ccp(0,0));
				label_first->setFontSize(20);
				button_first->addChild(label_first);

				button_second = UIButton::create();
				button_second->setTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				button_second->setScale9Enable(true);
				button_second->setScale9Size(CCSizeMake(120,46));
				button_second->setCapInsets(CCRect(18,9,2,23));
				button_second->setTouchEnable(true);
				button_second->setPressedActionEnabled(true);
				button_second->addReleaseEvent(this,coco_releaseselector(RecruitGeneralCard::callBackSecondButton));
				button_second->setAnchorPoint(ccp(0.5f,0.5f));
				button_second->setPosition(ccp(278,27));
				u_layer->addWidget(button_second);
				button_second->setVisible(false);

				const char * secondString_ = StringDataManager::getString("generals_Recurite_button_close");
				label_second = UILabel::create();
				label_second->setText(secondString_);
				label_second->setAnchorPoint(ccp(0.5f,0.5f));
				label_second->setPosition(ccp(0,0));
				label_second->setFontSize(20);
				button_second->addChild(label_second);
			}
			break;
		}

// 		CCActionInterval * action =(CCActionInterval *)CCSequence::create(
// 			CCScaleTo::create(0.4f,2.0f,2.0f),
// 			CCScaleTo::create(0.15f,0.8f,0.8f),
// 			CCScaleTo::create(0.1f,1.0f,1.0f),
// 			CCCallFunc::create(this, callfunc_selector(RecruitGeneralCard::lightActions)),
// 			NULL);
// 		imageGeneral_bg->runAction(action);

		imageGeneral_bg->setScale(1.0f);
		imageGeneral_bg->setColor(ccc3(0,0,0));

		CCActionInterval * action =(CCActionInterval *)CCSequence::create(
			CCTintTo::create(0.5f, 255,255,255),
			CCCallFunc::create(this,callfunc_selector(RecruitGeneralCard::createParticleSys)),
			CCDelayTime::create(0.5f),
			CCCallFunc::create(this, callfunc_selector(RecruitGeneralCard::lightActions)),
			CCDelayTime::create(0.5f),
			NULL);
		imageGeneral_bg->runAction(action);
		action->setTag(kTag_head_anm);
		//head imageView action
		imageGeneral_Icon->setColor(ccc3(0,0,0));
		CCFiniteTimeAction * head_action = CCSequence::create(
			CCDelayTime::create(1.0f),
			CCTintTo::create(0.3f, 255,255,255),
			NULL);
		imageGeneral_Icon->runAction(head_action);
		//name label action
		Label_generalName->setColor(ccc3(0,0,0));
		CCFiniteTimeAction * nameLabel_action = CCSequence::create(
			CCDelayTime::create(1.0f),
			CCTintTo::create(0.3f, 255,255,255),
			NULL);
		Label_generalName->runAction(nameLabel_action);
		

// 		CCRepeatForever *repeat1 = CCRepeatForever::create(CCSequence::create(CCMoveBy::create(0.5f,ccp(0,10)),CCMoveBy::create(0.5f,ccp(0,-10)),NULL));
// 		imageGeneral_bg->runAction(repeat1);

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(u_layer->getContentSize());
		return true;
	}
	return false;
}

void RecruitGeneralCard::onEnter()
{
	UIScene::onEnter();
}

void RecruitGeneralCard::onExit()
{
	UIScene::onExit();
}

bool RecruitGeneralCard::ccTouchBegan(CCTouch *touch, CCEvent * pEvent)
{
	if (m_type == kTypeFirstRecurite)
	{
		return true;
	}
	else if (m_type == kTypeCommonRecuriteGeneral || m_type == kTypeUsePropRecuriteGeneral )
	{
		if (isRunTutorials)
		{
			return true;
		}

		if (isFinishedAction())
		{
			return resignFirstResponder(touch,this,false);
		}
		else
		{
			return true;
		}
	}
	else
	{
		return true;
	}
}

void RecruitGeneralCard::ccTouchEnded(CCTouch *touch, CCEvent * pEvent)
{
}

void RecruitGeneralCard::ccTouchCancelled(CCTouch *touch, CCEvent * pEvent)
{
}

void RecruitGeneralCard::ccTouchMoved(CCTouch *touch, CCEvent * pEvent)
{
}

void RecruitGeneralCard::callBackShowGeneral( CCObject * obj )
{
	this->removeCCTutorialIndicator();
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5056,(void *)m_generalId,(void *)1);
	this->closeAnim();
}

void RecruitGeneralCard::addCCTutorialIndicator( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	int _w = (winSize.width-this->getContentSize().width)/2;
	int _h = (winSize.height - this->getContentSize().height)/2;
	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,true);
	tutorialIndicator->setPosition(ccp(pos.x,pos.y));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	tutorialIndicator->addHighLightFrameAction(150,50,ccp(0,5));
	tutorialIndicator->addCenterAnm(150,50,ccp(0,5));
	this->addChild(tutorialIndicator);
}

void RecruitGeneralCard::addCCTutorialIndicator1( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	int _w = (winSize.width-this->getContentSize().width)/2;
	int _h = (winSize.height - this->getContentSize().height)/2;

	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,100,40,true);
	tutorialIndicator->setDrawNodePos(ccp(pos.x+_w,pos.y+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void RecruitGeneralCard::removeCCTutorialIndicator()
{
	CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(u_layer->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		CCTeachingGuide* teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}

void RecruitGeneralCard::lightActions()
{
	CCActionInterval * action =(CCActionInterval *)CCSequence::create(
		CCScaleTo::create(0.4f,1.5f,1.5f),
		CCCallFunc::create(this, callfunc_selector(RecruitGeneralCard::generalStarActions)),
		NULL);
	image_light_a->runAction((CCActionInterval *)action->copy()->autorelease());

	CCRepeatForever *repeat = CCRepeatForever::create( CCRotateBy::create(6.0f,360) );
	image_light_a->runAction((CCActionInterval *)repeat->copy()->autorelease());

	image_light_b->runAction((CCActionInterval *)repeat->copy()->autorelease());

	CCActionInterval * action_b = (CCActionInterval *)CCSequence::create(
					CCScaleTo::create(0.7f,3.5f,3.5f),
					CCScaleTo::create(0.7f,3.0f,3.0f),
					NULL);
	CCRepeatForever *repeat_b = CCRepeatForever::create(action_b);
	image_light_b->runAction(repeat_b);
	
	image_up_->setVisible(true);
	image_up_->runAction(CCMoveBy::create(0.4f,ccp(0,10)));
	
	switch(m_type)
	{
	case kTypeFirstRecurite:
		{
			image_down_->setVisible(true);	
			image_down_->runAction(CCMoveBy::create(0.4f,ccp(0,-10)));
		}break;
	case kTypeCommonRecuriteGeneral:
		{
			button_first->setVisible(true);
			button_first->runAction(CCMoveBy::create(0.4f,ccp(0,-10)));
			button_second->setVisible(true);
			button_second->runAction(CCMoveBy::create(0.4f,ccp(0,-10)));
			button_third->setVisible(true);
			button_third->runAction(CCMoveBy::create(0.4f,ccp(0,-10)));

			if (u_layer->getWidgetByName("imageView_goldInGot"))
			{
				u_layer->getWidgetByName("imageView_goldInGot")->setVisible(true);
				u_layer->getWidgetByName("imageView_goldInGot")->runAction(CCMoveBy::create(0.4f,ccp(0,-10)));
			}
			
		}break;
	case kTypeUsePropRecuriteGeneral:
		{
			button_first->setVisible(true);
			button_first->runAction(CCMoveBy::create(0.4f,ccp(0,-10)));
			button_second->setVisible(true);
			button_second->runAction(CCMoveBy::create(0.4f,ccp(0,-10)));
		}break;
	}
}

void RecruitGeneralCard::generalStarActions()
{
	imageGeneral_Star->setVisible(true);
	CCActionInterval * action =(CCActionInterval *)CCSequence::create(
		CCScaleTo::create(0.2f,1.0,1.0f),
		CCCallFunc::create(this, callfunc_selector(RecruitGeneralCard::generalProfessionActions)),
		NULL);

	imageGeneral_Star->runAction(action);
}

void RecruitGeneralCard::generalProfessionActions()
{
	imageGeneral_profession->setVisible(true);
	imageGeneral_profession->runAction(CCScaleTo::create(0.2f,1.0f,1.0f));

	if (m_type == kTypeFirstRecurite)
	{
		const char *firstGeneralstring = StringDataManager::getString("general_fightfirstGetNeweGeneral");
		int x_ = image_down_->getPosition().x;
		int y_ = image_down_->getPosition().y - 5;
		this->addCCTutorialIndicator(firstGeneralstring, ccp(x_,y_),CCTutorialIndicator::Direction_LU);
	}
}

std::string RecruitGeneralCard::getGeneralProfessionIconPath( int prfofessionId )
{
	std::string iconPath = "res_ui/";
	switch(prfofessionId)
	{
	case PROFESSION_MJ_INDEX:
		{
			iconPath.append("mengjiang");
		}break;
	case PROFESSION_GM_INDEX:
		{
			iconPath.append("guimou");
		}break;
	case PROFESSION_HJ_INDEX:
		{
			iconPath.append("haojie");
		}break;
	case PROFESSION_SS_INDEX:
		{
			iconPath.append("shenshe");
		}break;
	}
	iconPath.append(".png");
	return iconPath;
}

std::string RecruitGeneralCard::getGeneralIconPath( std::string generalIcon )
{
	std::string path_ = "res_ui/general/";
	path_.append(generalIcon);
	path_.append(".png");

	return path_;
}

void RecruitGeneralCard::callbackFirstButton( CCObject * pSender )
{
	if (senderListener && senderSelectorFirstButton)
	{
		(senderListener->*senderSelectorFirstButton)(this);
	}
}

void RecruitGeneralCard::callBackSecondButton( CCObject * pSender )
{
	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(pSender);

	if (senderListener && senderSelectorSecondButton)
	{
		(senderListener->*senderSelectorSecondButton)(this);
	}
	this->closeAnim();
}

void RecruitGeneralCard::callBackThirdButton( CCObject * pSender )
{
	if (senderListener && senderSelectorThirdButton)
	{
		(senderListener->*senderSelectorThirdButton)(this);
	}
	//this->closeAnim();
}

void RecruitGeneralCard::AddcallBackFirstEvent( CCObject* pSender, SEL_CallFuncO pEvent )
{
	senderListener = pSender;
	senderSelectorFirstButton = pEvent;
}

void RecruitGeneralCard::AddcallBacksecondEvent( CCObject* pSender, SEL_CallFuncO pEvent )
{
	senderListener = pSender;
	senderSelectorSecondButton = pEvent;
}

void RecruitGeneralCard::AddcallBackThirdEvent( CCObject* pSender, SEL_CallFuncO pEvent )
{
	senderListener = pSender;
	senderSelectorThirdButton = pEvent;
}

UIButton * RecruitGeneralCard::getFirstButton()
{
	return button_first;
}

UIButton * RecruitGeneralCard::getSecondButton()
{
	return button_second;
}

UIButton * RecruitGeneralCard::getThirdButton()
{
	return button_third;
}

UILabel * RecruitGeneralCard::getFirstLabel()
{
	return label_first;
}

UILabel * RecruitGeneralCard::getSecondLabel()
{
	return label_second;
}

UILabel * RecruitGeneralCard::getThirdLabel()
{
	return label_third;
}

void RecruitGeneralCard::createParticleSys()
{
	CCNodeRGBA* pNode = BonusSpecialEffect::create();
	pNode->setPosition(ccp(192, 218));
	pNode->setScale(1.2f);
	addChild(pNode);
}

bool RecruitGeneralCard::isFinishedAction()
{
	if (imageGeneral_bg->getActionByTag(kTag_head_anm))
	{
		return false;
	}
	
	return true;
}

void RecruitGeneralCard::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void RecruitGeneralCard::RefreshCostValueByActionType( int actionType )
{
	for (int i =0;i<GameView::getInstance()->actionDetailList.size();++i)
	{
		if (GameView::getInstance()->actionDetailList.at(i)->type() == actionType)
		{
			char s_gold[20];
			sprintf(s_gold,"%d",GameView::getInstance()->actionDetailList.at(i)->gold());
			if (u_layer->getWidgetByName("imageView_goldInGot"))
			{
				UILabel * l_goldValue = (UILabel*)u_layer->getWidgetByName("imageView_goldInGot")->getChildByName("label_goldInGotValue");
				if (l_goldValue)
				{
					l_goldValue->setText(s_gold);
				}
			}
		}
	}
}


