#include "ControlTree.h"
#include "GameView.h"
#include "UIButtonTree.h"
#include "../../utils/GameUtils.h"



ControlTree::ControlTree(void)
{
	m_treeSelectState = "res_ui/highlight.png";
	m_secondTreeImage = "res_ui/no6_di.png";

	m_expandImage = "res_ui/plus2.png";
	m_shrink = "res_ui/jianhao.png";

	senderListener_tree = NULL;
	senderListener_branch = NULL;

	m_touchTreeIndex = 0;
	m_secondTreeOfPanelIndex = 0;

	m_isLastTree = true;
}

ControlTree::~ControlTree(void)
{
}

ControlTree * ControlTree::create(std::vector<TreeStructInfo *> treeVector,CCSize scrollSize,CCSize treeButtonSize,CCSize branchButtonSize)
{
	ControlTree * tree_ = new ControlTree();
	if (tree_ && tree_->init(treeVector,scrollSize,treeButtonSize,branchButtonSize))
	{
		tree_->autorelease();
		return tree_;
	}
	CC_SAFE_DELETE(tree_);
	return NULL;
}

bool ControlTree::init(std::vector<TreeStructInfo *> treeVector,CCSize scrollSize,CCSize treeButtonSize,CCSize branchButtonSize)
{
	if (UIScene::init())
	{
		for (int i = 0;i< treeVector.size();i++)
		{
			m_treeVector.push_back(treeVector.at(i));
		}
		
// 		UIImageView * imageFrame =UIImageView::create();
// 		imageFrame->setTexture("res_ui/zhezhao01.png");
// 		imageFrame->setAnchorPoint(ccp(0,0));
// 		imageFrame->setScale9Enable(true);
// 		imageFrame->setScale9Size(scrollSize);
// 		imageFrame->setCapInsets(CCRect(50,50,1,1));
// 		imageFrame->setPosition(ccp(0,0));
// 		m_pUiLayer->addWidget(imageFrame);

		scrollView_info = UIScrollView::create();
		scrollView_info->setTouchEnable(true);
		scrollView_info->setDirection(SCROLLVIEW_DIR_VERTICAL);
		scrollView_info->setSize(scrollSize);
		scrollView_info->setPosition(ccp(0,0));
		scrollView_info->setBounceEnabled(true);
		m_pUiLayer->addWidget(scrollView_info);
		int innerHeight = 0;
		for (int i = 1;i<m_treeVector.size()+1;i++)
		{
			UIPanel * panel_ = UIPanel::create();
			panel_->setAnchorPoint(ccp(0,0));
			panel_->setTag(i);
			scrollView_info->addChild(panel_);
			
			UIButton * button_= UIButton::create();
			button_->setTouchEnable(true);
			button_->setPressedActionEnabled(true);
			button_->addReleaseEvent(this,coco_releaseselector(ControlTree::callBackTreeCellEvent));
			button_->setAnchorPoint(ccp(0.5f,0.5f));
			button_->setScale9Enable(true);
			button_->setScale9Size(treeButtonSize);
			button_->setCapInsets(CCRect(15,30,1,1));
			button_->setPosition(ccp(0,0));
			button_->setTextures("res_ui/kuang01_new.png","res_ui/kuang01_new.png","");
			button_->setTag(i*TREE_BUTTON_TAG);
			panel_->addChild(button_);

			UIImageView * buttonSelectState_ =UIImageView::create();
			buttonSelectState_->setTexture(m_treeSelectState.c_str());			
			buttonSelectState_->setScale9Enable(true);
			buttonSelectState_->setScale9Size(button_->getSize());
			buttonSelectState_->setAnchorPoint(ccp(0.5f,0.5f));
			buttonSelectState_->setPosition(ccp(0,0));
			buttonSelectState_->setVisible(false);
			buttonSelectState_->setName(BUTTON_SELECT_STATE);
			button_->addChild(buttonSelectState_);
			
			//tree label 
			std::string label_ = m_treeVector.at(i-1)->getTreeName();
			UILabel * button_label = UILabel::create();
			button_label->setAnchorPoint(ccp(0.5f,0.5f));
			button_label->setPosition(ccp(0,0 ));
			button_label->setText(label_.c_str());
			button_label->setFontSize(18);
			button_label->setZOrder(10);
			button_->addChild(button_label);

			UIImageView * buttonState_ =UIImageView::create();
			buttonState_->setTexture(m_expandImage.c_str());
			buttonState_->setAnchorPoint(ccp(0.5f,0.5f));
			buttonState_->setPosition(ccp(-button_->getSize().width/2+buttonState_->getContentSize().width/2,0));
			buttonState_->setScale(0.5f);
			buttonState_->setName(BUTTON_EXPAND_SHRINK_MARK);
			buttonState_->setZOrder(10);
			button_->addChild(buttonState_);

			if (i == 1)
			{
				//set scrollView_info size
				tree_buttonHeight = button_->getSize().height;
				innerHeight = m_treeVector.size() * button_->getSize().height;
				if (innerHeight < scrollSize.height)
				{
					innerHeight = scrollSize.height;
				}

				buttonSelectState_->setVisible(true);
				scrollView_info->setInnerContainerSize(CCSizeMake(scrollView_info->getSize().width,innerHeight));
			}

			this->setButtonTreeSize(treeButtonSize);
			this->setBranchButtonSize(branchButtonSize);

			panel_->setSize(CCSizeMake(scrollView_info->getSize().width,button_->getSize().height));
			panel_->setPosition(ccp(0,innerHeight - button_->getSize().height*i));
			button_->setPosition(ccp(panel_->getSize().width/2,panel_->getSize().height - button_->getSize().height/2));
		}
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(scrollView_info->getContentSize());
		return true;
	}
	return false;
}

void ControlTree::onEnter()
{
	UIScene::onEnter();
}

void ControlTree::onExit()
{
	UIScene::onExit();
}

void ControlTree::callBackTreeCellEvent( CCObject * obj )
{
	UIButton * btn_ = (UIButton *)obj;
	int btnIndex_ = btn_->getWidgetTag();

	if (this->getTouchTreeIndex() == btnIndex_/TREE_BUTTON_TAG - 1)
	{
		this->setIsLastTree(true);
	}else
	{
		this->setIsLastTree(false);
	}

	this->setTouchTreeIndex(btnIndex_/TREE_BUTTON_TAG - 1);

	UIPanel * panel_index = (UIPanel *)scrollView_info->getChildByTag(btnIndex_/TREE_BUTTON_TAG);
	UIPanel * panel_secondTree = (UIPanel *)panel_index->getChildByTag(btnIndex_*TREE_BUTTON_TAG);
	
	if (btn_->getChildByName(BUTTON_EXPAND_SHRINK_MARK) != NULL)
	{
		UIImageView * image_ = (UIImageView *)btn_->getChildByName(BUTTON_EXPAND_SHRINK_MARK);
		int vectorindex_ = btnIndex_/TREE_BUTTON_TAG - 1;
		if (panel_secondTree == NULL)
		{
			image_->setTexture(m_shrink.c_str());

			//setvisible else expand secondtree,keep only one show
			for (int i = 1;i<m_treeVector.size()+1;i++)
			{
				UIPanel * panel_index = (UIPanel *)scrollView_info->getChildByTag(i);
				UIPanel * panel_secondTree = (UIPanel *)panel_index->getChildByTag(i*SECOND_PANEL_TAG);
				if (panel_secondTree != NULL)
				{
					UIButton * temp_button = (UIButton *)panel_index->getChildByTag(i*TREE_BUTTON_TAG);
					if (temp_button!= NULL && temp_button->getChildByName(BUTTON_EXPAND_SHRINK_MARK))
					{
						UIImageView * image_ = (UIImageView *)temp_button->getChildByName(BUTTON_EXPAND_SHRINK_MARK);
						image_->setTexture(m_expandImage.c_str());
					}
					this->createSecondtree(i*TREE_BUTTON_TAG,m_treeVector.at(i-1)->getSecondVector().size(),false);
				}
			}

			this->createSecondtree(btnIndex_,m_treeVector.at(vectorindex_)->getSecondVector().size(),true);

			//callBack addEvent
			if (senderListener_tree && senderSelector_tree)
			{
				(senderListener_tree->*senderSelector_tree)(this);
			}
		}else
		{
			image_->setTexture(m_expandImage.c_str());
			this->createSecondtree(btnIndex_,m_treeVector.at(vectorindex_)->getSecondVector().size(),false);

			//first setvisible all select state
			this->setAllButtonSelectStateVisible();
			if (btn_->getChildByName(BUTTON_SELECT_STATE) != NULL)
			{
				UIImageView * selectState_ = (UIImageView *)btn_->getChildByName(BUTTON_SELECT_STATE);
				selectState_->setVisible(true);
			}
		}
	}
}

void ControlTree::createSecondtree(int btnIndex,int num,bool isCreate)
{
	UIPanel * panel_index = (UIPanel *)scrollView_info->getChildByTag(btnIndex/TREE_BUTTON_TAG);
	UIButton * button_index = (UIButton *)panel_index->getChildByTag(btnIndex);
	
	int w_ = 0;
	int h_ = 0;
	int off_h = 0;
	int scroll_h = scrollView_info->getInnerContainer()->getSize().height;

	if (isCreate)
	{
		int temp_index_root = btnIndex/TREE_BUTTON_TAG;
		UIPanel * panel_second = UIPanel::create();
		panel_second->setAnchorPoint(ccp(0,0));
		panel_second->setTouchEnable(true);
		panel_second->setTag(temp_index_root*SECOND_PANEL_TAG);
		panel_index->addChild(panel_second);
		long long startTime = GameUtils::millisecondNow();//-----
		int secondTree_size = m_treeVector.at(btnIndex/TREE_BUTTON_TAG - 1)->getSecondVector().size();
		for (int i = 1;i<secondTree_size+1;i++)
		{
			UIButtonTree * button_branch= UIButtonTree::create();
			button_branch->setTouchEnable(true);
			//button_branch->setPressedActionEnabled(true);
			button_branch->addReleaseEvent(this,coco_releaseselector(ControlTree::callBackSecondTreeCell));
			button_branch->setAnchorPoint(ccp(0.5f,0.5f));
			button_branch->setScale9Enable(true);
			button_branch->setScale9Size(getBranchButtonSize());
			//button_branch->setCapInsets(CCRect(32,12,1,1));//39,7
			button_branch->setPosition(ccp(0,0));
			button_branch->setInfo(btnIndex/TREE_BUTTON_TAG);
			button_branch->setTextures(m_secondTreeImage.c_str(),m_secondTreeImage.c_str(),"");
			button_branch->setTag(i*SECOND_BUTTONTREE_TAG);
			panel_second->addChild(button_branch);
			
			UIImageView * second_btnSelect =UIImageView::create();
			//second_btnSelect->setTexture(m_treeSelectState.c_str());"res_ui/no1_light.png"
			second_btnSelect->setTexture("res_ui/no1_light.png");
			second_btnSelect->setScale9Enable(true);
			second_btnSelect->setScale9Size(button_branch->getSize());
			//second_btnSelect->setCapInsets(CCRect(39,7,1,1));//32,12
			second_btnSelect->setAnchorPoint(ccp(0.5f,0.5f));
			second_btnSelect->setPosition(ccp(0,0));
			second_btnSelect->setVisible(false);
			second_btnSelect->setName(SECOND_BUTTON_SELECT_STATE);
			button_branch->addChild(second_btnSelect);

			std::string labelStr_  =  (m_treeVector.at(btnIndex/100 - 1)->getSecondVector()).at(i -1 );
			UILabel * button_label = UILabel::create();
			button_label->setAnchorPoint(ccp(0.5f,0.5f));
			button_label->setPosition(ccp(0,0 ));
			button_label->setText(labelStr_.c_str());
			button_label->setFontSize(18);
			button_label->setZOrder(10);
			button_branch->addChild(button_label);

			if (i == 1)
			{
				secondTree_buttonHeight = button_branch->getSize().height;
				panel_second->setSize(CCSizeMake(scrollView_info->getSize().width,button_branch->getSize().height*num));
			}
			button_branch->setPosition(ccp(0,panel_second->getSize().height - button_branch->getSize().height *(i - 1)));
		}

		h_ = panel_index->getContentSize().height + num*secondTree_buttonHeight;
		panel_index->setSize(CCSizeMake(w_,h_));
		
		button_index->setPosition(ccp(scrollView_info->getContentSize().width/2,h_ - button_index->getSize().height/2));
		panel_second->setPosition(ccp(scrollView_info->getContentSize().width/2,-secondTree_buttonHeight/2));
		off_h = num *secondTree_buttonHeight;

		//default first select
		int tempIndex_select = 0;
		int curBtnIndex_ = btnIndex/TREE_BUTTON_TAG - 1;
		if (this->getIsLastTree())
		{
			tempIndex_select = this->getSecondTreeByEveryPanelIndex()+1; 
		}else
		{
			tempIndex_select++;
		}
		
		UIButtonTree * btn_tree = (UIButtonTree *)panel_second->getChildByTag(tempIndex_select*SECOND_BUTTONTREE_TAG);
		callBackSecondTreeCell(btn_tree);


		long long endTime = GameUtils::millisecondNow();
		CCLOG("cost time = %ld",endTime - startTime);
	}else
	{
		h_ = panel_index->getContentSize().height - num * secondTree_buttonHeight;
		panel_index->setSize(CCSizeMake(w_,h_));
		button_index->setPosition(ccp(scrollView_info->getContentSize().width/2,h_ - button_index->getSize().height/2));

		off_h = -num*secondTree_buttonHeight;
		UIPanel * panel_secondTree = (UIPanel *)panel_index->getChildByTag(btnIndex*TREE_BUTTON_TAG);
		if (panel_secondTree != NULL)
		{
			panel_secondTree->removeFromParentAndCleanup(true);
		}
	}
	scrollView_info->setInnerContainerSize(CCSizeMake(scrollView_info->getSize().width,scroll_h + off_h));
	int scroll_inner = scroll_h + off_h;
	//
	for (int j= 1;j<=m_treeVector.size();j++)
	{
		UIPanel * panel_temp = (UIPanel *)scrollView_info->getChildByTag(j);
		scroll_inner -= panel_temp->getContentSize().height;
		panel_temp->setPosition(ccp(0,scroll_inner));
	}
}

void ControlTree::callBackSecondTreeCell( CCObject * obj )
{
	this->setAllButtonSelectStateVisible();

	UIButtonTree * btn_ = (UIButtonTree *)obj;
	UIImageView * image_second = (UIImageView *)btn_->getChildByName(SECOND_BUTTON_SELECT_STATE);
	if (image_second != NULL)
	{
		image_second->setVisible(true);
	}

	//set secondButton of panelIndex
	this->setSecondTreeByEveryPanelIndex(btn_->getTag()/SECOND_BUTTONTREE_TAG - 1);

	//get secondButton by vector index
	int vectorIndex_ = 0;
	for (int i = 0;i<m_treeVector.size();i++)
	{
		if (i< btn_->getInfo() - 1)
		{
			vectorIndex_+= m_treeVector.at(i)->getSecondVector().size();
		}
	}
	vectorIndex_+= btn_->getTag()/SECOND_BUTTONTREE_TAG;
	int secondTreeIndex_ = vectorIndex_ - 1;
	this->setSecondTreeIndex(secondTreeIndex_);

	//callBack addEvent
	if (senderListener_branch && senderSelector_branch)
	{
		(senderListener_branch->*senderSelector_branch)(this);
	}
}

void ControlTree::setAllButtonSelectStateVisible()
{
	for (int i= 1;i<=m_treeVector.size();i++)
	{
		UIPanel * panel_temp = (UIPanel *)scrollView_info->getChildByTag(i);
		UIButton * button_ = (UIButton *)panel_temp->getChildByTag(i*TREE_BUTTON_TAG);
		if (button_->getChildByName(BUTTON_SELECT_STATE))
		{
			UIImageView * image_ = (UIImageView *)button_->getChildByName(BUTTON_SELECT_STATE);
			image_->setVisible(false);
		}
		
		UIPanel * panel_selecttree = (UIPanel *)panel_temp->getChildByTag(i*SECOND_PANEL_TAG);
		if (panel_selecttree != NULL)
		{
			CCArray * obj = panel_selecttree->getChildren();
			for (int j =0;j<obj->count();j++)
			{
				UIButton * btn_select = (UIButton *)obj->objectAtIndex(j);
				if (btn_select != NULL)
				{
					UIImageView * image_second = (UIImageView *)btn_select->getChildByName(SECOND_BUTTON_SELECT_STATE);
					if (image_second != NULL)
					{
						image_second->setVisible(false);
					}
				}
			}
		}
	}
}

void ControlTree::setTreeSelectState( std::string imagePath )
{
	m_treeSelectState = imagePath;
}

void ControlTree::setSecondTreeImage( std::string imagePath )
{
	m_secondTreeImage = imagePath;
}

void ControlTree::addcallBackTreeEvent( CCObject* pSender, SEL_CallFuncO pSelector )
{
	senderListener_tree = pSender;
	senderSelector_tree = pSelector;
}

void ControlTree::addcallBackSecondTreeEvent( CCObject* pSender, SEL_CallFuncO pSelector )
{
	senderListener_branch = pSender;
	senderSelector_branch = pSelector;
}


UIButton * ControlTree::getTreeButtonByIndex( int vectorIndex )
{
	//get cur panel
	int panelIndex_ = this->getTreeIndex(vectorIndex);
	UIPanel * panel_ = (UIPanel *)scrollView_info->getChildByTag(panelIndex_);
	UIButton * btn_;
	if (panel_ != NULL)
	{
		//get cur button
		btn_ = (UIButton *)panel_->getChildByTag(panelIndex_*TREE_BUTTON_TAG);
	}
	return btn_;
}

void ControlTree::addMarkOnButton( std::string str,CCPoint p,int vectorIndex /* = -1*/ )
{
	if (vectorIndex <0)
	{
		for (int i = 1;i<m_treeVector.size()+1;i++)
		{
			UIPanel * panel_ = (UIPanel *)scrollView_info->getChildByTag(i);
			UIButton * btn_ = (UIButton *)panel_->getChildByTag(i*TREE_BUTTON_TAG);
			if (btn_ != NULL && btn_->getChildByName(BUTTON_NEWADDMARK) == NULL)
			{
				UIImageView * mark_ =UIImageView::create();
				mark_->setTexture(str.c_str());
				mark_->setAnchorPoint(ccp(0.5f,0.5f));
				mark_->setPosition(p);
				mark_->setName(BUTTON_NEWADDMARK);
				btn_->addChild(mark_);
			}
		}
	}else
	{
		UIButton * btn_ = this->getTreeButtonByIndex(vectorIndex);
		if (btn_ != NULL && btn_->getChildByName(BUTTON_NEWADDMARK) == NULL)
		{
			UIImageView * mark_ =UIImageView::create();
			mark_->setTexture(str.c_str());
			mark_->setAnchorPoint(ccp(0.5f,0.5f));
			mark_->setPosition(p);
			mark_->setName(BUTTON_NEWADDMARK);
			btn_->addChild(mark_);
		}
	}
}

void ControlTree::removeMarkOnButton( int vectorIndex /*= -1*/ )
{
	if (vectorIndex <0)
	{
		for (int i = 1;i<m_treeVector.size()+1;i++)
		{
			UIPanel * panel_ = (UIPanel *)scrollView_info->getChildByTag(i);
			UIButton * btn_ = (UIButton *)panel_->getChildByTag(i*TREE_BUTTON_TAG);
			if (btn_ != NULL && btn_->getChildByName(BUTTON_NEWADDMARK) != NULL)
			{
				UIImageView * mark_ = (UIImageView *)btn_->getChildByName(BUTTON_NEWADDMARK);
				mark_->removeFromParentAndCleanup(true);
			}
		}
	}else
	{
		UIButton * btn_ = this->getTreeButtonByIndex(vectorIndex);
		if (btn_ != NULL)
		{
			UIImageView * mark_ = (UIImageView *)btn_->getChildByName(BUTTON_NEWADDMARK);
			if (mark_)
			{
				mark_->removeFromParentAndCleanup(true);
			}						
		}
	}
}

int ControlTree::getTreeIndex( int vectorIndex)
{
	int temp_index = vectorIndex+1;
	for (int i =0;i<m_treeVector.size();i++)
	{
		temp_index -=m_treeVector.at(i)->getSecondVector().size();
		if (temp_index <= 0)
		{
			return i+1;
		}
	}
}

UIButtonTree * ControlTree::getSecondTreeButtonByIndex( int vectorIndex)
{
	UIButtonTree * btn_ = NULL;
	int temp_index = vectorIndex+1;
	for (int i = 1;i<m_treeVector.size()+1;i++)
	{
		temp_index -=m_treeVector.at(i - 1)->getSecondVector().size();
		if (temp_index <= 0)
		{
			UIPanel * panel_ = (UIPanel *)scrollView_info->getChildByTag(i);
			UIPanel * second_panel = (UIPanel *)panel_->getChildByTag(i*SECOND_PANEL_TAG);
			int btnindex_ =  temp_index+m_treeVector.at(i-1)->getSecondVector().size();
			btn_ = (UIButtonTree *)second_panel->getChildByTag(btnindex_*SECOND_BUTTONTREE_TAG);
			break;
		}
	}
	return btn_;
}

UIButtonTree * ControlTree::getSecondTreeByPanelIndexOrSecondIndex( int panelIndex,int secondIndex )
{
	UIButtonTree * btn_ = NULL;
	UIPanel * panel_ = (UIPanel *)scrollView_info->getChildByTag(panelIndex);
	UIPanel * second_panel = (UIPanel *)panel_->getChildByTag(panelIndex*SECOND_PANEL_TAG);
	btn_ = (UIButtonTree *)second_panel->getChildByTag(secondIndex*SECOND_BUTTONTREE_TAG);
	return btn_;
}


void ControlTree::addMarkOnSecondButton( std::string str,CCPoint p,int vectorIndex /*= -1*/ )
{
	UIButtonTree * btn_ = (UIButtonTree *)this->getSecondTreeButtonByIndex(vectorIndex);
	if (btn_ && btn_->getChildByName(SECOND_BUTTON_NEWADDMARK)== NULL)
	{
		UIImageView * mark_ =UIImageView::create();
		mark_->setTexture(str.c_str());
		mark_->setAnchorPoint(ccp(0.5f,0.5f));
		mark_->setPosition(p);
		mark_->setName(SECOND_BUTTON_NEWADDMARK);
		btn_->addChild(mark_);
	}
}

void ControlTree::removeMarkOnSecondButton( int vectorIndex /*= -1*/ )
{
	if (vectorIndex <0)
	{
		for (int i = 1;i<m_treeVector.size()+1;i++)
		{
			UIPanel * panel_ = (UIPanel *)scrollView_info->getChildByTag(i);
			UIPanel * panel_second = (UIPanel *)panel_->getChildByTag(i*SECOND_PANEL_TAG);

			for (int j = 1;j<m_treeVector.at(i - 1)->getSecondVector().size()+1;j++)
			{
				UIButtonTree * treeButton_ = (UIButtonTree *)panel_second->getChildByTag(j*SECOND_BUTTONTREE_TAG);
				if (treeButton_ != NULL && treeButton_->getChildByName(SECOND_BUTTON_NEWADDMARK) != NULL)
				{
					UIImageView * mark_ = (UIImageView *)treeButton_->getChildByName(SECOND_BUTTON_NEWADDMARK);
					mark_->removeFromParentAndCleanup(true);
				}
			}
		}
	}else
	{
		UIButtonTree * btn_ =(UIButtonTree *) this->getSecondTreeButtonByIndex(vectorIndex);
		if (btn_ && btn_->getChildByName(SECOND_BUTTON_NEWADDMARK) != NULL)
		{
			UIImageView * mark_ = (UIImageView *)btn_->getChildByName(SECOND_BUTTON_NEWADDMARK);
			mark_->removeFromParentAndCleanup(true);
		}
	}
}

void ControlTree::setSecondTreeIndex( int index )
{
	m_curSelectIndex = index;
}

int ControlTree::getSecondTreeIndex()
{
	return m_curSelectIndex;
}

void ControlTree::setSecondTreeByEveryPanelIndex( int panelIndex )
{
	m_secondTreeOfPanelIndex = panelIndex;
}

int ControlTree::getSecondTreeByEveryPanelIndex()
{
	return m_secondTreeOfPanelIndex;
}


CCSize ControlTree::getBranchButtonSize()
{
	return branchButton_size;
}

void ControlTree::setBranchButtonSize( CCSize size_ )
{
	branchButton_size = size_;
}

void ControlTree::setButtonTreeSize( CCSize size_ )
{
	treeButton_size = size_;
}

CCSize ControlTree::getButtonTreeSize()
{
	return treeButton_size;
}

void ControlTree::removeSecondButton( int vectorIndex )
{
	/*
	//delete second button
	UIButtonTree * btn_ =(UIButtonTree *)this->getSecondTreeButtonByIndex(vectorIndex);
	if (btn_ )
	{
		btn_->removeFromParentAndCleanup(true);
	}

	int btnindex_ = 0;
	int vectorIndex_ = 0;
	//refresh tree data
	int temp_index = vectorIndex+1;
	for (int i = 1;i<m_treeVector.size()+1;i++)
	{
		temp_index -=m_treeVector.at(i - 1)->getSecondVector().size();
		if (temp_index <= 0)
		{
			btnindex_ =  temp_index+m_treeVector.at(i-1)->getSecondVector().size() - 1;
			vectorIndex_ = i - 1;

			TreeStructInfo * temp = m_treeVector.at(i - 1);
			std::vector<std::string> tempVector = temp->getSecondVector();
			std::vector<std::string>::iterator iter = tempVector.begin()+btnindex_;
			tempVector.erase(iter);
			
			int a = m_treeVector.at(vectorIndex_)->getSecondVector().size();			
			(m_treeVector.at(vectorIndex_)->getSecondVector()).clear();
			a = m_treeVector.at(vectorIndex_)->getSecondVector().size();
			break;
		}
	}
	
	//reSet every panel and button position
	//int btnIndex_temp = btnindex_+1;
	int btnIndex_temp = this->getTreeIndex(vectorIndex);
	int size_ = 3;//m_treeVector.at(vectorIndex_)->getSecondVector().size();
	UIPanel * panel_index = (UIPanel *)scrollView_info->getChildByTag(btnIndex_temp);
	UIPanel * branchPanel_ = (UIPanel *)panel_index->getChildByTag(btnIndex_temp*SECOND_PANEL_TAG);
	branchPanel_->setSize(CCSizeMake(scrollView_info->getSize().width,secondTree_buttonHeight*size_));
	
	for (int i = 1;i< size_+1;i++)
	{
		UIButtonTree * secondTree_ = (UIButtonTree *)branchPanel_->getChildByTag(i*SECOND_BUTTONTREE_TAG);
		if (secondTree_ != NULL)
		{
			secondTree_->setPosition(ccp(0,branchPanel_->getSize().height - secondTree_->getSize().height *(i - 1)));
		}
	}


	int scroll_inner = scrollView_info->getInnerContainerSize().height;
	for (int j= 1;j<=m_treeVector.size();j++)
	{
		UIPanel * panel_temp = (UIPanel *)scrollView_info->getChildByTag(j);
		scroll_inner -= panel_temp->getContentSize().height;
		panel_temp->setPosition(ccp(0,scroll_inner));
	}
	*/
}

void ControlTree::setTouchTreeIndex( int index )
{
	m_touchTreeIndex = index;
}

int ControlTree::getTouchTreeIndex()
{
	return m_touchTreeIndex;
}

void ControlTree::setShowSecondTreeCell( int vectorIndex )
{
	if (vectorIndex<0)
	{
		return;
	}
	UIButton* btn_ = (UIButton *)getTreeButtonByIndex(vectorIndex);
	this->callBackTreeCellEvent(btn_);

	UIButtonTree * btn_cell = (UIButtonTree *)getSecondTreeButtonByIndex(vectorIndex);
	this->callBackSecondTreeCell(btn_cell);

	//set position of can see
	int offset_H = btn_->getPosition().y + btn_cell->getPosition().y;

	float h_1 = btn_cell->getPosition().y/scrollView_info->getInnerContainerSize().height;
	if (scrollView_info->getContentSize().height < offset_H)
	{
		float prect_ = 100 - h_1*100;
		scrollView_info->jumpToPercentVertical(prect_);
	}
}

void ControlTree::setIsLastTree( bool isValue )
{
	m_isLastTree = isValue;
}

bool ControlTree::getIsLastTree()
{
	return m_isLastTree;
}




///////////////////////////////////
TreeStructInfo::TreeStructInfo( void )
{

}

TreeStructInfo::~TreeStructInfo( void )
{

}

void TreeStructInfo::setTreeName( std::string name )
{
	m_treeName_ = name;
}

std::string TreeStructInfo::getTreeName()
{
	return m_treeName_;
}

void TreeStructInfo::addSecondVector(std::string secondString)
{
	m_secondTree.push_back(secondString);
}

std::vector<std::string> TreeStructInfo::getSecondVector()
{
	return m_secondTree;
}
