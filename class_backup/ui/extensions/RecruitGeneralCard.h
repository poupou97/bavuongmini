
#ifndef _H_RECRUITGENERALCARD_H 
#define _H_RECRUITGENERALCARD_H

#include "../extensions/UIScene.h"
#include "../../legend_script/CCTutorialIndicator.h"

/*
* 第一次招募武将
* 通用的招募武将卡牌
*/
class CGeneralDetail;
class RecruitGeneralCard:public UIScene
{
public:
	enum RecruitGeneralCardType
	{
		kTypeFirstRecurite = 0,
		kTypeCommonRecuriteGeneral,
		kTypeUsePropRecuriteGeneral,
	};
public:
	RecruitGeneralCard(void);
	~RecruitGeneralCard(void);

	static RecruitGeneralCard * create(int curType = kTypeFirstRecurite,int generalModelId = 0);
	bool init(int curType,int generalModelId);
	void onEnter();
	void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	std::string getGeneralIconPath(std::string generalIcon);
	void callBackShowGeneral(CCObject * obj);

	cocos2d::extension::UIImageView * image_light_a;
	cocos2d::extension::UIImageView * image_light_b;
	void lightActions();

	void createParticleSys();

	cocos2d::extension::UIImageView * imageGeneral_Star;
	void generalStarActions();

	cocos2d::extension::UIImageView * imageGeneral_profession;
	void generalProfessionActions();

	cocos2d::extension::UIImageView * image_up_;
	cocos2d::extension::UIImageView * image_down_;

	void AddcallBackFirstEvent(CCObject* pSender, SEL_CallFuncO pEvent);
	void AddcallBacksecondEvent(CCObject* pSender, SEL_CallFuncO pEvent);
	void AddcallBackThirdEvent(CCObject* pSender, SEL_CallFuncO pEvent);

	cocos2d::extension::UIButton * getFirstButton();
	cocos2d::extension::UIButton * getSecondButton();
	cocos2d::extension::UIButton * getThirdButton();

	cocos2d::extension::UILabel * getFirstLabel();
	cocos2d::extension::UILabel * getSecondLabel();
	cocos2d::extension::UILabel * getThirdLabel();

	//是否正在教学
	bool isRunTutorials;
	bool isFinishedAction();

	void RefreshCostValueByActionType(int actionType);

private:
	cocos2d::extension::UIImageView * imageGeneral_bg;
	cocos2d::extension::UIButton * button_first;
	cocos2d::extension::UIButton * button_second;
	cocos2d::extension::UIButton * button_third;

	cocos2d::extension::UILabel * label_first;
	cocos2d::extension::UILabel * label_second;
	cocos2d::extension::UILabel * label_third;
	void callbackFirstButton(CCObject * pSender);
	void callBackSecondButton(CCObject * pSender);
	void callBackThirdButton(CCObject * pSender);
	
	CCObject * senderListener;
	SEL_PushEvent senderSelectorFirstButton;
	SEL_PushEvent senderSelectorSecondButton;
	SEL_PushEvent senderSelectorThirdButton;
public:
	static std::string getGeneralProfessionIconPath(int prfofessionId);
	//教学
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction );
	//教学点击继续（kTypeCommonRecuiiteGeneral）
	void addCCTutorialIndicator1(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction );
	void removeCCTutorialIndicator();
	int mTutorialScriptInstanceId;
private:
	CCSize winsize;
	UILayer * u_layer;
	//首次招募的武将ID
	long long m_generalId;
	int m_type;
};
#endif;
