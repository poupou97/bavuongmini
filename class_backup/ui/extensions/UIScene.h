
#ifndef _UI_EXTENSIONS_UISCENE_H_
#define _UI_EXTENSIONS_UISCENE_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

#define TAG_UISCENE_UILAYER 0

// this size is the same as the design resolution in the UIEditor
#define UI_DESIGN_RESOLUTION_WIDTH 800
#define UI_DESIGN_RESOLUTION_HEIGHT 480

class UIScene : public CCLayer
{
public:
	UIScene();
	virtual ~UIScene();
	bool init();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);

	void openAnim();
	void closeAnim();
	/**
	   * @param animated 关闭该界面时，是否有动画效果
	   */
	bool resignFirstResponder(CCTouch *touch,CCNode* pNode,bool isTouchContinue, bool animated = true);

	bool isClosing();

protected:
	UILayer *m_pUiLayer;   // default UILayer ( ZOrder = 0 )
};

#endif