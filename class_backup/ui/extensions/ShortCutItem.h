
#ifndef H_SHORTCUTITEM_H
#define H_SHORTCUTITEM_H

#include "../extensions/UIScene.h"
class FolderInfo;
/*
*获得好的装备时弹出快捷穿戴的窗口
*快捷穿装
*快捷使用物品
*/

#define CARD_LAYERTAG 100
#define CARD_ROLE_FRAMETAG 110
#define CARD_ROLE_ICONTAG 120

#define CARD_GENERAL_FRAMETAG 150
#define CARD_GENERAL_ICONTAG 160

/*区分此弹框的类型，在弹框排序时用到*/
enum
{
	type_equip = 1,
	type_else = 2,
};


class ShortCutItem : public UIScene
{
public:
	ShortCutItem(void);
	~ShortCutItem(void);

	static ShortCutItem * create(FolderInfo * foler,long long roleId,bool setPriority = false);
	bool init(FolderInfo * foler,long long roleId,bool setPriority);

	void onEnter();
	void onExit();

	void callBackCloseUi(CCObject * obj);

	void createGeneralForDressOnEquip(long long roleId);
	
	void callBackDressOn(CCObject * obj);
	void callBackShowEquipment(CCObject * obj);
	void callBackUseGoodsEvent(CCObject * obj);
	UILabel * goodsAmount;

	std::string getGoodsIconByQuality(int quality_);

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
public:
	void setItemId(std::string id_);
	std::string getItemId();	

	void setItemIndex(int index_);
	int getItemIndex();

	void setEquipClazz(int clazz_);
	int getEquipClazz();

	void setProfession(int profession_);
	int getProfession();

	void setLevel(int level_);
	int getLevel();

	void setFightcapacity(int fightValue);
	int getFightcapacity();

	void setFolderType(int typeValue);
	int getFolderType(); 
private:
	int m_profession;
	int m_level;
	int m_fightcapacity;
	int m_typeValue;
	std::string m_itemId;
	int m_itemIndex;
	int m_clazz;
private:
	CCSize winsize;
	UIPanel * panel_;

	FolderInfo * m_folder;
	long long m_generalId;

	int m_curGoodsRemAmount;
	int m_curGeneralId;
};

#endif