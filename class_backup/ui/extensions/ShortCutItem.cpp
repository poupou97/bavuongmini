#include "ShortCutItem.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "../backpackscene/GoodsItemInfoBase.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../gamescene_state/MainScene.h"
#include "../backpackscene/PackageScene.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../generals_ui/GeneralsUI.h"
#include "../generals_ui/RecuriteActionItem.h"
#include "../../gamescene_state/role/ActorUtils.h"
#include "../generals_ui/GeneralsListUI.h"
#include "../../AppMacros.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "gamescene_state/EffectDispatch.h"

#define btn_label_des_tag 1100

ShortCutItem::ShortCutItem(void)
{
}


ShortCutItem::~ShortCutItem(void)
{
	CC_SAFE_DELETE(m_folder);
}

ShortCutItem * ShortCutItem::create(FolderInfo * foler,long long roleId,bool setPriority )
{
	ShortCutItem * item_ = new ShortCutItem();
	if (item_ && item_->init(foler,roleId,setPriority))
	{
		item_->autorelease();
		return item_;
	}
	CC_SAFE_DELETE(item_);
	return NULL;
}

bool ShortCutItem::init(FolderInfo * foler,long long roleId,bool setPriority )
{
	if (UIScene::init())
	{
		winsize =CCDirector::sharedDirector()->getVisibleSize();

		m_folder =  new FolderInfo();
		m_folder->CopyFrom(*foler);

		panel_ =UIPanel::create();
		panel_->setAnchorPoint(ccp(0.5f,0.5f));
		panel_->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(panel_);

		UIImageView * imageMainFrame = UIImageView::create();
		imageMainFrame->setTexture("res_ui/kuangb.png");
		imageMainFrame->setAnchorPoint(ccp(0.5f,0.5f));
		imageMainFrame->setPosition(ccp(0,0));
		panel_->addChild(imageMainFrame);

		UIButton * buttonEqip= UIButton::create();
		buttonEqip->setTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
		buttonEqip->setTouchEnable(true);
		buttonEqip->setPressedActionEnabled(true);
		buttonEqip->setAnchorPoint(ccp(0.5f,0.5f));
		buttonEqip->setScale9Enable(true);
		buttonEqip->setScale9Size(CCSizeMake(85,35));
		buttonEqip->setCapInsets(CCRect(18,9,2,23));
		buttonEqip->setPosition(ccp(0,buttonEqip->getContentSize().height/2-imageMainFrame->getSize().height/2+17));
		imageMainFrame->addChild(buttonEqip);

		CCTutorialParticle * btnParticle = CCTutorialParticle::create("tuowei0.plist",65,40);
		btnParticle->setPosition(ccp(buttonEqip->getPosition().x,buttonEqip->getPosition().y+buttonEqip->getSize().height/2+6));
		btnParticle->setTag(CCTUTORIALPARTICLETAG);
		buttonEqip->addCCNode(btnParticle);

		UILabel * btn_des=UILabel::create();
		btn_des->setAnchorPoint(ccp(0.5f,0.5f));
		btn_des->setPosition(ccp(0,0));
		btn_des->setTag(btn_label_des_tag);
		btn_des->setFontSize(18);
		buttonEqip->addChild(btn_des);

		UIButton * buttonClose = UIButton::create();
		buttonClose->setTextures("res_ui/close.png","res_ui/close.png","");
		buttonClose->setTouchEnable(true);
		buttonClose->setPressedActionEnabled(true);
		buttonClose->setAnchorPoint(ccp(0.5f,0.5f));
		buttonClose->setPosition(ccp(imageMainFrame->getContentSize().width/2 -10,imageMainFrame->getContentSize().height/2 - 10));
		buttonClose->addReleaseEvent(this,coco_releaseselector(ShortCutItem::callBackCloseUi));
		imageMainFrame->addChild(buttonClose);

		int equipmentquality_ = m_folder->goods().quality();
		m_curGoodsRemAmount = m_folder->quantity();
		ccColor3B colonColor = GameView::getInstance()->getGoodsColorByQuality(equipmentquality_);
		UILabel * goodsName=UILabel::create();
		goodsName->setText(foler->goods().name().c_str());
		goodsName->setAnchorPoint(ccp(0.5f,0.5f));
		goodsName->setPosition(ccp(0, buttonEqip->getPosition().y + buttonEqip->getContentSize().height/2 + goodsName->getContentSize().height/2+2));
		goodsName->setColor(colonColor);
		goodsName->setFontSize(12);
		panel_->addChild(goodsName);

		std::string frameColorPath =  getGoodsIconByQuality(equipmentquality_);
		UIButton * Btn_pacItemFrame = UIButton::create();
		Btn_pacItemFrame->setTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
		Btn_pacItemFrame->setTouchEnable(true);
		Btn_pacItemFrame->setScale9Enable(true);
		Btn_pacItemFrame->setCapInsets(CCRect(9.5f,9.5f,6.0f,6.0f));
		Btn_pacItemFrame->setScale9Size(CCSizeMake(65,65));
		Btn_pacItemFrame->setAnchorPoint(ccp(0.5f,0.5f));
		Btn_pacItemFrame->setPosition(ccp(0,goodsName->getPosition().y + goodsName->getContentSize().height/2+ Btn_pacItemFrame->getContentSize().height/2+5));
		Btn_pacItemFrame->addReleaseEvent(this,coco_releaseselector(ShortCutItem::callBackShowEquipment));
		Btn_pacItemFrame->setPressedActionEnabled(true);
		panel_->addChild(Btn_pacItemFrame);
		Btn_pacItemFrame->setScale(0.85f);

		std::string  iconPath_ ="res_ui/props_icon/";
		std::string equipicon_ = m_folder->goods().icon();
		iconPath_.append(equipicon_);
		iconPath_.append(".png");

		UIImageView * goodsIcon_ =UIImageView::create();
		goodsIcon_->setTexture(iconPath_.c_str());
		goodsIcon_->setAnchorPoint(ccp(0.5f,0.5f));
		goodsIcon_->setPosition(ccp(0,0));
		Btn_pacItemFrame->addChild(goodsIcon_);

		if (m_folder->quantity() >1)
		{
			char goodsAmountStr[10];
			sprintf(goodsAmountStr,"%d",m_folder->quantity());
			goodsAmount =UILabel::create();
			goodsAmount->setText(goodsAmountStr);
			goodsAmount->setAnchorPoint(ccp(1,1));
			goodsAmount->setPosition(ccp(21,-10));
			goodsAmount->setFontSize(14);
			Btn_pacItemFrame->addChild(goodsAmount);
		}

		if (m_folder->goods().equipmentclazz() >0  && m_folder->goods().equipmentclazz() <11)
		{
			const char *strings = StringDataManager::getString("equip_getNewequip");
			btn_des->setText(strings);
			buttonEqip->addReleaseEvent(this,coco_releaseselector(ShortCutItem::callBackDressOn));


			this->setFightcapacity(m_folder->goods().equipmentdetail().fightcapacity());
			this->setFolderType(type_equip);
			this->setEquipClazz(m_folder->goods().equipmentclazz());
			this->setProfession(m_folder->goods().equipmentdetail().profession());
			//show general info for equip of dresson
			createGeneralForDressOnEquip(roleId);

		}else
		{
			this->setFolderType(type_else);

			const char *strings = StringDataManager::getString("teachRemind_useGoodsOnce");
			btn_des->setText(strings);
			buttonEqip->addReleaseEvent(this,coco_releaseselector(ShortCutItem::callBackUseGoodsEvent));
		}

		this->setItemIndex(m_folder->id());
		this->setItemId(m_folder->goods().id());

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(imageMainFrame->getContentSize());
		this->setTouchPriority(11);
		m_pUiLayer->setTouchPriority(10);

		return true;
	}
	return false;
}

void ShortCutItem::onEnter()
{
	UIScene::onEnter();
}

void ShortCutItem::onExit()
{
	UIScene::onExit();
}

void ShortCutItem::callBackCloseUi( CCObject * obj )
{
	this->removeFromParentAndCleanup(true);
	//erase first  folder_vector,if it size >0
	if (EffectEquipItem::folderItemVector.size() > 0)
	{
		std::vector<FolderInfo *>::iterator iter_ = EffectEquipItem::folderItemVector.begin();
		FolderInfo * iterFoler =  * iter_;
		EffectEquipItem::folderItemVector.erase(iter_);
		delete iterFoler;

		EffectEquipItem::reloadShortItem();
	}
}

void ShortCutItem::callBackDressOn( CCObject * obj )
{
	UIButton * btn_ = (UIButton *)obj;
	btn_->setTextures("res_ui/new_button_001.png","res_ui/new_button_001.png","");
	btn_->setTouchEnable(false);
	UILabel * label_des = (UILabel *)btn_->getChildByTag(btn_label_des_tag);
	const char *strings = StringDataManager::getString("equip_getNewequip_dressOn");
	label_des->setText(strings);

	GameMessageProcessor::sharedMsgProcessor()->sendReq(1303,m_folder,(void *)m_curGeneralId);
	//
	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	CCSprite* pSprite = CCSprite::create("res_ui/font/ableUp.png");
	pSprite->setAnchorPoint(ccp(0.5f,0.5f));
	pSprite->setPosition(ccp(0,100));
	pSprite->setScale(0.75f);

	CCLegendAnimation* effect = CCLegendAnimation::create("animation/texiao/renwutexiao/SJTX/sjtx1.anm");
	
	if (m_curGeneralId == 0)
	{
		//add role
		GameView::getInstance()->myplayer->addChild(pSprite);
		GameView::getInstance()->myplayer->addEffect(effect, false, 0);
	}else
	{
		//add general
		BaseFighter* target = dynamic_cast<BaseFighter*>(scene->getActor(m_curGeneralId));
		if(target)
		{
			target->addChild(pSprite);
			target->addEffect(effect, false, 0);
		}
	}

	CCFiniteTimeAction*  action = CCSequence::create(
		CCShow::create(),
		CCDelayTime::create(0.3f),
		CCMoveBy::create(0.5f,ccp(0,40)),
		CCRemoveSelf::create(),
		NULL);

	pSprite->runAction(action);

	//add general card icon is runaction
	if (this->getChildByTag(CARD_LAYERTAG))
	{
		UILayer * layer_ = (UILayer *)this->getChildByTag(CARD_LAYERTAG);
		if (m_curGeneralId == 0)
		{
			//role
			if (layer_->getWidgetByTag(CARD_ROLE_FRAMETAG))
			{
				UIImageView * imageFrame = (UIImageView *)layer_->getWidgetByTag(CARD_ROLE_FRAMETAG);
				if (imageFrame->getChildByTag(CARD_ROLE_ICONTAG))
				{
					UIImageView * imagerIcon = (UIImageView *)imageFrame->getChildByTag(CARD_ROLE_ICONTAG);

					CCSequence * seq_ = CCSequence::create(
						CCScaleTo::create(0.1f,1.2f),
						CCScaleTo::create(0.1f,1.0f),
						CCDelayTime::create(0.3f),
						CCCallFuncO::create(this,callfuncO_selector(ShortCutItem::callBackCloseUi),NULL),
						NULL);

					imagerIcon->runAction(seq_);
				}
			}
		}else
		{
			//general
			if (layer_->getChildByTag(CARD_GENERAL_FRAMETAG))
			{
				CCSprite * sp_frame = (CCSprite *)layer_->getChildByTag(CARD_GENERAL_FRAMETAG);
				if (sp_frame->getChildByTag(CARD_GENERAL_ICONTAG))
				{
					CCNode * node_ = (CCNode *)sp_frame->getChildByTag(CARD_GENERAL_ICONTAG);
					CCSequence * seq_ = CCSequence::create(
						CCScaleTo::create(0.1f,1.3f),//0.85f
						CCScaleTo::create(0.1f,1.0f),//0.7f
						CCDelayTime::create(0.3f),
						CCCallFuncO::create(this,callfuncO_selector(ShortCutItem::callBackCloseUi),NULL),
						NULL);
					node_->runAction(seq_);
				}
			}
		}
	}
}

void ShortCutItem::createGeneralForDressOnEquip( long long roleId )
{
	if (this->getChildByTag(CARD_LAYERTAG))
	{
		UILayer * layer_ = (UILayer *)this->getChildByTag(CARD_LAYERTAG);
		layer_->removeFromParentAndCleanup(true);
	}

	UILayer * temp_layer = UILayer::create();
	temp_layer->setTag(CARD_LAYERTAG);
	addChild(temp_layer);

	m_curGeneralId = roleId;

	if (roleId == 0)
	{
		UIImageView * smaillFrame = UIImageView::create();
		smaillFrame->setAnchorPoint(ccp(0.5f,0.5f));
		smaillFrame->setPosition(ccp(0, 0));
		smaillFrame->setTexture("res_ui/generals_white.png");
		smaillFrame->setTag(CARD_ROLE_FRAMETAG);
		smaillFrame->setScaleY(0.98f);
		temp_layer->addWidget(smaillFrame);
		//icon
		UIImageView * sprite_icon = UIImageView::create();
		sprite_icon->setTexture(BasePlayer::getBigHeadPathByProfession(GameView::getInstance()->myplayer->getProfession()).c_str());
		sprite_icon->setScale(0.98f);
		sprite_icon->setAnchorPoint(ccp(0.5f, 0));
		sprite_icon->setPosition(ccp(0,-sprite_icon->getContentSize().width/2));
		sprite_icon->setTag(CARD_ROLE_ICONTAG);
		smaillFrame->addChild(sprite_icon);
		//level
		UIImageView * sprite_lvFrame = UIImageView::create();
		sprite_lvFrame->setScale9Enable(true);
		sprite_lvFrame->setTexture("res_ui/zhezhao80.png");
		sprite_lvFrame->setScale9Size(CCSizeMake(34,13));
		sprite_lvFrame->setAnchorPoint(ccp(1.0f, 0));
		sprite_lvFrame->setPosition(ccp(smaillFrame->getContentSize().width/2-7, -smaillFrame->getContentSize().height/2+25));
		//sprite_lvFrame->setPosition(ccp(0,0));
		smaillFrame->addChild(sprite_lvFrame);

		std::string _lv = "LV";
		char s[10];
		sprintf(s,"%d",GameView::getInstance()->myplayer->getActiveRole()->level());
		_lv.append(s);
		UILabel * label_lv = UILabel::create();
		label_lv->setAnchorPoint(ccp(1.0f, 0));
		label_lv->setText(_lv.c_str());
		label_lv->setFontSize(13);
		label_lv->setPosition(ccp(0,0));
		sprite_lvFrame->addChild(label_lv);
		//name backGround
		UIImageView * sprite_nameFrame = UIImageView::create();
		sprite_nameFrame->setCapInsets(CCRectMake(10,10,1,5));
		sprite_nameFrame->setAnchorPoint(ccp(0.5f, 0.5f));
		sprite_nameFrame->setTexture("res_ui/name_di3.png");
		sprite_nameFrame->setScale9Size(CCSizeMake(72,13));
		sprite_nameFrame->setPosition(ccp(0,-smaillFrame->getContentSize().height/2+sprite_nameFrame->getContentSize().height/2+8));
		smaillFrame->addChild(sprite_nameFrame);
		//name
		const char * generalRoleName_ = StringDataManager::getString("generalList_RoleName");
		UILabel * label_name = UILabel::create();
		label_name->setAnchorPoint(ccp(0.5f, 0.5f));
		label_name->setPosition(ccp(0,0));
		label_name->setText(generalRoleName_);
		label_name->setFontSize(14);
		label_name->setColor(ccc3(0,255,0));
		sprite_nameFrame->addChild(label_name);
	}else
	{
		CGeneralBaseMsg * generalBaseMsg = new CGeneralBaseMsg();
		for (int i = 0;i<GameView::getInstance()->generalsInLineList.size();i++)
		{
			if (m_curGeneralId == GameView::getInstance()->generalsInLineList.at(i)->id())
			{
				generalBaseMsg->CopyFrom(*GameView::getInstance()->generalsInLineList.at(i));
			}
		}

		CGeneralBaseMsg * generalBaseMsgFromDB  = GeneralsConfigData::s_generalsBaseMsgData[generalBaseMsg->modelid()];

		CCSprite * sprite_frame = CCSprite::create(GeneralsUI::getBigHeadFramePath(generalBaseMsg->currentquality()).c_str());
		sprite_frame->setAnchorPoint(ccp(0.5f, 0.5f));
		sprite_frame->setPosition(ccp(0,0));
		sprite_frame->setTag(CARD_GENERAL_FRAMETAG);
		sprite_frame->setScaleY(0.98f);
		temp_layer->addChild(sprite_frame);

		//general icon
		CCSprite * temp_node = CCSprite::create("res_ui/di.png");
		temp_node->setAnchorPoint(ccp(0.5f,0));
		temp_node->setPosition(ccp(sprite_frame->getContentSize().width/2,sprite_frame->getContentSize().height/2));
		temp_node->setTag(CARD_GENERAL_ICONTAG);
		sprite_frame->addChild(temp_node);

		std::string sprite_icon_path = "res_ui/generals/";
		sprite_icon_path.append(generalBaseMsgFromDB->get_half_photo());
		sprite_icon_path.append(".anm");
		CCLegendAnimation *la_head = CCLegendAnimation::create(sprite_icon_path);
		if (la_head)
		{
			la_head->setPlayLoop(true);
			la_head->setReleaseWhenStop(false);
			//la_head->setPosition(ccp(6,25));
			la_head->setPosition(ccp(6-46,25 - 72));
			la_head->setScale(0.7f);
			temp_node->addChild(la_head);
		}
		//level
		CCScale9Sprite * sprite_lvFrame = CCScale9Sprite::create("res_ui/zhezhao80.png");
		sprite_lvFrame->setAnchorPoint(ccp(1.0f, 0));
		sprite_lvFrame->setContentSize(CCSizeMake(34,13));
		sprite_lvFrame->setPosition(ccp(sprite_frame->getContentSize().width*sprite_frame->getScaleX()-8, 25));
		sprite_frame->addChild(sprite_lvFrame);
		//general level
		std::string _lv = "LV";
		char s[5];
		sprintf(s,"%d",generalBaseMsg->level());
		_lv.append(s);
		CCLabelTTF * label_lv = CCLabelTTF::create(_lv.c_str(),APP_FONT_NAME,13);
		label_lv->setAnchorPoint(ccp(0.5f,0.5f));
		label_lv->setPosition(ccp(sprite_lvFrame->getContentSize().width/2,label_lv->getContentSize().height/2));
		label_lv->enableStroke(ccc3(0, 0, 0), 2.0f);
		label_lv->setTag(265);
		sprite_lvFrame->addChild(label_lv);
		//general name
		CCLabelTTF * label_name = CCLabelTTF::create(generalBaseMsgFromDB->name().c_str(),APP_FONT_NAME,14);
		label_name->setAnchorPoint(ccp(0.5f, 0));
		label_name->enableStroke(ccc3(0, 0, 0), 2.0f);
		label_name->setPosition(ccp(sprite_frame->getContentSize().width/2,7));
		label_name->setColor(GameView::getInstance()->getGeneralsColorByQuality(generalBaseMsg->currentquality()));
		sprite_frame->addChild(label_name);
		//rare
		if (generalBaseMsg->rare()>0)
		{
			CCSprite * sprite_star = CCSprite::create(RecuriteActionItem::getStarPathByNum(generalBaseMsg->rare()).c_str());
			sprite_star->setAnchorPoint(ccp(0,0));
			sprite_star->setScale(0.6f);
			sprite_star->setPosition(ccp(5, 25));
			sprite_frame->addChild(sprite_star);
		}
		//general post
		if (generalBaseMsg->evolution() > 0)
		{
			CCSprite * sprite_rank = ActorUtils::createGeneralRankSprite(generalBaseMsg->evolution());
			sprite_rank->setAnchorPoint(ccp(1.0f,1.0f));
			sprite_rank->setScale(0.7f);
			sprite_rank->setPosition(ccp(sprite_frame->getContentSize().width*sprite_frame->getScaleX()-5, sprite_frame->getContentSize().height*sprite_frame->getScaleY()-9));
			sprite_frame->addChild(sprite_rank);
		}

		if(generalBaseMsg->fightstatus() == GeneralsListUI::InBattle)
		{
			//fight flag
			CCSprite * imageView_inBattle = CCSprite::create("res_ui/wujiang/play.png");
			imageView_inBattle->setAnchorPoint(ccp(0.5,0.5));
			imageView_inBattle->setPosition(ccp(17,127));
			imageView_inBattle->setScale(0.8f);
			sprite_frame->addChild(imageView_inBattle);
		}
	}
	//
	temp_layer->setPosition(ccp(-108,2));
}

void ShortCutItem::callBackShowEquipment( CCObject * obj )
{
	GoodsInfo * goods_ =new GoodsInfo();
	goods_->CopyFrom(m_folder->goods());

	GoodsItemInfoBase * goodsbase_ =GoodsItemInfoBase::create(goods_,GameView::getInstance()->EquipListItem,0);
	goodsbase_->ignoreAnchorPointForPosition(false);
	goodsbase_->setAnchorPoint(ccp(0.5f,0.5f));
	GameView::getInstance()->getMainUIScene()->addChild(goodsbase_);
	delete goods_;
}

void ShortCutItem::callBackUseGoodsEvent( CCObject * obj )
{
	if (GameView::getInstance()->AllPacItem.at(m_folder->id())->has_goods())
	{
		if (m_folder->goods().id() == GameView::getInstance()->AllPacItem.at(m_folder->id())->goods().id())
		{
			PackageScene::IdxAndNum * temp = new PackageScene::IdxAndNum();
			temp->index = m_folder->id();
			temp->num = 1;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1302, (void *)0,temp);
			delete temp;
			m_curGoodsRemAmount--;
			if (m_curGoodsRemAmount <= 0)
			{
				this->removeFromParentAndCleanup(true);
			}else
			{
				char str[10];
				sprintf(str,"%d",m_curGoodsRemAmount);
				//goodsAmount->setText(str);
			}
		}else
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("teachRemind_useGoodsFail_dealog"));
			this->closeAnim();
		}
	}else
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("teachRemind_useGoodsFail_dealog"));
		this->closeAnim();
	}
}

bool ShortCutItem::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	CCPoint location = pTouch->getLocation();
	CCPoint location_temp = ccp(location.x+54,location.y+73);

	CCPoint pos = this->getPosition();
	CCSize size = this->getContentSize();
	CCPoint anchorPoint = this->getAnchorPointInPoints();
	pos = ccpSub(pos, anchorPoint);
	CCRect rect(pos.x, pos.y, size.width, size.height);
	if(rect.containsPoint(location_temp))
	{
		return true;
	}
	return false;
}

std::string ShortCutItem::getGoodsIconByQuality( int quality_ )
{
	std::string frameColorPath = "res_ui/";
	if (quality_ == 1)
	{
		frameColorPath.append("sdi_white");
	}
	else if (quality_ == 2)
	{
		frameColorPath.append("sdi_green");
	}
	else if (quality_ == 3)
	{
		frameColorPath.append("sdi_bule");
	}
	else if (quality_ == 4)
	{
		frameColorPath.append("sdi_purple");
	}
	else if (quality_ == 5)
	{
		frameColorPath.append("sdi_orange");
	}
	else
	{
		frameColorPath.append("sdi_white");
	}
	frameColorPath.append(".png");

	return frameColorPath;
}

void ShortCutItem::setFightcapacity( int fightValue )
{
	m_fightcapacity = fightValue;
}

int ShortCutItem::getFightcapacity()
{
	return m_fightcapacity;
}

void ShortCutItem::setFolderType( int typeValue )
{
	m_typeValue = typeValue;
}

int ShortCutItem::getFolderType()
{
	return m_typeValue;
}

void ShortCutItem::setProfession( int profession_ )
{
	m_profession = profession_;
}

int ShortCutItem::getProfession()
{
	return m_profession;
}

void ShortCutItem::setLevel( int level_ )
{
	m_level = level_;
}

int ShortCutItem::getLevel()
{
	return m_level;
}

void ShortCutItem::setItemId( std::string id_ )
{
	m_itemId = id_;
}

std::string ShortCutItem::getItemId()
{
	return m_itemId;
}

void ShortCutItem::setItemIndex( int index_ )
{
	m_itemIndex = index_;
}

int ShortCutItem::getItemIndex()
{
	return m_itemIndex;
}

void ShortCutItem::setEquipClazz( int clazz_ )
{
	m_clazz = clazz_;
}

int ShortCutItem::getEquipClazz()
{
	return m_clazz;
}
