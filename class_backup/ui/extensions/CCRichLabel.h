#ifndef _CUSTOM_CCRICHLABEL_H_
#define _CUSTOM_CCRICHLABEL_H_

#include "cocos2d.h"
#include "cocos-ext.h"

#define MAX_RICHLABEL_EMOTE 42

USING_NS_CC;
USING_NS_CC_EXT;

static const char* formatFlagChar			= { "/" };
static const int formatFlagCharLen			= 1;

static const char* chatExpression			= { "/e" };
static const int chatExpressionLen			= 2;
static const int chatExpressionContentLen	= 2;

static const char* chatItem				= { "/t" };
static const int chatItemLen			= 2;
static const int chatItemContentLen		= 4;

static const char* textDefinitionMarkup		= { "/#" };
static const int textDefinitionMarkupLen		= 2;

static const char* returnMarkup		= { "/n" };
static const int returnMarkupLen		= 2;

class RichElement;

class RichLabelDefinition {
public:
	RichLabelDefinition();

public:
	int firstLineIndent;
	int alignment;
	int charLimit;
	float fontSize;
	ccColor3B fontColor;   // this color will be applied to the text which has not color flag ( /#ff0000 xxx/ )
	bool strokeEnabled;
	bool shadowEnabled;
};

////////////////////////////////////////////////////////////

/**
 * parse a rich text to a CCNode
 * the format of the rich text is below:
 *     "hello world, /tSword,1012/, /e01 hi /e02, etc."
 *
 * button: /tButtonName,propid(propid,propinstanceid)/,
 *     for example: "/tSword,propid(jian01,1012)/", ButtonName = Sword, LinkContent = propid(jian01,1012)
 *
 * sprite: /eSpriteName
 *     for example: "/e01", SpriteName = 01
 * ( in the future, this can be extended by using url format )
 * 
 * text with definition
 *     for example: "/#ff0000 hello world/", this will show the text by using red color ( ff0000 )
 * ( in the future, this can be extended by using more font definition ( shadow, stroke etc.) )
 * 
 * 这个类的实现，主要灵感来源于: https://github.com/happykevins/cocos2dx-ext 其中的HTML Widget
 * @author zhaogang
 * @version 0.1.0
 */
class CCRichLabel : public CCNodeRGBA, public CCLabelProtocol
{
// these classes are friends, they can access the private members
friend class RichElementSprite;
friend class RichElementButton;
friend class RichElementText;
friend class RichElementReturn;

public:
	enum alignment {
		alignment_vertical_top = 0,
		alignment_vertical_center = 1,
		alignment_vertical_bottom = 2,
	};

public:
	CCRichLabel();
	virtual ~CCRichLabel();

	static CCRichLabel* create();
	/**
	   * utf8_str, the content
	   * preferred_size, the content size
	   * target, the listener
	   * selector, the event handler
	   * firstLineIndent, the first line indent
	   * fontSize, the rich label's font size
	   * charLimit, 
	   * alignment, the alignment, now mainly for the RichElementSprite
	   */
	static CCRichLabel* createWithString(const char* utf8_str, const CCSize& preferred_size, CCObject* target, SEL_MenuHandler selector, int firstLineIndent = 0, float fontSize = 20,int charLimit = 5, int alignment = alignment_vertical_bottom);
	static CCRichLabel* create(const char* utf8_str, const CCSize& preferred_size, RichLabelDefinition def, CCObject* target = NULL, SEL_MenuHandler selector = NULL);
	bool initWithString(const char* utf8_str, const CCSize& preferred_size, CCObject* target, SEL_MenuHandler selector);

	virtual void onEnter();

	// from CCLabelProtocol
	virtual void setString(const char *utf8_str);
	virtual const char* getString(void);
	/* if just append string, use this optimized function */
	void appendString(const char *string);

	virtual void parseString(const char* str, std::vector<RichElement*>* outputList);
	virtual int generateRichElementText(const char* str, int nLen, std::vector<RichElement*>* elementList);
	virtual int generateRichElementTextWithDefinition(const char* str, int nLen, std::vector<RichElement*>* elementList);
	virtual int generateRichElementSprite(const char* str, int nLen, std::vector<RichElement*>* elementList);
	virtual int generateRichElementButton(const char* str, int nLen, std::vector<RichElement*>* elementList);
	virtual int generateRichElementReturn(const char* str, int nLen, std::vector<RichElement*>* elementList);

	// from CCLayer
	//virtual void draw();

	/* when delete one element succeefully, m_string will be modified at the same time */
	void deleteElementAtIdx(int index);
	int getElementSize();

	/* when display RichElementText, the char number in one CCLabel. the min value is 1 */
	void setCharNumInOneLabel(int num) { m_nCharNumInOneLabel = num; };

	inline void setFirstLineIndent(int value) { m_nFirstLineIndent = value; };

	/* used for the input cursor's postion */
	CCPoint getEndPos();

	/** get the total char count of all elements */
	int getCharCount();
    inline const std::vector<int>& getLinesHeight() { return m_linesHeight; };

	inline float getFontSize()  { return m_fFontSize; };

	RichLabelDefinition& getLabelDefinition();

protected:
    CCObject*       m_pListener;
    SEL_MenuHandler    m_pfnSelector;

    /** Dimensions of the label in Points */
    CCSize m_tDimensions;

	 float m_fFontSize;

	 int m_alignment;

	 RichLabelDefinition m_labelDef;

private:
	// 排版算法
	// 各个元素已经被解析（Parse）出来，开始进行排版
	//     若富文本中，包含了Image Element，需要重置每行的y坐标
    bool updateAll(std::vector<RichElement*>* elementList);

private:
    /** label's string */
    std::string m_string;

	std::vector<RichElement*>* m_elementList;

	int m_nCharNumInOneLabel;

	// default value is 0, it means there is no indent for the first line
	int m_nFirstLineIndent;
    
    // the height of each lines in the rich label
    std::vector<int> m_linesHeight;
};

#endif
