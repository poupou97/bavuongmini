#include "CCRichLabel.h"

#include "RichElement.h"

static bool StringEqual( const char* p, const char* q, int nChar )  {
    int n = 0;
    if ( p == q ) {
        return true;
    }
    while( *p && *q && *p == *q && n<nChar ) {
        ++p;
        ++q;
        ++n;
    }
    if ( (n == nChar) || ( *p == 0 && *q == 0 ) ) {
        return true;
    }
    return false;
}

// find str, and return result(bool)
// searchedLen record how many chars were scaned by this call
bool StringFind(const char* source, const char* str, int nChar, int* searchedLen) {
	const char* begin = source;
	*searchedLen = 0;
	while(*source)
	{
		++(*searchedLen);
		if(StringEqual(source, str, nChar))
		{
			return true;
		}
		++source;
	}
	return false;
}

//std::vector<std::string>* parseString(const char* str)
//{
//	std::vector<std::string>* elementList = new std::vector<std::string>();
//
//	std::string _str = str;
//	//_str[0]
//	//_str.find(
//	//_str.fin
//	//int i = 0;
//	int maxLen = _str.size();
//	//while(i < maxLen);
//
//	//unsigned short* utf16str = cc_utf8_to_utf16(str);
//	//size_t maxLen = cc_wcslen(utf16str);
//	//for ( size_t i = 0; i < maxLen; i++ )
//	//{
//	//}
//	size_t i = 0;
//	//while(i < maxLen)
//	//{
//	//	const char* begin = _str[i];
//	//	// find "<"
//	//	// if "<" is not found
//	//	if(strcmp(begin, "<") == 0)
//	//	{
//	//	}
//	//	//char* name = utf16str[i];
//	//}
//
//	int len = _str.size();
//	char* _charBuffer = new char[ len+1 ];
//    memcpy( _charBuffer, str, len );
//    _charBuffer[len] = 0;
//
//    char* p = _charBuffer;
//    //char  endChar = *endTag;
//    //size_t length = strlen( endTag );
//
//    // Inner loop of text parsing.
//    while ( *p ) {
//        //if ( *p == endChar && strncmp( p, endTag, length ) == 0 ) {
//        //    Set( start, p, strFlags );
//        //    return p + length;
//        //}
//		const char test = *p;
//		if(StringEqual(p, chatExpression, chatExpressionLen))
//		{
//			char* begin = p;
//			p += chatExpressionLen;
//			p += 2;
//			std::string tmpstr(begin, chatExpressionLen+2);
//			elementList->push_back(tmpstr);
//		}
//		else if(StringEqual(p, chatItem, chatItemLen))
//		{
//			char* begin = p;
//			int i = 3;
//			p += chatItemLen;
//			p += 4;
//
//			std::string tmpstr(begin, chatItemLen+4);
//			elementList->push_back(tmpstr);
//		}
//		else
//		{
//			char* begin = p;
//			// find "/"
//			while(*p)
//			{
//				if(StringEqual(p, "/", 1))
//				{
//					break;
//				}
//				++p;
//			}
//			//begin += (p - begin);
//			std::string tmpstr(begin, p - begin);
//			elementList->push_back(tmpstr);
//
//			// convert to utf16
//			unsigned short* utf16str = cc_utf8_to_utf16(tmpstr.c_str());
//			size_t maxLen = cc_wcslen(utf16str);
//			unsigned short* pp = utf16str;
//			int cnt = 0;
//			unsigned short* urf16_begin = pp;
//			while(*pp)
//			{
//				const unsigned short* begin = pp;
//				long step = 5;   // split text by 5 chars
//				if((pp + step) - urf16_begin > maxLen)
//				{
//					step = maxLen - (pp - urf16_begin);
//				}
//
//				char* utf8Str = cc_utf16_to_utf8(begin, step, NULL, NULL);
//				std::string tmpstr(utf8Str);
//				elementList->push_back(tmpstr);
//				delete[] utf8Str;
//
//				pp += step;
//			}
//			
//
//			delete[] utf16str;
//		}
//        //++p;
//    }
//	delete[] _charBuffer;
//
//	return elementList;
//}

bool checkValid_Emote(const char* elementStr)
{
	// emote
	char emote[5];
	for(int i = 1; i <= MAX_RICHLABEL_EMOTE; i++)
	{
		if( i <= 9)
			sprintf(emote, "/e0%d", i);
		else
			sprintf(emote, "/e%d", i);

		if(StringEqual(emote, elementStr, chatExpressionLen + chatExpressionContentLen))
			return true;
	}
	return false;
}

bool checkValid_Button(const char* elementStr)
{
	// "/t/" is invalid
	if(StringEqual(elementStr, "/t/", 3))
		return false;

	return true;
	// item
	//if(StringEqual("/t0001", elementStr, chatItemLen + chatItemContentLen))
	//{
	//	return true;
	//}
	//else if(StringEqual("/t0002", elementStr, chatItemLen + chatItemContentLen))
	//{
	//	return true;
	//}
	//return false;

	// search next formatFlagChar
	int searchedLen = 0;
	return StringFind(elementStr, formatFlagChar, 1, &searchedLen);
}

bool checkValid_TextWithDefinition(const char* elementStr)
{
	// "/#/" is invalid
	if(StringEqual(elementStr, "/#/", 3))
		return false;

	return true;
}

//////////////////////////////////////////////////////////

RichLabelDefinition::RichLabelDefinition()
: firstLineIndent(0)
, alignment(2)   // 2 is alignment_vertical_bottom
, charLimit(5)
, fontSize(20)
, strokeEnabled(true)
, shadowEnabled(false)
{
}

////////////////////////////////////////////////////////////

CCRichLabel::CCRichLabel()
: m_fFontSize(20)
{
	m_elementList = new std::vector<RichElement*>();
	m_pListener = NULL;
    m_pfnSelector = NULL;
	m_nCharNumInOneLabel = 5;
	m_nFirstLineIndent = 0;
	m_alignment = alignment_vertical_bottom;

	// update the definition
	m_labelDef.charLimit = m_nCharNumInOneLabel;
	m_labelDef.fontSize = m_fFontSize;
	m_labelDef.fontColor = ccc3(255, 255, 255);   // default color is white
	m_labelDef.firstLineIndent = m_nFirstLineIndent;
	m_labelDef.alignment = m_alignment;

	this->setCascadeOpacityEnabled(true);
}

CCRichLabel::~CCRichLabel()
{
	std::vector<RichElement*>::iterator iter;
	for (iter = m_elementList->begin(); iter != m_elementList->end(); ++iter) {
		delete *iter;
	}
	delete m_elementList;
	CCSpriteFrameCache::sharedSpriteFrameCache()->removeUnusedSpriteFrames();
}

void CCRichLabel::onEnter()
{
	CCNode::onEnter();

	if(m_elementList->size() <= 0)
		return;

	// animate the RichElementSprites
	std::vector<RichElement*>::iterator iter;;
	for (iter = m_elementList->begin(); iter != m_elementList->end(); ++iter)
	{
		if((*iter)->type == RichElement::RichElementType_Sprite)
		{
			RichElementSprite* sprElement = (RichElementSprite*)(*iter);
			sprElement->animate();
		}
	}
}

int CCRichLabel::generateRichElementButton(const char* str, int nLen, std::vector<RichElement*>* elementList)
{
	std::string tmpstr(str, nLen);
	//elementList->push_back(tmpstr);

	RichElementButton* btnElement = new RichElementButton();
	btnElement->_container = this;
	btnElement->parse(tmpstr.c_str());
	elementList->push_back(btnElement);

	return nLen;
}

int CCRichLabel::generateRichElementSprite(const char* str, int nLen, std::vector<RichElement*>* elementList)
{
	std::string tmpstr(str, nLen);

	RichElementSprite* sprElement = new RichElementSprite();
	sprElement->_container = this;
	sprElement->parse(tmpstr.c_str());
	elementList->push_back(sprElement);

	return nLen;
}

int CCRichLabel::generateRichElementText(const char* str, int nLen, std::vector<RichElement*>* elementList)
{
	std::string tmpstr(str, nLen);

	// convert to utf16
	unsigned short* utf16str = cc_utf8_to_utf16(tmpstr.c_str());
	size_t maxLen = cc_wcslen(utf16str);
	unsigned short* pp = utf16str;
	int cnt = 0;
	unsigned short* urf16_begin = pp;
	while(*pp)
	{
		const unsigned short* begin = pp;
		long step = m_nCharNumInOneLabel;   // split text by m_nCharNumInOneLabel
		if((pp + step) - urf16_begin > maxLen)
		{
			step = maxLen - (pp - urf16_begin);
		}

		// convert back to utf8
		char* utf8Str = cc_utf16_to_utf8(begin, step, NULL, NULL);
		std::string tmpstr(utf8Str);
		//elementList->push_back(tmpstr);
		delete[] utf8Str;

		RichElementText* txtElement = new RichElementText();
		txtElement->_container = this;
		txtElement->parse(tmpstr.c_str());
		elementList->push_back(txtElement);

		pp += step;
	}
			
	delete[] utf16str;

	return nLen;
}

int CCRichLabel::generateRichElementTextWithDefinition(const char* str, int nLen, std::vector<RichElement*>* elementList)
{
	std::string tmpstr(str, nLen);

	// full format: /#ff00ff content/
	// parse the string to get definition and content
	int index = tmpstr.find_first_of(" ");
	CCAssert(index > 0, "wrong format, missing space");   // missing " "

	// color string, for example "ff0000"
	std::string colorStr = tmpstr.substr(chatItemLen, index-textDefinitionMarkupLen);
	CCAssert(colorStr.size() == 6, "wront format, not a color string");
	std::string red = colorStr.substr(0, 2);
	int r = strtol(red.c_str(), NULL, 16);
	std::string green = colorStr.substr(2, 2);
	int g = strtol(green.c_str(), NULL, 16);
	std::string blue = colorStr.substr(4, 2);
	int b = strtol(blue.c_str(), NULL, 16);
	ccColor3B color = ccc3(r, g, b);

	// content
	index += 1;
	std::string content = tmpstr.substr(index, tmpstr.size() - index - formatFlagCharLen);

	// convert to utf16
	unsigned short* utf16str = cc_utf8_to_utf16(content.c_str());
	size_t maxLen = cc_wcslen(utf16str);
	unsigned short* pp = utf16str;
	int cnt = 0;
	unsigned short* urf16_begin = pp;
	while(*pp)
	{
		const unsigned short* begin = pp;
		long step = m_nCharNumInOneLabel;   // split text by m_nCharNumInOneLabel
		if((pp + step) - urf16_begin > maxLen)
		{
			step = maxLen - (pp - urf16_begin);
		}

		// convert back to utf8
		char* utf8Str = cc_utf16_to_utf8(begin, step, NULL, NULL);
		std::string tmpElementStr(utf8Str);
		//elementList->push_back(tmpstr);
		delete[] utf8Str;

		RichElementText* txtElement = new RichElementText(color);
		txtElement->_container = this;
		txtElement->parse(tmpElementStr.c_str());
		elementList->push_back(txtElement);

		pp += step;
	}
			
	delete[] utf16str;

	return nLen;
}

int CCRichLabel::generateRichElementReturn(const char* str, int nLen, std::vector<RichElement*>* elementList)
{
	std::string tmpstr(str, nLen);

	RichElementReturn* returnElement = new RichElementReturn();
	returnElement->_container = this;
	returnElement->parse(tmpstr.c_str());
	elementList->push_back(returnElement);

	return nLen;
}

void CCRichLabel::parseString(const char* str, std::vector<RichElement*>* outputList)
{
	std::string _str = str;
	//_str[0]
	//_str.find(
	//_str.fin
	//int i = 0;
	int maxLen = _str.size();
	//while(i < maxLen);

	//unsigned short* utf16str = cc_utf8_to_utf16(str);
	//size_t maxLen = cc_wcslen(utf16str);
	//for ( size_t i = 0; i < maxLen; i++ )
	//{
	//}
	size_t i = 0;
	//while(i < maxLen)
	//{
	//	const char* begin = _str[i];
	//	// find "<"
	//	// if "<" is not found
	//	if(strcmp(begin, "<") == 0)
	//	{
	//	}
	//	//char* name = utf16str[i];
	//}

	int len = _str.size();
	char* _charBuffer = new char[ len+1 ];
    memcpy( _charBuffer, str, len );
    _charBuffer[len] = 0;

    char* p = _charBuffer;
    //char  endChar = *endTag;
    //size_t length = strlen( endTag );

    // Inner loop of text parsing.
    while ( *p ) {
        //if ( *p == endChar && strncmp( p, endTag, length ) == 0 ) {
        //    Set( start, p, strFlags );
        //    return p + length;
        //}

		if(StringEqual(p, formatFlagChar, formatFlagCharLen))
		{
			if(StringEqual(p, chatExpression, chatExpressionLen))
			{
				int offset = 0;
				if(checkValid_Emote(p))
				{
					offset = this->generateRichElementSprite(p, chatExpressionLen+chatExpressionContentLen, outputList);
				}
				else
				{
					// consider it as text
					offset = this->generateRichElementText(p, chatExpressionLen, outputList);
				}
				p += offset;
			}
			else if(StringEqual(p, chatItem, chatItemLen))
			{
				int offset = 0;

				// find next formatFlagChar ( end flag)
				int searchedLen = 0;
				bool bFound = StringFind(p+chatItemLen, formatFlagChar, 1, &searchedLen);

				if(bFound && checkValid_Button(p))
				{
					offset = this->generateRichElementButton(p, chatItemLen+searchedLen, outputList);
				}
				else
				{
					// consider it as text
					offset = this->generateRichElementText(p, chatItemLen, outputList);
				}
				p += offset;
			}
			else if(StringEqual(p, textDefinitionMarkup, textDefinitionMarkupLen))
			{
				int offset = 0;

				// find next formatFlagChar ( end flag)
				int searchedLen = 0;
				bool bFound = StringFind(p+textDefinitionMarkupLen, formatFlagChar, 1, &searchedLen);

				if(bFound && checkValid_TextWithDefinition(p))
				{
					offset = this->generateRichElementTextWithDefinition(p, textDefinitionMarkupLen+searchedLen, outputList);
				}
				else
				{
					// consider it as text
					offset = this->generateRichElementText(p, textDefinitionMarkupLen, outputList);
				}
				p += offset;
			}
			else if(StringEqual(p, returnMarkup, returnMarkupLen))
			{
				int offset = 0;
				offset = this->generateRichElementReturn(p, returnMarkupLen, outputList);
				p += offset;
			}
			else   // consider it as text, it is formatFlagChar
			{
				int offset = generateRichElementText(p, 1, outputList);
				p += formatFlagCharLen;
			}
		}
		else   // do not find any format info ,so consider it as text
		{
			//char* begin = p;
			// find "/"
			//p++;
			//while(*p)
			//{
			//	if(StringEqual(p, "/", 1))
			//	{
			//		break;
			//	}
			//	++p;
			//}
			//int offset = generateRichElementText(begin, p - begin, outputList);

			// find next formatFlagChar, then get the text before next formatFlagChar
			int searchedLen = 0;
			bool bFound = StringFind(p, formatFlagChar, 1, &searchedLen);
			int len = bFound ? searchedLen-1 : searchedLen;
			int offset = generateRichElementText(p, len, outputList);
			p += offset;
		}
    }
	delete[] _charBuffer;
}

void CCRichLabel::deleteElementAtIdx(int index)
{
	int size = this->m_elementList->size();
	CCAssert(index < size, "idx is out of range");

	// delete the element
	std::vector<RichElement*>::iterator iter = m_elementList->begin() + index;
    RichElement* pRemovedElement = *iter;
	this->removeChild(pRemovedElement->getNode());   // remove from cocos2d render scene
    m_elementList->erase(iter);
	delete pRemovedElement;

	std::string currentString = "";
	for (iter = m_elementList->begin(); iter != m_elementList->end(); ++iter)
	{
		currentString.append((*iter)->m_string);
	}

	// setString(currentString.c_str());
	// optimized, don't use setString()
	m_string = currentString;
	updateAll(m_elementList);
}

void CCRichLabel::setString(const char *string)
{
    CCAssert(string != NULL, "Invalid string");
    
    if (m_string.compare(string))
    {
        m_string = string;
        
		// remove old children, then applay new string
		this->removeAllChildrenWithCleanup(true);

		// remove old elements
		std::vector<RichElement*>::iterator iter;;
		for (iter = m_elementList->begin(); iter != m_elementList->end(); ++iter)
		{
			CC_SAFE_DELETE(*iter);
		}
		this->m_elementList->clear();
		parseString(string, m_elementList);

		updateAll(m_elementList);
    }
}

void CCRichLabel::appendString(const char *appendStr)
{
    CCAssert(appendStr != NULL, "Invalid string");
    
	int size = m_elementList->size();
	if(size <= 0)
	{
		this->setString(appendStr);
		return;
	}

	std::string wholeAppendString;

	// 是否允许在输入框逐字符输入button或sprite
	// false, 则只能通过插入的方式得到sprite或button
	if(false)   
	{
		// 1. delete some RicheElementText
		// delete some text elements (RicheElementText)
		//int deleteCount = 0;
		std::vector<std::string> tmpDeleteStrings;
		for(int i = size - 1; i >= 0; i--)
		{
			std::vector<RichElement*>::iterator iter = m_elementList->begin() + i;
			if((*iter)->type != RichElement::RichElementType_Text)
				break;

			RichElement* pRemovedElement = *iter;
			tmpDeleteStrings.push_back(pRemovedElement->m_string);
			this->removeChild(pRemovedElement->getNode());   // remove from cocos2d render scene
			m_elementList->erase(iter);
			delete pRemovedElement;

			//deleteCount++;
			//if(deleteCount >= 6)
			//	break;
		}

		// 2. parseString
		// get all strings
	
		std::vector<std::string>::iterator strIter;
		size = tmpDeleteStrings.size();
		for(int i = size - 1; i >= 0; i--)
		{
			strIter = tmpDeleteStrings.begin() + i;
			wholeAppendString.append((*strIter));
		}
		wholeAppendString.append(appendStr);
	}
	else
	{
		wholeAppendString = appendStr;
	}

	// 3. append
	std::vector<RichElement*>* appendElementList = new std::vector<RichElement*>();
	parseString(wholeAppendString.c_str(), appendElementList);

	std::vector<RichElement*>::iterator iter;
	for (iter = appendElementList->begin(); iter != appendElementList->end(); ++iter)
	{
		m_elementList->push_back((*iter));
	}
	delete appendElementList;

	std::string currentString = "";
	for (iter = m_elementList->begin(); iter != m_elementList->end(); ++iter)
	{
		currentString.append((*iter)->m_string);
	}
	//m_string = m_string.append(wholeAppendString);
	m_string = currentString;
	updateAll(m_elementList);
}

const char* CCRichLabel::getString(void)
{
	if(m_string.size() <= 0)
		return NULL;
    return m_string.c_str();
}

bool CCRichLabel::updateAll(std::vector<RichElement*>* elementList)
{
	// composit，对所有元素进行排版

	if(elementList->size() <= 0)
		return false;

	int line = 0;
    m_linesHeight.clear();
	m_linesHeight.push_back(0);

	int firstLineOffsetX = m_nFirstLineIndent;
	int maxWidth = 0;
	int offsetX = 0;
	std::vector<RichElement*>::iterator iter;
	for (iter = elementList->begin(); iter != elementList->end(); ++iter)
	{
		RichElement* element = (*iter);

		// if exceed the preferred width, goto another line
		bool bCanNewLine = offsetX != 0 || firstLineOffsetX != 0;
		if((bCanNewLine && offsetX + firstLineOffsetX + element->getSize().width >= m_tDimensions.width)
			|| element->type == RichElement::RichElementType_Return)
		{
			// new line
			m_linesHeight.push_back(0);
			maxWidth = MAX(offsetX, maxWidth);   // record max width
			line++;
			offsetX = 0;
			firstLineOffsetX = 0;
		}

		element->setPosition(ccp(offsetX + firstLineOffsetX, line * (-30)));
		element->line = line;
		int currentLineHeight = m_linesHeight.at(line);
		m_linesHeight.at(line) = MAX(currentLineHeight, element->getSize().height);

		offsetX += (firstLineOffsetX + element->getSize().width);
		firstLineOffsetX = 0;
	}
	maxWidth = MAX(offsetX, maxWidth);   // record max width

	int maxLine = line + 1;
	int currentY = 0;
	for(int i = maxLine - 1; i >= 0; i--)
	{
		for (iter = elementList->begin(); iter != elementList->end(); ++iter)
		{
			RichElement* element = (*iter);
			if(element->line == i)
			{
				if(m_alignment == alignment_vertical_center)
				{
					int y = currentY + (m_linesHeight.at(i) - element->getSize().height) / 2;
					element->setPosition(ccp(element->getPosition().x, y));
				}
				else if(m_alignment == alignment_vertical_bottom)
				{
					element->setPosition(ccp(element->getPosition().x, currentY));
				}
				else
				{
					CCAssert(false, "has not been implemented");
				}
			}
		}
		currentY += m_linesHeight.at(i);
	}

	//this->setContentSize(CCSizeMake(m_tDimensions.width,currentY ));
	this->setContentSize(CCSizeMake(maxWidth,currentY ));

	return true;
}

CCRichLabel* CCRichLabel::createWithString(const char* utf8_str, const CCSize& preferred_size, CCObject* target, SEL_MenuHandler selector, int firstLineIndent, float fontSize,int charLimit, int alignment)
{
	CCRichLabel* label = new CCRichLabel();
	label->m_fFontSize = fontSize;
	label->setFirstLineIndent(firstLineIndent);
	label->setCharNumInOneLabel(charLimit);
	label->m_alignment = alignment;
	label->initWithString(utf8_str, preferred_size, target, selector);
    label->autorelease();
    return label;
}

CCRichLabel* CCRichLabel::create(const char* utf8_str, const CCSize& preferred_size, RichLabelDefinition def, CCObject* target, SEL_MenuHandler selector)
{
	CCRichLabel* label = new CCRichLabel();

	label->m_labelDef.firstLineIndent = def.firstLineIndent;
	label->m_labelDef.alignment = def.alignment;
	label->m_labelDef.charLimit = def.charLimit;
	label->m_labelDef.fontSize = def.fontSize;
	label->m_labelDef.fontColor = def.fontColor;
	label->m_labelDef.strokeEnabled = def.strokeEnabled;
	label->m_labelDef.shadowEnabled = def.shadowEnabled;

	label->m_fFontSize = def.fontSize;
	label->setFirstLineIndent(def.firstLineIndent);
	label->setCharNumInOneLabel(def.charLimit);
	label->m_alignment = def.alignment;
	label->initWithString(utf8_str, preferred_size, target, selector);
    label->autorelease();
    return label;
}

bool CCRichLabel::initWithString(const char* utf8_str, const CCSize& preferred_size, CCObject* target, SEL_MenuHandler selector)
{
	m_pListener = target;
	m_pfnSelector = selector;

	m_tDimensions = CCSizeMake(preferred_size.width, preferred_size.height);

	this->setString(utf8_str);

	return true;
}

int CCRichLabel::getElementSize()
{
	return this->m_elementList->size();
}

CCPoint CCRichLabel::getEndPos()
{
	int size = m_elementList->size();
	if(size <= 0)
	{
		return CCPointZero;
	}

	std::vector<RichElement*>::iterator iter = m_elementList->begin() + size - 1;
    RichElement* pLastElement = *iter;
	return ccp(pLastElement->getPosition().x + pLastElement->getSize().width, pLastElement->getPosition().y);
}

int CCRichLabel::getCharCount()
{
	int cnt = 0;
	std::vector<RichElement*>::iterator iter;
	for (iter = m_elementList->begin(); iter != m_elementList->end(); ++iter)
	{
		RichElement* element = (*iter);
		cnt += element->getCharCount();
	}

	return cnt;
}

RichLabelDefinition& CCRichLabel::getLabelDefinition()
{
	return m_labelDef;
}