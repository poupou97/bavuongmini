
#include "UIScene.h"
#include "../../utils/GameUtils.h"
#include "../../GameAudio.h"

UIScene::UIScene(): 
m_pUiLayer(NULL)
{
}

UIScene::~UIScene()
{
	//cocos2d::extension::CCJsonReader::purgeJsonReader();
	//cocos2d::extension::UIActionManager::purgeUIActionManager();
	//cocos2d::extension::UIHelper::purgeUIHelper();
}

bool UIScene::init()
{
	if (CCLayer::init())
	{
		m_pUiLayer = UILayer::create();
		//m_pUiLayer->scheduleUpdate();
		addChild(m_pUiLayer, 0, TAG_UISCENE_UILAYER);

		return true;
	}
	return false;
}

void UIScene::openAnim()
{
	CCFiniteTimeAction*  action = CCSequence::create(
		CCScaleTo::create(0.15f*1/2,1.1f),
		CCScaleTo::create(0.15f*1/3,1.0f),
		NULL);
	runAction(action);
}

void UIScene::closeAnim()
{
	GameUtils::playGameSound(SCENE_CLOSE, 2, false);
	CCFiniteTimeAction*  action = CCSequence::create(
		CCScaleTo::create(0.15f*1/3,1.1f),
		CCScaleTo::create(0.15f*1/2,1.0f),
		CCRemoveSelf::create(),
		NULL);
	action->setTag(111);
	runAction(action);
}

bool UIScene::isClosing()
{
	if(this->getActionByTag(111))
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool UIScene::resignFirstResponder(CCTouch *touch,CCNode* pNode,bool isTouchContinue, bool animated)
{
	CCPoint location = touch->getLocation();

	CCPoint pos = this->getPosition();
	CCSize size = this->getContentSize();
	CCPoint anchorPoint = this->getAnchorPointInPoints();
	pos = ccpSub(pos, anchorPoint);
	CCRect rect(pos.x, pos.y, size.width, size.height);
	if(rect.containsPoint(location))
	{
		return true;
	}

	// remove it
	UIScene* pUIScene = dynamic_cast<UIScene*>(pNode);
	if(pUIScene == NULL)
		pNode->removeFromParentAndCleanup(true);
	else
	{
		if(animated)
			pUIScene->closeAnim();
		else
			pUIScene->removeFromParent();
	}
	
	if (isTouchContinue)
	{
		return false;
	}
	else
	{
		return true;
	}

}

bool UIScene::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
	if (!isVisible())
	{
		return false;
	}

	for (CCNode *c = this->getParent(); c != NULL; c = c->getParent())
	{
		if (c->isVisible() == false)
		{
			return false;
		}
	}
	
	return true;
}

void UIScene::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
{

}