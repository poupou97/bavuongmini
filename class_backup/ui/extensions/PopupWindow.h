#ifndef _UI_EXTENSIONS_POPUPWINDOW_H_
#define _UI_EXTENSIONS_POPUPWINDOW_H_

#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "UIScene.h"
#include "../../legend_script/CCTutorialIndicator.h"

USING_NS_CC;
USING_NS_CC_EXT;

/*
* PopupWindow是一个阻塞式的弹出框
* 除了该窗体，其他区域都被屏蔽，而不可交互
*/
class PopupWindow : public UIScene
{
public:
	enum PopWindowType
	{
		KtypeNeverRemove = 0,
		KTypeRemoveByTime = 1,
	}; 
public:
	PopupWindow();
	virtual ~PopupWindow();

	static PopupWindow * create(std::string string,int buttonNum,PopWindowType winType, int remainTime);
	bool init(std::string string,int buttonNum,PopWindowType winType, int remainTime);

	virtual void AddcallBackEvent(CCObject* pSender, SEL_CallFuncO pSelectorSure,SEL_CallFuncO pSelectorCancel);

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

    cocos2d::extension::UIButton * buttonSure;
	cocos2d::extension::UIButton * buttonCancel;
	CCLabelTTF * centerTimeLabel;
	void setButtonPosition();

	virtual void update(float delta);

public:
	//教学
	int mTutorialScriptInstanceId;
	//教学
	virtual void registerScriptCommand(int scriptId);

	void addCCTutorialIndicator(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction);
	void removeCCTutorialIndicator();
private:
	CCObject * senderListener;
	SEL_PushEvent senderSelectorSure;
	SEL_PushEvent senderSelectorCancel;
	PopWindowType curWindowType;
	int m_nRemainTime;
	void sureEvent(CCObject * pSender);
	void cancelEvent(CCObject * obj);

    cocos2d::extension::UILabel * timeCenter;
};

#endif