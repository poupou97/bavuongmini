#include "Counter.h"
#include "RichTextInput.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "AppMacros.h"


Counter::Counter(void):
senderListener(NULL),
senderSelector(NULL)
{
	sendNum_=0;
	goodSendNum=0;
}

Counter::~Counter(void)
{
}

Counter * Counter::create()
{
	Counter* box =new Counter();
	if (box && box->init(""))
	{
		box->autorelease();
		return box;
	}
	
	CC_SAFE_DELETE(box);
	return NULL;
	
}

bool Counter::init(std::string test)
{
	if (UIScene::init())
	{
		CCSize s=CCDirector::sharedDirector()->getVisibleSize();
		UIPanel * ppanel = (UIPanel*)GUIReader::shareReader()->widgetFromJsonFile("res_ui/jisuanqi_1.json");
		ppanel->setAnchorPoint(ccp(0,0));
		ppanel->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(ppanel);

		int buttonNumId = 1;
		for (int i=0;i<3;i++)
		{
			for (int j=0;j<3;j++)
			{
				char num[5];
				sprintf(num,"%d",buttonNumId);

				UIButton *extractAnnex=UIButton::create();
				extractAnnex->setTextures("res_ui/enjian_g.png","res_ui/enjian_g.png","");
				extractAnnex->setTouchEnable(true);
				extractAnnex->setPressedActionEnabled(true);
				extractAnnex->setAnchorPoint(ccp(0.5f,0.5f));
				extractAnnex->setWidgetTag(buttonNumId);
				extractAnnex->setPosition(ccp(43+j*61,153- i*50));
				extractAnnex->addReleaseEvent(this,coco_releaseselector(Counter::getAmount));

				UILabel *label_Num=UILabel::create();
				label_Num->setText(num);
				label_Num->setFontSize(25);
				label_Num->setAnchorPoint(ccp(0.5f,0.5f));
				label_Num->setPosition(ccp(0,0));
				extractAnnex->addChild(label_Num);
				m_pUiLayer->addWidget(extractAnnex);
				
				buttonNumId++;
			}
		}
		UIButton * button_clear=UIButton::create();
		button_clear->setTouchEnable(true);
		button_clear->setTextures("res_ui/enjian_y.png","res_ui/enjian_y.png","");
		button_clear->setAnchorPoint(ccp(0.5f,0.5f));
		button_clear->setPosition(ccp(230,213));
		button_clear->setWidgetTag(buttonNumId);
		button_clear->setPressedActionEnabled(true);
		button_clear->addReleaseEvent(this,coco_releaseselector(Counter::deleteNum));
		ppanel->addChild(button_clear);

		UIImageView * deleteSp=UIImageView::create();
		deleteSp->setTexture("res_ui/jisuanqi/jiantou.png");
		deleteSp->setAnchorPoint(ccp(0.5f,0.5f));
		deleteSp->setPosition(ccp(0,0));
		button_clear->addChild(deleteSp);

		UIButton *Button_C=UIButton::create();
		Button_C->setTextures("res_ui/enjian_g.png","res_ui/enjian_g.png","");
		Button_C->setTouchEnable(true);
		Button_C->setAnchorPoint(ccp(0.5f,0.5f));
		Button_C->setPosition(ccp(230,153));
		Button_C->setPressedActionEnabled(true);
		Button_C->addReleaseEvent(this,coco_releaseselector(Counter::clearNum));
		m_pUiLayer->addWidget(Button_C);

		UILabel *label_C=UILabel::create();
		label_C->setText("C");
		label_C->setFontSize(25);
		label_C->setAnchorPoint(ccp(0.5f,0.5f));
		label_C->setPosition(ccp(0,0));
		Button_C->addChild(label_C);

		UIButton *button_Zero=UIButton::create();
		button_Zero->setTextures("res_ui/enjian_g.png","res_ui/enjian_g.png","");
		button_Zero->setTouchEnable(true);
		button_Zero->setFontSize(25);
		button_Zero->setAnchorPoint(ccp(0.5f,0.5f));
		button_Zero->setWidgetTag(0);
		button_Zero->setPosition(ccp(230,103));
		button_Zero->setPressedActionEnabled(true);
		button_Zero->addReleaseEvent(this,coco_releaseselector(Counter::getAmount));
		m_pUiLayer->addWidget(button_Zero);

		UILabel *label_Zero=UILabel::create();
		label_Zero->setText("0");
		label_Zero->setFontSize(25);
		label_Zero->setAnchorPoint(ccp(0.5f,0.5f));
		label_Zero->setPosition(ccp(0,0));
		button_Zero->addChild(label_Zero);

		UIButton *button_Ok=UIButton::create();
		button_Ok->setTextures("res_ui/enjian_y.png","res_ui/enjian_y.png","");
		button_Ok->setTouchEnable(true);
		button_Ok->setAnchorPoint(ccp(0.5f,0.5f));
		button_Ok->setPosition(ccp(230,53));
		button_Ok->setPressedActionEnabled(true);
		button_Ok->addReleaseEvent(this,coco_releaseselector(Counter::sendButton));
		m_pUiLayer->addWidget(button_Ok);

		UILabel *label_Ok=UILabel::create();
		label_Ok->setText("OK");
		label_Ok->setFontSize(25);
		label_Ok->setAnchorPoint(ccp(0.5f,0.5f));
		label_Ok->setPosition(ccp(0,0));
		button_Ok->addChild(label_Ok);

		showNum=CCLabelTTF::create(goodsNum.c_str(),APP_FONT_NAME,28);//
		showNum->setFontSize(25);
		showNum->setAnchorPoint(ccp(0,0));//20 195
		showNum->setPosition(ccp(25,195));
		m_pUiLayer->addChild(showNum);

		UIButton * button_close=(UIButton * )UIHelper::seekWidgetByName(ppanel,"Button_close");
		button_close->setTouchEnable(true);
		button_close->setPressedActionEnabled(true);
		button_close->addReleaseEvent(this,coco_releaseselector(Counter::callBackExit));

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setPosition(CCPointZero);
		this->setContentSize(ppanel->getContentSize());
		return true;
	}	
	return false;
}

void Counter::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void Counter::onExit()
{
	UIScene::onExit();
}


void Counter::getAmount( CCObject * obj )
{
	UITextButton * btn =(UITextButton *)obj;
	int id_= btn->getWidgetTag();

	if (goodsNum.size() >= 9)
	{
		return;
	}

	if (std::atoi(goodsNum.c_str()) <= 0 && id_==0)
	{
		return;
	}
	char num[3];
	sprintf(num,"%d",id_);
	goodsNum.append(num);
	showNum->setString(goodsNum.c_str());
	goodSendNum = std::atoi(goodsNum.c_str());
}

void Counter::clearNum( CCObject * obj )
{
	goodsNum.clear();
	showNum->setString(goodsNum.c_str());
	goodSendNum =std::atoi(goodsNum.c_str());
}

void Counter::deleteNum( CCObject * obj )
{
	if (goodsNum.size()>0)
	{
		goodsNum.erase(goodsNum.end()-1);
		showNum->setString(goodsNum.c_str());

		goodSendNum =std::atoi(goodsNum.c_str());
	}

}

void Counter::sendButton( CCObject * obj )
{
	sendNum_ = goodSendNum;
	if (senderListener && senderSelector)
	{
		(senderListener->*senderSelector)(this);
	}

	/*
	if (sendNum_>0)
	{
		if (senderListener && senderSelector)
		{
			(senderListener->*senderSelector)(this);
		}
	}
	*/

	this->removeFromParentAndCleanup(true);
}
int Counter::getInputNum()
{
	return sendNum_;
}

void Counter::callBackSendNum( CCObject* pSender, SEL_CallFuncO pSelector )
{
	senderListener = pSender;
	senderSelector = pSelector;
}
void Counter::callBackExit( CCObject * obj )
{
	this->closeAnim();
	//removeFromParentAndCleanup(true);
}

bool Counter::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	return true;
}

void Counter::ccTouchMoved( CCTouch *pTouch, CCEvent *pEvent )
{

}

void Counter::ccTouchEnded( CCTouch *pTouch, CCEvent *pEvent )
{

}

void Counter::ccTouchCancelled( CCTouch *pTouch, CCEvent *pEvent )
{

}

void Counter::setShowNumDefault( int counts )
{
	goodSendNum = counts;
	char num[20];
	sprintf(num,"%d",counts);
	showNum->setString(num);
}
