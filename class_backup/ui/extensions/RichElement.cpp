#include "RichElement.h"
#include "CCMoveableMenu.h"

#include "CCRichLabel.h"
#include "AppMacros.h"
#include "../../utils/StrUtils.h"
#include "../../utils/GameUtils.h"

enum {
	kTagButoon = 0,
};

RichElement::RichElement()
{

}

RichElement::~RichElement()
{

}

/////////////////////////////////////////////////////////

RichElementSprite::RichElementSprite()
: m_sprite(NULL)
{
	type = RichElementType_Sprite;
}

RichElementSprite::~RichElementSprite()
{
	CC_SAFE_RELEASE(m_sprite);
	CC_SAFE_RELEASE(m_animation);
}

CCSize RichElementSprite::getSize()
{
	CCAssert(m_sprite != NULL, "sprite should not be NULL");

	return this->m_sprite->getContentSize();
}

void RichElementSprite::setPosition(CCPoint pos)
{
	CCAssert(m_sprite != NULL, "sprite should not be NULL");

	m_sprite->setPosition(pos);
}

CCPoint RichElementSprite::getPosition()
{
	return m_sprite->getPosition();
}

CCNode* RichElementSprite::getNode()
{
	return m_sprite;
}

void RichElementSprite::parse(const char* elementStr)
{
	m_string = elementStr;

	std::string emotionImageName = "res_ui/Expression";
	emotionImageName.append(elementStr);
	emotionImageName.append(".png");
	CCTexture2D *texture = CCTextureCache::sharedTextureCache()->addImage(emotionImageName.c_str());
	CCSize emotionSize = texture->getContentSize();

	// emotion image is organized by sequence
	// each frame's width is same as the height
	int oneFrameSize = emotionSize.height;
	int nFrames = emotionSize.width/oneFrameSize;
    
    //
    // Animation using Sprite BatchNode
    //
	CCSpriteFrame *frame0 = CCSpriteFrame::createWithTexture(texture, CCRectMake(0, 0, oneFrameSize, oneFrameSize));
    m_sprite = CCSprite::createWithSpriteFrame(frame0);
	m_sprite->setAnchorPoint(CCPointZero);
	m_sprite->retain();
    
	// manually add frames to the frame cache
    CCArray* animFrames = CCArray::createWithCapacity(3);
	animFrames->addObject(frame0);
	for(int i = 1; i < nFrames; i++)
	{
		CCSpriteFrame *frame = CCSpriteFrame::createWithTexture(texture, CCRectMake(oneFrameSize*i, 0, oneFrameSize, oneFrameSize));
		animFrames->addObject(frame);
	}
    m_animation = CCAnimation::createWithSpriteFrames(animFrames, 0.2f);
	m_animation->retain();

	animate();

	_container->addChild(m_sprite, 0);
}

void RichElementSprite::animate()
{
	m_sprite->stopAllActions();

    CCAnimate *animate = CCAnimate::create(m_animation);
    CCActionInterval* seq = CCSequence::create( animate,NULL);
    m_sprite->runAction(CCRepeatForever::create( seq ) );
}

int RichElementSprite::getCharCount()
{
	return 1;
}

/////////////////////////////////////////////////////////

structPropIds RichElementButton::parseLink(std::string& linkContent)
{
    structPropIds ids;
    
    int index = linkContent.find_first_of(RICHELEMENT_ITEM_KEYWORD);   // "propid"
    CCAssert(index != -1, "wrong format");
   
    int beginIndex = linkContent.find_first_of("(");
    CCAssert(beginIndex != -1, "wrong format: '(' is missing");
    
    int separatorIndex = linkContent.find(RICHELEMENT_ITEM_SEPARATOR, beginIndex);
    ids.propId = linkContent.substr(beginIndex+1, separatorIndex - beginIndex - 1);
    CCLOG("propId: %s", ids.propId.c_str());
    
    int endIndex = linkContent.find_last_of(")");
    CCAssert(endIndex != -1, "wrong format: ')' is missing");
    
    std::string propinstanceid =linkContent.substr(separatorIndex+1, endIndex - separatorIndex - 1);
    //CCLOG("propinstanceid str: %s", propinstanceid.c_str());
    ids.propInstanceId = atol(propinstanceid.c_str());
    CCLOG("propinstanceid: %ld", ids.propInstanceId);
    
    return ids;
}

std::string RichElementButton::makeLink(const char* propId, long propInstanceId)
{
    // propid(potion01,1001)
    std::string str = RICHELEMENT_ITEM_KEYWORD;
    str.append("(");
    str.append(propId);
    str.append(RICHELEMENT_ITEM_SEPARATOR);
    char longStr[15];
    sprintf(longStr, "%ld", propInstanceId);
    str.append(longStr);
    str.append(")");
    
    return str;
}

std::string RichElementButton::makeColor(int color)
{
    // color(ff00ff)
    std::string str = RICHELEMENT_COLOR_KEYWORD;
    str.append("(");
    
	char colorChars[7];
    ccColor3B newColor = GameUtils::convertToColor3B(color);
	sprintf(colorChars, "%02x%02x%02x", newColor.r, newColor.g, newColor.b);
    
    str.append(colorChars);
    str.append(")");
    
    return str;
}

ccColor3B RichElementButton::parseColor(std::string& colorContent)
{
    int index = colorContent.find_first_of(RICHELEMENT_COLOR_KEYWORD);   // "color"
    CCAssert(index != -1, "wrong format");
    
    int beginIndex = colorContent.find_first_of("(");
    CCAssert(beginIndex != -1, "wrong format: '(' is missing");
    
	// color string, for example "ff0000"
	std::string colorStr = colorContent.substr(beginIndex+1, 6);
	CCAssert(colorStr.size() == 6, "wront format, not a color string");
	std::string red = colorStr.substr(0, 2);
	int r = strtol(red.c_str(), NULL, 16);
	std::string green = colorStr.substr(2, 2);
	int g = strtol(green.c_str(), NULL, 16);
	std::string blue = colorStr.substr(4, 2);
	int b = strtol(blue.c_str(), NULL, 16);
	
    int endIndex = colorContent.find_last_of(")");
    CCAssert(endIndex != -1, "wrong format: ')' is missing");
    
    ccColor3B color = ccc3(r, g, b);
    return color;
}

std::string RichElementButton::makeButton(const char* buttonName, const char* propId, long propInstanceId, int color)
{
    // /tbuttonname,propid(potion01,1001)/
    std::string str="/t";
    str.append(buttonName);
    str.append(",");
    str.append(makeLink(propId, propInstanceId));
    str.append(",");
    str.append(makeColor(color));
    str.append("/");
    
    return str;
}

RichElementButton::RichElementButton()
{
	type = RichElementType_Button;
}

RichElementButton::~RichElementButton()
{
	CC_SAFE_RELEASE(m_pMenu);
}

CCSize RichElementButton::getSize()
{
	CCAssert(m_pMenu != NULL, "menu should not be NULL");


	return m_pMenu->getChildByTag(kTagButoon)->getContentSize();
}

void RichElementButton::setPosition(CCPoint pos)
{
	CCAssert(m_pMenu != NULL, "menu should not be NULL");

	m_pMenu->setPosition(pos);
}

CCPoint RichElementButton::getPosition()
{

	return m_pMenu->getPosition();
	
}

CCNode* RichElementButton::getNode()
{
	return m_pMenu;
}

void RichElementButton::parse(const char* elementStr)
{
	m_string = elementStr;

	// full format: /tButtonName,LinkContent/
	// parse the string to get ButtonName and LinkContent
	int beginIndex = m_string.find_first_of(",");
	buttonName = "";
	linkContent = "";
    colorContent = "edf03d";   // (237, 240, 61)
    
    // first, remove "/t" ( start ) and "/" ( end )
    std::string wholeStr = m_string.substr(2, m_string.size() - chatItemLen - formatFlagCharLen);
    CCLOG("wholeStr: %s", wholeStr.c_str());
    
    std::vector<std::string> parameters = StrUtils::split(wholeStr, ",");
    
    // generate button name
    if(parameters.size() >= 1)
    {
        buttonName = parameters.at(0);
        CCLOG("buttonName: %s", buttonName.c_str());
    }
    
    // generate link content
    if(parameters.size() >= 2)
    {
        linkContent = parameters.at(1);
        CCLOG("linkContent: %s", linkContent.c_str());
    }
    
    // generate button color
    ccColor3B buttonColor = ccc3(237, 240, 61);
    if(parameters.size() >= 3)
    {
        colorContent = parameters.at(2);
        CCLOG("colorContent: %s", colorContent.c_str());
        
        buttonColor = parseColor((colorContent));
    }

	if(buttonName == "")
	{
		m_pMenu = CCMenu::create(NULL);
		return;
	}

	CCMenuItemFont::setFontSize(20);
	CCMenuItemFont::setFontName("Marker Felt");
	CCMenuItemFont *link = CCMenuItemFont::create(buttonName.c_str(), _container->m_pListener, _container->m_pfnSelector);
	link->setZoomScale(1.1f);
	link->setPositionX(link->getContentSize().width/2);
	link->setColor(buttonColor);
	link->setAnchorPoint(ccp(0.5f,0));
	link->setTag(kTagButoon);
	link->setUserData(this);   // record RichElementButton instance, used for button callback
	m_pMenu = CCMoveableMenu::create(link, NULL);
	//m_menu->setAnchorPoint(ccp(0.5f, 0.5f));
	m_pMenu->retain();
	m_pMenu->setEnabled(false);

	_container->addChild(m_pMenu, 10);

	if(_container->m_pListener == NULL && _container->m_pfnSelector == NULL)
	{
		m_pMenu->setEnabled(false);   // the butoon can note be pressed
	}
	else
	{
		m_pMenu->setEnabled(true);
	}
}

int RichElementButton::getCharCount()
{
	return StrUtils::calcCharCount(buttonName.c_str());
}

/////////////////////////////////////////////////////////

RichElementText::RichElementText()
{
	type = RichElementType_Text;
	m_fontDefinition.hasColorFlag = false;   // default false
	m_fontDefinition.fontColor = ccc3(255, 255, 255);   // default color is white
}

RichElementText::RichElementText(ccColor3B& color)
{
	type = RichElementType_Text;
	m_fontDefinition.hasColorFlag = true;
	m_fontDefinition.fontColor = color;
}

RichElementText::~RichElementText()
{
	CC_SAFE_RELEASE(m_label);
}

CCSize RichElementText::getSize()
{
	CCAssert(m_label != NULL, "label should not be NULL");

	return this->m_label->getContentSize();
}

void RichElementText::setPosition(CCPoint pos)
{
	CCAssert(m_label != NULL, "label should not be NULL");

	m_label->setPosition(pos);
}

CCPoint RichElementText::getPosition()
{
	return m_label->getPosition();
}

CCNode* RichElementText::getNode()
{
	return m_label;
}

void RichElementText::parse(const char* elementStr)
{
	m_string = elementStr;
	// Marker Felt, Helvetica, Arial
	// res_ui/font/simhei.ttf
	float fontSize = _container->getFontSize();
	m_label = CCLabelTTF::create(elementStr, APP_FONT_NAME, fontSize, CCSizeZero, kCCTextAlignmentLeft);
	if(_container->getLabelDefinition().strokeEnabled)
	{
		ccColor3B blackColor = ccc3(0,0,0);   // black
		m_label->enableStroke(blackColor, 2.0f);
	}
	if(_container->getLabelDefinition().shadowEnabled)
	{
		ccColor3B blackColor = ccc3(0,0,0);   // black
		m_label->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, blackColor);
	}
	m_label->setAnchorPoint(CCPointZero);
	if(!m_fontDefinition.hasColorFlag)
		m_fontDefinition.fontColor = _container->getLabelDefinition().fontColor;
	m_label->setColor(m_fontDefinition.fontColor);
	m_label->retain();

	_container->addChild(m_label, 0);
}

int RichElementText::getCharCount()
{
	return StrUtils::calcCharCount(m_string.c_str());
}

////////////////////////////////////////////////////

RichElementReturn::RichElementReturn()
{
	type = RichElementType_Return;
}

RichElementReturn::~RichElementReturn()
{
	CC_SAFE_RELEASE(m_node);
}

CCSize RichElementReturn::getSize()
{
	return CCSizeMake(0, 30);   // is it ok?
}

void RichElementReturn::setPosition(CCPoint pos)
{
	CCAssert(m_node != NULL, "node should not be NULL");

	m_node->setPosition(pos);
}

CCPoint RichElementReturn::getPosition()
{
	return m_node->getPosition();
}

CCNode* RichElementReturn::getNode()
{
	return m_node;
}

void RichElementReturn::parse(const char* elementStr)
{
	m_string = elementStr;

	m_node = CCNode::create();
	m_node->retain();

	_container->addChild(m_node, 0);
}

int RichElementReturn::getCharCount()
{
	return 0;
}