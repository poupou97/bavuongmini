// #define COCOS2D_DEBUG   1

#include "RichTextInput.h"

//#include "CCRichLabel.h"
#include "../extensions/CCRichLabel.h"
#include "GameView.h"
#include "../../utils/StaticDataManager.h"
#include "../../utils/StrUtils.h"

//////////////////////////////////////////////////////////////////////////
// local function
//////////////////////////////////////////////////////////////////////////

#define RICHTEXT_INPUT_DEBUG 0

#define RICHTEXT_INPUT_INIT_WIDTH 200
#define RICHTEXT_INPUT_INIT_HEIGHT 50

#define FONT_NAME                       "Thonburi"
#define FONT_SIZE                       36

static CCRect getRect(CCNode * pNode)
{
    CCRect rc;
    rc.origin = pNode->getPosition();
    rc.size = pNode->getContentSize();
    //rc.origin.x -= rc.size.width / 2;
    //rc.origin.y -= rc.size.height / 2;
    return rc;
}

enum {
	kTagRichLabelScrollView = 100,
};

//////////////////////////////////////////////////////////////////////////
// implement KeyboardNotificationLayer
//////////////////////////////////////////////////////////////////////////

CCNode* KeyboardNotificationLayer::s_currentClickingNode = NULL;

KeyboardNotificationLayer::KeyboardNotificationLayer()
: m_pTrackNode(0)
, m_adjustVert(0)
{
    setTouchEnabled(true);
}

void KeyboardNotificationLayer::registerWithTouchDispatcher()
{
    CCDirector* pDirector = CCDirector::sharedDirector();
    pDirector->getTouchDispatcher()->addTargetedDelegate(this, 0, false);
}

#define KEYBOARD_SHOW_HIDE_TIME 0.23f
void KeyboardNotificationLayer::updateInputBoxPosition(bool bAnimated)
{
    if (! m_pTrackNode)
    {
        return;
    }
    
    //
    // modify the whole screen when the keyboard will show
    //
    if(s_currentClickingNode != m_pTrackNode)
        return;
    
    //CCRect rectTracked = getRect(m_pTrackNode);
    
    // get the rect according to world coordinate
    CCSize contentSize = m_pTrackNode->getContentSize();
    CCRect rect = CCRectMake(0, 0, contentSize.width, contentSize.height);
    CCRect rectTracked = CCRectApplyAffineTransform(rect, m_pTrackNode->nodeToWorldTransform());
    
    // if the input box has multi-lines content
    rectTracked.origin.y += m_inputCursorOffsetY;
    
    //CCLOG("TextInputTest:trackingNodeAt(origin:%f,%f, size:%f,%f)",
    //      rectTracked.origin.x, rectTracked.origin.y, rectTracked.size.width, rectTracked.size.height);
    
    // if the keyboard area doesn't intersect with the tracking node area, nothing need to do.
    if (! rectTracked.intersectsRect(m_keyboardEndRect) && m_adjustVert == 0)
    {
        return;
    }
    
    // assume keyboard at the bottom of screen, calculate the vertical adjustment.
    float adjustVert = m_keyboardEndRect.getMaxY() - rectTracked.getMinY();
    m_adjustVert += adjustVert;
    //CCLOG("TextInputTest:needAdjustVerticalPosition(%f)", m_adjustVert);
    
    CCDirector* pDirector = CCDirector::sharedDirector();
    if(bAnimated)
    {
        CCMoveBy *action = CCMoveBy::create(KEYBOARD_SHOW_HIDE_TIME, ccp(0, adjustVert));
        pDirector->getRunningScene()->runAction(action);
    }
    else
    {
        pDirector->getRunningScene()->setPositionY(m_adjustVert);
    }
    
    /*
     // move all the children node of KeyboardNotificationLayer
     CCArray * children = getChildren();
     CCNode * node = 0;
     int count = children->count();
     CCPoint pos;
     for (int i = 0; i < count; ++i)
     {
     node = (CCNode*)children->objectAtIndex(i);
     pos = node->getPosition();
     pos.y += adjustVert;
     node->setPosition(pos);
     }
     */
}

void KeyboardNotificationLayer::keyboardWillShow(CCIMEKeyboardNotificationInfo& info)
{
    CCLOG("TextInputTest:keyboardWillShowAt(origin:%f,%f, size:%f,%f)",
        info.end.origin.x, info.end.origin.y, info.end.size.width, info.end.size.height);
    
    m_keyboardEndRect = info.end;
    

    updateInputBoxPosition(true);
}

void KeyboardNotificationLayer::keyboardWillHide(CCIMEKeyboardNotificationInfo& info)
{
    if(m_adjustVert != 0)
    {
        CCDirector* pDirector = CCDirector::sharedDirector();
        //pDirector->getRunningScene()->setPositionY(0);
        CCMoveBy *action = CCMoveBy::create(KEYBOARD_SHOW_HIDE_TIME, ccp(0, -m_adjustVert));
        pDirector->getRunningScene()->runAction(action);
    }
    
    /*
    if(m_adjustVert != 0)
    {
        // move all the children node of KeyboardNotificationLayer
        CCArray * children = getChildren();
        CCNode * node = 0;
        int count = children->count();
        CCPoint pos;
        for (int i = 0; i < count; ++i)
        {
            node = (CCNode*)children->objectAtIndex(i);
            pos = node->getPosition();
            pos.y -= m_adjustVert;
            node->setPosition(pos);
        }
    }
     */
    
    m_adjustVert = 0;
    
    m_keyboardEndRect = CCRectMake(0, 0, 0, 0);
}

// CCLayer function

bool KeyboardNotificationLayer::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
	if (!isVisible())
	{
		return false;
	}

	for (CCNode *c = this->m_pParent; c != NULL; c = c->getParent())
	{
		if (c->isVisible() == false)
		{
			return false;
		}
	}

    CCLOG("++++++++++++++++++++++++++++++++++++++++++++");
    m_beginPos = pTouch->getLocation();    
    return true;
}

void KeyboardNotificationLayer::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
{
    if (! m_pTrackNode)
    {
        return;
    }
    
    CCPoint endPos = pTouch->getLocation();    

    float delta = 5.0f;
    if (::abs(endPos.x - m_beginPos.x) > delta
        || ::abs(endPos.y - m_beginPos.y) > delta)
    {
        // not click
        m_beginPos.x = m_beginPos.y = -1;
        return;
    }

    // decide the trackNode is clicked.
    CCRect rect;
    CCPoint point = convertTouchToNodeSpaceAR(pTouch);
    //CCLOG("KeyboardNotificationLayer:clickedAt(%f,%f)", point.x, point.y);

    rect = getRect(m_pTrackNode);
    //CCLOG("KeyboardNotificationLayer:TrackNode at(origin:%f,%f, size:%f,%f)",
    //    rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
    bool bClicked = rect.containsPoint(point);
    if(!bClicked)
    {
        bClicked = isClickTrackNode(rect, point);
    }

    this->onClickTrackNode(bClicked);
}

//////////////////////////////////////////////////////////////////////////
// implement RichTextInputBox
//////////////////////////////////////////////////////////////////////////

RichTextInputBox::RichTextInputBox()
: m_nCharLimit(100)
, m_nInputBoxWidth(RICHTEXT_INPUT_INIT_WIDTH)
, m_nInputBoxHeight(RICHTEXT_INPUT_INIT_HEIGHT)
, m_direction(kCCRichInputDirectionHorizontal)
, m_inputMode(kCCInputModeFullText)
, m_bMultiLines(false)
{
}

void RichTextInputBox::onClickTrackNode(bool bClicked)
{
    CCTextFieldTTF * pTextField = (CCTextFieldTTF*)m_pTrackNode;
    if (bClicked)
    {
        s_currentClickingNode = pTextField;   // record the current clicking input box
        
        // TextFieldTTFTest be clicked
        CCLOG("TextFieldTTFActionTest:CCTextFieldTTF attachWithIME");
		pTextField->attachWithIME(m_inputMode == kCCInputModeAppendOnly);
		m_pCursorSprite->setVisible(true);
		// blink the cursor
		CCRepeatForever *repeat = CCRepeatForever::create( CCBlink::create(2, 2) );
		m_pCursorSprite->runAction( repeat);
    }
    else
    {
        // TextFieldTTFTest not be clicked
        CCLOG("TextFieldTTFActionTest:CCTextFieldTTF detachWithIME");
        pTextField->detachWithIME();
		m_pCursorSprite->setVisible(false);
		m_pCursorSprite->stopAllActions();
		
    }
}

bool RichTextInputBox::isClickTrackNode(const CCRect& trackNodeRect, const CCPoint& touchPoint)
{
    CCRect rc = CCRectMake(trackNodeRect.origin.x, trackNodeRect.origin.y - (this->getInputBoxHeight() - trackNodeRect.size.height), trackNodeRect.size.width, this->getInputBoxHeight());
    
    return rc.containsPoint(touchPoint);
}

void RichTextInputBox::onEnter()
{
    KeyboardNotificationLayer::onEnter();

    // add CCTextFieldTTF
    CCSize s = CCDirector::sharedDirector()->getVisibleSize();

    m_pTextField = CCTextFieldTTF::textFieldWithPlaceHolder("<click here for input.......................>",
        FONT_NAME,
        FONT_SIZE);
    addChild(m_pTextField);

    m_pTextField->setDelegate(this);
    
	m_pTextField->setPosition(ccp(0,0));
	m_pTextField->setAnchorPoint(ccp(0,0));

	//m_pTextField->setString("abcdefghijklmnopqrstuvwxyz");   // m_pTextField must have some chars, otherwise, onTextFieldDeleteBackward will not be called
	m_pTextField->setString("");   // m_pTextField must have some chars, otherwise, onTextFieldDeleteBackward will not be called
	m_pTextField->setContentSize(CCSizeMake(m_nInputBoxWidth, RICHTEXT_INPUT_INIT_HEIGHT));
    m_pTrackNode = m_pTextField;

	// input content
    CCSize richLabelSize = CCSizeMake(2000, RICHTEXT_INPUT_INIT_HEIGHT);   // one line style
	if(m_bMultiLines)   // inputbox allows multilines
	{
		richLabelSize.setSize(m_nInputBoxWidth, m_nInputBoxHeight);
	}
	m_pRichLabel = CCRichLabel::createWithString("", richLabelSize, NULL, NULL);
	m_pRichLabel->setCharNumInOneLabel(1);
	m_pRichLabel->setContentSize(m_pTextField->getContentSize());
	m_pRichLabel->setPosition(ccp(0,0));
	m_pRichLabel->setAnchorPoint(ccp(0,0));
	//addChild(m_pRichLabel);

	// input cursor
	m_pCursorSprite = CCSprite::create("images/input_cursor.png");
    m_pCursorSprite->setScaleY(0.9f);
	m_pCursorSprite->setAnchorPoint(CCPointZero);
	m_pCursorSprite->setVisible(false);
	//m_pTextField->addChild(m_pCursorSprite);

	CCScrollView * scrollView_RichLabel = CCScrollView::create(CCSizeMake(m_nInputBoxWidth, m_pTextField->getContentSize().height));
	scrollView_RichLabel->setAnchorPoint(CCPointZero);
	scrollView_RichLabel->setContentOffset(ccp(0,0));
	scrollView_RichLabel->setTouchEnabled(true);
    if(m_direction == kCCRichInputDirectionHorizontal)
        scrollView_RichLabel->setDirection(kCCScrollViewDirectionHorizontal);
    else
        scrollView_RichLabel->setDirection(kCCScrollViewDirectionVertical);
	scrollView_RichLabel->setPosition(ccp(0,0));
	scrollView_RichLabel->setDelegate(this);
	scrollView_RichLabel->setBounceable(true);
	scrollView_RichLabel->setClippingToBounds(true);
	// if do not setContainer(), must setContentSize() manully
	// if setContainer(), the scrolling behavior of the CCScrollView is affected by this container
	//scrollView_RichLabel->setContainer(m_pRichLabel);
	addChild(scrollView_RichLabel);
	scrollView_RichLabel->setTag(kTagRichLabelScrollView);
	scrollView_RichLabel->addChild(m_pRichLabel);   // add to CCScrollView
	scrollView_RichLabel->addChild(m_pCursorSprite);   // add to CCScrollView

#if RICHTEXT_INPUT_DEBUG
	// insert button
	CCMenuItemFont::setFontSize(30);
	CCMenuItemFont::setFontName("Marker Felt");
	CCMenuItemFont *link = CCMenuItemFont::create("insert emote", this, menu_selector(RichTextInputBox::menuInsertEmotionCallback));	
	link->setZoomScale(1.05f);
	link->setPositionX(link->getContentSize().width/2);
	link->setColor(ccc3(255, 0, 0));
	link->setAnchorPoint(ccp(0.5f,0));
	CCMenu* menu = CCMenu::create(link, NULL);
	menu->setPosition(ccp(600, 100));
	m_pTextField->addChild(menu);
	
	link = CCMenuItemFont::create("insert item", this, menu_selector(RichTextInputBox::menuInsertItemCallback));
	link->setZoomScale(1.05f);
	link->setPositionX(link->getContentSize().width/2);
	link->setColor(ccc3(255, 0, 0));
	link->setAnchorPoint(ccp(0.5f,0));
	menu = CCMenu::create(link, NULL);
	menu->setPosition(ccp(600, 140));
	m_pTextField->addChild(menu);
#endif
}

void RichTextInputBox::onExit()
{
    KeyboardNotificationLayer::onExit();
}

void RichTextInputBox::setInputMode(int mode)
{
	m_inputMode = mode;
}

// CCTextFieldDelegate protocol
bool RichTextInputBox::onTextFieldAttachWithIME(CCTextFieldTTF * pSender)
{
    return false;
}

bool RichTextInputBox::onTextFieldDetachWithIME(CCTextFieldTTF * pSender)
{
    return false;
}

void RichTextInputBox::sychronizeTextFieldTTF()
{
	// sychronize the CCTextFieldTTF's content
	const char* wholeString = m_pRichLabel->getString();
	if(wholeString != NULL)
		m_pTextField->setString(wholeString);
	else
		m_pTextField->setString("");
	// this CCTextFieldTTF is a delegate, so always use the init content size 
	m_pTextField->setContentSize(CCSizeMake(m_nInputBoxWidth, RICHTEXT_INPUT_INIT_HEIGHT));

}

bool RichTextInputBox::onTextFieldInsertText(CCTextFieldTTF * pSender, const char * text, int nLen)
{
    // if insert enter, treat as default to detach with ime
    if ('\n' == *text)
    {
        return false;
    }

	std::string sInsert(text, nLen);

	//const char* _str = m_pRichLabel->getString();
	//int charCount = _str != NULL ? StrUtils::calcCharCount(_str) : 0;
	//int _count = StrUtils::calcCharCount(text);
	int charCount = m_pRichLabel->getCharCount();
	CCRichLabel* tmpRichLabel = CCRichLabel::createWithString(sInsert.c_str(), CCSizeMake(2000, RICHTEXT_INPUT_INIT_HEIGHT), NULL, NULL);
	int _count = tmpRichLabel->getCharCount();
	if(charCount + _count > m_nCharLimit)
	{
		//add limit input 
		const char * secondStr = StringDataManager::getString("limitInputmax");
		GameView::getInstance()->showAlertDialog(secondStr);
		return true;
	}

	//std::string currentString(this->m_pTextField->getString());
	//std::string currentString(m_pRichLabel->getString());
	//currentString.append(sInsert);
    //m_pRichLabel->setString(currentString.c_str());

	m_pRichLabel->appendString(sInsert.c_str());
	sychronizeTextFieldTTF();

	adjustInputCursorPos();

    // if the textfield's char count more than m_nCharLimit, doesn't insert text anymore.
    if (pSender != NULL && pSender->getCharCount() >= m_nCharLimit)
    {
        return true;
    }

    return true;
}

bool RichTextInputBox::onTextFieldDeleteBackward(CCTextFieldTTF * pSender, const char * delText, int nLen)
{
	int size = m_pRichLabel->getElementSize();
	if(size > 0)
	{
		m_pRichLabel->deleteElementAtIdx(size-1);

		sychronizeTextFieldTTF();

		adjustInputCursorPos();
	}
    return true;
}

bool RichTextInputBox::onDraw(CCTextFieldTTF * pSender)
{
#if RICHTEXT_INPUT_DEBUG
	ccDrawLine(m_pTextField->getPosition(), 
		ccp(m_pTextField->getPositionX()+m_pTextField->getContentSize().width, m_pTextField->getPositionY()));
	//ccDrawLine(ccp(0, 30), ccp(800, 30));
#endif

    return true;   // don't draw m_pTextField
	//return false;
}

void RichTextInputBox::setDirection(int dir)
{
    m_direction = dir;
}
void RichTextInputBox::setMultiLinesMode(bool bMultiLins)
{
	m_bMultiLines = bMultiLins;
}

void RichTextInputBox::menuInsertEmotionCallback(CCObject* pSender)
{
	//m_pRichLabel->appendString("/e02");
	onTextFieldInsertText(NULL, "/e02", 4);
}
void RichTextInputBox::menuInsertItemCallback(CCObject* pSender)
{
	//m_pRichLabel->appendString("/t[Sword]/");
	onTextFieldInsertText(NULL, "/t[Sword]/", 10);
}

const char* RichTextInputBox::getInputString()
{
	return m_pRichLabel->getString();
}

void RichTextInputBox::deleteAllInputString()
{
	int size = m_pRichLabel->getElementSize();
	if(size>0)
	{
		for(int i = size;i>0;i--)
		{
			m_pRichLabel->deleteElementAtIdx(i-1);
		}
	}
	//set default point ccp(0,0)
	m_pCursorSprite->setPosition(ccp(0,0));

}

//CCRichLabel* RichTextInputBox::getTextTTF()
//{
//	return m_pRichLabel;
//}

void RichTextInputBox::setCharLimit(int limit)
{
	m_nCharLimit = limit;
}

void RichTextInputBox::scrollViewDidScroll( CCScrollView* view )
{

}

void RichTextInputBox::scrollViewDidZoom( CCScrollView* view )
{

}

void RichTextInputBox::adjustInputCursorPos()
{
	CCScrollView* view = (CCScrollView*)this->getChildByTag(kTagRichLabelScrollView);
	//CCSprite* cursor = (CCSprite*)view->getChildByTag(kTagInputCursorSprite);
	CCSprite* cursor = m_pCursorSprite;
	int cursorWidth = cursor->getContentSize().width;   // the width of the cursor

	// 1. adjust the scroll view's contentsize
	int firstLineHeight = 0;
	if(m_pRichLabel->getLinesHeight().size() > 0)
	{
		firstLineHeight = m_pRichLabel->getLinesHeight().at(0);
	}
	int maxViewHeight = getInputBoxHeight() - (RICHTEXT_INPUT_INIT_HEIGHT - firstLineHeight);
	float _height = m_pRichLabel->getContentSize().height;
	if(_height > maxViewHeight)
 		_height = maxViewHeight;
	view->setContentSize(CCSizeMake(m_pRichLabel->getContentSize().width + cursorWidth, _height));
    CCSize scrollViewSize = view->getViewSize();
    view->setViewSize(CCSizeMake(scrollViewSize.width, _height));
    
	// 2. according to the cursor position, adjust scrollview's offset
	CCPoint endPos = m_pRichLabel->getEndPos();
	CCSize inputboxSize = m_pTextField->getContentSize();
	if(endPos.x > inputboxSize.width)
	{
		//m_pRichLabel->setPositionX(m_pTextField->getPositionX() - (endPos.x - inputboxSize.width));
		view->setContentOffset(ccp((m_pTextField->getPositionX()) -(endPos.x + cursorWidth - inputboxSize.width), 0));
	}
	else
	{
		//m_pRichLabel->setPositionX(m_pTextField->getPositionX());
		view->setContentOffset(ccp(0,0));
	}

	// 3. adjust the input cursor's position
	cursor->setPositionX(m_pRichLabel->getPositionX() + endPos.x);
 
    // for multi-lines inputbox
	if(m_pRichLabel->getLinesHeight().size() > 0)
	{
		m_inputCursorOffsetY = firstLineHeight-m_pRichLabel->getContentSize().height;
	}
	else
	{
		m_inputCursorOffsetY = 0;
	}
	float minCursorOffsetY = RICHTEXT_INPUT_INIT_HEIGHT - this->getInputBoxHeight();
	if(m_inputCursorOffsetY < minCursorOffsetY)
		view->setPositionY(minCursorOffsetY);
	else
		view->setPositionY(m_inputCursorOffsetY);
    
    updateInputBoxPosition(false);   // now, must use false
}
