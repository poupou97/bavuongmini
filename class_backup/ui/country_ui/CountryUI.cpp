#include "CountryUI.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../utils/StaticDataManager.h"
#include "AppMacros.h"

#define COUNTRY_1_TAG 1
#define COUNTRY_2_TAG 2
#define COUNTRY_3_TAG 3
#define HIGHLIGHT_TAG 55
#define HIGHLIGHT_TRANS_TIME 0.5f
#define ROLECOLOR 150,150,150
#define TABLE_LIGHT_TRANS_TIME 2.0f

int CountryUI::mCountryFlag = 3;

CountryUI::CountryUI(void)
{
}


CountryUI::~CountryUI(void)
{
}

CountryUI * CountryUI::create()
{
	CountryUI * countryui=new CountryUI();
	if (countryui && countryui->init())
	{
		countryui->autorelease();
		return countryui;
	}
	CC_SAFE_DELETE(countryui);
	return NULL;
}

bool CountryUI::init()
{
	if (UIScene::init())
	{
		CCSize s=CCDirector::sharedDirector()->getVisibleSize();

		UILayer * layer=UILayer::create();
		layer->ignoreAnchorPointForPosition(false);
		layer->setAnchorPoint(ccp(0.5f,0.5f));
		layer->setPosition(ccp(s.width/2,s.height/2));
		layer->setContentSize(CCSizeMake(UI_DESIGN_RESOLUTION_WIDTH,UI_DESIGN_RESOLUTION_HEIGHT));
		layer->setTag(255);
		addChild(layer);

		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("newcommerstory/zhezhao.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(s);
		mengban->setAnchorPoint(ccp(0,0));
		mengban->setPosition(ccp(0,0));
		mengban->setOpacity(240);
		m_pUiLayer->addWidget(mengban);
		
		UIImageView *countrySelect = UIImageView::create();
		countrySelect->setTexture("res_ui/country_icon/xzgj.png");
		countrySelect->setAnchorPoint(ccp(0.5f,0.5f));
		countrySelect->setPosition(ccp(400,430));
		layer->addWidget(countrySelect);
		
		UIImageView *right = UIImageView::create();
		right->setTexture("res_ui/country_icon/hw.png");
		right->setAnchorPoint(ccp(0.5f,0.5f));
		right->setPosition(ccp(530,430));
		layer->addWidget(right);
		
		UIImageView *left = UIImageView::create();
		left->setTexture("res_ui/country_icon/hw.png");
		left->setAnchorPoint(ccp(0.5f,0.5f));
		left->setPosition(ccp(270,430));
		left->setRotation(180);
		layer->addWidget(left);

		CCAction* action = CCRepeatForever::create(
								CCSpawn::create(
									CCSequence::create(
										CCFadeTo::create(TABLE_LIGHT_TRANS_TIME, 100), 
										CCFadeTo::create(TABLE_LIGHT_TRANS_TIME, 255), 
										NULL),
									CCSequence::create(
										CCScaleTo::create(TABLE_LIGHT_TRANS_TIME, 2.2f, 2.05f), 
										CCScaleTo::create(TABLE_LIGHT_TRANS_TIME, 2.1f, 1.95f), 
										NULL),
									NULL));
		cocos2d::extension::UIButton *country1 = cocos2d::extension::UIButton::create();
		country1->setTouchEnable(true);
		country1->loadTextures("res_ui/country_icon/weiguo.png", "res_ui/country_icon/weiguo.png", "");
		country1->setTag(COUNTRY_1_TAG);
		country1->setPosition(ccp(160,240));
		//country1->setScale(0.6f);
		country1->addReleaseEvent(this, coco_releaseselector(CountryUI::menuSelectCountry));
		country1->setPressedActionEnabled(true);

		cocos2d::extension::UIImageView * highLightFrame1 = cocos2d::extension::UIImageView::create();
		highLightFrame1->setTexture("res_ui/country_icon/lightb.png");
		highLightFrame1->setPosition(ccp(0.5f, 0.5f));
		highLightFrame1->setZOrder(-1);
		highLightFrame1->setVisible(false);
		highLightFrame1->setScaleX(2.15f);
		highLightFrame1->setScaleY(2.0f);
		highLightFrame1->setTag(HIGHLIGHT_TAG);
		country1->addChild(highLightFrame1);
		
		highLightFrame1->runAction(action);

		cocos2d::extension::UIButton *country2 = cocos2d::extension::UIButton::create();
		country2->setTouchEnable(true);
		country2->loadTextures("res_ui/country_icon/shuguo.png", "res_ui/country_icon/shuguo.png", "");
		country2->setTag(COUNTRY_2_TAG);
		country2->setPosition(ccp(400,240));
		//country2->setScale(0.6f);
		country2->addReleaseEvent(this, coco_releaseselector(CountryUI::menuSelectCountry));
		country2->setPressedActionEnabled(true);

		cocos2d::extension::UIImageView * highLightFrame2 = cocos2d::extension::UIImageView::create();
		highLightFrame2->setTexture("res_ui/country_icon/lightb.png");
		highLightFrame2->setPosition(ccp(0.5f, 0.5f));
		highLightFrame2->setZOrder(-1);
		highLightFrame2->setVisible(false);
		highLightFrame2->setScaleX(2.15f);
		highLightFrame2->setScaleY(2.0f);
		highLightFrame2->setTag(HIGHLIGHT_TAG);
		country2->addChild(highLightFrame2);

		highLightFrame2->runAction(action);

		cocos2d::extension::UIButton *country3 = cocos2d::extension::UIButton::create();
		country3->setTouchEnable(true);
		country3->loadTextures("res_ui/country_icon/wuguo.png", "res_ui/country_icon/wuguo.png", "");
		country3->setTag(COUNTRY_3_TAG);
		country3->setPosition(ccp(640,240));
		//country3->setScale(0.6f);
		country3->addReleaseEvent(this, coco_releaseselector(CountryUI::menuSelectCountry));
		country3->setPressedActionEnabled(true);

		cocos2d::extension::UIImageView * highLightFrame3 = cocos2d::extension::UIImageView::create();
		highLightFrame3->setTexture("res_ui/country_icon/lightb.png");
		highLightFrame3->setPosition(ccp(0.5f, 0.5f));
		highLightFrame3->setZOrder(-1);
		highLightFrame3->setVisible(false);
		highLightFrame3->setScaleX(2.15f);
		highLightFrame3->setScaleY(2.0f);
		highLightFrame3->setTag(HIGHLIGHT_TAG);
		country3->addChild(highLightFrame3);

		highLightFrame3->runAction(action);

		layer->addWidget(country1);
		layer->addWidget(country2);
		layer->addWidget(country3);

		updateCountryButton(mCountryFlag);
		
		UIButton *enter = UIButton::create();
		enter->setTouchEnable(true);
		enter->loadTextures("res_ui/new_button_13.png", "res_ui/new_button_13.png", "");
		enter->setPosition(ccp(400,60));
		enter->addReleaseEvent(this, coco_releaseselector(CountryUI::enterCallBack));
		enter->setPressedActionEnabled(true);
		layer->addWidget(enter);

		UILabel * goldSum = UILabel::create();
        goldSum->setStrokeEnabled(true);
		goldSum->setText(StringDataManager::getString("CountrySelect"));
		goldSum->setAnchorPoint(ccp(0.5f,0.5f));
		goldSum->setFontName(APP_FONT_NAME);
		goldSum->setFontSize(24);
		goldSum->setPosition(ccp(0, -8));
		enter->addChild(goldSum);
		
		/*
		CCTutorialParticle * tutorialParticle = CCTutorialParticle::create("xuanzhuan.plist",48,72);
		tutorialParticle->setPosition(ccp(0, -36));
		tutorialParticle->setAnchorPoint(ccp(0.5f, 0.5f));
		enter->addCCNode(tutorialParticle);
	*/
		
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(s);
		return true;
	}
	return false;
}

bool CountryUI::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
	return true;
}
void CountryUI::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
{

}
void CountryUI::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
{

}
void CountryUI::ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent)
{

}

void CountryUI::enterCallBack(CCObject* pSender)
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5103, (void*)&mCountryFlag);
}

void CountryUI::menuSelectCountry(CCObject* pSender)
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

	UIButton * btn = (UIButton *)pSender;
	mCountryFlag = btn->getTag();

	updateCountryButton(mCountryFlag);
}

void CountryUI::updateCountryButton(int country)
{
	CCAction* action = CCRepeatForever::create(
								CCSpawn::create(
									CCSequence::create(
										CCFadeTo::create(TABLE_LIGHT_TRANS_TIME, 100), 
										CCFadeTo::create(TABLE_LIGHT_TRANS_TIME, 255), 
										NULL),
									CCSequence::create(
										CCScaleTo::create(TABLE_LIGHT_TRANS_TIME, 2.2f, 2.05f), 
										CCScaleTo::create(TABLE_LIGHT_TRANS_TIME, 2.1f, 1.95f), 
										NULL),
									NULL));

	UILayer* layer = (UILayer*)getChildByTag(255);
	for(int i = 1; i <= 3; i++)
	{
		UIButton * btn = (UIButton *)layer->getWidgetByTag(i);
		UIImageView* highlightframe = (UIImageView*)btn->getChildByTag(HIGHLIGHT_TAG);
		if(i == country)
		{
			btn->setColor(ccc3(255, 255, 255));
			btn->setScale(1.0f);
			highlightframe->setVisible(true);
			highlightframe->runAction(action);
		}
		else
		{
			btn->setColor(ccc3(ROLECOLOR));
			btn->setScale(0.9f);
			highlightframe->setVisible(false);
		}
	}
}
