#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;
class CountryUI:public UIScene
{
public:
	CountryUI(void);
	~CountryUI(void);

	static CountryUI *create();
	bool init();
	
	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

	void menuSelectCountry(CCObject* pSender);
	void updateCountryButton(int country);
	void enterCallBack(CCObject* pSender);

	static int mCountryFlag;
};

