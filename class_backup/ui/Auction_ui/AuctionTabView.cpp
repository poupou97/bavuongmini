#include "AuctionTabView.h"
#include "../extensions/CCMoveableMenu.h"
#include "AuctionUi.h"
#include "GameView.h"
#include "AuctionTabViewCell.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/CAuctionInfo.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "AuctionGoodsInfo.h"
#include "../../ui/extensions/RichTextInput.h"
#include "../../ui/extensions/CCRichLabel.h"
#include "../../utils/StrUtils.h"


AuctionTabView::AuctionTabView(void)
{
	selectGoodsId=0;
}


AuctionTabView::~AuctionTabView(void)
{
}

AuctionTabView * AuctionTabView::create()
{
	AuctionTabView * auctionTabview =new AuctionTabView();
	auctionTabview->autorelease();
	return auctionTabview;
}

bool AuctionTabView::init()
{
	if (UIScene::init())
	{

		tabelView =CCTableView::create(this,CCSizeMake(465,320));
		tabelView->setSelectedEnable(true);
		tabelView->setSelectedScale9Texture("res_ui/highlight.png", CCRectMake(25, 24, 1, 1), ccp(0,0));
		tabelView->setPressedActionEnabled(true);
		tabelView->setDirection(kCCScrollViewDirectionVertical);
		tabelView->setAnchorPoint(ccp(0,0));
		tabelView->setPosition(ccp(0,0));
		tabelView->setDelegate(this);
		tabelView->setVerticalFillOrder(kCCTableViewFillTopDown);
		addChild(tabelView,1,AUCTIONCCTABVIEW);
		tabelView->reloadData();

		this->setContentSize(CCSizeMake(465,320));
		return true;
	}
	return false;
}

void AuctionTabView::onEnter()
{
	UIScene::onEnter();
}

void AuctionTabView::onExit()
{
	UIScene::onExit();
}


void AuctionTabView::tableCellTouched( cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell )
{
	CCLOG("cell->getIdx() = %d",cell->getIdx());
	selectGoodsId=cell->getIdx();
	AuctionUi * auctionui =(AuctionUi *)this->getParent()->getParent();
	auctionui->goodsid = GameView::getInstance()->auctionSourceVector.at(selectGoodsId)->id();
	
	GoodsInfo * goods_ =new GoodsInfo();
	goods_->CopyFrom(GameView::getInstance()->auctionSourceVector.at(selectGoodsId)->goods());  

	AuctionGoodsInfo * purchase_ =AuctionGoodsInfo::create(goods_,auctionui->tabsType);
	purchase_->ignoreAnchorPointForPosition(false);
	purchase_->setAnchorPoint(ccp(0.5f,0.5f));
	//purchase_->setPosition(ccp(winsize.width/2,winsize.height/2));
	GameView::getInstance()->getMainUIScene()->addChild(purchase_);

	delete goods_;
}

cocos2d::CCSize AuctionTabView::tableCellSizeForIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	return CCSizeMake(465,70);
}

cocos2d::extension::CCTableViewCell* AuctionTabView::tableCellAtIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	CCString * string =CCString::createWithFormat("%d",idx);	
	AuctionTabViewCell *cell =(AuctionTabViewCell *)table->dequeueCell();

	cell=AuctionTabViewCell::create(idx);

	AuctionUi * auction = (AuctionUi*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagAuction);
	if (idx == GameView::getInstance()->auctionSourceVector.size() - 3 && auction->tabsType == 0)
	{
		if (auction->hasMorePage == true )
		{
			auction->setpageindex++;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1801,auction);
		}
	}
	return cell;
}

unsigned int AuctionTabView::numberOfCellsInTableView( cocos2d::extension::CCTableView *table )
{
	int num = GameView::getInstance()->auctionSourceVector.size();
	AuctionUi * auctionUi_ = (AuctionUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagAuction);
	if (auctionUi_->tabsType == 1 && num <= 0)
	{
		auctionUi_->LabelBMFont_selfConsignIsNull->setVisible(true);
	}else
	{
		auctionUi_->LabelBMFont_selfConsignIsNull->setVisible(false);
	}
	CCRichLabel * rl_des = (CCRichLabel *)auctionUi_->layer_->getChildByTag(123465);
	if (rl_des!= NULL)
	{
		rl_des->removeFromParentAndCleanup(true);
	}

	if (auctionUi_->tabsType == 0 && num <= 0)
	{
		std::string infoStr_ = auctionUi_->setname;
		if (strcmp(infoStr_.c_str(),"") !=0)
		{
			const char * str1 = StringDataManager::getString("auction_checkIsNull_begin");
			std::string str3 = StrUtils::applyColor(infoStr_.c_str(),ccc3(255,0,0));
			const char * str2 = StringDataManager::getString("auction_checkIsNull_end");

			std::string temp_ = str1;
			temp_.append(str3.c_str());
			temp_.append(str2);

			RichLabelDefinition def;
			def.strokeEnabled = false;
			
			rl_des = CCRichLabel::create(temp_.c_str(),CCSizeMake(465,20),def);
			rl_des->setAnchorPoint(ccp(0.5f,0));
			rl_des->setPosition(ccp(306,210));
			rl_des->setTag(123465);
			auctionUi_->layer_->addChild(rl_des);
		}
	}

	return num;
}

void AuctionTabView::ccTouchMoved( CCTouch *pTouch, CCEvent *pEvent )
{

}

void AuctionTabView::ccTouchEnded( CCTouch *pTouch, CCEvent *pEvent )
{

}
void AuctionTabView::ccTouchCancelled( CCTouch *pTouch, CCEvent *pEvent )
{
}

void AuctionTabView::scrollViewDidScroll( cocos2d::extension::CCScrollView * view )
{
}

void AuctionTabView::scrollViewDidZoom( cocos2d::extension::CCScrollView* view )
{

}


