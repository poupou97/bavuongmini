#include "AuctionUi.h"
#include "../extensions/UITab.h"
#include "../extensions/RichTextInput.h"
#include "GameView.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "WhereExList.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/CAuctionInfo.h"
#include "../extensions/Counter.h"
#include "../backpackscene/PacPageView.h"
#include "AuctionTabView.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../gamescene_state/MainScene.h"
#include "GameAudio.h"
#include "../../utils/GameUtils.h"
#include "../extensions/FloderItem.h"
#include "../../utils/StrUtils.h"

AuctionUi::AuctionUi(void)
{
	buyandqualityLift=false;
	image_levelLift=false;
	image_remaintimLift=false;
	image_priceupLift=false;

	isConsignGoods=false;
	tabsType=0;

	hasMorePage=false;
	///////
	setAuctiontype=0;
	setAuctionsubtype=0;
	setprofession=0;
	setlevel=0;
	setquality=0;
	setpageindex=1;
	setorderby=0;
	setorderbydesc=0;
	setMoney = 0;

	setPrice=0;
	setPropNum=0;
	setPriceType =1;
	//setCurrency=1;
	//success consign goods
	successConsign=0;

	isSelectChat =false;

	curMaxConsignGoodsNum = 0;
}


AuctionUi::~AuctionUi(void)
{
	std::vector<CAuctionInfo*>::iterator iter;
	for (iter=GameView::getInstance()->auctionSourceVector.begin();iter != GameView::getInstance()->auctionSourceVector.end();iter++)
	{
		delete *iter;
	}
	GameView::getInstance()->auctionSourceVector.clear();

	for(iter=GameView::getInstance()->auctionConsignVector.begin();iter!=GameView::getInstance()->auctionConsignVector.end();iter++)
	{
		delete *iter;
	}
	GameView::getInstance()->auctionConsignVector.clear();

	for(iter=GameView::getInstance()->selfConsignVector.begin();iter!=GameView::getInstance()->selfConsignVector.end();iter++)
	{
		delete *iter;
	}
	GameView::getInstance()->selfConsignVector.clear();

	std::vector<CAuctionType *>::iterator typeV;
	for (typeV =acutionTypevector.begin();typeV!= acutionTypevector.end();typeV++ )
	{
		delete * typeV;
	}
	acutionTypevector.clear();

	this->getbackAuction(NULL);
}

AuctionUi * AuctionUi::create()
{
	AuctionUi * auction=new AuctionUi();
	if (auction && auction->init())
	{
		auction->autorelease();
		return auction;
	}
	CC_SAFE_DELETE(auction);
	return NULL;
}

bool AuctionUi::init()
{
	if (UIScene::init())
	{

		CCSize size=CCDirector::sharedDirector()->getVisibleSize();

		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(size);
		mengban->setAnchorPoint(ccp(0,0));
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);

		if(LoadSceneLayer::auctionPanel->getWidgetParent() != NULL)
		{
			LoadSceneLayer::auctionPanel->removeFromParentAndCleanup(false);
		}

		UIPanel * panel=LoadSceneLayer::auctionPanel;
		panel->setAnchorPoint(ccp(0.5f,0.5f));
		panel->setPosition(ccp(size.width/2,size.height/2));
		m_pUiLayer->addWidget(panel);

		layer_=UILayer::create();
		layer_->ignoreAnchorPointForPosition(false);
		layer_->setAnchorPoint(ccp(0.5f,0.5f));
		layer_->setPosition(ccp(size.width/2,size.height/2));
		layer_->setContentSize(CCSizeMake(800,480));
		addChild(layer_,0,AUCTIONLAYER);

		const char * firstStr = StringDataManager::getString("UIName_auction_ji");
		const char * secondStr = StringDataManager::getString("UIName_auction_shou");
		const char * thirdStr = StringDataManager::getString("UIName_auction_hang");
		CCArmature * atmature = MainScene::createPendantAnm(firstStr,secondStr,thirdStr,"");
		atmature->setPosition(ccp(30,240));
		layer_->addChild(atmature);

		const char *strings_consign = StringDataManager::getString("goods_auction_consion");
		char *consign_left=const_cast<char*>(strings_consign);

		const char *strings_buy = StringDataManager::getString("goods_auction_buy");
		char *buy_left=const_cast<char*>(strings_buy);

		const char *highLightImage="res_ui/tab_1_on.png";
		char * label[] = {buy_left,consign_left};
		//buyOrsale=UITab::createWithBMFont(2,"res_ui/tab_1_off.png","res_ui/tab_1_on.png","",label,"res_ui/font/ziti_1.fnt",HORIZONTAL,5);
		buyOrsale=UITab::createWithText(2,"res_ui/tab_1_off.png","res_ui/tab_1_on.png","",label,HORIZONTAL,5);
		buyOrsale->setPressedActionEnabled(true);
		buyOrsale->setAnchorPoint(ccp(0.5f,0.5f));
		buyOrsale->setPosition(ccp(70,413));
		buyOrsale->setHighLightImage((char *)highLightImage);
		buyOrsale->setDefaultPanelByIndex(0);
		buyOrsale->addIndexChangedEvent(this,coco_indexchangedselector(AuctionUi::callBackchangebuyOrsale));
		layer_->addWidget(buyOrsale);

		consignSales=(UIPanel *)UIHelper::seekWidgetByName(panel,"Panel_jishou_b");
		consignSales->setVisible(false);
		purchase =(UIPanel *)UIHelper::seekWidgetByName(panel,"Panel_goumai");
		purchase->setVisible(true);

		puerchaseSelect =(UIPanel *)UIHelper::seekWidgetByName(panel,"Panel_goumai_b");
		puerchaseSelect->setVisible(true);

		consignSelect =(UIPanel *)UIHelper::seekWidgetByName(panel,"Panel_jishou_a");
		consignSelect->setVisible(false);

		UIButton * btn_buyOfquality=(UIButton *)UIHelper::seekWidgetByName(panel,"Button_goumaipinzhi");
		btn_buyOfquality->setTouchEnable(true);
		btn_buyOfquality->addReleaseEvent(this,coco_releaseselector(AuctionUi::callBack_btn_buyOfqualitySort));

		buyandquality =(UIImageView *)UIHelper::seekWidgetByName(panel,"ImageView_buyandquality_up");
		buyandquality->setTexture("res_ui/jt_down.png");
		
		UIButton * btn_LvSort=(UIButton *)UIHelper::seekWidgetByName(panel,"Button_dengji");
		btn_LvSort->setTouchEnable(true);
		btn_LvSort->addReleaseEvent(this,coco_releaseselector(AuctionUi::callBack_btn_LvSort));
		
		image_level =(UIImageView *)UIHelper::seekWidgetByName(panel,"ImageView_level_up2");
		image_level->setTexture("res_ui/jt_down.png");

		UIButton * btn_remainTime=(UIButton *)UIHelper::seekWidgetByName(panel,"Button_shengyushijian");
		btn_remainTime->setTouchEnable(true);
		btn_remainTime->addReleaseEvent(this,coco_releaseselector(AuctionUi::callBack_btn_remainTimeSort));

		image_remaintime =(UIImageView *)UIHelper::seekWidgetByName(panel,"ImageView_remaintime_up2_Clone");
		image_remaintime->setTexture("res_ui/jt_down.png");

		UIButton * btn_price=(UIButton *)UIHelper::seekWidgetByName(panel,"Button_jiage");
		btn_price->setTouchEnable(true);
		btn_price->addReleaseEvent(this,coco_releaseselector(AuctionUi::callBack_btn_priceSort));

		image_priceup =(UIImageView *)UIHelper::seekWidgetByName(panel,"ImageView_price_up3");
		image_priceup->setTexture("res_ui/jt_down.png");

		goldlabel_=(UILabel *)UIHelper::seekWidgetByName(panel,"Label_yinzi");

		goldIngot=(UILabel *)UIHelper::seekWidgetByName(panel,"Label_yuanbao");

		this->refreshPlayerMoney();
		UIButton *btn_type =(UIButton *) UIHelper::seekWidgetByName(panel,"Button_leixing");
		btn_type->setPressedActionEnabled(true);
		btn_type->setTouchEnable(true);
		btn_type->addReleaseEvent(this,coco_releaseselector(AuctionUi::callBack_btn_typeList));
		 
		const char *strings_type = StringDataManager::getString("auction_SelectType");
		labelType =(UILabel *)UIHelper::seekWidgetByName(panel,"Label_leixing");
		labelType->setText(strings_type);

		UIButton *btn_kind =(UIButton *) UIHelper::seekWidgetByName(panel,"Button_kind");
		btn_kind->setPressedActionEnabled(true);
		btn_kind->setTouchEnable(true);
		btn_kind->addReleaseEvent(this,coco_releaseselector(AuctionUi::callBack_btn_kind));

		const char *strings_kind = StringDataManager::getString("auction_SelectKind");
		labelKind =(UILabel *)UIHelper::seekWidgetByName(panel,"Label_kind");
		labelKind->setText(strings_kind);

		UIButton *btn_Level =(UIButton *) UIHelper::seekWidgetByName(panel,"Button_level");
		btn_Level->setPressedActionEnabled(true);
		btn_Level->setTouchEnable(true);
		btn_Level->addReleaseEvent(this,coco_releaseselector(AuctionUi::callBack_btn_lv));

		const char *strings_level = StringDataManager::getString("auction_SelectLevel");
		labelLevel =(UILabel *)UIHelper::seekWidgetByName(panel,"Label_level");
		labelLevel->setText(strings_level);

		UIButton *btn_quality =(UIButton *) UIHelper::seekWidgetByName(panel,"Button_pinzhi");
		btn_quality->setPressedActionEnabled(true);
		btn_quality->setTouchEnable(true);
		btn_quality->addReleaseEvent(this,coco_releaseselector(AuctionUi::callBack_btn_quality));

		const char *strings_quality = StringDataManager::getString("auction_SelectQuality");
		labelQuality =(UILabel *)UIHelper::seekWidgetByName(panel,"Label_pinzhi");
		labelQuality->setText(strings_quality);

		UIButton *btn_gold =(UIButton *) UIHelper::seekWidgetByName(panel,"Button_gold");
		btn_gold->setPressedActionEnabled(true);
		btn_gold->setTouchEnable(true);
		btn_gold->addReleaseEvent(this,coco_releaseselector(AuctionUi::callBack_btn_gold));

		const char *strings_gold = StringDataManager::getString("auction_SelectMoney");
		label_gold =(UILabel *)UIHelper::seekWidgetByName(panel,"Label_kind_0");
		label_gold->setText(strings_gold);

		checkInfoBox =new RichTextInputBox();
		checkInfoBox->setInputBoxWidth(150);
		checkInfoBox->setAnchorPoint(ccp(0,0));
		checkInfoBox->setPosition(ccp(575,102));
		checkInfoBox->setCharLimit(8);
		layer_->addChild(checkInfoBox);
		checkInfoBox->autorelease();
		
		UIButton * btn_find=(UIButton *)UIHelper::seekWidgetByName(panel,"Button_chaxun");
		btn_find->setPressedActionEnabled(true);
		btn_find->setTouchEnable(true);
		btn_find->addReleaseEvent(this,coco_releaseselector(AuctionUi::callBAck_btn_find));
		
		UIButton * btn_enterConsign = (UIButton *)UIHelper::seekWidgetByName(panel,"Button_jishouwupin");
		btn_enterConsign->setPressedActionEnabled(true);
		btn_enterConsign->setTouchEnable(true);
		btn_enterConsign->addReleaseEvent(this,coco_releaseselector(AuctionUi::callBack_btn_EnterConsignMent));

		//UILabel * friendReminder1 =(UILabel *)UIHelper::seekWidgetByName(panel,"TextArea_wenzimore_b");
		//UILabel * friendReminder2 =(UILabel *)UIHelper::seekWidgetByName(panel,"TextArea_wenzimore");

		UIButton * btn_Inputprice =(UIButton *)UIHelper::seekWidgetByName(panel,"Button_price");
		btn_Inputprice->setTouchEnable(true);
		btn_Inputprice->addReleaseEvent(this,coco_releaseselector(AuctionUi::callBack_btn_priceCounter));
		UIButton * btn_Inputamount =(UIButton *)UIHelper::seekWidgetByName(panel,"Button_3671");
		btn_Inputamount->setTouchEnable(true);
		btn_Inputamount->addReleaseEvent(this,coco_releaseselector(AuctionUi::callBack_btn_amountCounter));

		dealMoney = (UIImageView *)UIHelper::seekWidgetByName(panel,"ImageView_249_0");

		handCharge =(UILabel *)UIHelper::seekWidgetByName(panel,"Label_10liang");
		handCharge->setText("");

		changeCost = (UIImageView *)UIHelper::seekWidgetByName(panel,"ImageView_img1");
		changeCost->setTexture("res_ui/coins.png");

		changeCostLabel =(UILabel *)UIHelper::seekWidgetByName(panel,"Label_10liang_2");
		changeCostLabel->setText("");

		LabelBMFont_selfConsignIsNull =(UILabelBMFont *)UIHelper::seekWidgetByName(panel,"LabelBMFont_selfConsignIsNull");
		LabelBMFont_selfConsignIsNull->setVisible(false);


		//////////////////////////////////////////////////////////////////////////
		std::map<int,std::string>::const_iterator auctionCIter;
		auctionCIter = SystemInfoConfigData::s_systemInfoList.find(6);
		if (auctionCIter == SystemInfoConfigData::s_systemInfoList.end()) // 没找到就是指向END了  
		{
		}
		else
		{
			UITextArea * auctionDes = (UITextArea *)UIHelper::seekWidgetByName(panel,"TextArea_wenzimore_b");
			auctionDes->setText(SystemInfoConfigData::s_systemInfoList[6].c_str());
		}


		checkbox_gold =(UICheckBox *)UIHelper::seekWidgetByName(panel,"CheckBox_Gold");
		checkbox_gold->setTouchEnable(true);
		checkbox_gold->addEventListenerCheckBox(this,checkboxselectedeventselector(AuctionUi::setGoldDeal));
		checkbox_gold->setSelectedState(true);

		UIButton * image_copyCheckBox_gold = (UIButton *)UIHelper::seekWidgetByName(panel,"ImageView_249");
		image_copyCheckBox_gold->setTouchEnable(true);
		image_copyCheckBox_gold->addReleaseEvent(this,coco_releaseselector(AuctionUi::setGoldDealCopyCheckbox));

		UIButton * label_copyCheckBox_gold = (UIButton *)UIHelper::seekWidgetByName(panel,"Label_250");
		label_copyCheckBox_gold->setTouchEnable(true);
		label_copyCheckBox_gold->addReleaseEvent(this,coco_releaseselector(AuctionUi::setGoldDealCopyCheckbox));
		
		checkbox_Money =(UICheckBox *)UIHelper::seekWidgetByName(panel,"CheckBox_yuanbao");
		checkbox_Money->setTouchEnable(true);
		checkbox_Money->addEventListenerCheckBox(this,checkboxselectedeventselector(AuctionUi::setMoneyDeal));
		checkbox_Money->setSelectedState(false);

		UIButton * image_copyCheckBox_money = (UIButton *)UIHelper::seekWidgetByName(panel,"ImageView_251");
		image_copyCheckBox_money->setTouchEnable(true);
		image_copyCheckBox_money->addReleaseEvent(this,coco_releaseselector(AuctionUi::setMoneyCopyCheckbox));

		UIButton * label_copyCheckBox_money = (UIButton *)UIHelper::seekWidgetByName(panel,"Label_252");
		label_copyCheckBox_money->setTouchEnable(true);
		label_copyCheckBox_money->addReleaseEvent(this,coco_releaseselector(AuctionUi::setMoneyCopyCheckbox));

		iamge_ = (UIImageView *)UIHelper::seekWidgetByName(panel,"ImageView_3511");
		iamge_->setTexture("res_ui/coins.png");
		labelPrice = (UILabel *)UIHelper::seekWidgetByName(panel,"Label_Price_");
		labelPrice->setText("");

		labelAmount=(UILabel *)UIHelper::seekWidgetByName(panel,"labelamountstr_");
		labelAmount->setText("");

		UIButton * btn_back =(UIButton *)UIHelper::seekWidgetByName(panel,"Button_fanhui");
		btn_back->setPressedActionEnabled(true);
		btn_back->setTouchEnable(true);
		btn_back->addReleaseEvent(this,coco_releaseselector(AuctionUi::callBack_btn_back));

		UIButton * btn_consignment =(UIButton *)UIHelper::seekWidgetByName(panel,"Button_jishou");
		btn_consignment->setPressedActionEnabled(true);
		btn_consignment->setTouchEnable(true);
		btn_consignment->addReleaseEvent(this,coco_releaseselector(AuctionUi::callBack_btn_consignment));

		currentPage = (UIImageView *)UIHelper::seekWidgetByName(panel,"ImageView_dianOn");
		/////////backpage
		pageView = GameView::getInstance()->pacPageView;
		pageView->setAnchorPoint(ccp(0,0));
		pageView->setPosition(ccp(393,96));
		pageView->getPageView()->addPageTurningEvent(this,coco_PageView_PageTurning_selector(AuctionUi::CurrentPageViewChanged));
		layer_->addChild(pageView);
		pageView->setCurUITag(kTagAuction);
		pageView->setVisible(false);
		CCSequence* seq = CCSequence::create(CCDelayTime::create(0.05f),CCCallFunc::create(this,callfunc_selector(AuctionUi::PageScrollToDefault)),NULL);
		pageView->runAction(seq);
		//CurrentPageViewChanged(pageView);
		pageView->checkCDOnBegan();

		int consignBeginNum=GameView::getInstance()->auctionConsignVector.size();
		consignAmount= (UILabelBMFont *)UIHelper::seekWidgetByName(panel,"LabelBMFont_date");

		char consignSecond[10];
		sprintf(consignSecond,"%d",consignBeginNum);
		char consignMaxGoodsNum[10];
		sprintf(consignMaxGoodsNum,"%d",curMaxConsignGoodsNum);
		
		std::string consignBegin="";
		consignBegin.append(consignSecond);
		consignBegin.append("/");
		consignBegin.append(consignMaxGoodsNum);
		consignAmount->setText(consignBegin.c_str());

		UIButton * btn_close =(UIButton *)UIHelper::seekWidgetByName(panel,"Button_close");
		btn_close->setPressedActionEnabled(true);
		btn_close->setTouchEnable(true);
		btn_close->addReleaseEvent(this,coco_releaseselector(AuctionUi::callBack_btn_colse));

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(size);
		return true;
	}
	return false;
}

void AuctionUi::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
	
	auctionView=AuctionTabView::create();
	auctionView->ignoreAnchorPointForPosition(false);
	auctionView->setAnchorPoint(ccp(0,0));
	auctionView->setPosition(ccp(74,50));
	layer_->addChild(auctionView,1,AUCTIONTABVIEW);
	auctionView->init();
	
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1804,this);

	GameMessageProcessor::sharedMsgProcessor()->sendReq(1805,this);
}

void AuctionUi::onExit()
{
	UIScene::onExit();
}

void AuctionUi::callBackchangebuyOrsale( CCObject * obj )
{
	UITab * tab =(UITab *)obj;
	int num= tab->getCurrentIndex();

	std::vector<CAuctionInfo *>::iterator iterSource;
	for (iterSource=GameView::getInstance()->auctionSourceVector.begin();iterSource!=GameView::getInstance()->auctionSourceVector.end();iterSource++)
	{
		delete *iterSource;
	}
	GameView::getInstance()->auctionSourceVector.clear();
	switch (num)
	{
	case PURCHASETABS:
		{
			tabsType=0;

			consignSales->setVisible(false);
			purchase->setVisible(true);
			puerchaseSelect->setVisible(true);
			consignSelect->setVisible(false);
			pageView ->setVisible(false);
			auctionView->setVisible(true);
			checkInfoBox->setVisible(true);
			for(int i=0;i<GameView::getInstance()->auctionConsignVector.size();i++)
			{
				CAuctionInfo * auction=new CAuctionInfo();
				auction->CopyFrom(*GameView::getInstance()->auctionConsignVector.at(i));
				GameView::getInstance()->auctionSourceVector.push_back(auction);
			}
			this->getbackAuction(NULL);
		}break;
	case CONSIGNTABS:
		{
			tabsType=1;
			consignSales->setVisible(false);
			purchase->setVisible(true);
			puerchaseSelect->setVisible(false);
			consignSelect->setVisible(true);
			pageView ->setVisible(false);
			auctionView->setVisible(true);
			checkInfoBox->setVisible(false);
			for(int i=0;i<GameView::getInstance()->selfConsignVector.size();i++)
			{
				CAuctionInfo * auction=new CAuctionInfo();
				auction->CopyFrom(*GameView::getInstance()->selfConsignVector.at(i));
				GameView::getInstance()->auctionSourceVector.push_back(auction);
			}
			this->getbackAuction(NULL);
		}break;

	}

	int consignNum=GameView::getInstance()->selfConsignVector.size();
	std::string consignBegin="";
	char consignSecond[10];
	sprintf(consignSecond,"%d",consignNum);
	char consignMaxGoodsNum[10];
	sprintf(consignMaxGoodsNum,"%d",curMaxConsignGoodsNum);
	consignBegin.append(consignSecond);
	consignBegin.append("/");
	consignBegin.append(consignMaxGoodsNum);
	consignAmount->setText(consignBegin.c_str());
	
	CCTableView *tabview= (CCTableView *)auctionView->getChildByTag(AUCTIONCCTABVIEW);
	tabview->reloadData();
}

void AuctionUi::callBack_btn_typeList( CCObject * obj )
{
	UIButton * btn_ = (UIButton *)obj;

	WhereExList * whereex =WhereExList::create(0);
	whereex->setAnchorPoint(ccp(1,1));
	whereex->setPosition(ccp(btn_->getPosition().x-btn_->getContentSize().width-155,layer_->getContentSize().height/2 - 100));
	layer_->addChild(whereex,2,TAGWHEREEXLIST);
}

void AuctionUi::callBack_btn_kind( CCObject * obj )
{
	UIButton * btn_ =(UIButton *)obj;

	WhereExList * whereex =WhereExList::create(1);
	whereex->setAnchorPoint(ccp(1,1));
	if (whereex->getContentSize().height > 280)
	{
		whereex->setPosition(ccp(btn_->getPosition().x-btn_->getContentSize().width-155,layer_->getContentSize().height/2 - 100));
	}else
	{
		whereex->setPosition(ccp(btn_->getPosition().x-btn_->getContentSize().width-155,layer_->getContentSize().height/2 - 100));
	}

	layer_->addChild(whereex,2,TAGWHEREEXLIST);

	/*
	if (isSelectChat==true)
	{
		WhereExList * whereex =WhereExList::create(1);
		whereex->setAnchorPoint(ccp(1,1));
		if (whereex->getContentSize().height > 280)
		{
			whereex->setPosition(ccp(btn_->getPosition().x-btn_->getContentSize().width-115,layer_->getContentSize().height/2 - 110));
		}else
		{
			whereex->setPosition(ccp(btn_->getPosition().x-btn_->getContentSize().width-115,layer_->getContentSize().height/2 - 110));
		}

		layer_->addChild(whereex,2,TAGWHEREEXLIST);
	}else
	{
		const char *strings_ = StringDataManager::getString("auction_type");
		GameView::getInstance()->showAlertDialog(strings_);
	}
	*/
}

void AuctionUi::callBack_btn_lv( CCObject * obj )
{
	UIButton * btn_ =(UIButton *)obj;

	WhereExList * whereex =WhereExList::create(2);
	whereex->setAnchorPoint(ccp(1,1));
	if (whereex->getContentSize().height >280 )
	{
		whereex->setPosition(ccp(btn_->getPosition().x-btn_->getContentSize().width-155,layer_->getContentSize().height/2 - 100));
	}else
	{
		whereex->setPosition(ccp(btn_->getPosition().x-btn_->getContentSize().width-155,layer_->getContentSize().height/2 - 100));
	}

	layer_->addChild(whereex,2,TAGWHEREEXLIST);

	/*
	if (isSelectChat==true)
	{
		WhereExList * whereex =WhereExList::create(2);
		whereex->setAnchorPoint(ccp(1,1));
		if (whereex->getContentSize().height >280 )
		{
			whereex->setPosition(ccp(btn_->getPosition().x-btn_->getContentSize().width-205,layer_->getContentSize().height/2 - 110));
		}else
		{
			whereex->setPosition(ccp(btn_->getPosition().x-btn_->getContentSize().width-205,layer_->getContentSize().height/2 - 110));
		}

		layer_->addChild(whereex,2,TAGWHEREEXLIST);
	}else
	{
		const char *strings_ = StringDataManager::getString("auction_type");
		GameView::getInstance()->showAlertDialog(strings_);
	}
	*/
}

void AuctionUi::callBack_btn_quality( CCObject * obj )
{
	UIButton * btn_ =(UIButton *)obj;
	
	WhereExList * whereex =WhereExList::create(3);
	whereex->setAnchorPoint(ccp(1,1));
	whereex->setPosition(ccp(btn_->getPosition().x-btn_->getContentSize().width-155,layer_->getContentSize().height/2 - 100));
	layer_->addChild(whereex,2,TAGWHEREEXLIST);

	/*
	if (isSelectChat==true)
	{
		WhereExList * whereex =WhereExList::create(3);
		whereex->setAnchorPoint(ccp(1,1));
		whereex->setPosition(ccp(btn_->getPosition().x-btn_->getContentSize().width-205,layer_->getContentSize().height/2 - 110));
		layer_->addChild(whereex,2,TAGWHEREEXLIST);
	}else
	{
		const char *strings_ = StringDataManager::getString("auction_type");
		GameView::getInstance()->showAlertDialog(strings_);
	}
	*/
}

void AuctionUi::callBack_btn_gold( CCObject * obj )
{
	UIButton * btn_ =(UIButton *)obj;
	WhereExList * whereex =WhereExList::create(4);
	whereex->setAnchorPoint(ccp(1,1));
	whereex->setPosition(ccp(btn_->getPosition().x-btn_->getContentSize().width-155,layer_->getContentSize().height/2 - 100));
	layer_->addChild(whereex,2,TAGWHEREEXLIST);

	/*
	if (isSelectChat==true)
	{
		WhereExList * whereex =WhereExList::create(4);
		whereex->setAnchorPoint(ccp(1,1));
		whereex->setPosition(ccp(btn_->getPosition().x-btn_->getContentSize().width-115,layer_->getContentSize().height/2 - 110));
		layer_->addChild(whereex,2,TAGWHEREEXLIST);
	}else
	{
		const char *strings_ = StringDataManager::getString("auction_type");
		GameView::getInstance()->showAlertDialog(strings_);
	}
	*/
}

void AuctionUi::callBAck_btn_find( CCObject *obj )
{
	const char* chat = checkInfoBox->getInputString();
	if (chat == NULL)
	{
		//const char *strings_ = StringDataManager::getString("auction_search");
		//GameView::getInstance()->showAlertDialog(strings_);
		//return;
		setname = "";
	}else
	{
		setname = checkInfoBox->getInputString();
	}
	
	std::vector<CAuctionInfo *>::iterator iter;
	for (iter=GameView::getInstance()->auctionSourceVector.begin();iter!=GameView::getInstance()->auctionSourceVector.end();iter++)
	{
		delete *iter;
	}
	GameView::getInstance()->auctionSourceVector.clear();

	setpageindex=1;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1801,this);
	//checkInfoBox->deleteAllInputString();
}

void AuctionUi::callBack_btn_EnterConsignMent( CCObject * obj )
{
	consignSales->setVisible(true);
	purchase->setVisible(false);
	puerchaseSelect->setVisible(false);
	consignSelect->setVisible(false);
	
	pageView ->setVisible(true);
	auctionView->setVisible(false);
}

void AuctionUi::callBack_btn_consignment( CCObject * obj )
{
	if (isConsignGoods==false)
	{
		const char *str = StringDataManager::getString("auction_selectConsignGoods");
		GameView::getInstance()->showAlertDialog(str);
		return;
	}

	if (setPropNum <=0)
	{
		const char *str = StringDataManager::getString("auction_conNum");
		GameView::getInstance()->showAlertDialog(str);
		return;
	}
	if (setPrice <=0)
	{
		const char *str = StringDataManager::getString("auction_conPrice");
		GameView::getInstance()->showAlertDialog(str);
		return;
	}

	//setIndex = pageView->curPackageItemIndex;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1802,this);
	GameUtils::playGameSound(AUCTION_SUCCESS, 2, false);
	this->getbackAuction(NULL);
// 	labelAmount->setText("");
// 	labelPrice->setText("");
// 	handCharge->setText("");
// 	isConsignGoods=false;
}


void AuctionUi::callBack_btn_back( CCObject * obj )
{
	std::vector<CAuctionInfo *>::iterator iterSource;
	for (iterSource=GameView::getInstance()->auctionSourceVector.begin();iterSource!=GameView::getInstance()->auctionSourceVector.end();iterSource++)
	{
		delete *iterSource;
	}
	GameView::getInstance()->auctionSourceVector.clear();

	tabsType=0;
	consignSales->setVisible(false);
	purchase->setVisible(true);
	puerchaseSelect->setVisible(true);
	consignSelect->setVisible(false);
	pageView ->setVisible(false);
	auctionView->setVisible(true);
	checkInfoBox->setVisible(true);

	for(int i=0;i<GameView::getInstance()->selfConsignVector.size();i++)
	{
		CAuctionInfo * auction=new CAuctionInfo();
		auction->CopyFrom(*GameView::getInstance()->selfConsignVector.at(i));
		GameView::getInstance()->auctionSourceVector.push_back(auction);
	}
	auctionView->tabelView->reloadData();

	this->getbackAuction(NULL);
	buyOrsale->setDefaultPanelByIndex(0);
	int consignNum=GameView::getInstance()->selfConsignVector.size();
	std::string consignBegin="";
	char consignSecond[10];
	sprintf(consignSecond,"%d",consignNum);
	char consignMaxGoodsNum[10];
	sprintf(consignMaxGoodsNum,"%d",curMaxConsignGoodsNum);
	consignBegin.append(consignSecond);
	consignBegin.append("/");
	consignBegin.append(consignMaxGoodsNum);
	consignAmount->setText(consignBegin.c_str());

}

void AuctionUi::callBack_btn_colse( CCObject * obj )
{
	this->closeAnim();
}

bool compareGoodsOfQualityUp(CAuctionInfo * auctionGoods1,CAuctionInfo *auctionGoods2)
{
	return auctionGoods1->goods().quality() < auctionGoods2->goods().quality();
}

bool compareGoodsOfQualityDown(CAuctionInfo * auctionGoods1,CAuctionInfo *auctionGoods2)
{
	return auctionGoods2->goods().quality() < auctionGoods1->goods().quality();
}

void AuctionUi::callBack_btn_buyOfqualitySort( CCObject * obj )
{
	if (buyandqualityLift ==true)
	{
		//down
		setorderby=1;
		setorderbydesc=1;
		buyandquality->setTexture("res_ui/jt_down.png");
		buyandqualityLift=false;
		sort(GameView::getInstance()->auctionSourceVector.begin(),GameView::getInstance()->auctionSourceVector.end(),compareGoodsOfQualityDown);
	}else
	{
		//up
		setorderby=1;
		setorderbydesc=0;
		buyandquality->setTexture("res_ui/jt_up.png");
		buyandqualityLift=true;
		sort(GameView::getInstance()->auctionSourceVector.begin(),GameView::getInstance()->auctionSourceVector.end(),compareGoodsOfQualityUp);
	}
	auctionView->tabelView->reloadData();
	/*
	std::vector<CAuctionInfo *>::iterator iter;
	for (iter=GameView::getInstance()->auctionSourceVector.begin();iter!=GameView::getInstance()->auctionSourceVector.end();iter++)
	{
		delete *iter;
	}

	for(iter=GameView::getInstance()->auctionConsignVector.begin();iter!=GameView::getInstance()->auctionConsignVector.end();iter++)
	{
		delete *iter;
	}

	GameView::getInstance()->auctionConsignVector.clear();
	GameView::getInstance()->auctionSourceVector.clear();
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1801,this);
	*/
}

bool compareGoodsOfUseLevelUp(CAuctionInfo * auctionGoods1,CAuctionInfo *auctionGoods2)
{
	return auctionGoods1->goods().uselevel() < auctionGoods2->goods().uselevel();
}

bool compareGoodsOfUseLevelDown(CAuctionInfo * auctionGoods1,CAuctionInfo *auctionGoods2)
{
	return auctionGoods2->goods().uselevel() < auctionGoods1->goods().uselevel();
}

void AuctionUi::callBack_btn_LvSort( CCObject * obj )
{
	if (image_levelLift==true)
	{
		//down
		setorderby=2;
		setorderbydesc=1;
		image_level->setTexture("res_ui/jt_down.png");
		image_levelLift=false;
		sort(GameView::getInstance()->auctionSourceVector.begin(),GameView::getInstance()->auctionSourceVector.end(),compareGoodsOfUseLevelDown);
	}else
	{
		//up
		setorderby=2;
		setorderbydesc=0;
		image_level->setTexture("res_ui/jt_up.png");
		image_levelLift=true;
		sort(GameView::getInstance()->auctionSourceVector.begin(),GameView::getInstance()->auctionSourceVector.end(),compareGoodsOfUseLevelUp);
	}
	auctionView->tabelView->reloadData();
	/*
	std::vector<CAuctionInfo *>::iterator iter;
	for (iter=GameView::getInstance()->auctionSourceVector.begin();iter!=GameView::getInstance()->auctionSourceVector.end();iter++)
	{
		delete *iter;
	}
	for(iter=GameView::getInstance()->auctionConsignVector.begin();iter!=GameView::getInstance()->auctionConsignVector.end();iter++)
	{
		delete *iter;
	}

	GameView::getInstance()->auctionConsignVector.clear();
	GameView::getInstance()->auctionSourceVector.clear();
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1801,this);
	*/
}

bool compareGoodsOfRemTimeUp(CAuctionInfo * auctionGoods1,CAuctionInfo *auctionGoods2)
{
	return auctionGoods1->period() < auctionGoods2->period();
}

bool compareGoodsOfRemTimeDown(CAuctionInfo * auctionGoods1,CAuctionInfo *auctionGoods2)
{
	return auctionGoods2->period() < auctionGoods1->period();
}

void AuctionUi::callBack_btn_remainTimeSort( CCObject * obj )
{
	if (image_remaintimLift==true)
	{
		//down
		setorderby=3;
		setorderbydesc=1;
		image_remaintime->setTexture("res_ui/jt_down.png");
		image_remaintimLift=false;
		sort(GameView::getInstance()->auctionSourceVector.begin(),GameView::getInstance()->auctionSourceVector.end(),compareGoodsOfRemTimeDown);
	}else
	{
		//up
		setorderby=3;
		setorderbydesc=0;
		image_remaintime->setTexture("res_ui/jt_up.png");
		image_remaintimLift=true;
		sort(GameView::getInstance()->auctionSourceVector.begin(),GameView::getInstance()->auctionSourceVector.end(),compareGoodsOfRemTimeUp);
	}
	auctionView->tabelView->reloadData();
	/*
	std::vector<CAuctionInfo *>::iterator iter;
	for (iter=GameView::getInstance()->auctionSourceVector.begin();iter!=GameView::getInstance()->auctionSourceVector.end();iter++)
	{
		delete *iter;
	}

	for(iter=GameView::getInstance()->auctionConsignVector.begin();iter!=GameView::getInstance()->auctionConsignVector.end();iter++)
	{
		delete *iter;
	}

	GameView::getInstance()->auctionConsignVector.clear();
	GameView::getInstance()->auctionSourceVector.clear();
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1801,this);
	*/
}

bool compareGoodsOfPriceUp(CAuctionInfo * auctionGoods1,CAuctionInfo *auctionGoods2)
{
	return auctionGoods1->price() < auctionGoods2->price();
}

bool compareGoodsOfPriceDown(CAuctionInfo * auctionGoods1,CAuctionInfo *auctionGoods2)
{
	return auctionGoods2->price() < auctionGoods1->price();
}

void AuctionUi::callBack_btn_priceSort( CCObject * obj )
{
	if(image_priceupLift==true)
	{
		//down
		setorderby=4;
		setorderbydesc=1;
		image_priceup->setTexture("res_ui/jt_down.png");
		image_priceupLift=false;
		sort(GameView::getInstance()->auctionSourceVector.begin(),GameView::getInstance()->auctionSourceVector.end(),compareGoodsOfPriceDown);
	}else
	{
		//up
		setorderby=4;
		setorderbydesc=0;
		image_priceup->setTexture("res_ui/jt_up.png");
		image_priceupLift=true;
		sort(GameView::getInstance()->auctionSourceVector.begin(),GameView::getInstance()->auctionSourceVector.end(),compareGoodsOfPriceUp);
	}

	auctionView->tabelView->reloadData();
	/*
	std::vector<CAuctionInfo *>::iterator iter;
	for (iter=GameView::getInstance()->auctionSourceVector.begin();iter!=GameView::getInstance()->auctionSourceVector.end();iter++)
	{
		delete *iter;
	}

	for(iter=GameView::getInstance()->auctionConsignVector.begin();iter!=GameView::getInstance()->auctionConsignVector.end();iter++)
	{
		delete *iter;
	}

	GameView::getInstance()->auctionConsignVector.clear();
	GameView::getInstance()->auctionSourceVector.clear();
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1801,this);
	*/
}

void AuctionUi::callBack_btn_priceCounter( CCObject * obj )
{
	if (isConsignGoods==true)
	{
		GameView::getInstance()->showCounter(this, callfuncO_selector(AuctionUi::callBack_Pricecounter));
	}else
	{
		const char *strings_ = StringDataManager::getString("auction_selectConsignGoods");
		GameView::getInstance()->showAlertDialog(strings_);
	}
	
}

void AuctionUi::callBack_btn_amountCounter( CCObject* obj )
{
	if (isConsignGoods==true)
	{
		GameView::getInstance()->showCounter(this, callfuncO_selector(AuctionUi::callBack_amountCounter),setPropNum);
	}else
	{
		const char *strings_ = StringDataManager::getString("auction_selectConsignGoods");
		GameView::getInstance()->showAlertDialog(strings_);
	}
}

void AuctionUi::callBack_Pricecounter( CCObject *obj )
{
	Counter * priceCounter =(Counter *)obj;
	setPrice= priceCounter->getInputNum();
	char labelNum[10];
	sprintf(labelNum,"%d",setPrice);
	labelPrice->setText(labelNum);
	int changeCost_ = setPrice * 0.05;
	char changeCostValue[10];
	sprintf(changeCostValue,"%d",changeCost_);
	changeCostLabel->setText(changeCostValue);
}

void AuctionUi::callBack_amountCounter( CCObject* obj )
{
	Counter * priceCounter =(Counter *)obj;
	setPropNum= priceCounter->getInputNum();

	if (setPropNum <= 0)
	{
		return;
	}

	if (priceCounter->getInputNum() > GameView::getInstance()->AllPacItem.at(setIndex)->quantity() )
	{
		setPropNum = GameView::getInstance()->AllPacItem.at(setIndex)->quantity();
	}
	char labelNum[15];
	sprintf(labelNum,"%d",setPropNum);
	labelAmount->setText(labelNum);
	if (setPropNum>0)
	{
		float type_ = 1;
		int goodsType_ = GameView::getInstance()->AllPacItem.at(setIndex)->goods().clazz();
		std::map<int,std::string>::const_iterator cIter;
		cIter = VipValueConfig::s_vipValue.find(17);
		if (cIter == VipValueConfig::s_vipValue.end())
		{

		}
		else
		{
			std::string value_ = VipValueConfig::s_vipValue[17];
			std::vector<std::string > stringVector_ = StrUtils::split(value_,";");
			for (int i = 0;i<stringVector_.size();i++)
			{
				std::vector<std::string > stringVector_2 = StrUtils::split(stringVector_.at(i),":");
				char typeStr[10];
				sprintf(typeStr,"%d",goodsType_);
				if (strcmp(stringVector_2.at(0).c_str(),typeStr) == 0)
				{
					std::string str_ = stringVector_2.at(1);
					type_ = std::atof(str_.c_str());    
				}
			}
		}
		int qulity = GameView::getInstance()->AllPacItem.at(setIndex)->goods().quality();
		int levell_ =GameView::getInstance()->AllPacItem.at(setIndex)->goods().uselevel();

		int price_1 = (qulity *10 + levell_*2)*type_*setPropNum;
		float price_2 = (qulity *10 + levell_*2)*type_*setPropNum;

		if (price_2 > price_1)
		{
			price_1+=1;
		}

		char priceStr_[15];
		sprintf(priceStr_,"%d",price_1);
		handCharge->setText(priceStr_);

		int goodsCount_ = GameView::getInstance()->AllPacItem.at(setIndex)->quantity();
		char goodsUseNum[50];
		sprintf(goodsUseNum,"%d",setPropNum);
		char goodsAllNum[50];
		sprintf(goodsAllNum,"%d",goodsCount_);
		std::string goodsUseAndCount = goodsUseNum;
		goodsUseAndCount.append("/");
		goodsUseAndCount.append(goodsAllNum);
		GameView::getInstance()->pacPageView->SetCurFolderGray(true,setIndex,goodsUseAndCount);
	}
}

bool AuctionUi::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return true;
}

void AuctionUi::CurrentPageViewChanged( CCObject * obj )
{
	switch((int)(pageView->getPageView()->getCurPageIndex()))
	{
	case 0:
		currentPage->setPosition(ccp(520,66));
		break;
	case 1:
		currentPage->setPosition(ccp(542,66));
		break;
	case 2:
		currentPage->setPosition(ccp(562,66));
		break;
	case 3:
		currentPage->setPosition(ccp(583,66));
		break;
	case 4:
		currentPage->setPosition(ccp(604,66));
		break;
	}
}

void AuctionUi::addAuctionPacInfo( CCObject * obj )
{
	if (isConsignGoods==false)
	{
		if (successConsign > curMaxConsignGoodsNum)
		{
			const char *strings_ = StringDataManager::getString("auction_curConsignMaxCount");
			GameView::getInstance()->showAlertDialog(strings_);
			return;
		}

		FolderInfo * temp_folder_ = new FolderInfo();
		temp_folder_->CopyFrom(*GameView::getInstance()->AllPacItem.at(pageView->curPackageItemIndex));
		FloderItemInstance * auction_Item = FloderItemInstance::create(temp_folder_,FloderItemInstance::ktype_auction);
		auction_Item->ignoreAnchorPointForPosition(false);
		auction_Item->setAnchorPoint(ccp(0,0));
		auction_Item->setPosition(ccp(216.8f,335.5f));
		auction_Item->setTag(AUCTIONCONSIGGOODSINFONTAG);
		layer_->addChild(auction_Item);
		delete temp_folder_;


		if (GameView::getInstance()->AllPacItem.at(pageView->curPackageItemIndex)->has_goods())
		{
			int goodsCount_ = GameView::getInstance()->AllPacItem.at(pageView->curPackageItemIndex)->quantity();
			char goodsUseNum[50];
			sprintf(goodsUseNum,"%d",goodsCount_);
			char goodsAllNum[50];
			sprintf(goodsAllNum,"%d",goodsCount_);
			std::string goodsUseAndCount = goodsUseNum;
			goodsUseAndCount.append("/");
			goodsUseAndCount.append(goodsAllNum);

			GameView::getInstance()->pacPageView->SetCurFolderGray(true,pageView->curPackageItemIndex,goodsUseAndCount);
		}
		isConsignGoods=true;
		setIndex = pageView->curPackageItemIndex;

		int goodsNum_ = GameView::getInstance()->AllPacItem.at(setIndex)->quantity();
		char labelNum[10];
		sprintf(labelNum,"%d",goodsNum_);
		labelAmount->setText(labelNum);
		
		setPropNum = goodsNum_;
		if (goodsNum_>0)
		{
			float type_ = 1;
			int goodsType_ = GameView::getInstance()->AllPacItem.at(setIndex)->goods().clazz();
			std::map<int,std::string>::const_iterator cIter;
			cIter = VipValueConfig::s_vipValue.find(17);
			if (cIter == VipValueConfig::s_vipValue.end()) // 没找到就是指向END了  
			{

			}
			else
			{
				std::string value_ = VipValueConfig::s_vipValue[17];
				std::vector<std::string > stringVector_ = StrUtils::split(value_,";");
				for (int i = 0;i<stringVector_.size();i++)
				{
					std::vector<std::string > stringVector_2 = StrUtils::split(stringVector_.at(i),":");
					char typeStr[10];
					sprintf(typeStr,"%d",goodsType_);
					if (strcmp(stringVector_2.at(0).c_str(),typeStr) == 0)
					{
						std::string str_ = stringVector_2.at(1);
						type_ = std::atof(str_.c_str());    
					}
				}
			}
			int qulity =GameView::getInstance()->AllPacItem.at(setIndex)->goods().quality();
			int levell_ =GameView::getInstance()->AllPacItem.at(setIndex)->goods().uselevel();
			int price_1 = (qulity *10 + levell_*2)*type_*setPropNum;
			float price_2 = (qulity *10 + levell_*2)*type_*setPropNum;

			if (price_2 > price_1)
			{
				price_1+=1;
			}

			char priceStr_[10];
			sprintf(priceStr_,"%d",price_1);
			handCharge->setText(priceStr_);
		}

	}else
	{
		const char *strings_ = StringDataManager::getString("auction_consignNum");
		GameView::getInstance()->showAlertDialog(strings_);
	}

}

void AuctionUi::getbackAuction( CCObject * obj )
{
	isConsignGoods=false;
	handCharge->setText("");
	labelAmount->setText("");
	labelPrice->setText("");
	setPropNum = 0;
	setPrice = 0;
	changeCostLabel->setText("");
	FloderItemInstance * auctionGoodsInfo =(FloderItemInstance*)layer_->getChildByTag(AUCTIONCONSIGGOODSINFONTAG);
	if (auctionGoodsInfo != NULL)
	{
		auctionGoodsInfo->removeFromParentAndCleanup(true);
		
		if (GameView::getInstance()->AllPacItem.at(setIndex)->has_goods())
		{
			GameView::getInstance()->pacPageView->SetCurFolderGray(false,setIndex);
		}
	}
}

void AuctionUi::setGoldDeal( CCObject * obj,CheckBoxEventType type )
{
	UICheckBox * checkbox =(UICheckBox *)obj;

	checkbox_gold->setSelectedState(true);
	checkbox_Money->setSelectedState(false);
	setPriceType = 1;

	changeCost->setTexture("res_ui/coins.png");
	iamge_->setTexture("res_ui/coins.png");
}

void AuctionUi::setGoldDealCopyCheckbox( CCObject * obj )
{
	checkbox_gold->setSelectedState(true);
	checkbox_Money->setSelectedState(false);
	setPriceType = 1;

	changeCost->setTexture("res_ui/coins.png");
	iamge_->setTexture("res_ui/coins.png");
}

void AuctionUi::setMoneyCopyCheckbox( CCObject * obj )
{
	checkbox_Money->setSelectedState(true);
	checkbox_gold->setSelectedState(false);
	setPriceType = 2;

	changeCost->setTexture("res_ui/ingot.png");
	iamge_->setTexture("res_ui/ingot.png");
}

void AuctionUi::setMoneyDeal( CCObject * obj,CheckBoxEventType type )
{
	UICheckBox * checkbox =(UICheckBox *)obj;
	checkbox_Money->setSelectedState(true);
	checkbox_gold->setSelectedState(false);
	setPriceType = 2;

	changeCost->setTexture("res_ui/ingot.png");
	iamge_->setTexture("res_ui/ingot.png");
}

void AuctionUi::refreshPlayerMoney()
{
	int m_goldValue = GameView::getInstance()->getPlayerGold();
	char GoldStr_[20];
	sprintf(GoldStr_,"%d",m_goldValue);
	goldlabel_->setText(GoldStr_);

	int m_goldIngot =  GameView::getInstance()->getPlayerGoldIngot();
	char GoldIngotStr_[20];
	sprintf(GoldIngotStr_,"%d",m_goldIngot);
	goldIngot->setText(GoldIngotStr_);
}

void AuctionUi::PageScrollToDefault()
{
	pageView->getPageView()->scrollToPage(0);
	CurrentPageViewChanged(pageView);
}








