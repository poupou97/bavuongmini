

#ifndef AUCTION_AUCTIONGOODSINFO_
#define AUCTION_AUCTIONGOODSINFO_

#include "../backpackscene/GoodsItemInfoBase.h"
class AuctionUi;
class AuctionGoodsInfo:public GoodsItemInfoBase
{
public:
	AuctionGoodsInfo(void);
	~AuctionGoodsInfo(void);
	
	static AuctionGoodsInfo * create(GoodsInfo * goods,int curTab);
	
	bool init(GoodsInfo * goods,int curTab);
	virtual void onEnter();
	virtual void onExit();
	

	virtual bool ccTouchBegan(CCTouch * pTouch,CCEvent * pEvent);

	void callBackPurchase(CCObject * obj);
	void callBackSearch(CCObject * obj);

	void callBackRecover(CCObject * obj);
	void callBackCancel(CCObject * obj);
private:
	std::string m_goodsName;

};

#endif;