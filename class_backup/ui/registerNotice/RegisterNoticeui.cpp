#include "RegisterNoticeui.h"
#include "../extensions/CCRichLabel.h"
#include "GameView.h"


RegisterNoticeui::RegisterNoticeui(void)
{
}


RegisterNoticeui::~RegisterNoticeui(void)
{
}

RegisterNoticeui * RegisterNoticeui::create()
{
	RegisterNoticeui * resisterui = new RegisterNoticeui();
	if (resisterui && resisterui->init())
	{
		resisterui->autorelease();
		return resisterui;
	}
	CC_SAFE_DELETE(resisterui);
	return NULL;
}

bool RegisterNoticeui::init()
{
	if (UIScene::init())
	{
		CCSize winsize=CCDirector::sharedDirector()->getVisibleSize();

		UILayer * layer_ = UILayer::create();
		layer_->ignoreAnchorPointForPosition(false);
		layer_->setAnchorPoint(ccp(0.5f,0.5f));
		layer_->setPosition(ccp(winsize.width/2,winsize.height/2));
		layer_->setContentSize(CCSizeMake(800,480));
		this->addChild(layer_);
		/*
		UIImageView * backGround = UIImageView::create();
		backGround->setTexture("res_ui/zhezhao01.png");
		backGround->setAnchorPoint(ccp(0.5f,0.5f));
		backGround->setScale9Enable(true);
		backGround->setCapInsets(CCRect(51,51,1,1));
		backGround->setScale9Size(CCSizeMake(500,300));
		backGround->setPosition(ccp(layer_->getContentSize().width/2,layer_->getContentSize().height/2));
		layer_->addWidget(backGround);

		UIImageView * logSp = UIImageView::create();
		logSp->setTexture("res_ui/dlgg.png");
		logSp->setAnchorPoint(ccp(0.5f,0.5f));
		logSp->setPosition(ccp(0,backGround->getSize().height/2 - 20));
		backGround->addChild(logSp);

		UIButton * button_close= UIButton::create();
		button_close->setTextures("res_ui/close.png","res_ui/close.png","");
		button_close->setAnchorPoint(ccp(0.5f,0.5f));
		button_close->setPosition(ccp(backGround->getSize().width/2-3,backGround->getSize().height/2-3));
		button_close->setPressedActionEnabled(true);
		button_close->setTouchEnable(true);
		button_close->addReleaseEvent(this, coco_releaseselector(RegisterNoticeui::callBackExit));
		backGround->addChild(button_close);
		*/

		UIPanel * ppanel = (UIPanel*)GUIReader::shareReader()->widgetFromJsonFile("res_ui/loginbulletin_1.json");
		ppanel->setAnchorPoint(ccp(0.5f,0.5f));
		ppanel->setPosition(ccp(layer_->getContentSize().width/2,layer_->getContentSize().height/2));
		layer_->addWidget(ppanel);

		UIButton * button_close=(UIButton *)UIHelper::seekWidgetByName(ppanel,"Button_close");
		button_close->setPressedActionEnabled(true);
		button_close->setTouchEnable(true);
		button_close->addReleaseEvent(this, coco_releaseselector(RegisterNoticeui::callBackExit));

		CCScrollView * scrollView = CCScrollView::create(CCSizeMake(460,280));
		scrollView->setContentSize(CCSizeMake(460,280));
		scrollView->ignoreAnchorPointForPosition(false);
		scrollView->setTouchEnabled(true);
		scrollView->setDirection(kCCScrollViewDirectionVertical);
		scrollView->setAnchorPoint(ccp(0.5f,0.5f));
		scrollView->setPosition(ccp(layer_->getContentSize().width/2,layer_->getContentSize().height/2 - 25));
		scrollView->setBounceable(true);
		scrollView->setClippingToBounds(true);
		layer_->addChild(scrollView);	

		std::string strings_ = GameView::getInstance()->registerNoticeVector.at(0);
		CCRichLabel *labelLink=CCRichLabel::createWithString(strings_.c_str(),CCSizeMake(460,250),NULL,NULL,0,20,2);
		labelLink->setAnchorPoint(ccp(0,1));		
		scrollView->addChild(labelLink,1);
		if (labelLink->getContentSize().height > scrollView->getContentSize().height)
		{
			scrollView->setContentSize(ccp(460,labelLink->getContentSize().height));
			scrollView->setContentOffset(ccp(0,280 - labelLink->getContentSize().height ));
		}
		labelLink->setPosition(ccp(5,scrollView->getContentSize().height - labelLink->getContentSize().height));
		setTouchEnabled(true);
		setTouchMode(kCCTouchesOneByOne);
		setContentSize(winsize);
		
		return true;
	}
	return false;
}

void RegisterNoticeui::onEnter()
{
	UIScene::onEnter();
}

void RegisterNoticeui::onExit()
{
	UIScene::onExit();
}

void RegisterNoticeui::callBackExit( CCObject * obj )
{
	this->closeAnim();
}
