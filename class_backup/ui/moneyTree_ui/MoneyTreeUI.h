#ifndef _UI_MONEYTREE_MONEYTREEUI_H_
#define _UI_MONEYTREE_MONEYTREEUI_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

/////////////////////////////////
/**
 * 摇钱树UI
 * @author liuliang
 * @version 0.1.0
 * @date 2014.11.20
 */

class MoneyTreeUI:public UIScene
{
public:
	MoneyTreeUI(void);
	~MoneyTreeUI(void);

public:
	static UIPanel * s_pPanel;
	static MoneyTreeUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual void update(float dt);

private:
	UIButton* m_pBtn_begin;								// 摇一摇
	
	UIButton* m_pBtn_begin_disable;						// 摇一摇不可用状态
	UILabel* m_pLabel_time;								// 倒计时

	UIImageView* m_pImgView_bar;						// 剩余条
	UILabel* m_pLabel_num;								// XX/XX（剩余次数/总次数）
	

private:
	void initUI();
	std::string getLeftCount();							// 获取 [剩余次数/总次数]
	float getBarValue();								// 返回剩余条的比例值
	
	void initBtnAndTextStatus(std::string strTimeLeft);

private:
	void callBackBtnClolse(CCObject * obj);																	// 关闭按钮的响应
	void callBackBtnBegin(CCObject * obj);																	// 开始 摇钱树
	

public:
	void initDataFromInternet();						// 从服务器获取数据（更新UI）
	void addMoneyTreeAnmation();						// 摇钱树的摇钱效果
};

#endif

