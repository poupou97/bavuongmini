#ifndef _UI_MONEYTREE_MONEYTREEDATA_H_
#define _UI_MONEYTREE_MONEYTREEDATA_H_

#include "cocos2d.h"

USING_NS_CC;

/////////////////////////////////
/**
 * 摇钱树模块（用于保存服务器数据）
 * @author liuliang
 * @version 0.1.0
 * @date 2014.11.19
 */

class MoneyTreeData
{
public:
	static MoneyTreeData * s_moneyTreeData;
	static MoneyTreeData * instance();

private:
	MoneyTreeData(void);
	~MoneyTreeData(void);

private:
	int m_nMaxNum;
	int m_nHasUseNum;
	int m_nNeedTime;
	int m_nCurrentCount;						// 第几次摇一摇（从1开始）

public:
	void set_maxNum(int nMaxNum);
	int get_maxNum();

	void set_hasUseNum(int nHasUseNum);
	int get_hasUserNum();

	void set_needTime(int nNeedTime);
	int get_needTime();

	void set_currentCount(int nCurrentCount);
	int get_currentCount();						

	// for LiuZhenXing use
	std::string get_numLeft();							// 次数显示 XX/XX
	bool hasAllGiftGet();								// 摇钱树次数是否全部用完

	// 获取剩余时间[如果剩余时间不为00:00的话，显示XX:XX；
	// 如果剩余时间为00:00的话，则显示StringDataManager::getString("MoneyTree_canGetInfo");]
	std::string get_strTimeLeft();							
};

#endif

