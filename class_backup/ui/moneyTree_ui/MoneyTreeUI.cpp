#include "MoneyTreeUI.h"

#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "../../utils/GameUtils.h"
#include "GameView.h"
#include "MoneyTreeTimeManager.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "MoneyTreeData.h"

using namespace CocosDenshion;

const CCSize CCSizeUIView = CCSizeMake(300, 370);

#define COCOSTUDIO_PANEL_WIDTH 300
#define COCOSTUDIO_PANEL_HEIGHT 370

UIPanel * MoneyTreeUI::s_pPanel;

MoneyTreeUI::MoneyTreeUI(void)
{

}

MoneyTreeUI::~MoneyTreeUI(void)
{

}

MoneyTreeUI* MoneyTreeUI::create()
{
	MoneyTreeUI * pMoneyTreeUI = new MoneyTreeUI();
	if (pMoneyTreeUI && pMoneyTreeUI->init())
	{
		pMoneyTreeUI->autorelease();
		return pMoneyTreeUI;
	}
	CC_SAFE_DELETE(pMoneyTreeUI);
	return NULL;
}

bool MoneyTreeUI::init()
{
	if (UIScene::init())
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

		// 即用即加载，不再提前加载
		s_pPanel = (UIPanel *)GUIReader::shareReader()->widgetFromJsonFile("res_ui/yaoqianshu_1.json");
		s_pPanel->setAnchorPoint(ccp(0, 0));
		s_pPanel->getValidNode()->setContentSize(CCSizeMake(COCOSTUDIO_PANEL_WIDTH, COCOSTUDIO_PANEL_HEIGHT));
		s_pPanel->setPosition(ccp((winSize.width - COCOSTUDIO_PANEL_WIDTH) / 2, (winSize.height - COCOSTUDIO_PANEL_HEIGHT) / 2));
		s_pPanel->setScale(1.0f);
		s_pPanel->setTouchEnable(true);
		m_pUiLayer->addWidget(s_pPanel);

		initUI();

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(winSize);

		this->scheduleUpdate();

		return true;
	}
	return false;
}

void MoneyTreeUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();

	GameUtils::playGameSound(SCENE_OPEN, 2, false);
}

void MoneyTreeUI::onExit()
{
	UIScene::onExit();

	SimpleAudioEngine::sharedEngine()->playEffect(SCENE_CLOSE, false);
}

void MoneyTreeUI::callBackBtnClolse( CCObject * obj )
{
	this->closeAnim();
}

void MoneyTreeUI::callBackBtnBegin( CCObject * obj )
{
	int nCurrentCount = MoneyTreeData::instance()->get_currentCount();

	// 请求服务器（第几次的摇一摇）
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5138, (void *)nCurrentCount);
}

void MoneyTreeUI::initUI()
{
	UIButton* pBtnClose = (UIButton*)UIHelper::seekWidgetByName(s_pPanel, "Button_close");
	pBtnClose->setTouchEnable(true);
	pBtnClose->setPressedActionEnabled(true);
	pBtnClose->addReleaseEvent(this, coco_releaseselector(MoneyTreeUI::callBackBtnClolse));

	// 开始摇钱
	m_pBtn_begin = (UIButton*)UIHelper::seekWidgetByName(s_pPanel, "Button_begin");
	m_pBtn_begin->setTouchEnable(true);
	m_pBtn_begin->setPressedActionEnabled(true);
	m_pBtn_begin->setVisible(false);
	m_pBtn_begin->addReleaseEvent(this, coco_releaseselector(MoneyTreeUI::callBackBtnBegin));

	// 摇一摇不可用btn
	m_pBtn_begin_disable = (UIButton*)UIHelper::seekWidgetByName(s_pPanel, "Button_begin_disable");
	m_pBtn_begin_disable->setTouchEnable(false);
	m_pBtn_begin_disable->setPressedActionEnabled(false);
	m_pBtn_begin_disable->setVisible(true);
	
	// 摇一摇倒计时
	m_pLabel_time = (UILabel*)UIHelper::seekWidgetByName(s_pPanel, "Label_timeLeft");
	m_pLabel_time->setVisible(true);

	// 摇一摇剩余条
	m_pImgView_bar = (UIImageView*)UIHelper::seekWidgetByName(s_pPanel, "ImageView_bar");
	m_pImgView_bar->setVisible(true);

	// XX/XX（剩余次数/总次数）
	m_pLabel_num = (UILabel*)UIHelper::seekWidgetByName(s_pPanel, "Label_num");
	m_pLabel_num->setVisible(true);
}

void MoneyTreeUI::initDataFromInternet()
{
	// 倒计时，剩余条，[剩余次数/总次数]
	if (NULL != GameView::getInstance()->getMainUIScene())
	{
		MoneyTreeTimeManager * pMoneyTreeTimeManager = (MoneyTreeTimeManager *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMoneyTreeManager);
		if (NULL != pMoneyTreeTimeManager)
		{
			// 倒计时
			std::string strTimeLeft = pMoneyTreeTimeManager->get_timeLeftStr();
			m_pLabel_time->setText(strTimeLeft.c_str());

			// 按钮及文本状态
			this->initBtnAndTextStatus(strTimeLeft);

			// [剩余次数/总次数]
			std::string strLeftCount = this->getLeftCount();
			m_pLabel_num->setText(strLeftCount.c_str());

			// 剩余条
			float scale = this->getBarValue();
			m_pImgView_bar->setTextureRect(CCRectMake(0, 0, m_pImgView_bar->getContentSize().width * scale, m_pImgView_bar->getContentSize().height));
		}
	}
}

void MoneyTreeUI::update( float dt )
{
	if (NULL != GameView::getInstance()->getMainUIScene())
	{
		// 倒计时，剩余条，[剩余次数/总次数]
		MoneyTreeTimeManager * pMoneyTreeTimeManager = (MoneyTreeTimeManager *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMoneyTreeManager);
		if (NULL != pMoneyTreeTimeManager)
		{
			std::string strTimeLeft = pMoneyTreeTimeManager->get_timeLeftStr();

			this->initBtnAndTextStatus(strTimeLeft);
		}	
	}
}

std::string MoneyTreeUI::getLeftCount()
{
	// [剩余次数/总次数]
	int nMaxCount = MoneyTreeData::instance()->get_maxNum();
	int nCurrentCount = MoneyTreeData::instance()->get_currentCount();

	int nLeftCount = nMaxCount - nCurrentCount + 1;

	char charLeftCount[10];
	sprintf(charLeftCount, "%d", nLeftCount);

	char charMaxCount[10];
	sprintf(charMaxCount, "%d", nMaxCount);

	std::string strLeftCount = "";
	strLeftCount.append(charLeftCount);
	strLeftCount.append("/");
	strLeftCount.append(charMaxCount);

	return strLeftCount;
}

float MoneyTreeUI::getBarValue()
{
	// [剩余次数/总次数]
	int nMaxCount = MoneyTreeData::instance()->get_maxNum();
	int nCurrentCount = MoneyTreeData::instance()->get_currentCount();

	int nLeftCount = nMaxCount - nCurrentCount + 1;

	float scale = (float)nLeftCount / (float)nMaxCount;

	return scale;
}

void MoneyTreeUI::addMoneyTreeAnmation()
{
	UIImageView* m_pImgView_tree = (UIImageView*)UIHelper::seekWidgetByName(s_pPanel, "ImageView_tree");

	float xPos = s_pPanel->getPosition().x + m_pImgView_tree->getPosition().x + 7;
	float yPos = s_pPanel->getPosition().y + m_pImgView_tree->getPosition().y - 29;

	// 摇钱树晃动的动画
	//从导出文件异步加载动画
	CCArmatureDataManager::sharedArmatureDataManager()->addArmatureFileInfo("res_ui/uiflash/yaoqianshu/yaoqianshu0.png","res_ui/uiflash/yaoqianshu/yaoqianshu0.plist","res_ui/uiflash/yaoqianshu/yaoqianshu.ExportJson");
	//根据动画名称创建动画精灵
	CCArmature *armature = CCArmature::create("yaoqianshu");
	//播放指定动作
	armature->getAnimation()->playByIndex(0);
	//修改属性
	armature->setScale(1.0f);
	//设置动画精灵位置
	armature->setPosition(xPos, yPos);
	//添加到当前页面
	this->addChild(armature);

	// 金币掉落的动画（左右一边一个）
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

	float yPosCoin = s_pPanel->getPosition().y + m_pImgView_tree->getPosition().y + 40;
	float xPosLeft = winSize.width / 2 - 30;
	float xPosRight = winSize.width / 2 + 30;

	std::string strAnm = "animation/texiao/particledesigner/jinbi.plist";

	CCParticleSystem* particleEffect_left = CCParticleSystemQuad::create(strAnm.c_str());
	particleEffect_left->setAnchorPoint(ccp(0.5f,0.5f));
	particleEffect_left->setPosition(ccp(xPosLeft, yPosCoin));
	particleEffect_left->setScale(0.3f);
	particleEffect_left->setDuration(2.5f);
	particleEffect_left->setAutoRemoveOnFinish(true);

	CCParticleSystem* particleEffect_right = CCParticleSystemQuad::create(strAnm.c_str());
	particleEffect_right->setAnchorPoint(ccp(0.5f,0.5f));
	particleEffect_right->setPosition(ccp(xPosRight, yPosCoin));
	particleEffect_right->setScale(0.3f);
	particleEffect_right->setDuration(2.5f);
	particleEffect_right->setAutoRemoveOnFinish(true);

	this->addChild(particleEffect_left);
	this->addChild(particleEffect_right);
}

void MoneyTreeUI::initBtnAndTextStatus( std::string strTimeLeft )
{
	std::string timeString = "";
	const char *str1 = StringDataManager::getString("MoneyTree_canGetInfo");
	timeString.append(str1);

	//判断 将要显示的是否为“可领取”，若是，则创建特效；否则 移除特效（如果先前的特效存在的话）
	if (strTimeLeft == timeString)
	{
		if (MoneyTreeData::instance()->hasAllGiftGet())
		{
			strTimeLeft = "00:00";

			m_pBtn_begin->setVisible(false);
			m_pBtn_begin_disable->setVisible(true);
			m_pLabel_time->setVisible(true);

			m_pLabel_time->setText(strTimeLeft.c_str());
		}
		else
		{
			m_pBtn_begin->setVisible(true);
			m_pBtn_begin_disable->setVisible(false);
			m_pLabel_time->setVisible(false);
		}	
	}
	else
	{
		m_pBtn_begin->setVisible(false);
		m_pBtn_begin_disable->setVisible(true);
		m_pLabel_time->setVisible(true);

		m_pLabel_time->setText(strTimeLeft.c_str());
	}
}






