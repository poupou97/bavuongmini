#ifndef _UI_MONEYTREE_MONEYTREETIMEMANAGER_H_
#define _UI_MONEYTREE_MONEYTREETIMEMANAGER_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

/////////////////////////////////
/**
 * 摇钱树模块（用于监测时间）
 * @author liuliang
 * @version 0.1.0
 * @date 2014.11.19
 */

class MoneyTreeTimeManager : public cocos2d::CCNode
{
public:
	MoneyTreeTimeManager(void);
	~MoneyTreeTimeManager(void);

public:
	static MoneyTreeTimeManager* create();

	virtual bool init();

private:
	std::string m_strTimeLeft;

	int m_nLeftTime;
	int m_nShowTime;

private:
	void updateTime(float dt);

	std::string timeFormatToString(int nTime);
	void refreshData();

public:
	void set_timeLeftStr(std::string strTimeLeft);
	std::string get_timeLeftStr();

	void initDataFromIntent();										// 从服务器数据初始化data
};

#endif

