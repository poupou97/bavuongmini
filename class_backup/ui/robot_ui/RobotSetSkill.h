
#ifndef _ROBOTUI_ROBOTSETSKILL_H
#define _ROBOTUI_ROBOTSETSKILL_H

#include "../extensions/UIScene.h"

class GameFightSkill;
class RobotSetSkill: public UIScene,public CCTableViewDelegate,public CCTableViewDataSource
{
public:
	RobotSetSkill(void);
	~RobotSetSkill(void);

	struct ReqData{
		long long generalsid ;
		std::string skillid ;
		int index;
	};

	static RobotSetSkill * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);

	void CloseEvent(CCObject *pSender);
	
	void refreshSkillInfo(GameFightSkill * gameFightSkill);
	//装配
	void EquipEvent(CCObject *pSender);


	void showSkillDes(std::string curSkillDes,std::string nextSkillDes);
public: 
	//std::vector<GameFightSkill *>DataSourceList;
	CCTableView * generalsSkillsList_tableView;

	UIImageView* i_skillFrame;
	UIImageView* i_skillImage;
	UILabel* l_skillName;
	UILabel* l_skillType;
	UILabel* l_skillLvValue ;
	UIImageView *imageView_skillQuality;
	UILabel* l_skillQualityValue;
	UILabel* l_skillMpValue;
	UILabel* l_skillCDTimeValue ;
	UILabel* l_skillDistanceValue;

	int setEquipSkillIndex;
	std::vector<GameFightSkill *> generalSkillVector;

	UILayer * Layer_skills;
private:
	int setSkillIndex;
};

#endif