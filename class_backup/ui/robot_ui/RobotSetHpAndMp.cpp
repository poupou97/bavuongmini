#include "RobotSetHpAndMp.h"
#include "../../messageclient/element/FolderInfo.h"
#include "RobotMainUI.h"
#include "GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/MapRobotDrug.h"
#include "../../utils/StaticDataManager.h"


RobotSetHpAndMp::RobotSetHpAndMp(void)
{
}


RobotSetHpAndMp::~RobotSetHpAndMp(void)
{
}

RobotSetHpAndMp * RobotSetHpAndMp::create(int drugtype)
{
	RobotSetHpAndMp * setHpMp =new RobotSetHpAndMp();
	if (setHpMp && setHpMp->init(drugtype))
	{
		setHpMp->autorelease();
		return setHpMp;
	}
	CC_SAFE_DELETE(setHpMp);
	return NULL;

}

bool RobotSetHpAndMp::init(int drugtype)
{
	if (UIScene::init())
	{

		m_drugType = drugtype;

		CCScale9Sprite * background = CCScale9Sprite::create("res_ui/dikuang_1.png");
		background->setCapInsets(CCRect(10,10,1,1));
		background->setPreferredSize(CCSizeMake(320,105));
		background->setAnchorPoint(ccp(0,0));
		background->setPosition(ccp(0,0));
		this->addChild(background);

		CCTableView *robotSetHpAdnMp = CCTableView::create(this,CCSizeMake(300,110));
		robotSetHpAdnMp->setDirection(kCCScrollViewDirectionHorizontal);
		robotSetHpAdnMp->setAnchorPoint(ccp(0,0));
		robotSetHpAdnMp->setPosition(ccp(10,5));
		robotSetHpAdnMp->setDelegate(this);
		robotSetHpAdnMp->setVerticalFillOrder(kCCTableViewFillTopDown);
		addChild(robotSetHpAdnMp);

		robotSetHpAdnMp->reloadData();

		this->setContentSize(CCSizeMake(300,110));
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);

		return true;
	}
	return false;
}

void RobotSetHpAndMp::onEnter()
{
	UIScene::onEnter();
}

void RobotSetHpAndMp::onExit()
{
	UIScene::onExit();
}

bool RobotSetHpAndMp::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return resignFirstResponder(touch,this,false);
}

void RobotSetHpAndMp::scrollViewDidScroll( CCScrollView* view )
{

}

void RobotSetHpAndMp::scrollViewDidZoom( CCScrollView* view )
{

}

void RobotSetHpAndMp::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{
 	int index =cell->getIdx();
 	//add select goods
 	RobotMainUI * robotui = (RobotMainUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRobotUI);
 
	std::map<int,MapRobotDrug *> * robotdrugMap = RobotDrugConfigData::s_RobotDrugConfig[m_drugType];
	
	std::string goodsIcon_;
	std::string goodsId_;
	std::string  goodsName_ ;
	if (index ==0)
	{
		goodsIcon_ = robotdrugMap->at(1)->get_icon();
		goodsId_ = robotdrugMap->at(1)->get_id();
		goodsName_ = robotdrugMap->at(1)->get_name();
	}else
	{
		goodsIcon_ = robotdrugMap->at(index*15)->get_icon();
		goodsId_ = robotdrugMap->at(index*15)->get_id();
		goodsName_ = robotdrugMap->at(index*15)->get_name();
	}

 	robotui->addUseGoods(goodsIcon_,goodsId_,goodsName_);
	this->removeFromParentAndCleanup(true);
}

cocos2d::CCSize RobotSetHpAndMp::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeMake(83,110);
}

cocos2d::extension::CCTableViewCell* RobotSetHpAndMp::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell = new CCTableViewCell();
	cell->autorelease();

	CCScale9Sprite * lvSp = CCScale9Sprite::create("res_ui/LV4_dididi.png");
	lvSp->setCapInsets(CCRect(9,10,1,1));
	lvSp->setPreferredSize(CCSize(77,21));
	lvSp->setAnchorPoint(ccp(0,0));
	lvSp->setPosition(ccp(3,0));
	cell->addChild(lvSp);

	CCSprite * spBg = CCSprite::create("res_ui/di_white.png");
	spBg->setAnchorPoint(ccp(0, 0));
	spBg->setPosition(ccp(0,lvSp->getContentSize().height));
	cell->addChild(spBg);

	//map data
	std::map<int,MapRobotDrug *> * robotdrugMap = RobotDrugConfigData::s_RobotDrugConfig[m_drugType];
	std::string goodsIcon_;
	std::string  goodsName_ ;
	int goodsUseLevel;
	if (idx ==0)
	{
		goodsIcon_ = robotdrugMap->at(1)->get_icon();
		goodsName_ = robotdrugMap->at(1)->get_name();
		goodsUseLevel = robotdrugMap->at(1)->get_level();
	}else
	{
		goodsIcon_ = robotdrugMap->at(idx*15)->get_icon();
		goodsName_ = robotdrugMap->at(idx*15)->get_name();
		goodsUseLevel = robotdrugMap->at(idx * 15)->get_level();
	}

	std::string headImageStr ="res_ui/props_icon/";
	headImageStr.append(goodsIcon_);
	headImageStr.append(".png");

	CCSprite* spGoods=CCSprite::create(headImageStr.c_str());
	spGoods->setAnchorPoint(ccp(0,0));
	spGoods->setPosition(ccp((spBg->getContentSize().width - spGoods->getContentSize().width) / 2,
								(spBg->getContentSize().height - spGoods->getContentSize().height) / 2 + lvSp->getContentSize().height));
	cell->addChild(spGoods);

	CCLabelTTF * label_name =CCLabelTTF::create();
	label_name->setFontSize(12);
	label_name->setString(goodsName_.c_str());
	label_name->setAnchorPoint(ccp(0,0));
	label_name->setPosition(ccp((spBg->getContentSize().width - label_name->getContentSize(). width) / 2,5+lvSp->getContentSize().height));
	cell->addChild(label_name);
	
	char levelStr[10];
	sprintf(levelStr,"%d",goodsUseLevel);
	std::string levelString_ ="LV";
	levelString_.append(levelStr);
	CCLabelTTF * label_useLevel =CCLabelTTF::create();
	label_useLevel->setFontSize(14);
	label_useLevel->setString(levelString_.c_str());
	label_useLevel->setAnchorPoint(ccp(0.5f,0));
	label_useLevel->setPosition(ccp(lvSp->getContentSize().width/2 +3 ,2));
	cell->addChild(label_useLevel);

	return cell;
}

unsigned int RobotSetHpAndMp::numberOfCellsInTableView( CCTableView *table )
{
	std::map<int,MapRobotDrug *> * robotdrugMap = RobotDrugConfigData::s_RobotDrugConfig[m_drugType];
	int mapsiez = robotdrugMap->size();
	return mapsiez;
}
