#ifndef CHALLENGE_SHOWLEVELUI_H 
#define CHALLENGE_SHOWLEVELUI_H

#include "../extensions/UIScene.h"
#include "../../legend_script/CCTutorialIndicator.h"

class CTowerHistory;
class ShowLevelUi:public UIScene
{
public:
	ShowLevelUi(void);
	~ShowLevelUi(void);

	static ShowLevelUi * create(int clazzIndex);
	bool init(int clazzIndex);
	void onEnter();
	void onExit();

	void showCurLevelContent(CCObject * obj);
	void addPageViewIndex(int pageCount);
	void refreshPageDate();

	void callBackReward(CCObject * obj);
	void callBackRankList(CCObject * obj);
	void callBackRuleDes(CCObject * obj);
	
	void callBackAddPhyPower(CCObject * obj);

	void callBackCloseUi(CCObject *obj);

	void enterCopyLevel(CCObject * obj);
	void enterSureSignCopyLevel(CCObject * obj);
	void enterSingCopyAction(CCObject * obj);

	int m_clazz;
	int m_everyPageNum;
	void setCurPageIndex(int index);
	int getCurpageIndex();

	std::vector<CTowerHistory *> singCopyHistoryVector;
	void setTodaymaxlevel(int level_);
	int getTodaymaxLevel();

	void setCurLevel(int value_);
	int getCurLevel();

public:
	//��ѧ
	int mTutorialScriptInstanceId;
	int mTutorialIndex;
	//��ѧ
	virtual void registerScriptCommand(int scriptId);

	void addCCTutorialIndicator(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction);
	void removeCCTutorialIndicator();
public:
	UILabelBMFont * labelTodayMaxLevel;
	UILabelBMFont * labelTodayMaxUsetime;
	//UILabel * labelpageIndex;
private:
	CCSize winsize;
	UIPageView * m_pageView;
	int m_curPageIndex;
	int m_todaymaxLevel;
	int m_curLevel;
};

#endif