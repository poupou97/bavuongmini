#include "SingleCopyResult.h"
#include "../../messageclient/element/CSingCopyClazz.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/generals_ui/RecuriteActionItem.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/GameStatistics.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../FivePersonInstance/ResultHeadPartItem.h"
#include "../../ui/GameUIConstant.h"
#include "SingCopyInstance.h"
#include "../../gamescene_state/MainAnimationScene.h"
#include "gamescene_state/sceneelement/MissionAndTeam.h"
#include "gamescene_state/MainScene.h"


SingleCopyResult::SingleCopyResult(void):
m_nRemainTime(30000)
{
}


SingleCopyResult::~SingleCopyResult(void)
{
}

SingleCopyResult * SingleCopyResult::create(int clazz,int level,int curTime,int bestTime,int money,int result)
{
	SingleCopyResult * resultui_ = new SingleCopyResult();
	if (resultui_ && resultui_->init(clazz,level,curTime,bestTime,money,result))
	{
		resultui_->autorelease();
		return resultui_;
	}
	CC_SAFE_DELETE(resultui_);
	return NULL;
}

bool SingleCopyResult::init(int clazz,int level,int curTime,int bestTime,int money,int result)
{
	if (UIScene::init())
	{
		winsize = CCDirector::sharedDirector()->getVisibleSize();

		m_clazz = clazz;
		m_level = level;
		m_curTime = curTime;
		m_bestTime = bestTime;;
		m_money = money;
		m_result = result;

		layer_anm = UILayer::create();
		addChild(layer_anm);

		CCActionInterval * action =(CCActionInterval *)CCSequence::create(
					CCDelayTime::create(END_BATTLE_BEFORE_SLOWMOTION_DURATION),
					CCCallFunc::create(this, callfunc_selector(SingleCopyResult::startSlowMotion)),
					CCCallFunc::create(this,callfunc_selector(SingleCopyResult::addLightAnimation)),
					CCDelayTime::create(END_BATTLE_SLOWMOTION_DURATION),
					CCCallFunc::create(this, callfunc_selector(SingleCopyResult::endSlowMotion)),
					CCDelayTime::create(2.0f),
					CCCallFunc::create(this, callfunc_selector(SingleCopyResult::showUIAnimation)),
					NULL);
		this->runAction(action);

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(CCSize(670,351));
		return true;
	}
	return false;
}

void SingleCopyResult::onEnter()
{
	this->openAnim();
	UIScene::onEnter();
}

void SingleCopyResult::onExit()
{
	UIScene::onExit();
}

void SingleCopyResult::callBackCloseUi( CCObject * obj )
{
	this->closeAnim();

	MissionAndTeam * missionAndTeam = (MissionAndTeam *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMissionAndTeam);
	if (missionAndTeam)
	{
		missionAndTeam->setStateIsRobot(false);
		missionAndTeam->unschedule(schedule_selector(MissionAndTeam::updateSingCopyCheckMonst));
	}
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1902,(void *)m_clazz,(void *)m_level);
}

void SingleCopyResult::callBackEnterNextLevel( CCObject * obj )
{
	MissionAndTeam * missionAndTeam = (MissionAndTeam *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMissionAndTeam);
	if (missionAndTeam)
	{
		if (missionAndTeam->getStateIsRobot())
		{
			missionAndTeam->schedule(schedule_selector(MissionAndTeam::updateSingCopyCheckMonst),1.0f);
		}
	}

	int temp_level = m_level+1;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1901,(void *)m_clazz,(void *)temp_level);

	if (m_result == 1)
	{
		SingCopyInstance::setCurSingCopyLevel(temp_level);
	}else
	{
		SingCopyInstance::setCurSingCopyLevel(m_level);
	}

	DamageStatistics::clear();
}

void SingleCopyResult::callBackEnterCurLevel( CCObject * obj )
{
	MissionAndTeam * missionAndTeam = (MissionAndTeam *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMissionAndTeam);
	if (missionAndTeam)
	{
		if (missionAndTeam->getStateIsRobot())
		{
			missionAndTeam->schedule(schedule_selector(MissionAndTeam::updateSingCopyCheckMonst),1.0f);
		}
	}

	GameMessageProcessor::sharedMsgProcessor()->sendReq(1901,(void *)m_clazz,(void *)m_level);
	SingCopyInstance::setCurSingCopyLevel(m_level);
	DamageStatistics::clear();
}


void SingleCopyResult::update( float dt )
{
	m_nRemainTime -= 1000;
	if (m_nRemainTime <= 0)
	{
		m_nRemainTime = 0;
		this->callBackCloseUi(NULL);
	}
	char s_remainTime[20];
	sprintf(s_remainTime,"%d",m_nRemainTime/1000);
	labelCOuntTime->setText(s_remainTime);
}

void SingleCopyResult::showUIAnimation()
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	UILayer * u_layer=UILayer::create();
	u_layer->ignoreAnchorPointForPosition(false);
	u_layer->setAnchorPoint(ccp(0.5f,0.5f));
	u_layer->setPosition(ccp(670/2,351/2));
	u_layer->setContentSize(CCSizeMake(670,351));
	addChild(u_layer);

	UIImageView *mengban = UIImageView::create();
	mengban->setTexture("res_ui/zhezhao80.png");
	mengban->setScale9Enable(true);
	mengban->setScale9Size(winSize);
	mengban->setAnchorPoint(ccp(0.5f,0.5f));
	mengban->setPosition(ccp(670/2,351/2));
	m_pUiLayer->addWidget(mengban);
	
	//根据动画名称创建动画精灵
	CCArmature *armature ;
	if (m_result == 1)
	{
		//从导出文件异步加载动画
		CCArmatureDataManager::sharedArmatureDataManager()->addArmatureFileInfo("res_ui/uiflash/winLogo/winLogo0.png","res_ui/uiflash/winLogo/winLogo0.plist","res_ui/uiflash/winLogo/winLogo.ExportJson");
		armature = CCArmature::create("winLogo");
		armature->getAnimation()->play("Animation_begin");
		armature->getAnimation()->setMovementEventCallFunc(this,movementEvent_selector(SingleCopyResult::ArmatureFinishCallback));  
	}
	else
	{	
		//从导出文件异步加载动画
		CCArmatureDataManager::sharedArmatureDataManager()->addArmatureFileInfo("res_ui/uiflash/lostLogo/lostLogo0.png",
																"res_ui/uiflash/lostLogo/lostLogo0.plist",
															"res_ui/uiflash/lostLogo/lostLogo.ExportJson");
		armature = CCArmature::create("lostLogo");
		//播放指定动作
		armature->getAnimation()->playByIndex(0,-1,-1,ANIMATION_NO_LOOP);
	}
	//修改属性
	armature->setScale(1.0f);
	//设置动画精灵位置
	armature->setPosition(ccp(670/2,351/2));
	//添加到当前页面
	u_layer->addChild(armature);

	CCSequence * sequence = CCSequence::create(CCScaleTo::create(RESULT_UI_RESULTFLAG_SCALECHANGE_TIME,1.0f,1.0f),
		CCDelayTime::create(2.3f),
		CCCallFunc::create(this,callfunc_selector(SingleCopyResult::createUI)),
		NULL);
	
	this->runAction(sequence);
	/*
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	UIImageView *mengban = UIImageView::create();
	mengban->setTexture("res_ui/zhezhao80.png");
	mengban->setScale9Enable(true);
	mengban->setScale9Size(winSize);
	mengban->setAnchorPoint(ccp(.5f,.5f));
	m_pUiLayer->addWidget(mengban);
	mengban->setPosition(ccp(670/2,351/2));

	if (m_result == 1)
	{
		UIImageView * imageView_light = UIImageView::create();
		imageView_light->setTexture("res_ui/lightlight.png");
		imageView_light->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_light->setPosition(ccp(670/2,351/2));
		layer_anm->addWidget(imageView_light); 

		CCRotateBy * rotateAnm = CCRotateBy::create(4.0f,360);
		CCRepeatForever * repeatAnm = CCRepeatForever::create(CCSequence::create(rotateAnm,NULL));
		imageView_light->runAction(repeatAnm);

		
		CCSequence * sequence_light = CCSequence::create(
			CCDelayTime::create(RESULT_UI_RESULTFLAG_SCALECHANGE_TIME),
			CCShow::create(),
			CCDelayTime::create(RESULT_UI_RESULTFLAG_DELAY_TIME),
			CCMoveTo::create(RESULT_UI_RESULTFLAG_MOVE_TIME,ccp(670/2,351)),
			NULL);
		imageView_light->runAction(sequence_light);
	}

	UIImageView * imageView_result = UIImageView::create();
	if (m_result == 0)
	{
		imageView_result->setTexture("res_ui/jiazuzhan/lost.png");
	}
	else
	{
		imageView_result->setTexture("res_ui/jiazuzhan/win.png");
	}
	imageView_result->setAnchorPoint(ccp(0.5f,0.5f));
	imageView_result->setPosition(ccp(670/2,351/2));
	imageView_result->setScale(RESULT_UI_RESULTFLAG_BEGINSCALE);
	layer_anm->addWidget(imageView_result);

	CCSequence * sequence = CCSequence::create(CCScaleTo::create(RESULT_UI_RESULTFLAG_SCALECHANGE_TIME,1.0f,1.0f),
		CCDelayTime::create(RESULT_UI_RESULTFLAG_DELAY_TIME),
		CCMoveTo::create(RESULT_UI_RESULTFLAG_MOVE_TIME,ccp(670/2,351)),
		CCDelayTime::create(0.05f),
		CCCallFunc::create(this,callfunc_selector(SingleCopyResult::createUI)),
		NULL);
	imageView_result->runAction(sequence);
	*/
}

void SingleCopyResult::startSlowMotion()
{
	CCDirector::sharedDirector()->getScheduler()->setTimeScale(0.2f);
}

void SingleCopyResult::endSlowMotion()
{
	CCDirector::sharedDirector()->getScheduler()->setTimeScale(1.0f);
}

void SingleCopyResult::createUI()
{
	UIPanel * mainPanel = (UIPanel *)GUIReader::shareReader()->widgetFromJsonFile("res_ui/guoguanzhanjiangAccounts_1.json");
	mainPanel->setAnchorPoint(ccp(0,0));
	mainPanel->setPosition(ccp(0,-35));
	m_pUiLayer->addWidget(mainPanel);

	UILabelBMFont * clazzAndLevel_ = (UILabelBMFont *)UIHelper::seekWidgetByName(mainPanel,"LabelBMFont_24");
	UILabel * labelCurCostTime = (UILabel *)UIHelper::seekWidgetByName(mainPanel,"cueTime");
	UILabel * labelCurbestTime = (UILabel *)UIHelper::seekWidgetByName(mainPanel,"cueBestTime");
	UILabel * labelReward = (UILabel *)UIHelper::seekWidgetByName(mainPanel,"prize_money");
	labelCOuntTime = (UILabel *)UIHelper::seekWidgetByName(mainPanel,"Label_35");

	const char *strings_curLevelBmfont = StringDataManager::getString("singleCopy_curLevelLabelBmFont");
	char levelStr[50];
	sprintf(levelStr,strings_curLevelBmfont,m_level);
	CSingCopyClazz * copyMsg  = SingleCopyClazzConfig::s_SingCopyClazzMsgData[m_clazz];
	std::string clazz_level = "";
	clazz_level.append(copyMsg->get_name().c_str());
	clazz_level.append(levelStr);
	clazzAndLevel_->setText(clazz_level.c_str());

	labelCurCostTime->setText(RecuriteActionItem::timeFormatToString(m_curTime).c_str());
	labelCurbestTime->setText(RecuriteActionItem::timeFormatToString(m_bestTime).c_str());

	char rewardMoneyStr[10];
	sprintf(rewardMoneyStr,"%d",m_money);
	labelReward->setText(rewardMoneyStr);

	char s_remainTime[20];
	sprintf(s_remainTime,"%d",m_nRemainTime/1000);
	labelCOuntTime->setText(s_remainTime);

	//show general 
	int space = 25;
	int width = 66;
	int allsize = GameView::getInstance()->generalsInLineList.size()+1;
	float firstPos_x = 335- (space+width)*0.5f*(allsize-1);

	for (int i = 0;i<allsize;++i)
	{
		if (i >= 6)
			continue;

		ResultHeadPartItem * temp = ResultHeadPartItem::create(0,i,ResultHeadPartItem::type_dmgOnly);
		temp->ignoreAnchorPointForPosition(false);
		temp->setAnchorPoint(ccp(0.5f,0.5f));
		temp->setPosition(ccp(firstPos_x+(width+space)*i,80));
		layer_anm->addChild(temp);
	}

	UIButton * btn_again = (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_37");
	btn_again->setTouchEnable(true);
	btn_again->setPressedActionEnabled(true);
	btn_again->addReleaseEvent(this,coco_releaseselector(SingleCopyResult::callBackEnterCurLevel));

	UIButton * btn_next = (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_37_0");
	btn_next->setTouchEnable(true);
	btn_next->setPressedActionEnabled(true);
	btn_next->addReleaseEvent(this,coco_releaseselector(SingleCopyResult::callBackEnterNextLevel));

	UIButton * btn_close = (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_37_0_0");
	btn_close->setTouchEnable(true);
	btn_close->setPressedActionEnabled(true);
	btn_close->addReleaseEvent(this,coco_releaseselector(SingleCopyResult::callBackCloseUi));

	if (m_level >= SingCopyInstance::getMaxlevel())
	{
		btn_next->setVisible(false);
	}

	this->schedule(schedule_selector(SingleCopyResult::update),1.0f);
}

void SingleCopyResult::ArmatureFinishCallback( cocos2d::extension::CCArmature *armature, cocos2d::extension::MovementEventType movementType, const char *movementID )
{
	std::string id = movementID;
	if (id.compare("Animation_begin") == 0)
	{
		armature->stopAllActions();
		armature->getAnimation()->play("Animation1_end");
	}
}

void SingleCopyResult::addLightAnimation()
{
	CCSize size = CCDirector::sharedDirector()->getVisibleSize();
	std::string animFileName = "animation/texiao/renwutexiao/SHANGUANG/shanguang.anm";
	CCLegendAnimation *la_light = CCLegendAnimation::create(animFileName);
	if (la_light)
	{
		la_light->setPlayLoop(false);
		la_light->setReleaseWhenStop(true);
		la_light->setPlaySpeed(5.0f);
		la_light->setScale(0.85f);
		la_light->setPosition(ccp(size.width/2,size.height/2));
		GameView::getInstance()->getMainAnimationScene()->addChild(la_light);
	}
}
