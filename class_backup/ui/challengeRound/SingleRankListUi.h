
#ifndef _SINGLERANKLISTUI_H
#define _SINGLERANKLISTUI_H

#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CRankingPlayer;
#define  SelectImageTag 1011
class SingleRankListUi:public UIScene,public CCTableViewDelegate,public CCTableViewDataSource
{
public:
	SingleRankListUi(void);
	~SingleRankListUi(void);

	static SingleRankListUi * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	
	void callBackCloseUi(CCObject * obj);

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);

	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
public:
	UILabelBMFont * labelSelfRank;
	CCTableView * tableView_ranking;
	std::vector<CRankingPlayer *> rangkPlayervector;
	int curRanklistPage;
	int everyPageNum;
	bool booIsHaveNext;
private:
	CCSize winsize;
	int lastSelectCellId;
};


///////////////ranklist
class RankListMenu:public UIScene
{
public:
	RankListMenu(void);
	~RankListMenu(void);

	static RankListMenu *create(int idx);
	bool init(int idx);

	void onEnter();
	void onExit();

	void callBackList(CCObject * obj);
	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
private:
	int m_index;

	struct FriendStruct
	{
		int operation;
		int relationType;
		long long playerid;
		std::string playerName;
	};
};




#endif