#include "RankRewardUi.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../../messageclient/element/CTRankingPrize.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../backpackscene/GoodsItemInfoBase.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/extensions/CCMoveableMenu.h"
#include "ChallengeRoundUi.h"


RankRewardUi::RankRewardUi(void)
{
	m_clazz = 1;
}


RankRewardUi::~RankRewardUi(void)
{
}

RankRewardUi * RankRewardUi::create(int clazz)
{
	RankRewardUi * rewardui = new RankRewardUi();
	if (rewardui && rewardui->init(clazz))
	{
		rewardui->autorelease();
		return rewardui;
	}
	CC_SAFE_DELETE(rewardui);
	return NULL;
}

bool RankRewardUi::init(int clazz)
{
	if (UIScene::init())
	{
		winsize = CCDirector::sharedDirector()->getVisibleSize();

// 		UIImageView * imageSp = UIImageView::create();
// 		imageSp->setTexture("res_ui/LV4_red.png");
// 		imageSp->setScale9Enable(true);
// 		imageSp->setScale9Size(CCSizeMake(200,300));
// 		imageSp->setAnchorPoint(ccp(0,0));
// 		imageSp->setPosition(ccp(0,0));
// 		m_pUiLayer->addWidget(imageSp);
		
		UIPanel * ppanel = (UIPanel *)GUIReader::shareReader()->widgetFromJsonFile("res_ui/familyRankReward_1.json");
		ppanel->setAnchorPoint(ccp(0.0f,0.0f));
		ppanel->setPosition(CCPointZero);	
		ppanel->setTouchEnable(true);
		m_pUiLayer->addWidget(ppanel);

		label_selfRank = (UILabel *)UIHelper::seekWidgetByName(ppanel,"label_singCopyRankValue");
		labelBmFont_clazzName = (UILabelBMFont *)UIHelper::seekWidgetByName(ppanel,"LabelBMFont_Clazzname");

		this->setClazz(clazz);
		labelBmFont_clazzName->setText(ChallengeRoundUi::getClazzName(this->getClazz()).c_str());

		tableView_reward = CCTableView::create(this,CCSizeMake(275,250));
		tableView_reward->setDirection(kCCScrollViewDirectionVertical);
		tableView_reward->setAnchorPoint(ccp(0,0));
		tableView_reward->setPosition(ccp(49,35));
		tableView_reward->setDelegate(this);
		tableView_reward->setVerticalFillOrder(kCCTableViewFillTopDown);
		addChild(tableView_reward);

		UIButton * btnClose = (UIButton *)UIHelper::seekWidgetByName(ppanel,"Button_close");
		btnClose->setTouchEnable(true);
		btnClose->setPressedActionEnabled(true);
		btnClose->addReleaseEvent(this,coco_releaseselector(RankRewardUi::callBackClose));

		this->setContentSize(ppanel->getContentSize());
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		return true;
	}
	return false;
}

void RankRewardUi::onEnter()
{
	UIScene::onEnter();
}

void RankRewardUi::onExit()
{
	this->openAnim();
	UIScene::onExit();
}

void RankRewardUi::callBackClose(CCObject * obj)
{
	this->closeAnim();
}

void RankRewardUi::scrollViewDidScroll( CCScrollView* view )
{

}

void RankRewardUi::scrollViewDidZoom( CCScrollView* view )
{

}

void RankRewardUi::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{
	
}

cocos2d::CCSize RankRewardUi::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeMake(275,64);
}

cocos2d::extension::CCTableViewCell* RankRewardUi::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell = new CCTableViewCell();
	cell->autorelease();

	PrizeCell * prizecell_ = PrizeCell::create(idx);
	prizecell_->ignoreAnchorPointForPosition(false);
	prizecell_->setAnchorPoint(ccp(0,0));
	prizecell_->setPosition(ccp(0,0));
	cell->addChild(prizecell_);

	return cell;
}

unsigned int RankRewardUi::numberOfCellsInTableView( CCTableView *table )
{
	return rankPrizevector.size();
}

bool RankRewardUi::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return true;
}

void RankRewardUi::setClazz( int clazz_ )
{
	m_clazz = clazz_;
}

int RankRewardUi::getClazz()
{
	return m_clazz;
}


/////////////////////////////
PrizeCell::PrizeCell( void )
{

}

PrizeCell::~PrizeCell( void )
{

}

PrizeCell * PrizeCell::create( int idx )
{
	PrizeCell * cell_ = new PrizeCell();
	if (cell_ && cell_->init(idx))
	{
		cell_->autorelease();
		return cell_;
	}
	CC_SAFE_DELETE(cell_);
	return NULL;
}

bool PrizeCell::init( int idx )
{
	if (CCTableViewCell::init())
	{
		RankRewardUi * rankui_ = (RankRewardUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagSingCopyReward);
		
		CCScale9Sprite * sprite_bigFrame = CCScale9Sprite::create("res_ui/LV3_dikuang.png");
		sprite_bigFrame->setAnchorPoint(ccp(0, 0));
		sprite_bigFrame->setPosition(ccp(0, 0));
		sprite_bigFrame->setCapInsets(CCRectMake(9,14,1,1));
		sprite_bigFrame->setPreferredSize(CCSizeMake(273,62));
		addChild(sprite_bigFrame);

		int starlevel_ = rankui_->rankPrizevector.at(idx)->startranking();
		int endlevel_ = rankui_->rankPrizevector.at(idx)->endranking();
		int offx_ = 0;
		char starStr[10];
		sprintf(starStr,"%d",starlevel_);
		char endStr[10];
		sprintf(endStr,"%d",endlevel_);

		if (starlevel_ == endlevel_)
		{
			std::string icon_path = "res_ui/rank";
			icon_path.append(starStr);
			icon_path.append(".png");

			CCSprite * sp_ranking = CCSprite::create(icon_path.c_str());
			sp_ranking->setAnchorPoint(ccp(0.5f,0.5f));
			sp_ranking->setPosition(ccp(sp_ranking->getContentSize().width,32));
			addChild(sp_ranking);

			offx_ = sp_ranking->getPosition().x + sp_ranking->getContentSize().width;
		}else
		{
			const char * string_ = StringDataManager::getString("singCopyReward_sortName");
			const char * string_levelAndlevel = StringDataManager::getString("singCopy_levelAndLevel_");
			
			std::string rankStirng_ = starStr;
			rankStirng_.append(string_levelAndlevel);
			rankStirng_.append(endStr);
			rankStirng_.append(string_);

			CCLabelTTF * label_ranking = CCLabelTTF::create();
			label_ranking->setString(rankStirng_.c_str());
			label_ranking->setAnchorPoint(ccp(0.5f,0.5f));
			label_ranking->setFontSize(16);
			label_ranking->setColor(ccc3(47, 93, 13));
			label_ranking->setPosition(ccp(34,32));			
			addChild(label_ranking);
			offx_ = label_ranking->getPosition().x + label_ranking->getContentSize().width;
		}
		offx_ = 80;
		CCSprite * goodsBg_ =CCSprite::create("res_ui/sdi_orange.png");
		CCMenuItemSprite * item_goodsBg = CCMenuItemSprite::create(goodsBg_, goodsBg_, goodsBg_, this, menu_selector(PrizeCell::callBackShowGoodsInfo));
		item_goodsBg->setTag(idx);
		item_goodsBg->setZoomScale(1.0f);
		CCMoveableMenu * menu_goods =CCMoveableMenu::create(item_goodsBg,NULL);
		menu_goods->setAnchorPoint(ccp(0,0));
		menu_goods->setPosition(ccp(offx_ + goodsBg_->getContentSize().width/2,32));
		menu_goods->setScale(0.8f);
		//menu_goods->setTag(idx);
		addChild(menu_goods);
		
		std::string goodsInfoStr_ = "res_ui/props_icon/";
		goodsInfoStr_.append(rankui_->rankPrizevector.at(idx)->goods().icon());
		goodsInfoStr_.append(".png");

		CCSprite * sp_goodsIcon = CCSprite::create(goodsInfoStr_.c_str());
		sp_goodsIcon->setAnchorPoint(ccp(0.5f,0.5f));
		sp_goodsIcon->setPosition(ccp(menu_goods->getPositionX(),32));
		addChild(sp_goodsIcon);
		
		if (rankui_->rankPrizevector.at(idx)->canreceive())
		{
			CCScale9Sprite * prize_bgsp =CCScale9Sprite::create("res_ui/new_button_2.png");
			prize_bgsp->setPreferredSize(CCSizeMake(80,42));
			prize_bgsp->setCapInsets(CCRectMake(18,9,2,23));
			CCMenuItemSprite * item_prize = CCMenuItemSprite::create(prize_bgsp, prize_bgsp, prize_bgsp, this, menu_selector(PrizeCell::callBackGetPrize));
			item_prize->setZoomScale(0.5f);
			CCMoveableMenu * menu_prize = CCMoveableMenu::create(item_prize,NULL);
			menu_prize->setAnchorPoint(ccp(0,0));
			menu_prize->setPosition(ccp(menu_goods->getPosition().x + goodsBg_->getContentSize().width + prize_bgsp->getContentSize().width/2,32));
			menu_prize->setTag(1010);
			addChild(menu_prize);

			const char * string_prize = StringDataManager::getString("singCopyReward_getPrize");
			CCLabelTTF * label_Prize = CCLabelTTF::create();
			label_Prize->setAnchorPoint(ccp(0.5f,0.5f));
			label_Prize->setPosition(ccp(menu_prize->getPositionX(),32));
			label_Prize->setFontSize(16);
			label_Prize->setString(string_prize);
			addChild(label_Prize);
		}
		
		return true;
	}
	return false;
}

void PrizeCell::onEnter()
{
	CCTableViewCell::onEnter();
}

void PrizeCell::onExit()
{
	CCTableViewCell::onExit();
}

void PrizeCell::callBackShowGoodsInfo( CCObject * obj )
{
	CCMenuItemSprite * btn_ = (CCMenuItemSprite *)obj;
	int index_ = btn_->getTag();
	
	RankRewardUi * rankui_ = (RankRewardUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagSingCopyReward);
	GoodsInfo * goodsInfo_ = new GoodsInfo();
	goodsInfo_->CopyFrom(rankui_->rankPrizevector.at(index_)->goods());

	GoodsItemInfoBase * goodsInfo = GoodsItemInfoBase::create(goodsInfo_,GameView::getInstance()->EquipListItem,0);
	goodsInfo->ignoreAnchorPointForPosition(false);
	goodsInfo->setAnchorPoint(ccp(0.5f,0.5f));
	GameView::getInstance()->getMainUIScene()->addChild(goodsInfo);
	delete goodsInfo_;
}

void PrizeCell::callBackGetPrize( CCObject * obj )
{
	RankRewardUi * rankui_ = (RankRewardUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagSingCopyReward);
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1906,(void *)rankui_->getClazz());
}
