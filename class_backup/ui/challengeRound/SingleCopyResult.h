
#ifndef SINGLECOPYRESULT_H_
#define SINGLECOPYRESULT_H_

#include "../extensions/UIScene.h"
class SingleCopyResult:public UIScene
{
public:
	SingleCopyResult(void);
	~SingleCopyResult(void);

	static SingleCopyResult * create(int clazz,int level,int curTime,int bestTime,int money,int result);
	bool init(int clazz,int level,int curTime,int bestTime,int money,int result);

	void onEnter();
	void onExit();

	void showUIAnimation();
	void startSlowMotion();
	void endSlowMotion();
	void addLightAnimation();
	void createUI();
	virtual void update(float dt);

	void callBackCloseUi(CCObject * obj);

	void callBackEnterNextLevel(CCObject * obj);
	void callBackEnterCurLevel(CCObject * obj);
	
	void ArmatureFinishCallback(cocos2d::extension::CCArmature *armature, cocos2d::extension::MovementEventType movementType, const char *movementID);

private:
	CCSize winsize;
	UILayer *layer_anm;
	int m_clazz;
	int m_level;
	int m_curTime;
	int m_bestTime;
	int m_money;
	int m_result;
	
	int m_nRemainTime;
	UILabel * labelCOuntTime;



};

#endif