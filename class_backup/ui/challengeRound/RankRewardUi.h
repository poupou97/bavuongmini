
#ifndef CHALLENGEROUDUI_RANREWARDUI_H
#define CHALLENGEROUDUI_RANREWARDUI_H

#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;
class CTRankingPrize;

class RankRewardUi:public UIScene,public CCTableViewDelegate,public CCTableViewDataSource
{
public:
	RankRewardUi(void);
	~RankRewardUi(void);

	static RankRewardUi * create(int clazz);
	
	bool init(int clazz);

	void onEnter();
	void onExit();

	void callBackClose(CCObject * obj);

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);

	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);

	void setClazz(int clazz_);
	int getClazz();
public: 
	std::vector<CTRankingPrize *> rankPrizevector;
	UILabel * label_selfRank;
	UILabelBMFont * labelBmFont_clazzName;
	CCTableView * tableView_reward;
private:
	CCSize winsize;
	int m_clazz;
};
/////////
class PrizeCell:public CCTableViewCell
{
public:
	PrizeCell(void);
	~PrizeCell(void);

	static PrizeCell *create(int idx);
	bool init(int idx);

	void onEnter();
	void onExit();

	void callBackShowGoodsInfo(CCObject * obj);
	void callBackGetPrize(CCObject * obj);

private:
	CCSize winsize;
};

#endif