#include "ChallengeRoundUi.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "ShowLevelUi.h"
#include "GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/CSingCopyClazz.h"
#include "../../utils/StaticDataManager.h"
#include "../../AppMacros.h"
#include "../extensions/UITab.h"
#include "../../legend_script/CCTeachingGuide.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/ScriptManager.h"


ChallengeRoundUi::ChallengeRoundUi(void)
{
	m_copyClazz = 1;
	m_copyResume = 0;
}


ChallengeRoundUi::~ChallengeRoundUi(void)
{
}

ChallengeRoundUi * ChallengeRoundUi::create()
{
	ChallengeRoundUi * challengeui = new ChallengeRoundUi();
	if (challengeui && challengeui->init())
	{
		challengeui->autorelease();
		return challengeui;
	}
	CC_SAFE_DELETE(challengeui);
	return NULL;
}

bool ChallengeRoundUi::init()
{
	if (UIScene::init())
	{
		winsize= CCDirector::sharedDirector()->getVisibleSize();

		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winsize);
		mengban->setAnchorPoint(ccp(0,0));
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);

		UILayer * m_Layer= UILayer::create();
		m_Layer->ignoreAnchorPointForPosition(false);
		m_Layer->setAnchorPoint(ccp(0.5f,0.5f));
		m_Layer->setPosition(ccp(winsize.width/2,winsize.height/2));
		m_Layer->setContentSize(CCSizeMake(800,480));
		m_Layer->setZOrder(10);
		this->addChild(m_Layer);

		const char * firstStr = StringDataManager::getString("ChalllengeRoundUi_diaoshi_guo");
		const char * secondStr = StringDataManager::getString("ChalllengeRoundUi_diaoshi_guan");
		const char * thirdStr = StringDataManager::getString("ChalllengeRoundUi_diaoshi_zhan");
		const char * fourStr = StringDataManager::getString("ChalllengeRoundUi_diaoshi_jiang");

		CCArmature * atmature = MainScene::createPendantAnm(firstStr,secondStr,thirdStr,fourStr);
		atmature->setPosition(ccp(30,240));
		m_Layer->addChild(atmature);

		UIPanel * mainPanel = (UIPanel *)GUIReader::shareReader()->widgetFromJsonFile("res_ui/guoguanzhanjiang_1.json");
		mainPanel->setAnchorPoint(ccp(0.5f,0.5f));
		mainPanel->setPosition(ccp(winsize.width/2,winsize.height/2));
		m_pUiLayer->addWidget(mainPanel);

		UIButton * btn_close = (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_close");
		btn_close->setTouchEnable(true);
		btn_close->setPressedActionEnabled(true);
		btn_close->addReleaseEvent(this,coco_releaseselector(ChallengeRoundUi::callBackCloseUi));

		UILayer * layerClazz = UILayer::create();
		layerClazz->ignoreAnchorPointForPosition(false);
		layerClazz->setAnchorPoint(ccp(0.5f, 0.5f));
		layerClazz->setContentSize(CCSizeMake(800, 480));
		layerClazz->setPosition(ccp(winsize.width/2, winsize.height/2));
		this->addChild(layerClazz);

// 		const char * normalImage = "res_ui/tab_3_off.png";
// 		const char * selectImage = "res_ui/tab_3_on.png";
// 		const char * finalImage = "";
// 		const char * highLightImage = "res_ui/tab_3_on.png";
// 		const char *str1 = StringDataManager::getString("ActiveUi_ChallengeRoundTab");
// 		char* p1 = const_cast<char*>(str1);
// 		char* pHightLightImage = const_cast<char*>(highLightImage);
// 		char * mainNames[] = {p1};
// 		UITab * m_tab = UITab::createWithBMFont(1, normalImage, selectImage, finalImage, mainNames,"res_ui/font/ziti_1.fnt",HORIZONTAL,5);
// 		m_tab->setAnchorPoint(ccp(0,0));
// 		m_tab->setPosition(ccp(74, 408));
// 		m_tab->setHighLightImage(pHightLightImage);
// 		m_tab->setDefaultPanelByIndex(0);
// 		m_tab->addIndexChangedEvent(this, coco_indexchangedselector(ChallengeRoundUi::tabIndexChangedEvent));
// 		m_tab->setPressedActionEnabled(true);
// 		layerClazz->addWidget(m_tab);

		CCTableView * copyClazzTableView = CCTableView::create(this, CCSizeMake(665,367));
		copyClazzTableView->setPressedActionEnabled(true);
		copyClazzTableView->setDirection(kCCScrollViewDirectionHorizontal);
		copyClazzTableView->setAnchorPoint(ccp(0,0));
		copyClazzTableView->setPosition(ccp(75,60));
		copyClazzTableView->setDelegate(this);
		copyClazzTableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		layerClazz->addChild(copyClazzTableView);

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(winsize);
		return true;
	}
	return false;
}

void ChallengeRoundUi::onEnter()
{
	this->openAnim();
	UIScene::onEnter();
}

void ChallengeRoundUi::onExit()
{
	UIScene::onExit();
}

void ChallengeRoundUi::scrollViewDidScroll( cocos2d::extension::CCScrollView * view )
{

}

void ChallengeRoundUi::scrollViewDidZoom( cocos2d::extension::CCScrollView* view )
{

}

void ChallengeRoundUi::tableCellTouched( cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell )
{
	int cellIndex = cell->getIdx()+1;
	this->setCopyClazz(cellIndex);


	if (cell->getIdx() == 0)
	{
		Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if(sc != NULL)
			sc->endCommand(this);
	}
	
	CSingCopyClazz * copyMsg  = SingleCopyClazzConfig::s_SingCopyClazzMsgData[this->getCopyClazz()];
	this->setCopyCostResume(copyMsg->get_resume());

	ShowLevelUi * levelui_ = ShowLevelUi::create(cellIndex);
	levelui_->ignoreAnchorPointForPosition(false);
	levelui_->setAnchorPoint(ccp(0.5f,0.5f));
	levelui_->setPosition(ccp(winsize.width/2,winsize.height/2));
	GameView::getInstance()->getMainUIScene()->addChild(levelui_);
	levelui_->setTag(ktagSingCopylevel);

	GameMessageProcessor::sharedMsgProcessor()->sendReq(1903,levelui_);
}

cocos2d::CCSize ChallengeRoundUi::tableCellSizeForIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	return CCSizeMake(220,360);
}

cocos2d::extension::CCTableViewCell* ChallengeRoundUi::tableCellAtIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	CCString * string =CCString::createWithFormat("%d",idx);	
	CCTableViewCell *cell =table->dequeueCell();
	cell=new CCTableViewCell();
	cell->autorelease();

	CSingCopyClazz * copyMsg  = SingleCopyClazzConfig::s_SingCopyClazzMsgData[idx+1];
	if (copyMsg)
	{
		int clazz_ = idx+1;
		int starLevel = copyMsg->get_starLevel();
		int endLevel = copyMsg->get_endLevel();
		std::string name_ =copyMsg->get_name();
		std::string icon_ = copyMsg->get_icon();
		int resume_ = copyMsg->get_resume();
		std::string rewardInfo_= copyMsg->get_reward();

		std::string clazzNamePath_ = "res_ui/guoguanzhanjiang/";
		std::string mapNamepath = "res_ui/guoguanzhanjiang/";
		switch(clazz_)
		{
		case 1:
			{
				clazzNamePath_.append("chushiniudao.png");
				mapNamepath.append("pic1.png");
			}break;
		case 2:
			{
				clazzNamePath_.append("xiaoyoumingqi.png");
				mapNamepath.append("pic2.png");
			}break;
		case 3:
			{
				clazzNamePath_.append("jiangongliye.png");
				mapNamepath.append("pic3.png");
			}break;
		}

		CCScale9Sprite * spbg = CCScale9Sprite::create("res_ui/LV4_dia.png");
		spbg->setPreferredSize(CCSizeMake(208,344));
		spbg->setCapInsets(CCRectMake(19,20,1,1));
		spbg->setAnchorPoint(ccp(0,0));
		spbg->setPosition(ccp(10,0));
		cell->addChild(spbg);
		/*
		const char* resume_str = StringDataManager::getString("singleCopy_clazz_Costpower");
		CCLabelBMFont * labelresume_ = CCLabelBMFont::create(resume_str,"res_ui/font/ziti_3.fnt");
		labelresume_->setAnchorPoint(ccp(0,0));
		labelresume_->setPosition(ccp(20,10));
		spbg->addChild(labelresume_);

		char resumeValue[10];
		sprintf(resumeValue,"%d",resume_);
		CCLabelTTF  * labelresume_value=CCLabelTTF::create(resumeValue,APP_FONT_NAME,18);
		labelresume_value->setAnchorPoint(ccp(0,0));
		labelresume_value->setPosition(ccp(labelresume_->getPositionX()+labelresume_->getContentSize().width,labelresume_->getPositionY()+2));
		spbg->addChild(labelresume_value);
		*/
		const char* reward_str = StringDataManager::getString("singleCopy_clazz_rewardInfo");
		CCLabelBMFont * labeleReward_ = CCLabelBMFont::create(reward_str,"res_ui/font/ziti_3.fnt");
		labeleReward_->setAnchorPoint(ccp(0,0));
		labeleReward_->setPosition(ccp(20,labeleReward_->getContentSize().height));
		spbg->addChild(labeleReward_);

		CCLabelTTF  * labeleReward_value=CCLabelTTF::create(rewardInfo_.c_str(),APP_FONT_NAME,18);
		labeleReward_value->setAnchorPoint(ccp(0,0));
		labeleReward_value->setPosition(ccp(labeleReward_->getPositionX() + labeleReward_->getContentSize().width,labeleReward_->getPositionY()+2));
		spbg->addChild(labeleReward_value);
		//
		const char* recommendLevel_str = StringDataManager::getString("singleCopy_clazz_recommend");
		CCLabelBMFont * labeleRecommendLevel_ = CCLabelBMFont::create(recommendLevel_str,"res_ui/font/ziti_3.fnt");
		labeleRecommendLevel_->setAnchorPoint(ccp(0,0));
		labeleRecommendLevel_->setPosition(ccp(labeleReward_->getPositionX(),labeleReward_->getPositionY()+labeleReward_->getContentSize().height));
		spbg->addChild(labeleRecommendLevel_);

		std::string recommendLevelStrint = "";
		char recommendStarLevel[10];
		sprintf(recommendStarLevel,"%d",starLevel);
		recommendLevelStrint.append(recommendStarLevel);
		recommendLevelStrint.append("-");
		char recommendEndLevel[10];
		sprintf(recommendEndLevel,"%d",endLevel);
		recommendLevelStrint.append(recommendEndLevel);

		CCLabelTTF  * labeleRecommendLevel_value=CCLabelTTF::create(recommendLevelStrint.c_str(),APP_FONT_NAME,18);
		labeleRecommendLevel_value->setAnchorPoint(ccp(0,0));
		labeleRecommendLevel_value->setPosition(ccp(labeleRecommendLevel_->getPositionX()+labeleRecommendLevel_->getContentSize().width,labeleRecommendLevel_->getPositionY()+2));
		spbg->addChild(labeleRecommendLevel_value);
		//
		CCScale9Sprite * spbg_firstBoundary = CCScale9Sprite::create("res_ui/LV4_dibian.png");
		spbg_firstBoundary->setPreferredSize(CCSizeMake(208,35));
		spbg_firstBoundary->setCapInsets(CCRectMake(19,35,1,1));
		spbg_firstBoundary->setAnchorPoint(ccp(0,0));
		spbg_firstBoundary->setPosition(ccp(0,labeleRecommendLevel_->getPositionY()+ labeleRecommendLevel_->getContentSize().height));
		spbg->addChild(spbg_firstBoundary,1);

		CCScale9Sprite * spbg_firstMapIcon = CCScale9Sprite::create(mapNamepath.c_str());
		spbg_firstMapIcon->setAnchorPoint(ccp(0,0));
		spbg_firstMapIcon->setPosition(ccp(3,spbg_firstBoundary->getPositionY()+ spbg_firstBoundary->getContentSize().height/2));
		spbg_firstMapIcon->setScaleX(201/spbg_firstMapIcon->getContentSize().width);
		spbg_firstMapIcon->setScaleY(200/spbg_firstMapIcon->getContentSize().height);
		spbg->addChild(spbg_firstMapIcon);

		CCScale9Sprite * spbg_secondBoundary = CCScale9Sprite::create("res_ui/LV4_dibian.png");
		spbg_secondBoundary->setPreferredSize(CCSizeMake(208,35));
		spbg_secondBoundary->setCapInsets(CCRectMake(19,35,1,1));
		spbg_secondBoundary->setAnchorPoint(ccp(0,0));
		spbg_secondBoundary->setPosition(ccp(0,spbg_firstMapIcon->getPositionY()+180));
		spbg->addChild(spbg_secondBoundary,1);

		CCScale9Sprite * spbg_firstClazzName = CCScale9Sprite::create(clazzNamePath_.c_str());
		spbg_firstClazzName->setAnchorPoint(ccp(0,0));
		spbg_firstClazzName->setPosition(ccp(45,spbg_secondBoundary->getPositionY()+ spbg_secondBoundary->getContentSize().height/2+spbg_firstClazzName->getContentSize().height/2 - 5));
		spbg->addChild(spbg_firstClazzName);


		/*
		CCLabelTTF  * labeleClazzName=CCLabelTTF::create(name_.c_str(),APP_FONT_NAME,16);
		labeleClazzName->setAnchorPoint(ccp(0.5f,0.5f));
		labeleClazzName->setPosition(ccp(spbg->getContentSize().width/2,spbg->getContentSize().height/2));
		spbg->addChild(labeleClazzName);
		*/
	}
	return cell;
}

unsigned int ChallengeRoundUi::numberOfCellsInTableView( cocos2d::extension::CCTableView *table )
{
	return SingleCopyClazzConfig::s_SingCopyClazzMsgData.size();
}

void ChallengeRoundUi::ccTouchMoved( CCTouch *pTouch, CCEvent *pEvent )
{

}

void ChallengeRoundUi::ccTouchEnded( CCTouch *pTouch, CCEvent *pEvent )
{

}

void ChallengeRoundUi::ccTouchCancelled( CCTouch *pTouch, CCEvent *pEvent )
{

}

void ChallengeRoundUi::callBackCloseUi(CCObject *obj)
{
	this->closeAnim();
}

void ChallengeRoundUi::setCopyClazz( int value_ )
{
	m_copyClazz = value_;
}

int ChallengeRoundUi::getCopyClazz()
{
	return m_copyClazz;
}

void ChallengeRoundUi::setCopyCostResume( int value_ )
{
	m_copyResume = value_;
}

int ChallengeRoundUi::getCopyCostResume()
{
	return m_copyResume;
}

std::string ChallengeRoundUi::getClazzName( int clazz_ )
{
	std::string clazzName ="";
	switch(clazz_)
	{
	case 1:
		{
			const char* string_ = StringDataManager::getString("singCopy_firstClazzName_chushiniudao");
			clazzName.append(string_);
		}break;
	case 2:
		{
			const char* string_ = StringDataManager::getString("singCopy_secondClazzName_xiaoyoumingqi");
			clazzName.append(string_);
		}break;
	case 3:
		{
			const char* string_ = StringDataManager::getString("singCopy_thirdClazzName_jiangongliye");
			clazzName.append(string_);
		}break;
	}
	return clazzName;
}

std::string ChallengeRoundUi::getClazzLevel( int clazz )
{
	std::string clazzLevel ="";
	switch(clazz)
	{
	case 1:
		{
			const char* string_ = StringDataManager::getString("singCopy_firstClazz_level_");
			clazzLevel.append(string_);
		}break;
	case 2:
		{
			const char* string_ = StringDataManager::getString("singCopy_secondClazz_level_");
			clazzLevel.append(string_);
		}break;
	case 3:
		{
			const char* string_ = StringDataManager::getString("singCopy_thirdClazz_level_");
			clazzLevel.append(string_);
		}break;
	}
	return clazzLevel;
}

void ChallengeRoundUi::tabIndexChangedEvent( CCObject * obj )
{

}

void ChallengeRoundUi::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void ChallengeRoundUi::addCCTutorialIndicator( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction)
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height-480)/2;

	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,215,360,true);
	tutorialIndicator->setDrawNodePos(ccp(pos.x+190+_w,pos.y+235+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void ChallengeRoundUi::removeCCTutorialIndicator()
{
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		CCTeachingGuide* teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}
