#include "OnlineGiftLayer.h"
#include "GameView.h"
#include "OnlineGiftData.h"
#include "../../messageclient/element/COneOnlineGift.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/GameUIConstant.h"
#include "../../ui/Reward_ui/RewardUi.h"
#include "../../messageclient/element/CRewardBase.h"

OnlineGiftLayer * OnlineGiftLayer::s_onLineGiftLayer = NULL;

OnlineGiftLayer::OnlineGiftLayer(void)
	:m_bIsUseful(false)
	,m_nOnlineGiftTime(0)
	,m_nTimeShow(0)
	,m_timeDelta(0.0f)
	,m_str_timeLeft("")
{

}

OnlineGiftLayer::~OnlineGiftLayer(void)
{
	OnlineGiftData::instance()->getGiftDataTimeFromIcon(m_nTimeShow, m_nGiftNum);
}

OnlineGiftLayer * OnlineGiftLayer::instance()
{
	if (NULL == s_onLineGiftLayer)
	{
		s_onLineGiftLayer = OnlineGiftLayer::create();
	}

	return s_onLineGiftLayer;
}

OnlineGiftLayer* OnlineGiftLayer::create()
{
	OnlineGiftLayer * pOnlineGiftLayer = new OnlineGiftLayer();
	if (pOnlineGiftLayer && pOnlineGiftLayer->init())
	{
		pOnlineGiftLayer->autorelease();
		return pOnlineGiftLayer;
	}
	CC_SAFE_DELETE(pOnlineGiftLayer);
	return NULL;
}

bool OnlineGiftLayer::init()
{
	bool bRet=false;
	do 
	{
		CC_BREAK_IF(!CCLayer::init());

		//this->scheduleUpdate();
		this->schedule(schedule_selector(OnlineGiftLayer::update), 1.0f);

		bRet = true;

	} while (0);

	return bRet;
}

void OnlineGiftLayer::update(float dt)
{
	m_timeDelta += dt;
	if (m_timeDelta > 1)
	{
		m_timeDelta -= 1;
	}
	else
	{
		/*char str_time[20];
		sprintf(str_time, "%f", m_timeDelta);
		GameView::getInstance()->showAlertDialog(str_time);*/

		return;
	}

	m_nOnlineGiftTime -= 1000;
	m_nTimeShow = m_nOnlineGiftTime;
	if (m_nTimeShow <= 0)
	{
		m_nTimeShow = 0;
	}

	refreshData();
}

void OnlineGiftLayer::setLayerUserful( bool bFlag )
{
	this->m_bIsUseful = bFlag;
}

void OnlineGiftLayer::initGiftTime( const int nTime )
{
	m_nTimeShow = nTime;
	m_nOnlineGiftTime = nTime;
}

void OnlineGiftLayer::initGiftNum( const int nGiftNum )
{
	m_nGiftNum = nGiftNum;
}

void OnlineGiftLayer::initDataFromIntent()
{
	// 在线奖励ICON 获取数据
	this->initGiftTime(OnlineGiftData::instance()->initGetGiftNeedTime());
	this->initGiftNum(OnlineGiftData::instance()->initGetGiftNum());
}

void OnlineGiftLayer::refreshData()
{
	std::string strShow = timeFormatToString(m_nTimeShow);

	// 赋值给 m_str_timeLeft;
	m_str_timeLeft = strShow;

	std::string timeString = "";
	const char *str1 = StringDataManager::getString("label_timeout");
	timeString.append(str1);

	//GameView::getInstance()->showAlertDialog(strShow);

	//判断 将要显示的是否为“可领取”，若是，则创建特效；否则 移除特效（如果先前的特效存在的话）
	if (strShow == timeString)
	{
		// 接口处 留给振兴哥用
		//GameView::getInstance()->showAlertDialog("gift can get!!!");
		
		if (m_nGiftNum > -1 && m_nGiftNum < 8)
		{
			for (int i = 0;i<GameView::getInstance()->rewardvector.size();i++)
			{
				if (GameView::getInstance()->rewardvector.at(i)->getId() == REWARD_LIST_ID_ONLINEREWARD)
				{
					GameView::getInstance()->rewardvector.at(i)->setRewardState(1);
					RewardUi::setRewardListParticle();
				}
			}
		}
	}
	/*
	else 
	{
		// 接口处 留给振兴哥用
		//GameView::getInstance()->showAlertDialog("gift can't get");

		for (int i = 0;i<GameView::getInstance()->rewardvector.size();i++)
		{
			if (GameView::getInstance()->rewardvector.at(i)->getId() == REWARD_LIST_ID_ONLINEREWARD)
			{
				GameView::getInstance()->rewardvector.at(i)->setRewardState(0);
				RewardUi::setRewardListParticle();
			}
		}
	}
	*/
}

std::string OnlineGiftLayer::timeFormatToString( int nTime )
{
	if (0 == nTime)
	{
		std::string timeString = "";
		const char *str1 = StringDataManager::getString("label_timeout");
		timeString.append(str1);
		return timeString;
	}
	else
	{
		int int_m = nTime / 1000 / 60;
		int int_s = nTime / 1000 % 60;

		std::string timeString = "";
		char str_m[10];
		if (int_m < 10)
		{
			timeString.append("0");
		}
		sprintf(str_m,"%d",int_m);
		timeString.append(str_m);
		timeString.append(":");

		char str_s[10];
		if (int_s < 10)
		{
			timeString.append("0");
		}
		sprintf(str_s,"%d",int_s);
		timeString.append(str_s);

		return timeString;
	}
}

void OnlineGiftLayer::allGiftGetLayerExit()
{
	this->removeFromParent();
}

int OnlineGiftLayer::get_giftTime()
{
	return this->m_nTimeShow;
}

int OnlineGiftLayer::get_giftNum()
{
	return this->m_nGiftNum;
}

std::string OnlineGiftLayer::get_timeLeft()
{
	return this->m_str_timeLeft;
}




