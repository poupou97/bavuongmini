#ifndef _UI_ONLINEREWARD_ONLINEREWARDUI_H_
#define _UI_ONLINEREWARD_ONLINEREWARDUI_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

/////////////////////////////////
/**
 * 在线奖励 界面
 * @author liuliang
 * @version 0.1.0
 * @date 2014.03.11
 */

class COneOnlineGift;
class OnlineGift;
class CCTutorialParticle;

class OnLineRewardUI : public UIScene
{
public:
	OnLineRewardUI();
	virtual ~OnLineRewardUI();

	static UIPanel * s_pPanel;

	static OnLineRewardUI * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);

	void update(float dt);

public:
	void callBackBtnClolse(CCObject * obj);																	// 关闭按钮的响应
	void callBackBtnGetGift(CCObject* obj);																	// 领取奖励 按钮的响应
	void callBackBtnGiftInfo(CCObject* obj);																	// 点击奖励时的响应

public:

	void refreshData();																									// 用 服务器 中的数据初始化 UI
	int getGiftNeedTime();																								// 用服务器 中的 数据 初始化 当前正在进行奖励 所需时间
	int getGiftNum();																										// 用服务器 中的 数据 初始化 当前正在进行奖励 的编号

	void giftGetSuc(const int nGiftNum, const int nGiftStatus);										// UI界面 获取 编号为nNum的奖励成功

	void refreshTimeAndNum();

private:
	void initUI();

	void initBtnStatus(OnlineGift * onlineGift);						// 初始化Btn的状态
	void initGoodsInfo(OnlineGift* onlineGift);						// 初始化goods的信息
	void initGiftFixTime(OnlineGift * onlineGift);						// 初始化奖励的固定领取时间

	int m_nTimeShow;															// 显示在面板的 该奖励可领取的时间
	int m_nOnlineGiftTime;														// 服务器所获取的 该奖励 领取 所需时间

	int m_nGiftNum;																// 当前正在进行的 GiftNum;

	void refreshTime();															// 每秒更新 正在进行奖励的 剩余时间

	std::string timeFormatToString(int nTime);						// 时间转换函数

	void refreshGiftTime();														// 当前奖励 还需 nTime 毫秒可领取
	void refreshGiftNum();														// 当前奖励的编号

	std::string getEquipmentQualityByIndex(int quality);			// 根据 品阶 获得该goods需要的底图

	CCTutorialParticle* m_tutorialParticle;								// 环绕可领取按钮的粒子特效


	typedef enum TagBtnGet{
		TAG_BTN_GET_1 = 101,
		TAG_BTN_GET_2 = 102,
		TAG_BTN_GET_3 = 103,
		TAG_BTN_GET_4 = 104,
		TAG_BTN_GET_5 = 105,
		TAG_BTN_GET_6 = 106,
		TAG_BTN_GET_7 = 107,
		TAG_BTN_GET_8 = 108,
	};

	typedef enum TagBtnGift1{
		TAG_BTN_GIFT_1_1 = 1011,
		TAG_BTN_GIFT_2_1 = 1021,
		TAG_BTN_GIFT_3_1 = 1031,
		TAG_BTN_GIFT_4_1 = 1041,
		TAG_BTN_GIFT_5_1 = 1051,
		TAG_BTN_GIFT_6_1 = 1061,
		TAG_BTN_GIFT_7_1 = 1071,
		TAG_BTN_GIFT_8_1 = 1081,
	};

	typedef enum TagBtnGift2{
		TAG_BTN_GIFT_1_2 = 1012,
		TAG_BTN_GIFT_2_2 = 1022,
		TAG_BTN_GIFT_3_2 = 1032,
		TAG_BTN_GIFT_4_2 = 1042,
		TAG_BTN_GIFT_5_2 = 1052,
		TAG_BTN_GIFT_6_2 = 1062,
		TAG_BTN_GIFT_7_2 = 1072,
		TAG_BTN_GIFT_8_2 = 1082,
	};

	TagBtnGet m_array_tagBtnGet[8];									// tag数组
	TagBtnGift1 m_array_tagBtnGift1[8];
	TagBtnGift2 m_array_tagBtnGift2[8];


};

#endif