#ifndef _UI_ONLINEREWARD_ONLINEGIFTICON_H_
#define _UI_ONLINEREWARD_ONLINEGIFTICON_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CCTutorialParticle;

class OnlineGiftIcon:public UIScene
{
public:
	OnlineGiftIcon(void);
	~OnlineGiftIcon(void);

public:
	static OnlineGiftIcon* create();
	bool init();

	void update(float dt);

	virtual void onEnter();
	virtual void onExit();

public:
	UIButton * m_btn_onlineGiftIcon;					// 可点击的ICON
	UILayer * m_layer_onlineGift;						// 需要的Layer
	UILabel * m_label_onlineGift_time;				// 剩余时间的label		
	CCScale9Sprite * m_spirte_fontBg;				// 文字显示的底图

public:
	void callBackBtnOnlineGift(CCObject *obj);

public:
	void initGiftTime(const int nTime);							// 当前奖励 还需 nTime 毫秒可领取
	void initGiftNum(const int nGiftNum);					// 当前奖励的编号

	void initDataFromIntent();										// 从服务器数据初始化Icon

	void allGiftGetIconExit();										// 所有奖励已成功领取，icon消失（或者等级达到上限，icon消失）

	int m_nTimeShow;												// 显示在面板的 该奖励可领取的时间
	int m_nOnlineGiftTime;											// 服务器所获取的 该奖励 领取 所需时间
	int m_nGiftNum;													// 当前显示奖励 的 编号

private:
	 std::string timeFormatToString(int nTime);
	 void refreshData();

	 CCTutorialParticle * m_tutorialParticle;					// 在线奖励特效
	

};

#endif

