#include "PickUI.h"
#include "../../messageclient/element/CPickingInfo.h"
#include "../../gamescene_state/role/PickingActor.h"
#include "../../GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/element/CPushCollectInfo.h"
#include "AppMacros.h"
#include "../../utils/StaticDataManager.h"

PickUI::PickUI():
m_nCount(0)
{
}


PickUI::~PickUI()
{
}

PickUI* PickUI::create(CPickingInfo* pickingInfo)
{
	PickUI * pickUI = new PickUI();
	if(pickUI && pickUI->init(pickingInfo))
	{
		pickUI->autorelease();
		return pickUI;
	}
	CC_SAFE_DELETE(pickUI);
	return NULL;
}

PickUI* PickUI::create( float time )
{
	PickUI * pickUI = new PickUI();
	if(pickUI && pickUI->init(time))
	{
		pickUI->autorelease();
		return pickUI;
	}
	CC_SAFE_DELETE(pickUI);
	return NULL;
}

bool PickUI::init(CPickingInfo* pickingInfo)
{
	if (UIScene::init())
	{
// 		CCSprite * bg = CCSprite::create("gamescene_state/caiji/caiji_di.png");
// 		bg->ignoreAnchorPointForPosition(false);
// 		bg->setAnchorPoint(ccp(0,0));
// 		bg->setPosition(CCPointZero);
// 		addChild(bg);
		CCScale9Sprite *bg = CCScale9Sprite::create("gamescene_state/zhujiemian3/caiji/caiji_di.png");
		bg->setPreferredSize(CCSizeMake(187,32));
		bg->setCapInsets(CCRect(11,11,1,1));
		bg->ignoreAnchorPointForPosition(false);
		bg->setAnchorPoint(ccp(0,0));
		bg->setPosition(CCPointZero);
		addChild(bg);

		CCSprite * spriteIcon = CCSprite::create("gamescene_state/zhujiemian3/caiji/caiji_jindu.png");
		spriteIcon->ignoreAnchorPointForPosition(false);
		spriteIcon->setAnchorPoint(ccp(0,0));
		CCProgressTimer * progressTimer = CCProgressTimer::create(spriteIcon);
		progressTimer->setAnchorPoint(ccp(0.5f,0.5f));
		progressTimer->setPosition(ccp(bg->getContentSize().width/2,bg->getContentSize().height/2));
		//设置进度起点在最左边 
		progressTimer->setMidpoint(ccp(0,0)); 
		//设置进度动画方向为从左到右随进度增长而显现。 
		progressTimer->setBarChangeRate(ccp(1, 0)); 
		this->addChild(progressTimer);  
		CCActionInterval* action_progress_from_to;
		CCCallFunc* action_callback;

		progressTimer->setType(kCCProgressTimerTypeBar);

		action_progress_from_to = CCProgressFromTo::create(pickingInfo->actionTime/1000, 0, 100);     
		action_callback = CCCallFuncN::create(this, callfuncN_selector(PickUI::FinishedCallBack));
		progressTimer->runAction(CCSequence::create(action_progress_from_to, action_callback, NULL));

		PickingActor * pickingActor = dynamic_cast<PickingActor*>(GameView::getInstance()->myplayer->getLockedActor());
		if (pickingActor)
		{
			//CCLabelTTF * label_description = CCLabelTTF::create(pickingInfo->description.c_str(),APP_FONT_NAME,14);
			CCLabelTTF * label_description = CCLabelTTF::create(pickingActor->p_collectInfo->actiondescription().c_str(),APP_FONT_NAME,14);
			label_description->setAnchorPoint(ccp(0.5f,0.5f));
			label_description->setPosition(ccp(bg->getContentSize().width/2,bg->getContentSize().height/2));
			ccColor3B shadowColor = ccc3(0,0,0);   // black
			label_description->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);

			CCRepeatForever * repeapAction = CCRepeatForever::create(
				CCSequence::create(
					CCScaleTo::create(0.9f, 1.10f),
					CCScaleTo::create(0.7f, 1.0f),
					NULL));
			label_description->runAction(repeapAction);

			this->addChild(label_description);
		}

		CCSprite * adorn_left = CCSprite::create("gamescene_state/zhujiemian3/renwuduiwu/aa.png");
		adorn_left->setAnchorPoint(ccp(0.5f,0.5f));
		adorn_left->setPosition(ccp(4,6));
		adorn_left->setFlipX(true);
		addChild(adorn_left);

		CCSprite * adorn_right = CCSprite::create("gamescene_state/zhujiemian3/renwuduiwu/aa.png");
		adorn_right->setAnchorPoint(ccp(0.5f,0.5f));
		adorn_right->setPosition(ccp(182,6));
		addChild(adorn_right);
		

		this->setContentSize(progressTimer->getContentSize());

		return true;
	}
	return false;
}

bool PickUI::init( float time )
{
	if (UIScene::init())
	{
		// 		CCSprite * bg = CCSprite::create("gamescene_state/caiji/caiji_di.png");
		// 		bg->ignoreAnchorPointForPosition(false);
		// 		bg->setAnchorPoint(ccp(0,0));
		// 		bg->setPosition(CCPointZero);
		// 		addChild(bg);
		CCScale9Sprite *bg = CCScale9Sprite::create("gamescene_state/zhujiemian3/caiji/caiji_di.png");
		bg->setPreferredSize(CCSizeMake(187,32));
		bg->setCapInsets(CCRect(11,11,1,1));
		bg->ignoreAnchorPointForPosition(false);
		bg->setAnchorPoint(ccp(0,0));
		bg->setPosition(CCPointZero);
		addChild(bg);

		CCSprite * spriteIcon = CCSprite::create("gamescene_state/zhujiemian3/caiji/caiji_jindu.png");
		spriteIcon->ignoreAnchorPointForPosition(false);
		spriteIcon->setAnchorPoint(ccp(0,0));
		CCProgressTimer * progressTimer = CCProgressTimer::create(spriteIcon);
		progressTimer->setAnchorPoint(ccp(0.5f,0.5f));
		progressTimer->setPosition(ccp(bg->getContentSize().width/2,bg->getContentSize().height/2));
		//设置进度起点在最左边 
		progressTimer->setMidpoint(ccp(0,0)); 
		//设置进度动画方向为从左到右随进度增长而显现。 
		progressTimer->setBarChangeRate(ccp(1, 0)); 
		this->addChild(progressTimer);  
		CCActionInterval* action_progress_from_to;
		CCCallFunc* action_callback;

		progressTimer->setType(kCCProgressTimerTypeBar);

		action_progress_from_to = CCProgressFromTo::create(time / 1000, 0, 100);     
		action_callback = CCCallFuncN::create(this, callfuncN_selector(PickUI::FinishedCallBack));
		progressTimer->runAction(CCSequence::create(action_progress_from_to, action_callback, NULL));

		//PickingActor * pickingActor = dynamic_cast<PickingActor*>(GameView::getInstance()->myplayer->getLockedActor());
		//if (pickingActor)
		{
			//CCLabelTTF * label_description = CCLabelTTF::create(pickingInfo->description.c_str(),APP_FONT_NAME,14);
			//const char * charDescription = "the presentBox is collecting!";
			const char * charDescription = StringDataManager::getString("PresentBox_openPresentBox");
			CCLabelTTF * label_description = CCLabelTTF::create(charDescription,APP_FONT_NAME,14);
			label_description->setAnchorPoint(ccp(0.5f,0.5f));
			label_description->setPosition(ccp(bg->getContentSize().width/2,bg->getContentSize().height/2));
			ccColor3B shadowColor = ccc3(0,0,0);   // black
			label_description->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
			this->addChild(label_description);
		}

		CCSprite * adorn_left = CCSprite::create("gamescene_state/zhujiemian3/renwuduiwu/aa.png");
		adorn_left->setAnchorPoint(ccp(0.5f,0.5f));
		adorn_left->setPosition(ccp(4,6));
		adorn_left->setFlipX(true);
		addChild(adorn_left);

		CCSprite * adorn_right = CCSprite::create("gamescene_state/zhujiemian3/renwuduiwu/aa.png");
		adorn_right->setAnchorPoint(ccp(0.5f,0.5f));
		adorn_right->setPosition(ccp(182,6));
		addChild(adorn_right);


		this->setContentSize(progressTimer->getContentSize());

		return true;
	}
	return false;
}

void PickUI::update( float dt )
 {
	 
 }
// void PickUI::update( float dt )
// {
// 	m_nCount++;
// 	if (m_nCount == 100)
// 	{
// 		this->removeFromParent();
// 	}
// 	else if (m_nCount > 100)
// 	{
// 		m_nCount = 0;
// 	}
// 
// 	UILoadingBar* loadingBar = dynamic_cast<UILoadingBar*>(m_pUiLayer->getWidgetByName("LoadingBar"));
// 	loadingBar->setPercent(m_nCount);

// }

void PickUI::FinishedCallBack(CCNode* node)
{
	CCLOG("loading finished");
	this->removeFromParent();
}
