#ifndef _PICKUI_PICKUI_H_
#define _PICKUI_PICKUI_H_

#include "../extensions/UIScene.h"
#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CPickingInfo;

class PickUI : public UIScene
{
public:
	PickUI();
	~PickUI();

	static PickUI* create(CPickingInfo* pickingInfo);
	bool init(CPickingInfo* pickingInfo);

	static PickUI* create(float time);
	bool init(float time);

	virtual void update(float dt);

	void FinishedCallBack(CCNode* node);

protected:
	int m_nCount;
};

#endif

