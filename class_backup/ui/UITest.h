#ifndef _UIRICHANG_H_
#define _UIRICHANG_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class UITest : public CCLayer
{
public:
	UITest();
	~UITest();

public:
	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
    virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
    virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
    virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

    // a selector callback
    void menuCloseCallback(CCObject* pSender);
	void menuButtonCallback(CCObject* pSender);
	void menuSkillCallback(CCObject* pSender);

};

#endif // _UIRICHANG_H_
