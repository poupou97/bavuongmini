#pragma once

#include "cocos2d.h"
USING_NS_CC;

class MapAction
{
public:
	MapAction();
	~MapAction();

	static MapAction * s_mapAction;
	static MapAction* getInstance();

	void update();
	bool isFinishedMove();

	void setData(int state, CCPoint pos, std::string mapId, long long actorId);
	void setIsUpdate(bool value);
private:
	int m_iState;
	CCPoint targetPos;
	std::string targetMapId;
	long long targetActorId;
	bool isUpdate;
};
