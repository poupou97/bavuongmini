
#ifndef _UI_MAPSCENE_MAPSCENE_H_
#define _UI_MAPSCENE_MAPSCENE_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "AreaMap.h"
#include "../extensions/UIScene.h"
#include "../extensions/UITab.h"
//#include "../../messageclient/pushhandler/PushHandler5080.h"
#include "../../messageclient/GameProtobuf.h"
#include "../../messageclient/protobuf/PlayerMessage.pb.h"
#include "../../gamescene_state/role/GameActor.h"

USING_NS_CC;
USING_NS_CC_EXT;
USING_NS_THREEKINGDOMS_PROTOCOL;

class UIScene;
class UITab;

class MapScene :public UIScene,public CCTableViewDataSource,public CCTableViewDelegate
{
public:
	MapScene();
	~MapScene();
	static MapScene* create();
	bool init();

	//scrollview滚动的时候会调用
	void scrollViewDidScroll(CCScrollView* view);
	//scrollview缩放的时候会调用
	void scrollViewDidZoom(CCScrollView* view);

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

	virtual void tableCellTouched(cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell);
	virtual cocos2d::CCSize tableCellSizeForIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	virtual unsigned int numberOfCellsInTableView(cocos2d::extension::CCTableView *table);

	virtual void tableCellHighlight(CCTableView* table, CCTableViewCell* cell);
    virtual void tableCellUnhighlight(CCTableView* table, CCTableViewCell* cell);

	void showCountryMapScale();
	void showCountryMap(CCObject * obj);
	
	void showCurrMap(CCObject * obj);
	////////add by liuzhenxing

	bool  isTemplateIdExist(int monsterIndex);

	void initMapInfo(std::string mapId);

	void initMapButton();
	void initMyplayerIcon(int mode);

	void reloadAreaMap(std::string mapIdName);
	void setCurrentMapId(std::string mapId){m_mapId = mapId;}

	CCPoint getNearReachablePoint(CCPoint& targetPoint, bool isCurrentMap, LegendLevel* pLevelData);

	//virtual void update(float dt);
	bool isFinishedMove();
public:
	UIPanel * panelWorld;
	UIPanel * panelCountry;
	UIPanel * panelcurrent;
	void callBackScene(CCObject * obj);
	void callBackPerson(CCObject * obj);
	void callBackExit(CCObject * obj);
	void ChangeLineEvent(CCObject *pSender);
	void showCurrMap_child( CCObject * obj );
	void callBackFunc();
private:
	void initAreaMapLayer();
	std::string getCountryInfo(char countryId);
	void checkActorValidity();
	void callbackClickedNotReached(CCObject * obj);
private:
	//UIPanel * worldMapPanel;
	//UIPanel * areaMapPanel;
	UIPanel * npcListPanel;
	UIImageView *image_countryNameFrame;

	CCLayer * worldMapLayer;
	CCLayer * areaMapLayer;
	CCLayer * npcListLayer;

	CCTableView *infoTabview;
	UITab * equipTab;
	UITab * findTab;

	UIImageView * lianzhouImage;
	UIImageView * youzhouImage;
	UIImageView * yizhouImage;
	UIImageView * jingzhouImage;
	UIImageView * yangzhouImage;

	UIImageView * ImageView_biankuang_right;

	int m_iMapCountry;
	int m_iMapFindInfo;
	int m_iMapType;
	std::string m_mapId;
	std::string m_currentMapId;

	std::vector<long> monsterTemplateId;
	std::vector<int> monsterIndex;
	std::vector<long long> playerActor;

	std::vector<std::string> m_reachedMap;

	LegendLevel* pLevelData;

	CCPoint m_mapDragStartPoint;
	bool m_mapScrollViewDraged;

	std::vector<char*> mapIdList;
	//myplayer icon
	CCSprite* myIcon;
	//update state
	int m_iState;
	CCPoint targetPos;
	std::string targetMapId;
	long long targetActorId;

	CCPoint countryCenterPos;
};
#endif;
