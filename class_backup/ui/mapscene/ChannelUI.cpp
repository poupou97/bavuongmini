#include "ChannelUI.h"
#include "../../utils/StaticDataManager.h"
#include "GameView.h"
#include "AppMacros.h"
#include "../../messageclient/element/CMapInfo.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/role/MyPlayer.h"

ChannelUI::ChannelUI()
{
}


ChannelUI::~ChannelUI()
{
}


ChannelUI * ChannelUI::create()
{
	ChannelUI * channelUI = new ChannelUI();
	if (channelUI && channelUI->init())
	{
		channelUI->autorelease();
		return channelUI;
	}
	CC_SAFE_DELETE(channelUI);
	return NULL;
}

bool ChannelUI::init()
{
	if (UIScene::init())
	{
		CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
		CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

		UIImageView * bg = UIImageView::create();
		bg->setTexture("gamescene_state/zhujiemian3/renwuduiwu/renwu_di.png");
		bg->setAnchorPoint(CCPointZero);
		bg->setPosition(CCPointZero);
		bg->setScale9Enable(true);
		bg->setCapInsets(CCRectMake(12,12,1,1));
		bg->setScale9Size(CCSizeMake(217,240));
		m_pUiLayer->addWidget(bg);

		UIButton * btn_close = UIButton::create();
		btn_close->setTextures("res_ui/close.png","res_ui/close.png","");
		btn_close->setAnchorPoint(ccp(0.5f,0.5f));
		btn_close->setPosition(ccp(212,232));
		btn_close->setTouchEnable(true);
		btn_close->setPressedActionEnabled(true);
		btn_close->addReleaseEvent(this,coco_releaseselector(ChannelUI::CloseEvent));
		m_pUiLayer->addWidget(btn_close);

		m_tableView = CCTableView::create(this,CCSizeMake(217,216));
		m_tableView->setDirection(kCCScrollViewDirectionVertical);
		m_tableView->setAnchorPoint(ccp(0,0));
		m_tableView->setPosition(ccp(0,12));
		m_tableView->setDelegate(this);
		m_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		this->addChild(m_tableView);

		this->ignoreAnchorPointForPosition(false);
		this->setAnchorPoint(ccp(0,0));
		this->setPosition(ccp(0,0));
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(CCSizeMake(217,240));

		return true;
	}
	return false;
}



void ChannelUI::onEnter()
{
	UIScene::onEnter();
}
void ChannelUI::onExit()
{
	UIScene::onExit();
}

bool ChannelUI::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
	return this->resignFirstResponder(pTouch,this,false);
}
void ChannelUI::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
{

}
void ChannelUI::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
{

}
void ChannelUI::ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent)
{

}


CCSize ChannelUI::tableCellSizeForIndex(CCTableView *table, unsigned int idx)
{
	return CCSizeMake(210,49);
}

CCTableViewCell* ChannelUI::tableCellAtIndex(CCTableView *table, unsigned int idx)
{
	CCString *string = CCString::createWithFormat("%d", idx);
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell = new CCTableViewCell();
	cell->autorelease();

	UILayer * u_layer = UILayer::create();
	cell->addChild(u_layer);
	u_layer->setSwallowsTouches(false);

	UIButton * btn_channel = UIButton::create();
	btn_channel->setTextures("res_ui/new_button_5.png","res_ui/new_button_5.png","");
	btn_channel->setAnchorPoint(ccp(0.5f,0.5f));
	btn_channel->setPosition(ccp(110,25));
	btn_channel->setTouchEnable(true);
	btn_channel->setPressedActionEnabled(true);
	btn_channel->addReleaseEvent(this,coco_releaseselector(ChannelUI::ChannelEvent));
	btn_channel->setTag(idx);
	u_layer->addWidget(btn_channel);

	if (idx == GameView::getInstance()->getMapInfo()->channel())
	{
		btn_channel->setTextures("res_ui/new_button_6.png","res_ui/new_button_6.png","");
	}
	else
	{
		btn_channel->setTextures("res_ui/new_button_5.png","res_ui/new_button_5.png","");
	}

	UILabel * l_channelId = UILabel::create();
	std::string str_channelid;
	char s_id[5];
	sprintf(s_id,"%d",idx+1);
	str_channelid.append(s_id);
	str_channelid.append(StringDataManager::getString("GuideMap_Channel_Name"));
	l_channelId->setText(str_channelid.c_str());
	l_channelId->setFontName(APP_FONT_NAME);
	l_channelId->setFontSize(20);
	l_channelId->setAnchorPoint(ccp(0.5f,0.5f));
	l_channelId->setPosition(ccp(0.f,0.f));
	btn_channel->addChild(l_channelId);

	return cell;

}

unsigned int ChannelUI::numberOfCellsInTableView(CCTableView *table)
{
	return GameView::getInstance()->getMapInfo()->channelsize();
}

void ChannelUI::tableCellTouched(CCTableView* table, CCTableViewCell* cell)
{
}

void ChannelUI::scrollViewDidScroll(CCScrollView* view )
{
}

void ChannelUI::scrollViewDidZoom(CCScrollView* view )
{
}

void ChannelUI::ChannelEvent( CCObject *pSender )
{
	UIButton * btn_channel = dynamic_cast<UIButton*>(pSender);
	if (!btn_channel)
		return;

	//正在PK时不允许传送，只能跑路
	if(GameView::getInstance()->myplayer->isPKStatus())
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("PKMode_canNoChangeChannel"));
	}
	else
	{
		int id = btn_channel->getTag();
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1110,(void*)id);

		this->closeAnim();
	}
}

void ChannelUI::CloseEvent( CCObject *pSender )
{
	this->closeAnim();
}








