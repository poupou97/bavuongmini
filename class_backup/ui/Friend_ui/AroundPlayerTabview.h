#ifndef _UI_FRIEND_AROUNDPLAYERTABVIEW_H_
#define _UI_FRIEND_AROUNDPLAYERTABVIEW_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;
typedef enum
{
	AROUNDPLAYERTABVIEW = 510,
	PLAYEROTHERFRIENDCELLL = 528,
	PLAYEROTHERFRIENDCELLR = 529,
};

class AroundPlayerTabview:public UIScene,public cocos2d::extension::CCTableViewDataSource,public cocos2d::extension::CCTableViewDelegate
{
public:
	AroundPlayerTabview(void);
	~AroundPlayerTabview(void);

	static AroundPlayerTabview * create();
	bool init();
	virtual void onEnter();
	virtual void onExit();

	virtual void scrollViewDidScroll(cocos2d::extension::CCScrollView * view);
	virtual void scrollViewDidZoom(cocos2d::extension::CCScrollView* view);
	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(cocos2d::extension::CCTableView *table);

	// default implements are used to call script callback if exist
	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);


	//void callBack(CCObject * obj);

	int lastSelectCellId;
	int lastSelectCellTag;
	
};

#endif

