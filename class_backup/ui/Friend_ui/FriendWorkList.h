#ifndef _UI_FRIEND_FRIENDWORKLIST_H_
#define _UI_FRIEND_FRIENDWORKLIST_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "../generals_ui/GeneralsListBase.h"
USING_NS_CC;
USING_NS_CC_EXT;
class UITab;
typedef enum
{
	FRIENDWORKLISTTAB = 503,
	FRIENDINFOCELLL = 508,
	FRIENDINFOCELLR = 509,
};

class FriendWorkList:public GeneralsListBase,public cocos2d::extension::CCTableViewDataSource,public cocos2d::extension::CCTableViewDelegate
{
public:
	FriendWorkList(void);
	~FriendWorkList(void);

	struct FriendStruct
	{
		int relationType;
		int pageNum;
		int pageIndex;
		int showOnline;
	};

	static FriendWorkList *create();

	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual void scrollViewDidScroll(cocos2d::extension::CCScrollView * view);
	virtual void scrollViewDidZoom(cocos2d::extension::CCScrollView* view);
	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(cocos2d::extension::CCTableView *table);

	// default implements are used to call script callback if exist
	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);


	void ReloadTableViewWithoutChangeOffSet();

public:
	CCTableView *tableView;
	int totalPageNum;
	int lastSelectCellId;
	int lastSelectCellTag;
private:
	CCSize winSize;
	UILayer * layer_friend;
	int friendNum;
};

#endif

