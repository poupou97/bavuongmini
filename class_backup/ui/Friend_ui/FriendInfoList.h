#ifndef _UI_FRIEND_FRIENDINFOLIST_H_
#define _UI_FRIEND_FRIENDINFOLIST_H_


#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"


enum {
	_PRIVATE=0,
	_CHECK,
	_TEAM,
	_MAIL,
	_DELETE,
	_BACKLIST,
	_ADDFRIEND,
	_REVENGE,
};
class FriendInfoList:public UIScene
{
public:
	FriendInfoList(void);
	~FriendInfoList(void);

	static FriendInfoList * create(int currType);

	bool init(int currType);

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);

	void callBackFriendList(CCObject * obj);
	void callBackBackList(CCObject * obj);
	void callBackEnemyList(CCObject * obj);
	void callBackAroundPlayer(CCObject * obj);
	void callBackCheckList(CCObject * obj);

	void callBackPhypower(CCObject * obj);

	void deleteFriendSure(CCObject * obj);
	void backListFriendSure(CCObject * obj);
public:
	void deldtePhypowerSureDelete(CCObject * obj);
	void backListPhypowerSure(CCObject * obj);

public:
	struct FriendStruct
	{
		int operation;
		int relationType;
		long long playerid;
		std::string playerName;
	};
};

#endif

