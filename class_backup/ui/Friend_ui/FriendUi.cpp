#include "FriendUi.h"
#include "FriendWorkList.h"
#include "GameView.h"
#include "../extensions/CCMoveableMenu.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/CRelationPlayer.h"
#include "../extensions/Counter.h"
#include "../extensions/RichTextInput.h"
#include "../extensions/UITab.h"
#include "TeamList.h"
#include "TeamMemberList.h"
#include "../../messageclient/element/CMapTeam.h"
#include "AroundPlayerTabview.h"
#include "../../messageclient/element/CTeamMember.h"
#include "../../utils/StaticDataManager.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/GameUtils.h"
#include "../../gamescene_state/role/MyPlayerAIConfig.h"

using namespace CocosDenshion;

FriendUi::FriendUi(void)
{
	curType=FRIENDLIST;

	sexSelect=2;
	professionSelect=0;
	showOnlineSelect=0;
	pageNumSelect=20;//每页显示d数量
	pageIndexSelect=0;

	selectIndex=0;
	fuctionValue=0;

	minLvSelect = 0;
	maxLvSelect = 0;

	totalPlayerNum_ = 0;
}


FriendUi::~FriendUi(void)
{
	std::vector<CRelationPlayer *>::iterator iter;
	for (iter=GameView::getInstance()->relationFriendVector.begin();iter!=GameView::getInstance()->relationFriendVector.end();iter++)
	{
		delete *iter;
	}
	GameView::getInstance()->relationFriendVector.clear();

	for (iter=GameView::getInstance()->relationBlackVector.begin();iter!=GameView::getInstance()->relationBlackVector.end();iter++)
	{
		delete *iter;
	}
	GameView::getInstance()->relationBlackVector.clear();


	for (iter=GameView::getInstance()->relationEnemyVector.begin();iter!=GameView::getInstance()->relationEnemyVector.end();iter++)
	{
		delete *iter;
	}
	GameView::getInstance()->relationEnemyVector.clear();


	for (iter=GameView::getInstance()->relationSourceVector.begin();iter!=GameView::getInstance()->relationSourceVector.end();iter++)
	{
		delete *iter;
	}
	GameView::getInstance()->relationSourceVector.clear();
}

FriendUi * FriendUi::create()
{
	FriendUi * friendui=new FriendUi();

	if (friendui && friendui->init())
	{
		friendui->autorelease();
		return friendui;
	}
	CC_SAFE_DELETE(friendui);
	return NULL;
}

bool FriendUi::init()
{
	if (UIScene::init())
	{

		winsize=CCDirector::sharedDirector()->getVisibleSize();

		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winsize);
		mengban->setAnchorPoint(ccp(0,0));
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);

		if(LoadSceneLayer::friendPanelBg->getWidgetParent() != NULL)
		{
			LoadSceneLayer::friendPanelBg->removeFromParentAndCleanup(false);
		}

		friendPanel=LoadSceneLayer::friendPanelBg;
		friendPanel->setAnchorPoint(ccp(0.5f,0.5f));
		friendPanel->setPosition(ccp(winsize.width/2,winsize.height/2));
		m_pUiLayer->addWidget(friendPanel);

		layer_= UILayer::create();
		layer_->ignoreAnchorPointForPosition(false);
		layer_->setAnchorPoint(ccp(0.5f,0.5f));
		layer_->setPosition(ccp(winsize.width/2,winsize.height/2));
		layer_->setContentSize(CCSizeMake(800,480));
		this->addChild(layer_,0,FRIENDADDLAYER);

		const char * secondStr = StringDataManager::getString("UIName_friend_hao");
		const char * thirdStr = StringDataManager::getString("UIName_friend_you");
		CCArmature * atmature = MainScene::createPendantAnm("",secondStr,thirdStr,"");
		atmature->setPosition(ccp(30,240));
		layer_->addChild(atmature);


		const char *str_friend = StringDataManager::getString("friend_friend");
		char *friend_left=const_cast<char*>(str_friend);	

		const char *str_enemy = StringDataManager::getString("friend_enemy");
		char *enemy_left=const_cast<char*>(str_enemy);

		const char *str_black = StringDataManager::getString("friend_black");
		char *black_left=const_cast<char*>(str_black);

		const char *str_team = StringDataManager::getString("chatui_team");
		char *team_left=const_cast<char*>(str_team);

		const char *str_around = StringDataManager::getString("friend_around");
		char *around_left=const_cast<char*>(str_around);

		const char * highLightImage = "res_ui/tab_4_on.png";
		char * labelChange[] ={friend_left,enemy_left,black_left,team_left,around_left};
		//friendType= UITab::createWithBMFont(5,"res_ui/tab_4_off.png","res_ui/tab_4_on.png","",labelChange,"res_ui/font/ziti_1.fnt",HORIZONTAL,5);
		friendType= UITab::createWithText(5,"res_ui/tab_4_off.png","res_ui/tab_4_on.png","",labelChange,HORIZONTAL,5);
		friendType->setAnchorPoint(ccp(0.5f,0.5f));
		friendType->setPosition(ccp(70,413));
		friendType->setPressedActionEnabled(true);
		friendType->setHighLightImage((char * )highLightImage);
		friendType->setDefaultPanelByIndex(0);
		friendType->setWidgetZOrder(2);
		friendType->addIndexChangedEvent(this,coco_indexchangedselector(FriendUi::callBackChangeFriendType));
		layer_->addWidget(friendType);

		const char * highLightwork = "res_ui/tab_b.png";
		//char * labelwork[] ={"res_ui/haoyou/haoyou2.png","res_ui/haoyou/linshi.png","res_ui/haoyou/chazhao.png"};

		const char *str1_haoyou = StringDataManager::getString("friend_uitab_haoyou");
		char* p1_haoyou =const_cast<char*>(str1_haoyou);
		const char *str2_linshi = StringDataManager::getString("friend_uitab_linshi");
		char* p2_linshi =const_cast<char*>(str2_linshi);
		const char *str2_chazhao = StringDataManager::getString("friend_uitab_chazhao");
		char* p3_chazhao =const_cast<char*>(str2_chazhao);

		char * labelImage[] ={p1_haoyou,p2_linshi,p3_chazhao};
		friendWork = UITab::createWithText(3,"res_ui/tab_b_off.png","res_ui/tab_b.png","",labelImage,VERTICAL,-5,18);
		//friendWork= UITab::createWithImage(3,"res_ui/tab_b_off.png","res_ui/tab_b.png","",labelwork,VERTICAL,-5);
		friendWork->setAnchorPoint(ccp(0.5f,0.5f));
		friendWork->setPosition(ccp(758,405));
		friendWork->setPressedActionEnabled(true);
		friendWork->setHighLightImage((char * )highLightwork);
		friendWork->setHightLightLabelColor(ccc3(47,93,13));
		friendWork->setNormalLabelColor(ccc3(255,255,255));
		friendWork->setDefaultPanelByIndex(0);
		friendWork->setWidgetZOrder(2);
		friendWork->addIndexChangedEvent(this,coco_indexchangedselector(FriendUi::callBackFriendButton));
		layer_->addWidget(friendWork);

		palPanel =(UIPanel *)UIHelper::seekWidgetByName(friendPanel,"Panel_haoyou");
		palPanel->setVisible(true);
		checkPanel=(UIPanel *)UIHelper::seekWidgetByName(friendPanel,"Panel_chazhao");
		checkPanel->setVisible(false);
		
		btn_phypower = (UIButton *)UIHelper::seekWidgetByName(friendPanel,"Button_getPhypower");
		btn_phypower->setPressedActionEnabled(true);
		btn_phypower->setTouchEnable(true);
		btn_phypower->addReleaseEvent(this,coco_releaseselector(FriendUi::callBackPhypower));

		checkResultPanel =(UIPanel *)UIHelper::seekWidgetByName(friendPanel,"Panel_chazhaojieguo");
		checkResultPanel->setVisible(false);
		labclCheckResult = (UILabel*)UIHelper::seekWidgetByName(friendPanel,"chazhaojieguoNumValue");
		labclCheckResult->setText("0");

		panelTeam=(UIPanel *)UIHelper::seekWidgetByName(friendPanel,"Panel_zudui"); 
		panelTeam->setVisible(false);

		panelOtherplayer=(UIPanel *)UIHelper::seekWidgetByName(friendPanel,"Panel_other players");
		panelOtherplayer->setVisible(false);

		isShowFriendCheckbox=(UICheckBox *)UIHelper::seekWidgetByName(friendPanel,"CheckBox_xianshizaixian");
		isShowFriendCheckbox->setTouchEnable(true);
		isShowFriendCheckbox->setSelectedState(false);
		isShowFriendCheckbox->addReleaseEvent(this,coco_selectselector(FriendUi::showfriendOfLine));
		isShowFriendCheckbox->setVisible(true);

		labelFriendType = (UILabel *)UIHelper::seekWidgetByName(friendPanel,"Label_795");

// 		int onLineFriendNum =GameView::getInstance()->relationSourceVector.size();
// 		char friendnum_[10]; 
// 		sprintf(friendnum_,"%d",onLineFriendNum);

		label_Friendnum =(UILabel *)UIHelper::seekWidgetByName(friendPanel,"showLabelNumValue"); 
		label_Friendnum->setText("0/50");
		label_Friendnum->setVisible(true);

		////check yijianzhengyou   panel=checkpanel
		UIButton * chackButton=(UIButton *)UIHelper::seekWidgetByName(friendPanel,"Button_chazhao");
		chackButton->setPressedActionEnabled(true);
		chackButton->setTouchEnable(true);
		chackButton->addReleaseEvent(this,coco_releaseselector(FriendUi::callBackChackButton));

		UIButton * keyPersonal=(UIButton *)UIHelper::seekWidgetByName(friendPanel,"Button_yijianzhengyou");
		keyPersonal->setTouchEnable(true);
		keyPersonal->setPressedActionEnabled(true);
		keyPersonal->addReleaseEvent(this,coco_releaseselector(FriendUi::callBackKeyPersonalButton));

		nameBox=new RichTextInputBox();
		nameBox->setInputBoxWidth(310);
		nameBox->setAnchorPoint(ccp(0,0));
		nameBox->setPosition(ccp(164,340));
		nameBox->setCharLimit(10);
		layer_->addChild(nameBox);
		nameBox->setVisible(false);
		nameBox->autorelease();

		////find profession
		CheckBox_pro_HaoJie =(UICheckBox *)UIHelper::seekWidgetByName(friendPanel,"CheckBox_zhiye_mo_haojie");
		CheckBox_pro_HaoJie->setTouchEnable(true);
		CheckBox_pro_HaoJie->setSelectedState(false);

		CheckBox_pro_MengJiang =(UICheckBox *)UIHelper::seekWidgetByName(friendPanel,"CheckBox_zhiye_mo_mengjiang");
		CheckBox_pro_MengJiang->setTouchEnable(true);
		CheckBox_pro_MengJiang->setSelectedState(false);

		CheckBox_pro_GuiMou =(UICheckBox *)UIHelper::seekWidgetByName(friendPanel,"CheckBox_zhiye_mo_guimou");
		CheckBox_pro_GuiMou->setTouchEnable(true);
		CheckBox_pro_GuiMou->setSelectedState(false);

		CheckBox_pro_ShenShe =(UICheckBox *)UIHelper::seekWidgetByName(friendPanel,"CheckBox_zhiye_mo_shenshe");
		CheckBox_pro_ShenShe->setTouchEnable(true);
		CheckBox_pro_ShenShe->setSelectedState(false);
		/*

		UICheckBox * checkBoxbuyHp =(UICheckBox *)UIHelper::seekWidgetByName(protectionPanel,"CheckBox_honggou_2");
		checkBoxbuyHp->setTouchEnable(true);
		checkBoxbuyHp->addEventListenerCheckBox(this,checkboxselectedeventselector(RobotMainUI::setCheckBoxBuyHp));
		*/
		checkBox_country_yi = (UICheckBox *)UIHelper::seekWidgetByName(friendPanel,"CheckBox_country_yi");
		checkBox_country_yi->setTouchEnable(true);
		checkBox_country_yi->setSelectedState(false);
		//checkBox_country_yi->addEventListenerCheckBox(this,checkboxselectedeventselector());

		checkBox_country_yang = (UICheckBox *)UIHelper::seekWidgetByName(friendPanel,"CheckBox_country_yang");
		checkBox_country_yang->setTouchEnable(true);
		checkBox_country_yang->setSelectedState(false);
		//checkBox_country_yang->addEventListenerCheckBox(this,checkboxselectedeventselector());

		checkBox_country_jing = (UICheckBox *)UIHelper::seekWidgetByName(friendPanel,"CheckBox_country_jing");
		checkBox_country_jing->setTouchEnable(true);
		checkBox_country_jing->setSelectedState(false);
		//checkBox_country_jing->addEventListenerCheckBox();
		/*
		checkBox_country_you = (UICheckBox *)UIHelper::seekWidgetByName(friendPanel,"CheckBox_country_you");
		checkBox_country_you->setTouchEnable(true);
		checkBox_country_you->setSelectedState(false);

		checkBox_country_liang = (UICheckBox *)UIHelper::seekWidgetByName(friendPanel,"CheckBox_country_liang");
		checkBox_country_liang->setTouchEnable(true);
		checkBox_country_liang->setSelectedState(false);
		*/

		//Button_dengjikuang1  Button_dengjikuang1
		UIButton * Button_LV1=(UIButton *)UIHelper::seekWidgetByName(friendPanel,"Button_dengjikuang1");
		Button_LV1->setTouchEnable(true);
		Button_LV1->addReleaseEvent(this,coco_releaseselector(FriendUi::callBackPopCounterLV1));

		UIButton * Button_LV2=(UIButton *)UIHelper::seekWidgetByName(friendPanel,"Button_dengjikuang2");
		Button_LV2->setTouchEnable(true);
		Button_LV2->addReleaseEvent(this,coco_releaseselector(FriendUi::callBackPopCounterLV2));

		label_lv1= (UILabel *)UIHelper::seekWidgetByName(friendPanel,"Label_LV1");
		label_lv1->setText("");
		label_lv2= (UILabel *)UIHelper::seekWidgetByName(friendPanel,"Label_LV2");
		label_lv2->setText("");

		button_backCheck =(UIButton *)UIHelper::seekWidgetByName(friendPanel,"Button_fanhui");
		button_backCheck->setPressedActionEnabled(true);
		button_backCheck->setTouchEnable(true);
		button_backCheck->addReleaseEvent(this,coco_releaseselector(FriendUi::callBackButton_backCheck));
		
		button_addFriend =(UIButton *)UIHelper::seekWidgetByName(friendPanel,"Button_yijiantianjia");
		button_addFriend->setPressedActionEnabled(true);
		button_addFriend->setTouchEnable(true);
		button_addFriend->addReleaseEvent(this,coco_releaseselector(FriendUi::callbackButton_addFriend));

		CheckBox_accept =(UICheckBox *)UIHelper::seekWidgetByName(friendPanel,"CheckBox_accept");
		CheckBox_accept->setTouchEnable(true);
		if (MyPlayerAIConfig::getAcceptTeam() == true)
		{
			CheckBox_accept->setSelectedState(true);
		}else
		{
			CheckBox_accept->setSelectedState(false);
		}
		CheckBox_accept->addEventListenerCheckBox(this,checkboxselectedeventselector(FriendUi::selectAcceptTeam));


		CheckBox_refuse=(UICheckBox *)UIHelper::seekWidgetByName(friendPanel,"CheckBox_refuse");
		CheckBox_refuse->setTouchEnable(true);
		if (MyPlayerAIConfig::getRefuseTeam() == true)
		{
			CheckBox_refuse->setSelectedState(true);
		}else
		{
			CheckBox_refuse->setSelectedState(false);
		}
		
		CheckBox_refuse->addEventListenerCheckBox(this,checkboxselectedeventselector(FriendUi::selectRefuseTeam));

		UIButton *button_callTogether =(UIButton *)UIHelper::seekWidgetByName(friendPanel,"Button_538");
		button_callTogether->setPressedActionEnabled(true);
		button_callTogether->setTouchEnable(true);
		button_callTogether->addReleaseEvent(this,coco_releaseselector(FriendUi::callToTogether));

		UIButton *button_autoTeam =(UIButton *)UIHelper::seekWidgetByName(friendPanel,"Button_538_0");
		button_autoTeam->setPressedActionEnabled(true);
		button_autoTeam->setTouchEnable(true);
		button_autoTeam->addReleaseEvent(this,coco_releaseselector(FriendUi::autoApplyTeam));
		//button exit
		UIButton *button_exit =(UIButton *)UIHelper::seekWidgetByName(friendPanel,"Button_close");
		button_exit->setPressedActionEnabled(true);
		button_exit->setTouchEnable(true);
		button_exit->addReleaseEvent(this,coco_releaseselector(FriendUi::callBackExit));


		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(winsize);
		return true;
	}
	return false;
}

void FriendUi::onEnter()
{
	UIScene::onEnter();
	this->openAnim();

	GameUtils::playGameSound(SCENE_OPEN, 2, false);

	friendWorkList =FriendWorkList::create();
	friendWorkList->ignoreAnchorPointForPosition(false);
	friendWorkList->setAnchorPoint(ccp(0,0));
	friendWorkList->setPosition(ccp(25,25));
	layer_->addChild(friendWorkList,2,FRIENDWORKLISTTAG);
	friendWorkList->init();

	teamList=TeamList::create();
	teamList->ignoreAnchorPointForPosition(false);
	teamList->setAnchorPoint(ccp(0,0));
	teamList->setPosition(ccp(74,91));
	layer_->addChild(teamList,2,TEAMLISTTAG);
	teamList->init();
	teamList->setVisible(false);
	//////
	aroundplayer= AroundPlayerTabview::create();
	aroundplayer->ignoreAnchorPointForPosition(false);
	aroundplayer->setAnchorPoint(ccp(0,0));
	aroundplayer->setPosition(ccp(60,51));
	layer_->addChild(aroundplayer);
	aroundplayer->init();
	aroundplayer->setVisible(false);
}

void FriendUi::onExit()
{
	UIScene::onExit();
	SimpleAudioEngine::sharedEngine()->playEffect(SCENE_CLOSE, false);
}

void FriendUi::callBackChangeFriendType( CCObject * obj )
{
	UITab * m_tab=(UITab *)obj;
	int num= m_tab->getCurrentIndex();

	pageIndexSelect=0;
	switch(num)
	{
	case 0:
		{
			curType=FRIENDLIST;
			FriendList friend1={curType,pageNumSelect,pageIndexSelect,showOnlineSelect};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2202,&friend1);

			friendWorkList->setVisible(true);
			teamList->setVisible(false);
			aroundplayer->setVisible(false);

			friendWork->setVisible(true);
			nameBox->setVisible(false);
			
			palPanel->setVisible(true);
			checkResultPanel->setVisible(false);
			checkPanel->setVisible(false);
			panelTeam->setVisible(false);
			panelOtherplayer->setVisible(false);

			refreshCheckCountryAndPression(false);
// 			CheckBox_pro_HaoJie->setSelectedState(false);
// 			CheckBox_pro_GuiMou->setSelectedState(false);
// 			CheckBox_pro_MengJiang->setSelectedState(false);
// 			CheckBox_pro_ShenShe->setSelectedState(false);

			UILayer * teamlayer = (UILayer *)layer_->getChildByTag(UILAYEROFTEAMTAG);
			if (teamlayer != NULL)
			{
				teamlayer->removeFromParentAndCleanup(true);
			}

		}break;
	case 1:
		{
			curType=ENEMYLIST;

			FriendList friend1={curType,pageNumSelect,pageIndexSelect,showOnlineSelect};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2202,&friend1);

			friendWorkList->setVisible(true);
			teamList->setVisible(false);
			aroundplayer->setVisible(false);

			friendWork->setVisible(false);
			nameBox->setVisible(false);
			
			palPanel->setVisible(true);
			checkResultPanel->setVisible(false);
			checkPanel->setVisible(false);
			panelTeam->setVisible(false);
			panelOtherplayer->setVisible(false);

			refreshCheckCountryAndPression(false);
// 			CheckBox_pro_HaoJie->setSelectedState(false);
// 			CheckBox_pro_GuiMou->setSelectedState(false);
// 			CheckBox_pro_MengJiang->setSelectedState(false);
// 			CheckBox_pro_ShenShe->setSelectedState(false);
			
			UILayer * teamlayer = (UILayer *)layer_->getChildByTag(UILAYEROFTEAMTAG);
			if (teamlayer != NULL)
			{
				teamlayer->removeFromParentAndCleanup(true);
			}
		}break;
	case 2:
		{
			curType=BACKLIST;
			FriendList friend1={curType,pageNumSelect,pageIndexSelect,showOnlineSelect};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2202,&friend1);

			friendWorkList->setVisible(true);
			teamList->setVisible(false);
			aroundplayer->setVisible(false);

			friendWork->setVisible(false);
			nameBox->setVisible(false);
			
			palPanel->setVisible(true);
			checkResultPanel->setVisible(false);
			checkPanel->setVisible(false);
			panelTeam->setVisible(false);
			panelOtherplayer->setVisible(false);

			refreshCheckCountryAndPression(false);
// 			CheckBox_pro_HaoJie->setSelectedState(false);
// 			CheckBox_pro_GuiMou->setSelectedState(false);
// 			CheckBox_pro_MengJiang->setSelectedState(false);
// 			CheckBox_pro_ShenShe->setSelectedState(false);

			UILayer * teamlayer = (UILayer *)layer_->getChildByTag(UILAYEROFTEAMTAG);
			if (teamlayer != NULL)
			{
				teamlayer->removeFromParentAndCleanup(true);
			}

		}break;
	case 3:
		{
			curType=TEAMPLAYER;
			//team
			void *teamId =(void *)0;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1408,teamId);
			
			friendWorkList->setVisible(false);
			teamList->setVisible(true);
			aroundplayer->setVisible(false);

			friendWork->setVisible(false);
			nameBox->setVisible(false);

			palPanel->setVisible(false);
			checkResultPanel->setVisible(false);
			checkPanel->setVisible(false);
			panelTeam->setVisible(true);
			panelOtherplayer->setVisible(false);

			refreshCheckCountryAndPression(false);
// 			CheckBox_pro_HaoJie->setSelectedState(false);
// 			CheckBox_pro_GuiMou->setSelectedState(false);
// 			CheckBox_pro_MengJiang->setSelectedState(false);
// 			CheckBox_pro_ShenShe->setSelectedState(false);
		
			if (GameView::getInstance()->mapteamVector.size()>0)
			{
				this->showTeamPlayer(0);
			}
			
		}break;
	case 4:
		{
			curType=AROUNDPLAYER;
			void *otherPlayer = (void *)1;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1408,otherPlayer);
			
			friendWorkList->setVisible(false);
			teamList->setVisible(false);
			aroundplayer->setVisible(true);
			//otherplayer
			friendWork->setVisible(false);
			nameBox->setVisible(false);
			
			palPanel->setVisible(false);
			checkResultPanel->setVisible(false);
			checkPanel->setVisible(false);
			panelTeam->setVisible(false);
			panelOtherplayer->setVisible(true);
			
			refreshCheckCountryAndPression(false);
// 			CheckBox_pro_HaoJie->setSelectedState(false);
// 			CheckBox_pro_GuiMou->setSelectedState(false);
// 			CheckBox_pro_MengJiang->setSelectedState(false);
// 			CheckBox_pro_ShenShe->setSelectedState(false);

			UILayer * teamlayer = (UILayer *)layer_->getChildByTag(UILAYEROFTEAMTAG);
			if (teamlayer != NULL)
			{
				teamlayer->removeFromParentAndCleanup(true);
			}
		}break;
	}
	//this->reloadSourceData(obj);
}

void FriendUi::callBackFriendButton( CCObject * obj )
{
	UITab * m_tab=(UITab *)obj;

	m_tab->setHightLightLabelColor(ccc3(47,93,13));
	m_tab->setNormalLabelColor(ccc3(255,255,255));


	int num= m_tab->getCurrentIndex();

	std::vector<CRelationPlayer *>::iterator iter;
	for (iter=GameView::getInstance()->relationSourceVector.begin();iter!=GameView::getInstance()->relationSourceVector.end();iter++)
	{
		delete *iter;
	}
	GameView::getInstance()->relationSourceVector.clear();
	pageIndexSelect=0;
	switch(num)
	{
	case 0:
		{
			curType=FRIENDLIST;
			FriendList friend1={curType,pageNumSelect,pageIndexSelect,0};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2202,&friend1);

			friendWorkList->setVisible(true);
			teamList->setVisible(false);
			aroundplayer->setVisible(false);

			friendWork->setVisible(true);
			nameBox->setVisible(false);

			palPanel->setVisible(true);
			checkResultPanel->setVisible(false);
			checkPanel->setVisible(false);
			panelTeam->setVisible(false);
			panelOtherplayer->setVisible(false);
			
			UILayer * teamlayer = (UILayer *)layer_->getChildByTag(UILAYEROFTEAMTAG);
			if (teamlayer != NULL)
			{
				teamlayer->removeFromParentAndCleanup(true);
			}
		}break;
	case 1:
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2211,this);

			friendWorkList->setVisible(true);
			teamList->setVisible(false);
			aroundplayer->setVisible(false);

			friendWork->setVisible(true);
			nameBox->setVisible(false);

			palPanel->setVisible(true);
			checkResultPanel->setVisible(false);
			checkPanel->setVisible(false);
			panelTeam->setVisible(false);
			panelOtherplayer->setVisible(false);
			UILayer * teamlayer = (UILayer *)layer_->getChildByTag(UILAYEROFTEAMTAG);
			if (teamlayer != NULL)
			{
				teamlayer->removeFromParentAndCleanup(true);
			}
		}break;
	case 2:
		{
			friendWorkList->setVisible(false);
			teamList->setVisible(false);
			aroundplayer->setVisible(false);

			label_lv1->setText("");
			label_lv2->setText("");
			friendWork->setVisible(true);
			nameBox->setVisible(true);

			palPanel->setVisible(false);
			checkResultPanel->setVisible(false);
			checkPanel->setVisible(true);
			panelTeam->setVisible(false);
			panelOtherplayer->setVisible(false);

			minLvSelect = 0;
			maxLvSelect = 0;

			UILayer * teamlayer = (UILayer *)layer_->getChildByTag(UILAYEROFTEAMTAG);
			if (teamlayer != NULL)
			{
				teamlayer->removeFromParentAndCleanup(true);
			}
		}break;
	}
}
void FriendUi::callBackExit( CCObject * obj )
{
	this->closeAnim();
}

void FriendUi::callBackChackButton( CCObject * obj )
{	
	countryVector.clear();
	if (checkBox_country_yi->getSelectedState() == true)
	{
		countryVector.push_back(1);
	}
	if (checkBox_country_yang->getSelectedState() == true)
	{
		countryVector.push_back(2);
	}
	if (checkBox_country_jing->getSelectedState() == true)
	{
		countryVector.push_back(3);
	}
	/*
	if (checkBox_country_you->getSelectedState() == true)
	{
		countryVector.push_back(4);
	}
	if (checkBox_country_liang->getSelectedState() == true)
	{
		countryVector.push_back(5);
	}
	*/

	professionVector.clear();
	if (CheckBox_pro_MengJiang->getSelectedState()==true)//猛将、鬼谋、豪杰、神射
	{
		professionSelect=1;
		professionVector.push_back(professionSelect);
	}

	if (CheckBox_pro_GuiMou->getSelectedState()==true)
	{
		professionSelect=2;
		professionVector.push_back(professionSelect);
	}
	if (CheckBox_pro_HaoJie->getSelectedState()==true)
	{
		professionSelect=3;
		professionVector.push_back(professionSelect);
	}
	if (CheckBox_pro_ShenShe->getSelectedState()==true)
	{
		professionSelect=4;
		professionVector.push_back(professionSelect);
	}

 	const char* name_ = nameBox->getInputString();
// 	if(name_ == NULL && professionVector.size() <= 0 && countryVector.size() <= 0 && maxLvSelect <=0 && minLvSelect<=0 )
// 	{		
// 		const char *strings = StringDataManager::getString("friend_search");
// 		GameView::getInstance()->showAlertDialog(strings);
// 		return;
// 	}
	
	if (isShowFriendCheckbox->getSelectedState()==true)
	{
		showOnlineSelect=1;
	}else
	{
		showOnlineSelect=0;
	}
	if (name_ == NULL)
	{
		nameString_ = "";
	}else
	{
		nameString_ = name_;
	}
	pageIndexSelect = 0;
	curType = CHECKTABLIST_CHECKBOX;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(2209,this);
	///////////
	label_lv1->setText("");
	label_lv2->setText("");
	palPanel->setVisible(true);
	nameBox->deleteAllInputString();

	nameBox->setVisible(false);
	palPanel->setVisible(false);
	friendWorkList->setVisible(true);
	checkResultPanel->setVisible(true);
	checkPanel->setVisible(false);
	button_backCheck->setVisible(true);
	button_addFriend->setVisible(true);
	refreshCheckCountryAndPression(false);
// 	CheckBox_pro_HaoJie->setSelectedState(false);
// 	CheckBox_pro_GuiMou->setSelectedState(false);
// 	CheckBox_pro_MengJiang->setSelectedState(false);
// 	CheckBox_pro_ShenShe->setSelectedState(false);
}

void FriendUi::callBackKeyPersonalButton( CCObject * obj )
{
	if (isShowFriendCheckbox->getSelectedState()==true)
	{
		showOnlineSelect=1;
	}else
	{
		showOnlineSelect=0;
	}
	curType = CHECKTABLIST_ONEKEY;
	pageIndexSelect = 0;
	//随机选出与玩家角色级别相差最少的10名其他非好友玩家角色
	GameMessageProcessor::sharedMsgProcessor()->sendReq(2210,this);

	palPanel->setVisible(false);
	friendWorkList->setVisible(true);
	checkResultPanel->setVisible(true);
	checkPanel->setVisible(false);
	button_backCheck->setVisible(true);
	button_addFriend->setVisible(true);

	nameBox->setVisible(false);

	CheckBox_pro_HaoJie->setSelectedState(false);
	CheckBox_pro_GuiMou->setSelectedState(false);
	CheckBox_pro_MengJiang->setSelectedState(false);
	CheckBox_pro_ShenShe->setSelectedState(false);
}

void FriendUi::callBackButton_backCheck( CCObject * obj )
{
	//查找
	palPanel->setVisible(false);
	friendWorkList->setVisible(false);
	checkPanel->setVisible(true);
	nameBox->setVisible(true);

	label_lv1->setText("");
	label_lv2->setText("");
	checkResultPanel->setVisible(false);
	checkPanel->setVisible(true);
	panelTeam->setVisible(false);
	panelOtherplayer->setVisible(false);
}

void FriendUi::callbackButton_addFriend( CCObject * obj )
{
	operation_=0;
	relationtype_=0;

	for (int i=0;i<GameView::getInstance()->relationSourceVector.size();i++)
	{
		playerId_ = GameView::getInstance()->relationSourceVector.at(i)->playerid();
		playerName_=GameView::getInstance()->relationSourceVector.at(i)->playername();
		FriendStruct friend1={0,0,playerId_,playerName_};
		GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend1);
	}

	refreshCheckCountryAndPression(false);
	//成员变化 在2201里面处理过了
	//this->reloadSourceData();
}

bool FriendUi::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	return true;
}

void FriendUi::callBackPopCounterLV1( CCObject * obj )
{
	if (this->isClosing())
		return;

	GameView::getInstance()->showCounter(this,callfuncO_selector(FriendUi::callBackLabelLV1));
	
}
void FriendUi::callBackPopCounterLV2( CCObject * obj )
{
	if (this->isClosing())
		return;

	GameView::getInstance()->showCounter(this,callfuncO_selector(FriendUi::callBackLabelLV2));
}

void FriendUi::callBackLabelLV1( CCObject * obj )
{
	Counter * couter_ =(Counter *)obj;
	if (maxLvSelect != 0)
	{
		if (couter_->getInputNum() > 999999 || couter_->getInputNum() > maxLvSelect)
		{
			const char *str_friend = StringDataManager::getString("friend_putIn_maxLevel_not");
			GameView::getInstance()->showAlertDialog(str_friend);
		}else
		{
			minLvSelect = couter_->getInputNum();
			char string_LV[10];
			sprintf(string_LV,"%d",minLvSelect);
			label_lv1->setText(string_LV);
		}
	}else
	{
		if (couter_->getInputNum() > 999999)
		{
			const char *str_friend = StringDataManager::getString("friend_putIn_maxLevel_not");
			GameView::getInstance()->showAlertDialog(str_friend);
		}else
		{
			minLvSelect = couter_->getInputNum();
			char string_LV[10];
			sprintf(string_LV,"%d",minLvSelect);
			label_lv1->setText(string_LV);
		}
	}
}

void FriendUi::callBackLabelLV2( CCObject * obj )
{
	Counter * couter_ =(Counter *)obj;

	if (couter_->getInputNum() > 999999 || couter_->getInputNum() < minLvSelect )
	{
		const char *str_friend = StringDataManager::getString("friend_putIn_maxLevel_not");
		GameView::getInstance()->showAlertDialog(str_friend);
	}else
	{
		maxLvSelect = couter_->getInputNum();
		char string_LV[10];
		sprintf(string_LV,"%d",maxLvSelect);
		label_lv2->setText(string_LV);
	}
}

void FriendUi::showfriendOfLine( CCObject * obj )
{
	std::vector<CRelationPlayer *>::iterator iter;
	for (iter=GameView::getInstance()->relationSourceVector.begin();iter!=GameView::getInstance()->relationSourceVector.end();iter++)
	{
		delete *iter;
	}
	GameView::getInstance()->relationSourceVector.clear();

	int temp_num = 0;
	if (isShowFriendCheckbox->getSelectedState()==false)
	{
		showOnlineSelect=1;

		for (int i=0;i<GameView::getInstance()->relationFriendVector.size();i++)
		{
			if (GameView::getInstance()->relationFriendVector.at(i)->online()==1)
			{
				CRelationPlayer * player=new CRelationPlayer();
				player->CopyFrom(*(GameView::getInstance()->relationFriendVector.at(i)));

				GameView::getInstance()->relationSourceVector.push_back(player);
				temp_num++;
			}
		}
	}else
	{
		showOnlineSelect=0;
		for (int i=0;i<GameView::getInstance()->relationFriendVector.size();i++)
		{
			CRelationPlayer * player=new CRelationPlayer();
			player->CopyFrom(*(GameView::getInstance()->relationFriendVector.at(i)));

			GameView::getInstance()->relationSourceVector.push_back(player);

			temp_num++;
		}
	}
	this->reloadSourceData(temp_num);
}

void FriendUi::showTeamPlayer(int idx)
{
	UILayer * teamlayer = (UILayer *)layer_->getChildByTag(UILAYEROFTEAMTAG);
	if (teamlayer != NULL)
	{
		teamlayer->removeFromParentAndCleanup(true);
	}
	if (GameView::getInstance()->mapteamVector.size()<=0)
	{
		return;
	}
	if (curType != 3)
	{
		return;
	}
	UILayer * layerOfTeam =UILayer::create();
	layerOfTeam->ignoreAnchorPointForPosition(false);
	layerOfTeam->setAnchorPoint(ccp(0.5f,0.5f));
	layerOfTeam->setPosition(ccp(winsize.width/2,winsize.height/2));
	layerOfTeam->setTag(UILAYEROFTEAMTAG);
	layer_->addChild(layerOfTeam);
	for (int i=0;i<GameView::getInstance()->mapteamVector.at(idx)->teammembers_size();i++)
	{
		long long leaderid = GameView::getInstance()->mapteamVector.at(idx)->leaderid();
		CTeamMember * teammenber = new CTeamMember();
		teammenber->CopyFrom(GameView::getInstance()->mapteamVector.at(idx)->teammembers(i));

		TeamMemberList * teamlistPlayer =TeamMemberList::create(teammenber,leaderid);
		teamlistPlayer->setAnchorPoint(ccp(0,0));
		teamlistPlayer->setPosition(ccp(438,310-i*55));
		teamlistPlayer->setTag(50+i);
		teamlistPlayer->setZOrder(10);
		layerOfTeam->addChild(teamlistPlayer);
		delete teammenber;
	}
}

void FriendUi::selectAcceptTeam(CCObject * obj,CheckBoxEventType type )
{
	UICheckBox * checkbox_ = (UICheckBox *)obj;
	if (checkbox_->getSelectedState() == true)
	{
		selectIndex=0;
		fuctionValue=1;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1405,(void *)selectIndex,(void *)fuctionValue);
		CheckBox_refuse->setSelectedState(false);
		MyPlayerAIConfig::setAcceptTeam(true);
		MyPlayerAIConfig::setRefuseTeam(false);
	}else
	{
		MyPlayerAIConfig::setAcceptTeam(false);
		selectIndex=0;
		fuctionValue=0;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1405,(void *)selectIndex,(void *)fuctionValue);
	}
}

void FriendUi::selectRefuseTeam(CCObject * obj,CheckBoxEventType type )
{
	UICheckBox * check_ = (UICheckBox *)obj;
	if (check_->getSelectedState() == true)
	{
		selectIndex=1;
		fuctionValue=1;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1405,(void *)selectIndex,(void *)fuctionValue);
		CheckBox_accept->setSelectedState(false);
		MyPlayerAIConfig::setAcceptTeam(false);
		MyPlayerAIConfig::setRefuseTeam(true);
	}else
	{
		MyPlayerAIConfig::setRefuseTeam(false);
		selectIndex=1;
		fuctionValue=0;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1405,(void *)selectIndex,(void *)fuctionValue);
	}
}

void FriendUi::callToTogether( CCObject * obj )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1414);
}

void FriendUi::autoApplyTeam( CCObject * obj )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1413);
}


void FriendUi::reloadSourceData(int playerNum)
{
	CCTableView * friendTab =(CCTableView *)friendWorkList->getChildByTag(FRIENDWORKLISTTAB);
	friendTab->reloadData();

	switch(curType)
	{
	case 0:
		{
			const char *str_friend = StringDataManager::getString("friend_LabelshowStr_friend");
			labelFriendType->setText(str_friend);
		}break;
	case 1:
		{
			const char *str_friend = StringDataManager::getString("friend_LabelshowStr_black");
			labelFriendType->setText(str_friend);
		}break;
	case 2:
		{	
			const char *str_friend = StringDataManager::getString("friend_LabelshowStr_enemy");
			labelFriendType->setText(str_friend);
		}break;
	}

	std::string showFriend = "";
	char friendnum_[10];
	sprintf(friendnum_,"%d",playerNum);
	showFriend.append(friendnum_);
	showFriend.append("/50");
	label_Friendnum->setText(showFriend.c_str());
}

void FriendUi::refreshCheckCountryAndPression( bool isSelect )
{
	CheckBox_pro_HaoJie->setSelectedState(isSelect);
	CheckBox_pro_MengJiang->setSelectedState(isSelect);
	CheckBox_pro_GuiMou->setSelectedState(isSelect);
	CheckBox_pro_ShenShe->setSelectedState(isSelect);

	checkBox_country_yi->setSelectedState(isSelect);
	checkBox_country_yang->setSelectedState(isSelect);
	checkBox_country_jing->setSelectedState(isSelect);
	//checkBox_country_you->setSelectedState(isSelect);
	//checkBox_country_liang->setSelectedState(isSelect);
}

void FriendUi::callBackPhypower( CCObject * obj )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5132);
}

void FriendUi::setTouchCellType(int type_)
{
	m_touchType = type_;
}

int FriendUi::getTouchCellType()
{
	return m_touchType;
}
