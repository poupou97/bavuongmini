#ifndef _TEAMMEMBERLIST_H
#define _TEAMMEMBERLIST_H

#include "../extensions/UIScene.h"


class CTeamMember;
class TeamMemberList:public UIScene
{
public:
	TeamMemberList(void);
	~TeamMemberList(void);

	static TeamMemberList *create(CTeamMember * teammenber,long long leaderid);
	bool init(CTeamMember * teammenber,long long leaderid);

	void onEnter();
	void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);
	void callBackUitab(CCObject * obj);
private:
	CTeamMember * m_teammenber;
	long long m_leaderid;

};

////////////////////////////////////////////////////
class TeamMemberListOperator:public UIScene
{
public:
	TeamMemberListOperator(void);
	~TeamMemberListOperator(void);

	static TeamMemberListOperator *create(CTeamMember * teammenber,long long leaderid);
	bool init(CTeamMember * teammenber,long long leaderid);

	void onEnter();
	void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);

	void callBack1(CCObject * obj);
	void callBack2(CCObject * obj);
private:
	CCSize winSize;
	CTeamMember * mm_teammenber;

	struct FriendStruct
	{
		int operation;
		int relationType;
		long long playerid;
		std::string playerName;
	};
};

#endif