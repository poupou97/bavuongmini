#include "RewardUi.h"
#include "GameView.h"
#include "../../messageclient/element/CRewardBase.h"
#include "AppMacros.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/extensions/CCMoveableMenu.h"
#include "../../ui/GameUIConstant.h"
#include "../../ui/SignDaily_ui/SignDailyUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/vip_ui/VipEveryDayRewardsUI.h"
#include "../../ui/offlinearena_ui/OffLineArenaUI.h"
#include "../../ui/offlineExp_ui/OffLineExpUI.h"
#include "../../ui/challengeRound/RankRewardUi.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../ui/backpackscene/BattleAchievementUI.h"
#include "../family_ui/familyFight_ui/FamilyFightRewardsUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../ui/SignDaily_ui/SignDailyData.h"
#include "../../gamescene_state/sceneelement/GuideMap.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../ui/OnlineReward_ui/OnLineRewardUI.h"
#include "../../ui/OnlineReward_ui/OnlineGiftData.h"
#include "../../ui/OnlineReward_ui/OnlineGiftLayer.h"
#include "../../ui/Active_ui/WorldBoss_ui/GetRewardUI.h"
#include "../moneyTree_ui/MoneyTreeUI.h"
#include "../moneyTree_ui/MoneyTreeData.h"
#include "ui/signMonthly_ui/SignMonthlyUI.h"
#include "ui/signMonthly_ui/SignMonthlyData.h"

RewardUi::RewardUi(void)
{
	m_uiState = state_open;
	m_openUiTag = 0;
}


RewardUi::~RewardUi(void)
{
}

RewardUi * RewardUi::create()
{
	RewardUi * reward_ui = new RewardUi();
	if (reward_ui && reward_ui->init())
	{
		reward_ui->autorelease();
		return reward_ui;
	}
	CC_SAFE_DELETE(reward_ui);
	return NULL;
}

bool compareCellId(CRewardBase * reward1,CRewardBase * reward2)
{
	return reward1->getClose() < reward2->getClose();
}


bool RewardUi::init()
{
	if (UIScene::init())
	{
		winsize= CCDirector::sharedDirector()->getVisibleSize();

		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winsize);
		mengban->setAnchorPoint(ccp(0,0));
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);

		UIPanel *mainPanel = (UIPanel *)GUIReader::shareReader()->widgetFromJsonFile("res_ui/reward_1.json");
		mainPanel->setAnchorPoint(ccp(0.5f,0.5f));
		mainPanel->setPosition(ccp(winsize.width/2,winsize.height/2));
		m_pUiLayer->addWidget(mainPanel);

		//sort vector
		sort(GameView::getInstance()->rewardvector.begin(),GameView::getInstance()->rewardvector.end(),compareCellId);

		UILayer *loadLayer= UILayer::create();
		loadLayer->ignoreAnchorPointForPosition(false);
		loadLayer->setAnchorPoint(ccp(0.5f,0.5f));
		loadLayer->setPosition(ccp(winsize.width/2,winsize.height/2));
		loadLayer->setContentSize(CCSizeMake(600,440));
		this->addChild(loadLayer);

		tablevie_reward = CCTableView::create(this, CCSizeMake(485,318));
		tablevie_reward->setSelectedEnable(true);
		tablevie_reward->setDirection(kCCScrollViewDirectionVertical);
		tablevie_reward->setAnchorPoint(ccp(0,0));
		tablevie_reward->setPosition(ccp(61,40));
		tablevie_reward->setDelegate(this);
		tablevie_reward->setVerticalFillOrder(kCCTableViewFillTopDown);
		loadLayer->addChild(tablevie_reward);
		tablevie_reward->reloadData();

		CCScale9Sprite * sp_ = CCScale9Sprite::create("res_ui/LV5_dikuang1_miaobian1.png");
		sp_->setPreferredSize(CCSizeMake(510,330));
		sp_->setCapInsets(CCRect(32,32,1,1));
		sp_->setAnchorPoint(ccp(0,0));
		sp_->setPosition(ccp(46,33));
		loadLayer->addChild(sp_);

		UIButton * btnClose =(UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_close");
		btnClose->setTouchEnable(true);
		btnClose->setPressedActionEnabled(true);
		btnClose->addReleaseEvent(this,coco_releaseselector(RewardUi::callBackExit));

		this->schedule(schedule_selector(RewardUi::updateShowCountTime),1.0f);
		this->scheduleUpdate();
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(winsize);
		return true;
	}
	return false;
}

void RewardUi::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void RewardUi::onExit()
{
	UIScene::onExit();
}

void RewardUi::scrollViewDidScroll( cocos2d::extension::CCScrollView * view )
{

}

void RewardUi::scrollViewDidZoom( cocos2d::extension::CCScrollView* view )
{

}

void RewardUi::tableCellTouched( cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell )
{
	
}

cocos2d::CCSize RewardUi::tableCellSizeForIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	return CCSizeMake(480,72);
}

cocos2d::extension::CCTableViewCell* RewardUi::tableCellAtIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	CCTableViewCell * cell = table->dequeueCell();
	cell = new CCTableViewCell();
	cell->autorelease();

	CCScale9Sprite * spbg = CCScale9Sprite::create("res_ui/kuang02_new.png");
	spbg->setPreferredSize(CCSizeMake(480,69));
	spbg->setCapInsets(CCRect(15,30,1,1));
	spbg->setAnchorPoint(ccp(0,0));
	spbg->setPosition(ccp(0,0));
	cell->addChild(spbg);
	//
	CCSprite * rewardIcon_sp = CCSprite::create("res_ui/mo_5.png");
	rewardIcon_sp->setAnchorPoint(ccp(0,0.5f));
	rewardIcon_sp->setPosition(ccp(17,36));
	rewardIcon_sp->setScale(1.3f);
	cell->addChild(rewardIcon_sp);
	//icon
	std::string icon_ = GameView::getInstance()->rewardvector.at(idx)->getIcon();
	//std::string temp_icon_path = "res_ui/gamescene_state/zhujiemian3/tubiao";
	std::string temp_icon_path = "res_ui/reward/";
	temp_icon_path.append(icon_);
	temp_icon_path.append(".png");

	CCSprite * rewardIcon_ = CCSprite::create(temp_icon_path.c_str());
	rewardIcon_->setAnchorPoint(ccp(0,0.5f));
	rewardIcon_->setPosition(ccp(17,36));
	cell->addChild(rewardIcon_);

	CCScale9Sprite * name_spbg =CCScale9Sprite::create("res_ui/LV4_diaa.png");
	name_spbg->setPreferredSize(CCSizeMake(166,27));
	name_spbg->setAnchorPoint(ccp(0.5f,0.5f));
	name_spbg->setPosition(ccp(165,46));
	name_spbg->setTag(CELL_NAME_SP_TAG);
	cell->addChild(name_spbg);
	//name
	std::string temp_name = GameView::getInstance()->rewardvector.at(idx)->getName();
	CCLabelTTF * rewardName_= CCLabelTTF::create(temp_name.c_str(), APP_FONT_NAME,16);
	rewardName_->setAnchorPoint(ccp(0.5f,0.5f));
	rewardName_->setPosition(ccp(82,13));
	rewardName_->setTag(CELL_NAME_TAG);
	name_spbg->addChild(rewardName_);

	//add count time 
	switch(GameView::getInstance()->rewardvector.at(idx)->getId())
	{
	case REWARD_LIST_ID_ONLINEREWARD:
		{
			OnlineGiftLayer * pOnlineGiftLayer = (OnlineGiftLayer *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOnLineGiftLayer);
			if (pOnlineGiftLayer != NULL)
			{
				std::string strings_ = pOnlineGiftLayer->get_timeLeft();
				
				CCLabelTTF * rewardCount_= CCLabelTTF::create(strings_.c_str(), APP_FONT_NAME,16);
				rewardCount_->setAnchorPoint(ccp(0,0.5f));
				rewardCount_->setPosition(ccp(name_spbg->getPositionX()+ name_spbg->getContentSize().width/2+20,46));//rewardCount_->getContentSize().width/2
				rewardCount_->setTag(REWARD_ONLINE_TIME_TAG);
				cell->addChild(rewardCount_);

				const char *str1 = StringDataManager::getString("label_timeout");
				if (strcmp(strings_.c_str(),str1) == 0)
				{
					rewardCount_->setVisible(false);
				}else
				{
					rewardCount_->setVisible(true);
				}
			}
		}break;
	case REWARD_LIST_ID_MONEYTREE:
		{
			std::string temp_moneyTree_name = GameView::getInstance()->rewardvector.at(idx)->getName();
			std::string temp_count = MoneyTreeData::instance()->get_numLeft();
			temp_moneyTree_name.append("(");
			temp_moneyTree_name.append(temp_count.c_str());
			temp_moneyTree_name.append(")");
			rewardName_->setString(temp_moneyTree_name.c_str());
			
			std::string strings_ = MoneyTreeData::instance()->get_strTimeLeft();
			CCLabelTTF * rewardCount_= CCLabelTTF::create(strings_.c_str(), APP_FONT_NAME,16);
			rewardCount_->setAnchorPoint(ccp(0,0.5f));
			rewardCount_->setPosition(ccp(name_spbg->getPositionX()+name_spbg->getContentSize().width/2+20,46));
			rewardCount_->setTag(REWARD_MONEYTREE_TIME_TAG);
			cell->addChild(rewardCount_);

			const char *str1 = StringDataManager::getString("MoneyTree_canGetInfo");
			if (strcmp(strings_.c_str(),str1) == 0)
			{
				GameView::getInstance()->rewardvector.at(idx)->setRewardState(1);
				rewardCount_->setVisible(false);
			}else
			{
				GameView::getInstance()->rewardvector.at(idx)->setRewardState(0);
				rewardCount_->setVisible(true);
			}
			
		}break;
	}

	//des
	std::string temp_des = GameView::getInstance()->rewardvector.at(idx)->getDes();
	CCLabelTTF * rewardDes_= CCLabelTTF::create(temp_des.c_str(), APP_FONT_NAME,14);
	rewardDes_->setAnchorPoint(ccp(0,1));
	rewardDes_->setPosition(ccp(80,30));
	cell->addChild(rewardDes_);

	UILayer * layer_ = UILayer::create();
	layer_->setTag(CELL_LAYER_TAG);
	cell->addChild(layer_);

	UIImageView * image_checkReward_sp = UIImageView::create();
	image_checkReward_sp->setTexture("res_ui/reward/btn_di.png");
	image_checkReward_sp->setAnchorPoint(ccp(0.5f,0.5f));
	image_checkReward_sp->setPosition(ccp(410,33));
	image_checkReward_sp->setName("image_checkReward_sp");
	layer_->addWidget(image_checkReward_sp);

	//menu 
	UIButton * btn_check = UIButton::create();
	btn_check->setTextures("res_ui/new_button_4.png","res_ui/new_button_4.png","");
	btn_check->setAnchorPoint(ccp(0.5f,0.5f));
	btn_check->setScale9Enable(true);
	btn_check->setScale9Size(CCSizeMake(81,40));
	btn_check->setCapInsets(CCRect(18,9,2,23));
	btn_check->setTouchEnable(true);
	btn_check->setPressedActionEnabled(true);
	btn_check->addReleaseEvent(this,coco_releaseselector(RewardUi::callBackGetReward));
	btn_check->setTag(idx);
	btn_check->setName("btn_onlineCheck_Reward"); 
	btn_check->setVisible(false);
	btn_check->setPosition(ccp(410,33));
	layer_->addWidget(btn_check);
	
	const char *strings_check = StringDataManager::getString("family_apply_btncheck");
	UILabel * label_ = UILabel::create();
	label_->setText(strings_check);
	label_->setAnchorPoint(ccp(0.5f,0.5f));
	label_->setPosition(ccp(0,0));
	label_->setFontSize(16);
	btn_check->addChild(label_);

	UIButton * btn_get = UIButton::create();
	btn_get->setTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
	btn_get->setScale9Enable(true);
	btn_get->setScale9Size(CCSizeMake(81,40));
	btn_get->setCapInsets(CCRect(18,9,2,23));
	btn_get->setAnchorPoint(ccp(0.5f,0.5f));
	btn_get->setTouchEnable(true);
	btn_get->setPressedActionEnabled(true);
	btn_get->addReleaseEvent(this,coco_releaseselector(RewardUi::callBackGetReward));
	btn_get->setTag(idx);
	btn_get->setName("btn_onlineget_Reward"); 
	btn_get->setVisible(false);
	btn_get->setPosition(ccp(410,33));
	layer_->addWidget(btn_get);

	const char *strings_getReward = StringDataManager::getString("label_timeout");
	UILabel * label_getReward = UILabel::create();
	label_getReward->setText(strings_getReward);
	label_getReward->setAnchorPoint(ccp(0.5f,0.5f));
	label_getReward->setPosition(ccp(0,0));
	label_getReward->setFontSize(16);
	btn_get->addChild(label_getReward);


	int temp_close = GameView::getInstance()->rewardvector.at(idx)->getClose();
	int temp_state = GameView::getInstance()->rewardvector.at(idx)->getRewardState();
	if (temp_close == 1 && temp_state == 0)
	{
		btn_check->setVisible(true);
		btn_get->setVisible(false);
		image_checkReward_sp->setVisible(false);
	}else
	{
		btn_check->setVisible(false);
		btn_get->setVisible(true);
		image_checkReward_sp->setVisible(true);
	}
	return cell;
}

unsigned int RewardUi::numberOfCellsInTableView( cocos2d::extension::CCTableView *table )
{
	return GameView::getInstance()->rewardvector.size();
}

void RewardUi::ccTouchMoved( CCTouch *pTouch, CCEvent *pEvent )
{

}

void RewardUi::ccTouchEnded( CCTouch *pTouch, CCEvent *pEvent )
{

}

void RewardUi::ccTouchCancelled( CCTouch *pTouch, CCEvent *pEvent )
{

}

void RewardUi::callBackCheck( CCObject * obj )
{

}

void RewardUi::callBackGetReward( CCObject * obj )
{	
	UIButton * btn_ = (UIButton *)obj;
	int index_ = btn_->getTag();
	int id_ = GameView::getInstance()->rewardvector.at(index_)->getId();
	MainScene * mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
	switch(id_)
	{
	case REWARD_LIST_ID_SIGNDAILY:
		{
			SignDailyUI * pSignDailyUI = (SignDailyUI  *)mainscene_->getChildByTag(kTagSignDailyUI);
			if (pSignDailyUI == NULL)
			{
				pSignDailyUI = SignDailyUI::create();
				pSignDailyUI->ignoreAnchorPointForPosition(false);
				pSignDailyUI->setAnchorPoint(ccp(0.5f, 0.5f));
				pSignDailyUI->setPosition(ccp(winsize.width / 2, winsize.height / 2));
				pSignDailyUI->setTag(kTagSignDailyUI);
				GameView::getInstance()->getMainUIScene()->addChild(pSignDailyUI);
				pSignDailyUI->refreshUI();

				m_openUiTag = kTagSignDailyUI;
			}
		}break;
	case REWARD_LIST_ID_ONLINEREWARD:
		{		
			OnLineRewardUI* pOnLineRewardUI = (OnLineRewardUI *)mainscene_->getChildByTag(kTagOnLineGiftUI);
			if (pOnLineRewardUI ==  NULL)
			{
				// 把nGiftNum + 1的数据 同步给 layer
				OnlineGiftLayer * pOnlineGiftLayer = (OnlineGiftLayer *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOnLineGiftLayer);
				if (NULL != pOnlineGiftLayer)
				{
					OnlineGiftData::instance()->getGiftDataTimeFromIcon(pOnlineGiftLayer->get_giftTime(), pOnlineGiftLayer->get_giftNum());
					OnlineGiftData::instance()->initDataFromIntent();
				}

				pOnLineRewardUI = OnLineRewardUI::create();
				pOnLineRewardUI->ignoreAnchorPointForPosition(false);
				pOnLineRewardUI->setAnchorPoint(ccp(0.5f, 0.5f));
				pOnLineRewardUI->setPosition(ccp(winsize.width / 2, winsize.height / 2));
				pOnLineRewardUI->setTag(kTagOnLineGiftUI);

				pOnLineRewardUI->refreshData();
				pOnLineRewardUI->refreshTimeAndNum();					// 更新 奖励编号 和 时间
				pOnLineRewardUI->update(0);

				GameView::getInstance()->getMainUIScene()->addChild(pOnLineRewardUI);

				m_openUiTag = kTagOnLineGiftUI;
			}

		}break;
	case REWARD_LIST_ID_VIPEVERYDAYREWARD:
		{
			CCLayer *vipEveryDayRewardScene = (CCLayer*)mainscene_->getChildByTag(kTagVipEveryDayRewardsUI);
			if(vipEveryDayRewardScene == NULL)
			{
				VipEveryDayRewardsUI * vpEveryDayRewardsUI = VipEveryDayRewardsUI::create();
				vpEveryDayRewardsUI->ignoreAnchorPointForPosition(false);
				vpEveryDayRewardsUI->setAnchorPoint(ccp(0.5f,0.5f));
				vpEveryDayRewardsUI->setPosition(ccp(winsize.width/2, winsize.height/2));
				mainscene_->addChild(vpEveryDayRewardsUI,0,kTagVipEveryDayRewardsUI);

				m_openUiTag = kTagVipEveryDayRewardsUI;
			}
		}break;
	case REWARD_LIST_ID_OFFLINEEXP:
		{
			OffLineExpUI *offLineExpUI = (OffLineExpUI*)mainscene_->getChildByTag(kTagOffLineExpUI);
			if(offLineExpUI == NULL)
			{
				offLineExpUI = OffLineExpUI::create();
				offLineExpUI->ignoreAnchorPointForPosition(false);
				offLineExpUI->setAnchorPoint(ccp(0.5f, 0.5f));
				offLineExpUI->setPosition(ccp(winsize.width/2, winsize.height/2));
				offLineExpUI->setTag(kTagOffLineExpUI);
				GameView::getInstance()->getMainUIScene()->addChild(offLineExpUI);

				m_openUiTag = kTagOffLineExpUI;
			}
		}break;
	case REWARD_LIST_ID_SINGCOPYGETREWARD_CHUSHINIUDAO:
		{
			RankRewardUi * ranklist_ = (RankRewardUi *)mainscene_->getChildByTag(ktagSingCopyReward);
			if (ranklist_ == NULL)
			{
				ranklist_ = RankRewardUi::create(1);
				ranklist_->ignoreAnchorPointForPosition(false);
				ranklist_->setAnchorPoint(ccp(0.5f,0.5f));
				ranklist_->setPosition(ccp(winsize.width/2,winsize.height/2));
				ranklist_->setTag(ktagSingCopyReward);
				GameView::getInstance()->getMainUIScene()->addChild(ranklist_);
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1905,(void *)1);

				m_openUiTag = ktagSingCopyReward;
			}
		}break;
	case REWARD_LIST_ID_SINGCOPYGETREWARD_XIAOYOUMINGQI:
		{
			RankRewardUi * ranklist_ = (RankRewardUi* )mainscene_->getChildByTag(ktagSingCopyReward);
			if (ranklist_ == NULL)
			{
				ranklist_ = RankRewardUi::create(2);
				ranklist_->ignoreAnchorPointForPosition(false);
				ranklist_->setAnchorPoint(ccp(0.5f,0.5f));
				ranklist_->setPosition(ccp(winsize.width/2,winsize.height/2));
				ranklist_->setTag(ktagSingCopyReward);
				GameView::getInstance()->getMainUIScene()->addChild(ranklist_);
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1905,(void *)2);

				m_openUiTag = ktagSingCopyReward;
			}
		}break;
	case REWARD_LIST_ID_SINGCOPYGETREWARD_JIANGONGLEYE:
		{
			RankRewardUi * ranklist_ = (RankRewardUi *)mainscene_->getChildByTag(ktagSingCopyReward);
			if (ranklist_ == NULL)
			{
				ranklist_ = RankRewardUi::create(3);
				ranklist_->ignoreAnchorPointForPosition(false);
				ranklist_->setAnchorPoint(ccp(0.5f,0.5f));
				ranklist_->setPosition(ccp(winsize.width/2,winsize.height/2));
				ranklist_->setTag(ktagSingCopyReward);
				GameView::getInstance()->getMainUIScene()->addChild(ranklist_);
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1905,(void *)3);

				m_openUiTag = ktagSingCopyReward;
			}
		}break;
	case REWARD_LIST_ID_OFFARENA:
		{
			OffLineArenaUI * offLineArenaUI = (OffLineArenaUI *)mainscene_->getChildByTag(kTagOffLineArenaUI);
			if (offLineArenaUI == NULL)
			{
				offLineArenaUI =OffLineArenaUI::create();
				offLineArenaUI->ignoreAnchorPointForPosition(false);
				offLineArenaUI->setAnchorPoint(ccp(0.5f,0.5f));
				offLineArenaUI->setPosition(ccp(winsize.width/2,winsize.height/2));
				offLineArenaUI->setTag(kTagOffLineArenaUI);
				GameView::getInstance()->getMainUIScene()->addChild(offLineArenaUI);

				m_openUiTag = kTagOffLineArenaUI;
			}
		}break;
	case REWARD_LIST_ID_PHYSICAL_DAY:
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5093);
		}break;
	case REWARD_LIST_ID_PHYSICAL_FRIEND:
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5132);
			m_openUiTag = kTagGetphyPowerUi;
		}break;
	case REWARD_LIST_ID_FAMILYFIGHT:
		{
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyFightRewardUI) == NULL)
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1544);

				FamilyFightRewardsUI * familyFightRewardUI = FamilyFightRewardsUI::create();
				familyFightRewardUI->ignoreAnchorPointForPosition(false);
				familyFightRewardUI->setAnchorPoint(ccp(.5f,.5f));
				familyFightRewardUI->setPosition(ccp(winsize.width/2,winsize.height/2));
				familyFightRewardUI->setTag(kTagFamilyFightRewardUI);
				GameView::getInstance()->getMainUIScene()->addChild(familyFightRewardUI);
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1545);

				m_openUiTag = kTagFamilyFightRewardUI;
			}
		}break;
	case REWARD_LIST_ID_BATTLEACHIEVEMENT:
		{
			BattleAchievementUI *tmpBattleAchUI = (BattleAchievementUI*)mainscene_->getChildByTag(kTagBattleAchievementUI);
			if (NULL == tmpBattleAchUI)
			{
				BattleAchievementUI* battleAchUI = BattleAchievementUI::create();
				battleAchUI->ignoreAnchorPointForPosition(false);
				battleAchUI->setAnchorPoint(ccp(0.5f, 0.5f));
				battleAchUI->setPosition(ccp(winsize.width/2,winsize.height/2));
				battleAchUI->setTag(kTagBattleAchievementUI);

				GameView::getInstance()->getMainUIScene()->addChild(battleAchUI);
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5101);

				m_openUiTag = kTagBattleAchievementUI;
			}
		}break;
	case REWARD_LIST_ID_WORLDBOSS:
		{
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGetRewardUI) == NULL)
			{
				CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
				GetRewardUI * getRewardUI = GetRewardUI::create();
				getRewardUI->ignoreAnchorPointForPosition(false);
				getRewardUI->setAnchorPoint(ccp(.5f,.5f));
				getRewardUI->setPosition(ccp(winSize.width/2,winSize.height/2));
				getRewardUI->setTag(kTagGetRewardUI);
				GameView::getInstance()->getMainUIScene()->addChild(getRewardUI);

				m_openUiTag = kTagGetRewardUI;
			}
		}break;
	case REWARD_LIST_ID_MONEYTREE:
		{
			MoneyTreeUI * pMoneyTreeUI = (MoneyTreeUI *)this->getChildByTag(kTagMoneyTreeUI);
			if (NULL == pMoneyTreeUI)
			{
				MoneyTreeUI* pTmpMoneyTreeUI = MoneyTreeUI::create();
				pTmpMoneyTreeUI->ignoreAnchorPointForPosition(false);
				pTmpMoneyTreeUI->setAnchorPoint(ccp(0.5f, 0.5f));
				pTmpMoneyTreeUI->setPosition(ccp(winsize.width/2, winsize.height/2));
				pTmpMoneyTreeUI->initDataFromInternet();
				pTmpMoneyTreeUI->setTag(kTagMoneyTreeUI);
				GameView::getInstance()->getMainUIScene()->addChild(pTmpMoneyTreeUI);

				m_openUiTag = kTagMoneyTreeUI;
			}
		}break;
		case REWARD_LIST_ID_SIGNMONTHLY:
			{
				//open ui
				SignMonthlyUI * signMonthUI = (SignMonthlyUI *)this->getChildByTag(kTagSignMonthlyUI);
				if (NULL == signMonthUI)
				{
					signMonthUI = SignMonthlyUI::create();
					signMonthUI->ignoreAnchorPointForPosition(false);
					signMonthUI->setAnchorPoint(ccp(.5f,.5f));
					signMonthUI->setPosition(ccp(winsize.width/2,winsize.height/2));
					GameView::getInstance()->getMainUIScene()->addChild(signMonthUI,0,kTagSignMonthlyUI);

					m_openUiTag = kTagSignMonthlyUI;
				}
			}break;
	}

	if (id_ != REWARD_LIST_ID_PHYSICAL_DAY)
	{
		m_uiState = state_visible;
	}
}

void RewardUi::callBackExit( CCObject * obj )
{
	this->closeAnim();
}

void RewardUi::addRewardListEvent( int index_ )
{
	for (int i = 0;i<GameView::getInstance()->rewardvector.size();i++)
	{
		if (index_ == GameView::getInstance()->rewardvector.at(i)->getId())
		{
			//if reward is have,not add again
			return;
		}
	}

	CRewardBase * temp_reward = new CRewardBase();
	temp_reward->setId(RewardListConfig::s_RewardListMsgData[index_]->getId());
	temp_reward->setName(RewardListConfig::s_RewardListMsgData[index_]->getName());
	temp_reward->setIcon(RewardListConfig::s_RewardListMsgData[index_]->getIcon());
	temp_reward->setDes(RewardListConfig::s_RewardListMsgData[index_]->getDes());
	temp_reward->setClose(RewardListConfig::s_RewardListMsgData[index_]->getClose());

	switch(index_)
	{
	case REWARD_LIST_ID_SIGNDAILY:
		{
			if (SignDailyData::instance()->isCanSign())
			{
				temp_reward->setRewardState(1);
			}else
			{
				temp_reward->setRewardState(0);
			}
		}break;
	case REWARD_LIST_ID_ONLINEREWARD:
		{
			temp_reward->setRewardState(0);
		}break;
	case REWARD_LIST_ID_MONEYTREE:
		{
			std::string strings_ = MoneyTreeData::instance()->get_strTimeLeft();
			const char *str1 = StringDataManager::getString("MoneyTree_canGetInfo");
			if (strcmp(strings_.c_str(),str1) == 0)
			{
				temp_reward->setRewardState(1);
			}else
			{
				temp_reward->setRewardState(0);
			}
		}break;
	default:
		{
			temp_reward->setRewardState(1);
		}break;
	case REWARD_LIST_ID_SIGNMONTHLY:
		{
			//是否可以领取
			if (SignMonthlyData::getInstance()->m_bCanGetGift)
			{
				temp_reward->setRewardState(1);
			}else
			{
				temp_reward->setRewardState(0);
			}
		}break;
	}
	GameView::getInstance()->rewardvector.push_back(temp_reward);

	if (GameView::getInstance()->getMainUIScene())
	{
		RewardUi * reward_ui = (RewardUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRewardUI);
		if (reward_ui != NULL)
		{
			reward_ui->tablevie_reward->reloadData();
		}
	}

	RewardUi::setRewardListParticle();
}

void RewardUi::removeRewardListEvent( int index )
{
	for (int i = 0;i<GameView::getInstance()->rewardvector.size();i++)
	{
		if (GameView::getInstance()->rewardvector.at(i)->getId() == index)
		{
			switch(index)
			{
			case REWARD_LIST_ID_SIGNDAILY:
				{
					GameView::getInstance()->rewardvector.at(i)->setRewardState(0);

					RewardUi * reward_ui = (RewardUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRewardUI);
					if (reward_ui != NULL)
					{
						reward_ui->tablevie_reward->updateCellAtIndex(i);
					}
				}break;
			case REWARD_LIST_ID_ONLINEREWARD:
				{
					GameView::getInstance()->rewardvector.at(i)->setRewardState(0);

					RewardUi * reward_ui = (RewardUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRewardUI);
					if (reward_ui != NULL)
					{
						reward_ui->tablevie_reward->updateCellAtIndex(i);
					}
				}break;
			default:
				{
					std::vector<CRewardBase *>::iterator iter= GameView::getInstance()->rewardvector.begin()+i;
					CRewardBase * rewardBase_ = *iter ;
					GameView::getInstance()->rewardvector.erase(iter);
					delete rewardBase_;	

					RewardUi * reward_ui = (RewardUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRewardUI);
					if (reward_ui != NULL)
					{
						reward_ui->tablevie_reward->reloadData();
					}
				}break;
			}
		}
	}

	RewardUi::setRewardListParticle();
}

void RewardUi::setRewardListParticle()
{
	bool isHave = false;
	for (int i = 0;i<GameView::getInstance()->rewardvector.size();i++)
	{
		if (GameView::getInstance()->rewardvector.at(i)->getRewardState() == 1)
		{
			isHave =  true;
			break;
		}
	}
	if (GameView::getInstance()->getMainUIScene() == NULL)
		return;

	GuideMap * guideMap = (GuideMap*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
	if (isHave== true)
	{
		if (guideMap &&  guideMap->getActionLayer())
		{
			if (guideMap->getActionLayer()->getWidgetByName("btn_onLineGift"))
			{
				guideMap->btn_RewardParticle->setVisible(true);
			}
		}
	}else
	{
		if (guideMap &&  guideMap->getActionLayer())
		{
			if (guideMap->getActionLayer()->getWidgetByName("btn_onLineGift"))
			{
				guideMap->btn_RewardParticle->setVisible(false);
			}
		}
	}
}

void RewardUi::updateShowCountTime( float dt )
{
	for (int i =0;i<GameView::getInstance()->rewardvector.size();i++)
	{
		switch(GameView::getInstance()->rewardvector.at(i)->getId())
		{
		case REWARD_LIST_ID_ONLINEREWARD:
			{
				CCTableViewCell * cell_ = tablevie_reward->cellAtIndex(i);
				if (cell_)
				{
					CCLabelTTF * label_time = (CCLabelTTF *)cell_->getChildByTag(REWARD_ONLINE_TIME_TAG);
					if (label_time)
					{
						OnlineGiftLayer * pOnlineGiftLayer = (OnlineGiftLayer *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOnLineGiftLayer);
						if (pOnlineGiftLayer != NULL)
						{
							UILayer *  layer_ = (UILayer *)cell_->getChildByTag(CELL_LAYER_TAG);

							std::string strings_ = pOnlineGiftLayer->get_timeLeft();
							const char *str1 = StringDataManager::getString("label_timeout");
							if (strcmp(strings_.c_str(),str1) == 0 && pOnlineGiftLayer->m_nGiftNum > -1 && pOnlineGiftLayer->m_nGiftNum <8)
							{
								label_time->setVisible(false);

								if (layer_ != NULL)
								{
									UIButton * btn_check = (UIButton *)layer_->getWidgetByName("btn_onlineCheck_Reward");
									UIButton * btn_get = (UIButton *)layer_->getWidgetByName("btn_onlineget_Reward");
									UIImageView * image_sp_ = (UIImageView *)layer_->getWidgetByName("image_checkReward_sp");
									if (btn_check!= NULL && btn_get!= NULL)
									{
										btn_check->setVisible(false);
										btn_get->setVisible(true);
										image_sp_->setVisible(true);
									}
								}
							}else
							{
								label_time->setString(strings_.c_str());
								if (pOnlineGiftLayer->m_nGiftNum > -1 && pOnlineGiftLayer->m_nGiftNum <8)
								{
									label_time->setVisible(true);
								}else
								{
									label_time->setVisible(false);
								}
								UIButton * btn_check = (UIButton *)layer_->getWidgetByName("btn_onlineCheck_Reward");
								UIButton * btn_get = (UIButton *)layer_->getWidgetByName("btn_onlineget_Reward");
								UIImageView * image_sp_ = (UIImageView *)layer_->getWidgetByName("image_checkReward_sp");
								if (btn_check!= NULL && btn_get!= NULL)
								{
									btn_check->setVisible(true);
									btn_get->setVisible(false);
									image_sp_->setVisible(false);
								}
							}
						}
					}
				}
			}break;
		case REWARD_LIST_ID_MONEYTREE:
			{
				CCTableViewCell * cell_ = tablevie_reward->cellAtIndex(i);
				if (cell_)
				{
					CCLabelTTF * label_time = (CCLabelTTF *)cell_->getChildByTag(REWARD_MONEYTREE_TIME_TAG);
					if (label_time)
					{
						UILayer *  layer_ = (UILayer *)cell_->getChildByTag(CELL_LAYER_TAG);

						std::string strings_ = MoneyTreeData::instance()->get_strTimeLeft();
						const char *str1 = StringDataManager::getString("MoneyTree_canGetInfo");
						label_time->setString(strings_.c_str());

						if (strcmp(strings_.c_str(),str1) == 0)
						{
							label_time->setVisible(false);
							if (layer_ != NULL)
							{
								UIButton * btn_check = (UIButton *)layer_->getWidgetByName("btn_onlineCheck_Reward");
								UIButton * btn_get = (UIButton *)layer_->getWidgetByName("btn_onlineget_Reward");
								UIImageView * image_sp_ = (UIImageView *)layer_->getWidgetByName("image_checkReward_sp");
								if (btn_check!= NULL && btn_get!= NULL)
								{
									btn_check->setVisible(false);
									image_sp_->setVisible(true);

									btn_get->setVisible(true);
								}
							}
						}else
						{
							label_time->setVisible(true);
							UIButton * btn_check = (UIButton *)layer_->getWidgetByName("btn_onlineCheck_Reward");
							UIButton * btn_get = (UIButton *)layer_->getWidgetByName("btn_onlineget_Reward");
							UIImageView * image_sp_ = (UIImageView *)layer_->getWidgetByName("image_checkReward_sp");
							if (btn_check!= NULL && btn_get!= NULL)
							{
								btn_check->setVisible(true);
								image_sp_->setVisible(false);

								btn_get->setVisible(false);
							}
						}
					}
				}
			}break;
		}
	}
}

void RewardUi::update( float delta )
{
	/*在点击cell打开新的ui时，隐藏此ui；当关闭新的ui时，再次显示此ui*/

	if (GameView::getInstance()->getMainUIScene()->getChildByTag(m_openUiTag) == NULL)
	{
		m_uiState = stat_show;
	}

	switch(m_uiState)
	{
	case state_open:
		{

		}break;
	case state_visible:
		{
			this->setVisible(false);
		}break;
	case stat_show:
		{
			this->setVisible(true);
		}break;
	}
}
