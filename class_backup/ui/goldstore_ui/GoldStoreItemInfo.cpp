#include "GoldStoreItemInfo.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"


GoldStoreItemInfo::GoldStoreItemInfo()
{
}


GoldStoreItemInfo::~GoldStoreItemInfo()
{
}

GoldStoreItemInfo * GoldStoreItemInfo::create( GoodsInfo * goodsInfo )
{
	GoldStoreItemInfo * goldStoreItemInfo = new GoldStoreItemInfo();
	if (goldStoreItemInfo && goldStoreItemInfo->init(goodsInfo))
	{
		goldStoreItemInfo->autorelease();
		return goldStoreItemInfo;
	}
	CC_SAFE_DELETE(goldStoreItemInfo);
	return NULL;
}

bool GoldStoreItemInfo::init( GoodsInfo * goodsInfo )
{
	if (GoodsItemInfoBase::init(goodsInfo,GameView::getInstance()->EquipListItem,0))
	{

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setAnchorPoint(ccp(0,0));
		return true;
	}
	return false;
}

void GoldStoreItemInfo::onEnter()
{
	GoodsItemInfoBase::onEnter();
	this->openAnim();
}

void GoldStoreItemInfo::onExit()
{
	GoodsItemInfoBase::onExit();
}

bool GoldStoreItemInfo::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	return resignFirstResponder(pTouch,this,true);
}

void GoldStoreItemInfo::ccTouchMoved( CCTouch *pTouch, CCEvent *pEvent )
{

}

void GoldStoreItemInfo::ccTouchEnded( CCTouch *pTouch, CCEvent *pEvent )
{

}

void GoldStoreItemInfo::ccTouchCancelled( CCTouch *pTouch, CCEvent *pEvent )
{

}

