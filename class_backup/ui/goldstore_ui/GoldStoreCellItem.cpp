#include "GoldStoreCellItem.h"
#include "../../messageclient/element/CLableGoods.h"
#include "../extensions/CCMoveableMenu.h"
#include "GoodsItem.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../utils/StaticDataManager.h"
#include "GoldStoreItemBuyInfo.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "AppMacros.h"
#include "../../utils/GameUtils.h"
#include "../../ui/backpackscene/EquipmentItem.h"
#include "../../ui/backpackscene/GoodsItemInfoBase.h"

GoldStoreCellItem::GoldStoreCellItem()
{
}


GoldStoreCellItem::~GoldStoreCellItem()
{
	delete curLableGoods;
}

GoldStoreCellItem* GoldStoreCellItem::create( CLableGoods * lableGoods )
{
	GoldStoreCellItem * goldStoreCellItem = new GoldStoreCellItem();
	if (goldStoreCellItem && goldStoreCellItem->init(lableGoods))
	{
		goldStoreCellItem->autorelease();
		return goldStoreCellItem;
	}
	CC_SAFE_DELETE(goldStoreCellItem);
	return NULL;
}

bool GoldStoreCellItem::init( CLableGoods * lableGoods )
{
	if (UIScene::init())
	{
		/////////////////////////////////////////////////////////////////////////////////////

		// 该UILayer位于CCTableView之内，为了正确响应TableView的拖动事件，故设置为不吞掉触摸事件
		m_pUiLayer->setSwallowsTouches(false);

		curLableGoods = new CLableGoods();
		curLableGoods->CopyFrom(*lableGoods);
		UIImageView * imageView_frame = UIImageView::create();
		imageView_frame->setTexture("res_ui/kuang01_new.png");
		imageView_frame->setScale9Enable(true);
		imageView_frame->setScale9Size(CCSizeMake(203,115));
		imageView_frame->setCapInsets(CCRectMake(15,30,1,1));
		imageView_frame->setRotation(180);
		imageView_frame->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_frame->setPosition(ccp(203/2,115/2));
		m_pUiLayer->addWidget(imageView_frame);

		l_name = UILabel::create();
        l_name->setStrokeEnabled(true);
		l_name->setText(lableGoods->goods().name().c_str());
		l_name->setAnchorPoint(ccp(0.5f,0.5f));
		l_name->setFontName(APP_FONT_NAME);
		l_name->setFontSize(18);
		l_name->setPosition(ccp(103,96));
		l_name->setColor(GameView::getInstance()->getGoodsColorByQuality(lableGoods->goods().quality()));
		m_pUiLayer->addWidget(l_name);

		//原价 
		l_oldPrice_value = UILabel::create();
		l_oldPrice_value->setStrokeEnabled(true);
		char s_oldPrice[20];
		sprintf(s_oldPrice,"%d",lableGoods->goods().price());
		l_oldPrice_value->setText(s_oldPrice);
		l_oldPrice_value->setAnchorPoint(ccp(0,0.5f));
		l_oldPrice_value->setFontName(APP_FONT_NAME);
		l_oldPrice_value->setFontSize(16);
		l_oldPrice_value->setPosition(ccp(144,53));
		m_pUiLayer->addWidget(l_oldPrice_value);
		l_oldPrice = UILabel::create();
		l_oldPrice->setStrokeEnabled(true);
		const char *str1 = StringDataManager::getString("goldStore_oldPrice");
		l_oldPrice->setText(str1);
		l_oldPrice->setAnchorPoint(ccp(0.5f,0.5f));
		l_oldPrice->setFontName(APP_FONT_NAME);
		l_oldPrice->setFontSize(16);
		l_oldPrice->setPosition(ccp(-41,0));
		l_oldPrice_value->addChild(l_oldPrice);
		imageView_oldPrice_goldIngold = UIImageView::create();
		imageView_oldPrice_goldIngold->setTexture("res_ui/ingot.png");
		imageView_oldPrice_goldIngold->setAnchorPoint(ccp(0,0.5f));
		imageView_oldPrice_goldIngold->setPosition(ccp(-25,0));
		l_oldPrice_value->addChild(imageView_oldPrice_goldIngold);
		//红线
		imageView_redLine = UIImageView::create();
		imageView_redLine->setTexture("res_ui/redgang.png");
		imageView_redLine->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_redLine->setPosition(ccp(130,53));
		imageView_redLine->setScaleX(1.6f);
		m_pUiLayer->addWidget(imageView_redLine);

		//现价
		l_newPrice_value = UILabel::create();
		l_newPrice_value->setStrokeEnabled(true);
		char s_newPrice[20];
		sprintf(s_newPrice,"%d",lableGoods->discount());
		l_newPrice_value->setText(s_newPrice);
		l_newPrice_value->setAnchorPoint(ccp(0,0.5f));
		l_newPrice_value->setFontName(APP_FONT_NAME);
		l_newPrice_value->setFontSize(16);
		l_newPrice_value->setPosition(ccp(144,69));
		m_pUiLayer->addWidget(l_newPrice_value);
		l_newPrice = UILabel::create();
		l_newPrice->setStrokeEnabled(true);
		const char *str2 = StringDataManager::getString("goldStore_newPrice");
		l_newPrice->setText(str2);
		l_newPrice->setAnchorPoint(ccp(0.5f,0.5f));
		l_newPrice->setFontName(APP_FONT_NAME);
		l_newPrice->setFontSize(16);
		l_newPrice->setPosition(ccp(-41,0));
		l_newPrice_value->addChild(l_newPrice);
		imageView_newPrice_goldIngold = UIImageView::create();
		imageView_newPrice_goldIngold->setTexture("res_ui/ingot.png");
		imageView_newPrice_goldIngold->setAnchorPoint(ccp(0,0.5f));
		imageView_newPrice_goldIngold->setPosition(ccp(-25,0));
		l_newPrice_value->addChild(imageView_newPrice_goldIngold);

		ImageView_hot = UIImageView::create();
		ImageView_hot->setTexture("res_ui/rmbshop/hot.png");
		ImageView_hot->setAnchorPoint(ccp(0.5f,0.5f));
		ImageView_hot->setPosition(ccp(192,100));
		m_pUiLayer->addWidget(ImageView_hot);
		ImageView_hot->setVisible(true);

		//价格
		l_price_value = UILabel::create();
		l_price_value->setStrokeEnabled(true);
		char s_nowPrice[20];
		sprintf(s_nowPrice,"%d",lableGoods->goods().price());
		l_price_value->setText(s_nowPrice);
		l_price_value->setAnchorPoint(ccp(0,0.5f));
		l_price_value->setFontName(APP_FONT_NAME);
		l_price_value->setFontSize(16);
		l_price_value->setPosition(ccp(144,61));
		m_pUiLayer->addWidget(l_price_value);
		l_price = UILabel::create();
		l_price->setStrokeEnabled(true);
		const char *str3 = StringDataManager::getString("goldStore_price");
		l_price->setText(str3);
		l_price->setAnchorPoint(ccp(0.5f,0.5f));
		l_price->setFontName(APP_FONT_NAME);
		l_price->setFontSize(16);
		l_price->setPosition(ccp(-41,0));
		l_price_value->addChild(l_price);
		imageView_price_goldIngold = UIImageView::create();
		imageView_price_goldIngold->setTexture("res_ui/ingot.png");
		imageView_price_goldIngold->setAnchorPoint(ccp(0,0.5f));
		imageView_price_goldIngold->setPosition(ccp(-25,0));
		l_price_value->addChild(imageView_price_goldIngold);

		if (lableGoods->isbind())  //绑定
		{
			imageView_oldPrice_goldIngold->setTexture("res_ui/ingotBind.png");
			imageView_newPrice_goldIngold->setTexture("res_ui/ingotBind.png");
			imageView_price_goldIngold->setTexture("res_ui/ingotBind.png");
		}
		else
		{
			imageView_oldPrice_goldIngold->setTexture("res_ui/ingot.png");
			imageView_newPrice_goldIngold->setTexture("res_ui/ingot.png");
			imageView_price_goldIngold->setTexture("res_ui/ingot.png");
		}

		if (lableGoods->ishot())
		{
			//原价 
			l_oldPrice_value->setVisible(true);
			l_oldPrice->setVisible(true);
			imageView_oldPrice_goldIngold->setVisible(true);
			//红线
			imageView_redLine->setVisible(true);

			//现价
			l_newPrice_value->setVisible(true);
			l_newPrice->setVisible(true);
			imageView_newPrice_goldIngold->setVisible(true);

			ImageView_hot->setVisible(true);


			l_price_value->setVisible(false);
			l_price->setVisible(false);
			imageView_price_goldIngold->setVisible(false);
		}
		else
		{
			//原价 
			l_oldPrice_value->setVisible(false);
			l_oldPrice->setVisible(false);
			imageView_oldPrice_goldIngold->setVisible(false);
			//红线
			imageView_redLine->setVisible(false);

			//现价
			l_newPrice_value->setVisible(false);
			l_newPrice->setVisible(false);
			imageView_newPrice_goldIngold->setVisible(false);

			ImageView_hot->setVisible(false);


			//价格
			l_price_value->setVisible(true);
			l_price->setVisible(true);
			imageView_price_goldIngold->setVisible(true);
		}

		/*********************判断装备的颜色***************************/
		std::string frameColorPath;
		if (lableGoods->goods().quality() == 1)
		{
			frameColorPath = EquipWhiteFramePath;
		}
		else if (lableGoods->goods().quality() == 2)
		{
			frameColorPath = EquipGreenFramePath;
		}
		else if (lableGoods->goods().quality() == 3)
		{
			frameColorPath = EquipBlueFramePath;
		}
		else if (lableGoods->goods().quality() == 4)
		{
			frameColorPath = EquipPurpleFramePath;
		}
		else if (lableGoods->goods().quality() == 5)
		{
			frameColorPath = EquipOrangeFramePath;
		}
		else
		{
			frameColorPath = EquipVoidFramePath;
		}

		Btn_goodsItemFrame = UIButton::create();
		Btn_goodsItemFrame->setTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
		Btn_goodsItemFrame->setTouchEnable(true);
		Btn_goodsItemFrame->setAnchorPoint(ccp(.5f,.5f));
		Btn_goodsItemFrame->setPosition(ccp(17+Btn_goodsItemFrame->getContentSize().width/2,17+Btn_goodsItemFrame->getContentSize().height/2));
		Btn_goodsItemFrame->setScale(0.9f);
		Btn_goodsItemFrame->addReleaseEvent(this,coco_releaseselector(GoldStoreCellItem::GoodItemEvent));
		Btn_goodsItemFrame->setPressedActionEnabled(true);
		m_pUiLayer->addWidget(Btn_goodsItemFrame);

		uiiImageView_goodsItem = UIImageView::create();
		if (strcmp(lableGoods->goods().icon().c_str(),"") == 0)
		{
			uiiImageView_goodsItem->setTexture("res_ui/props_icon/prop.png");
		}
		else
		{
			std::string _path = "res_ui/props_icon/";
			_path.append(lableGoods->goods().icon().c_str());
			_path.append(".png");
			uiiImageView_goodsItem->setTexture(_path.c_str());
		}
		uiiImageView_goodsItem->setAnchorPoint(ccp(0.5f,0.5f));
		uiiImageView_goodsItem->setPosition(ccp(0,0));
		Btn_goodsItemFrame->addChild(uiiImageView_goodsItem);

		//购买按钮
		UIButton * btn_buy  = UIButton::create();
		btn_buy->setTextures("res_ui/new_button_6.png","res_ui/new_button_6.png","");
		btn_buy->setTouchEnable(true);
		btn_buy->setPressedActionEnabled(true);
		//btn_buy->setScale9Enable(true);
		//btn_buy->setScale9Size(CCSizeMake(80,40));
		//btn_buy->setCapInsets(CCRectMake(18,9,2,23));
		btn_buy->setAnchorPoint(ccp(0.5f,0.5f));
		btn_buy->setPosition(ccp(131,25));
		btn_buy->addReleaseEvent(this,coco_releaseselector(GoldStoreCellItem::BuyEvent));
		m_pUiLayer->addWidget(btn_buy);

		UILabel * l_buy = UILabel::create();    
		const char *str4 = StringDataManager::getString("goods_auction_buy");
		//const char *str4 = "123";
		l_buy->setText(str4);
		l_buy->setAnchorPoint(ccp(0.5f,0.5f));
		l_buy->setFontName(APP_FONT_NAME);
		l_buy->setFontSize(20);
		l_buy->setPosition(ccp(0,0));
		btn_buy->addChild(l_buy);

		this->setContentSize(CCSizeMake(203,115));

		return true;
	}
	return false;
}

void GoldStoreCellItem::BuyEvent( CCObject *pSender )
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoldStoreItemBuyInfo) == NULL)
	{
		CCSize size= CCDirector::sharedDirector()->getVisibleSize();
		GoldStoreItemBuyInfo * goldStoreItemBuyInfo = GoldStoreItemBuyInfo::create(curLableGoods);
		goldStoreItemBuyInfo->ignoreAnchorPointForPosition(false);
		goldStoreItemBuyInfo->setAnchorPoint(ccp(0.5f,0.5f));
		goldStoreItemBuyInfo->setPosition(ccp(size.width/2,size.height/2));
		goldStoreItemBuyInfo->setTag(kTagGoldStoreItemBuyInfo);
		GameView::getInstance()->getMainUIScene()->addChild(goldStoreItemBuyInfo);
	}
}

void GoldStoreCellItem::GoodItemEvent( CCObject *pSender )
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoodsItemInfoBase) == NULL)
	{
		CCSize size= CCDirector::sharedDirector()->getVisibleSize();
		GoodsInfo * curGoods = new GoodsInfo();
		curGoods->CopyFrom(curLableGoods->goods());
		GoodsItemInfoBase * goodsItemInfoBase = GoodsItemInfoBase::create(curGoods,GameView::getInstance()->EquipListItem,0);
		goodsItemInfoBase->ignoreAnchorPointForPosition(false);
		goodsItemInfoBase->setAnchorPoint(ccp(0.5f,0.5f));
		//goodsItemInfoBase->setPosition(ccp(size.width/2,size.height/2));
		goodsItemInfoBase->setTag(kTagGoodsItemInfoBase);
		GameView::getInstance()->getMainUIScene()->addChild(goodsItemInfoBase);
		delete curGoods;
	}
}

void GoldStoreCellItem::refreshUI( CLableGoods * lableGoods )
{
	curLableGoods->CopyFrom(*lableGoods);

	l_name->setText(lableGoods->goods().name().c_str());
	l_name->setColor(GameView::getInstance()->getGoodsColorByQuality(lableGoods->goods().quality()));

	if (lableGoods->isbind())  //绑定
	{
		imageView_oldPrice_goldIngold->setTexture("res_ui/ingotBind.png");
		imageView_newPrice_goldIngold->setTexture("res_ui/ingotBind.png");
		imageView_price_goldIngold->setTexture("res_ui/ingotBind.png");
	}
	else
	{
		imageView_oldPrice_goldIngold->setTexture("res_ui/ingot.png");
		imageView_newPrice_goldIngold->setTexture("res_ui/ingot.png");
		imageView_price_goldIngold->setTexture("res_ui/ingot.png");
	}

	if (lableGoods->ishot())
	{
		//原价 
		char s_oldPrice[20];
		sprintf(s_oldPrice,"%d",lableGoods->goods().price());
		l_oldPrice_value->setText(s_oldPrice);
		const char *str1 = StringDataManager::getString("goldStore_oldPrice");
		l_oldPrice->setText(str1);

		//现价
		char s_newPrice[20];
		sprintf(s_newPrice,"%d",lableGoods->discount());
		l_newPrice_value->setText(s_newPrice);
		const char *str2 = StringDataManager::getString("goldStore_newPrice");
		l_newPrice->setText(str2);

		//原价 
		l_oldPrice_value->setVisible(true);
		l_oldPrice->setVisible(true);
		imageView_oldPrice_goldIngold->setVisible(true);
		//红线
		imageView_redLine->setVisible(true);

		//现价
		l_newPrice_value->setVisible(true);
		l_newPrice->setVisible(true);
		imageView_newPrice_goldIngold->setVisible(true);

		ImageView_hot->setVisible(true);


		l_price_value->setVisible(false);
		l_price->setVisible(false);
		imageView_price_goldIngold->setVisible(false);
	}
	else
	{
		//价格
		char s_nowPrice[20];
		sprintf(s_nowPrice,"%d",lableGoods->goods().price());
		l_price_value->setText(s_nowPrice);
		const char *str3 = StringDataManager::getString("goldStore_price");
		l_price->setText(str3);

		//原价 
		l_oldPrice_value->setVisible(false);
		l_oldPrice->setVisible(false);
		imageView_oldPrice_goldIngold->setVisible(false);
		//红线
		imageView_redLine->setVisible(false);

		//现价
		l_newPrice_value->setVisible(false);
		l_newPrice->setVisible(false);
		imageView_newPrice_goldIngold->setVisible(false);

		ImageView_hot->setVisible(false);


		//价格
		l_price_value->setVisible(true);
		l_price->setVisible(true);
		imageView_price_goldIngold->setVisible(true);
	}

	/*********************判断装备的颜色***************************/
	std::string frameColorPath;
	if (lableGoods->goods().quality() == 1)
	{
		frameColorPath = EquipWhiteFramePath;
	}
	else if (lableGoods->goods().quality() == 2)
	{
		frameColorPath = EquipGreenFramePath;
	}
	else if (lableGoods->goods().quality() == 3)
	{
		frameColorPath = EquipBlueFramePath;
	}
	else if (lableGoods->goods().quality() == 4)
	{
		frameColorPath = EquipPurpleFramePath;
	}
	else if (lableGoods->goods().quality() == 5)
	{
		frameColorPath = EquipOrangeFramePath;
	}
	else
	{
		frameColorPath = EquipVoidFramePath;
	}

	Btn_goodsItemFrame->setTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");

	if (strcmp(lableGoods->goods().icon().c_str(),"") == 0)
	{
		uiiImageView_goodsItem->setTexture("res_ui/props_icon/prop.png");
	}
	else
	{
		std::string _path = "res_ui/props_icon/";
		_path.append(lableGoods->goods().icon().c_str());
		_path.append(".png");
		uiiImageView_goodsItem->setTexture(_path.c_str());
	}
}
