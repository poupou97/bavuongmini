#include "GoodsItem.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "GoldStoreItemInfo.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/backpackscene/EquipmentItem.h"


GoodsItem::GoodsItem()
{
}


GoodsItem::~GoodsItem()
{
	delete curGoodsInfo;
}

GoodsItem * GoodsItem::create( GoodsInfo* goods )
{
	GoodsItem *widget = new GoodsItem();
	if (widget && widget->initWithGoods(goods))
	{
		widget->autorelease();
		return widget;
	}
	CC_SAFE_DELETE(widget);
	return NULL;
}

bool GoodsItem::initWithGoods( GoodsInfo* goods )
{
	if (UIWidget::init())
	{
		curGoodsInfo = new GoodsInfo();
		curGoodsInfo->CopyFrom(*goods);
		/*********************判断装备的颜色***************************/
		/*********************判断装备的颜色***************************/
		std::string frameColorPath;
		if (curGoodsInfo->quality() == 1)
		{
			frameColorPath = EquipWhiteFramePath;
		}
		else if (curGoodsInfo->quality() == 2)
		{
			frameColorPath = EquipGreenFramePath;
		}
		else if (curGoodsInfo->quality() == 3)
		{
			frameColorPath = EquipBlueFramePath;
		}
		else if (curGoodsInfo->quality() == 4)
		{
			frameColorPath = EquipPurpleFramePath;
		}
		else if (curGoodsInfo->quality() == 5)
		{
			frameColorPath = EquipOrangeFramePath;
		}
		else
		{
			frameColorPath = EquipVoidFramePath;
		}

// 		UIImageView * cloud = UIImageView::create();
// 		cloud->setTexture(cloudColorPaht.c_str());
// 		cloud->setAnchorPoint(ccp(0,0));
// 		cloud->setPosition(ccp(0,0));
// 		cloud->setScale9Enable(true);
// 		cloud->setScale9Size(CCSizeMake(56,56));
// 		cloud->setName("res_ui/imageView_cloud");
// 		this->addChild(cloud);

		UIButton *Btn_goodsItemFrame = UIButton::create();
		Btn_goodsItemFrame->setTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
		Btn_goodsItemFrame->setTouchEnable(true);
		Btn_goodsItemFrame->setAnchorPoint(ccp(0,0));
		Btn_goodsItemFrame->setPosition(ccp(0,0));
		Btn_goodsItemFrame->setScale(0.9f);
		Btn_goodsItemFrame->addReleaseEvent(this,coco_releaseselector(GoodsItem::GoodItemEvent));
		this->addChild(Btn_goodsItemFrame);

		UIImageView * uiiImageView_goodsItem = UIImageView::create();
		if (strcmp(goods->icon().c_str(),"") == 0)
		{
			uiiImageView_goodsItem->setTexture("res_ui/props_icon/prop.png");
		}
		else
		{
			std::string _path = "res_ui/props_icon/";
			_path.append(goods->icon().c_str());
			_path.append(".png");
			uiiImageView_goodsItem->setTexture(_path.c_str());
		}
		uiiImageView_goodsItem->setAnchorPoint(ccp(0.5f,0.5f));
		uiiImageView_goodsItem->setPosition(ccp(28,28));
		this->addChild(uiiImageView_goodsItem);

		this->setTouchEnable(true);
		this->setSize(CCSizeMake(56,56));

		return true;
	}
	return false;
}

void GoodsItem::GoodItemEvent( CCObject *pSender )
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoldStoreItemInfo) == NULL)
	{
		CCSize size= CCDirector::sharedDirector()->getVisibleSize();
		GoldStoreItemInfo * goldStoreItemInfo = GoldStoreItemInfo::create(curGoodsInfo);
		goldStoreItemInfo->ignoreAnchorPointForPosition(false);
		goldStoreItemInfo->setAnchorPoint(ccp(0.5f,0.5f));
		//goldStoreItemInfo->setPosition(ccp(size.width/2,size.height/2));
		goldStoreItemInfo->setTag(kTagGoldStoreItemInfo);
		GameView::getInstance()->getMainUIScene()->addChild(goldStoreItemInfo);
	}
}
