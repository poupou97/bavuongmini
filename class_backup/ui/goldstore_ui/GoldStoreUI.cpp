#include "GoldStoreUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../GameView.h"
#include "../extensions/UITab.h"
#include "../../messageclient/element/CLable.h"
#include "GoldStoreCellItem.h"
#include "../../messageclient/element/CLableGoods.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "GoldStoreMarquee.h"
#include "../../utils/StrUtils.h"
#include "../vip_ui/VipDetailUI.h"
#include "../vip_ui/VipEveryDayRewardsUI.h"
#include "../vip_ui/FirstBuyVipUI.h"
#include "../vip_ui/VipData.h"
#include "../recharge_ui/Recharge.h"
#include "../../utils/GameConfig.h"

#define MARQUEE_LABLE_WIDTH 150
#define MARQUEE_LABEL_HEIGHT 72
#define GOLD_STORE_MAX_FILTER 6

#define kTag_CellItem_Left 236
#define kTag_CellItem_Right 237

#define kTag_MARQUEE 300

GoldStoreUI::GoldStoreUI():
curLableId(0),
m_strMarquee("")
{
}


GoldStoreUI::~GoldStoreUI()
{
	std::vector<GoodsInfo *>::iterator iterGoodsInfo;
	for (iterGoodsInfo = m_vector_goodsInfo.begin(); iterGoodsInfo != m_vector_goodsInfo.end(); iterGoodsInfo++)
	{
		delete *iterGoodsInfo;
	}
	m_vector_goodsInfo.clear();
}

GoldStoreUI* GoldStoreUI::create()
{
	GoldStoreUI * goldStoreUI = new GoldStoreUI();
	if (goldStoreUI && goldStoreUI->init())
	{
		goldStoreUI->autorelease();
		return goldStoreUI;
	}
	CC_SAFE_DELETE(goldStoreUI);
	return goldStoreUI;
}

bool GoldStoreUI::init()
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		this->scheduleUpdate();

		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winsize);
		mengban->setAnchorPoint(CCPointZero);
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);

		u_layer = UILayer::create();
		u_layer->ignoreAnchorPointForPosition(false);
		u_layer->setAnchorPoint(ccp(0.5f,0.5f));
		u_layer->setContentSize(CCSizeMake(800, 480));
		u_layer->setPosition(CCPointZero);
		u_layer->setPosition(ccp(winsize.width/2,winsize.height/2));
		addChild(u_layer);

		//加载UI
		if(LoadSceneLayer::GoldStoreLayer->getWidgetParent() != NULL)
		{
			LoadSceneLayer::GoldStoreLayer->removeFromParentAndCleanup(false);
		}
		ppanel = LoadSceneLayer::GoldStoreLayer;
		ppanel->getValidNode()->setContentSize(CCSizeMake(800, 480));
		ppanel->setAnchorPoint(ccp(0.5f,0.5f));
		ppanel->setPosition(CCPointZero);
		ppanel->setTouchEnable(true);
		ppanel->setPosition(ccp(winsize.width/2,winsize.height/2));
		m_pUiLayer->addWidget(ppanel);

		const char * firstStr = StringDataManager::getString("UIName_yuan");
		const char * secondStr = StringDataManager::getString("UIName_bao");
		const char * thirdStr = StringDataManager::getString("UIName_shang");
		const char * fourthStr = StringDataManager::getString("UIName_cheng");
		CCArmature * atmature = MainScene::createPendantAnm(firstStr,secondStr,thirdStr,fourthStr);
		atmature->setPosition(ccp(30,240));
		u_layer->addChild(atmature);

		//关闭按钮
		UIButton * Button_close = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnable(true);
		Button_close->addReleaseEvent(this, coco_releaseselector(GoldStoreUI::CloseEvent));
		Button_close->setPressedActionEnabled(true);

		Button_recharge = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_recharge");
		Button_recharge->setTouchEnable(true);
		Button_recharge->addReleaseEvent(this, coco_releaseselector(GoldStoreUI::RechargeEvent));
		Button_recharge->setPressedActionEnabled(true);

		Button_vip = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_vip");
		Button_vip->setTouchEnable(true);
		Button_vip->addReleaseEvent(this, coco_releaseselector(GoldStoreUI::VipEvent));
		Button_vip->setPressedActionEnabled(true);

		// vip开关控制
		bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
		if (vipEnabled)
		{
			Button_vip->setVisible(true);
		}
		else
		{
			Button_vip->setVisible(false);
		}

		ImageView_inGotPart = (UIImageView*)UIHelper::seekWidgetByName(ppanel,"ImageView_lv6di");
		ImageView_inGotPart->setVisible(true);
		ImageView_bindInGotPart = (UIImageView*)UIHelper::seekWidgetByName(ppanel,"ImageView_lv6di_1");
		ImageView_bindInGotPart->setVisible(false);

		l_ingot_value = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_IngotValue");
		char str_ingot[20];
		sprintf(str_ingot,"%d",GameView::getInstance()->getPlayerGoldIngot());
		l_ingot_value->setText(str_ingot);

		l_bindIngot_value = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_BindIngotValue");
		char str_bindingot[20];
		sprintf(str_bindingot,"%d",GameView::getInstance()->getPlayerBindGoldIngot());
		l_bindIngot_value->setText(str_bindingot);

		// 小喇叭
		m_imageView_horn = (UIImageView *)UIHelper::seekWidgetByName(ppanel, "ImageView_horn");
		m_imageView_horn->setVisible(true);

		const char *function_type_str1 = StringDataManager::getString("goldStore_tab_type_1");
		char* function_type_p1 =const_cast<char*>(function_type_str1);
		const char *function_type_str2 = StringDataManager::getString("goldStore_tab_type_2");
		char* function_type_p2 =const_cast<char*>(function_type_str2);
		char * mainNames[] ={function_type_p2,function_type_p1};
		tab_main  = UITab::createWithText(2,"res_ui/tab_3_off.png","res_ui/tab_3_on.png","",mainNames,HORIZONTAL,5);
		tab_main->setAnchorPoint(ccp(0,0));
		tab_main->setPosition(ccp(66,408));
		tab_main->setHighLightImage("res_ui/tab_3_on.png");
		tab_main->setDefaultPanelByIndex(0);
		tab_main->addIndexChangedEvent(this,coco_indexchangedselector(GoldStoreUI::FunctionTypeIndexChangedEvent));
		tab_main->setPressedActionEnabled(true);
		u_layer->addWidget(tab_main);

// 		int _num = GameView::getInstance()->goldStoreList.size();
// 		const char * normalImage = "res_ui/tab_s_off.png";
// 		const char * selectImage = "res_ui/tab_s.png";
// 		const char * finalImage = "";
// 		const char * highLightImage = "res_ui/tab_s.png";
// 		CCAssert(_num <= GOLD_STORE_MAX_FILTER, "out of range");
// 		char * ItemName[GOLD_STORE_MAX_FILTER];
// 		for (int i =0;i<_num;++i)
// 		{
// 			std::string imagePath = "res_ui/rmbshop/";
// 			imagePath.append(GameView::getInstance()->goldStoreList.at(i)->name().c_str());
// 
// 			ItemName[i] = new char [50];
// 			strcpy(ItemName[i],imagePath.c_str());
// 		}
// 		if (_num > 0)
// 		{
// 			tab_function_type  = UITab::createWithImage(_num,normalImage,selectImage,finalImage,ItemName,VERTICAL,-10);
// 			tab_function_type->setAnchorPoint(ccp(0,0));
// 			tab_function_type->setPosition(ccp(760,420));
// 			tab_function_type->setHighLightImage((char * )highLightImage);
// 			tab_function_type->setDefaultPanelByIndex(0);
// 			tab_function_type->addIndexChangedEvent(this,coco_indexchangedselector(GoldStoreUI::IndexChangedEvent));
// 			tab_function_type->setPressedActionEnabled(true);
// 			u_layer->addWidget(tab_function_type);
// 		}
// 		
// 		for (int i = 0;i<_num;i++)
// 		{
// 			delete ItemName[i];
// 		}

		m_tableView = CCTableView::create(this,CCSizeMake(449,317));
		m_tableView->setDirection(kCCScrollViewDirectionVertical);
		m_tableView->setAnchorPoint(ccp(0,0));
		m_tableView->setPosition(ccp(284,81));
		m_tableView->setDelegate(this);
		m_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		u_layer->addChild(m_tableView);

		refreshTypeTabByIndex(0);

		//this->scheduleUpdate();

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(winsize);

		return true;
	}
	return false;
}

void GoldStoreUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();

	//add new data
//	if (tab_main->getCurrentIndex() == 0)
//	{
// 		if (GameView::getInstance()->goldStoreList.size() > 0)
// 		{
// 			for (int i = 0;i<GameView::getInstance()->goldStoreList.at(0)->commdity_size();++i)
// 			{
// 				CLableGoods * _LableGoods = new CLableGoods();
// 				_LableGoods->CopyFrom(GameView::getInstance()->goldStoreList.at(0)->commdity(i));
// 				DataSource.push_back(_LableGoods);
// 			}
// 		}
//	}
//	else
//	{
// 		if (GameView::getInstance()->bingGoldStoreList.size() > 0)
// 		{
// 			for (int i = 0;i<GameView::getInstance()->bingGoldStoreList.at(0)->commdity_size();++i)
// 			{
// 				CLableGoods * _LableGoods = new CLableGoods();
// 				_LableGoods->CopyFrom(GameView::getInstance()->bingGoldStoreList.at(0)->commdity(i));
// 				DataSource.push_back(_LableGoods);
// 			}
// 		}
//	}

//	m_tableView->reloadData();
}

void GoldStoreUI::onExit()
{
	UIScene::onExit();
}

bool GoldStoreUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return true;
}

void GoldStoreUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void GoldStoreUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void GoldStoreUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void GoldStoreUI::CloseEvent( CCObject *pSender )
{
	this->closeAnim();
}

void GoldStoreUI::RechargeEvent( CCObject *pSender )
{
	// 充值控制开关
	bool bChargeEnabled = GameConfig::getBoolForKey("charge_enable");
	if (true == bChargeEnabled)
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
		CCLayer *rechargeScene = (CCLayer*)mainscene->getChildByTag(kTagRechargeUI);
		if(rechargeScene == NULL)
		{
			RechargeUI * rechargeUI = RechargeUI::create();
			mainscene->addChild(rechargeUI,0,kTagRechargeUI);
			rechargeUI->ignoreAnchorPointForPosition(false);
			rechargeUI->setAnchorPoint(ccp(0.5f,0.5f));
			rechargeUI->setPosition(ccp(winSize.width/2, winSize.height/2));
		}

		//GameView::getInstance()->showAlertDialog(StringDataManager::getString("skillui_nothavefuction"));
	}
	else
	{
		const char * charChargeEnabled = StringDataManager::getString("RechargeUI_disable");
		GameView::getInstance()->showAlertDialog(charChargeEnabled);
	}
}

void GoldStoreUI::VipEvent( CCObject *pSender )
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	CCLayer *vipScene = (CCLayer*)mainscene->getChildByTag(kTagVipDetailUI);
	if(vipScene == NULL)
	{
		if (VipData::vipDetailUI)
		{
			VipDetailUI * vipDetailUI = (VipDetailUI*)VipData::vipDetailUI;
			mainscene->addChild(vipDetailUI,0,kTagVipDetailUI);
			vipDetailUI->ignoreAnchorPointForPosition(false);
			vipDetailUI->setAnchorPoint(ccp(0.5f,0.5f));
			vipDetailUI->setPosition(ccp(winSize.width/2, winSize.height/2));
			vipDetailUI->setToDefault();
		}
	}
}

void GoldStoreUI::FunctionTypeIndexChangedEvent(CCObject* pSender)
{
	if (tab_main->getCurrentIndex() == 0)
	{
		refreshTypeTabByIndex(0);
	}
	else
	{
		refreshTypeTabByIndex(1);
	}
}

void GoldStoreUI::IndexChangedEvent( CCObject* pSender )
{
	tab_function_type->setHightLightLabelColor(ccc3(47,93,13));
	tab_function_type->setNormalLabelColor(ccc3(255,255,255));

	//delete old data
	DataSource.erase(DataSource.begin(),DataSource.end());
	std::vector<CLableGoods*>::iterator iter;
	for (iter = DataSource.begin(); iter != DataSource.end(); ++iter)
	{
		delete *iter;
	}
	DataSource.clear();

	if (tab_main->getCurrentIndex() == 1)
	{
		int idx = tab_function_type->getCurrentIndex();
		curLableId = GameView::getInstance()->bingGoldStoreList.at(idx)->id();

		//add new data
		for (int i = 0;i<GameView::getInstance()->bingGoldStoreList.at(idx)->commdity_size();++i)
		{
			CLableGoods * _LableGoods = new CLableGoods();
			_LableGoods->CopyFrom(GameView::getInstance()->bingGoldStoreList.at(idx)->commdity(i));
			DataSource.push_back(_LableGoods);
		}
	}
	else
	{
		int idx = tab_function_type->getCurrentIndex();
		curLableId = GameView::getInstance()->goldStoreList.at(idx)->id();

		//add new data
		for (int i = 0;i<GameView::getInstance()->goldStoreList.at(idx)->commdity_size();++i)
		{
			CLableGoods * _LableGoods = new CLableGoods();
			_LableGoods->CopyFrom(GameView::getInstance()->goldStoreList.at(idx)->commdity(i));
			DataSource.push_back(_LableGoods);
		}
	}

	this->m_tableView->reloadData();
}

void GoldStoreUI::scrollViewDidScroll( CCScrollView* view )
{

}

void GoldStoreUI::scrollViewDidZoom( CCScrollView* view )
{

}

void GoldStoreUI::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{
}

cocos2d::CCSize GoldStoreUI::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeMake(444,120);
}

cocos2d::extension::CCTableViewCell* GoldStoreUI::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called
	if (!cell)
	{
		cell = new CCTableViewCell();
		cell->autorelease();

		if (idx == DataSource.size()/2)
		{
			if (DataSource.size()%2 == 0)
			{
				GoldStoreCellItem * cellItem1 = GoldStoreCellItem::create(DataSource.at(2*idx));
				cellItem1->setPosition(ccp(15,0));
				cellItem1->setTag(kTag_CellItem_Left);
				cell->addChild(cellItem1);

				GoldStoreCellItem * cellItem2 = GoldStoreCellItem::create(DataSource.at(2*idx+1));
				cellItem2->setPosition(ccp(226,0));
				cellItem2->setTag(kTag_CellItem_Right);
				cell->addChild(cellItem2);
			}
			else
			{
				GoldStoreCellItem * cellItem1 = GoldStoreCellItem::create(DataSource.at(2*idx));
				cellItem1->setPosition(ccp(15,0));
				cellItem1->setTag(kTag_CellItem_Left);
				cell->addChild(cellItem1);
			}
		}
		else
		{
			GoldStoreCellItem * cellItem1 = GoldStoreCellItem::create(DataSource.at(2*idx));
			cellItem1->setPosition(ccp(15,0));
			cellItem1->setTag(kTag_CellItem_Left);
			cell->addChild(cellItem1);

			GoldStoreCellItem * cellItem2 = GoldStoreCellItem::create(DataSource.at(2*idx+1));
			cellItem2->setPosition(ccp(226,0));
			cellItem2->setTag(kTag_CellItem_Right);
			cell->addChild(cellItem2);
		}
	}
	else
	{
		GoldStoreCellItem * cellItem1 = dynamic_cast<GoldStoreCellItem*>(cell->getChildByTag(kTag_CellItem_Left));
		GoldStoreCellItem * cellItem2 = dynamic_cast<GoldStoreCellItem*>(cell->getChildByTag(kTag_CellItem_Right));
		if (idx == DataSource.size()/2)
		{
			if (DataSource.size()%2 == 0)
			{
				if (cellItem1)
				{
					cellItem1->refreshUI(DataSource.at(2*idx));
				}
				else
				{
					GoldStoreCellItem * cellItem1 = GoldStoreCellItem::create(DataSource.at(2*idx));
					cellItem1->setPosition(ccp(15,0));
					cellItem1->setTag(kTag_CellItem_Left);
					cell->addChild(cellItem1);
				}

				if (cellItem2)
				{
					cellItem2->refreshUI(DataSource.at(2*idx+1));
				}
				else
				{
					GoldStoreCellItem * cellItem2 = GoldStoreCellItem::create(DataSource.at(2*idx+1));
					cellItem2->setPosition(ccp(226,0));
					cellItem2->setTag(kTag_CellItem_Right);
					cell->addChild(cellItem2);
				}
			}
			else
			{
				if (cellItem1)
				{
					cellItem1->refreshUI(DataSource.at(2*idx));
				}
				else
				{
					GoldStoreCellItem * cellItem1 = GoldStoreCellItem::create(DataSource.at(2*idx));
					cellItem1->setPosition(ccp(15,0));
					cellItem1->setTag(kTag_CellItem_Left);
					cell->addChild(cellItem1);
				}

				if (cellItem2)
				{
					cellItem2->removeFromParent();
				}
			}
		}
		else
		{
			if (cellItem1)
			{
				cellItem1->refreshUI(DataSource.at(2*idx));
			}
			else
			{
				GoldStoreCellItem * cellItem1 = GoldStoreCellItem::create(DataSource.at(2*idx));
				cellItem1->setPosition(ccp(15,0));
				cellItem1->setTag(kTag_CellItem_Left);
				cell->addChild(cellItem1);
			}

			if (cellItem2)
			{
				cellItem2->refreshUI(DataSource.at(2*idx+1));
			}
			else
			{
				GoldStoreCellItem * cellItem2 = GoldStoreCellItem::create(DataSource.at(2*idx+1));
				cellItem2->setPosition(ccp(226,0));
				cellItem2->setTag(kTag_CellItem_Right);
				cell->addChild(cellItem2);
			}
		}
	}

	return cell;
}

unsigned int GoldStoreUI::numberOfCellsInTableView( CCTableView *table )
{
	if (DataSource.size()%2 == 0)
	{
		return DataSource.size()/2;
	}
	else
	{
		return DataSource.size()/2 + 1;
	}
}

void GoldStoreUI::update(float dt)
{
	char str_ingot[20];
	sprintf(str_ingot,"%d",GameView::getInstance()->getPlayerGoldIngot());
	l_ingot_value->setText(str_ingot);

	char str_bindingot[20];
	sprintf(str_bindingot,"%d",GameView::getInstance()->getPlayerBindGoldIngot());
	l_bindIngot_value->setText(str_bindingot);
}

void GoldStoreUI::initMarquee()
{
	// clear vector
	std::vector<GoodsInfo*>::iterator iter;
	for (iter = m_vector_goodsInfo.begin(); iter != m_vector_goodsInfo.end(); ++iter)
	{
		delete *iter;
	}
	m_vector_goodsInfo.clear();


	// 获取打折物品列表
	int nDataSourceSize = DataSource.size();
	for (int i = 0; i < nDataSourceSize; i++)
	{
		bool bIsHot = DataSource.at(i)->ishot();
		if (true == bIsHot)
		{
			GoodsInfo * goodsInfo = new GoodsInfo();
			goodsInfo->CopyFrom(DataSource.at(i)->goods());

			m_vector_goodsInfo.push_back(goodsInfo);
		}
	}

	// 判断是否有打折物品，如果没有打折物品，则不显示跑马灯
	if (0 == m_vector_goodsInfo.size())
	{
		return;
	}

	//GameView::getInstance()->getGoodsColorByQuality(lableGoods->goods().quality())

	// 加入到要显示的字符串中
	const char * str_tip = StringDataManager::getString("goldStore_buy_marquee");
	const char * str_tip_left = StringDataManager::getString("goldStore_buy_goodsInfoShowBegin");
	const char * str_tip_right = StringDataManager::getString("goldStore_buy_goodsInfoShowEnd");
	const char * str_tip_seperateSignal = StringDataManager::getString("goldStore_buy_dunhao");
	const char * str_tip_fullstopSignal = StringDataManager::getString("goldStore_buy_junhao");

	m_strMarquee = "";
	m_strMarquee.append(str_tip);

	int nVectorSize = m_vector_goodsInfo.size();
	for (int i = 0; i < nVectorSize; i++)
	{
		std::string strGoodsName = m_vector_goodsInfo.at(i)->name();
		ccColor3B colorFont = GameView::getInstance()->getGoodsColorByQuality(m_vector_goodsInfo.at(i)->quality());
		
		std::string strGoodsNameWithColor = StrUtils::applyColor(strGoodsName.c_str(), colorFont);
		std::string strTipLeft = StrUtils::applyColor(str_tip_left, colorFont);
		std::string strTipRight = StrUtils::applyColor(str_tip_right, colorFont);

		m_strMarquee.append(strTipLeft.c_str());
		m_strMarquee.append(strGoodsNameWithColor.c_str());
		m_strMarquee.append(strTipRight.c_str());

		if (i == nVectorSize - 1)
		{
			m_strMarquee.append(str_tip_fullstopSignal);
		}
		else 
		{
			m_strMarquee.append(str_tip_seperateSignal);
		}
	}

	int nPositionX = m_imageView_horn->getPosition().x + m_imageView_horn->getContentSize().width - 43;
	int nPositionY = m_imageView_horn->getPosition().y + m_imageView_horn->getContentSize().height - 7;

	// 跑马灯
	GoldStoreMarquee * pMarqueeLabel = GoldStoreMarquee::create(nPositionX, nPositionY, MARQUEE_LABLE_WIDTH,  MARQUEE_LABEL_HEIGHT);
	pMarqueeLabel->initLabelStr(m_strMarquee.c_str(), 14);
	pMarqueeLabel->setTag(kTag_MARQUEE);
	u_layer->addChild(pMarqueeLabel);

}

void GoldStoreUI::refreshTypeTabByIndex( int idx )
{
	//refresh btn type , ingotValue
	if (idx == 0)
	{
		Button_recharge->setVisible(true);
		Button_vip->setVisible(true);
		ImageView_inGotPart->setVisible(true);
		ImageView_bindInGotPart->setVisible(false);
	}
	else
	{
		Button_recharge->setVisible(false);
		Button_vip->setVisible(true);
		ImageView_inGotPart->setVisible(false);
		ImageView_bindInGotPart->setVisible(true);
	}

	if (u_layer->getWidgetByName("TypeTab"))
	{
		u_layer->getWidgetByName("TypeTab")->removeFromParent();
	}

	const char * normalImage = "res_ui/tab_s_off.png";
	const char * selectImage = "res_ui/tab_s.png";
	const char * finalImage = "";
	const char * highLightImage = "res_ui/tab_s.png";
	int _num = 0;
	if (idx == 0)   //元宝商城
	{
		_num = GameView::getInstance()->goldStoreList.size();
		CCAssert(_num <= GOLD_STORE_MAX_FILTER, "out of range");
		char * ItemName[GOLD_STORE_MAX_FILTER];
		for (int i =0;i<_num;++i)
		{
			//std::string imagePath = "res_ui/rmbshop/";
			std::string imagePath = "";
			imagePath.append(GameView::getInstance()->goldStoreList.at(i)->name().c_str());

			ItemName[i] = new char [50];
			strcpy(ItemName[i],imagePath.c_str());
		}

		if (_num > 0)
		{
			tab_function_type  = UITab::createWithText(_num,normalImage,selectImage,finalImage,ItemName,VERTICAL,-10,18);
			tab_function_type->setAnchorPoint(ccp(0,0));
			tab_function_type->setPosition(ccp(760,420));
			tab_function_type->setHighLightImage((char * )highLightImage);
			tab_function_type->setHightLightLabelColor(ccc3(47,93,13));
			tab_function_type->setNormalLabelColor(ccc3(255,255,255));
			tab_function_type->setDefaultPanelByIndex(0);
			tab_function_type->addIndexChangedEvent(this,coco_indexchangedselector(GoldStoreUI::IndexChangedEvent));
			tab_function_type->setPressedActionEnabled(true);
			tab_function_type->setName("TypeTab");
			u_layer->addWidget(tab_function_type);
		}

		for (int i = 0;i<_num;i++)
		{
			delete ItemName[i];
		}
	}
	else
	{
		_num = GameView::getInstance()->bingGoldStoreList.size();
		CCAssert(_num <= GOLD_STORE_MAX_FILTER, "out of range");
		char * ItemName[GOLD_STORE_MAX_FILTER];
		for (int i =0;i<_num;++i)
		{
			//std::string imagePath = "res_ui/rmbshop/";
			std::string imagePath = "";
			imagePath.append(GameView::getInstance()->bingGoldStoreList.at(i)->name().c_str());

			ItemName[i] = new char [50];
			strcpy(ItemName[i],imagePath.c_str());
		}

		if (_num > 0)
		{
			tab_function_type  = UITab::createWithText(_num,normalImage,selectImage,finalImage,ItemName,VERTICAL,-10,18);
			tab_function_type->setAnchorPoint(ccp(0,0));
			tab_function_type->setPosition(ccp(760,420));
			tab_function_type->setHighLightImage((char * )highLightImage);
			tab_function_type->setHightLightLabelColor(ccc3(47,93,13));
			tab_function_type->setNormalLabelColor(ccc3(255,255,255));
			tab_function_type->setDefaultPanelByIndex(0);
			tab_function_type->addIndexChangedEvent(this,coco_indexchangedselector(GoldStoreUI::IndexChangedEvent));
			tab_function_type->setPressedActionEnabled(true);
			tab_function_type->setName("TypeTab");
			u_layer->addWidget(tab_function_type);
		}

		for (int i = 0;i<_num;i++)
		{
			delete ItemName[i];
		}
	}

	//refresh tableView
	//delete old data
	DataSource.erase(DataSource.begin(),DataSource.end());
	std::vector<CLableGoods*>::iterator iter;
	for (iter = DataSource.begin(); iter != DataSource.end(); ++iter)
	{
		delete *iter;
	}
	DataSource.clear();

	if (tab_main->getCurrentIndex() == 0)
	{
		if (u_layer->getWidgetByName("TypeTab"))
		{
			int idx = tab_function_type->getCurrentIndex();
			curLableId = GameView::getInstance()->goldStoreList.at(idx)->id();

			//add new data
			for (int i = 0;i<GameView::getInstance()->goldStoreList.at(idx)->commdity_size();++i)
			{
				CLableGoods * _LableGoods = new CLableGoods();
				_LableGoods->CopyFrom(GameView::getInstance()->goldStoreList.at(idx)->commdity(i));
				DataSource.push_back(_LableGoods);
			}
		}
	}
	else
	{
		if (u_layer->getWidgetByName("TypeTab"))
		{
			int idx = tab_function_type->getCurrentIndex();
			curLableId = GameView::getInstance()->bingGoldStoreList.at(idx)->id();

			//add new data
			for (int i = 0;i<GameView::getInstance()->bingGoldStoreList.at(idx)->commdity_size();++i)
			{
				CLableGoods * _LableGoods = new CLableGoods();
				_LableGoods->CopyFrom(GameView::getInstance()->bingGoldStoreList.at(idx)->commdity(i));
				DataSource.push_back(_LableGoods);
			}
		}
	}

	refreshMarquee();

	this->m_tableView->reloadData();
}

void GoldStoreUI::refreshMarquee()
{
	GoldStoreMarquee* tmpMarquee = (GoldStoreMarquee*)u_layer->getChildByTag(kTag_MARQUEE);
	if (NULL != tmpMarquee)
	{
		u_layer->removeChildByTag(kTag_MARQUEE);
	}

	initMarquee();
}

UITab * GoldStoreUI::getTab_function()
{
	return tab_function_type;
}
