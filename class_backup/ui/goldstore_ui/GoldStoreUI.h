#ifndef _GOLDSTOREUI_GOLDSTOREUI_H_
#define _GOLDSTOREUI_GOLDSTOREUI_H_

#include "../../ui/extensions/UIScene.h"

class UITab;
class CLableGoods;
class GoodsInfo;

class GoldStoreUI : public UIScene, public CCTableViewDataSource, public CCTableViewDelegate
{
public:
	/*商店右面的页签*/
	enum goldStore_tabType
	{
		type_all = 0,
		type_strength,
		type_aid,
		type_phypower,
		type_gem
	};


	GoldStoreUI();
	~GoldStoreUI();

	static GoldStoreUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);

	void CloseEvent(CCObject *pSender);
	void RechargeEvent(CCObject *pSender);
	void VipEvent(CCObject *pSender);

	void FunctionTypeIndexChangedEvent(CCObject* pSender);
	void IndexChangedEvent(CCObject* pSender);

	void refreshTypeTabByIndex(int idx);

	virtual void update(float dt);

	UITab * getTab_function();

private:
	UIPanel * ppanel;
	UILayer * u_layer;
	//
	UITab * tab_main;
	UITab * tab_function_type;

    cocos2d::extension::UIButton * Button_recharge;
	cocos2d::extension::UIButton * Button_vip;

	cocos2d::extension::UIImageView *ImageView_inGotPart;
	cocos2d::extension::UIImageView *ImageView_bindInGotPart;
	
	cocos2d::extension::UILabel * l_ingot_value;
	cocos2d::extension::UILabel * l_bindIngot_value;

	cocos2d::extension::UIImageView * m_imageView_horn;

	std::vector<CLableGoods *> DataSource;
	std::vector<GoodsInfo *> m_vector_goodsInfo;										// 打折物品（热卖物品）的vector

	std::string m_strMarquee;

	CCTableView * m_tableView;

public:
	int curLableId;

private:
	//  LiuLiang++
	void initMarquee();																					// 商城打折物品（热卖物品）跑马灯的实现
	void refreshMarquee();
    
public:
    void requestPay();
    void onHttpResponsePay(CCHttpClient *sender, CCHttpResponse *response);
};

#endif

