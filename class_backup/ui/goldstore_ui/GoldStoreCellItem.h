#ifndef  _GOLDSTOREUI_GOLDSTORECELL_H_
#define _GOLDSTOREUI_GOLDSTORECELL_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CLableGoods;

class GoldStoreCellItem : public UIScene
{
public:
	GoldStoreCellItem();
	~GoldStoreCellItem();

	static GoldStoreCellItem* create(CLableGoods * lableGoods);
	bool init(CLableGoods * lableGoods);

	void BuyEvent(CCObject *pSender);
	void GoodItemEvent(CCObject *pSender);

	void refreshUI(CLableGoods * lableGoods);

private:
	CLableGoods * curLableGoods;

	UILabel * l_name;
	UILabel * l_oldPrice_value;
	UILabel * l_oldPrice;
	UIImageView * imageView_oldPrice_goldIngold;
	UIImageView * imageView_redLine;
	UILabel * l_newPrice_value;
	UILabel * l_newPrice;
	UIImageView * imageView_newPrice_goldIngold;
	UIImageView * ImageView_hot;
	UILabel * l_price_value;
	UILabel * l_price;
	UIImageView * imageView_price_goldIngold;
	UIButton *Btn_goodsItemFrame;
	UIImageView * uiiImageView_goodsItem;
};

#endif

