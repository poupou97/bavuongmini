#include "ChatCell.h"
#include "ChatUI.h"
#include "../extensions/RichTextInput.h"
#include "AddPrivateUi.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../extensions/CCMoveableMenu.h"
#include "../extensions/CCRichLabel.h"
#include "../../GameView.h"
#include "../backpackscene/PacPageView.h"
#include "../../ui/extensions/RichElement.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "AppMacros.h"
#include "../../gamescene_state/sceneelement/ChatWindows.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/sceneelement/HeadMenu.h"
#include "../../legend_script/CCTutorialParticle.h"


ChatCell::ChatCell(void)
{	
}
ChatCell::~ChatCell(void)
{
}
ChatCell * ChatCell::create( int channel_id,std::string player_country,std::string play_name,std::string describe,long long playerid,bool isMainChat,int viplevel,long long listenerId,std::string listenerName /*= ""*/ )
{
	ChatCell * chatcell=new ChatCell();
	if(chatcell && chatcell->init(channel_id,player_country,play_name,describe,playerid,isMainChat,viplevel,listenerId,listenerName))
	{
		chatcell->autorelease();
		return chatcell;
	}
	CC_SAFE_DELETE(chatcell);
	return NULL;
}

bool ChatCell::init(int channel_id,std::string player_country,std::string play_name,std::string describe,long long playerid,bool isMainChat,int viplevel,long long listenerId,std::string listenerName /*= ""*/)
{
	if (CCTableViewCell::init())
	{
		channelId = channel_id;
		playerCountry = player_country;
		long long selfRoleId = GameView::getInstance()->myplayer->getRoleId();
		if ( channelId ==0 && listenerId != selfRoleId && strlen(listenerName.c_str()) >0)
		{
			playerId = listenerId;
		}else
		{
			playerId = playerid;
		}
		
		playName =play_name;
		describeText = describe;
		listenerName_ = listenerName;
		chatContent_ = describe;
		playerVip_ = viplevel;

		int nameSize2 = strlen(playName.c_str());
		std::string pathImage = getChannelImageOfIndex(channel_id);
		CCSprite * channelSp = CCSprite::create(pathImage.c_str());
		channelSp->setAnchorPoint(ccp(0,0));
		channelSp->setPosition(ccp(0,0));
		CCSprite * sp_country;
		//self name
		std::string selfName = GameView::getInstance()->myplayer->getActorName();
		//player name
		const char *countryState = StringDataManager::getString("chatui_playerNameOfcountry");
		std::string str_nameend = "[";
		//str_nameend.append(playerCountry);
		//str_nameend.append(countryState);
		str_nameend.append(playName);
		str_nameend.append("]");

        const int CHANNEL_TEXT_OFFSET = 2;
        const int COLON_TEXT_OFFSET = 2;
		if (isMainChat ==true)
		{
			int itemSize = 0;
			CCMoveableMenu *name_menu;
			CCMenuItemFont *itemImage_name;
			itemImage_name->setFontSize(20);
			CCLabelTTF *label_PrivateBegin;
			CCLabelTTF *label_PrivateEnd;
			CCNode *widgetVip;
			if (channelId == 0)
			{
				MainScene * mainscene_ =(MainScene *)GameView::getInstance()->getMainUIScene();
				if (strcmp(playName.c_str(),selfName.c_str())==0)
				{
					//self sayto other
					const char *selfToOtherSayStr = StringDataManager::getString("selfToOtherSay");
					label_PrivateBegin = CCLabelTTF::create(selfToOtherSayStr, APP_FONT_NAME, 20);
					label_PrivateBegin->setAnchorPoint(ccp(0,0));
					label_PrivateBegin->setPosition(ccp(channelSp->getContentSize().width,0));
					addChild(label_PrivateBegin);
					itemSize = channelSp->getContentSize().width + label_PrivateBegin->getContentSize().width;

					sp_country = CCSprite::create(getCountryImageByStr(playerCountry.c_str()).c_str());
					sp_country->setAnchorPoint(ccp(0,0));
					sp_country->setPosition(ccp(itemSize+1,0));
					sp_country->setScale(0.7f);
					addChild(sp_country);
					itemSize += sp_country->getContentSize().width*0.7f;

					std::string str_private_="[";
					//str_private_.append(playerCountry);
					//str_private_.append(countryState);
					str_private_.append(listenerName);
					str_private_.append("]");
					itemImage_name = CCMenuItemFont::create(str_private_.c_str(), this, menu_selector(ChatCell::NameEvent));
					ccColor3B nameColor=ccc3(161,255,156);
					itemImage_name->setColor(nameColor);
					itemImage_name->setAnchorPoint(ccp(0.5f,0));
					name_menu=CCMoveableMenu::create(itemImage_name,NULL);
					name_menu->setAnchorPoint(ccp(0,0));
					name_menu->setPosition(ccp(itemSize+itemImage_name->getContentSize().width/2,0));
					this->addChild(name_menu);
					itemSize += itemImage_name->getContentSize().width;
// 					if (viplevel > 0)
// 					{
// 						widgetVip = MainScene::addVipInfoByLevelForNode(viplevel);
// 						addChild(widgetVip);
// 						widgetVip->setPosition(ccp(itemSize+ widgetVip->getContentSize().width/2,0));
// 
// 						itemSize += widgetVip->getContentSize().width+5;
// 					}
				}else
				{
					sp_country = CCSprite::create(getCountryImageByStr(playerCountry.c_str()).c_str());
					sp_country->setAnchorPoint(ccp(0,0));
					sp_country->setPosition(ccp(channelSp->getContentSize().width+1.5f,0));
					sp_country->setScale(0.7f);
					addChild(sp_country);
					itemSize = channelSp->getContentSize().width + sp_country->getContentSize().width*0.7f;

					//other sayto self
					itemImage_name = CCMenuItemFont::create(str_nameend.c_str(), this, menu_selector(ChatCell::NameEvent));
					ccColor3B nameColor=ccc3(161,255,156);
					itemImage_name->setColor(nameColor);
					itemImage_name->setAnchorPoint(ccp(0.5f,0));
					name_menu=CCMoveableMenu::create(itemImage_name,NULL);
					name_menu->setAnchorPoint(ccp(0,0));
					name_menu->setPosition(ccp(itemSize+itemImage_name->getContentSize().width/2,0));
					this->addChild(name_menu);
					itemSize += itemImage_name->getContentSize().width;

					if (viplevel > 0)
					{
						widgetVip = MainScene::addVipInfoByLevelForNode(viplevel);
						addChild(widgetVip);
						widgetVip->setPosition(ccp(itemSize+ widgetVip->getContentSize().width/2,0));

						itemSize += widgetVip->getContentSize().width+5;
					}

					const char *otherToSelfSayStr = StringDataManager::getString("otherToSelfSay");
					label_PrivateBegin = CCLabelTTF::create(otherToSelfSayStr, APP_FONT_NAME, 20);
					label_PrivateBegin->setAnchorPoint(ccp(0,0));
					label_PrivateBegin->setPosition(ccp(itemSize,0));
					addChild(label_PrivateBegin);
					itemSize += label_PrivateBegin->getContentSize().width;
					
					GameView::getInstance()->hasPrivatePlaer = true;
					mainscene_->btnRemin->setVisible(true);
					mainscene_->btnReminOfNewMessageParticle->setVisible(true);
				}
				const char *privateChatOfPlayerStr = StringDataManager::getString("privateChatOfPlayer");
				label_PrivateEnd = CCLabelTTF::create(privateChatOfPlayerStr, APP_FONT_NAME, 20);
				label_PrivateEnd->setAnchorPoint(ccp(0,0));
				label_PrivateEnd->setPosition(ccp(itemSize,0));
				addChild(label_PrivateEnd);
				itemSize += label_PrivateEnd->getContentSize().width;
			}else
			{				
				if (nameSize2 > 0)
				{
					sp_country = CCSprite::create(getCountryImageByStr(playerCountry.c_str()).c_str());
					sp_country->setAnchorPoint(ccp(0,0));
					sp_country->setPosition(ccp(channelSp->getContentSize().width+1.5f,0));
					sp_country->setScale(0.7f);
					addChild(sp_country);
					itemSize = channelSp->getContentSize().width+sp_country->getContentSize().width*0.7f;

					itemImage_name = CCMenuItemFont::create(str_nameend.c_str(), this, menu_selector(ChatCell::NameEvent));
					ccColor3B nameColor=ccc3(161,255,156);
					itemImage_name->setColor(nameColor);
					itemImage_name->setAnchorPoint(ccp(0.5f,0));
					name_menu=CCMoveableMenu::create(itemImage_name,NULL);
					name_menu->setAnchorPoint(ccp(0,0));
					name_menu->setPosition(ccp(itemSize+itemImage_name->getContentSize().width/2,0));
					this->addChild(name_menu);

					itemSize += itemImage_name->getContentSize().width;

					if (viplevel > 0)
					{
						widgetVip = MainScene::addVipInfoByLevelForNode(viplevel);
						addChild(widgetVip);
						widgetVip->setPosition(ccp(itemSize+ widgetVip->getContentSize().width/2,0));

						itemSize += widgetVip->getContentSize().width + 5;
					}
				}else
				{
					itemSize=channelSp->getContentSize().width;
				}
			}
			
			CCLabelTTF *label_colon = CCLabelTTF::create(":", APP_FONT_NAME, 20);
			label_colon->setAnchorPoint(ccp(0,0));
			label_colon->setPosition(ccp(itemSize,0));
			ccColor3B colonColor=ccc3(161,255,156);
			label_colon->setColor(colonColor);

			int richlabel_space = itemSize+ 5;
			CCRichLabel *labelLink=CCRichLabel::createWithString(describe.c_str(),CCSizeMake(620,50),this,menu_selector(ChatCell::LinkEvent),richlabel_space);
			labelLink->setAnchorPoint(ccp(0,0));
			labelLink->setPosition(ccp(0,0));

			//if system channel ignor
			if (channel_id != 4 && nameSize2 > 0)
			{
				CCSprite *spBg=CCSprite::create("res_ui/liaotian/zhuan_di.png");
				CCMenuItemSprite *transmit = CCMenuItemSprite::create(spBg, spBg, spBg, this, menu_selector(ChatCell::callBackTransmit));
				transmit->setZoomScale(0.5f);
				CCMoveableMenu *transmit_menu=CCMoveableMenu::create(transmit,NULL);
				transmit_menu->setAnchorPoint(ccp(0,0));
				transmit_menu->setPosition(ccp(635,spBg->getContentSize().height/2));
				this->addChild(transmit_menu);
			}
			
			this->addChild(labelLink);
			this->addChild(label_colon);
			this->addChild(channelSp);

			const std::vector<int>& linesHeight = labelLink->getLinesHeight();
            CCAssert(linesHeight.size() > 0, "out of range");
			int offsetY = labelLink->getContentSize().height - linesHeight.at(0);
			
			if (nameSize2 > 0)
			{
				sp_country->setPositionY(offsetY);
				name_menu->setPositionY(offsetY);
			}
			if (channelId == 0)
			{
				label_PrivateBegin->setPositionY(offsetY);
				label_PrivateEnd->setPositionY(offsetY);
			}
			if (viplevel > 0)
			{
				if (!(channelId == 0 && strcmp(playName.c_str(),selfName.c_str())==0))
				{
					widgetVip->setPositionY(offsetY + widgetVip->getContentSize().height/2 - CHANNEL_TEXT_OFFSET );
				}
			}

			channelSp->setPositionY(offsetY + CHANNEL_TEXT_OFFSET);
			label_colon->setPositionY(offsetY + COLON_TEXT_OFFSET);
			label_width=labelLink->getContentSize().width;
			this->setContentSize(CCSizeMake(480,labelLink->getContentSize().height));
		}else
		{
			int itemSize = 0;
			CCLabelTTF *label_PrivateBegin;
			CCLabelTTF *label_PrivateEnd;
			//CCMenuItemFont * itemImage_name;
			//CCMoveableMenu * playerNameLabel;
			CCLabelTTF * label_name;

			CCNode * widgetVip;
			if (channelId == 0)
			{
				//ccColor3B nameColor=ccc3(161,255,156);
				ccColor3B nameColor= ccc3(243,252,2);
				if (strcmp(playName.c_str(),selfName.c_str())==0)
				{
					//self sayto other
					const char *selfToOtherSayStr = StringDataManager::getString("selfToOtherSay");
					label_PrivateBegin = CCLabelTTF::create(selfToOtherSayStr, APP_FONT_NAME, 20);
					label_PrivateBegin->setAnchorPoint(ccp(0,0));
					label_PrivateBegin->setPosition(ccp(channelSp->getContentSize().width,0));
					addChild(label_PrivateBegin);
					itemSize = channelSp->getContentSize().width + label_PrivateBegin->getContentSize().width;

					sp_country = CCSprite::create(getCountryImageByStr(playerCountry.c_str()).c_str());
					sp_country->setAnchorPoint(ccp(0,0));
					sp_country->setPosition(ccp(itemSize,0));
					sp_country->setScale(0.7f);
					addChild(sp_country);
					itemSize += sp_country->getContentSize().width*0.7f;

					std::string str_private_="[";
					//str_private_.append(playerCountry);
					//str_private_.append(countryState);
					str_private_.append(listenerName);
					str_private_.append("]");
					
					label_name = CCLabelTTF::create(str_private_.c_str(),APP_FONT_NAME, 20);
					label_name->setAnchorPoint(ccp(0.5f,0));
					label_name->setPosition(ccp(itemSize+label_name->getContentSize().width/2,0));
					label_name->setColor(nameColor);
					this->addChild(label_name);
					itemSize += label_name->getContentSize().width;

// 					if (viplevel > 0)
// 					{
// 						widgetVip = MainScene::addVipInfoByLevelForNode(viplevel);
// 						addChild(widgetVip);
// 						widgetVip->setPosition(ccp(itemSize+ widgetVip->getContentSize().width/2,0));
// 
// 						itemSize += widgetVip->getContentSize().width+5;
// 					}
				}else
				{
					//other sayto self
					sp_country = CCSprite::create(getCountryImageByStr(playerCountry.c_str()).c_str());
					sp_country->setAnchorPoint(ccp(0,0));
					sp_country->setPosition(ccp(channelSp->getContentSize().width+1,0));
					sp_country->setScale(0.7f);
					addChild(sp_country);
					itemSize = channelSp->getContentSize().width + sp_country->getContentSize().width*0.7f;

					label_name = CCLabelTTF::create(str_nameend.c_str(),APP_FONT_NAME, 20);
					label_name->setAnchorPoint(ccp(0.5f,0));
					label_name->setColor(nameColor);
					label_name->setPosition(ccp(itemSize + label_name->getContentSize().width/2,0));
					this->addChild(label_name);
					itemSize +=  label_name->getContentSize().width;

					if (viplevel > 0)
					{
						widgetVip = MainScene::addVipInfoByLevelForNode(viplevel);
						addChild(widgetVip);
						widgetVip->setPosition(ccp(itemSize+ widgetVip->getContentSize().width/2,0));

						itemSize += widgetVip->getContentSize().width+5;
					}
					const char *otherToSelfSayStr = StringDataManager::getString("otherToSelfSay");
					label_PrivateBegin = CCLabelTTF::create(otherToSelfSayStr, APP_FONT_NAME, 20);
					label_PrivateBegin->setAnchorPoint(ccp(0,0));
					label_PrivateBegin->setPosition(ccp(itemSize,0));
					addChild(label_PrivateBegin);
					itemSize += label_PrivateBegin->getContentSize().width;
				}
				const char *privateChatOfPlayerStr = StringDataManager::getString("privateChatOfPlayer");
				label_PrivateEnd = CCLabelTTF::create(privateChatOfPlayerStr, APP_FONT_NAME, 20);
				label_PrivateEnd->setAnchorPoint(ccp(0,0));
				label_PrivateEnd->setPosition(ccp(itemSize,0));
				addChild(label_PrivateEnd);
				itemSize += label_PrivateEnd->getContentSize().width;
		}else
		{
			if (nameSize2 > 0)
			{
				sp_country = CCSprite::create(getCountryImageByStr(playerCountry.c_str()).c_str());
				sp_country->setAnchorPoint(ccp(0,0));
				sp_country->setPosition(ccp(channelSp->getContentSize().width,0));
				sp_country->setScale(0.7f);
				addChild(sp_country);
				itemSize = channelSp->getContentSize().width+sp_country->getContentSize().width*0.7f;

				label_name = CCLabelTTF::create(str_nameend.c_str(),APP_FONT_NAME, 20);
				label_name->setAnchorPoint(ccp(0.5f,0));
				label_name->setPosition(ccp(itemSize+label_name->getContentSize().width/2,0));
				label_name->setColor(ccc3(243,252,2));
				this->addChild(label_name);
				itemSize += label_name->getContentSize().width;

				if (viplevel > 0)
				{
					widgetVip = MainScene::addVipInfoByLevelForNode(viplevel);
					addChild(widgetVip);
					widgetVip->setPosition(ccp(itemSize+ widgetVip->getContentSize().width/2,0));

					itemSize += widgetVip->getContentSize().width+5;
				}
			}else
			{
				itemSize=channelSp->getContentSize().width;
			}
		}

		CCLabelTTF *label_colon = CCLabelTTF::create(":",APP_FONT_NAME, 20);
		label_colon->setAnchorPoint(ccp(0,0));
		label_colon->setPosition(ccp(itemSize,0));

		CCRichLabel *labelLink =CCRichLabel::createWithString(describe.c_str(),CCSizeMake(430,50),NULL,NULL,itemSize+5);
		labelLink->setAnchorPoint(ccp(0,0));
		labelLink->setPosition(ccp(0,0));
		
		setOneCellOfLine(labelLink->getLinesHeight().size());

		this->addChild(labelLink);
		this->addChild(label_colon);
		this->addChild(channelSp);

        const std::vector<int>& linesHeight = labelLink->getLinesHeight();
        CCAssert(linesHeight.size() > 0, "out of range");
        int offsetY = labelLink->getContentSize().height - linesHeight.at(0);
		if (nameSize2 > 0)
		{
			sp_country->setPositionY(offsetY+CHANNEL_TEXT_OFFSET);
			label_name->setPositionY(offsetY+CHANNEL_TEXT_OFFSET);
			//playerNameLabel->setPositionY(offsetY+CHANNEL_TEXT_OFFSET);
		}
		if (channelId == 0)
		{
			label_PrivateBegin->setPositionY(offsetY);
			label_PrivateEnd->setPositionY(offsetY);
		}
		if (viplevel > 0)
		{
			if (!(channelId == 0 && strcmp(playName.c_str(),selfName.c_str())==0))
			{
				widgetVip->setPositionY(offsetY + widgetVip->getContentSize().height/2 - CHANNEL_TEXT_OFFSET);
			}
		}
        
        channelSp->setPositionY(offsetY + CHANNEL_TEXT_OFFSET);
		label_colon->setPositionY(offsetY + COLON_TEXT_OFFSET);
		label_width=labelLink->getContentSize().width;

		this->setContentSize(CCSizeMake(480,labelLink->getContentSize().height));
		}
		return true;
	}
	return false;
}

void ChatCell::onEnter()
{
	CCTableViewCell::onEnter();
}
void ChatCell::onExit()
{
	CCTableViewCell::onExit();
}

void ChatCell::LinkEvent( CCObject * obj )
{
	//add equipment
	UIScene * chatUi= (UIScene *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTabChat);
    if(chatUi == NULL)
        return;
    
	CCMenuItemFont* button = (CCMenuItemFont*)obj;
	RichElementButton* btnElement = (RichElementButton*)button->getUserData();
	//CCLog("richlabel clicked, %s", btnElement->linkContent.c_str());
    structPropIds prop_ids = RichElementButton::parseLink(btnElement->linkContent);
    CCLOG("prop id: %s, prop instance id: %ld", prop_ids.propId.c_str(), prop_ids.propInstanceId);
    
	const char * goodsPropId = prop_ids.propId.c_str();
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1322,(void *)prop_ids.propInstanceId,(void *)goodsPropId);
}

void ChatCell::NameEvent( CCObject* pSender )
{
	int selfLevel_ =GameView::getInstance()->myplayer->getActiveRole()->level();
	int openlevel = 0;
	std::map<int,int>::const_iterator cIter;
	cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(20);
	if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end())
	{
	}
	else
	{
		openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[20];
	}

	if (selfLevel_ < openlevel)
	{
		char openLevelStr[100];
		const char *strings = StringDataManager::getString("chatAndmailIsOpenInFiveLevel");
		sprintf(openLevelStr,strings,openlevel);
		GameView::getInstance()->showAlertDialog(openLevelStr);
		return;
	}

	
	ChatUI * m_chatUi= (ChatUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTabChat);
	long long selfRoleId = GameView::getInstance()->myplayer->getRoleId();
	if (selfRoleId == playerId)
	{
		return;
	}

	CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
	AddPrivateUi * nameList=AddPrivateUi::create(2);
	nameList->ignoreAnchorPointForPosition(false);
	nameList->setAnchorPoint(ccp(0,0));

	int cellOff = this->getPosition().y;
	//nameList->setPosition(ccp(200,cellOff+winsize.height/2-m_chatUi->getContentSize().height/2));
	nameList->setPosition(ccp(100,winsize.height/2));
	m_chatUi->addChild(nameList,4);
}

void ChatCell::callBackTransmit( CCObject * obj )
{
	CCMoveableMenu * sendId=(CCMoveableMenu *)obj;
	int tag = sendId->getTag();

	int chatSize = describeText.size();
	const char * chat_text=describeText.c_str();

	CCScene * pScene=CCDirector::sharedDirector()->getRunningScene();
	UIScene * m_chatUi= (UIScene *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTabChat);
	RichTextInputBox* text = (RichTextInputBox*)m_chatUi->getChildByTag(CHATUILAYER3)->getChildByTag(RICHTEXTINPUT_TAG);
	
	if (ChatUI::sendSecond==0)
	{
		text->onTextFieldInsertText(NULL, chat_text, chatSize);
		ChatUI::sendSecond++;
	}else
	{
		text->deleteAllInputString();
		text->onTextFieldInsertText(NULL, chat_text, chatSize);
	}
}

CCLabelTTF *ChatCell::getChannelNameByindex(int index)
{
	ccColor3B channelColor;
	const char *strings;
	switch(index)
	{
	case 0:
		{
			strings = StringDataManager::getString("chatui_private");
			channelColor=ccc3(255,163,226);
		}break;
	case 1:
		{
			strings = StringDataManager::getString("chatui_world");
			channelColor=ccc3(142,69,252);
		}break;
	case 2:
		{
			strings = StringDataManager::getString("chatui_room");
			channelColor=ccc3(0,240,216);
		}break;
	case 3:
		{
			strings = StringDataManager::getString("chatui_current");
			channelColor=ccc3(255,163,226);
		}break;
	case 4:
		{
			strings = StringDataManager::getString("chatui_system");
			channelColor=ccc3(255,163,226);
		}break;
	case 5:
		{
			strings = StringDataManager::getString("chatui_team");
			channelColor=ccc3(249,186,46);
		}break;
	case 6:
		{
			strings = StringDataManager::getString("chatui_camp");
			channelColor=ccc3(255,255,255);
		}break;
	case 7:
		{
			strings = StringDataManager::getString("chatui_faction");
		}break;
	case 8:
		{
			strings = StringDataManager::getString("chatui_country");
		}break;
	}

	std::string strFirst="<";
	std::string strSec=strFirst.append(strings);
	std::string chat_end=strSec.append(">");

	CCLabelTTF *channel_label = CCLabelTTF::create(chat_end.c_str(), APP_FONT_NAME, 20);
	channel_label->setColor(channelColor);
	return channel_label;
}

std::string ChatCell::getChannelImageOfIndex( int index )
{
	std::string imagepath;
	switch(index)
	{
	case 0:
		{
			imagepath = "res_ui/liaotian/siliao.png";
		}break;
	case 1:
		{
			imagepath = "res_ui/liaotian/shijie.png";
		}break;
	case 2:
		{
			/*
			imagepath = "res_ui/liaotian/siliao.png";
			strings = StringDataManager::getString("chatui_room");
			channelColor=ccc3(0,240,216);
			*/
		}break;
	case 3:
		{
			imagepath = "res_ui/liaotian/dangqian.png";
		}break;
	case 4:
		{
			imagepath = "res_ui/liaotian/xitong.png";
		}break;
	case 5:
		{
			imagepath = "res_ui/liaotian/duiwu.png";
		}break;
	case 6:
		{
			imagepath = "res_ui/liaotian/jiazu.png";
		}break;
	case 7:
		{
			imagepath = "res_ui/liaotian/siliao.png";
		}break;
	case 8:
		{
			imagepath = "res_ui/liaotian/guojia.png";
		}break;
	case 9:
		{
			imagepath = "res_ui/liaotian/chuan.png";
		}break;
	}
	return imagepath;
}

int ChatCell::getOneCellOfLine()
{
	return m_oneCellLine;
}

void ChatCell::setOneCellOfLine( int line )
{
	m_oneCellLine = line;
}

std::string ChatCell::getCountryImageByStr( std::string country )
{
	const char *country_1 = StringDataManager::getString("country_1");
	const char *country_2 = StringDataManager::getString("country_2");
	const char *country_3 = StringDataManager::getString("country_3");

	std::string imagePath_ = "res_ui/Expression/";
	if (strcmp(country.c_str(),country_1)== 0)
	{
		imagePath_.append("e31");
	}

	if (strcmp(country.c_str(),country_2)== 0)
	{
		imagePath_.append("e32");
	}

	if (strcmp(country.c_str(),country_3)== 0)
	{
		imagePath_.append("e33");
	}

	imagePath_.append(".png");

	return imagePath_;
}


