#ifndef  PRIVATECHATUI_H 
#define PRIVATECHATUI_H

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "../extensions/RichTextInput.h"

USING_NS_CC;
USING_NS_CC_EXT;

class PrivateChatUi:public UIScene,public cocos2d::extension::CCTableViewDataSource,public cocos2d::extension::CCTableViewDelegate
{
public:
	PrivateChatUi(void);
	~PrivateChatUi(void);

	static PrivateChatUi * create(int index);
	bool init(int index);
	void onEnter();
	void onExit();

	void setNewmessagePlayerId(long long playerid);
	long long getNewmessagePlayerId();

	//void setIsNewMessage(bool value_);
	void refreshNewMessageFlag(long long id_);

	void setNewMessageFlag(bool isVisible_);

	void refreshPrivateContent(CCObject * obj);
	
	void showPrivateContent(std::string selectName);

	RichTextInputBox * textInputBox;

	void callBackMin(CCObject * obj);
	void callBackNewMessage(CCObject * obj);
	void callBackDelete(CCObject * obj);
	void callBackBrow(CCObject * obj);
	void callBackProps(CCObject * obj);
	void callBackSend(CCObject * obj);
	void callBackClose(CCObject * obj);
public:
	virtual void scrollViewDidScroll(cocos2d::extension::CCScrollView * view);
	virtual void scrollViewDidZoom(cocos2d::extension::CCScrollView* view);
	virtual void tableCellTouched(cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell);
	virtual cocos2d::CCSize tableCellSizeForIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	virtual unsigned int numberOfCellsInTableView(cocos2d::extension::CCTableView *table);

	long long selectPlayerId;
	std::string selectPlayerName;
	CCTableView * nameTabview;
	UILayer * layer2;

	void setCellSelectIndex(int index);
	int getCellSelectIndex();
private:
	CCSize winsize;
	UIButton * btnBrow;
	
	long long m_newMessagePlayerId;
	bool m_isHaveNewMessage;
	int m_tabviewSelectIndex;

	UIImageView * image_newMessageFlag;
	UILabelBMFont * label_newMessageNum;
	struct ChatInfoStruct
	{
		int channelId;
		std::string chatContent;
		long long listenerId;
		std::string listenerName;
	};
};
////////////////////
class NameTableCell:public CCTableViewCell
{
public:
	NameTableCell(void);
	~NameTableCell(void);

	static NameTableCell *create(int idx);
	bool init(int idx);

	void onEnter();
	void onExit();
	void callBackNameList(CCObject * obj);
	void deleteSelectCell(CCObject * obj);
private:
	long long m_roleId;
	std::string m_roleName;
};


////
class NameList:public UIScene
{
public:
	NameList(void);
	~NameList(void);

	static NameList * create(long long id_,std::string name_);
	bool init(long long id_,std::string name_);
	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	
	void callBackFriendList(CCObject * obj);
private:
	struct FriendStruct
	{
		int operation;
		int relationType;
		long long playerid;
		std::string playerName;
	};

	long long m_roleId;
	std::string m_roleName;
};

#endif