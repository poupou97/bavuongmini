#include "PrivateChatUi.h"
#include "../extensions/CCMoveableMenu.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../AppMacros.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../../messageclient/element/CRelationPlayer.h"
#include "ChatCell.h"
#include "PrivateContent.h"
#include "ExpressionLayer.h"
#include "BackPackage.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "PrivateCell.h"
#include "../../utils/StaticDataManager.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "ChatUI.h"
#include "ui/extensions/UITab.h"
#include "ui/Mail_ui/MailUI.h"


PrivateChatUi::PrivateChatUi(void)
{
	selectPlayerId = 0;
	m_tabviewSelectIndex = -1;
}


PrivateChatUi::~PrivateChatUi(void)
{
}

PrivateChatUi * PrivateChatUi::create(int index)
{
	PrivateChatUi * privateUi =  new PrivateChatUi();
	if (privateUi && privateUi->init(index))
	{
		privateUi->autorelease();
		return privateUi;
	}
	CC_SAFE_DELETE(privateUi);
	return NULL;
}

bool PrivateChatUi::init(int index)
{
	if (UIScene::init())
	{
		winsize = CCDirector::sharedDirector()->getVisibleSize();

		UIPanel * privatePanel =(UIPanel*) GUIReader::shareReader()->widgetFromJsonFile("res_ui/PM_1.json");
		privatePanel->setAnchorPoint(ccp(0,0));
		privatePanel->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(privatePanel);

		nameTabview = CCTableView::create(this, CCSizeMake(206,300));
		nameTabview->setSelectedEnable(true);
		nameTabview->setSelectedScale9Texture("res_ui/highlight.png", CCRectMake(25, 24, 1, 1), ccp(0,0));
		nameTabview->setDirection(kCCScrollViewDirectionVertical);
		nameTabview->setAnchorPoint(ccp(0,0));
		nameTabview->setPosition(ccp(48,95));
		nameTabview->setDelegate(this);
		nameTabview->setVerticalFillOrder(kCCTableViewFillTopDown);
		m_pUiLayer->addChild(nameTabview);

		PrivateContent * contentui = PrivateContent::create();
		contentui->ignoreAnchorPointForPosition(false);
		contentui->setAnchorPoint(ccp(0,0));
		contentui->setPosition(ccp(292,135));
		contentui->setTag(ktagPrivateContentUi);
		this->addChild(contentui);

		UILayer * layer1 = UILayer::create();
		this->addChild(layer1,1);

		layer2 = UILayer::create();
		layer2->setContentSize(CCSizeMake(673,430));
		this->addChild(layer2,2);

		textInputBox=new RichTextInputBox();
		textInputBox->setInputMode(RichTextInputBox::kCCInputModeAppendOnly);
		textInputBox->setInputBoxWidth(335);
		textInputBox->setAnchorPoint(ccp(0,0));
		textInputBox->setPosition(ccp(287,85));
		layer1->addChild(textInputBox);
		textInputBox->setCharLimit(50);
		textInputBox->autorelease();

		UIButton * btnMinMize = (UIButton *)UIHelper::seekWidgetByName(privatePanel,"Button_minimize");
		btnMinMize->setTouchEnable(true);
		btnMinMize->setPressedActionEnabled(true);
		btnMinMize->addReleaseEvent(this,coco_releaseselector(PrivateChatUi::callBackMin));

		UIButton * btnNewMessage = (UIButton *)UIHelper::seekWidgetByName(privatePanel,"Button_31_newMessage");
		btnNewMessage->setTouchEnable(true);
		btnNewMessage->setPressedActionEnabled(true);
		btnNewMessage->addReleaseEvent(this,coco_releaseselector(PrivateChatUi::callBackNewMessage));
		
		image_newMessageFlag = UIImageView::create();
		image_newMessageFlag->setTexture("res_ui/new1.png");
		image_newMessageFlag->setAnchorPoint(ccp(0.5f,0.5f));
		image_newMessageFlag->setPosition(ccp(btnNewMessage->getContentSize().width - 1,btnNewMessage->getContentSize().height/2 - 3));
		btnNewMessage->addChild(image_newMessageFlag);
		image_newMessageFlag->setVisible(false);

		label_newMessageNum = UILabelBMFont::create();
		label_newMessageNum->setAnchorPoint(ccp(0.5f,0.5f));
		label_newMessageNum->setPosition(ccp(0.5,0));
		label_newMessageNum->setFntFile("res_ui/font/ziti_1.fnt");
		label_newMessageNum->setText("0");
		label_newMessageNum->setScale(0.55f);
		image_newMessageFlag->addChild(label_newMessageNum);

		int m_vectorSize = GameView::getInstance()->chatPrivateSpeakerVector.size();
		if ( m_vectorSize > 0)
		{
			if (index == 0)
			{
				selectPlayerName = GameView::getInstance()->chatPrivateSpeakerVector.at(0)->playername();
				selectPlayerId = GameView::getInstance()->chatPrivateSpeakerVector.at(0)->playerid();
				this->showPrivateContent(selectPlayerName);
				nameTabview->selectCell(0);
			}else
			{
				selectPlayerName = GameView::getInstance()->chatPrivateSpeakerVector.at(m_vectorSize - 1)->playername();
				selectPlayerId = GameView::getInstance()->chatPrivateSpeakerVector.at(m_vectorSize - 1)->playerid();
				this->showPrivateContent(selectPlayerName);
				nameTabview->selectCell(m_vectorSize - 1);
			}
		}

		
		UIButton * btnDelete = (UIButton *)UIHelper::seekWidgetByName(privatePanel,"Button_delete");
		btnDelete->setTouchEnable(true);
		btnDelete->setPressedActionEnabled(true);
		btnDelete->addReleaseEvent(this,coco_releaseselector(PrivateChatUi::callBackDelete));

		btnBrow = (UIButton *)UIHelper::seekWidgetByName(privatePanel,"Button_brow");
		btnBrow->setTouchEnable(true);
		btnBrow->setPressedActionEnabled(true);
		btnBrow->addReleaseEvent(this,coco_releaseselector(PrivateChatUi::callBackBrow));

		UIButton * btnProps = (UIButton *)UIHelper::seekWidgetByName(privatePanel,"Button_props");
		btnProps->setTouchEnable(true);
		btnProps->setPressedActionEnabled(true);
		btnProps->addReleaseEvent(this,coco_releaseselector(PrivateChatUi::callBackProps));

		UIButton * btnSend = (UIButton *)UIHelper::seekWidgetByName(privatePanel,"Button_send");
		btnSend->setTouchEnable(true);
		btnSend->setPressedActionEnabled(true);
		btnSend->addReleaseEvent(this,coco_releaseselector(PrivateChatUi::callBackSend));

		UIButton * btnClose = (UIButton *)UIHelper::seekWidgetByName(privatePanel,"Button_close");
		btnClose->setTouchEnable(true);
		btnClose->setPressedActionEnabled(true);
		btnClose->addReleaseEvent(this,coco_releaseselector(PrivateChatUi::callBackClose));

// 		UIImageView * imageKuang = UIImageView::create();
// 		imageKuang->setTexture("res_ui/LV5_dikuang_miaobian1.png");
// 		imageKuang->setAnchorPoint(ccp(0,0));
// 		imageKuang->setScale9Enable(true);
// 		imageKuang->setScale9Size(CCSizeMake(226,319));
// 		imageKuang->setCapInsets(CCRect(32,32,1,1));
// 		imageKuang->setPosition(ccp(39,87));
// 		layer2->addWidget(imageKuang);

		this->setContentSize(CCSizeMake(673,430));
		this->setTouchMode(kCCTouchesOneByOne);
		this->setTouchEnabled(true);
		return true;
	}
	return false;
}

void PrivateChatUi::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void PrivateChatUi::onExit()
{
	UIScene::onExit();
}

void PrivateChatUi::scrollViewDidScroll( cocos2d::extension::CCScrollView * view )
{

}

void PrivateChatUi::scrollViewDidZoom( cocos2d::extension::CCScrollView* view )
{

}

void PrivateChatUi::tableCellTouched( cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell )
{
	int index_ = cell->getIdx();
	
	CCTableViewCell * m_cell = table->cellAtIndex(this->getCellSelectIndex());
	if (m_cell != NULL)
	{
		NameTableCell * nameCell_1 = (NameTableCell *)m_cell->getChildByTag(1010);
		CCMoveableMenu * menu_ = (CCMoveableMenu *)nameCell_1->getChildByTag(1011);
		menu_->runAction(CCMoveBy::create(0.2f,ccp(80,0)));
		nameCell_1->runAction(CCMoveBy::create(0.2f,ccp(5,0)));
		menu_->setVisible(false);
	}
 	
 	this->setCellSelectIndex(index_);
	NameTableCell * nameCell_ = (NameTableCell *)cell->getChildByTag(1010);
	if (nameCell_ != NULL)
	{
		CCMoveableMenu * menu_ = (CCMoveableMenu *)nameCell_->getChildByTag(1011);
		menu_->runAction(CCMoveBy::create(0.2f,ccp(-80,0)));
		nameCell_->runAction(CCMoveBy::create(0.2f,ccp(-5,0)));
		menu_->setVisible(true);
	}
	
	selectPlayerId =  GameView::getInstance()->chatPrivateSpeakerVector.at(this->getCellSelectIndex())->playerid();
	selectPlayerName = GameView::getInstance()->chatPrivateSpeakerVector.at(this->getCellSelectIndex())->playername();
	this->refreshNewMessageFlag(selectPlayerId);
	this->showPrivateContent(GameView::getInstance()->chatPrivateSpeakerVector.at(this->getCellSelectIndex())->playername());
}

cocos2d::CCSize PrivateChatUi::tableCellSizeForIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	return CCSizeMake(206,61);
}

cocos2d::extension::CCTableViewCell* PrivateChatUi::tableCellAtIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	CCString * string =CCString::createWithFormat("%d",idx);	
	CCTableViewCell *cell =table->dequeueCell();
	cell=new CCTableViewCell();
	cell->autorelease();

	CCScale9Sprite * spbg = CCScale9Sprite::create("res_ui/kuang0_new.png");
	spbg->setPreferredSize(CCSizeMake(206,61));
	spbg->setCapInsets(CCRect(15,30,1,1));
	spbg->setAnchorPoint(ccp(0,0));
	spbg->setPosition(ccp(0,0));
	cell->addChild(spbg);

	NameTableCell * m_layer = NameTableCell::create(idx);
	m_layer->ignoreAnchorPointForPosition(false);
	m_layer->setAnchorPoint(ccp(0,0));
	m_layer->setPosition(ccp(0,0));
	m_layer->setTag(1010);
	cell->addChild(m_layer);

	return cell;
}

unsigned int PrivateChatUi::numberOfCellsInTableView( cocos2d::extension::CCTableView *table )
{
	return GameView::getInstance()->chatPrivateSpeakerVector.size();
}

void PrivateChatUi::refreshPrivateContent( CCObject * obj )
{
	CCMenuItemSprite * item = (CCMenuItemSprite *)obj;
	int index = item->getTag();
	
	selectPlayerName = GameView::getInstance()->chatPrivateSpeakerVector.at(index)->playername();
	selectPlayerId = GameView::getInstance()->chatPrivateSpeakerVector.at(index)->playerid();
	this->showPrivateContent(selectPlayerName);
}

void PrivateChatUi::callBackMin( CCObject * obj )
{
	this->closeAnim();
	MainScene * mainScene =(MainScene *)GameView::getInstance()->getMainUIScene();
	mainScene->btnRemin->setVisible(true);
	mainScene->btnReminOfNewMessageParticle->setVisible(false);

	GameView::getInstance()->hasPrivatePlaer = false;
	GameView::getInstance()->isShowPrivateChatBtn = true;
}

void PrivateChatUi::callBackNewMessage( CCObject * obj )
{
	int m_size = GameView::getInstance()->chatPrivateNewMessageInfo.size();
	if (m_size > 0)
	{
		long long m_id = GameView::getInstance()->chatPrivateNewMessageInfo.at(m_size - 1)->playerid();
		if (GameView::getInstance()->chatPrivateSpeakerVector.size() >0)
		{
			for (int i =0; i<GameView::getInstance()->chatPrivateSpeakerVector.size();i++)
			{
				if (GameView::getInstance()->chatPrivateSpeakerVector.at(i)->playerid() == m_id)
				{
					nameTabview->selectCell(i);
					showPrivateContent(GameView::getInstance()->chatPrivateSpeakerVector.at(i)->playername());
					selectPlayerName = GameView::getInstance()->chatPrivateSpeakerVector.at(i)->playername();
				}
			}

			this->refreshNewMessageFlag(m_id);
		}
	}
}

void PrivateChatUi::callBackDelete( CCObject * obj )
{
	textInputBox->onTextFieldDeleteBackward(NULL, NULL, 0);
}

void PrivateChatUi::callBackBrow( CCObject * obj )
{
	ExpressionLayer * ex=ExpressionLayer::create(ktagPrivateUI);
	ex->ignoreAnchorPointForPosition(false);
	ex->setAnchorPoint(ccp(0.5f,0));
	ex->setPosition(ccp(layer2->getContentSize().width/2,btnBrow->getPosition().y  + btnBrow->getContentSize().height));
	layer2->addChild(ex,4);
}

void PrivateChatUi::callBackProps( CCObject * obj )
{
	BackPackage * backbag=BackPackage::create(ktagPrivateUI);
	backbag->ignoreAnchorPointForPosition(false);
	backbag->setAnchorPoint(ccp(0.5f,0.5f));
	backbag->setTag(PACKAGEAGE);
	backbag->setPosition(ccp(layer2->getContentSize().width/2,layer2->getContentSize().height/2));
	layer2->addChild(backbag);
}

void PrivateChatUi::callBackSend( CCObject * obj )
{
	//send 2203
	const char * inputStr = textInputBox->getInputString();
	if (inputStr == NULL)
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("chatui_callBackSend_null"));
		return;
	}
	if (strcmp(selectPlayerName.c_str(),"")==0)
	{
		const char *string_ = StringDataManager::getString("privateChat_privateNameIsNull");
		GameView::getInstance()->showAlertDialog(string_);
		return;
	}

	ChatInfoStruct info = {0,inputStr,-1,selectPlayerName};
	GameMessageProcessor::sharedMsgProcessor()->sendReq(2203, &info);

	textInputBox->deleteAllInputString();
}

void PrivateChatUi::callBackClose( CCObject * obj )
{
	this->closeAnim();
	MainScene * mainScene =(MainScene *)GameView::getInstance()->getMainUIScene();
	mainScene->btnRemin->setVisible(false);
	mainScene->btnReminOfNewMessageParticle->setVisible(false);
	GameView::getInstance()->hasPrivatePlaer = false;
	GameView::getInstance()->isShowPrivateChatBtn = false;
}

void PrivateChatUi::setNewmessagePlayerId( long long playerid )
{
	m_newMessagePlayerId = playerid;
}

long long PrivateChatUi::getNewmessagePlayerId()
{
	return m_newMessagePlayerId;
}

void PrivateChatUi::refreshNewMessageFlag(long long id_)
{
	for (int i = 0; i<GameView::getInstance()->chatPrivateNewMessageInfo.size();i++)
	{
		if (id_ ==GameView::getInstance()->chatPrivateNewMessageInfo.at(i)->playerid())
		{
			std::vector<CRelationPlayer *>::iterator iter= GameView::getInstance()->chatPrivateNewMessageInfo.begin() + i;
			CRelationPlayer * playerInfo =*iter ;
			GameView::getInstance()->chatPrivateNewMessageInfo.erase(iter);
			delete playerInfo;
			i--;
		}
	}
		
	if (GameView::getInstance()->chatPrivateNewMessageInfo.size() > 0)
	{
		this->setNewMessageFlag(true);
	}else
	{
		this->setNewMessageFlag(false);
	}
}

void PrivateChatUi::showPrivateContent(std::string selectName )
{
	GameView::getInstance()->chatPrivateContentSourceVector.clear();
	for (int i=0;i< GameView::getInstance()->chatPrivateContentVector.size();i++)
	{
		//遍历vector 找到和自己私聊的相关人及相关聊天内容
		if (strcmp(selectName.c_str(),GameView::getInstance()->chatPrivateContentVector.at(i)->speakName.c_str())==0 ||
			strcmp(selectName.c_str(),GameView::getInstance()->chatPrivateContentVector.at(i)->listenerName.c_str())==0)
		{
			GameView::getInstance()->chatPrivateContentSourceVector.push_back(GameView::getInstance()->chatPrivateContentVector.at(i));
		}
	}
	PrivateContent * contentui = (PrivateContent *)this->getChildByTag(ktagPrivateContentUi);
	contentui->chatContentTabview->reloadData();

	if(contentui->chatContentTabview->getContentSize().height > contentui->chatContentTabview->getViewSize().height)
		contentui->chatContentTabview->setContentOffset(contentui->chatContentTabview->maxContainerOffset(), false);	
}

void PrivateChatUi::setNewMessageFlag( bool isVisible_ )
{
	int size_ = GameView::getInstance()->chatPrivateNewMessageInfo.size();
	char num[10];
	sprintf(num,"%d",size_);
	image_newMessageFlag->setVisible(isVisible_);
	label_newMessageNum->setText(num);
}

void PrivateChatUi::setCellSelectIndex( int index )
{
	m_tabviewSelectIndex = index;
}

int PrivateChatUi::getCellSelectIndex()
{
	return m_tabviewSelectIndex;
}

////////////////////////////
NameTableCell::NameTableCell( void )
{

}

NameTableCell::~NameTableCell( void )
{

}

NameTableCell * NameTableCell::create( int idx )
{
	NameTableCell * cell_ = new NameTableCell();
	if (cell_ && cell_->init(idx))
	{
		cell_->autorelease();
		return cell_;
	}
	CC_SAFE_DELETE(cell_);
	return NULL;
}

bool NameTableCell::init( int idx )
{
	if (CCTableViewCell::init())
	{
		int playerId_ = GameView::getInstance()->chatPrivateSpeakerVector.at(idx)->playerid();
		std::string playName_ = GameView::getInstance()->chatPrivateSpeakerVector.at(idx)->playername();
		int playerLevel_ = GameView::getInstance()->chatPrivateSpeakerVector.at(idx)->level();
		int profession_ = GameView::getInstance()->chatPrivateSpeakerVector.at(idx)->profession();
		int vipLevel_ = GameView::getInstance()->chatPrivateSpeakerVector.at(idx)->viplevel();
		int countryId = GameView::getInstance()->chatPrivateSpeakerVector.at(idx)->country();

		m_roleId = playerId_;
		m_roleName = playName_;

		CCSprite * iconBackGround =CCSprite::create("res_ui/round.png");
		iconBackGround->setAnchorPoint(ccp(0.5f,0.5f));
		iconBackGround->setPosition(ccp(40,30));
		addChild(iconBackGround);

		std::string playerIcon_ = "res_ui/protangonis56x55/none.png";
		if (profession_ >0 && profession_ <5)
		{
			playerIcon_ = BasePlayer::getHeadPathByProfession(profession_);
		}
		
		/*
		CCSprite * friendHead =CCSprite::create(playerIcon_.c_str());
		CCMenuItemSprite * nameIcon_item = CCMenuItemSprite::create(friendHead, friendHead, friendHead, this, menu_selector(NameTableCell::callBackNameList));
		nameIcon_item->setZoomScale(0.6f);
		CCMoveableMenu * nameIcon_menu = CCMoveableMenu::create(nameIcon_item,NULL);
		nameIcon_menu->setAnchorPoint(ccp(0.5f,0.5f));
		nameIcon_menu->setPosition(ccp(-60,-23));
		//nameIcon_menu->setPosition(ccp(iconBackGround->getContentSize().width/2,iconBackGround->getContentSize().height/2));
		iconBackGround->addChild(nameIcon_menu);
		nameIcon_menu->setScale(0.8f);
		*/

		UILayer * layer = UILayer::create();
		addChild(layer);

		UIButton * btn_icon = UIButton::create();
		btn_icon->setTextures(playerIcon_.c_str(),playerIcon_.c_str(),"");
		btn_icon->setAnchorPoint(ccp(0.5f,0.5f));
		btn_icon->addReleaseEvent(this,coco_releaseselector(NameTableCell::callBackNameList));
		btn_icon->setTouchEnable(true);
		btn_icon->setPosition(ccp(40,30));
		btn_icon->setPressedActionEnabled(true);
		layer->addWidget(btn_icon);

		btn_icon->setScale(0.8f);

		CCScale9Sprite  * playerLvBg = CCScale9Sprite::create("res_ui/zhezhao80_new.png");
		playerLvBg->setAnchorPoint(ccp(0,0));
		playerLvBg->setPreferredSize(CCSizeMake(35,12));
		playerLvBg->setPosition(ccp(iconBackGround->getPositionX()+playerLvBg->getContentSize().width/2 - 8,4));
		addChild(playerLvBg);

		char string_lv[5];
		sprintf(string_lv,"%d",playerLevel_);
		std::string playerlevelStr_ = "LV";
		playerlevelStr_.append(string_lv);

		CCLabelTTF *label_level = CCLabelTTF::create(playerlevelStr_.c_str(), APP_FONT_NAME, 12);
		label_level->setAnchorPoint(ccp(0.5,0));
		label_level->setPosition(ccp(playerLvBg->getContentSize().width/2,0));
		playerLvBg->addChild(label_level,10);

		CCLabelTTF *label_playName = CCLabelTTF::create(playName_.c_str(), APP_FONT_NAME, 16);
		label_playName->setAnchorPoint(ccp(0.5f,0));
		label_playName->setPosition(ccp(115,30));
		addChild(label_playName,10);

		if (countryId >0 && countryId <6)
		{
			std::string countryIdName = "country_";
			char id[2];
			sprintf(id, "%d", countryId);
			countryIdName.append(id);
			std::string iconPathName = "res_ui/country_icon/";
			iconPathName.append(countryIdName);
			iconPathName.append(".png");

			CCSprite * countrySp_ = CCSprite::create(iconPathName.c_str());
			countrySp_->setAnchorPoint(ccp(0.5f,0));
			countrySp_->setPosition(ccp(100,5));
			countrySp_->setScale(0.8f);
			countrySp_->setTag(10);
			addChild(countrySp_);
		}

		if (vipLevel_ > 0)
		{
			CCNode *  widgetVip = MainScene::addVipInfoByLevelForNode(vipLevel_);
			addChild(widgetVip);
			widgetVip->setTag(11);
			widgetVip->setPosition(ccp(135,16));
		}

		CCSprite *delete_ImageBg=CCSprite::create("res_ui/close.png");
		CCMenuItemSprite *delete_itemBg = CCMenuItemSprite::create(delete_ImageBg, delete_ImageBg, delete_ImageBg, this, menu_selector(NameTableCell::deleteSelectCell));
		delete_itemBg->setZoomScale(0.5f);
		CCMoveableMenu *delete_name=CCMoveableMenu::create(delete_itemBg,NULL);
		delete_name->setAnchorPoint(ccp(1,0));
		delete_name->setPosition(ccp(260,29));
		delete_name->setTag(1011);
		addChild(delete_name);
		
		PrivateChatUi * privateui_ = (PrivateChatUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagPrivateUI);
		if (privateui_ != NULL)
		{
			if (privateui_->getCellSelectIndex() == idx )
			{
				iconBackGround->setPositionX(iconBackGround->getPositionX() - 5);
				//friendHead->setPositionX(friendHead->getPositionX() - 5);
				btn_icon->setPosition(ccp(35,30));
				playerLvBg->setPositionX(playerLvBg->getPositionX() - 5);
				label_playName->setPositionX(label_playName->getPositionX() - 5);
				if (countryId >0 && countryId <6)
				{
					CCSprite * sp_= (CCSprite *)getChildByTag(10);
					sp_->setPositionX(sp_->getPositionX() - 5);
				}
				if (vipLevel_ > 0)
				{
					CCNode * node_ = (CCNode *)getChildByTag(11);
					node_->setPositionX(node_->getPositionX() - 5);
				}

				delete_name->setPosition(ccp(175,29));
			}
		}
		
		return true;
	}
	return false;
}

void NameTableCell::onEnter()
{
	CCTableViewCell::onEnter();
}

void NameTableCell::onExit()
{
	CCTableViewCell::onExit();
}

void NameTableCell::deleteSelectCell( CCObject * obj )
{
	PrivateChatUi * privateui_ = (PrivateChatUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagPrivateUI);
	int index = privateui_->getCellSelectIndex();
	privateui_->selectPlayerId = GameView::getInstance()->chatPrivateSpeakerVector.at(index)->playerid();
	//std::string m_name = GameView::getInstance()->chatPrivateSpeakerVector.at(index)->playername();
	for (int i=0;i< GameView::getInstance()->chatPrivateSpeakerVector.size();i++)
	{
		if (privateui_->selectPlayerId == GameView::getInstance()->chatPrivateSpeakerVector.at(i)->playerid())
		{
			std::vector<CRelationPlayer *>::iterator iter= GameView::getInstance()->chatPrivateSpeakerVector.begin() + i;
			CRelationPlayer * playerInfo =*iter ;
			GameView::getInstance()->chatPrivateSpeakerVector.erase(iter);
			delete playerInfo;
		}
	}
	privateui_->nameTabview->reloadData();
	/*
	//遍历聊天内容 把内容删除（或可不删除  留有历史聊天记录）
	for (int i=0;i< GameView::getInstance()->chatPrivateContentVector.size();i++)
	{
		if (strcmp(m_name.c_str(),GameView::getInstance()->chatPrivateContentVector.at(i)->speakName.c_str())==0 ||
					strcmp(m_name.c_str(),GameView::getInstance()->chatPrivateContentVector.at(i)->listenerName.c_str())==0)
		{
			std::vector<PrivateCell *>::iterator iter= GameView::getInstance()->chatPrivateContentVector.begin() + i;
			PrivateCell * contentStr_ =*iter ;
			GameView::getInstance()->chatPrivateContentVector.erase(iter);
			delete contentStr_;
			//GameView::getInstance()->chatPrivateContentSourceVector.push_back(GameView::getInstance()->chatPrivateContentVector.at(i));
		}
	}
	*/
	if (GameView::getInstance()->chatPrivateSpeakerVector.size()<= index)
	{
		index--;
	}

	if (GameView::getInstance()->chatPrivateSpeakerVector.size() > 0)
	{
		privateui_->selectPlayerName = GameView::getInstance()->chatPrivateSpeakerVector.at(index)->playername();
		privateui_->nameTabview->selectCell(index);
	}else
	{
		privateui_->selectPlayerName = "";
	}
	
	privateui_->showPrivateContent(privateui_->selectPlayerName);
}

void NameTableCell::callBackNameList( CCObject * obj )
{
 	PrivateChatUi * privateui_ = (PrivateChatUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagPrivateUI);

	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	NameList * nameList = NameList::create(m_roleId,m_roleName);
	nameList->ignoreAnchorPointForPosition(false);
	nameList->setAnchorPoint(ccp(0,0));
	//nameList->setPosition(ccp(50,winSize.height/2 - nameList->getContentSize().height/2+20));
	nameList->setPosition(ccp(-80,winSize.height/2 - nameList->getContentSize().height/2+20));
	privateui_->layer2->addChild(nameList);
}


////////////////////////////
NameList::NameList( void )
{

}

NameList::~NameList( void )
{

}

NameList * NameList::create(long long id_,std::string name_)
{
	NameList * pui = new NameList();
	if (pui && pui->init(id_,name_))
	{
		pui->autorelease();
		return pui;
	}
	CC_SAFE_DELETE(	pui);
	return NULL;
}

bool NameList::init(long long id_,std::string name_)
{
	if(UIScene::init())
	{
		CCSize s=CCDirector::sharedDirector()->getVisibleSize();

		const char *str_addfriend = StringDataManager::getString("friend_addfriend");
		char *addfriend_left=const_cast<char*>(str_addfriend);	

		const char *str_check = StringDataManager::getString("friend_check");
		char *check_left=const_cast<char*>(str_check);

		const char *str_team = StringDataManager::getString("friend_team");
		char *team_left=const_cast<char*>(str_team);

		const char *str_mail = StringDataManager::getString("friend_mail");
		char *mail_left=const_cast<char*>(str_mail);

		const char *str_black = StringDataManager::getString("friend_black");
		char *black_left=const_cast<char*>(str_black);

		UIPanel* panel = UIPanel::create();
		panel->setSize(CCSizeMake(100, 200));
		panel->setPosition(ccp(200,150));
		const char * normalImage = "res_ui/new_button_5.png";
		const char * selectImage ="res_ui/new_button_5.png";
		const char * finalImage = "";
		char * label[] = {check_left,team_left,addfriend_left,mail_left,black_left};

		m_roleId = id_;
		m_roleName = name_;

		UITab* privateTab = UITab::createWithText(5,normalImage,selectImage,finalImage,label,VERTICAL,-2);
		privateTab->setAnchorPoint(ccp(0,0));
		privateTab->setPosition(ccp(0,0));
		privateTab->setHighLightImage((char*)finalImage);
		privateTab->addIndexChangedEvent(this,coco_indexchangedselector(NameList::callBackFriendList));
		privateTab->setPressedActionEnabled(true);
		panel->addChild(privateTab);
		m_pUiLayer->addWidget(panel);

		setContentSize(CCSizeMake(100,200));
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		return true;
	}
	return false;
}

void NameList::onEnter()
{
	UIScene::onEnter();
}

void NameList::onExit()
{
	UIScene::onExit();
}

bool NameList::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	return this->resignFirstResponder(pTouch,this,false,false);
}

void NameList::callBackFriendList( CCObject * obj )
{
	UITab * tab =(UITab *)obj;
	int num = tab->getCurrentIndex();
	
	CCSize winSize =CCDirector::sharedDirector()->getWinSize();

	switch(num)
	{
	case 0:
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1142,(void *)m_roleId);
		}break;
	case 1:
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1401,(void *)m_roleId);
		}break;
	case 2:
		{
			FriendStruct friend1={0,0,m_roleId,m_roleName};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend1);
		}break;
	case 3:
		{
			MailUI * mail_ui=MailUI::create();
			mail_ui->ignoreAnchorPointForPosition(false);
			mail_ui->setAnchorPoint(ccp(0.5f,0.5f));
			mail_ui->setPosition(ccp(winSize.width/2,winSize.height/2));
			GameView::getInstance()->getMainUIScene()->addChild(mail_ui,0,kTagMailUi);
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2001,(void *)mail_ui->curMailPage);
			mail_ui->channelLabel->setDefaultPanelByIndex(1);
			mail_ui->callBackChangeMail(mail_ui->channelLabel);

			mail_ui->friendName->onTextFieldInsertText(NULL,m_roleName.c_str(),30);
		}break;
	case 4:
		{
			FriendStruct friend1={0,1,m_roleId,m_roleName};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend1);
		}break;
	}
	removeFromParentAndCleanup(true);
}
