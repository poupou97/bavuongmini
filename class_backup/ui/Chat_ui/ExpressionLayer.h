#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;
class ExpressionLayer :public UIScene
{
public:
	ExpressionLayer(void);
	~ExpressionLayer(void);

	static ExpressionLayer * create(int uitag);
	bool init(int uitag);
	void onEnter();

	bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	void callBack(CCObject * obj);
private:
	int index;
	int uitagIndex;
};

