#ifndef _H_PRIVATECELL_
#define _H_PRIVATECELL_
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CCRichLabel;

class PrivateCell:public CCTableViewCell
{
public:
	PrivateCell(void);
	~PrivateCell(void);
	static PrivateCell * create(int pressionId_,std::string play_name,std::string chatContent,long long playerid,int viplevel,std::string listener_Name = "");
	bool init(int pressionId_,std::string play_name,std::string chatContent,long long playerid,int viplevel,std::string listener_Name = "");
	
	void onEnter();
	void onExit();
	
	void LinkEvent(CCObject * obj);
	void NameEvent(CCObject* pSender);

	void setCurCellHeight(int height);
	int getCurCellHeight();

	void callBackNameList(CCObject * obj);

public:
	std::string speakName;
	std::string listenerName;
	long long speakerId;

private:
	int m_curCellHeight;
};
#endif
