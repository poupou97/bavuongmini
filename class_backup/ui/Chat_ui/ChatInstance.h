#ifndef _INSTANCE_CHATINSTANCE_H_
#define _INSTANCE_CHATINSTANCE_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class ChatInstance
{
public:
	ChatInstance();
	~ChatInstance();

	static ChatInstance * s_chatInstance;
	static ChatInstance * getInstance();

	void setCurChannel(int index_);
	int getCurChannel();
private:
	int m_curSelectChannel;
};

#endif