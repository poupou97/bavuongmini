#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class RichTextInputBox;

typedef enum{

	RICHBOXINPUT_TAG=205,
};
enum
{
	PRIVATECHAT=0,
	CHECK,
	TEAM,
	FRIEND,
	MAIL,
	BLACKLIST
};
class AddPrivateUi:public UIScene
{
public:
	AddPrivateUi(void);
	~AddPrivateUi(void);

	struct FriendList
	{
		int relationType;
		int pageNum;
		int pageIndex;
		int showOnline;
	};

	static AddPrivateUi * create(int num);
	bool init(int num);
	virtual void onEnter();
	virtual void onExit();
	void callBackFriend(CCObject * obj);

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);
	
	void callBackClosed(CCObject * obj);
	void callBackFriendList(CCObject * obj);

	void selectButton_channel(CCObject * obj);
public:
	RichTextInputBox * textBox_private;
	int currType;
private:
	 UIPanel * privatePanel;
	 struct FriendStruct
	 {
		 int operation;
		 int relationType;
		 long long playerid;
		 std::string playerName;
	 };
};

