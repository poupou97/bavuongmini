#include "StoreHouseUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "GameView.h"
#include "StoreHouseItem.h"
#include "StoreHousePageView.h"
#include "../backpackscene/PacPageView.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../extensions/Counter.h"
#include "../goldstore_ui/GoldStoreUI.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "../../messageclient/element/CShortCut.h"
#include "../../messageclient/element/CDrug.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutSlot.h"

StoreHouseUI::StoreHouseUI():
storeOrPac(false),
m_remainArrangePac(0.0f),
m_remainArrangeStore(0.0f)
{
// 	//coordinate for packageitem
// 	for (int m = 4;m>=0;m--)
// 	{
// 		for (int n = 3;n>=0;n--)
// 		{
// 			int _m = 4-m;
// 			int _n = 3-n;
// 
// 			Coordinate temp ;
// 			temp.x = 6+_n*68;
// 			temp.y = 1+m*64;
// 
// 			CoordinateVector.push_back(temp);
// 		}
// 	}
// 	//UIPageView_Panel_Name
// 	for (int i = 0;i<4;i++)
// 	{
// 		std::string storePageViewBaseName = "storePageViewPanel_";
// 		char num[4];
// 		int tmpID = i;
// 		sprintf(num, "0%d", tmpID);
// 		std::string number = num;
// 		storePageViewBaseName.append(num);
// 		storePageViewPanelName[i] = storePageViewBaseName;
// 	}
// 	//storeItem name
// 	for (int i=0 ;i<itemNumEveryPage; ++i)
// 	{
// 		std::string storeBaseName = "storeItem_";
// 		char num[4];
// 		int tmpID = i;
// 		if(tmpID < 10)
// 			sprintf(num, "0%d", tmpID);
// 		else
// 			sprintf(num, "%d", tmpID);
// 		std::string number = num;
// 		storeBaseName.append(num);
// 		storeItem_name[i] = storeBaseName;
// 	}
}


StoreHouseUI::~StoreHouseUI()
{
}

StoreHouseUI* StoreHouseUI::create()
{
	StoreHouseUI * storeHouseUI = new StoreHouseUI();
	if (storeHouseUI && storeHouseUI->init())
	{
		storeHouseUI->autorelease();
		return storeHouseUI;
	}
	CC_SAFE_DELETE(storeHouseUI);
	return NULL;
}

bool StoreHouseUI::init()
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();

		this->scheduleUpdate();

		u_layer = UILayer::create();
		u_layer->ignoreAnchorPointForPosition(false);
		u_layer->setAnchorPoint(ccp(0.5f,0.5f));
		u_layer->setContentSize(CCSizeMake(800, 480));
		u_layer->setPosition(CCPointZero);
		u_layer->setPosition(ccp(winsize.width/2,winsize.height/2));
		addChild(u_layer);

		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winsize);
		mengban->setAnchorPoint(CCPointZero);
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);

		//加载UI
		//ppanel = (UIPanel*)GUIReader::shareReader()->widgetFromJsonFile("renwubeibao_2/renwubeibao_1.json");
		if(LoadSceneLayer::StoreHouseLayer->getWidgetParent() != NULL)
		{
			LoadSceneLayer::StoreHouseLayer->removeFromParentAndCleanup(false);
		}
		//ppanel = (UIPanel*)LoadSceneLayer::s_pPanel->copy();
		ppanel = LoadSceneLayer::StoreHouseLayer;
		ppanel->setAnchorPoint(ccp(0.5f,0.5f));
		ppanel->getValidNode()->setContentSize(CCSizeMake(800, 480));
		ppanel->setPosition(CCPointZero);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnable(true);
		ppanel->setPosition(ccp(winsize.width/2,winsize.height/2));
		m_pUiLayer->addWidget(ppanel);

		const char * secondStr = StringDataManager::getString("UIName_cang");
		const char * thirdStr = StringDataManager::getString("UIName_ku");
		CCArmature * atmature = MainScene::createPendantAnm("",secondStr,thirdStr,"");
		atmature->setPosition(ccp(30,240));
		u_layer->addChild(atmature);

		pointLeft = (UIImageView *)UIHelper::seekWidgetByName(ppanel,"dian_on_left");
		pointLeft->setPosition(ccp(178,94));
		pointRight = (UIImageView *)UIHelper::seekWidgetByName(ppanel,"dian_on_right");

		//关闭按钮
		UIButton * Button_close = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnable(true);
		Button_close->setPressedActionEnabled(true);
		Button_close->addReleaseEvent(this, coco_releaseselector(StoreHouseUI::CloseEvent));
		//整理背包按钮
		UIButton * Button_sortPac = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_ArrangeBackpack");
		Button_sortPac->setTouchEnable(true);
		Button_sortPac->addReleaseEvent(this, coco_releaseselector(StoreHouseUI::SortPacEvent));
		Button_sortPac->setPressedActionEnabled(true);
		//整理仓库按钮
		UIButton * Button_sortStore = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_ArrangeWarehouse");
		Button_sortStore->setTouchEnable(true);
		Button_sortStore->addReleaseEvent(this, coco_releaseselector(StoreHouseUI::SortStoreHouseEvent));
		Button_sortStore->setPressedActionEnabled(true);
		//商城按钮
		UIButton * Button_shop = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_shop");
		Button_shop->setTouchEnable(true);
		Button_shop->addReleaseEvent(this, coco_releaseselector(StoreHouseUI::ShopEvent));
		Button_shop->setPressedActionEnabled(true);

		//jinbi
		int m_goldValue = GameView::getInstance()->getPlayerGold();
		char GoldStr_[20];
		sprintf(GoldStr_,"%d",m_goldValue);
		Label_gold = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_gold");
		Label_gold->setText(GoldStr_);
		//yuanbao
		int m_goldIngot =  GameView::getInstance()->getPlayerGoldIngot();
		char GoldIngotStr_[20];
		sprintf(GoldIngotStr_,"%d",m_goldIngot);
		Label_goldIngot = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_goldIngold");
		Label_goldIngot->setText(GoldIngotStr_);


		// Create the page view
		//reloadInit();
		//左侧仓库
		m_storePageView = GameView::getInstance()->storeHousePageView;
		m_storePageView->getPageView()->addPageTurningEvent(this,coco_PageView_PageTurning_selector(StoreHouseUI::LeftPageViewChanged));
		u_layer->addChild(m_storePageView);
		m_storePageView->setPosition(ccp(75,119));
		m_storePageView->setCurUITag(kTagStoreHouseUI);
		m_storePageView->setVisible(true);
		
		//右侧背包
		m_pacPageView = GameView::getInstance()->pacPageView;
		m_pacPageView->getPageView()->addPageTurningEvent(this,coco_PageView_PageTurning_selector(StoreHouseUI::RightPageViewChanged));
		u_layer->addChild(m_pacPageView);
		m_pacPageView->setPosition(ccp(389,125));
		m_pacPageView->setCurUITag(kTagStoreHouseUI);
		m_pacPageView->setVisible(true);
		CCSequence* seq = CCSequence::create(CCDelayTime::create(0.05f),CCCallFunc::create(this,callfunc_selector(StoreHouseUI::PageScrollToDefault)),NULL);
		m_pacPageView->runAction(seq);
		m_pacPageView->checkCDOnBegan();
		//m_pacPageView->getPageView()->scrollToPage(0);
		//m_pacPageView->getPageView()->onTouchMoved(m_pacPageView->getPageView()->getPage(1)->getPosition());

		//refresh cd
		ShortcutLayer * shortCutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
		if (shortCutLayer)
		{
			for (int i = 0;i<7;++i)  //一共7个快捷栏
			{
				if (i < GameView::getInstance()->shortCutList.size())
				{
					CShortCut * c_shortcut = GameView::getInstance()->shortCutList.at(i);
					if (c_shortcut->storedtype() == 2)    //快捷栏中是物品
					{
						if(CDrug::isDrug(c_shortcut->skillpropid()))
						{
							if (shortCutLayer->getChildByTag(shortCutLayer->shortcurTag[c_shortcut->index()]+100))
							{
								ShortcutSlot * shortcutSlot = (ShortcutSlot*)shortCutLayer->getChildByTag(shortCutLayer->shortcurTag[c_shortcut->index()]+100);
								this->m_pacPageView->RefreshCD(c_shortcut->skillpropid());
							}
						}
					}
				}
			}
		}

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(winsize);

		return true;
	} 
	return false;
}

void StoreHouseUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void StoreHouseUI::onExit()
{
	UIScene::onExit();
}

bool StoreHouseUI::ccTouchBegan(CCTouch *touch, CCEvent * pEvent)
{
	//return this->resignFirstResponder(touch,this,false);
	CCPoint location = touch->getLocation();

	CCPoint pos = this->getPosition();
	CCSize size = this->getContentSize();
	CCPoint anchorPoint = this->getAnchorPointInPoints();
	pos = ccpSub(pos, anchorPoint);
	CCRect rect(pos.x, pos.y, size.width, size.height);
	if(rect.containsPoint(location))
	{
		return true;
	}

	return false;
}

void StoreHouseUI::ccTouchEnded(CCTouch *touch, CCEvent * pEvent)
{
}

void StoreHouseUI::ccTouchCancelled(CCTouch *touch, CCEvent * pEvent)
{
}

void StoreHouseUI::ccTouchMoved(CCTouch *touch, CCEvent * pEvent)
{
}


void StoreHouseUI::CloseEvent( CCObject * pSender )
{
	this->closeAnim();
}

void StoreHouseUI::SortPacEvent( CCObject *pSender )
{
	if (m_remainArrangePac > 0.f)
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("package_arrange_canNot"));
		return;
	}

	m_remainArrangePac = 15.0f;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1308, (void *)1);
}

void StoreHouseUI::SortStoreHouseEvent( CCObject * pSender )
{
	if (m_remainArrangeStore > 0.f)
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("package_arrange_canNot"));
		return;
	}

	m_remainArrangeStore = 15.0f;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1308, (void *)2);
}

void StoreHouseUI::ShopEvent( CCObject * pSender )
{
	GoldStoreUI *goldStoreUI = (GoldStoreUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoldStoreUI);
	if(goldStoreUI == NULL)
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		goldStoreUI = GoldStoreUI::create();
		GameView::getInstance()->getMainUIScene()->addChild(goldStoreUI,0,kTagGoldStoreUI);

		goldStoreUI->ignoreAnchorPointForPosition(false);
		goldStoreUI->setAnchorPoint(ccp(0.5f, 0.5f));
		goldStoreUI->setPosition(ccp(winSize.width/2, winSize.height/2));
	}
}

void StoreHouseUI::LeftPageViewChanged( CCObject* pSender )
{
	switch((int)(m_storePageView->getPageView()->getCurPageIndex()))
	{
	case 0:
		pointLeft->setPosition(ccp(178,94));
		break;
	case 1:
		pointLeft->setPosition(ccp(199,94));
		break;
	case 2:
		pointLeft->setPosition(ccp(218,94));
		break;
	case 3:
		pointLeft->setPosition(ccp(237,94));
		break;
	}
}

void StoreHouseUI::RightPageViewChanged( CCObject* pSender )
{
	switch((int)(m_pacPageView->getPageView()->getCurPageIndex()))
	{
	case 0:
		pointRight->setPosition(ccp(513,94));
		break;
	case 1:
		pointRight->setPosition(ccp(534,94));
		break;
	case 2:
		pointRight->setPosition(ccp(553,94));
		break;
	case 3:
		pointRight->setPosition(ccp(572,94));
		break;
	case 4:
		pointRight->setPosition(ccp(591,94));
		break;
	}
}

void StoreHouseUI::PutInOrOutEvent( CCObject * pSender )
{
	if (this->isClosing())
		return;

 	if (GameView::getInstance()->pacPageView->curFolder->quantity()>1)
 	{
 		GameView::getInstance()->showCounter(this, callfuncO_selector(StoreHouseUI::GetStoreItemNum),GameView::getInstance()->pacPageView->curFolder->quantity());
 	}
 	else
 	{
		ReqData * temp = new ReqData();
		temp->mode = 0;
		temp->source = GameView::getInstance()->pacPageView->curPackageItemIndex;
		temp->num = 1;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1319,temp);
		delete temp;
	}
	
}

void StoreHouseUI::ReloadData()
{
	this->m_storePageView->ReloadData();

	//左侧仓库
	if (GameView::getInstance()->storeHousePageView->getParent() != NULL) 
	{
		GameView::getInstance()->storeHousePageView->removeFromParent();
	}

	m_storePageView = GameView::getInstance()->storeHousePageView;
	m_storePageView->getPageView()->addPageTurningEvent(this,coco_PageView_PageTurning_selector(StoreHouseUI::LeftPageViewChanged));
	u_layer->addChild(m_storePageView);
	m_storePageView->setPosition(ccp(75,119));
	m_storePageView->setCurUITag(kTagStoreHouseUI);
	m_storePageView->setVisible(true);
}
// 
// void StoreHouseUI::reloadInit()
// {
// 	m_storePageView = UIPageView::create();
// 	m_storePageView->setTouchEnable(true);
// 	m_storePageView->setSize(CCSizeMake(278, 324));
// 	m_storePageView->setPosition(ccp(77,116));
// 
// 	int idx = 0;
// 	for (int i = 0; i < 4; ++i)
// 	{
// 		UIPanel* panel = UIPanel::create();
// 		panel->setSize(CCSizeMake(278, 324));
// 		panel->setName(storePageViewPanelName[i].c_str());
// 		panel->setTouchEnable(true);
// 		if ((int)GameView::getInstance()->storeHouseItemList.size()<=itemNumEveryPage*(i+1))
// 		{
// 			if (itemNumEveryPage*i < (int)GameView::getInstance()->storeHouseItemList.size())
// 			{
// 				for (int m = itemNumEveryPage*i;m<(int)GameView::getInstance()->storeHouseItemList.size();m++)
// 				{
// 					StoreHouseItem * storeHouseItem = StoreHouseItem::create(GameView::getInstance()->storeHouseItemList.at(m));
// 					storeHouseItem->setAnchorPoint(ccp(0,0));
// 					storeHouseItem->setPosition(ccp(CoordinateVector.at(idx%itemNumEveryPage).x, CoordinateVector.at(idx%itemNumEveryPage).y));
// 					storeHouseItem->setName(storeItem_name[idx%itemNumEveryPage].c_str());
// 					//packageItem->setSize(CCSizeMake(91,84));
// 					storeHouseItem->setScale(1.0f);
// 					panel->addChild(storeHouseItem);
// 					idx++;
// 				}
// 				for (int n = GameView::getInstance()->storeHouseItemList.size();n<itemNumEveryPage*(i+1);n++)
// 				{
// 					//create locked 
// 					StoreHouseItem * storeHouseItem = StoreHouseItem::createLockedItem(n);
// 					storeHouseItem->setAnchorPoint(ccp(0,0));
// 					storeHouseItem->setPosition(ccp(CoordinateVector.at(idx%itemNumEveryPage).x, CoordinateVector.at(idx%itemNumEveryPage).y));
// 					storeHouseItem->setName(storeItem_name[idx%itemNumEveryPage].c_str());
// 					//packageItem->setSize(CCSizeMake(91,84));
// 					storeHouseItem->setScale(1.0f);
// 					panel->addChild(storeHouseItem);
// 					idx++;
// 				}
// 			}
// 			else
// 			{
// 				for (int n = itemNumEveryPage*i;n<itemNumEveryPage*(i+1);n++)
// 				{
// 					//create locked 
// 					StoreHouseItem * storeHouseItem = StoreHouseItem::createLockedItem(n);
// 					storeHouseItem->setAnchorPoint(ccp(0,0));
// 					storeHouseItem->setPosition(ccp(CoordinateVector.at(idx%itemNumEveryPage).x, CoordinateVector.at(idx%itemNumEveryPage).y));
// 					storeHouseItem->setName(storeItem_name[idx%itemNumEveryPage].c_str());
// 					//packageItem->setSize(CCSizeMake(91,84));
// 					storeHouseItem->setScale(1.0f);
// 					panel->addChild(storeHouseItem);
// 					idx++;
// 				}
// 			}
// 		}
// 		else
// 		{
// 			for (int m = 0;m<itemNumEveryPage*(i+1);m++)
// 			{
// 				StoreHouseItem * storeHouseItem = StoreHouseItem::create(GameView::getInstance()->storeHouseItemList.at(m));
// 				storeHouseItem->setAnchorPoint(ccp(0,0));
// 				storeHouseItem->setPosition(ccp(CoordinateVector.at(idx%itemNumEveryPage).x, CoordinateVector.at(idx%itemNumEveryPage).y));
// 				storeHouseItem->setName(storeItem_name[idx%itemNumEveryPage].c_str());
// 				//packageItem->setSize(CCSizeMake(91,84));
// 				storeHouseItem->setScale(1.0f);
// 				panel->addChild(storeHouseItem);
// 				idx++;
// 			}
// 		}
// 		m_storePageView->addPage(panel);
// 		m_storePageView->addPageTurningEvent(this,coco_PageView_PageTurning_selector(StoreHouseUI::LeftPageViewChanged));
// 
// 	}
// 	u_layer->addWidget(m_storePageView);
// 
// }

void StoreHouseUI::ReloadOneStoreHouseItem( FolderInfo * folderInfo )
{
	int id = folderInfo->id();
	int pageIndex  = 0 ;

	if (id/this->m_storePageView->itemNumEveryPage < 1)
	{
		pageIndex = 0;
	}
	else if (id/this->m_storePageView->itemNumEveryPage < 2)
	{
		pageIndex = 1;
	}
	else if (id/this->m_storePageView->itemNumEveryPage < 3)
	{
		pageIndex = 2;
	}
	else if (id/this->m_storePageView->itemNumEveryPage < 4)
	{
		pageIndex = 3;
	}
	//delete old
	UIPanel * panel = (UIPanel*)m_storePageView->getPageView()->getChildByName(m_storePageView->storePageViewPanelName[pageIndex].c_str());
	StoreHouseItem * oldStoreHouseItem = (StoreHouseItem *)panel->getChildByName(this->m_storePageView->storeItem_name[id%this->m_storePageView->itemNumEveryPage].c_str());
	oldStoreHouseItem->removeFromParentAndCleanup(true);

	StoreHouseItem * storeHouseItem = StoreHouseItem::create(folderInfo);
	storeHouseItem->setAnchorPoint(ccp(0,0));
	storeHouseItem->setPosition(ccp(this->m_storePageView->CoordinateVector.at(id%this->m_storePageView->itemNumEveryPage).x, this->m_storePageView->CoordinateVector.at(id%this->m_storePageView->itemNumEveryPage).y));
	storeHouseItem->setName(this->m_storePageView->storeItem_name[id%this->m_storePageView->itemNumEveryPage].c_str());
	storeHouseItem->setScale(1.0f);
	panel->addChild(storeHouseItem);
}

void StoreHouseUI::GetStoreItemNum( CCObject* pSender )
{
	Counter * counter =(Counter *)pSender;

	ReqData * temp = new ReqData();
	temp->mode = 0;
	temp->source = GameView::getInstance()->pacPageView->curPackageItemIndex;
	temp->num = counter->getInputNum();
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1319,temp);
	delete temp;

}

void StoreHouseUI::update( float dt )
{
	//金币
	char str_gold[20];
	sprintf(str_gold,"%d",GameView::getInstance()->getPlayerGold());
	Label_gold->setText(str_gold);
	//元宝
	char str_ingot[20];
	sprintf(str_ingot,"%d",GameView::getInstance()->getPlayerGoldIngot());
	Label_goldIngot->setText(str_ingot);

	m_remainArrangePac -= 1.0f/60;
	if (m_remainArrangePac < 0)
		m_remainArrangePac = 0.f;

	m_remainArrangeStore -= 1.0f/60;
	if (m_remainArrangeStore < 0)
		m_remainArrangeStore = 0.f;
}

void StoreHouseUI::PageScrollToDefault()
{
	m_pacPageView->getPageView()->scrollToPage(0);
	RightPageViewChanged(m_pacPageView);
}
