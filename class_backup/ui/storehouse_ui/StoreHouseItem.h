
#ifndef _STOREHOUSE_STOREHOSUEITEM_H_
#define _STOREHOUSE_STOREHOSUEITEM_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class FolderInfo;

typedef enum {
	COMMONSTOREHOUSEITEM,
	VOIDSTOREHOUSEITEM,
	LOCKSTOREHOUSEITEM,
}storeHouseItemType;


class StoreHouseItem : public UIWidget
{
public:
	StoreHouseItem();
	~StoreHouseItem();

	static StoreHouseItem * create(FolderInfo* storeFolder);
	bool init(FolderInfo* storeFolder);
	static StoreHouseItem * createLockedItem(int storeFolderIndex);
	bool initLockedItem(int storeFolderIndex);
	FolderInfo * curStoreFolder;

	void StoreHouseItemEvent(CCObject * pSender);
	CCPoint autoGetPosition(CCObject * pSender);

	void BtnLockEvent(CCObject * pSender);
	void SureEvent(CCObject * pSender);
	void CancelEvent(CCObject * pSender);

protected:
	storeHouseItemType cur_type;
	int index ;
	UIImageView *ImageView_storeItem;
	UILabel * Label_num;

};

#endif

