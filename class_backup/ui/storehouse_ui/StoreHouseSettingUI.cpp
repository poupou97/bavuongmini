#include "StoreHouseSettingUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../extensions/RichTextInput.h"


StoreHouseSettingUI::StoreHouseSettingUI()
{
}


StoreHouseSettingUI::~StoreHouseSettingUI()
{
}

StoreHouseSettingUI* StoreHouseSettingUI::create()
{
	StoreHouseSettingUI * storeHouseSettingUI = new StoreHouseSettingUI();
	if (storeHouseSettingUI && storeHouseSettingUI->init())
	{
		storeHouseSettingUI->autorelease();
		return storeHouseSettingUI;
	}
	CC_SAFE_DELETE(storeHouseSettingUI);
	return NULL;
}

bool StoreHouseSettingUI::init()
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();

		this->scheduleUpdate();

		u_layer = UILayer::create();
		u_layer->ignoreAnchorPointForPosition(false);
		u_layer->setAnchorPoint(ccp(0.5f,0.5f));
		u_layer->setContentSize(CCSizeMake(800, 480));
		u_layer->setPosition(CCPointZero);
		u_layer->setPosition(ccp(winsize.width/2,winsize.height/2));
		addChild(u_layer);

		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winsize);
		mengban->setAnchorPoint(CCPointZero);
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);

		//����UI
		if(LoadSceneLayer::StoreHouseSettingLayer == NULL)
		{
			LoadSceneLayer::StoreHouseSettingLayer=(UIPanel *)GUIReader::shareReader()->widgetFromJsonFile("res_ui/password_popupo_1.json");
			LoadSceneLayer::StoreHouseSettingLayer->retain();
		}
		if(LoadSceneLayer::StoreHouseSettingLayer->getWidgetParent() != NULL)
		{
			LoadSceneLayer::StoreHouseSettingLayer->removeFromParentAndCleanup(false);
		}
		//ppanel = (UIPanel*)LoadSceneLayer::s_pPanel->copy();
		ppanel = LoadSceneLayer::StoreHouseSettingLayer;
		ppanel->setAnchorPoint(ccp(0.5f,0.5f));
		ppanel->getValidNode()->setContentSize(CCSizeMake(354, 315));
		ppanel->setPosition(CCPointZero);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnable(true);
		ppanel->setPosition(ccp(winsize.width/2,winsize.height/2));
		m_pUiLayer->addWidget(ppanel);

		//�رհ�ť
		UIButton * Button_close = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnable(true);
		Button_close->setPressedActionEnabled(true);
		Button_close->addReleaseEvent(this, coco_releaseselector(StoreHouseSettingUI::CloseEvent));

		panel_enterPassWord = (UIPanel *)UIHelper::seekWidgetByName(ppanel,"Panel_EnterPassword");
		panel_enterPassWord->setVisible(false);
		layer_enterPassWord = UILayer::create();
		layer_enterPassWord->setVisible(false);
		panel_changePassWord = (UIPanel *)UIHelper::seekWidgetByName(ppanel,"Panel_ChangePassword");
		panel_changePassWord->setVisible(false);
		layer_changePassWord = UILayer::create();
		layer_changePassWord->setVisible(false);
		panel_setPassWord = (UIPanel *)UIHelper::seekWidgetByName(ppanel,"Panel_SetPassword");
		panel_setPassWord->setVisible(false);
		layer_setPassWord = UILayer::create();
		layer_setPassWord->setVisible(false);
		///////////
		RichTextInputBox * Enter_enterPassWord = new RichTextInputBox();
		Enter_enterPassWord->setInputBoxWidth(163);
		Enter_enterPassWord->setAnchorPoint(ccp(0,0));
		Enter_enterPassWord->setPosition(ccp(131,142));
		Enter_enterPassWord->setCharLimit(10);
		layer_enterPassWord->addChild(Enter_enterPassWord);
		Enter_enterPassWord->autorelease();
		////////////
		RichTextInputBox * Set_enterPassWord = new RichTextInputBox();
		Set_enterPassWord->setInputBoxWidth(163);
		Set_enterPassWord->setAnchorPoint(ccp(0,0));
		Set_enterPassWord->setPosition(ccp(131,165));
		Set_enterPassWord->setCharLimit(10);
		layer_setPassWord->addChild(Set_enterPassWord);
		Set_enterPassWord->autorelease();
		RichTextInputBox *Set_enterPassWordAgain = new RichTextInputBox();
		Set_enterPassWordAgain->setInputBoxWidth(163);
		Set_enterPassWordAgain->setAnchorPoint(ccp(0,0));
		Set_enterPassWordAgain->setPosition(ccp(131,120));
		Set_enterPassWordAgain->setCharLimit(10);
		layer_setPassWord->addChild(Set_enterPassWordAgain);
		Set_enterPassWordAgain->autorelease();
		///////////
		RichTextInputBox * Change_enterOldPassWord = new RichTextInputBox();
		Change_enterOldPassWord->setInputBoxWidth(163);
		Change_enterOldPassWord->setAnchorPoint(ccp(0,0));
		Change_enterOldPassWord->setPosition(ccp(131,174));
		Change_enterOldPassWord->setCharLimit(10);
		layer_changePassWord->addChild(Change_enterOldPassWord);
		Change_enterOldPassWord->autorelease();
		RichTextInputBox * Change_enterNewPassWord = new RichTextInputBox();
		Change_enterNewPassWord->setInputBoxWidth(163);
		Change_enterNewPassWord->setAnchorPoint(ccp(0,0));
		Change_enterNewPassWord->setPosition(ccp(131,132));
		Change_enterNewPassWord->setCharLimit(10);
		layer_changePassWord->addChild(Change_enterNewPassWord);
		Change_enterNewPassWord->autorelease();
		RichTextInputBox * Change_enterNewPassWordAgain = new RichTextInputBox();
		Change_enterNewPassWordAgain->setInputBoxWidth(163);
		Change_enterNewPassWordAgain->setAnchorPoint(ccp(0,0));
		Change_enterNewPassWordAgain->setPosition(ccp(131,89));
		Change_enterNewPassWordAgain->setCharLimit(10);
		layer_changePassWord->addChild(Change_enterNewPassWordAgain);
		Change_enterNewPassWordAgain->autorelease();

		return true;
	}
	return false;
}

void StoreHouseSettingUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void StoreHouseSettingUI::onExit()
{
	UIScene::onExit();
}

bool StoreHouseSettingUI::ccTouchBegan(CCTouch *touch, CCEvent * pEvent)
{
	//return this->resignFirstResponder(touch,this,false);
	CCPoint location = touch->getLocation();

	CCPoint pos = this->getPosition();
	CCSize size = this->getContentSize();
	CCPoint anchorPoint = this->getAnchorPointInPoints();
	pos = ccpSub(pos, anchorPoint);
	CCRect rect(pos.x, pos.y, size.width, size.height);
	if(rect.containsPoint(location))
	{
		return true;
	}

	return false;
}

void StoreHouseSettingUI::ccTouchEnded(CCTouch *touch, CCEvent * pEvent)
{
}

void StoreHouseSettingUI::ccTouchCancelled(CCTouch *touch, CCEvent * pEvent)
{
}

void StoreHouseSettingUI::ccTouchMoved(CCTouch *touch, CCEvent * pEvent)
{
}

void StoreHouseSettingUI::CloseEvent( CCObject *pSender )
{
	this->closeAnim();
}
