#ifndef _STOREHOUSE_STOREHOUSEINFO_H
#define _STOREHOUSE_STOREHOUSEINFO_H

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../backpackscene/GoodsItemInfoBase.h"

class FolderInfo;

class StoreHouseInfo : public GoodsItemInfoBase
{
public:
	StoreHouseInfo();
	~StoreHouseInfo();

	static StoreHouseInfo * create(FolderInfo * folder);
	bool init(FolderInfo * folder);

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

	void GetOutEvent(CCObject *pSender);
	void GetStoreItemNum(CCObject *pSender);

private:
	int  m_nfolderIndex;
	int  m_quantity;

};

#endif

