#include "StoreHouseItem.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../backpackscene/PackageItemInfo.h"
#include "StoreHouseInfo.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "AppMacros.h"


#define voidFramePath "res_ui/sdi_none.png"
#define whiteFramePath "res_ui/sdi_white.png"
#define greenFramePath "res_ui/sdi_green.png"
#define blueFramePath "res_ui/sdi_bule.png"
#define purpleFramePath "res_ui/sdi_purple.png"
#define orangeFramePath "res_ui/sdi_orange.png"

#define fontBgPath "res_ui/zidi.png"

#define M_Scale_Cur 0.95f

StoreHouseItem::StoreHouseItem()
{
}


StoreHouseItem::~StoreHouseItem()
{
}

StoreHouseItem * StoreHouseItem::create( FolderInfo* storeFolder )
{
	StoreHouseItem * storeHouseItem = new StoreHouseItem();
	if (storeHouseItem && storeHouseItem->init(storeFolder))
	{
		storeHouseItem->autorelease();
		return storeHouseItem;
	}
	CC_SAFE_DELETE(storeHouseItem);
	return NULL;
}

bool StoreHouseItem::init( FolderInfo* storeFolder )
{
	if (UIWidget::init())
	{
		curStoreFolder = storeFolder;
		index = storeFolder->id();
		if (storeFolder->has_goods() == true)
		{
			cur_type = COMMONSTOREHOUSEITEM;

			/*********************判断装备的颜色***************************/
			std::string frameColorPath;
			if (storeFolder->goods().quality() == 1)
			{
				frameColorPath = whiteFramePath;
			}
			else if (storeFolder->goods().quality() == 2)
			{
				frameColorPath = greenFramePath;
			}
			else if (storeFolder->goods().quality() == 3)
			{
				frameColorPath = blueFramePath;
			}
			else if (storeFolder->goods().quality() == 4)
			{
				frameColorPath = purpleFramePath;
			}
			else if (storeFolder->goods().quality() == 5)
			{
				frameColorPath = orangeFramePath;
			}
			else
			{
				frameColorPath = voidFramePath;
			}

			UIButton * Btn_storeItemFrame = UIButton::create();
			Btn_storeItemFrame->setTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
			Btn_storeItemFrame->setTouchEnable(true);
			Btn_storeItemFrame->setAnchorPoint(ccp(0.5f,0.5f));
			Btn_storeItemFrame->setPosition(ccp(Btn_storeItemFrame->getContentSize().width/2,Btn_storeItemFrame->getContentSize().height/2));
			Btn_storeItemFrame->setScale(M_Scale_Cur);
			Btn_storeItemFrame->setPressedActionEnabled(true);
			Btn_storeItemFrame->addReleaseEvent(this,coco_releaseselector(StoreHouseItem::StoreHouseItemEvent));
			this->addChild(Btn_storeItemFrame);

			UIImageView *ImageView_storeItem = UIImageView::create();
			//ImageView_storeItem->setTexture("res_ui/props_icon/cubuyi_n.png");
			if (strcmp(storeFolder->goods().icon().c_str(),"") == 0)
			{
				ImageView_storeItem->setTexture("res_ui/props_icon/cubuyi.png");
			}
			else
			{
				std::string _path = "res_ui/props_icon/";
				_path.append(storeFolder->goods().icon().c_str());
				_path.append(".png");
				ImageView_storeItem->setTexture(_path.c_str());
			}
			ImageView_storeItem->setAnchorPoint(ccp(0.5f,0.5f));
			ImageView_storeItem->setPosition(ccp(0,0));
			ImageView_storeItem->setScale(M_Scale_Cur);
			Btn_storeItemFrame->addChild(ImageView_storeItem);

			Label_num = UILabel::create();
			char s_num[5];
			sprintf(s_num,"%d",storeFolder->quantity());
			Label_num->setText(s_num);
			Label_num->setFontName(APP_FONT_NAME);
			Label_num->setFontSize(14);
			Label_num->setAnchorPoint(ccp(1.0f,0));
			Label_num->setPosition(ccp(25,-26));
			Btn_storeItemFrame->addChild(Label_num);
			
			if (storeFolder->goods().binding() == 1)
			{
				UIImageView *ImageView_bound = UIImageView::create();
				ImageView_bound->setTexture("res_ui/binding.png");
				ImageView_bound->setAnchorPoint(ccp(0,1.0f));
				ImageView_bound->setScale(M_Scale_Cur);
				ImageView_bound->setPosition(ccp(-25,26));
				Btn_storeItemFrame->addChild(ImageView_bound);
			}

			//如果是装备
			if(storeFolder->goods().has_equipmentdetail())
			{
				if (storeFolder->goods().equipmentdetail().gradelevel() > 0)  //精练等级
				{
					std::string ss_gradeLevel = "+";
					char str_gradeLevel [10];
					sprintf(str_gradeLevel,"%d",storeFolder->goods().equipmentdetail().gradelevel());
					ss_gradeLevel.append(str_gradeLevel);
					UILabel *Lable_gradeLevel = UILabel::create();
					Lable_gradeLevel->setText(ss_gradeLevel.c_str());
					Lable_gradeLevel->setFontName(APP_FONT_NAME);
					Lable_gradeLevel->setFontSize(14);
					Lable_gradeLevel->setAnchorPoint(ccp(1.0f,1.0f));
					Lable_gradeLevel->setPosition(ccp(25,26));
					Lable_gradeLevel->setName("Lable_gradeLevel");
					Btn_storeItemFrame->addChild(Lable_gradeLevel);
				}

				if (storeFolder->goods().equipmentdetail().starlevel() > 0)  //星级
				{
					char str_starLevel [10];
					sprintf(str_starLevel,"%d",storeFolder->goods().equipmentdetail().starlevel());
					UILabel *Lable_starLevel = UILabel::create();
					Lable_starLevel->setText(str_starLevel);
					Lable_starLevel->setFontName(APP_FONT_NAME);
					Lable_starLevel->setFontSize(14);
					Lable_starLevel->setAnchorPoint(ccp(0,0));
					Lable_starLevel->setPosition(ccp(-25,-26));
					Lable_starLevel->setName("Lable_starLevel");
					Btn_storeItemFrame->addChild(Lable_starLevel);

					UIImageView *ImageView_star = UIImageView::create();
					ImageView_star->setTexture("res_ui/star_on.png");
					ImageView_star->setAnchorPoint(ccp(0,0));
					ImageView_star->setPosition(ccp(Lable_starLevel->getContentSize().width,2));
					ImageView_star->setVisible(true);
					ImageView_star->setScale(0.5f);
					Lable_starLevel->addChild(ImageView_star);
				}
			}
		}
		else
		{
			this->cur_type = VOIDSTOREHOUSEITEM;
			//init(VOIDITEM);
			UIImageView *imageView_background = UIImageView::create();
			imageView_background->setTexture(voidFramePath);
			imageView_background->setAnchorPoint(ccp(.5f,.5f));
			imageView_background->setPosition(ccp(imageView_background->getContentSize().width/2,imageView_background->getContentSize().height/2));
			imageView_background->setScale(M_Scale_Cur);
			imageView_background->setName("imageView_background");
			this->addChild(imageView_background);
		}
		this->setTouchEnable(true);
		this->setSize(CCSizeMake(60,60));
		return true;
	}
	return false;
}

StoreHouseItem * StoreHouseItem::createLockedItem( int storeFolderIndex )
{
	StoreHouseItem * storeHouseItem = new StoreHouseItem();
	if (storeHouseItem && storeHouseItem->initLockedItem(storeFolderIndex))
	{
		storeHouseItem->autorelease();
		return storeHouseItem;
	}
	CC_SAFE_DELETE(storeHouseItem);
	return NULL;
}

bool StoreHouseItem::initLockedItem( int storeFolderIndex )
{
	if (UIWidget::init())
	{
		this->setSize(CCSizeMake(60,60));
		this->cur_type = LOCKSTOREHOUSEITEM;
		this->index = storeFolderIndex;

		UIButton * Btn_lockFrame = UIButton::create();
		Btn_lockFrame->setTextures(voidFramePath,voidFramePath,"");
		Btn_lockFrame->setTouchEnable(true);
		Btn_lockFrame->setAnchorPoint(ccp(0.5f,0.5f));
		Btn_lockFrame->setPosition(ccp(Btn_lockFrame->getContentSize().width/2,Btn_lockFrame->getContentSize().height/2));
		Btn_lockFrame->setScale(M_Scale_Cur);
		Btn_lockFrame->setPressedActionEnabled(true);
		Btn_lockFrame->addReleaseEvent(this,coco_releaseselector(StoreHouseItem::BtnLockEvent));
		this->addChild(Btn_lockFrame);

		//锁定图标
		UIImageView * imageView_lock = UIImageView::create();
		imageView_lock->setTexture("res_ui/suo.png");
		imageView_lock->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_lock->setPosition(ccp(0,0));
		imageView_lock->setName("imageView_lock");
		Btn_lockFrame->addChild(imageView_lock);

		return true;
	}
	return false;
}

void StoreHouseItem::StoreHouseItemEvent( CCObject * pSender )
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	StoreHouseInfo * storeHouseInfo = StoreHouseInfo::create(curStoreFolder);
	storeHouseInfo->ignoreAnchorPointForPosition(false);
	storeHouseInfo->setAnchorPoint(ccp(0.5f,0.5f));
	//storeHouseInfo->setPosition(ccp(winSize.width/2,winSize.height/2));
	GameView::getInstance()->getMainUIScene()->addChild(storeHouseInfo);
}

void StoreHouseItem::BtnLockEvent( CCObject * pSender )
{
	if (this->index >= (int)GameView::getInstance()->storeHouseItemList.size())
	{
// 		int num = this->index + 1 - GameView::getInstance()->storeHouseItemList.size();
// 		std::string dialog = "suer to open ";
// 		char * s = new char [5];
// 		sprintf(s,"%d",num);
// 		dialog.append(s);
// 		delete [] s;
// 		dialog.append(" folder ? ");
// 		GameView::getInstance()->showPopupWindow(dialog,2,this,coco_selectselector(StoreHouseItem::SureEvent),coco_selectselector(StoreHouseItem::CancelEvent));
		int num =  this->index + 1 - GameView::getInstance()->storeHouseItemList.size();
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1320,(void *)1,(void *)num);
	}
}

cocos2d::CCPoint StoreHouseItem::autoGetPosition( CCObject * pSender )
{
	return ccp(400,240);
}

void StoreHouseItem::SureEvent( CCObject * pSender )
{
	int num =  this->index + 1 - GameView::getInstance()->storeHouseItemList.size();
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1320,(void *)1,(void *)num);
}

void StoreHouseItem::CancelEvent( CCObject * pSender )
{

}
