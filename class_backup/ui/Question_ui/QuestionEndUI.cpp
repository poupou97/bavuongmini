#include "QuestionEndUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/CPropReward.h"
#include "QuestionData.h"
#include "QuestionRewardCardItem.h"
#include "QuestionUI.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../GameUIConstant.h"

QuestionEndUI::QuestionEndUI():
remainTime(30)
{
	//coordinate for card
	for (int m = 1;m>=0;m--)
	{
		for (int n = 4;n>=0;n--)
		{
			int _m = 1-m;
			int _n = 4-n;

			Coordinate temp;
			temp.x = 145+_n*94 ;
			temp.y = 185+m*119;

			Coordinate_item.push_back(temp);
		}
	}
}


QuestionEndUI::~QuestionEndUI()
{
}

QuestionEndUI * QuestionEndUI::create()
{
	QuestionEndUI * questionEndUI = new QuestionEndUI();
	if (questionEndUI && questionEndUI->init())
	{
		return questionEndUI;
	}
	CC_SAFE_DELETE(questionEndUI);
	return NULL;
}

bool QuestionEndUI::init()
{
	if(UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		
		remainNumber = QuestionData::instance()->get_cardNum();
		//����UI
// 		if(LoadSceneLayer::GeneralsFateInfoLayer->getWidgetParent() != NULL)
// 		{
// 			LoadSceneLayer::GeneralsFateInfoLayer->removeFromParentAndCleanup(false);
// 		}
// 		ppanel = LoadSceneLayer::GeneralsFateInfoLayer;
		UIPanel *ppanel = (UIPanel*)GUIReader::shareReader()->widgetFromJsonFile("res_ui/takeCard_1.json");
		ppanel->setAnchorPoint(ccp(0.0f,0.0f));
		ppanel->getValidNode()->setContentSize(CCSizeMake(588, 405));
		ppanel->setPosition(CCPointZero);
		ppanel->setTouchEnable(true);
		m_pUiLayer->addWidget(ppanel);

		UIButton * btn_close = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		btn_close->setTouchEnable( true );
		btn_close->setPressedActionEnabled(true);
		btn_close->addReleaseEvent(this,coco_releaseselector(QuestionEndUI::CloseEvent));

		l_remainTime = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_timeValue");
		std::string str_remainTime = "";
		char s_remainTime[20];
		sprintf(s_remainTime,"%d",remainTime);
		str_remainTime.append(s_remainTime);
		str_remainTime.append("s");
		l_remainTime->setText(str_remainTime.c_str());

		l_remainNumber = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_numberValue");
		char s_remainNumber[20];
		sprintf(s_remainNumber,"%d",remainNumber);
		l_remainNumber->setText(s_remainNumber);

		u_layer = UILayer::create();
		addChild(u_layer);

		this->schedule(schedule_selector(QuestionEndUI::update),1.0f);

		this->setContentSize(CCSizeMake(588,405));
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);

		return true;
	}
	return false;
}

void QuestionEndUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void QuestionEndUI::onExit()
{
	UIScene::onExit();
}

bool QuestionEndUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return true;
}

void QuestionEndUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void QuestionEndUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void QuestionEndUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void QuestionEndUI::CloseEvent( CCObject * pSender )
{
	QuestionUI * questionUI = (QuestionUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagQuestionUI);
	if (questionUI)
		questionUI->closeAnim();

	this->closeAnim();
}



void QuestionEndUI::AllCardTurnToBack()
{
	for(int i = 0;i<12;++i)
	{
		if (u_layer->getChildByTag(kTag_Base_RewardCardItem +i))
		{
			QuestionRewardCardItem * rewardCardItem = (QuestionRewardCardItem*)u_layer->getChildByTag(kTag_Base_RewardCardItem +i);
			rewardCardItem->FlopAnimationToBack();
		}
	}
}

void QuestionEndUI::BeginShuffle()
{
	CCPoint _pos = ccp(333,245);

	for(int i = 0;i<12;++i)
	{
		if (u_layer->getChildByTag(kTag_Base_RewardCardItem +i))
		{
			QuestionRewardCardItem * rewardCardItem = (QuestionRewardCardItem*)u_layer->getChildByTag(kTag_Base_RewardCardItem +i);
			CCFiniteTimeAction*  action1 = CCSequence::create(
				CCMoveTo::create(REWARD_CARD_MOVE_TIME,_pos),
				CCDelayTime::create(0.5f),
				//CCMoveTo::create(0.7f,ccp(Coordinate_card.at(random_value).x,Coordinate_card.at(random_value).y)),
				CCMoveTo::create(REWARD_CARD_MOVE_TIME,ccp(Coordinate_item.at(i).x,Coordinate_item.at(i).y)),
				CCCallFunc::create(rewardCardItem, callfunc_selector(QuestionRewardCardItem::setBtnTouchEnabled)),
				NULL);
			rewardCardItem->runAction(action1);
		}
	}
}

void QuestionEndUI::initReward(std::vector<CPropReward*> tempList)
{
	for(int i = 0;i<tempList.size();++i)
	{
		if (i >= 10)
			continue;

		QuestionRewardCardItem * rewardCardItem = QuestionRewardCardItem::create(tempList.at(i),i);
		rewardCardItem->ignoreAnchorPointForPosition(false);
		rewardCardItem->setAnchorPoint(ccp(0.5f,0.5f));
		rewardCardItem->setPosition(ccp(Coordinate_item.at(i).x,Coordinate_item.at(i).y));
		rewardCardItem->setTag(kTag_Base_RewardCardItem+i);
		u_layer->addChild(rewardCardItem);
	}

	CCFiniteTimeAction*  action1 = CCSequence::create(
		CCDelayTime::create(2.0f),
		CCCallFunc::create(this, callfunc_selector(QuestionEndUI::AllCardTurnToBack)),
		CCDelayTime::create(0.7f),
		CCCallFunc::create(this,callfunc_selector(QuestionEndUI::BeginShuffle)),
		NULL);

	this->stopAllActions();
	this->runAction(action1);
}

void QuestionEndUI::update( float delta )
{
	if (remainTime >0)
	{
		--remainTime;

		std::string str_remainTime = "";
		char s_remainTime[20];
		sprintf(s_remainTime,"%d",remainTime);
		str_remainTime.append(s_remainTime);
		str_remainTime.append("s");
		l_remainTime->setText(str_remainTime.c_str());

		char s_remainNumber[20];
		sprintf(s_remainNumber,"%d",remainNumber);
		l_remainNumber->setText(s_remainNumber);
	}
	else
	{
		//GameMessageProcessor::sharedMsgProcessor()->sendReq(1924);
		QuestionUI * questionUI = (QuestionUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagQuestionUI);
		if (questionUI)
			questionUI->closeAnim();

		this->closeAnim();
		
	}
}

void QuestionEndUI::set_RemainNumber( int _value )
{
	remainNumber = _value;
}

int QuestionEndUI::get_RemainNumber()
{
	return remainNumber;
}
