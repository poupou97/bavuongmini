#include "QuestionRewardCardItem.h"
#include "../../messageclient/element/CRewardProp.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "GameView.h"
#include "AppMacros.h"
#include "../../messageclient/element/CPropReward.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "QuestionData.h"
#include "../../utils/StaticDataManager.h"
#include "../GameUIConstant.h"

#define voidFramePath "res_ui/smdi_none.png"
#define whiteFramePath "res_ui/smdi_white.png"
#define greenFramePath "res_ui/smdi_green.png"
#define blueFramePath "res_ui/smdi_bule.png"
#define purpleFramePath "res_ui/smdi_purple.png"
#define orangeFramePath "res_ui/smdi_orange.png"


QuestionRewardCardItem::QuestionRewardCardItem():
isFloped(false),
grid(-1),
isFrontOrBack(true)
{
}


QuestionRewardCardItem::~QuestionRewardCardItem()
{
}

QuestionRewardCardItem * QuestionRewardCardItem::create( CPropReward *rewardProp,int index )
{
	QuestionRewardCardItem * questionRewardCardItem = new QuestionRewardCardItem();
	if (questionRewardCardItem && questionRewardCardItem->init(rewardProp,index))
	{
		questionRewardCardItem->autorelease();
		return questionRewardCardItem;
	}
	CC_SAFE_DELETE(questionRewardCardItem);
	return NULL;
}

bool QuestionRewardCardItem::init( CPropReward *rewardProp,int index )
{
	if (UIScene::init())
	{
		isFrontOrBack = true;
		grid = index;

		//���Ʊ���
		btn_backCardFrame = UIButton::create();
		btn_backCardFrame->setTextures("res_ui/instance_end/card.png","res_ui/instance_end/card.png","");
		btn_backCardFrame->setAnchorPoint(ccp(0.5f,0.5f));
		btn_backCardFrame->setPosition(ccp(0,0));
		btn_backCardFrame->setTouchEnable(false);
		btn_backCardFrame->addReleaseEvent(this,coco_releaseselector(QuestionRewardCardItem::BackCradEvent));
		m_pUiLayer->addWidget(btn_backCardFrame);
		btn_backCardFrame->setVisible(true);

		//��������
		btn_frontCardFrame = UIButton::create();
		btn_frontCardFrame->setTextures("res_ui/instance_end/card_get.png","res_ui/instance_end/card_get.png","");
		btn_frontCardFrame->setAnchorPoint(ccp(0.5f,0.5f));
		btn_frontCardFrame->setPosition(ccp(0,0));
		btn_backCardFrame->setTouchEnable(false);
		btn_frontCardFrame->addReleaseEvent(this,coco_releaseselector(QuestionRewardCardItem::FrontCradEvent));
		btn_frontCardFrame->setRotationY(0);
		m_pUiLayer->addWidget(btn_frontCardFrame);
		btn_frontCardFrame->setVisible(true);


		/*********************�ж���ɫ***************************/
		std::string frameColorPath;
		if (rewardProp->good().quality() == 1)
		{
			frameColorPath = whiteFramePath;
		}
		else if (rewardProp->good().quality() == 2)
		{
			frameColorPath = greenFramePath;
		}
		else if (rewardProp->good().quality() == 3)
		{
			frameColorPath = blueFramePath;
		}
		else if (rewardProp->good().quality() == 4)
		{
			frameColorPath = purpleFramePath;
		}
		else if (rewardProp->good().quality() == 5)
		{
			frameColorPath = orangeFramePath;
		}
		else
		{
			frameColorPath = voidFramePath;
		}

		//��
		imageView_frame = UIImageView::create();
		imageView_frame->setTexture(frameColorPath.c_str());
		imageView_frame->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_frame->setPosition(ccp(0,1));
		btn_frontCardFrame->addChild(imageView_frame);
		//����ͼƬ
		imageView_icon = UIImageView::create();
		std::string iconPath = "res_ui/props_icon/";
		iconPath.append(rewardProp->good().icon());
		iconPath.append(".png");
		imageView_icon->setTexture(iconPath.c_str());
		imageView_icon->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_icon->setPosition(ccp(0,1));
		imageView_icon->setScale(0.8f);
		btn_frontCardFrame->addChild(imageView_icon);
		//��������
		l_rewardName = UILabel::create();
		l_rewardName->setText(rewardProp->good().name().c_str());
		l_rewardName->setFontName(APP_FONT_NAME);
		l_rewardName->setFontSize(14);
		l_rewardName->setAnchorPoint(ccp(0.5f,0));
		l_rewardName->setPosition(ccp(0,-btn_frontCardFrame->getContentSize().width/2-7));
		l_rewardName->setColor(GameView::getInstance()->getGoodsColorByQuality(rewardProp->good().quality()));
		btn_frontCardFrame->addChild(l_rewardName);

		//��������
		l_roleName = UILabel::create();
		l_roleName->setText("");
		l_roleName->setFontName(APP_FONT_NAME);
		l_roleName->setFontSize(14);
		l_roleName->setAnchorPoint(ccp(0.5f,0));
		l_roleName->setPosition(ccp(0,btn_frontCardFrame->getContentSize().width/2-5));
		btn_frontCardFrame->addChild(l_roleName);

		this->setContentSize(btn_backCardFrame->getContentSize());
		return true;
	}
	return false;
}

void QuestionRewardCardItem::BackCradEvent( CCObject * pSender )
{
	if (isFloped)
		return;

	// 	//���ƣ������棩
	// 	this->FlopAnimationToFront();
	//������ 2805
	// 
	// 
	if (QuestionData::instance()->get_cardNum() <= 0)
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("QuestionUI_hasNoCardNumber"));
	}
	else
	{
		QuestionData::instance()->set_reward_index(grid);
		GameMessageProcessor::sharedMsgProcessor()->sendReq(2805);
		isFloped = true;
	}
}

void QuestionRewardCardItem::FrontCradEvent( CCObject * pSender )
{

}

//�ѿ��Ʒ������沢���ò��ɴ���
void QuestionRewardCardItem::FlopAnimationToFront()
{
	btn_backCardFrame->setZOrder(10);
	btn_frontCardFrame->setRotationY(-180);
	CCActionInterval*  orbit1 = CCOrbitCamera::create(REWARD_CARD_FLOP_DEGREE_90_TIME,1, 0, 0, -90, 0, 0);
	CCSequence*  action1 = CCSequence::create(
		CCShow::create(),
		orbit1,
		CCHide::create(),
		NULL);

	CCActionInterval*  orbit2 = CCOrbitCamera::create(REWARD_CARD_FLOP_DEGREE_180_TIME,1, 0, 0, 180, 0, 0);
	CCSequence*  action2 = CCSequence::create(
		CCShow::create(),
		orbit2,
		NULL);

	CCFiniteTimeAction* action_ToFront = CCSequence::create(action1,NULL);
	btn_backCardFrame->runAction(action_ToFront);
	CCFiniteTimeAction* action_ToBack = CCSequence::create(action2,NULL);
	btn_frontCardFrame->runAction(action_ToBack);

	isFrontOrBack = true;
	btn_frontCardFrame->setTouchEnable(false);
	btn_backCardFrame->setTouchEnable(false);
}

//�ѿ��Ʒ������沢���ò��ɴ���
void QuestionRewardCardItem::FlopAnimationToBack()
{
	CCActionInterval*  orbit1 = CCOrbitCamera::create(REWARD_CARD_FLOP_DEGREE_180_TIME,1, 0, 0, 180, 0, 0);
	CCSequence*  action1 = CCSequence::create(
		orbit1,
		NULL);

	CCActionInterval*  orbit2 = CCOrbitCamera::create(REWARD_CARD_FLOP_DEGREE_90_TIME,1, 0, 0, 90, 0, 0);
	CCSequence*  action2 = CCSequence::create(
		orbit2,
		CCHide::create(),
		NULL);

	CCFiniteTimeAction* action_ToBack = CCSequence::create(action1,NULL);
	btn_backCardFrame->runAction(action_ToBack);
	CCFiniteTimeAction* action_ToFront = CCSequence::create(action2,NULL);
	btn_frontCardFrame->runAction(action_ToFront);

	isFrontOrBack = false;
	btn_frontCardFrame->setTouchEnable(false);
	btn_backCardFrame->setTouchEnable(false);
}

void QuestionRewardCardItem::setBtnTouchEnabled()
{
	btn_backCardFrame->setTouchEnable(true);
	btn_frontCardFrame->setTouchEnable(false);
}

void QuestionRewardCardItem::RefreshCardInfo(GoodsInfo *goodsInfo,int amount)
{
	/*********************�ж���ɫ***************************/
	std::string frameColorPath;
	if (goodsInfo->quality() == 1)
	{
		frameColorPath = whiteFramePath;
	}
	else if (goodsInfo->quality() == 2)
	{
		frameColorPath = greenFramePath;
	}
	else if (goodsInfo->quality() == 3)
	{
		frameColorPath = blueFramePath;
	}
	else if (goodsInfo->quality() == 4)
	{
		frameColorPath = purpleFramePath;
	}
	else if (goodsInfo->quality() == 5)
	{
		frameColorPath = orangeFramePath;
	}
	else
	{
		frameColorPath = voidFramePath;
	}

	//��
	imageView_frame->setTexture(frameColorPath.c_str());
	//����ͼƬ
	std::string iconPath = "res_ui/props_icon/";
	iconPath.append(goodsInfo->icon());
	iconPath.append(".png");
	imageView_icon->setTexture(iconPath.c_str());
	//��������
	l_rewardName->setText(goodsInfo->name().c_str());
	l_rewardName->setColor(GameView::getInstance()->getGoodsColorByQuality(goodsInfo->quality()));

}