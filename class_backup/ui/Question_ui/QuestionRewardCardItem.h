
#ifndef _QUESTIONUI_REWARDCARDITEM_H_
#define _QUESTIONUI_REWARDCARDITEM_H_

#include "../extensions/UIScene.h"

class CPropReward;
class GoodsInfo;

/////////////////////////////////
/**
 * 答题结束后领奖UI中的卡牌项（只用于卡牌领奖）
 * @author yangjun
 * @version 0.1.0
 * @date 2014.05.19
 */

class QuestionRewardCardItem : public UIScene
{
public:
	QuestionRewardCardItem();
	~QuestionRewardCardItem();

	static QuestionRewardCardItem * create(CPropReward *rewardProp,int index);
	bool init(CPropReward *rewardProp,int index);

	void BackCradEvent(CCObject * pSender);
	void FrontCradEvent(CCObject * pSender);

	void FlopAnimationToFront();
	void FlopAnimationToBack();

	void setBtnTouchEnabled();

	void RefreshCardInfo(GoodsInfo *goodsInfo,int amount);

private:
	UIButton * btn_backCardFrame;
	UIButton * btn_frontCardFrame;
	UIImageView * imageView_frame;
	UIImageView * imageView_icon;
	UILabel * l_rewardName;
	UILabel * l_roleName;

	//格子位置
	int grid;
	//当前显示的正面(true)还是背面(false)
	bool isFrontOrBack;
	//是否已经翻开
	bool isFloped;
};

#endif