#include "QuestionIcon.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "QuestionUI.h"

#define  TAG_TUTORIALPARTICLE 600

QuestionIcon::QuestionIcon(void):
m_btn_questionIcon(NULL)
,m_layer_question(NULL)
,m_label_questionIcon_text(NULL)
,m_spirte_fontBg(NULL)
{

}


QuestionIcon::~QuestionIcon(void)
{
	
}

QuestionIcon* QuestionIcon::create()
{
	QuestionIcon * questionIcon = new QuestionIcon();
	if (questionIcon && questionIcon->init())
	{
		questionIcon->autorelease();
		return questionIcon;
	}
	CC_SAFE_DELETE(questionIcon);
	return NULL;
}

bool QuestionIcon::init()
{
	if (UIScene::init())
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

		// icon
		m_btn_questionIcon = UIButton::create();
		m_btn_questionIcon->setTouchEnable(true);
		m_btn_questionIcon->setPressedActionEnabled(true);
		//m_btn_signDailyIcon->setTextures("gamescene_state/zhujiemian2/tubiao/OnlineAwards.png", "gamescene_state/zhujiemian2/tubiao/OnlineAwards.png","");
		m_btn_questionIcon->setTextures("res_ui/meiriqiandao.png", "res_ui/meiriqiandao.png","");
		m_btn_questionIcon->setAnchorPoint(ccp(0.5f, 0.5f));
		m_btn_questionIcon->setPosition(ccp( winSize.width - 70 - 159 - 80 - 100, winSize.height-70));
		m_btn_questionIcon->addReleaseEvent(this, coco_cancelselector(QuestionIcon::callBackBtnQuestion));

		// label
		m_label_questionIcon_text = UILabel::create();
		m_label_questionIcon_text->setText("bu ke qian dao");
		m_label_questionIcon_text->setColor(ccc3(255, 246, 0));
		m_label_questionIcon_text->setFontSize(14);
		m_label_questionIcon_text->setAnchorPoint(ccp(0.5f, 0.5f));
		m_label_questionIcon_text->setPosition(ccp(m_btn_questionIcon->getPosition().x, m_btn_questionIcon->getPosition().y - m_btn_questionIcon->getContentSize().height / 2 - 5));


		// text_bg
		m_spirte_fontBg = CCScale9Sprite::create(CCRectMake(5, 10, 1, 4) , "res_ui/zhezhao70.png");
		m_spirte_fontBg->setPreferredSize(CCSize(47, 14));
		m_spirte_fontBg->setAnchorPoint(ccp(0.5f, 0.5f));
		m_spirte_fontBg->setPosition(m_label_questionIcon_text->getPosition());


		m_layer_question = UILayer::create();
		m_layer_question->setTouchEnabled(true);
		m_layer_question->setAnchorPoint(CCPointZero);
		m_layer_question->setPosition(CCPointZero);
		m_layer_question->setContentSize(winSize);

		m_layer_question->addChild(m_spirte_fontBg, -10);
		m_layer_question->addWidget(m_btn_questionIcon);
		m_layer_question->addWidget(m_label_questionIcon_text);

		addChild(m_layer_question);

		this->setContentSize(winSize);

		return true;
	}
	return false;
}

void QuestionIcon::callBackBtnQuestion( CCObject *obj )
{

	// 初始化 UI WINDOW数据
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

	//SignDailyUI* pTmpSignDailyUI = (SignDailyUI *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagSignDailyUI);
	//if (NULL == pTmpSignDailyUI)
	//{
	//	SignDailyUI * pSignDailyUI = SignDailyUI::create();
	//	pSignDailyUI->ignoreAnchorPointForPosition(false);
	//	pSignDailyUI->setAnchorPoint(ccp(0.5f, 0.5f));
	//	pSignDailyUI->setPosition(ccp(winSize.width / 2, winSize.height / 2));
	//	pSignDailyUI->setTag(kTagSignDailyUI);
	//	//pSignDailyUI->refreshUI();

	//	GameView::getInstance()->getMainUIScene()->addChild(pSignDailyUI);
	//}

	QuestionUI * pTmpQuestionUI = (QuestionUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagQuestionUI);
	if (NULL == pTmpQuestionUI)
	{
		QuestionUI * pQuestionUI = QuestionUI::create();
		pQuestionUI->ignoreAnchorPointForPosition(false);
		pQuestionUI->setAnchorPoint(ccp(0.5f, 0.5f));
		pQuestionUI->setPosition(ccp(winSize.width / 2, winSize.height / 2));
		pQuestionUI->setTag(kTagQuestionUI);
		pQuestionUI->requestQuestion();				// 向服务器请求题目

		GameView::getInstance()->getMainUIScene()->addChild(pQuestionUI);
	}
}

void QuestionIcon::onEnter()
{
	UIScene::onEnter();
}

void QuestionIcon::onExit()
{
	UIScene::onExit();
}






