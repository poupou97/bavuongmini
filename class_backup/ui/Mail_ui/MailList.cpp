#include "MailList.h"
#include "MailUI.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../extensions/CCMoveableMenu.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../messageclient/element/CMailInfo.h"
#include "../extensions/CCRichLabel.h"
#include "../../utils/StaticDataManager.h"
#include "AppMacros.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/GameUtils.h"

// the mail in the mailbox will be deleted after some days
#define MAIL_KEEP_TIME_NORMAL 14   // day
#define MAIL_KEEP_TIME_WITH_ATTACHMENT 30
#define  MAIL_KEEP_TIME_WITH_NEEDTOPAY 2

#define TOTAL_SECOND_FOR_ONE_DAY 86400

MailList::MailList(void)
{
	lastSelectCellId =0;
}


MailList::~MailList(void)
{
}

MailList * MailList::create()
{
	MailList * mailList=new MailList();
	mailList->autorelease();
	return mailList;

}

bool MailList::init()
{
	if (UIScene::init())
	{
		CCSize winsize =CCDirector::sharedDirector()->getVisibleSize();

		tableView = CCTableView::create(this, CCSizeMake(270, 360));
		tableView->setSelectedEnable(true);
		tableView->setSelectedScale9Texture("res_ui/highlight.png", CCRectMake(25, 24, 1, 1), ccp(0,0));
		tableView->setDirection(kCCScrollViewDirectionVertical);
		tableView->setAnchorPoint(ccp(0,0));
		tableView->setPosition(ccp(0,0));
		tableView->setDelegate(this);
		tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		tableView->setPressedActionEnabled(true);
		addChild(tableView,1,MAILTABTAG);
		tableView->reloadData();

// 		pageView_kuang = CCScale9Sprite::create("res_ui/LV5_dikuang_miaobian1.png");
// 		pageView_kuang->setPreferredSize(CCSizeMake(287,376));
// 		pageView_kuang->setCapInsets(CCRectMake(32,32,1,1));
// 		pageView_kuang->setAnchorPoint(ccp(0,0));
// 		pageView_kuang->setPosition(ccp(-7,-10));
// 		addChild(pageView_kuang,2);

		this->setContentSize(CCSizeMake(270,360));
		return true;
	}
	return false;
}

void MailList::onEnter()
{
	UIScene::onEnter();
}

void MailList::onExit()
{
	UIScene::onExit();
}

void MailList::scrollViewDidScroll( cocos2d::extension::CCScrollView * view )
{
}

void MailList::scrollViewDidZoom( cocos2d::extension::CCScrollView* view )
{	
}

void MailList::tableCellHighlight(CCTableView* table, CCTableViewCell* cell)
{
}
void MailList::tableCellUnhighlight(CCTableView* table, CCTableViewCell* cell)
{
}

void MailList::tableCellTouched( cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell )
{
	int cellIndex_ = cell->getIdx();
	MainScene * mainscene =(MainScene *) GameView::getInstance()->getMainUIScene();
	MailUI *mailUI=(MailUI *)mainscene->getChildByTag(kTagMailUi);
	long long mailId_ = mailUI->sourceDataVector.at(cellIndex_)->id();
	mailUI->sourceDataVector.at(cellIndex_)->set_flag(1);
	mailUI->operationType = 1;
	mailUI->mailList_Id = cellIndex_;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(2002,(void *)1,(void *)mailId_);
	
	this->reSetRemind(mailId_);

	CCSprite * cellSp_ =(CCSprite *)cell->getChildByTag(mailStateFlag);
	cellSp_->setVisible(false);
	mailUI->showMailInfo(cellIndex_);
}

cocos2d::CCSize MailList::tableCellSizeForIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	return CCSizeMake(270,63);
}

cocos2d::extension::CCTableViewCell* MailList::tableCellAtIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	CCString *string =CCString::createWithFormat("%d",idx);
	CCTableViewCell * cell=table->dequeueCell();
	cell=new CCTableViewCell();
	cell->autorelease();

	MailUI *mailUI=(MailUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMailUi);
	//mailUI->sourceDataVector.at(0)->set_flag(1);
	cell = MailListCell::create(idx);
	//this->reSetRemind(mailUI->sourceDataVector.at(0)->id());

	if (idx == mailUI->sourceDataVector.size() - 3)
	{
		if (mailUI->isHasNextPage ==true)
		{
			mailUI->curMailPage++;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2001,(void*)mailUI->curMailPage);
		}
	}
	return cell;
}

unsigned int MailList::numberOfCellsInTableView( cocos2d::extension::CCTableView *table )
{
	MailUI *mailUI=(MailUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMailUi);
	return mailUI->sourceDataVector.size();
}

void MailList::ccTouchMoved( CCTouch *pTouch, CCEvent *pEvent )
{

}

void MailList::ccTouchEnded( CCTouch *pTouch, CCEvent *pEvent )
{

}

void MailList::ccTouchCancelled( CCTouch *pTouch, CCEvent *pEvent )
{

}

void MailList::reSetRemind(long long mailId)
{
	MainScene * mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();

	for (unsigned int mailIndex =0;mailIndex<GameView::getInstance()->mailVectorOfSystem.size();mailIndex++)
	{
		if (mailId == GameView::getInstance()->mailVectorOfSystem.at(mailIndex)->id())
		{
			GameView::getInstance()->mailVectorOfSystem.at(mailIndex)->set_flag(1);
		}
	}
	/*
	for (unsigned int mailIndex =0;mailIndex<GameView::getInstance()->mailVectorOfPlayer.size();mailIndex++)
	{
		if (mailId == GameView::getInstance()->mailVectorOfPlayer.at(mailIndex)->id())
		{
			GameView::getInstance()->mailVectorOfPlayer.at(mailIndex)->set_flag(1);
		}
	}
	*/
	mainscene->remindMail();
}

//////////////////////////////////////////////////////////
MailListCell::MailListCell(void)
{
}


MailListCell::~MailListCell(void)
{
}

MailListCell * MailListCell::create(int idx)
{
	MailListCell * cell=new MailListCell();
	if (cell && cell->init(idx))
	{
		cell->autorelease();
		return cell;
	}
	CC_SAFE_DELETE(cell);
	return NULL;
}

bool MailListCell::init(int idx)
{
	if (CCTableViewCell::init())
	{
		MailUI * maillUI= (MailUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMailUi);
		MailList * mailList =(MailList *)maillUI->mailList;

		CCScale9Sprite * imageBackGround = CCScale9Sprite::create("res_ui/kuang01_new.png");
		imageBackGround->setPreferredSize(CCSizeMake(270,63));
		imageBackGround->setCapInsets(CCRect(15,30,1,1));
		imageBackGround->setAnchorPoint(ccp(0,0));
		imageBackGround->setPosition(ccp(0,0));
		addChild(imageBackGround);

		const char *str_Source_ = StringDataManager::getString("mail_sendSource");
		const char *str_MailPlayerof = StringDataManager::getString("mail_sendsourceOfMail");
		std::string text_label =maillUI->sourceDataVector.at(idx)->sendername();

		ccColor3B shadowColor = ccc3(0,0,0);
		CCLabelTTF  * labelSource_=CCLabelTTF::create(str_Source_,APP_FONT_NAME,20);
		labelSource_->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		labelSource_->setAnchorPoint(ccp(0,0));
		labelSource_->setPosition(ccp(40,25));
		addChild(labelSource_,10);

		ccColor3B SourceName;
		if (/*maillUI->curTabType == 0*/maillUI->sourceDataVector.at(idx)->sender() == LLONG_MAX)
		{
			SourceName = ccc3(255,0,1);
		}else
		{
			SourceName = ccc3(60,255,0);
		}
		
		CCLabelTTF * labelSourceName_  =CCLabelTTF::create();
		labelSourceName_->setAnchorPoint(ccp(0,0));
		labelSourceName_->setPosition(ccp(labelSource_->getPosition().x+ labelSource_->getContentSize().width,25));
		labelSourceName_->setFontSize(20);
		labelSourceName_->setColor(SourceName);
		labelSourceName_->setString(text_label.c_str());
		addChild(labelSourceName_,10);

		CCLabelTTF  * labelContentStr_=CCLabelTTF::create(str_MailPlayerof,APP_FONT_NAME,20);
		labelContentStr_->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		labelContentStr_->setAnchorPoint(ccp(0,0));
		labelContentStr_->setPosition(ccp(labelSourceName_->getPosition().x+ labelSourceName_->getContentSize().width,25));
		addChild(labelContentStr_,10);

		CCSprite * mailFlag=CCSprite::create("res_ui/newMailFlag.png");
		mailFlag->setAnchorPoint(ccp(0,0));
		mailFlag->setPosition(ccp(20,30));
		mailFlag->setVisible(false);
		mailFlag->setTag(mailStateFlag);
		addChild(mailFlag);

		// show the remain time
		// calc the remain day
		long long sendtime = maillUI->sourceDataVector.at(idx)->sendtime();
		int elapseDay = (GameUtils::getDateSecond() - sendtime / 1000) / TOTAL_SECOND_FOR_ONE_DAY;
		int keepDay = MAIL_KEEP_TIME_NORMAL;
		if(maillUI->sourceDataVector.at(idx)->attachment_size() > 0 )   // with attachment
		{
			if (maillUI->sourceDataVector.at(idx)->goldingot() > 0 || maillUI->sourceDataVector.at(idx)->gold() > 0)
			{
				keepDay = MAIL_KEEP_TIME_WITH_NEEDTOPAY;
			}else
			{
				keepDay = MAIL_KEEP_TIME_WITH_ATTACHMENT;
			}
		}
		int remain_day = keepDay - elapseDay;
		if(remain_day <= 0)
			remain_day = 1;
	
		char remainTimeStr[20];
		const char* templateStr = StringDataManager::getString("mail_remaintime");
		sprintf(remainTimeStr, templateStr, remain_day);

		CCLabelTTF  * labelRemainTime = CCLabelTTF::create(remainTimeStr,APP_FONT_NAME,16);
		labelRemainTime->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		labelRemainTime->setAnchorPoint(ccp(0,0));
		labelRemainTime->setPosition(ccp(40,5));
		addChild(labelRemainTime, 10);

		if (maillUI->sourceDataVector.at(idx)->flag() == 0 || maillUI->sourceDataVector.at(idx)->flag() == 3)
		{
			mailFlag->setVisible(true);
		}

		if (maillUI->sourceDataVector.at(idx)->attachment_size()>0 && maillUI->sourceDataVector.at(idx)->attachment(0).state()==0)
		{
			CCSprite * mailAttach=CCSprite::create("res_ui/youjian/kejierenwuup.png");
			mailAttach->setAnchorPoint(ccp(0,0));
			mailAttach->setPosition(ccp(230,20));
			mailAttach->setTag(mailAttachmentStateFlag);
			addChild(mailAttach);
		}

		return true;
	}
	return false;
}

void MailListCell::onEnter()
{
	CCTableViewCell::onEnter();
}

void MailListCell::onExit()
{
	CCTableViewCell::onExit();
}