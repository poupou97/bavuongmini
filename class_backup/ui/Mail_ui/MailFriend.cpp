#include "MailFriend.h"
#include "GameView.h"
#include "../Friend_ui/FriendInfo.h"
#include "MailUI.h"
#include "../extensions/CCMoveableMenu.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "AppMacros.h"
#include "../../messageclient/element/CRelationPlayer.h"

MailFriend::MailFriend(void)
{
	setFriendsPage =0;
	uitag_ = 0;
}


MailFriend::~MailFriend(void)
{
	std::vector<CRelationPlayer *>::iterator iter_relation;
	for (iter_relation=GameView::getInstance()->relationSourceVector.begin();iter_relation!=GameView::getInstance()->relationSourceVector.end();iter_relation++)
	{
		delete *iter_relation;
	}
	GameView::getInstance()->relationSourceVector.clear();
}

MailFriend* MailFriend::create(int usedUiTag)
{
	MailFriend * mailfriend=new MailFriend();
	if (mailfriend && mailfriend->init(usedUiTag))
	{
		mailfriend->autorelease();
		return mailfriend;
	}
	CC_SAFE_DELETE(mailfriend);
	return NULL;
}

bool MailFriend::init(int usedUiTag)
{
	if (UIScene::init())
	{
		UIPanel * panel=LoadSceneLayer::mailFriendPopup;
		panel->setAnchorPoint(ccp(0,0));
		panel->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(panel);

		uitag_ = usedUiTag;

		CCTableView* tableView = CCTableView::create(this, CCSizeMake(675,310));
		tableView->setDirection(kCCScrollViewDirectionVertical);
		tableView->setAnchorPoint(ccp(0,0));
		tableView->setPosition(ccp(36,48));
		tableView->setDelegate(this);
		tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		addChild(tableView,0,MAILFRIENDTABVIEW);
		tableView->reloadData();

// 		CCScale9Sprite * pageView_kuang = CCScale9Sprite::create("res_ui/LV5_dikuang_miaobian1.png");
// 		pageView_kuang->setPreferredSize(CCSizeMake(658,330));
// 		pageView_kuang->setCapInsets(CCRectMake(32,32,1,1));
// 		pageView_kuang->setAnchorPoint(ccp(0,0));
// 		pageView_kuang->setPosition(ccp(53,41));
// 		addChild(pageView_kuang,1);

		labelBM_isNotFriend = (UILabelBMFont *)UIHelper::seekWidgetByName(panel,"LabelBMFont_friendsIsNull");
		labelBM_isNotFriend->setVisible(false);
		
		UILayer * layer_ = UILayer::create();
		addChild(layer_,2);
		UIButton * button_close= UIButton::create();
		button_close->setTextures("res_ui/close.png","res_ui/close.png","");
		button_close->setTouchEnable(true);
		button_close->setPressedActionEnabled(true);
		button_close->addReleaseEvent(this, coco_releaseselector(MailFriend::callBackExit));
		button_close->setAnchorPoint(ccp(0.5f,0.5f));
		//button_close->setPosition(ccp(panel->getContentSize().width,panel->getContentSize().height));
		button_close->setPosition(ccp(718,372));
		layer_->addWidget(button_close);

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(panel->getContentSize());

		return true;
	}
	return false;
}

void MailFriend::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void MailFriend::onExit()
{
	UIScene::onExit();
}

void MailFriend::tableCellTouched( cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell )
{
}

cocos2d::CCSize MailFriend::tableCellSizeForIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	return CCSizeMake(640,80);
}

cocos2d::extension::CCTableViewCell* MailFriend::tableCellAtIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	CCString *string = CCString::createWithFormat("%d", idx);
	CCTableViewCell *cell =table->dequeueCell();// this method must be called

	cell=new CCTableViewCell();
	cell->autorelease();
	int friendNum = GameView::getInstance()->relationSourceVector.size();
	if (idx == friendNum/2)
	{
		if (friendNum%2 == 0)
		{
			FriendInfo * friendInfoL =FriendInfo::create(2*idx,uitag_);
			friendInfoL->setAnchorPoint(ccp(0.5f,0.5f));
			friendInfoL->setPosition(ccp(-5,-10));
			friendInfoL->setScale(0.95f);
			cell->addChild(friendInfoL);

			FriendInfo * friendInfoR =FriendInfo::create(2*idx+1,uitag_);
			friendInfoR->setAnchorPoint(ccp(0.5f,0.5f));
			friendInfoR->setPosition(ccp(315,-10));
			friendInfoR->setScale(0.95f);
			cell->addChild(friendInfoR);
		}else
		{
			FriendInfo * friendInfoL =FriendInfo::create(2*idx,uitag_);
			friendInfoL->setAnchorPoint(ccp(0.5f,0.5f));
			friendInfoL->setPosition(ccp(-5,-10));
			friendInfoL->setScale(0.95f);
			cell->addChild(friendInfoL);
		}
	}
	else
	{
		FriendInfo * friendInfoL =FriendInfo::create(2*idx,uitag_);
		friendInfoL->setAnchorPoint(ccp(0.5f,0.5f));
		friendInfoL->setPosition(ccp(-5,-10));
		friendInfoL->setScale(0.95f);
		cell->addChild(friendInfoL);

		FriendInfo * friendInfoR =FriendInfo::create(2*idx+1,uitag_);
		friendInfoR->setAnchorPoint(ccp(0.5f,0.5f));
		friendInfoR->setScale(0.95f);
		friendInfoR->setPosition(ccp(315,-10));
		cell->addChild(friendInfoR);
	}
	
	if(idx == friendNum/2 - 2)
	{
		if (setFriendsPage < totalFriendsPageNum - 1)
		{
			setFriendsPage++;
			FriendStruct friend1={0,20,setFriendsPage,0};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2202,&friend1);
		}
	}

	return cell;
}

unsigned int MailFriend::numberOfCellsInTableView( cocos2d::extension::CCTableView *table )
{
	int friendnum =GameView::getInstance()->relationSourceVector.size();

	if (friendnum%2 == 0)
	{
		return friendnum/2;
	}else
	{
		return friendnum/2+1;
	}
}

void MailFriend::scrollViewDidScroll( cocos2d::extension::CCScrollView * view )
{
	
}

void MailFriend::scrollViewDidZoom( cocos2d::extension::CCScrollView* view )
{

}

bool MailFriend::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{

	return resignFirstResponder(pTouch,this,false);
}

void MailFriend::ccTouchMoved( CCTouch *pTouch, CCEvent *pEvent )
{

}

void MailFriend::ccTouchEnded( CCTouch *pTouch, CCEvent *pEvent )
{

}

void MailFriend::ccTouchCancelled( CCTouch *pTouch, CCEvent *pEvent )
{

}

void MailFriend::callBackExit( CCObject * obj )
{
	this->closeAnim();
}

void MailFriend::setShowBMfriendIsNull( bool value_ )
{
	labelBM_isNotFriend->setVisible(value_);
}




