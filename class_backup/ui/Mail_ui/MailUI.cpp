#include "MailUI.h"
#include "../extensions/UITab.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "GameView.h"
#include "../../messageclient/element/CMailInfo.h"
#include "../backpackscene/PackageItem.h"
#include "../backpackscene/PackageScene.h"
#include "../extensions/PopupWindow.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "MailFriend.h"
#include "../extensions/Counter.h"
#include "../../messageclient/element/CAttachedProps.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../backpackscene/PacPageView.h"
#include "../extensions/CCRichLabel.h"
#include "../extensions/RichTextInput.h"
#include "MailList.h"
#include "../../messageclient/element/CMailAttachment.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../backpackscene/GoodsItemInfoBase.h"
#include "../../utils/StaticDataManager.h"
#include "../goldstore_ui/GoldStoreUI.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "AppMacros.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "../../messageclient/element/CShortCut.h"
#include "../../messageclient/element/CDrug.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutSlot.h"
#include "../../utils/GameUtils.h"
#include "../../utils/StrUtils.h"
#include "../extensions/FloderItem.h"

using namespace CocosDenshion;

MailUI::MailUI(void)
{
	mailList_Id=0;
	selectAnnexNum=0;
	mail_attactMent=false;
	firstIsNull=true;
	mailAttachment_amount=0;
	curMailPage = 0;
	isHasNextPage = false;
	operationType =0;
	curTabType= 0;

	sendMailCollectcoins = 0;
	sendMailCollectinIngots = 0;
}


MailUI::~MailUI(void)
{
	std::vector<CMailInfo *>::iterator iter;
	for (iter=sourceDataVector.begin();iter!=sourceDataVector.end();iter++)
	{
		delete *iter;
	}
	sourceDataVector.clear();

	FloderItemInstance * mailAnnexui1 =(FloderItemInstance *)sendMailLayer->getChildByTag(REDUCTIONANNEXBUTTON1);
	if (mailAnnexui1 != NULL)
	{
		reductionAnnex1(NULL);
	}

	FloderItemInstance * mailAnnexui2 =(FloderItemInstance *)sendMailLayer->getChildByTag(REDUCTIONANNEXBUTTON2);
	if (mailAnnexui2 != NULL)
	{
		reductionAnnex2(NULL);
	}
}

MailUI * MailUI::create()
{
	MailUI * mailui=new MailUI();
	if (mailui && mailui->init())
	{
		mailui->autorelease();
		return mailui;
	}
	CC_SAFE_DELETE(mailui);
	return NULL;
}

bool MailUI::init()
{
	if (UIScene::init())
	{

		CCSize size=CCDirector::sharedDirector()->getVisibleSize();

		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(size);
		mengban->setAnchorPoint(ccp(0,0));
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);

		if(LoadSceneLayer::mailPanel->getWidgetParent() != NULL)
		{
			LoadSceneLayer::mailPanel->removeFromParentAndCleanup(false);
		}

		UIPanel * ppanel = LoadSceneLayer::mailPanel;
		ppanel->setAnchorPoint(ccp(0.5f,0.5f));
		ppanel->setPosition(ccp(size.width/2,size.height/2));
		m_pUiLayer->addWidget(ppanel);
	
		layer_= UILayer::create();
		layer_->ignoreAnchorPointForPosition(false);
		layer_->setAnchorPoint(ccp(0.5f,0.5f));
		layer_->setPosition(ccp(size.width/2,size.height/2));
		layer_->setContentSize(CCSizeMake(800,480));
		this->addChild(layer_,0,MAILADDLAYER);

		const char * secondStr = StringDataManager::getString("UIName_mail_you");
		const char * thirdStr = StringDataManager::getString("UIName_mial_jian");
		CCArmature * atmature = MainScene::createPendantAnm("",secondStr,thirdStr,"");
		atmature->setPosition(ccp(30,240));
		layer_->addChild(atmature);

		currentPage = (UIImageView *)UIHelper::seekWidgetByName(ppanel,"ImageView_dianon");
		/////////backpage
		pageView = GameView::getInstance()->pacPageView;
		pageView->getPageView()->addPageTurningEvent(this,coco_PageView_PageTurning_selector(MailUI::CurrentPageViewChanged));
		pageView->setPosition(ccp(396,97));
		layer_->addChild(pageView,0);
		pageView->setVisible(false);
		pageView->setCurUITag(kTagMailUi);
		CCSequence* seq = CCSequence::create(CCDelayTime::create(0.05f),CCCallFunc::create(this,callfunc_selector(MailUI::PageScrollToDefault)),NULL);
		pageView->runAction(seq);
		//CurrentPageViewChanged(pageView);
		pageView->checkCDOnBegan();

		//refresh cd
		ShortcutLayer * shortCutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
		if (shortCutLayer)
		{
			for (int i = 0;i<7;++i)  //一共7个快捷栏
			{
				if (i < GameView::getInstance()->shortCutList.size())
				{
					CShortCut * c_shortcut = GameView::getInstance()->shortCutList.at(i);
					if (c_shortcut->storedtype() == 2)    //快捷栏中是物品
					{
						if(CDrug::isDrug(c_shortcut->skillpropid()))
						{
							if (shortCutLayer->getChildByTag(shortCutLayer->shortcurTag[c_shortcut->index()]+100))
							{
								ShortcutSlot * shortcutSlot = (ShortcutSlot*)shortCutLayer->getChildByTag(shortCutLayer->shortcurTag[c_shortcut->index()]+100);
								this->pageView->RefreshCD(c_shortcut->skillpropid());
							}
						}
					}
				}
			}
		}

		const char *str_shouxin = StringDataManager::getString("mail_uitab_shouxin_1");
		char *shouxin_left =const_cast<char*>(str_shouxin);

		const char *str_xiexin = StringDataManager::getString("mail_uitab_xiexin_2");
		char *xiexin_left=const_cast<char*>(str_xiexin);


		const char * highLightImage = "res_ui/tab_1_on.png";
		char * labelChange[] ={shouxin_left,xiexin_left};
		//channelLabel= UITab::createWithImage(2,"res_ui/tab_1_off.png","res_ui/tab_1_on.png","",labelChange,HORIZONTAL,5);
		channelLabel= UITab::createWithText(2,"res_ui/tab_1_off.png","res_ui/tab_1_on.png","",labelChange,HORIZONTAL,5);
		channelLabel->setAnchorPoint(ccp(0.5f,0.5f));
		channelLabel->setPosition(ccp(70,413));
		channelLabel->setPressedActionEnabled(true);
		channelLabel->setHighLightImage((char * )highLightImage);
		channelLabel->setDefaultPanelByIndex(0);
		channelLabel->addIndexChangedEvent(this,coco_indexchangedselector(MailUI::callBackChangeMail));
		layer_->addWidget(channelLabel);
		/*
		const char * highImage = "res_ui/tab_b.png";
		char * labelMail[] ={"res_ui/youjian/xitong.png","res_ui/youjian/wanjia.png"};
		channelLabelMail= UITab::createWithImage(2,"res_ui/tab_b_off.png","res_ui/tab_b.png","",labelMail,VERTICAL,8);
		channelLabelMail->setPressedActionEnabled(true);
		channelLabelMail->setAnchorPoint(ccp(0,0));
		channelLabelMail->setPosition(ccp(758,405));
		channelLabelMail->setHighLightImage((char * )highImage);
		channelLabelMail->setDefaultPanelByIndex(0);
		channelLabelMail->addIndexChangedEvent(this,coco_indexchangedselector(MailUI::callBackChangeMailSender));
		layer_->addWidget(channelLabelMail);
		*/
		UIButton * button_close = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		button_close->setTouchEnable(true);
		button_close->setPressedActionEnabled(true);
		button_close->addReleaseEvent(this,coco_releaseselector(MailUI::callbackClose));
		//receive mail
		getMailLayer=UILayer::create();
		getMailLayer->ignoreAnchorPointForPosition(false);
		getMailLayer->setAnchorPoint(ccp(0.5f,0.5f));
		getMailLayer->setContentSize(CCSizeMake(800,480));
		getMailLayer->setPosition(ccp(size.width/2,size.height/2));
		addChild(getMailLayer,1,TAGMAILLAYER);
		readPanel = (UIPanel*)UIHelper::seekWidgetByName(ppanel,"Panel_shouxin");

		extractAnnex= (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_tiqufujian");
		extractAnnex->setPressedActionEnabled(true);
		extractAnnex->setTouchEnable(true);
		extractAnnex->addReleaseEvent(this, coco_releaseselector(MailUI::callBackExtractAnnex));

		UIButton*deleteMail= (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_shanchu");
		deleteMail->setTouchEnable(true);
		deleteMail->setPressedActionEnabled(true);
		deleteMail->addReleaseEvent(this, coco_releaseselector(MailUI::callBackDeleteMail));

		replyMail= (UIButton *)UIHelper::seekWidgetByName(ppanel,"Button_huixin");
		replyMail->setTouchEnable(true);
		replyMail->setPressedActionEnabled(true);
		replyMail->addReleaseEvent(this, coco_releaseselector(MailUI::callBackReplyMail));
		replyMail->setVisible(false);

		UIImageView * mailTittleBg = (UIImageView *)UIHelper::seekWidgetByName(ppanel,"ImageView_biaotishurukuang");
		mailTittleBg->removeAllChildren();
		mailTitle=UILabel::create();
		mailTitle->setAnchorPoint(ccp(0,0));
		mailTitle->setPosition(ccp(8,8));
		mailTitle->setFontSize(16);
		mailTittleBg->addChild(mailTitle);
	
		m_scrollView = UIScrollView::create();
		m_scrollView->setTouchEnable(true);
		m_scrollView->setDirection(SCROLLVIEW_DIR_VERTICAL);
		m_scrollView->setSize(CCSizeMake(285,150));
		m_scrollView->setPosition(ccp(440,190));
		m_scrollView->setBounceEnabled(true);
		m_scrollView->setZOrder(10);
		getMailLayer->addWidget(m_scrollView);

		mailContentPanel = UIPanel::create();
		mailContentPanel->setAnchorPoint(ccp(0,0));
		mailContentPanel->setPosition(ccp(0,0));
		mailContentPanel->setSize(CCSizeMake(274,362));
		mailContentPanel->setTouchEnable(true);
		mailContentPanel->setZOrder(10);
		m_scrollView->addChild(mailContentPanel);

		image_mailItem_1 = (UIImageView *)UIHelper::seekWidgetByName(ppanel,"ImageView_fubenkuang_Clone");
		image_mailItem_2 = (UIImageView *)UIHelper::seekWidgetByName(ppanel,"ImageView_fubenkuang");
		image_mailItem_1->setVisible(true);
		image_mailItem_2->setVisible(true);
		///send mail
		sendMailLayer=UILayer::create();
		sendMailLayer->ignoreAnchorPointForPosition(false);
		sendMailLayer->setAnchorPoint(ccp(0.5f,0.5f));
		sendMailLayer->setPosition(ccp(size.width/2,size.height/2));
		sendMailLayer->setContentSize(CCSizeMake(800,480));
		addChild(sendMailLayer,1,TAGSENDMAILLAYER);
		
// 		UIImageView * pageView_kuang = UIImageView::create();
// 		pageView_kuang->setTexture("res_ui/LV5_dikuang_miaobian1.png");
// 		pageView_kuang->setScale9Enable(true);
// 		pageView_kuang->setScale9Size(CCSizeMake(363,323));
// 		pageView_kuang->setCapInsets(CCRectMake(30,30,1,1));
// 		pageView_kuang->setAnchorPoint(ccp(0,0));
// 		pageView_kuang->setZOrder(10);
// 		pageView_kuang->setPosition(ccp(389,89));
// 		sendMailLayer->addWidget(pageView_kuang);
		writePanel = (UIPanel*)UIHelper::seekWidgetByName(ppanel,"Panel_xiexin");
		
		mailNull = (UIPanel *)UIHelper::seekWidgetByName(ppanel,"Panel_null");
		mailNull->setVisible(false);
		//friend find
		UIButton * friendSelect=(UIButton *)UIHelper::seekWidgetByName(ppanel,"Button_haoyou");
		friendSelect->setTouchEnable(true);
		friendSelect->setPressedActionEnabled(true);
		friendSelect->addReleaseEvent(this,coco_releaseselector(MailUI::callBackFriendList));
		
		//intputBox  friendName
		friendName=new RichTextInputBox();
		//friendName->setInputBoxWidth(217);
		friendName->setInputBoxWidth(260);
		friendName->setCharLimit(10);
		friendName->setAnchorPoint(ccp(0,0));
		friendName->setPosition(ccp(139,370));
		friendName->setScale(0.8f);
		layer_->addChild(friendName,2,FRIENDNAMERICHINPUT);
		friendName->autorelease();
		//intputBox  title
		titleBox=new RichTextInputBox();
		//titleBox->setInputBoxWidth(217);
		titleBox->setInputBoxWidth(260);
		titleBox->setAnchorPoint(ccp(0,0));
		titleBox->setPosition(ccp(139,328));
		titleBox->setScale(0.8f);
		layer_->addChild(titleBox,2,TITLERICHINPUT);
		titleBox->setCharLimit(10);
		titleBox->autorelease();
		//intputBox  content
		contentBox=new RichTextInputBox();
		contentBox->setMultiLinesMode(true);
		contentBox->setInputBoxWidth(285);
        contentBox->setInputBoxHeight(150);
        contentBox->setDirection(RichTextInputBox::kCCRichInputDirectionVertical);
		contentBox->setAnchorPoint(ccp(0,0));
		contentBox->setScale(0.9f);
		contentBox->setPosition(ccp(75,290));
		contentBox->setCharLimit(200);
		layer_->addChild(contentBox,2,CONTENTRICHINPUT);
		contentBox->release();
		//delete
		UIButton *cleanText= (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_qingkong");
		cleanText->setTouchEnable(true);
		cleanText->setPressedActionEnabled(true);
		cleanText->addReleaseEvent(this, coco_releaseselector(MailUI::callBackClean));
		//send mail
		UIButton *sendMail= (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_fasong");
		sendMail->setTouchEnable(true);
		sendMail->setPressedActionEnabled(true);
		sendMail->addReleaseEvent(this, coco_releaseselector(MailUI::callBackSendMail));
		///////////
		UIButton *button_shop= (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_shangcheng");
		button_shop->setTouchEnable(true);
		button_shop->setPressedActionEnabled(true);
		button_shop->addReleaseEvent(this, coco_releaseselector(MailUI::callBackShop));
		
		label_Gold =(UILabel*)UIHelper::seekWidgetByName(ppanel,"Labeljinbi_num");
		label_GoldIngold =(UILabel*)UIHelper::seekWidgetByName(ppanel,"Labelyuanbao_num");
		this->refreshPlayerMoney();
		//////////////////
		UIButton * buttonCoins = (UIButton *)UIHelper::seekWidgetByName(ppanel,"button_jinbi_count");
		buttonCoins->setTouchEnable(true);
		//buttonCoins->setPressedActionEnabled(true);
		buttonCoins->addReleaseEvent(this,coco_releaseselector(MailUI::collectCoins));

		labelCoins = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Labeljinbi_num_0");
		labelCoins->setText("0");

		UIButton * buttonIngot = (UIButton *)UIHelper::seekWidgetByName(ppanel,"button_yuanbao_count");
		buttonIngot->setTouchEnable(true);
		//buttonIngot->setPressedActionEnabled(true);
		buttonIngot->addReleaseEvent(this,coco_releaseselector(MailUI::collectIngot));
		labelIngot = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Labelyuanbao_num_0");
		labelIngot->setText("0");

		//imageCostgetMail = (UIImageView *)UIHelper::seekWidgetByName(ppanel,"ImageView_zhifu");
		//imageCostgetMail->setVisible(false);
		readMailCoinsImage = (UIImageView *)UIHelper::seekWidgetByName(ppanel,"ImageView_jinbikuang_0_0");
		readMailCoinsImage->setVisible(false);
		readMailCoinsLabel = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Labeljinbi_num_0_0");
		readMailCoinsLabel->setText("0");

		readMailIngotImage = (UIImageView *)UIHelper::seekWidgetByName(ppanel,"ImageView_yuanbao_0_0");
		readMailIngotImage->setVisible(false);
		readMailIngotLabel = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Labelyuanbao_num_0_0");
		readMailIngotLabel->setText("0");
		///////////////
		getMailLayer->setVisible(false);
		readPanel->setVisible(false);
		mailNull->setVisible(true);

		//channelLabelMail->setVisible(true);
		writePanel->setVisible(false);
		sendMailLayer->setVisible(false);
		friendName->setVisible(false);
		titleBox->setVisible(false);
		contentBox->setVisible(false);
		
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(size);
		return true;
	}
	return false;
}

void MailUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();

	GameUtils::playGameSound(SCENE_OPEN, 2, false);

	for (int i=0;i<GameView::getInstance()->mailVectorOfSystem.size();i++)
	{
		CMailInfo * m_mailSource = new CMailInfo();
		m_mailSource->CopyFrom(*GameView::getInstance()->mailVectorOfSystem.at(i));

		sourceDataVector.push_back(m_mailSource);
	}

	mailList=MailList::create();
	mailList->ignoreAnchorPointForPosition(false);
	mailList->setAnchorPoint(ccp(0,0));
	mailList->setPosition(ccp(74,42));
	layer_->addChild(mailList,3,MAILLISTTAG);
	mailList->init();
	mailList->setVisible(false);
}

void MailUI::onExit()
{
	UIScene::onExit();
	SimpleAudioEngine::sharedEngine()->playEffect(SCENE_CLOSE, false);
}

void MailUI::callBackChangeMail( CCObject * obj )
{
	UITab * m_tab=(UITab *)obj;
	int num= m_tab->getCurrentIndex();	
	switch(num)
	{
	case 0:
		{
			readPanel->setVisible(true);
			mailList->setVisible(true);
			if (sourceDataVector.size()>0)
			{
				mailNull->setVisible(false);
				getMailLayer->setVisible(true);
				readPanel->setVisible(true);
				//channelLabelMail->setVisible(true);
				mailList->setVisible(true);
			}else
			{
				mailNull->setVisible(true);
				getMailLayer->setVisible(false);
				readPanel->setVisible(false);
				//channelLabelMail->setVisible(true);
				mailList->setVisible(false);
			}

			writePanel->setVisible(false);
			sendMailLayer->setVisible(false);
			friendName->setVisible(false);
			titleBox->setVisible(false);
			contentBox->setVisible(false);
			pageView->setVisible(false);
		}break;
	case 1:
		{
			readPanel->setVisible(false);
			getMailLayer->setVisible(false);
			mailList->setVisible(false);
			mailNull->setVisible(false);
			//channelLabelMail->setVisible(false);
			writePanel->setVisible(true);
			sendMailLayer->setVisible(true);
			friendName->setVisible(true);
			titleBox->setVisible(true);
			contentBox->setVisible(true);
			pageView->setVisible(true);
		}break;
	}
}

void MailUI::callBackSendMail( CCObject * obj )
{
	const char * playerName = friendName->getInputString();
	if (playerName ==NULL )
	{
		const char *str_ = StringDataManager::getString("mailui_sendPlayer");
		GameView::getInstance()->showAlertDialog(str_);
		return;
	}
	const char* titleText = titleBox->getInputString();
	const char *contentText= contentBox->getInputString();
	if (titleText == NULL && contentText == NULL)
	{
		const char *str_ = StringDataManager::getString("mailui_sendTittle");
		GameView::getInstance()->showAlertDialog(str_);
		return;
	}
	if (titleText != NULL)
	{
		mail_subject=titleText;
	}

	if (contentText != NULL)
	{
		mail_text=contentText;
	}

	if (mailAttachment_amount >0)
	{
		mail_attactMent=true;
	}
	receiverName = playerName;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(2000, this);
	
	GameUtils::playGameSound(MAIL_SEND, 2, false);
}

void MailUI::callBackFriendList( CCObject * obj )
{
	MailFriend * mailFriendui =(MailFriend *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagPopFriendListUI);
	if (mailFriendui == NULL)
	{
		CCSize winsize=CCDirector::sharedDirector()->getVisibleSize();
		MailFriend * mailFriend =MailFriend::create(kTagMailUi);
		mailFriend->ignoreAnchorPointForPosition(false);
		mailFriend->setAnchorPoint(ccp(0.5f,0.5f));
		mailFriend->setPosition(ccp(winsize.width/2,winsize.height/2));
		GameView::getInstance()->getMainUIScene()->addChild(mailFriend,3,kTagPopFriendListUI);

		FriendStruct friend1={0,20,mailFriend->setFriendsPage,0};
		GameMessageProcessor::sharedMsgProcessor()->sendReq(2202,&friend1);
	}
}

void MailUI::callBackClean( CCObject * obj )
{
	contentBox->deleteAllInputString();
}


void MailUI::callBackAddexan( CCObject * obj )
{
	if (this->isClosing())
		return;

	UIButton * btn=(UIButton * )obj;
	int btnId=btn->getWidgetTag();

	if (mailAttachment_amount <2)
	{
		int goodsNum = GameView::getInstance()->AllPacItem.at(pageView->curPackageItemIndex)->quantity();
		if (goodsNum >1)
		{
			GameView::getInstance()->showCounter(this, callfuncO_selector(MailUI::counterAnnex),goodsNum);
		}else
		{
			this->counterAnnex(NULL);
		}

	}else
	{		
		const char *str_ = StringDataManager::getString("mailui_addAnnex");
		GameView::getInstance()->showAlertDialog(str_);	
	}

}

void MailUI::callBackDeleteMail( CCObject * obj )
{	
	if (sourceDataVector.size()<=0)
	{
		return;
	}
	std::string string_ = "";
	if (sourceDataVector.size() <=mailList_Id)
	{
		mailList_Id--;
	}
	if (sourceDataVector.at(mailList_Id)->attachment_size() > 0 && sourceDataVector.at(mailList_Id)->mutable_attachment(0)->state()==0)
	{
		const char * mailOfAttachement = StringDataManager::getString("mailui_ofAttachsExit");
		string_.append(mailOfAttachement);
	}
	const char * mailOfDelete = StringDataManager::getString("mailui_delete");
	string_.append(mailOfDelete);
	/*
	CCSize winSize=CCDirector::sharedDirector()->getVisibleSize();
	PopupWindow * popupWindow_ =PopupWindow::create(string_,2,PopupWindow::KtypeNeverRemove,10);
	popupWindow_->ignoreAnchorPointForPosition(false);
	popupWindow_->setAnchorPoint(ccp(0.5f,0.5f));
	popupWindow_->setPosition(ccp(winSize.width/2,winSize.height/2));
	popupWindow_->setButtonPosition();
	popupWindow_->AddcallBackEvent(this,callfuncO_selector(MailUI::deleteMailSure),NULL);
	GameView::getInstance()->getMainUIScene()->addChild(popupWindow_, 100);
	*/
	
	GameView::getInstance()->showPopupWindow(string_,2,this,callfuncO_selector(MailUI::deleteMailSure),NULL);
}

void MailUI::callBackExtractAnnex( CCObject * obj )
{
	if (sourceDataVector.size() <= 0)
	{
		return;
	}
	if (sourceDataVector.size() <=mailList_Id)
	{
		mailList_Id--;
	}
	int gold_ = sourceDataVector.at(mailList_Id)->gold();
	int goldIngot_ = sourceDataVector.at(mailList_Id)->goldingot();
	if ( gold_> 0 || goldIngot_ > 0)
	{
		std::string tempString = "";
		const char* string_ = StringDataManager::getString("mail_payToMail");
		tempString.append(string_);
		if (gold_ > 0)
		{
			char goldStr[50];
			const char* templateStr = StringDataManager::getString("mail_payToMailOfGold");
			sprintf(goldStr,templateStr,gold_);
			//tempString.append(StrUtils::applyColor(goldStr,ccc3(255,0,0)).c_str());
			tempString.append(goldStr);
		}
		if (gold_ > 0 && goldIngot_ > 0)
		{
			const char* templateStr = StringDataManager::getString("mail_payToMailOfGoldAnd");
			tempString.append(templateStr);
		}

		if (goldIngot_ > 0)
		{
			char goldIngotStr[50];
			const char* templateStr = StringDataManager::getString("mail_payToMailOfGoldIngot");
			sprintf(goldIngotStr,templateStr,goldIngot_);
			tempString.append(goldIngotStr);
		}
		const char* tempStringPayTomail = StringDataManager::getString("mail_payToMailOfSureGetback");
		tempString.append(tempStringPayTomail);
		GameView::getInstance()->showPopupWindow(tempString,2,this,callfuncO_selector(MailUI::callBackSurePayToMoney),NULL);
	}else
	{
		operationType = 3;
		long long mailindex_ =sourceDataVector.at(mailList_Id)->id();
		mailList->tableView->selectCell(mailList_Id);
		GameMessageProcessor::sharedMsgProcessor()->sendReq(2002,(void *)3,(void *)mailindex_);
		GameUtils::playGameSound(ANNEX_EXTRACT, 2, false);
	}
}

void MailUI::callBackSurePayToMoney( CCObject * obj )
{
	operationType = 3;
	long long mailindex_ =sourceDataVector.at(mailList_Id)->id();
	mailList->tableView->selectCell(mailList_Id);
	GameMessageProcessor::sharedMsgProcessor()->sendReq(2002,(void *)3,(void *)mailindex_);
	GameUtils::playGameSound(ANNEX_EXTRACT, 2, false);
}

void MailUI::deleteMailSure( CCObject * obj )
{	
	if (sourceDataVector.size() <=mailList_Id)
	{
		mailList_Id--;
	}
	operationType = 2;
	long long deleteMailId =sourceDataVector.at(mailList_Id)->id();
	GameMessageProcessor::sharedMsgProcessor()->sendReq(2002,(void *)2,(void *)deleteMailId);
	GameUtils::playGameSound(MAIL_DELETE, 2, false);
	//operation vector
	std::vector<CMailInfo *>::iterator iter= sourceDataVector.begin() + mailList_Id;
	CMailInfo * mailV=*iter ;
	sourceDataVector.erase(iter);
	delete mailV;

	CCTableView * tabMail=(CCTableView *)mailList->getChildByTag(MAILTABTAG);
	tabMail->reloadData();
	showMailInfo(mailList_Id);

	for (int i=0;i<GameView::getInstance()->mailVectorOfSystem.size();i++)
	{
		if (GameView::getInstance()->mailVectorOfSystem.at(i)->id()==deleteMailId)
		{
			std::vector<CMailInfo *>::iterator iter= GameView::getInstance()->mailVectorOfSystem.begin() + i;
			CMailInfo * systemMailV=*iter ;
			GameView::getInstance()->mailVectorOfSystem.erase(iter);
			delete systemMailV;
		}
	}
	/*
	for (int i=0;i<GameView::getInstance()->mailVectorOfPlayer.size();i++)
	{
		if (GameView::getInstance()->mailVectorOfPlayer.at(i)->id()==deleteMailId)
		{
			std::vector<CMailInfo *>::iterator iter= GameView::getInstance()->mailVectorOfPlayer.begin() + i;
			CMailInfo * systemMailV=*iter ;
			GameView::getInstance()->mailVectorOfPlayer.erase(iter);
			delete systemMailV;
		}
	}
	*/
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	//mainScene->checkIsNewRemind();
	mainScene->remindMail();
}

void MailUI::callBackReplyMail( CCObject * obj )
{
	if (mailList_Id <0 || sourceDataVector.size() <= 0 || sourceDataVector.size() < mailList_Id+1)
	{
		return;
	}
	std::string sendName_ = sourceDataVector.at(mailList_Id)->sendername();
	friendName->deleteAllInputString();
	friendName->onTextFieldInsertText(NULL,sendName_.c_str(),30);

	channelLabel->setDefaultPanelByIndex(1);

	readPanel->setVisible(false);
	getMailLayer->setVisible(false);
	mailList->setVisible(false);
	//channelLabelMail->setVisible(false);
	writePanel->setVisible(true);
	sendMailLayer->setVisible(true);
	friendName->setVisible(true);
	titleBox->setVisible(true);
	contentBox->setVisible(true);
	pageView->setVisible(true);

}

void MailUI::callBackChangeMailSender( CCObject * obj )
{
	/*
	UITab * m_tab=(UITab *)obj;
	int num= m_tab->getCurrentIndex();

	std::vector<CMailInfo *>::iterator iter;
	for (iter=sourceDataVector.begin();iter!=sourceDataVector.end();iter++)
	{
		delete *iter;
	}
	sourceDataVector.clear();
	
	switch (num)
	{
	case 0:
		{
			curTabType = 0;
			curMailPage = 0;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2001,(void *)curMailPage);
			replyMail->setVisible(false);
		}break;
	case 1:
		{
			curTabType =1;
			replyMail->setVisible(true);
			curMailPage = 0;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2001,(void *)curMailPage);

		}break;
	}
	*/
}

void MailUI::callBackShop( CCObject * obj )
{
	GoldStoreUI *goldStoreUI = (GoldStoreUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoldStoreUI);
	if(goldStoreUI == NULL)
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		goldStoreUI = GoldStoreUI::create();
		GameView::getInstance()->getMainUIScene()->addChild(goldStoreUI,0,kTagGoldStoreUI);

		goldStoreUI->ignoreAnchorPointForPosition(false);
		goldStoreUI->setAnchorPoint(ccp(0.5f, 0.5f));
		goldStoreUI->setPosition(ccp(winSize.width/2, winSize.height/2));
	}
}

void MailUI::CurrentPageViewChanged(CCObject* pSender)
{
	switch((int)(pageView->getPageView()->getCurPageIndex()))
	{
	case 0:
		currentPage->setPosition(ccp(523,72));
		break;
	case 1:
		currentPage->setPosition(ccp(545,72));
		break;
	case 2:
		currentPage->setPosition(ccp(565,72));
		break;
	case 3:
		currentPage->setPosition(ccp(586,72));
		break;
	case 4:
		currentPage->setPosition(ccp(607,72));
		break;
	}
}

void MailUI::counterAnnex( CCObject * obj )
{	
	Counter * couter_ = (Counter*)obj;
	if (couter_ != NULL)
	{
		mail_amount =couter_->getInputNum();
	}else
	{
		mail_amount =1;
	}
	
	if (mail_amount <= 0)
	{
		const char *strings = StringDataManager::getString("mail_country");
		GameView::getInstance()->showAlertDialog(strings);
		return;
	}

	int goodsNum_ = GameView::getInstance()->AllPacItem.at(pageView->curPackageItemIndex)->quantity();
	if (mail_amount>goodsNum_ )
	{
		mail_amount = goodsNum_;
	}

	if (sendMailLayer->getChildByTag(REDUCTIONANNEXBUTTON1) != NULL)
	{
		FloderItemInstance * annex_1 = (FloderItemInstance *)sendMailLayer->getChildByTag(REDUCTIONANNEXBUTTON1);

		if (pageView->curPackageItemIndex == annex_1->getFloderIndex())
		{
			reductionAnnex1(NULL);
		}
	}

	if (sendMailLayer->getChildByTag(REDUCTIONANNEXBUTTON2) != NULL)
	{
		FloderItemInstance * annex_2 = (FloderItemInstance *)sendMailLayer->getChildByTag(REDUCTIONANNEXBUTTON2);

		if (pageView->curPackageItemIndex == annex_2->getFloderIndex())
		{
			reductionAnnex2(NULL);
		}
	}


	packageIndex = pageView->curPackageItemIndex;

	CAttachedProps * temp = new CAttachedProps();
	temp->set_folderidx(packageIndex);
	temp->set_amount(mail_amount);
	attachePropVector.push_back(temp);

	if (GameView::getInstance()->AllPacItem.at(packageIndex)->has_goods())
	{
		int goodsCount_ = GameView::getInstance()->AllPacItem.at(packageIndex)->quantity();
		char goodsUseNum[50];
		sprintf(goodsUseNum,"%d",mail_amount);
		char goodsAllNum[50];
		sprintf(goodsAllNum,"%d",goodsCount_);
		std::string goodsUseAndCount = goodsUseNum;
		goodsUseAndCount.append("/");
		goodsUseAndCount.append(goodsAllNum);

		GameView::getInstance()->pacPageView->SetCurFolderGray(true,packageIndex,goodsUseAndCount);
	}

	if (firstIsNull == true)
	{
		FolderInfo * temp_folder_ = new FolderInfo();
		temp_folder_->CopyFrom(*GameView::getInstance()->AllPacItem.at(pageView->curPackageItemIndex));
		FloderItemInstance * mail_Item = FloderItemInstance::create(temp_folder_,FloderItemInstance::ktype_mail_addAnnex,mail_amount);
		mail_Item->ignoreAnchorPointForPosition(false);
		mail_Item->setAnchorPoint(ccp(0,0));
		mail_Item->setPosition(ccp(79,96));
		mail_Item->setTag(REDUCTIONANNEXBUTTON1);
		sendMailLayer->addChild(mail_Item);
		delete temp_folder_;
		firstIsNull=false;
	}else
	{
		FolderInfo * temp_folder_ = new FolderInfo();
		temp_folder_->CopyFrom(*GameView::getInstance()->AllPacItem.at(pageView->curPackageItemIndex));
		FloderItemInstance * mail_Item = FloderItemInstance::create(temp_folder_,FloderItemInstance::ktype_mail_addAnnex,mail_amount);
		mail_Item->ignoreAnchorPointForPosition(false);
		mail_Item->setAnchorPoint(ccp(0,0));
		mail_Item->setPosition(ccp(153,96));
		mail_Item->setTag(REDUCTIONANNEXBUTTON2);
		sendMailLayer->addChild(mail_Item);
		delete temp_folder_;
	}
	mailAttachment_amount++;
}

void MailUI::reductionAnnex1( CCObject *obj )
{
	FloderItemInstance * mailAnnexui =(FloderItemInstance *)sendMailLayer->getChildByTag(REDUCTIONANNEXBUTTON1);
	if (mailAnnexui == NULL)
	{
		return;
	}
	mailAnnexui->removeFromParentAndCleanup(true);
	firstIsNull=true;
	mailAttachment_amount--;
	//selectAnnexNum--;

	if (GameView::getInstance()->AllPacItem.at(attachePropVector.at(0)->folderidx())->has_goods())
	{
		GameView::getInstance()->pacPageView->SetCurFolderGray(false,attachePropVector.at(0)->folderidx());
	}

	if (mailAttachment_amount <= 0)
	{
		sendMailCollectcoins =  0;
		labelCoins->setText("0");
		sendMailCollectinIngots =  0;
		labelIngot->setText("0");
	}

	std::vector<CAttachedProps *>::iterator iter=attachePropVector.begin();
	CAttachedProps * attache=*iter;
	attachePropVector.erase(iter);		
	delete attache;
}
void MailUI::reductionAnnex2( CCObject *obj )
{
	FloderItemInstance * mailAnnexui =(FloderItemInstance *)sendMailLayer->getChildByTag(REDUCTIONANNEXBUTTON2);
	if (mailAnnexui == NULL)
	{
		return;
	}
	mailAnnexui->removeFromParentAndCleanup(true);
	mailAttachment_amount--;
	//selectAnnexNum--;

	if (attachePropVector.size()>1)
	{
		if (GameView::getInstance()->AllPacItem.at(attachePropVector.at(1)->folderidx())->has_goods())
		{
			GameView::getInstance()->pacPageView->SetCurFolderGray(false,attachePropVector.at(1)->folderidx());
		}

		std::vector<CAttachedProps *>::iterator iter=attachePropVector.begin()+1;
		CAttachedProps * attache=*iter;
		attachePropVector.erase(iter);	
		delete attache; 
	}else
	{
		if (GameView::getInstance()->AllPacItem.at(attachePropVector.at(0)->folderidx())->has_goods())
		{
			GameView::getInstance()->pacPageView->SetCurFolderGray(false,attachePropVector.at(0)->folderidx());
		}

		std::vector<CAttachedProps *>::iterator iter=attachePropVector.begin();
		CAttachedProps * attache=*iter;
		attachePropVector.erase(iter);	
		delete attache;
	}

	if (mailAttachment_amount <= 0)
	{
		sendMailCollectcoins =  0;
		labelCoins->setText("0");
		sendMailCollectinIngots =  0;
		labelIngot->setText("0");
	}
}

void MailUI::callbackClose( CCObject * obj )
{
	this->closeAnim();
}

void MailUI::scrollViewDidScroll( CCScrollView* view )
{
}

void MailUI::scrollViewDidZoom( CCScrollView* view )
{
}

bool MailUI::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	return resignFirstResponder(pTouch,this,false);
}

void MailUI::showMailInfo( int idx )
{
	mailContentPanel->removeAllChildren();
	mailTitle->setVisible(false);
	for (int i=0;i<2;i++)
	{
		GoodsItemInfoBase * goodsui = (GoodsItemInfoBase *)getMailLayer->getChildByTag(MAILANNEXT+i);
		if (goodsui!= NULL)
		{
			goodsui->removeFromParentAndCleanup(true);
		}
	}

	image_mailItem_1->setVisible(true);
	image_mailItem_2->setVisible(true);

	if (sourceDataVector.size() > idx)
	{
		if (sourceDataVector.at(idx)->sender() == LLONG_MAX)
		{
			replyMail->setVisible(false);
		}else
		{
			replyMail->setVisible(true);
		}

		std::string labelSubject =sourceDataVector.at(idx)->subject();
		std::string labelText = sourceDataVector.at(idx)->text();
		std::string labelName =sourceDataVector.at(idx)->sendername();
		long long timeSecond = sourceDataVector.at(idx)->sendtime();

		mailTitle->setText(labelSubject.c_str());
		mailTitle->setVisible(true);
		UILabel * mailContentTest = UILabel::create();
		mailContentTest->setAnchorPoint(ccp(0,0));
		mailContentTest->setText(labelText.c_str());
		mailContentTest->setFontSize(16);
		mailContentTest->setTextAreaSize(CCSizeMake(270,0));
		mailContentPanel->addChild(mailContentTest);

		//get m_scrollView height and width
		int touchHight=mailContentTest->getContentSize().height;
		int innerWidth=m_scrollView->getRect().size.width;
		int innerHeight=m_scrollView->getRect().size.height + touchHight/2;
		m_scrollView->setInnerContainerSize(CCSizeMake(innerWidth,innerHeight));
		
		mailContentTest->setPosition(ccp(10,innerHeight - touchHight));
		if (sourceDataVector.at(idx)->goldingot() > 0|| sourceDataVector.at(idx)->gold() > 0)
		{
			char coinstr[10];
			sprintf(coinstr,"%d",sourceDataVector.at(idx)->gold());
			char ingotstr[10];
			sprintf(ingotstr,"%d",sourceDataVector.at(idx)->goldingot());
			//imageCostgetMail->setVisible(true);
			readMailIngotImage->setVisible(true);
			readMailCoinsImage->setVisible(true);
			readMailCoinsLabel->setText(coinstr);
			readMailIngotLabel->setText(ingotstr);
		}else
		{
			//imageCostgetMail->setVisible(false);
			readMailIngotImage->setVisible(false);
			readMailCoinsImage->setVisible(false);
		}

		if (sourceDataVector.at(idx)->attachment_size()>0 && sourceDataVector.at(idx)->mutable_attachment(0)->state()==0)//有附件 && 0 未提取
		{
			extractAnnex->setVisible(true);
			int vectorSize = sourceDataVector.at(idx)->attachment_size();
			for (int index=0;index<vectorSize;index++)
			{
				GoodsInfo * goodsInfo =new GoodsInfo();
				goodsInfo->CopyFrom(sourceDataVector.at(idx)->mutable_attachment(index)->good());
				
				int goodsAmount = sourceDataVector.at(idx)->mutable_attachment(index)->amount();
				GoodsItemInstance * mailgoodsinfo =GoodsItemInstance::create(goodsInfo,GoodsItemInstance::ktype_mail_getBackAnnex,goodsAmount);
				mailgoodsinfo->ignoreAnchorPointForPosition(false);
				mailgoodsinfo->setAnchorPoint(ccp(0,0));
				mailgoodsinfo->setScale(0.95f);
				mailgoodsinfo->setPosition(ccp(440.2f +74*index,106));
				mailgoodsinfo->setTag(MAILANNEXT+ index);
				getMailLayer->addChild(mailgoodsinfo);
				delete goodsInfo;

				if (index == 0)
				{
					image_mailItem_1->setVisible(false);	
				}
				if(index == 1)
				{					
					image_mailItem_2->setVisible(false);
				}
				

			}
		}else
		{
			extractAnnex->setVisible(false);
		}
	}
}

void MailUI::refreshPlayerMoney()
{
	int m_goldValue = GameView::getInstance()->getPlayerGold();
	char GoldStr_[20];
	sprintf(GoldStr_,"%d",m_goldValue);
	label_Gold->setText(GoldStr_);

	int m_goldIngot =  GameView::getInstance()->getPlayerGoldIngot();
	char GoldIngotStr_[20];
	sprintf(GoldIngotStr_,"%d",m_goldIngot);
	label_GoldIngold->setText(GoldIngotStr_);
}

void MailUI::collectCoins( CCObject * obj )
{
	if (this->isClosing())
		return;

	int openlevel = 0;
	std::map<int,int>::const_iterator cIter;
	cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(14);
	if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // 没找到就是指向END了  
	{
	}
	else
	{
		openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[14];
	}
	int selfLevel_ =GameView::getInstance()->myplayer->getActiveRole()->level();
	if (selfLevel_ >= openlevel)
	{
		if (mailAttachment_amount <= 0)
		{
			const char *strings_ = StringDataManager::getString("mail_pleaseAddExtranFirst");
			GameView::getInstance()->showAlertDialog(strings_);
		}else
		{
			GameView::getInstance()->showCounter(this, callfuncO_selector(MailUI::setCollectCoinsValue));
		}
	}else
	{
		char openLevelStr[100];
		const char *strings = StringDataManager::getString("mail_OfMoneyOpenLevel");
		sprintf(openLevelStr,strings,openlevel);
		GameView::getInstance()->showAlertDialog(openLevelStr);
	}
}

void MailUI::collectIngot( CCObject * obj )
{
	if (this->isClosing())
		return;

	int openlevel = 0;
	std::map<int,int>::const_iterator cIter;
	cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(14);
	if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // 没找到就是指向END了  
	{
	}
	else
	{
		openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[14];
	}
	int selfLevel_ =GameView::getInstance()->myplayer->getActiveRole()->level();
	if (selfLevel_ >= openlevel)
	{
		if (mailAttachment_amount <= 0)
		{
			const char *strings_ = StringDataManager::getString("mail_pleaseAddExtranFirst");
			GameView::getInstance()->showAlertDialog(strings_);
		}else
		{
			GameView::getInstance()->showCounter(this, callfuncO_selector(MailUI::setCollectIngotValue));
		}	
	}else
	{
		char openLevelStr[100];
		const char *strings = StringDataManager::getString("mail_OfMoneyOpenLevel");
		sprintf(openLevelStr,strings,openlevel);
		GameView::getInstance()->showAlertDialog(openLevelStr);
	}
}

void MailUI::setCollectCoinsValue( CCObject * obj )
{
	Counter * count_ = (Counter *)obj;
	int num_ = count_->getInputNum();

	sendMailCollectcoins =  num_;
	char numStr[10];
	sprintf(numStr,"%d",num_);
	labelCoins->setText(numStr);
	
}

void MailUI::setCollectIngotValue( CCObject * obj )
{
	Counter * count_ = (Counter *)obj;
	int num_ = count_->getInputNum();
	sendMailCollectinIngots =  num_;
	char numStr[10];
	sprintf(numStr,"%d",num_);
	labelIngot->setText(numStr);
}

void MailUI::PageScrollToDefault()
{
	pageView->getPageView()->scrollToPage(0);
	CurrentPageViewChanged(pageView);
}




