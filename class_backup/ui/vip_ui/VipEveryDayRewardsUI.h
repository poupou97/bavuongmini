
#ifndef  _VIPUI_VIPEVERYDATREWARDSUI_H_
#define _VIPUI_VIPEVERYDATREWARDSUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class VipEveryDayRewardsUI : public UIScene, public CCTableViewDataSource, public CCTableViewDelegate
{
private:
	UILayer * u_layer;
	CCTableView * m_tableView;
public:
	VipEveryDayRewardsUI();
	~VipEveryDayRewardsUI();

	static VipEveryDayRewardsUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);

	void CloseEvent(CCObject * pSender);

	void refreshTableViewWithOutChangeOffSet();
 	void VipBuyEvent(CCObject* pSender);
// 	void Vip1GetEvent(CCObject* pSender);
// 	void Vip2GetEvent(CCObject* pSender);
// 	void Vip3GetEvent(CCObject* pSender);
// 	void Vip4GetEvent(CCObject* pSender);
// 	void Vip5GetEvent(CCObject* pSender);

//	void RefreshRewardItem();
//	void RefreshStatus(int vipLevel,int status);

private:
// 	UIButton *Btn_get_1;
// 	UIButton *Btn_get_2;
// 	UIButton *Btn_get_3;
// 	UIButton *Btn_get_4;
// 	UIButton *Btn_get_5;
// 
// 	UIImageView * imageView_canNotGet_1;
// 	UIImageView * imageView_canNotGet_2;
// 	UIImageView * imageView_canNotGet_3;
// 	UIImageView * imageView_canNotGet_4;
// 	UIImageView * imageView_canNotGet_5;
// 
// 	UILabel * l_content_1;
// 	UILabel * l_content_2;
// 	UILabel * l_content_3;
// 	UILabel * l_content_4;
// 	UILabel * l_content_5;
};



/////////////////////////////////
/**
 * vip每日领取下的cell
 * @author yangjun
 * @version 0.1.0
 * @date 2014.5.6
 */

class COneVipGift;

class VipEveryDayRewardCell : public UIScene
{
public:
	VipEveryDayRewardCell();
	~VipEveryDayRewardCell();
	static VipEveryDayRewardCell* create(COneVipGift * oneVipGift);
	bool init(COneVipGift * oneVipGift);

	void getRewardEvent(CCObject *pSender);

public:
	COneVipGift * curOneVipGift;
};



#endif

