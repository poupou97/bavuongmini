#include "Recharge.h"


#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS

#include "../Classes/loadscene_state/LoadSceneState.h"
#include "../Classes/GameView.h"
#include "../Classes/ui/extensions/UITab.h"
#include "../Classes/messageclient/element/CLable.h"
#include "../Classes/messageclient/element/CLableGoods.h"
#include "../Classes/utils/StaticDataManager.h"
#include "../Classes/gamescene_state/MainScene.h"
#include "../Classes/messageclient/element/GoodsInfo.h"
#include "../Classes/utils/StrUtils.h"
#include "../Classes/ui/vip_ui/VipDetailUI.h"
#include "../Classes/ui/vip_ui/VipEveryDayRewardsUI.h"
#include "../Classes/ui/vip_ui/FirstBuyVipUI.h"
#include "../Classes/ui/vip_ui/VipData.h"
#include "RechargeCellItem.h"
#include "../Classes/ui/extensions/Counter.h"
#include "../Classes/utils/GameConfig.h"

#include "WmComPlatform.h"
#include "ExtenalClass.h"
#include "../Classes/login_state/Login.h"
#include "../Classes/login_state/LoginState.h"
#include "../Classes/utils/GameConfig.h"

#else

#include "../../loadscene_state/LoadSceneState.h"
#include "../../GameView.h"
#include "../extensions/UITab.h"
#include "../../messageclient/element/CLable.h"
#include "../../messageclient/element/CLableGoods.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../utils/StrUtils.h"
#include "../vip_ui/VipDetailUI.h"
#include "../vip_ui/VipEveryDayRewardsUI.h"
#include "../vip_ui/FirstBuyVipUI.h"
#include "../vip_ui/VipData.h"
#include "RechargeCellItem.h"
#include "../extensions/Counter.h"
#include "../../utils/GameConfig.h"

#endif



#define MARQUEE_LABLE_WIDTH 596
#define GOLD_STORE_MAX_FILTER 6
#define HIGHLIGHT_FIRST_TAG 100
#define HIGHLIGHT_SECOND_TAG 101

RechargeUI::RechargeUI():
curLableId(0),
m_strMarquee("")
{
}


RechargeUI::~RechargeUI()
{

}

RechargeUI* RechargeUI::create()
{
	RechargeUI * rechargeUI = new RechargeUI();
	if (rechargeUI && rechargeUI->init())
	{
		rechargeUI->autorelease();
		return rechargeUI;
	}
	CC_SAFE_DELETE(rechargeUI);
	return rechargeUI;
}

bool RechargeUI::init()
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		this->scheduleUpdate();

		cocos2d::extension::UIImageView *mengban = cocos2d::extension::UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winsize);
		mengban->setAnchorPoint(CCPointZero);
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);

		u_layer = UILayer::create();
		u_layer->ignoreAnchorPointForPosition(false);
		u_layer->setAnchorPoint(ccp(0.5f,0.5f));
		u_layer->setContentSize(CCSizeMake(800, 480));
		u_layer->setPosition(CCPointZero);
		u_layer->setPosition(ccp(winsize.width/2,winsize.height/2));
		addChild(u_layer);

		//加载UI
		if(LoadSceneLayer::RechargePanel->getWidgetParent() != NULL)
		{
			LoadSceneLayer::RechargePanel->removeFromParentAndCleanup(false);
		}
		ppanel = LoadSceneLayer::RechargePanel;
		ppanel->getValidNode()->setContentSize(CCSizeMake(800, 480));
		ppanel->setAnchorPoint(ccp(0.5f,0.5f));
		ppanel->setPosition(CCPointZero);
		ppanel->setTouchEnable(true);
		ppanel->setPosition(ccp(winsize.width/2,winsize.height/2));
		m_pUiLayer->addWidget(ppanel);

		//关闭按钮
		cocos2d::extension::UIButton * Button_close = (cocos2d::extension::UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnable(true);
		Button_close->addReleaseEvent(this, coco_releaseselector(RechargeUI::CloseEvent));
		Button_close->setPressedActionEnabled(true);

		cocos2d::extension::UIButton * Button_recharge = (cocos2d::extension::UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_buy_0");
		Button_recharge->setTouchEnable(true);
		Button_recharge->addReleaseEvent(this, coco_releaseselector(RechargeUI::RechargeEvent));
		Button_recharge->setPressedActionEnabled(true);
		bool anyEnabled = GameConfig::getBoolForKey("recharge_any_enable");
		if(anyEnabled)
		{
			Button_recharge->setVisible(true);
		}
		else
		{
			Button_recharge->setVisible(false);
		}

		cocos2d::extension::UIButton * Button_vip = (cocos2d::extension::UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_buy");
		Button_vip->setTouchEnable(true);
		Button_vip->addReleaseEvent(this, coco_releaseselector(RechargeUI::VipEvent));
		Button_vip->setPressedActionEnabled(true);
		bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
		if(vipEnabled)
		{
			Button_vip->setVisible(true);
		}
		else
		{
			Button_vip->setVisible(false);
		}

		l_ingot_value = (cocos2d::extension::UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_myIngotValue");
		char str_ingot[20];
		sprintf(str_ingot,"%d",GameView::getInstance()->getPlayerGoldIngot());
		l_ingot_value->setText(str_ingot);
		
		UILabelBMFont* label_first = (UILabelBMFont*)UIHelper::seekWidgetByName(ppanel,"LabelBMFont_1293");
		std::string strLabel;
		strLabel.append(StringDataManager::getString("RechargeUI_First_front"));
		memset(str_ingot, 0, sizeof(str_ingot));
		int a = atoi(VipValueConfig::s_vipValue[1].c_str());
		sprintf(str_ingot,"%d",a*100);
		strLabel.append(str_ingot);
		strLabel.append(StringDataManager::getString("RechargeUI_First_back"));
		label_first->setText(strLabel.c_str());
		
		UILabelBMFont* label_second = (UILabelBMFont*)UIHelper::seekWidgetByName(ppanel,"LabelBMFont_1293_0");
		label_second->setText(StringDataManager::getString("RechargeUI_Second"));

		if(vipEnabled)
		{
			label_first->setVisible(true);
			label_second->setVisible(true);
		}
		else
		{
			label_first->setVisible(false);
			label_second->setVisible(false);
		}

		m_tableView = CCTableView::create(this,CCSizeMake(520,236));
		m_tableView->setDirection(kCCScrollViewDirectionVertical);
		m_tableView->setAnchorPoint(ccp(0.5f,0.5f));
		m_tableView->setPosition(ccp(140,80));
		m_tableView->setDelegate(this);
		m_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		u_layer->addChild(m_tableView);
		m_tableView->reloadData();
		
		CCScale9Sprite *pFrame = CCScale9Sprite::create("res_ui/LV5_dikuang1_miaobian2.png");
		pFrame->setAnchorPoint(ccp(0,0));
		pFrame->setPosition(ccp(128,80));
		pFrame->setPreferredSize(CCSizeMake(543,243));
		pFrame->setCapInsets(CCRectMake(32,32,1,1));
		u_layer->addChild(pFrame, 2);
	
		//this->scheduleUpdate();

		return true;
	}
	return false;
}

void RechargeUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void RechargeUI::onExit()
{
	UIScene::onExit();
}

bool RechargeUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return true;
}

void RechargeUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void RechargeUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void RechargeUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void RechargeUI::CloseEvent( CCObject *pSender )
{
	this->closeAnim();
}

void RechargeUI::callBackCounter(CCObject *obj)
{
	Counter * priceCounter =(Counter *)obj;
	int price = priceCounter->getInputNum();
	CCLOG("to SDK recharge");

#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS

	if(price > 100000)
	{
		NSString * strLoginTip = @"超过单笔金额上限100000，请您重试";
		const char * charLoginTip = [strLoginTip UTF8String];
		GameView::getInstance()->showAlertDialog(charLoginTip);

		return ;
	}

	//RechargeCellItem::s_nCharge = nCharge;
	[ExtenalClass set_Charge:price];

	requestPay();

#endif

}

void RechargeUI::RechargeEvent( CCObject *pSender )
{
	// 充值控制开关
	bool bChargeEnabled = GameConfig::getBoolForKey("charge_enable");
	if (true == bChargeEnabled)
	{
		GameView::getInstance()->showCounter(this, callfuncO_selector(RechargeUI::callBackCounter));
	}
	else
	{
		const char * charChargeEnabled = StringDataManager::getString("RechargeUI_disable");
		GameView::getInstance()->showAlertDialog(charChargeEnabled);
	}
}

void RechargeUI::VipEvent( CCObject *pSender )
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	CCLayer *vipScene = (CCLayer*)mainscene->getChildByTag(kTagVipDetailUI);
	if(vipScene == NULL)
	{
		VipDetailUI * vipDetailUI = (VipDetailUI*)VipData::vipDetailUI;
		mainscene->addChild(vipDetailUI,0,kTagVipDetailUI);
		vipDetailUI->ignoreAnchorPointForPosition(false);
		vipDetailUI->setAnchorPoint(ccp(0.5f,0.5f));
		vipDetailUI->setPosition(ccp(winSize.width/2, winSize.height/2));
	}
}

void RechargeUI::scrollViewDidScroll( CCScrollView* view )
{

}

void RechargeUI::scrollViewDidZoom( CCScrollView* view )
{

}

void RechargeUI::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{

}

cocos2d::CCSize RechargeUI::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	//return CCSizeMake(444,120);
	return CCSizeMake(510, 76);
}

cocos2d::extension::CCTableViewCell* RechargeUI::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell = new CCTableViewCell();
	cell->autorelease();
	
	CCScale9Sprite* pHighlightSpr = CCScale9Sprite::create("res_ui/kuang_0_on.png");
	pHighlightSpr->setCapInsets(CCRectMake(7, 7, 1, 1));
	pHighlightSpr->setPreferredSize(CCSizeMake(257,71));
	pHighlightSpr->setAnchorPoint(ccp(0, 0));
	pHighlightSpr->setVisible(false);
	pHighlightSpr->setTag(HIGHLIGHT_FIRST_TAG);
	cell->addChild(pHighlightSpr);
		
	CCScale9Sprite* pHighlightSprSecond = CCScale9Sprite::create("res_ui/kuang_0_on.png");
	pHighlightSprSecond->setCapInsets(CCRectMake(7, 7, 1, 1));
	pHighlightSprSecond->setPreferredSize(CCSizeMake(257,71));
	pHighlightSprSecond->setAnchorPoint(ccp(0, 0));
	pHighlightSprSecond->setVisible(false);
	pHighlightSprSecond->setPosition(ccp(262, 0));
	pHighlightSprSecond->setTag(HIGHLIGHT_SECOND_TAG);
	cell->addChild(pHighlightSprSecond);
		
	if (idx == RechargeConfig::s_rechargeValue.size()/2)
	{
		if (RechargeConfig::s_rechargeValue.size()%2 == 0)
		{
			RechargeCellItem * cellItem1 = RechargeCellItem::create(RechargeConfig::s_rechargeValue[2*idx+1]);
			cellItem1->setPosition(ccp(0,0));
			cellItem1->setTableView(m_tableView);
			cell->addChild(cellItem1);

			RechargeCellItem * cellItem2 = RechargeCellItem::create(RechargeConfig::s_rechargeValue[2*idx+2]);
			cellItem2->setPosition(ccp(262,0));
			cellItem2->setTableView(m_tableView);
			cell->addChild(cellItem2);
		}
		else
		{
			RechargeCellItem * cellItem1 = RechargeCellItem::create(RechargeConfig::s_rechargeValue[2*idx+1]);
			cellItem1->setPosition(ccp(0,0));
			cellItem1->setTableView(m_tableView);
			cell->addChild(cellItem1);
		}
	}
	else
	{
		RechargeCellItem * cellItem1 = RechargeCellItem::create(RechargeConfig::s_rechargeValue[2*idx+1]);
		cellItem1->setPosition(ccp(0,0));
		cellItem1->setTableView(m_tableView);
		cell->addChild(cellItem1);

		RechargeCellItem * cellItem2 = RechargeCellItem::create(RechargeConfig::s_rechargeValue[2*idx+2]);
		cellItem2->setPosition(ccp(262,0));
		cellItem2->setTableView(m_tableView);
		cell->addChild(cellItem2);
	}
	
	return cell;
}

unsigned int RechargeUI::numberOfCellsInTableView( CCTableView *table )
{
	if (RechargeConfig::s_rechargeValue.size()%2 == 0)
	{
		return RechargeConfig::s_rechargeValue.size()/2;
	}
	else
	{
		return RechargeConfig::s_rechargeValue.size()/2 + 1;
	}
}

void RechargeUI::update(float dt)
{
	char str_ingot[20];
	sprintf(str_ingot,"%d",GameView::getInstance()->getPlayerGoldIngot());
	l_ingot_value->setText(str_ingot);
}
