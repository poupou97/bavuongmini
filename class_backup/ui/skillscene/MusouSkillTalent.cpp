#include "MusouSkillTalent.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/CMusouTalent.h"
#include "../../messageclient/protobuf/PlayerMessage.pb.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../utils/StaticDataManager.h"
#include "../../AppMacros.h"
#include "../../GameView.h"
#include "SkillScene.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../extensions/ShowSystemInfo.h"
#include "../../gamescene_state/GameSceneEffects.h"

#define  MusouSkillTalentItemBaseTag 234
#define  M_SCROLLVIEWTAG 587

MusouSkillTalent::MusouSkillTalent():
curSelectIndex(1)
{
	for (int m = 2;m>=0;m--)
	{
		for (int n = 3;n>=0;n--)
		{
			int _m = 2-m;
			int _n = 3-n;

			Coordinate temp ;
			temp.x = 132+_n*88;
			temp.y = 65+m*120;

			CoordinateVector.push_back(temp);
		}
	}
}


MusouSkillTalent::~MusouSkillTalent()
{
	std::vector<CMusouTalent*>::iterator iter;
	for (iter = curMusouTalentList.begin(); iter != curMusouTalentList.end(); ++iter)
	{
		delete *iter;
	}
	curMusouTalentList.clear();
}

MusouSkillTalent* MusouSkillTalent::create()
{
	MusouSkillTalent * musouSkillTalent = new MusouSkillTalent();
	if (musouSkillTalent && musouSkillTalent->init())
	{
		musouSkillTalent->autorelease();
		return musouSkillTalent;
	}
	CC_SAFE_DELETE(musouSkillTalent);
	return NULL;
}

bool MusouSkillTalent::init()
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();

		//龙魂天赋面板
		panel_musouTalent = (UIPanel*)UIHelper::seekWidgetByName(LoadSceneLayer::SkillSceneLayer,"Panel_inborn");
		panel_musouTalent->setVisible(true);
		
		layer_musouTalent= UILayer::create();
		addChild(layer_musouTalent);
		layer_musouTalent->setVisible(true);
		//龙魂说明
		UIButton *btn_info = (UIButton *)UIHelper::seekWidgetByName(panel_musouTalent,"Button_explanation");
		btn_info->setTouchEnable(true);
		btn_info->addReleaseEvent(this, coco_releaseselector(MusouSkillTalent::InfoEvent));
		btn_info->setPressedActionEnabled(true);
		//升级
		UIButton *btn_upgrade = (UIButton *)UIHelper::seekWidgetByName(panel_musouTalent,"Button_upgrade");
		btn_upgrade->setTouchEnable(true);
		btn_upgrade->addReleaseEvent(this, coco_releaseselector(MusouSkillTalent::UpgradeEvent));
		btn_upgrade->setPressedActionEnabled(true);
		
		l_essenceValue = (UILabel*)UIHelper::seekWidgetByName(panel_musouTalent,"Label_3703");
		
		ImageView_select = (UIImageView *)UIHelper::seekWidgetByName(panel_musouTalent,"ImageView_talentSelect");
		ImageView_select->setAnchorPoint(ccp(0.5f,0.5f));
		ImageView_select->setScale(0.9f);
		ImageView_select->setVisible(true);

		//refresh data
		std::map<int,int>::iterator iter_musouTalent;
		for (iter_musouTalent = GameView::getInstance()->m_musouTalentList.begin(); iter_musouTalent != GameView::getInstance()->m_musouTalentList.end(); ++iter_musouTalent)
		{
			MusouTalentConfigData::IdAndLevel temp;
			temp.idx = iter_musouTalent->first;
			if (iter_musouTalent->second == 0)
			{
				temp.level = 1;
			}
			else
			{
				temp.level = iter_musouTalent->second;
			}

			std::map<MusouTalentConfigData::IdAndLevel,CMusouTalent*>::const_iterator cIter;
			cIter = MusouTalentConfigData::s_musouTalentMsg.find(temp);
			if (cIter == MusouTalentConfigData::s_musouTalentMsg.end()) // 没找到就是指向END了  
			{
			}
			else
			{
				CMusouTalent * temp = new CMusouTalent;
				temp->copyFrom(cIter->second);
				temp->set_level(iter_musouTalent->second);
				curMusouTalentList.push_back(temp);
			}
		}
		//test by yangjun 2014.3.25
// 		for (int i = 0;i<13;++i)
// 		{
// 			MusouTalentConfigData::IdAndLevel temp;
// 			temp.id = i+1;
// 			temp.level = 1;
// 			 
// 			std::map<MusouTalentConfigData::IdAndLevel,CMusouTalent*>::const_iterator cIter;
// 			cIter = MusouTalentConfigData::s_musouTalentMsg.find(temp);
// 			if (cIter == MusouTalentConfigData::s_musouTalentMsg.end()) // 没找到就是指向END了  
// 			{
// 			}
// 			else
// 			{
// 			 	CMusouTalent * temp = new CMusouTalent;
// 			 	temp->copyFrom(cIter->second);
// 			 	curMusouTalentList.push_back(temp);
// 			}
// 		}
		//refresh ui
		for (int i =0;i<curMusouTalentList.size();++i)
		//for (int i =0;i<8;++i)
		{
			if (i >= 12)
				continue;

			MusouTalentItem * musouTalentItem = MusouTalentItem::create(curMusouTalentList.at(i));
			musouTalentItem->setPosition(ccp(CoordinateVector.at(curMusouTalentList.at(i)->get_idx()-1).x,CoordinateVector.at(curMusouTalentList.at(i)->get_idx()-1).y));
			musouTalentItem->setTag(MusouSkillTalentItemBaseTag+curMusouTalentList.at(i)->get_idx());
			layer_musouTalent->addChild(musouTalentItem);
		}

		if (curMusouTalentList.size()>0)
		{
			//默认选中第一个
			setSelectImagePos(curSelectIndex);
			RefreshTalentInfo(curMusouTalentList.at(0));
			RefreshTalentItemLevel(curMusouTalentList.at(0));
			RefreshEssenceValue();

		}

		return true;
	}
	return false;
}

void MusouSkillTalent::onEnter()
{
	UIScene::onEnter();
}

void MusouSkillTalent::onExit()
{
	UIScene::onExit();
}


void MusouSkillTalent::setVisible( bool visible )
{
	panel_musouTalent->setVisible(visible);
	layer_musouTalent->setVisible(visible);
}

void MusouSkillTalent::InfoEvent( CCObject *pSender )
{
	//龙魂天赋信息描述
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	std::map<int,std::string>::const_iterator cIter;
	cIter = SystemInfoConfigData::s_systemInfoList.find(3);
	if (cIter == SystemInfoConfigData::s_systemInfoList.end()) // 没找到就是指向END了  
	{
		return ;
	}
	else
	{
		if(GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesDetailInfo) == NULL)
		{
			ShowSystemInfo * showSystemInfo = ShowSystemInfo::create(SystemInfoConfigData::s_systemInfoList[3].c_str());
			GameView::getInstance()->getMainUIScene()->addChild(showSystemInfo,0,kTagStrategiesDetailInfo);
			showSystemInfo->ignoreAnchorPointForPosition(false);
			showSystemInfo->setAnchorPoint(ccp(0.5f, 0.5f));
			showSystemInfo->setPosition(ccp(winSize.width/2, winSize.height/2));
		}
	}
}

void MusouSkillTalent::UpgradeEvent( CCObject *pSender )
{
	for (int i =0;i < curMusouTalentList.size();++i)
	{
		if (curSelectIndex == curMusouTalentList.at(i)->get_idx())
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5121,(void *)curMusouTalentList.at(i)->get_idx());
		}
	}
}

void MusouSkillTalent::setSelectImagePos( int index )
{
	ImageView_select->setVisible(true);
	ImageView_select->setPosition(ccp(CoordinateVector.at(index-1).x+42,CoordinateVector.at(index-1).y+50));
}

void MusouSkillTalent::RefreshTalentItemLevel( CMusouTalent * musouTalent )
{
	MusouTalentItem * temp  = (MusouTalentItem*)layer_musouTalent->getChildByTag(MusouSkillTalentItemBaseTag+musouTalent->get_idx());
	if (!temp)
		return;

	temp->RefreshLevel(musouTalent->get_level());
	temp->RefreshCurMusouTalent(musouTalent);
	
}

void MusouSkillTalent::RefreshTalentInfo( CMusouTalent * musouTalent )
{
	if (layer_musouTalent->getChildByTag(M_SCROLLVIEWTAG) != NULL)
	{
		layer_musouTalent->getChildByTag(M_SCROLLVIEWTAG)->removeFromParent();
	}

	int scroll_height = 0;
	//技能名称
	CCSprite * s_nameFrame = CCSprite::create("res_ui/moji_0.png");
	s_nameFrame->setScaleY(0.6f);
	CCLabelTTF * l_name = CCLabelTTF::create(musouTalent->get_name().c_str(),APP_FONT_NAME,18);
	l_name->setColor(ccc3(196,255,68));
	int zeroLineHeight = s_nameFrame->getContentSize().height;
	scroll_height = zeroLineHeight;
	//
	CCSprite * s_first_line = CCSprite::create("res_ui/henggang.png");
	s_first_line->setScaleX(0.9f);
	int zeroLineHeight_1 = zeroLineHeight + s_first_line->getContentSize().height+6;
	scroll_height = zeroLineHeight_1;

	//简介
	CCLabelTTF * t_des = CCLabelTTF::create(musouTalent->get_description().c_str(),APP_FONT_NAME,16,CCSizeMake(230, 0 ), kCCTextAlignmentLeft);
	t_des->setColor(ccc3(30,255,0));
	int firstLineHeight = zeroLineHeight_1+t_des->getContentSize().height;
	scroll_height = firstLineHeight;

	/////////////////////////////////////////////////////
	//int fifthLineHeight = 0;
	int secondLineHeight = 0;
	//int sixthLineHeight = 0;
	int thirdLineHeight = 0;
	//int seventhLineHeight = 0;
	int fouthLineHeight = 0;
	//int seventhLineHeight_1 = 0;
	int fouthLineHeight_1 = 0;
	//int eighthLineHeight = 0;
	int fifthLineHeight = 0;
	//int ninthLineHeight = 0;
	int sixthLineHeight = 0;
	if (musouTalent->get_level()<10)
	{
		//下一级
		if (musouTalent->get_level() == 0)
		{
			secondLineHeight = firstLineHeight;
			scroll_height = secondLineHeight;

			thirdLineHeight = secondLineHeight;
			scroll_height = thirdLineHeight;
			/////////////////////////////////////////////////////
			//升级需求
			s_upgradeNeed = CCSprite::create("res_ui/moji_0.png");
			s_upgradeNeed->setScaleY(0.6f);
			//const char *shenjixuqiu  = ((CCString*)strings->objectForKey("skillinfo_shengjixuqiu"))->m_sString.c_str();
			const char *xuexixuqiu = StringDataManager::getString("skillinfo_xuexixuqiu");
			char* skillinfo_xuexixuqiu =const_cast<char*>(xuexixuqiu);
			l_upgradeNeed = CCLabelTTF::create(skillinfo_xuexixuqiu,APP_FONT_NAME,18);
			l_upgradeNeed->setColor(ccc3(196,255,68));
			fouthLineHeight = thirdLineHeight + s_upgradeNeed->getContentSize().height;
			scroll_height = fouthLineHeight;

			//////////////////////////////////////////////////////
			//
			s_second_line = CCSprite::create("res_ui/henggang.png");
			s_second_line->setScaleX(0.9f);
			fouthLineHeight_1 = fouthLineHeight + s_first_line->getContentSize().height+7;
			scroll_height = fouthLineHeight_1;

			/////////////////////////////////////////////////////
			//主角等级
			if (musouTalent->get_require_level()>0)
			{
				std::string str_lv_des = StringDataManager::getString("skillinfo_zhujuedengji");
				str_lv_des.append(StringDataManager::getString("skillinfo_maohao"));
				l_playerLv = CCLabelTTF::create(str_lv_des.c_str(),APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
				l_playerLv->setColor(ccc3(212,255,151));
				char s_required_level[5];
				sprintf(s_required_level,"%d",musouTalent->get_require_level());
				l_playerLvValue = CCLabelTTF::create(s_required_level,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
				l_playerLvValue->setColor(ccc3(212,255,151));

				fifthLineHeight = fouthLineHeight_1 + l_playerLv->getContentSize().height;
				scroll_height = fifthLineHeight;

				if (GameView::getInstance()->myplayer->getActiveRole()->level()<musouTalent->get_require_level())  //人物等级未达到技能升级要求
				{
					l_playerLv->setColor(ccc3(255,51,51));
					l_playerLvValue->setColor(ccc3(255,51,51));
				}
			}
			else
			{
				fifthLineHeight = fouthLineHeight_1;
				scroll_height = fifthLineHeight;
			}
			//龙魂精华
			if (musouTalent->get_require_essence()>0)
			{
				const char *longhunjinghua = StringDataManager::getString("musouTalentSkillinfo_musou");
				char* skillinfo_longhunjinghua =const_cast<char*>(longhunjinghua);
				l_skillPoint = CCLabelTTF::create(skillinfo_longhunjinghua,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
				l_skillPoint->setColor(ccc3(212,255,151));
				char s_required_point[10];
				sprintf(s_required_point,"%d",musouTalent->get_require_essence());
				l_skillPointValue = CCLabelTTF::create(s_required_point,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
				l_skillPointValue->setColor(ccc3(212,255,151));

				sixthLineHeight = fifthLineHeight + l_skillPoint->getContentSize().height;
				scroll_height = sixthLineHeight;

// 				if(GameView::getInstance()->myplayer->player->skillpoint()<curFightSkill->getCFightSkill()->get_required_point())//技能点不足
// 				{
// 					l_skillPoint->setColor(ccc3(255,51,51));
// 					l_skillPointValue->setColor(ccc3(255,51,51));
// 				}
			}
			else
			{
				sixthLineHeight = fifthLineHeight;
				scroll_height = sixthLineHeight;
			}
		}
		else
		{

			MusouTalentConfigData::IdAndLevel temp;
			temp.idx = musouTalent->get_idx();
			temp.level = musouTalent->get_level()+1;
			std::map<MusouTalentConfigData::IdAndLevel,CMusouTalent*>::const_iterator cIter;
			cIter = MusouTalentConfigData::s_musouTalentMsg.find(temp);
			if (cIter == MusouTalentConfigData::s_musouTalentMsg.end()) // 没找到就是指向END了  
			{
				
			}
			else
			{
				CMusouTalent * nextMusouTalent = new CMusouTalent();
				nextMusouTalent->copyFrom(cIter->second);

				const char *xiayiji = StringDataManager::getString("skillinfo_xiayiji");
				char* skillinfo_xiayiji =const_cast<char*>(xiayiji);
				l_nextLv = CCLabelTTF::create(skillinfo_xiayiji,APP_FONT_NAME,16,CCSizeMake(100, 0 ), kCCTextAlignmentLeft);
				l_nextLv->setColor(ccc3(0,255,255));

				secondLineHeight = firstLineHeight+l_nextLv->getContentSize().height*2;
				scroll_height = secondLineHeight;
				/////////////////////////////////////////////////////
				t_nextLvDes = CCLabelTTF::create(nextMusouTalent->get_description().c_str(),APP_FONT_NAME,16,CCSizeMake(230, 0 ), kCCTextAlignmentLeft);
				t_nextLvDes->setColor(ccc3(0,255,255));

				thirdLineHeight = secondLineHeight+t_nextLvDes->getContentSize().height;
				scroll_height = thirdLineHeight;
				/////////////////////////////////////////////////////
				//升级需求
				s_upgradeNeed = CCSprite::create("res_ui/moji_0.png");
				s_upgradeNeed->setScaleY(0.6f);
				//const char *shenjixuqiu  = ((CCString*)strings->objectForKey("skillinfo_shengjixuqiu"))->m_sString.c_str();
				const char *shenjixuqiu = StringDataManager::getString("skillinfo_shengjixuqiu");
				char* skillinfo_shenjixuqiu =const_cast<char*>(shenjixuqiu);
				l_upgradeNeed = CCLabelTTF::create(skillinfo_shenjixuqiu,APP_FONT_NAME,18);
				l_upgradeNeed->setColor(ccc3(196,255,68));
				fouthLineHeight = thirdLineHeight + s_upgradeNeed->getContentSize().height;
				scroll_height = fouthLineHeight;

				//////////////////////////////////////////////////////
				//
				s_second_line = CCSprite::create("res_ui/henggang.png");
				s_second_line->setScaleX(0.9f);
				fouthLineHeight_1 = fouthLineHeight + s_first_line->getContentSize().height+7;
				scroll_height = fouthLineHeight_1;

				/////////////////////////////////////////////////////
				//主角等级
				if (nextMusouTalent->get_require_level()>0)
				{
					std::string str_lv_des = StringDataManager::getString("skillinfo_zhujuedengji");
					str_lv_des.append(StringDataManager::getString("skillinfo_maohao"));
					l_playerLv = CCLabelTTF::create(str_lv_des.c_str(),APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
					l_playerLv->setColor(ccc3(212,255,151));
					char s_next_required_level[5];
					sprintf(s_next_required_level,"%d",nextMusouTalent->get_require_level());
					l_playerLvValue = CCLabelTTF::create(s_next_required_level,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
					l_playerLvValue->setColor(ccc3(212,255,151));

					fifthLineHeight = fouthLineHeight_1 + l_playerLv->getContentSize().height;
					scroll_height = fifthLineHeight;

					if (GameView::getInstance()->myplayer->getActiveRole()->level()<nextMusouTalent->get_require_level())  //人物等级未达到技能升级要求
					{
						l_playerLv->setColor(ccc3(255,51,51));
						l_playerLvValue->setColor(ccc3(255,51,51));
					}
				}
				else
				{
					fifthLineHeight = fouthLineHeight_1;
					scroll_height = fifthLineHeight;
				}
				//龙魂精华
				if (nextMusouTalent->get_require_essence()>0)
				{
					const char *longhunjinghua = StringDataManager::getString("musouTalentSkillinfo_musou");
					char* skillinfo_longhunjinghua =const_cast<char*>(longhunjinghua);
					l_skillPoint = CCLabelTTF::create(skillinfo_longhunjinghua,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
					l_skillPoint->setColor(ccc3(212,255,151));
					char s_next_required_musou[10];
					sprintf(s_next_required_musou,"%d",nextMusouTalent->get_require_essence());
					l_skillPointValue = CCLabelTTF::create(s_next_required_musou,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
					l_skillPointValue->setColor(ccc3(212,255,151));

					sixthLineHeight = fifthLineHeight + l_skillPoint->getContentSize().height;
					scroll_height = sixthLineHeight;

// 					if(GameView::getInstance()->myplayer->player->skillpoint()<curFightSkill->getCFightSkill()->get_next_required_point())//技能点不足
// 					{
// 						l_skillPoint->setColor(ccc3(255,51,51));
// 						l_skillPointValue->setColor(ccc3(255,51,51));
// 					}
				}
				else
				{
					sixthLineHeight = fifthLineHeight;
					scroll_height = sixthLineHeight;
				}
				
			}
		}
	}

	CCScrollView * m_scrollView = CCScrollView::create(CCSizeMake(243,240));
	m_scrollView->setViewSize(CCSizeMake(243, 240));
	m_scrollView->ignoreAnchorPointForPosition(false);
	m_scrollView->setTouchEnabled(true);
	m_scrollView->setDirection(kCCScrollViewDirectionVertical);
	m_scrollView->setAnchorPoint(ccp(0,0));
	m_scrollView->setPosition(ccp(504,159));
	m_scrollView->setBounceable(true);
	m_scrollView->setClippingToBounds(true);
	m_scrollView->setTag(M_SCROLLVIEWTAG);
	layer_musouTalent->addChild(m_scrollView);

	if (scroll_height > 212)
	{
		m_scrollView->setContentSize(CCSizeMake(243,scroll_height));
		m_scrollView->setContentOffset(ccp(0,240-scroll_height));  
	}
	else
	{
		m_scrollView->setContentSize(ccp(243,240));
	}
	m_scrollView->setClippingToBounds(true);

	s_nameFrame->ignoreAnchorPointForPosition(false);
	s_nameFrame->setAnchorPoint(ccp(0,0));
	m_scrollView->addChild(s_nameFrame);
	s_nameFrame->setPosition(ccp(8,m_scrollView->getContentSize().height - zeroLineHeight));

	m_scrollView->addChild(l_name);
	l_name->setPosition(ccp(15,m_scrollView->getContentSize().height - zeroLineHeight));

	m_scrollView->addChild(s_first_line);
	s_first_line->setPosition(ccp(5,m_scrollView->getContentSize().height - zeroLineHeight_1+5));

	t_des->ignoreAnchorPointForPosition( false );  
	t_des->setAnchorPoint( ccp( 0, 0 ) );  
	m_scrollView->addChild(t_des);
	t_des->setPosition(ccp(8,m_scrollView->getContentSize().height - firstLineHeight));


	if (musouTalent->get_level()<10)
	{
		//下一级
		if (musouTalent->get_level() == 0)
		{
			s_upgradeNeed->ignoreAnchorPointForPosition( false );  
			s_upgradeNeed->setAnchorPoint( ccp( 0, 0 ) );  
			m_scrollView->addChild(s_upgradeNeed);
			s_upgradeNeed->setPosition(ccp(8,m_scrollView->getContentSize().height - fouthLineHeight));

			m_scrollView->addChild(l_upgradeNeed);
			l_upgradeNeed->setPosition(ccp(15,m_scrollView->getContentSize().height - fouthLineHeight));

			m_scrollView->addChild(s_second_line);
			s_second_line->setPosition(ccp(5,m_scrollView->getContentSize().height - fouthLineHeight_1+5));

			//主角等级
			if (musouTalent->get_require_level()>0)
			{
				l_playerLv->ignoreAnchorPointForPosition( false );  
				l_playerLv->setAnchorPoint( ccp( 0, 0 ) );  
				m_scrollView->addChild(l_playerLv);
				l_playerLv->setPosition(ccp(8,m_scrollView->getContentSize().height - fifthLineHeight));

				l_playerLvValue->ignoreAnchorPointForPosition( false );  
				l_playerLvValue->setAnchorPoint( ccp( 0, 0 ) );  
				m_scrollView->addChild(l_playerLvValue);
				l_playerLvValue->setPosition(ccp(8+l_playerLv->getContentSize().width,m_scrollView->getContentSize().height - fifthLineHeight));
			}
			//龙魂精华
			if (musouTalent->get_require_essence()>0)
			{
				l_skillPoint->ignoreAnchorPointForPosition( false );  
				l_skillPoint->setAnchorPoint( ccp( 0, 0 ) );  
				m_scrollView->addChild(l_skillPoint);
				l_skillPoint->setPosition(ccp(8,m_scrollView->getContentSize().height - sixthLineHeight));

				l_skillPointValue->ignoreAnchorPointForPosition( false );  
				l_skillPointValue->setAnchorPoint( ccp( 0, 0 ) );  
				m_scrollView->addChild(l_skillPointValue);
				l_skillPointValue->setPosition(ccp(8+l_skillPoint->getContentSize().width,m_scrollView->getContentSize().height - sixthLineHeight));
			}
		}
		else
		{
			l_nextLv->ignoreAnchorPointForPosition( false );  
			l_nextLv->setAnchorPoint( ccp( 0, 0 ) );  
			m_scrollView->addChild(l_nextLv);
			l_nextLv->setPosition(ccp(8,m_scrollView->getContentSize().height - secondLineHeight));

			t_nextLvDes->ignoreAnchorPointForPosition( false );  
			t_nextLvDes->setAnchorPoint( ccp( 0, 0 ) );  
			m_scrollView->addChild(t_nextLvDes);
			t_nextLvDes->setPosition(ccp(8,m_scrollView->getContentSize().height - thirdLineHeight));

			s_upgradeNeed->ignoreAnchorPointForPosition( false );  
			s_upgradeNeed->setAnchorPoint( ccp( 0, 0 ) );  
			m_scrollView->addChild(s_upgradeNeed);
			s_upgradeNeed->setPosition(ccp(8,m_scrollView->getContentSize().height - fouthLineHeight));

			m_scrollView->addChild(l_upgradeNeed);
			l_upgradeNeed->setPosition(ccp(15,m_scrollView->getContentSize().height - fouthLineHeight));

			m_scrollView->addChild(s_second_line);
			s_second_line->setPosition(ccp(5,m_scrollView->getContentSize().height - fouthLineHeight_1+5));

			//主角等级
			if (musouTalent->get_require_level()>0)
			{
				l_playerLv->ignoreAnchorPointForPosition( false );  
				l_playerLv->setAnchorPoint( ccp( 0, 0 ) );  
				m_scrollView->addChild(l_playerLv);
				l_playerLv->setPosition(ccp(8,m_scrollView->getContentSize().height - fifthLineHeight));

				l_playerLvValue->ignoreAnchorPointForPosition( false );  
				l_playerLvValue->setAnchorPoint( ccp( 0, 0 ) );  
				m_scrollView->addChild(l_playerLvValue);
				l_playerLvValue->setPosition(ccp(8+l_playerLv->getContentSize().width,m_scrollView->getContentSize().height - fifthLineHeight));
			}
			//龙魂精华
			if (musouTalent->get_require_essence()>0)
			{
				l_skillPoint->ignoreAnchorPointForPosition( false );  
				l_skillPoint->setAnchorPoint( ccp( 0, 0 ) );  
				m_scrollView->addChild(l_skillPoint);
				l_skillPoint->setPosition(ccp(8,m_scrollView->getContentSize().height - sixthLineHeight));

				l_skillPointValue->ignoreAnchorPointForPosition( false );  
				l_skillPointValue->setAnchorPoint( ccp( 0, 0 ) );  
				m_scrollView->addChild(l_skillPointValue);
				l_skillPointValue->setPosition(ccp(8+l_skillPoint->getContentSize().width,m_scrollView->getContentSize().height - sixthLineHeight));
			}
		}
	}
}

void MusouSkillTalent::RefreshEssenceValue()
{
	char s_essence[20];
	sprintf(s_essence,"%d",GameView::getInstance()->myplayer->getMusouEssence());
	l_essenceValue->setText(s_essence);
}

void MusouSkillTalent::AddBonusSpecialEffect( CMusouTalent * musouTalent )
{
	MusouTalentItem * temp  = (MusouTalentItem*)layer_musouTalent->getChildByTag(MusouSkillTalentItemBaseTag+musouTalent->get_idx());
	if (!temp)
		return;

	// Bonus Special Effect
	CCNodeRGBA* pNode = BonusSpecialEffect::create();
	pNode->setScale(0.7f);
	pNode->setPosition(ccp(temp->getPosition().x+45,temp->getPosition().y+50));
	layer_musouTalent->addChild(pNode);
}


/////////////////////////////////////////////////////////

MusouTalentItem::MusouTalentItem()
{
}

MusouTalentItem::~MusouTalentItem()
{
	delete m_curMusouTalent;
}

MusouTalentItem* MusouTalentItem::create( CMusouTalent * musouTalent )
{
	MusouTalentItem * musouTalentItem = new MusouTalentItem();
	if (musouTalentItem && musouTalentItem->init(musouTalent))
	{
		musouTalentItem->autorelease();
		return musouTalentItem;
	}
	CC_SAFE_DELETE(musouTalentItem);
	return NULL;
}

bool MusouTalentItem::init( CMusouTalent * musouTalent )
{
	if (UIScene::init())
	{
		// 该UILayer位于CCTableView之内，为了正确响应TableView的拖动事件，故设置为不吞掉触摸事件
		m_pUiLayer->setSwallowsTouches(false);

		m_curMusouTalent = new CMusouTalent();
		m_curMusouTalent->copyFrom(musouTalent);

		//框
		std::string frameColorPath = "res_ui/jineng/jinengdi_1.png";
		btn_bgFrame = UIButton::create();
		btn_bgFrame->setTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
		btn_bgFrame->setAnchorPoint(ccp(0.5f,0.5f));
		btn_bgFrame->setPosition(ccp(42,50));
		btn_bgFrame->setTouchEnable(true);
		btn_bgFrame->addReleaseEvent(this,coco_releaseselector(MusouTalentItem::DoThing));
		m_pUiLayer->addWidget(btn_bgFrame);

		std::string skillIconPath = "res_ui/jineng_icon/";
		if (strcmp(musouTalent->get_icon().c_str(),"") == 0)
		{
			skillIconPath.append("jineng_2");
		}
		else
		{
			skillIconPath.append(musouTalent->get_icon().c_str());
		}
		skillIconPath.append(".png");

		UIButton * imageView_skill = UIButton::create();
		imageView_skill->setTextures(skillIconPath.c_str(),skillIconPath.c_str(),"");
		imageView_skill->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_skill->setPosition(ccp(btn_bgFrame->getPosition().x,btn_bgFrame->getPosition().y));
		imageView_skill->setTouchEnable(true);
		//imageView_skill->setScale(0.9f);
		imageView_skill->addReleaseEvent(this,coco_releaseselector(MusouTalentItem::DoThing));
		m_pUiLayer->addWidget(imageView_skill);

		//高光
		UIImageView * image_highLight = UIImageView::create();
		image_highLight->setTexture("res_ui/jineng/gaoguang_1.png");
		image_highLight->setAnchorPoint(ccp(.5f,.5f));
		image_highLight->setPosition(ccp(btn_bgFrame->getPosition().x,btn_bgFrame->getPosition().y+2));
		m_pUiLayer->addWidget(image_highLight);

		//名字底
		UIImageView * imageView_nameFrame = UIImageView::create();
		imageView_nameFrame->setTexture("res_ui/LV4_di80.png");
		imageView_nameFrame->setScale9Enable(true);
		imageView_nameFrame->setScale9Size(CCSizeMake(75,23));
		imageView_nameFrame->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_nameFrame->setPosition(ccp(42,9));
		m_pUiLayer->addWidget(imageView_nameFrame);
		//名字
		UILabel * l_name = UILabel::create();
		l_name->setText(musouTalent->get_name().c_str());
		l_name->setFontName(APP_FONT_NAME);
		l_name->setFontSize(16);
		l_name->setAnchorPoint(ccp(0.5f,0.5f));
		l_name->setPosition(ccp(42,9));
		m_pUiLayer->addWidget(l_name);
		//等级底
		UIImageView * imageView_lvFrame = UIImageView::create();
		imageView_lvFrame->setTexture("res_ui/jineng/jineng_zhezhao.png");
		imageView_lvFrame->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_lvFrame->setPosition(ccp(42,35));
		m_pUiLayer->addWidget(imageView_lvFrame);
		//等级
		std::string str_lvstr = "";
		char str_lv [10];
		sprintf(str_lv,"%d",musouTalent->get_level());
		str_lvstr.append(str_lv);
		str_lvstr.append("/10");
		l_lv = UILabelBMFont::create();
		l_lv->setFntFile("res_ui/font/ziti_3.fnt");
		l_lv->setScale(0.6f);
		l_lv->setText(str_lvstr.c_str());
		l_lv->setAnchorPoint(ccp(0.5f,0.5f));
		l_lv->setPosition(ccp(0,0));
		imageView_lvFrame->addChild(l_lv);

		this->setTouchEnabled(false);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(CCSizeMake(83,143));
		return true;
	}
	return false;
}

void MusouTalentItem::DoThing( CCObject *pSender )
{
	SkillScene *skillScene = (SkillScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
	if (!skillScene)
		return;

	skillScene->musouSkillTalent->RefreshTalentInfo(m_curMusouTalent);
	skillScene->musouSkillTalent->curSelectIndex = m_curMusouTalent->get_idx();
	skillScene->musouSkillTalent->setSelectImagePos(skillScene->musouSkillTalent->curSelectIndex);
}

//void MusouTalentItem::RefreshGeneralSkillInfo( std::string skillid )
//{
// 	std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GeneralsGameFightSkillList.begin();
// 	for ( ; it != GameView::getInstance()->GeneralsGameFightSkillList.end(); ++ it )
// 	{
// 		if (strcmp(skillid.c_str(),it->second->getId().c_str()) == 0)
// 		{
// 			GameFightSkill * gameFightSkill = new GameFightSkill();
// 			gameFightSkill->initSkill(it->second->getId(), it->second->getId(), it->second->getLevel(), GameActor::type_player);
// 			skillScene->musouSkillLayer->RefreshSkillInfo(it->second->getLevel(),gameFightSkill);
// 			delete gameFightSkill;
// 			break;
// 		}
// 	}	
//}

void MusouTalentItem::setFrameVisible( bool isVisible )
{
	btn_bgFrame->setVisible(isVisible);
}

void MusouTalentItem::RefreshLevel( int _value )
{
	m_curMusouTalent->set_level(_value);

	std::string str_lvstr = "";
	char str_lv [10];
	sprintf(str_lv,"%d",_value);
	str_lvstr.append(str_lv);
	str_lvstr.append("/10");
	l_lv->setText(str_lvstr.c_str());
}

void MusouTalentItem::RefreshCurMusouTalent( CMusouTalent * temp )
{
	m_curMusouTalent->copyFrom(temp);
}

