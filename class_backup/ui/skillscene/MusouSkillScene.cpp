#include "MusouSkillScene.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "GameView.h"
#include "../../gamescene_state/skill/GameFightSkill.h"
#include "AppMacros.h"
#include "../../ui/extensions/CCMoveableMenu.h"
#include "../../utils/StaticDataManager.h"
#include "GeneralMusouSkillListUI.h"
#include "SkillScene.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/CShortCut.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../utils/StrUtils.h"

#define  M_SCROLLVIEW_TAG 621

#define  M_ROLEMUSOUSKILL_FLAT_TAG 631

MusouSkillScene::MusouSkillScene():
isRoleOrGeneral(true)
{
}


MusouSkillScene::~MusouSkillScene()
{
}

MusouSkillScene* MusouSkillScene::create()
{
	MusouSkillScene * musouSkillScene = new MusouSkillScene();
	if (musouSkillScene && musouSkillScene->init())
	{
		musouSkillScene->autorelease();
		return musouSkillScene;
	}
	CC_SAFE_DELETE(musouSkillScene);
	return NULL;
}

bool MusouSkillScene::init()
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		curSelectRoleSkillSkillId = "";

		//龙魂技能面板
		panel_musouSkill = (UIPanel*)UIHelper::seekWidgetByName(LoadSceneLayer::SkillSceneLayer,"Panel_generals_jineng");
		panel_musouSkill->setVisible(true);
		//龙魂技能
		layer_musouSkill= UILayer::create();
		addChild(layer_musouSkill);
		layer_musouSkill->setVisible(true);

		btn_set = (UIButton *)UIHelper::seekWidgetByName(panel_musouSkill,"Button_set");
		btn_set->setTouchEnable(true);
		btn_set->addReleaseEvent(this, coco_releaseselector(MusouSkillScene::SetEvent));
		btn_set->setPressedActionEnabled(true);

		btn_suggestSet = (UIButton *)UIHelper::seekWidgetByName(panel_musouSkill,"Button_suggestSet");
		btn_suggestSet->setTouchEnable(true);
		btn_suggestSet->addReleaseEvent(this, coco_releaseselector(MusouSkillScene::SuggestSetEvent));
		btn_suggestSet->setPressedActionEnabled(true);

		//武将技能列表界面
	    generalMusouSkillListUI = GeneralMusouSkillListUI::create();
		layer_musouSkill->addChild(generalMusouSkillListUI);
		generalMusouSkillListUI->setPosition(70,41);

		//简介
		std::string str_info;
		std::map<int,std::string>::const_iterator cIter;
		cIter = SystemInfoConfigData::s_systemInfoList.find(2);
		if (cIter == SystemInfoConfigData::s_systemInfoList.end()) // 没找到就是指向END了  
		{
			str_info.append("");
		}
		else
		{
			str_info.append(SystemInfoConfigData::s_systemInfoList[2].c_str());
		}
		CCLabelTTF * l_des = CCLabelTTF::create(str_info.c_str(),APP_FONT_NAME,16,CCSizeMake(230, 0 ), kCCTextAlignmentLeft);
		//l_des->setColor(ccc3(47,93,13));
		//StrUtils::applyColor(str_info.c_str(),ccc3(47,93,13));
		//CCRichLabel * l_des = CCRichLabel::createWithString(str_info.c_str(),CCSizeMake(290, 0 ),NULL,NULL);
		//l_des->setScale(0.8f);
		
		int scroll_height = l_des->getContentSize().height*l_des->getScale();
		CCScrollView * m_scrollView = CCScrollView::create(CCSizeMake(250,120));
		m_scrollView->setViewSize(CCSizeMake(250, 120));
		m_scrollView->ignoreAnchorPointForPosition(false);
		m_scrollView->setTouchEnabled(true);
		m_scrollView->setDirection(kCCScrollViewDirectionVertical);
		m_scrollView->setAnchorPoint(ccp(0,0));
		m_scrollView->setPosition(ccp(504,97));
		m_scrollView->setBounceable(true);
		m_scrollView->setClippingToBounds(true);
		layer_musouSkill->addChild(m_scrollView);
		if (scroll_height > 120)
		{
			m_scrollView->setContentSize(CCSizeMake(250,scroll_height));
			m_scrollView->setContentOffset(ccp(0,120-scroll_height));  
		}
		else
		{
			m_scrollView->setContentSize(ccp(250,130));
		}
		m_scrollView->setClippingToBounds(true);

		m_scrollView->addChild(l_des);
		l_des->setPosition(ccp(8,m_scrollView->getContentSize().height - scroll_height-5));

		layer_upper= UILayer::create();
		addChild(layer_upper);
		layer_upper->setVisible(true);

		UIImageView * tableView_kuang = UIImageView::create();
		tableView_kuang->setTexture("res_ui/LV5_dikuang_miaobian1.png");
		tableView_kuang->setScale9Enable(true);
		tableView_kuang->setScale9Size(CCSizeMake(429,373));
		tableView_kuang->setCapInsets(CCRectMake(30,30,1,1));
		tableView_kuang->setAnchorPoint(ccp(0,0));
		tableView_kuang->setPosition(ccp(61,34));
		layer_upper->addWidget(tableView_kuang);

		return true;
	}
	return false;
}

void MusouSkillScene::onEnter()
{
	UIScene::onEnter();
}

void MusouSkillScene::onExit()
{
	UIScene::onExit();
}

void MusouSkillScene::setVisible( bool visible )
{
	panel_musouSkill->setVisible(visible);
	layer_musouSkill->setVisible(visible);
	layer_upper->setVisible(visible);
}

void MusouSkillScene::SetEvent( CCObject *pSender )
{
	if (isRoleOrGeneral)
	{
		if (strcmp(curSelectRoleSkillSkillId.c_str(),"") == 0)
			return;

		SkillScene::ReqForPreeless * temp = new SkillScene::ReqForPreeless();
		temp->skillid = curSelectRoleSkillSkillId;
		temp->grid = 4;
		temp->roleId = GameView::getInstance()->myplayer->getRoleId();
		temp->roleType = GameActor::type_player;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5075,temp);
		delete temp;
	}
	else
	{
		if(generalMusouSkillListUI->getSelectGeneralID() == -1)
			return;

		if (!generalMusouSkillListUI->getSelectShortCut()->has_skillpropid())
			return;

		SkillScene::ReqForPreeless * temp = new SkillScene::ReqForPreeless();
		temp->skillid = generalMusouSkillListUI->getSelectShortCut()->skillpropid();
		temp->grid = generalMusouSkillListUI->getSelectShortCut()->index();
		temp->roleId = generalMusouSkillListUI->getSelectGeneralID();
		temp->roleType = GameActor::type_pet;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5075,temp);
		delete temp;
	}
}

void MusouSkillScene::SuggestSetEvent( CCObject *pSender )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5076);

	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(pSender);
}


void MusouSkillScene::RefreshSkillInfo( int curLevel,GameFightSkill * gameFightSkill )
{
	if (layer_musouSkill->getChildByTag(M_SCROLLVIEW_TAG) != NULL)
	{
		layer_musouSkill->getChildByTag(M_SCROLLVIEW_TAG)->removeFromParent();
	}

	if (curLevel == 0 || gameFightSkill == NULL)
		return;

	int scroll_height = 0;
	//技能名称
	CCSprite * s_nameFrame = CCSprite::create("res_ui/moji_0.png");
	s_nameFrame->setScaleY(0.6f);
	CCLabelTTF * l_name = CCLabelTTF::create(gameFightSkill->getCBaseSkill()->name().c_str(),APP_FONT_NAME,18);
	l_name->setColor(ccc3(196,255,68));
	int zeroLineHeight = s_nameFrame->getContentSize().height;
	scroll_height = zeroLineHeight;

	//
	CCSprite * s_first_line = CCSprite::create("res_ui/henggang.png");
	s_first_line->setScaleX(2.0f);
	int zeroLineHeight_1 = zeroLineHeight + s_first_line->getContentSize().height+6;
	scroll_height = zeroLineHeight_1;

	//主动/被动
	CCLabelTTF * l_type;
	if (gameFightSkill->getCBaseSkill()->usemodel() == 0)
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_zhudongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		l_type = CCLabelTTF::create(skillinfo_zhudongjineng,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
	}
	else if (gameFightSkill->getCBaseSkill()->usemodel() == 2)
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_beidongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		l_type = CCLabelTTF::create(skillinfo_zhudongjineng,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
	}
	else
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_zhudongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		l_type = CCLabelTTF::create(skillinfo_zhudongjineng,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
	}
	l_type->setColor(ccc3(212,255,151));

	//等级
	//const char *dengji  = ((CCString*)strings->objectForKey("skillinfo_dengji"))->m_sString.c_str();
	const char *dengji = StringDataManager::getString("skillinfo_dengji");
	char* skillinfo_dengji =const_cast<char*>(dengji);
	CCLabelTTF * l_lv = CCLabelTTF::create(skillinfo_dengji,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
	l_lv->setColor(ccc3(212,255,151));
	char s_lv[5];
	if (curLevel == 0)
	{
		sprintf(s_lv,"%d",0);
	}
	else
	{
		sprintf(s_lv,"%d",gameFightSkill->getCFightSkill()->level());
	}
	CCLabelTTF * l_lvValue = CCLabelTTF::create(s_lv,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
	l_lvValue->setColor(ccc3(212,255,151));

	int firstLineHeight = zeroLineHeight_1 + l_type->getContentSize().height;
	scroll_height = firstLineHeight;
	///////////////////////2////////////////////////////
	//MP消耗
	//const char *mofaxiaohao  = ((CCString*)strings->objectForKey("skillinfo_mofaxiaohao"))->m_sString.c_str();
	const char *mofaxiaohao = StringDataManager::getString("skillinfo_mofaxiaohao");
	char* skillinfo_mofaxiaohao =const_cast<char*>(mofaxiaohao);
	CCLabelTTF * l_mp = CCLabelTTF::create(skillinfo_mofaxiaohao,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
	l_mp->setColor(ccc3(212,255,151));
	char s_mp[5];
	sprintf(s_mp,"%d",gameFightSkill->getCFightSkill()->mp());
	CCLabelTTF * l_mpValue = CCLabelTTF::create(s_mp,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
	l_mpValue->setColor(ccc3(212,255,151));
	//CD时间 
	//const char *lengqueshijian  = ((CCString*)strings->objectForKey("skillinfo_lengqueshijian"))->m_sString.c_str();
	const char *lengqueshijian = StringDataManager::getString("skillinfo_lengqueshijian");
	char* skillinfo_lengqueshijian =const_cast<char*>(lengqueshijian);
	CCLabelTTF * l_cd = CCLabelTTF::create(skillinfo_lengqueshijian,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
	l_cd->setColor(ccc3(212,255,151));
	char s_intervaltime[5];
	sprintf(s_intervaltime,"%d",gameFightSkill->getCFightSkill()->intervaltime()/1000);
	CCLabelTTF * l_cdValue = CCLabelTTF::create(s_intervaltime,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
	l_cdValue->setColor(ccc3(212,255,151));

	int secondLineHeight = firstLineHeight + l_mp->getContentSize().height;
	scroll_height = secondLineHeight;
	///////////////////////3////////////////////////////
	//施法距离
	//const char *shifajuli  = ((CCString*)strings->objectForKey("skillinfo_shifajuli"))->m_sString.c_str();
	const char *shifajuli = StringDataManager::getString("skillinfo_shifajuli");
	char* skillinfo_shifajuli =const_cast<char*>(shifajuli);
	CCLabelTTF * l_dis = CCLabelTTF::create(skillinfo_shifajuli,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
	l_dis->setColor(ccc3(212,255,151));
	char s_distance[5];
	sprintf(s_distance,"%d",gameFightSkill->getCFightSkill()->distance());
	CCLabelTTF * l_disValue = CCLabelTTF::create(s_distance,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
	l_disValue->setColor(ccc3(212,255,151));
	//职业
	//const char *zhiye  = ((CCString*)strings->objectForKey("skillinfo_zhiye"))->m_sString.c_str();
	const char *zhiye = StringDataManager::getString("skillinfo_zhiye");
	char* skillinfo_zhiye =const_cast<char*>(zhiye);
	CCLabelTTF * l_pro = CCLabelTTF::create(skillinfo_zhiye,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
	l_pro->setColor(ccc3(212,255,151));
	CCLabelTTF * l_proValue = CCLabelTTF::create(GameView::getInstance()->myplayer->getActiveRole()->profession().c_str(),APP_FONT_NAME,16,CCSizeMake(100, 0 ), kCCTextAlignmentLeft);
	l_proValue->setColor(ccc3(212,255,151));

	int thirdLineHeight = secondLineHeight + l_dis->getContentSize().height;
	scroll_height = thirdLineHeight;
	////////////////////4///////////////////////////////
	//简介
	CCLabelTTF * t_des = CCLabelTTF::create(gameFightSkill->getCFightSkill()->description().c_str(),APP_FONT_NAME,16,CCSizeMake(230, 0 ), kCCTextAlignmentLeft);
	t_des->setColor(ccc3(30,255,0));

	int fourthLineHeight = thirdLineHeight+t_des->getContentSize().height;
	scroll_height = fourthLineHeight;
	/////////////////////////////////////////////////////
	int fifthLineHeight = 0;
	int sixthLineHeight = 0;
	int seventhLineHeight = 0;
	int seventhLineHeight_1 = 0;
	int eighthLineHeight = 0;
	int ninthLineHeight = 0;
	int tenthLineHeight = 0;
	int eleventhLineHeight = 0;
	if (gameFightSkill->getLevel()<gameFightSkill->getCBaseSkill()->maxlevel())
	{
		//下一级
			const char *xiayiji = StringDataManager::getString("skillinfo_xiayiji");
			char* skillinfo_xiayiji =const_cast<char*>(xiayiji);
			l_nextLv = CCLabelTTF::create(skillinfo_xiayiji,APP_FONT_NAME,16,CCSizeMake(100, 0 ), kCCTextAlignmentLeft);
			l_nextLv->setColor(ccc3(0,255,255));

			fifthLineHeight = fourthLineHeight+l_nextLv->getContentSize().height*2;
			scroll_height = fifthLineHeight;
			/////////////////////////////////////////////////////
			t_nextLvDes = CCLabelTTF::create(gameFightSkill->getCFightSkill()->get_next_description().c_str(),APP_FONT_NAME,16,CCSizeMake(230, 0 ), kCCTextAlignmentLeft);
			t_nextLvDes->setColor(ccc3(0,255,255));

			sixthLineHeight = fifthLineHeight+t_nextLvDes->getContentSize().height;
			scroll_height = sixthLineHeight;
			/////////////////////////////////////////////////////
			//升级需求
			s_upgradeNeed = CCSprite::create("res_ui/moji_0.png");
			s_upgradeNeed->setScaleY(0.6f);
			//const char *shenjixuqiu  = ((CCString*)strings->objectForKey("skillinfo_shengjixuqiu"))->m_sString.c_str();
			const char *shenjixuqiu = StringDataManager::getString("skillinfo_shengjixuqiu");
			char* skillinfo_shenjixuqiu =const_cast<char*>(shenjixuqiu);
			l_upgradeNeed = CCLabelTTF::create(skillinfo_shenjixuqiu,APP_FONT_NAME,18);
			l_upgradeNeed->setColor(ccc3(196,255,68));
			seventhLineHeight = sixthLineHeight + s_upgradeNeed->getContentSize().height;
			scroll_height = seventhLineHeight;

			//////////////////////////////////////////////////////
			//
			s_second_line = CCSprite::create("res_ui/henggang.png");
			s_second_line->setScaleX(2.0f);
			seventhLineHeight_1 = seventhLineHeight + s_first_line->getContentSize().height+7;
			scroll_height = seventhLineHeight_1;

			/////////////////////////////////////////////////////
			//主角等级
			if (gameFightSkill->getCFightSkill()->get_next_required_level()>0)
			{
				//CCDictionary *strings = CCDictionary::createWithContentsOfFile("res_ui/font/strings.xml");
				//const char *zhujuedengji  = ((CCString*)strings->objectForKey("skillinfo_zhujuedengji"))->m_sString.c_str();
				const char *zhujuedengji = StringDataManager::getString("skillinfo_zhujuedengji");
				char* skillinfo_zhujuedengji =const_cast<char*>(zhujuedengji);
				l_playerLv = CCLabelTTF::create(skillinfo_zhujuedengji,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
				l_playerLv->setColor(ccc3(212,255,151));
				char s_next_required_level[5];
				sprintf(s_next_required_level,"%d",gameFightSkill->getCFightSkill()->get_next_required_level());
				l_playerLvValue = CCLabelTTF::create(s_next_required_level,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
				l_playerLvValue->setColor(ccc3(212,255,151));

				eighthLineHeight = seventhLineHeight_1 + l_playerLv->getContentSize().height;
				scroll_height = eighthLineHeight;

				if (GameView::getInstance()->myplayer->getActiveRole()->level()<gameFightSkill->getCFightSkill()->get_next_required_level())  //人物等级未达到技能升级要求
				{
					l_playerLv->setColor(ccc3(255,51,51));
					l_playerLvValue->setColor(ccc3(255,51,51));
				}
			}
			else
			{
				eighthLineHeight = seventhLineHeight_1;
				scroll_height = eighthLineHeight;
			}
			//技能点
			if (gameFightSkill->getCFightSkill()->get_next_required_point()>0)
			{
				//CCDictionary *strings = CCDictionary::createWithContentsOfFile("res_ui/font/strings.xml");
				//const char *jinengdian  = ((CCString*)strings->objectForKey("skillinfo_jinengdian"))->m_sString.c_str();
				const char *jinengdian = StringDataManager::getString("skillinfo_jinengdian");
				char* skillinfo_jinengdian =const_cast<char*>(jinengdian);
				l_skillPoint = CCLabelTTF::create(skillinfo_jinengdian,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
				l_skillPoint->setColor(ccc3(212,255,151));
				char s_next_required_point[5];
				sprintf(s_next_required_point,"%d",gameFightSkill->getCFightSkill()->get_next_required_point());
				l_skillPointValue = CCLabelTTF::create(s_next_required_point,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
				l_skillPointValue->setColor(ccc3(212,255,151));

				ninthLineHeight = eighthLineHeight + l_skillPoint->getContentSize().height;
				scroll_height = ninthLineHeight;

				if(GameView::getInstance()->myplayer->player->skillpoint()<gameFightSkill->getCFightSkill()->get_next_required_point())//技能点不足
				{
					l_skillPoint->setColor(ccc3(255,51,51));
					l_skillPointValue->setColor(ccc3(255,51,51));
				}
			}
			else
			{
				ninthLineHeight = eighthLineHeight;
				scroll_height = ninthLineHeight;
			}
			//XXX技能
			if (gameFightSkill->getCFightSkill()->get_next_pre_skill() != "")
			{
				std::string skillname = "";
				CBaseSkill * temp = StaticDataBaseSkill::s_baseSkillData[gameFightSkill->getCFightSkill()->get_next_pre_skill()];
				skillname.append(temp->name().c_str());
				skillname.append(": ");
				l_preSkill = CCLabelTTF::create(skillname.c_str(),APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
				l_preSkill->setColor(ccc3(212,255,151));
				char s_next_pre_skill_level[5];
				sprintf(s_next_pre_skill_level,"%d",gameFightSkill->getCFightSkill()->get_next_pre_skill_level());
				l_preSkillValue = CCLabelTTF::create(s_next_pre_skill_level,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
				l_preSkillValue->setColor(ccc3(212,255,151));

				tenthLineHeight = ninthLineHeight + l_preSkill->getContentSize().height;
				scroll_height = tenthLineHeight;

				std::map<std::string,GameFightSkill*>::const_iterator cIter;
				cIter = GameView::getInstance()->GameFightSkillList.find(gameFightSkill->getCFightSkill()->get_next_pre_skill());
				if (cIter == GameView::getInstance()->GameFightSkillList.end()) // 没找到就是指向END了  
				{
				}
				else
				{
					GameFightSkill * gameFightSkillStuded = GameView::getInstance()->GameFightSkillList[gameFightSkill->getCFightSkill()->get_next_pre_skill()];
					if (gameFightSkillStuded)
					{
						if (gameFightSkillStuded->getLevel() < gameFightSkill->getCFightSkill()->get_next_pre_skill_level())
						{
							l_preSkill->setColor(ccc3(255,51,51));
							l_preSkillValue->setColor(ccc3(255,51,51));
						}
					}
				}
			}
			else
			{
				tenthLineHeight = ninthLineHeight;
				scroll_height = tenthLineHeight;
			}
			//金币
			if (gameFightSkill->getCFightSkill()->get_next_required_gold()>0)
			{
				//CCDictionary *strings = CCDictionary::createWithContentsOfFile("res_ui/font/strings.xml");
				//const char *jinbi  = ((CCString*)strings->objectForKey("skillinfo_jinbi"))->m_sString.c_str();
				const char *jinbi = StringDataManager::getString("skillinfo_jinbi");
				char* skillinfo_jinbi =const_cast<char*>(jinbi);
				l_gold = CCLabelTTF::create(skillinfo_jinbi,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
				l_gold->setColor(ccc3(212,255,151));
				char s_next_required_gold[20];
				sprintf(s_next_required_gold,"%d",gameFightSkill->getCFightSkill()->get_next_required_gold());
				l_goldValue = CCLabelTTF::create(s_next_required_gold,APP_FONT_NAME,16,CCSizeMake(0, 0 ), kCCTextAlignmentLeft);
				l_goldValue->setColor(ccc3(212,255,151));

				eleventhLineHeight = tenthLineHeight + l_gold->getContentSize().height;
				scroll_height = eleventhLineHeight;

				if(GameView::getInstance()->getPlayerGold()<gameFightSkill->getCFightSkill()->get_next_required_gold())//金币不足
				{
					l_gold->setColor(ccc3(255,51,51));
					l_goldValue->setColor(ccc3(255,51,51));
				}
			}
			else
			{
				eleventhLineHeight = tenthLineHeight;
				scroll_height = eleventhLineHeight;
			}
	}

	CCScrollView * m_scrollView = CCScrollView::create(CCSizeMake(243,162));
	m_scrollView->setViewSize(CCSizeMake(243, 162));
	m_scrollView->ignoreAnchorPointForPosition(false);
	m_scrollView->setTouchEnabled(true);
	m_scrollView->setDirection(kCCScrollViewDirectionVertical);
	m_scrollView->setAnchorPoint(ccp(0,0));
	m_scrollView->setPosition(ccp(504,237));
	m_scrollView->setBounceable(true);
	m_scrollView->setClippingToBounds(true);
	m_scrollView->setTag(M_SCROLLVIEW_TAG);
	layer_musouSkill->addChild(m_scrollView);
	if (scroll_height > 162)
	{
		m_scrollView->setContentSize(CCSizeMake(243,scroll_height));
		m_scrollView->setContentOffset(ccp(0,162-scroll_height));  
	}
	else
	{
		m_scrollView->setContentSize(ccp(243,162));
	}
	m_scrollView->setClippingToBounds(true);

	s_nameFrame->ignoreAnchorPointForPosition(false);
	s_nameFrame->setAnchorPoint(ccp(0,0));
	m_scrollView->addChild(s_nameFrame);
	s_nameFrame->setPosition(ccp(8,m_scrollView->getContentSize().height - zeroLineHeight));

	m_scrollView->addChild(l_name);
	l_name->setPosition(ccp(15,m_scrollView->getContentSize().height - zeroLineHeight));

	m_scrollView->addChild(s_first_line);
	s_first_line->setPosition(ccp(5,m_scrollView->getContentSize().height - zeroLineHeight_1+5));

	l_type->ignoreAnchorPointForPosition( false );  
	l_type->setAnchorPoint( ccp( 0, 0 ) );  
	m_scrollView->addChild(l_type);
	l_type->setPosition(ccp(8,m_scrollView->getContentSize().height - firstLineHeight));

	l_mp->ignoreAnchorPointForPosition( false );  
	l_mp->setAnchorPoint( ccp( 0, 0 ) );  
	m_scrollView->addChild(l_mp);
	l_mp->setPosition(ccp(105,m_scrollView->getContentSize().height - firstLineHeight));

	l_mpValue->ignoreAnchorPointForPosition( false );  
	l_mpValue->setAnchorPoint( ccp( 0.5f, 0.5f ) );  
	m_scrollView->addChild(l_mpValue);
	l_mpValue->setPosition(ccp(105+l_mp->getContentSize().width+2,m_scrollView->getContentSize().height - firstLineHeight));

	l_lv->ignoreAnchorPointForPosition( false );  
	l_lv->setAnchorPoint( ccp( 0, 0 ) );  
	m_scrollView->addChild(l_lv);
	l_lv->setPosition(ccp(8,m_scrollView->getContentSize().height - secondLineHeight));

	l_lvValue->ignoreAnchorPointForPosition( false );  
	l_lvValue->setAnchorPoint( ccp( 0, 0 ) );  
	m_scrollView->addChild(l_lvValue);
	l_lvValue->setPosition(ccp(8+l_lv->getContentSize().width+2,m_scrollView->getContentSize().height - secondLineHeight));

	l_cd->ignoreAnchorPointForPosition( false );  
	l_cd->setAnchorPoint( ccp( 0, 0 ) );  
	m_scrollView->addChild(l_cd);
	l_cd->setPosition(ccp(105,m_scrollView->getContentSize().height - secondLineHeight));

	l_cdValue->ignoreAnchorPointForPosition( false );  
	l_cdValue->setAnchorPoint( ccp( 0, 0 ) );  
	m_scrollView->addChild(l_cdValue);
	l_cdValue->setPosition(ccp(105+l_cd ->getContentSize().width+2,m_scrollView->getContentSize().height - secondLineHeight));

	l_pro->ignoreAnchorPointForPosition( false );  
	l_pro->setAnchorPoint( ccp( 0, 0 ) );  
	m_scrollView->addChild(l_pro);
	l_pro->setPosition(ccp(8,m_scrollView->getContentSize().height - thirdLineHeight));

	l_proValue->ignoreAnchorPointForPosition( false );  
	l_proValue->setAnchorPoint( ccp( 0, 0 ) );  
	m_scrollView->addChild(l_proValue);
	l_proValue->setPosition(ccp(8+l_pro->getContentSize().width+2,m_scrollView->getContentSize().height - thirdLineHeight));

	l_dis->ignoreAnchorPointForPosition( false );  
	l_dis->setAnchorPoint( ccp( 0, 0 ) );  
	m_scrollView->addChild(l_dis);
	l_dis->setPosition(ccp(105,m_scrollView->getContentSize().height - thirdLineHeight));

	l_disValue->ignoreAnchorPointForPosition( false );  
	l_disValue->setAnchorPoint( ccp( 0, 0 ) );  
	m_scrollView->addChild(l_disValue);
	l_disValue->setPosition(ccp(105+l_dis->getContentSize().width+2,m_scrollView->getContentSize().height - thirdLineHeight));

	t_des->ignoreAnchorPointForPosition( false );  
	t_des->setAnchorPoint( ccp( 0, 0 ) );  
	m_scrollView->addChild(t_des);
	t_des->setPosition(ccp(8,m_scrollView->getContentSize().height - fourthLineHeight));

	if (gameFightSkill->getLevel()<gameFightSkill->getCBaseSkill()->maxlevel())
	{
			l_nextLv->ignoreAnchorPointForPosition( false );  
			l_nextLv->setAnchorPoint( ccp( 0, 0 ) );  
			m_scrollView->addChild(l_nextLv);
			l_nextLv->setPosition(ccp(8,m_scrollView->getContentSize().height - fifthLineHeight));

			t_nextLvDes->ignoreAnchorPointForPosition( false );  
			t_nextLvDes->setAnchorPoint( ccp( 0, 0 ) );  
			m_scrollView->addChild(t_nextLvDes);
			t_nextLvDes->setPosition(ccp(8,m_scrollView->getContentSize().height - sixthLineHeight));

			s_upgradeNeed->ignoreAnchorPointForPosition( false );  
			s_upgradeNeed->setAnchorPoint( ccp( 0, 0 ) );  
			m_scrollView->addChild(s_upgradeNeed);
			s_upgradeNeed->setPosition(ccp(8,m_scrollView->getContentSize().height - seventhLineHeight));

			m_scrollView->addChild(l_upgradeNeed);
			l_upgradeNeed->setPosition(ccp(15,m_scrollView->getContentSize().height - seventhLineHeight));

			m_scrollView->addChild(s_second_line);
			s_second_line->setPosition(ccp(5,m_scrollView->getContentSize().height - seventhLineHeight_1+5));

			//主角等级
			if (gameFightSkill->getCFightSkill()->get_next_required_level()>0)
			{
				l_playerLv->ignoreAnchorPointForPosition( false );  
				l_playerLv->setAnchorPoint( ccp( 0, 0 ) );  
				m_scrollView->addChild(l_playerLv);
				l_playerLv->setPosition(ccp(8,m_scrollView->getContentSize().height - eighthLineHeight));

				l_playerLvValue->ignoreAnchorPointForPosition( false );  
				l_playerLvValue->setAnchorPoint( ccp( 0, 0 ) );  
				m_scrollView->addChild(l_playerLvValue);
				l_playerLvValue->setPosition(ccp(8+l_playerLv->getContentSize().width+5,m_scrollView->getContentSize().height - eighthLineHeight));
			}
			//技能点
			if (gameFightSkill->getCFightSkill()->get_next_required_point()>0)
			{
				l_skillPoint->ignoreAnchorPointForPosition( false );  
				l_skillPoint->setAnchorPoint( ccp( 0, 0 ) );  
				m_scrollView->addChild(l_skillPoint);
				l_skillPoint->setPosition(ccp(8,m_scrollView->getContentSize().height - ninthLineHeight));

				l_skillPointValue->ignoreAnchorPointForPosition( false );  
				l_skillPointValue->setAnchorPoint( ccp( 0, 0 ) );  
				m_scrollView->addChild(l_skillPointValue);
				l_skillPointValue->setPosition(ccp(8+l_skillPoint->getContentSize().width+5,m_scrollView->getContentSize().height - ninthLineHeight));
			}
			//XXX技能
			if (gameFightSkill->getCFightSkill()->get_next_pre_skill() != "")
			{
				l_preSkill->ignoreAnchorPointForPosition( false );  
				l_preSkill->setAnchorPoint( ccp( 0, 0 ) );  
				m_scrollView->addChild(l_preSkill);
				l_preSkill->setPosition(ccp(8,m_scrollView->getContentSize().height - tenthLineHeight));

				l_preSkillValue->ignoreAnchorPointForPosition( false );  
				l_preSkillValue->setAnchorPoint( ccp( 0, 0 ) );  
				m_scrollView->addChild(l_preSkillValue);
				l_preSkillValue->setPosition(ccp(8+l_preSkill->getContentSize().width+5,m_scrollView->getContentSize().height - tenthLineHeight));
			}
			//金币
			if (gameFightSkill->getCFightSkill()->get_next_required_gold()>0)
			{
				l_gold->ignoreAnchorPointForPosition( false );  
				l_gold->setAnchorPoint( ccp( 0, 0 ) );  
				m_scrollView->addChild(l_gold);
				l_gold->setPosition(ccp(8,m_scrollView->getContentSize().height - eleventhLineHeight));

				l_goldValue->ignoreAnchorPointForPosition( false );  
				l_goldValue->setAnchorPoint( ccp( 0, 0 ) );  
				m_scrollView->addChild(l_goldValue);
				l_goldValue->setPosition(ccp(8+l_gold->getContentSize().width+5,m_scrollView->getContentSize().height - eleventhLineHeight));
			}
	}
}

void MusouSkillScene::AddMusouFlagWithAnm( std::string skillId )
{
// 
// 	int idx = -1;
// 	for(int i = 0;i<m_studedSkillList.size();i++)
// 	{
// 		if (strcmp(skillId.c_str(),m_studedSkillList.at(i)->getId().c_str()) == 0)
// 		{
// 			idx = i;
// 			break;
// 		}
// 	}
// 
// 	if (idx == -1)
// 		return;

// 	for(int i = 0;i<m_studedSkillList.size();i++)
// 	{
// 		CCTableViewCell * cell = generalList_tableView->cellAtIndex(i);
// 		if(!cell)
// 			continue;
// 
// 		if (cell->getChildByTag(M_ROLEMUSOUSKILL_FLAT_TAG))
// 		{
// 			cell->getChildByTag(M_ROLEMUSOUSKILL_FLAT_TAG)->removeFromParent();
// 		}
// 	}
// 
// 	CCTableViewCell * cell = generalList_tableView->cellAtIndex(idx);
// 	CCSprite * sprite_musou = CCSprite::create("res_ui/wujiang/longhun.png");
// 	sprite_musou->setPosition(ccp(65,65));
// 	sprite_musou->setTag(M_ROLEMUSOUSKILL_FLAT_TAG);
// 	cell->addChild(sprite_musou);
// 	sprite_musou->setScale(3.0f);
// 	sprite_musou->setVisible(true);
// 	CCFiniteTimeAction*  action = CCSequence::create(
// 		CCEaseElasticIn::create(CCScaleTo::create(0.7f,1.0f)),
// 		NULL);
// 	sprite_musou->runAction(action);
}

void MusouSkillScene::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void MusouSkillScene::addCCTutorialIndicator1( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,60,50);
	tutorialIndicator->setPosition(ccp(pos.x-45,pos.y+95));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	this->addChild(tutorialIndicator);
}

void MusouSkillScene::removeCCTutorialIndicator()
{
	CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();
}
