#ifndef _SKILLUI_MUSOUSKILLTALENT_H_
#define _SKILLUI_MUSOUSKILLTALENT_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CMusouTalent;

struct Coordinate
{
	float x;
	float y;
};


class MusouSkillTalent : public UIScene
{
public:
	MusouSkillTalent();
	~MusouSkillTalent();

	static MusouSkillTalent* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual void setVisible(bool visible);

	void InfoEvent(CCObject *pSender);
	void UpgradeEvent(CCObject *pSender);

	void setSelectImagePos(int index);

	void RefreshTalentItemLevel(CMusouTalent * musouTalent);
	void RefreshTalentInfo(CMusouTalent * musouTalent);
	void RefreshEssenceValue();

	void AddBonusSpecialEffect(CMusouTalent * musouTalent);

public: 
	int curSelectIndex;
	std::vector<CMusouTalent *> curMusouTalentList;
private:
	UIPanel * panel_musouTalent;
	UILayer * layer_musouTalent;

	UILabel * l_essenceValue;
	UIImageView * ImageView_select;
	
	//坐标
	std::vector<Coordinate> CoordinateVector;


	//下一级描述
	CCLabelTTF * l_nextLv;
	//下一级
	CCLabelTTF * t_nextLvDes;
	//升级需要
	CCSprite * s_upgradeNeed;
	CCLabelTTF * l_upgradeNeed;
	CCSprite * s_second_line;
	//人物等级
	CCLabelTTF * l_playerLv;
	CCLabelTTF * l_playerLvValue;
	//技能点
	CCLabelTTF * l_skillPoint;
	CCLabelTTF * l_skillPointValue;
};


//////////////////////////////////////////////////////////
class MusouTalentItem : public UIScene
{
public:
	MusouTalentItem();
	~MusouTalentItem();
	static MusouTalentItem* create(CMusouTalent * musouTalent);
	bool init(CMusouTalent * musouTalent);

	void DoThing(CCObject *pSender);
	//void RefreshGeneralSkillInfo(std::string skillid);

	void setFrameVisible(bool isVisible);

	void RefreshLevel(int _value);

	void RefreshCurMusouTalent(CMusouTalent * temp);

private:
	//std::string m_skillId;
	CMusouTalent * m_curMusouTalent;

	UIButton *btn_bgFrame;
	UILabelBMFont * l_lv;
};

#endif

