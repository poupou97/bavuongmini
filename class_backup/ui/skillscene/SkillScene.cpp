#include "SkillScene.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "AppMacros.h"
#include "../extensions/UITab.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/skill/GameFightSkill.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutConfigure.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../generals_ui/generals_popup_ui/GeneralsSkillInfoUI.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/CBaseSkill.h"
#include "../../messageclient/element/CShortCut.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "../../utils/GameUtils.h"
#include "AppMacros.h"
#include "MusouSkillScene.h"
#include "GeneralMusouSkillListUI.h"
#include "MusouSkillTalent.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutSlot.h"
#include "../equipMent_ui/SystheSisUI.h"
#include "../../gamescene_state/GameSceneEffects.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutUIConstant.h"
#include "../../legend_script/CCTeachingGuide.h"
#include "../../utils/StrUtils.h"

using namespace CocosDenshion;

#define  M_SCROLLVIEWTAG 44
#define  BUTTON_NORMAL_PATH "res_ui/new_button_1.png"
#define  BUTTON_DISABLED_PATH "res_ui/new_button_001.png"

#define kTag_BonusSpecialEffect 55

SkillScene::SkillScene():
curFightSkill(NULL),
updown(1),
curSkillLevel(0)
{
}


SkillScene::~SkillScene()
{
	CC_SAFE_DELETE(curFightSkill);
}

SkillScene* SkillScene::create()
{
	SkillScene * skillScene = new SkillScene();
	if (skillScene && skillScene->init())
	{
		skillScene->autorelease();
		return skillScene;
	}
	CC_SAFE_DELETE(skillScene);
	return NULL;
}

bool SkillScene::init()
{
	if(UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		this->scheduleUpdate();

		curFightSkill = new GameFightSkill();

		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winsize);
		mengban->setAnchorPoint(CCPointZero);
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);

		//加载UI
		if(LoadSceneLayer::SkillSceneLayer->getWidgetParent() != NULL)
		{
			LoadSceneLayer::SkillSceneLayer->removeFromParentAndCleanup(false);
		}
		ppanel = LoadSceneLayer::SkillSceneLayer;
		ppanel->getValidNode()->setContentSize(CCSizeMake(800, 480));
		ppanel->setAnchorPoint(ccp(0.5f,0.5f));
		ppanel->setPosition(CCPointZero);
		ppanel->setTouchEnable(true);
		ppanel->setPosition(ccp(winsize.width/2,winsize.height/2));
		m_pUiLayer->addWidget(ppanel);

		u_layer = UILayer::create();
		u_layer->ignoreAnchorPointForPosition(false);
		u_layer->setAnchorPoint(ccp(0.5f,0.5f));
		u_layer->setContentSize(CCSizeMake(800, 480));
		u_layer->setPosition(CCPointZero);
		u_layer->setPosition(ccp(winsize.width/2,winsize.height/2));
		addChild(u_layer);

		const char * secondStr = StringDataManager::getString("UIName_ji");
		const char * thirdStr = StringDataManager::getString("UIName_neng");
		CCArmature * atmature = MainScene::createPendantAnm("",secondStr,thirdStr,"");
		atmature->setPosition(ccp(30,240));
		u_layer->addChild(atmature);

		roleSkillPanel = (UIPanel*)UIHelper::seekWidgetByName(ppanel,"Panel_role_jineng");
		roleSkillPanel->setVisible(true);
		//
		labelBmfont = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_shengyu");
// 		//龙魂技能
// 		long long time = GameUtils::millisecondNow();
// 		musouSkillLayer = MusouSkillScene::create();
// 		u_layer->addChild(musouSkillLayer);
// 		musouSkillLayer->setVisible(false);
// 		CCLOG(" MusouSkillScene::create() TIME = %ld",GameUtils::millisecondNow()-time);
// 
// 		//龙魂天赋
// 		time = GameUtils::millisecondNow();
// 		musouSkillTalent = MusouSkillTalent::create();
// 		u_layer->addChild(musouSkillTalent);
// 		musouSkillTalent->setVisible(false);
// 		CCLOG(" MusouSkillTalent::create() TIME = %ld",GameUtils::millisecondNow()-time);
		u_scrollView = (UIScrollView*)UIHelper::seekWidgetByName(ppanel,"ScrollView_skill");
		u_scrollView->setBounceEnabled(true);
		//关闭按钮
		Button_close = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnable(true);
		Button_close->addReleaseEvent(this, coco_releaseselector(SkillScene::CloseEvent));
		Button_close->setPressedActionEnabled(true);
		//重置按钮
		UIButton * Button_reset = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_chongzhi");
		Button_reset->setTouchEnable(true);
		Button_reset->addReleaseEvent(this, coco_releaseselector(SkillScene::ResetEvent));
		Button_reset->setPressedActionEnabled(true);
		//升级按钮
		Button_levUp = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_shengji");
		Button_levUp->setTouchEnable(true);
		Button_levUp->addReleaseEvent(this, coco_releaseselector(SkillScene::LevelUpEvent));
		Button_levUp->setPressedActionEnabled(true);
		//降级按钮
		Button_levDown = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_jiangji");
		Button_levDown->setTouchEnable(true);
		Button_levDown->addReleaseEvent(this, coco_releaseselector(SkillScene::LevelDownEvent));
		Button_levDown->setPressedActionEnabled(true);
		//快捷按钮
		Button_convenient = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_kuaijie");
		Button_convenient->setTouchEnable(true);
		Button_convenient->addReleaseEvent(this, coco_releaseselector(SkillScene::ConvenientEvent));
		Button_convenient->setPressedActionEnabled(true);
		//学习按钮
		Button_study = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_xuexi");
		Button_study->setTouchEnable(true);
		Button_study->addReleaseEvent(this, coco_releaseselector(SkillScene::StudyEvent));
		Button_study->setPressedActionEnabled(true);
		//已用技能点
		label_usedSkillPoint = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_yiyongjinengdian");
		label_usedSkillPoint->setText("45");
		//剩余技能点
		label_notUsedSkillPoint = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_shengyujinengdian");
		label_notUsedSkillPoint->setText("55");
		//
		image_remainFightpPointFrame = (UIImageView *)UIHelper::seekWidgetByName(ppanel,"ImageView_5184_0");

		//升级需要
// 		textArea_lvUp = (UITextArea *)UIHelper::seekWidgetByName(ppanel,"TextArea_jinengxiangqiang_c");
// 		textArea_lvUp->setText("hello everyone !");

		ImageView_select = (UIImageView *)UIHelper::seekWidgetByName(ppanel,"ImageView_select");
		ImageView_select->setVisible(false);

// 		ImageView_musou = (UIImageView *)UIHelper::seekWidgetByName(ppanel,"ImageView_musou");
// 		ImageView_musou->setTexture("res_ui/longhun.png");
// 		ImageView_musou->setScale(1.05f);
// 		ImageView_musou->setVisible(false);

		Label_skillName =  (UILabelBMFont *)UIHelper::seekWidgetByName(ppanel,"LabelBMFont_jinengmingcheng");
		//textArea_lv = (UITextArea *)UIHelper::seekWidgetByName(ppanel,"TextArea_jinengxiangqiang_a");
		//textArea_des = (UITextArea *)UIHelper::seekWidgetByName(ppanel,"TextArea_jinengxiangqiang_b");

		//获取各个职业的技能树
		panel_mengjiang = (UIPanel *)UIHelper::seekWidgetByName(ppanel,"Panel_mengjiang");
		panel_guimou = (UIPanel *)UIHelper::seekWidgetByName(ppanel,"Panel_guimou");
		panel_shenshe = (UIPanel *)UIHelper::seekWidgetByName(ppanel,"Panel_shenshe");
		panel_haojie = (UIPanel *)UIHelper::seekWidgetByName(ppanel,"Panel_haojie");
		panel_mengjiang->setVisible(false);
		panel_guimou->setVisible(false);
		panel_shenshe->setVisible(false);
		panel_haojie->setVisible(false);
		defaultAtkSkill = "";
		if(GameView::getInstance()->myplayer->getActiveRole()->profession() == PROFESSION_MJ_NAME)
		{
			defaultAtkSkill = "WarriorDefaultAttack";
			curSkillId = "AZ1ltnh";
			panel_mengjiang->setVisible(true);
		}
		else if (GameView::getInstance()->myplayer->getActiveRole()->profession() == PROFESSION_GM_NAME)
		{
			defaultAtkSkill = "MagicDefaultAttack";
			curSkillId = "BZ1ssb";
			panel_guimou->setVisible(true);
		}
		else if (GameView::getInstance()->myplayer->getActiveRole()->profession() == PROFESSION_HJ_NAME)
		{
			defaultAtkSkill = "WarlockDefaultAttack";
			curSkillId = "CX1ycww";
			panel_haojie->setVisible(true);
		}
		else if (GameView::getInstance()->myplayer->getActiveRole()->profession() == PROFESSION_SS_NAME)
		{
			defaultAtkSkill = "RangerDefaultAttack";
			curSkillId = "DX1lys";
			panel_shenshe->setVisible(true);
		}
		
		//curSkillId = defaultAtkSkill;

		AllSkillLayer = UILayer::create();
		u_layer->addChild(AllSkillLayer);
		AllSkillLayer->setContentSize(designResolutionSize);
		AllSkillLayer->setAnchorPoint(CCPointZero);
		AllSkillLayer->setVisible(true);

		//maintab
		const char * normalImage = "res_ui/tab_4_off.png";
		const char * selectImage = "res_ui/tab_4_on.png";
		const char * finalImage = "";
		const char * highLightImage = "res_ui/tab_4_on.png";
		const char *str1 = StringDataManager::getString("skillui_tab_roleSkill");
		char* p1 =const_cast<char*>(str1);
//  		const char *str2  = StringDataManager::getString("skillui_tab_musouSkill");
//  		char* p2 =const_cast<char*>(str2);
		const char *str3  = StringDataManager::getString("skillui_tab_musouTalent");
		char* p3 =const_cast<char*>(str3);
		char * mainNames[] ={p1,p3};
		SkillTypeTab = UITab::createWithText(2,normalImage,selectImage,finalImage,mainNames,HORIZONTAL,5);
		SkillTypeTab->setAnchorPoint(ccp(0,0));
		SkillTypeTab->setPosition(ccp(66,410));
		SkillTypeTab->setHighLightImage((char * )highLightImage);
		SkillTypeTab->setDefaultPanelByIndex(0);
		SkillTypeTab->addIndexChangedEvent(this,coco_indexchangedselector(SkillScene::SkillTypeTabIndexChangedEvent));
		SkillTypeTab->setPressedActionEnabled(true);
		u_layer->addWidget(SkillTypeTab);
		this->RefreshData();
		this->RefreshToDefault();

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(winsize);

		return true;
	}
	return false;
}


void SkillScene::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
	GameUtils::playGameSound(SCENE_OPEN, 2, false);

	CCFiniteTimeAction*  action = CCSequence::create(
		CCDelayTime::create(0.3f),
		CCCallFunc::create(this,callfunc_selector(SkillScene::createTalentAndMusou)),
		NULL);
	this->runAction(action);
}

void SkillScene::onExit()
{
	UIScene::onExit();
	SimpleAudioEngine::sharedEngine()->playEffect(SCENE_CLOSE, false);
}


bool SkillScene::ccTouchBegan(CCTouch *touch, CCEvent * pEvent)
{
	return this->resignFirstResponder(touch,this,false);
}

void SkillScene::ccTouchEnded(CCTouch *touch, CCEvent * pEvent)
{
}

void SkillScene::ccTouchCancelled(CCTouch *touch, CCEvent * pEvent)
{
}

void SkillScene::ccTouchMoved(CCTouch *touch, CCEvent * pEvent)
{
}

void SkillScene::CloseEvent( CCObject *pSender )
{
	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(pSender);

	roleSkillPanel->setVisible(true);
	AllSkillLayer->setVisible(true);
	musouSkillLayer->setVisible(false);
	musouSkillTalent->setVisible(false);

	RemoveBonumSpecialEffect();

	this->closeAnim();
}

void SkillScene::SkillTypeTabIndexChangedEvent( CCObject *pSender )
{
	switch(((UITab*)pSender)->getCurrentIndex())
	{
	case 0:
	//	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	//	if(sc != NULL)
	//		sc->endCommand(this);
		{
			roleSkillPanel->setVisible(true);
			AllSkillLayer->setVisible(true);
			musouSkillLayer->setVisible(false);
			musouSkillTalent->setVisible(false);
			//this->RefreshData();
			//this->RefreshToDefault();
		}
		break;
// 	case 1:
// 		{
// 			//开启等级
// 			int openlevel = 0;
// 			std::map<int,int>::const_iterator cIter;
// 			cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(2);
// 			if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // 没找到就是指向END了  
// 			{
// 			}
// 			else
// 			{
// 				openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[2];
// 			}
// 
// 			if (openlevel <= GameView::getInstance()->myplayer->getActiveRole()->level())
// 			{
// 				Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
// 				if(sc != NULL)
// 					sc->endCommand(SkillTypeTab);
// 
// 				roleSkillPanel->setVisible(false);
// 				AllSkillLayer->setVisible(false);
// 				musouSkillLayer->setVisible(true);
// 				musouSkillTalent->setVisible(false);
// 				musouSkillLayer->generalMusouSkillListUI->RefreshStudedSkillList();
// 				musouSkillLayer->generalMusouSkillListUI->generalList_tableView->reloadData();
// 				musouSkillLayer->RefreshSkillInfo(0,NULL);
// 			}
// 			else
// 			{
// 				std::string str_des = StringDataManager::getString("feature_will_be_open_function");
// 				char str_level[20];
// 				sprintf(str_level,"%d",openlevel);
// 				str_des.append(str_level);
// 				str_des.append(StringDataManager::getString("feature_will_be_open_open"));
// 				GameView::getInstance()->showAlertDialog(str_des.c_str());
// 			}
// 		}
// 		break;
	case 1:
		{
			//开启等级
			int openlevel = 0;
			std::map<int,int>::const_iterator cIter;
			cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(7);
			if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // 没找到就是指向END了  
			{
			}
			else
			{
				openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[7];
			}

			if (openlevel <= GameView::getInstance()->myplayer->getActiveRole()->level())
			{
				roleSkillPanel->setVisible(false);
				AllSkillLayer->setVisible(false);
				musouSkillLayer->setVisible(false);
				musouSkillTalent->setVisible(true);
			}
			else
			{
				std::string str_des = StringDataManager::getString("feature_will_be_open_function");
				char str_level[20];
				sprintf(str_level,"%d",openlevel);
				str_des.append(str_level);
				str_des.append(StringDataManager::getString("feature_will_be_open_open"));
				GameView::getInstance()->showAlertDialog(str_des.c_str());
			}
		}
		break;
	}

}

// void SkillScene::scrollViewDidScroll(CCScrollView* view)
// {
// }
// 
// void SkillScene::scrollViewDidZoom(CCScrollView* view)
// {
// }

void SkillScene::ResetEvent( CCObject *pSender )
{
 	const char *str1 = StringDataManager::getString("skillui_reset_1");
 	char* p1 =const_cast<char*>(str1);
 	GameView::getInstance()->showPopupWindow(p1,2,this,callfuncO_selector(SkillScene::SureResetEvent),NULL);
}

void SkillScene::ConvenientEvent( CCObject *pSender )
{
	if(ImageView_select->isVisible())
	{
		if (curSkillLevel > 0)
		{
			Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
			if(sc != NULL)
				sc->endCommand(pSender);
			//打开快捷面板
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutConfigure) == NULL)
			{
				CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
				ShortcutConfigure * shortcutConfigure = ShortcutConfigure::create();
				shortcutConfigure->setTag(kTagShortcutConfigure);
				GameView::getInstance()->getMainUIScene()->addChild(shortcutConfigure);
				shortcutConfigure->ignoreAnchorPointForPosition(false);
				shortcutConfigure->setAnchorPoint(ccp(0.5f,0.5f));
				shortcutConfigure->setPosition(ccp(winsize.width/2,winsize.height/2));
			}
		}
		else
		{
			//CCDictionary *strings = CCDictionary::createWithContentsOfFile("res_ui/font/strings.xml");
			//const char *str1  = ((CCString*)strings->objectForKey("skillui_didnotstudy"))->m_sString.c_str();
			const char *str1 = StringDataManager::getString("skillui_didnotstudy");
			char* p1 =const_cast<char*>(str1);
			GameView::getInstance()->showAlertDialog(p1);
		}
	}
	else
	{
		//CCDictionary *strings = CCDictionary::createWithContentsOfFile("res_ui/font/strings.xml");
		//const char *str1  = ((CCString*)strings->objectForKey("skillui_peleaseselcetskill"))->m_sString.c_str();
		const char *str1 = StringDataManager::getString("skillui_peleaseselcetskill");
		char* p1 =const_cast<char*>(str1);
		GameView::getInstance()->showAlertDialog(p1);
	}
	
	
}

void SkillScene::SureResetEvent( CCObject *pSender )
{
	//确定重置技能点
	updown = 3;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1126, (void *)updown,(void *)curSkillId.c_str());
}

void SkillScene::SkillInfoEvent( CCObject *pSender )
{
	CCLOG("SkillInfoEvent");
	//设置选中状态的图片位置
	UIButton * selectSkill = (UIButton*)pSender;
	std::string buttonName = selectSkill->getName();
	std::string skillid = buttonName.substr(9);
	curSkillId = skillid;
	std::string temp = buttonName.substr(9,1);

	if (strcmp(skillid.c_str(),m_firstSkillName.c_str()) == 0)
	{
		Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if(sc != NULL)
			sc->endCommand(pSender);
	}

	ImageView_select->setVisible(true);
	if (strcmp(temp.c_str(),"P") == 0) //被动技能
	{
		ImageView_select->setTexture("res_ui/jineng/light_name2.png");
		//ImageView_select->setPosition(ccp(selectSkill->getPosition().x-46,selectSkill->getPosition().y+159));
		ImageView_select->setPosition(ccp(selectSkill->getPosition().x-48,selectSkill->getPosition().y+(u_scrollView->getInnerContainerSize().height-82-u_scrollView->getSize().height)-15));
	}
	else
	{
		ImageView_select->setTexture("res_ui/jineng/light_name.png");
		//ImageView_select->setPosition(ccp(selectSkill->getPosition().x-46,selectSkill->getPosition().y+165));
		ImageView_select->setPosition(ccp(selectSkill->getPosition().x-40,selectSkill->getPosition().y+(u_scrollView->getInnerContainerSize().height-77-u_scrollView->getSize().height)-14));
	}

	GameFightSkill * gameFightSkill = new GameFightSkill();
	bool isHave = false;
	int curLevel;
	std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GameFightSkillList.begin();
	for ( ; it != GameView::getInstance()->GameFightSkillList.end(); ++ it )
	{
		GameFightSkill * tempSkill = it->second;
		if (strcmp(tempSkill->getId().c_str(),skillid.c_str()) == 0)
		{
			curLevel = tempSkill->getLevel();
			curFightSkill->initSkill(skillid, skillid, tempSkill->getLevel(),GameActor::type_player);
			gameFightSkill->initSkill(skillid, skillid, tempSkill->getLevel(),GameActor::type_player);
			isHave = true;
			break;
		}
	}
	
	if (isHave == false)    //没学会
	{
		curLevel = 0;
		curFightSkill->initSkill(skillid,skillid,1,GameActor::type_player);
		gameFightSkill->initSkill(skillid,skillid,1,GameActor::type_player);
	}

	if (isHave == false)    //没学会
	{
		Button_study->setVisible(true);
		Button_study->setTouchEnable(true);
		Button_levUp->setVisible(false);
		Button_levUp->setTouchEnable(false);
		//Button_levDown->setBright(false);
		Button_levDown->setNormalTexture(BUTTON_DISABLED_PATH);
		Button_levDown->setTouchEnable(false);
		//Button_convenient->setBright(false);
		Button_convenient->setNormalTexture(BUTTON_DISABLED_PATH);
		Button_convenient->setTouchEnable(false);
	}
	else                //已学会
	{
		Button_study->setVisible(false);
		Button_study->setTouchEnable(false);
		Button_levUp->setVisible(true);
		Button_levUp->setTouchEnable(true);
		Button_levUp->setNormalTexture(BUTTON_NORMAL_PATH);
		//Button_levDown->setBright(true);
		Button_levDown->setNormalTexture(BUTTON_NORMAL_PATH);
		Button_levDown->setTouchEnable(true);
		if (strcmp(temp.c_str(),"P") == 0) //被动技能
		{
			//Button_convenient->setBright(false);
			Button_convenient->setNormalTexture(BUTTON_DISABLED_PATH);
			Button_convenient->setTouchEnable(false);
		}
		else
		{
			//Button_convenient->setBright(true);
			Button_convenient->setNormalTexture(BUTTON_NORMAL_PATH);
			Button_convenient->setTouchEnable(true);
		}
	}

	if (strcmp(defaultAtkSkill.c_str(),skillid.c_str()) == 0)
	{
		Button_study->setVisible(false);
		Button_study->setTouchEnable(false);
		Button_levUp->setVisible(true);
		Button_levUp->setNormalTexture(BUTTON_DISABLED_PATH);
		Button_levUp->setTouchEnable(false);
		//Button_levDown->setBright(false);
		Button_levDown->setNormalTexture(BUTTON_DISABLED_PATH);
		Button_levDown->setTouchEnable(false);
		//Button_convenient->setBright(false);
		Button_convenient->setNormalTexture(BUTTON_NORMAL_PATH);
		Button_convenient->setTouchEnable(true);
	}


	RefreshOneItem(curLevel,gameFightSkill);
	RefreshOneItemInfo(curLevel,gameFightSkill);

	delete gameFightSkill;
}

void SkillScene::StudyEvent( CCObject *pSender )
{
	if (GameView::getInstance()->myplayer->getActiveRole()->level()<curFightSkill->getCFightSkill()->get_required_level())  //人物等级未达到技能升级要求
	{
		const char *str_roleLvNotEhouth = StringDataManager::getString("skillui_roleLvNotEhouth");
		GameView::getInstance()->showAlertDialog(str_roleLvNotEhouth);
	}
	else if(GameView::getInstance()->myplayer->player->skillpoint()<curFightSkill->getCFightSkill()->get_required_point())//技能点不足
	{
		const char *str_skillPointNotEnough = StringDataManager::getString("skillui_skillPointNotEnough");
		GameView::getInstance()->showAlertDialog(str_skillPointNotEnough);
	}
	else
	{
		updown = 1;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1126, (void *)updown,(void *)curSkillId.c_str());

		GameUtils::playGameSound(SKILL_LEARN, 2, false);

		Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if(sc != NULL)
			sc->endCommand(pSender);
	}
}

void SkillScene::SureToStudyEvent( CCObject *pSender )
{

}

void SkillScene::LevelUpEvent( CCObject *pSender )
{
	if (curFightSkill->getLevel()<curFightSkill->getCBaseSkill()->maxlevel())
	{
		//下一级
		if (curFightSkill->getLevel() == 0)
		{
			//前置技能等级
			int preSkillLevel = 0;
			std::map<std::string,GameFightSkill*>::const_iterator cIter;
			cIter = GameView::getInstance()->GameFightSkillList.find(curFightSkill->getCFightSkill()->get_pre_skill());
			if (cIter == GameView::getInstance()->GameFightSkillList.end()) // 没找到就是指向END了  
			{
			}
			else
			{
				GameFightSkill * gameFightSkillStuded = GameView::getInstance()->GameFightSkillList[curFightSkill->getCFightSkill()->get_pre_skill()];
				if (gameFightSkillStuded)
				{
					preSkillLevel = gameFightSkillStuded->getLevel();
				}
			}

			if (GameView::getInstance()->myplayer->getActiveRole()->level()<curFightSkill->getCFightSkill()->get_required_level())  //人物等级未达到技能升级要求
			{
				const char *str_roleLvNotEhouth = StringDataManager::getString("skillui_roleLvNotEhouth");
				GameView::getInstance()->showAlertDialog(str_roleLvNotEhouth);
			}
			else if(GameView::getInstance()->myplayer->player->skillpoint()<curFightSkill->getCFightSkill()->get_required_point())//技能点不足
			{
				const char *str_skillPointNotEnough = StringDataManager::getString("skillui_skillPointNotEnough");
				GameView::getInstance()->showAlertDialog(str_skillPointNotEnough);
			}
			else if (preSkillLevel < curFightSkill->getCFightSkill()->get_pre_skill_level())//前置技能等级不足
			{
				std::string preSkillname = "";
				CBaseSkill * temp = StaticDataBaseSkill::s_baseSkillData[curFightSkill->getCFightSkill()->get_pre_skill()];
				preSkillname.append(temp->name().c_str());

				char des[200];
				const char *str_skillPointNotEnough = StringDataManager::getString("skillui_skillPreSkillLevelNotEnough");
				sprintf(des,str_skillPointNotEnough,preSkillname.c_str(),curFightSkill->getCFightSkill()->get_pre_skill_level());
				GameView::getInstance()->showAlertDialog(des);
			}
			else if (GameView::getInstance()->getPlayerGold()<curFightSkill->getCFightSkill()->get_required_gold())//金币不足
			{
				const char *str_goldNotEnough = StringDataManager::getString("generals_teach_noEnoughGold");
				GameView::getInstance()->showAlertDialog(str_goldNotEnough);
			}
			else
			{
				std::string preSkillname = "";
				CBaseSkill * temp = StaticDataBaseSkill::s_baseSkillData[curFightSkill->getId()];
				//preSkillname.append(temp->name().c_str());
				preSkillname = StrUtils::applyColor(temp->name().c_str(),ccc3(243,252,2));

				char des[300];
				const char *str_skillPointNotEnough = StringDataManager::getString("skillui_areYouSureToLevelUp");
				sprintf(des,str_skillPointNotEnough,preSkillname.c_str(),
					curFightSkill->getCFightSkill()->level()+1,
					curFightSkill->getCFightSkill()->get_required_point(),
					curFightSkill->getCFightSkill()->get_required_gold(),
					GameView::getInstance()->getPlayerGold());

				GameView::getInstance()->showPopupWindow (des,2,this,coco_selectselector(SkillScene::SureToLevUpSkill),NULL);

				Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
				if(sc != NULL)
					sc->endCommand(Button_study);
			}
		}
		else
		{
			//前置技能等级
			int preSkillLevel = 0;
			std::map<std::string,GameFightSkill*>::const_iterator cIter;
			cIter = GameView::getInstance()->GameFightSkillList.find(curFightSkill->getCFightSkill()->get_next_pre_skill());
			if (cIter == GameView::getInstance()->GameFightSkillList.end()) // 没找到就是指向END了  
			{
			}
			else
			{
				GameFightSkill * gameFightSkillStuded = GameView::getInstance()->GameFightSkillList[curFightSkill->getCFightSkill()->get_next_pre_skill()];
				if (gameFightSkillStuded)
				{
					preSkillLevel = gameFightSkillStuded->getLevel();
				}
			}

			if (GameView::getInstance()->myplayer->getActiveRole()->level()<curFightSkill->getCFightSkill()->get_next_required_level())  //人物等级未达到技能升级要求
			{
				const char *str_roleLvNotEhouth = StringDataManager::getString("skillui_roleLvNotEhouth");
				GameView::getInstance()->showAlertDialog(str_roleLvNotEhouth);
			}
			else if(GameView::getInstance()->myplayer->player->skillpoint()<curFightSkill->getCFightSkill()->get_next_required_point())//技能点不足
			{
				const char *str_skillPointNotEnough = StringDataManager::getString("skillui_skillPointNotEnough");
				GameView::getInstance()->showAlertDialog(str_skillPointNotEnough);
			}
			else if (preSkillLevel < curFightSkill->getCFightSkill()->get_next_pre_skill_level())//前置技能等级不足
			{
				std::string preSkillname = "";
				CBaseSkill * temp = StaticDataBaseSkill::s_baseSkillData[curFightSkill->getCFightSkill()->get_next_pre_skill()];
				preSkillname.append(temp->name().c_str());

				char des[200];
				const char *str_skillPointNotEnough = StringDataManager::getString("skillui_skillPreSkillLevelNotEnough");
				sprintf(des,str_skillPointNotEnough,preSkillname.c_str(),curFightSkill->getCFightSkill()->get_next_pre_skill_level());
				GameView::getInstance()->showAlertDialog(des);
			}
			else if (GameView::getInstance()->getPlayerGold()<curFightSkill->getCFightSkill()->get_next_required_gold())//金币不足
			{
				const char *str_goldNotEnough = StringDataManager::getString("generals_teach_noEnoughGold");
				GameView::getInstance()->showAlertDialog(str_goldNotEnough);
			}
			else
			{
				std::string preSkillname = "";
				CBaseSkill * temp = StaticDataBaseSkill::s_baseSkillData[curFightSkill->getId()];
				//preSkillname.append(temp->name().c_str());
				preSkillname = StrUtils::applyColor(temp->name().c_str(),ccc3(243,252,2));

				char des[300];
				const char *str_skillPointNotEnough = StringDataManager::getString("skillui_areYouSureToLevelUp");
				sprintf(des,str_skillPointNotEnough,preSkillname.c_str(),
					curFightSkill->getCFightSkill()->level()+1,
					curFightSkill->getCFightSkill()->get_next_required_point(),
					curFightSkill->getCFightSkill()->get_next_required_gold(),
					GameView::getInstance()->getPlayerGold());

				GameView::getInstance()->showPopupWindow (des,2,this,coco_selectselector(SkillScene::SureToLevUpSkill),NULL);

				Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
				if(sc != NULL)
					sc->endCommand(Button_study);
			}
		}
	}
	else
	{
		const char *str_isMaxLevel = StringDataManager::getString("skillui_maxLevel");
		GameView::getInstance()->showAlertDialog(str_isMaxLevel);
	}
}

void SkillScene::LevelDownEvent( CCObject *pSender )
{
	//updown = 2;
	//GameMessageProcessor::sharedMsgProcessor()->sendReq(1126, (void *)updown,(void *)curSkillId.c_str());
	GameFightSkill *temp  = GameView::getInstance()->myplayer->getGameFightSkill(curSkillId.c_str(),curSkillId.c_str());
	if (!temp)
		return;

	if (temp->getCFightSkill()->get_back_num() <= 0)
	{
		updown = 2;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1126, (void *)updown,(void *)curSkillId.c_str());
		return;
	}

	std::string str_des ;
	str_des.append(StringDataManager::getString("skillui_levelDown_1"));
	char s_num[10];
	sprintf(s_num,"%d",temp->getCFightSkill()->get_back_num());
	str_des.append(s_num);
	str_des.append(StringDataManager::getString("skillui_levelDown_2"));
	PropInfo propInfo = SysthesisUI::getPropInfoFromDBByPropId(temp->getCFightSkill()->get_back_prop().c_str());
	str_des.append(propInfo.name);
	str_des.append(StringDataManager::getString("skillui_levelDown_3"));
	str_des.append("1");
	str_des.append(StringDataManager::getString("skillui_levelDown_4"));
	if (ImageView_select->isVisible())
	{
		GameView::getInstance()->showPopupWindow (str_des.c_str(),2,this,coco_selectselector(SkillScene::SureToLevDownSkill),NULL);
	}
	else
	{
		GameView::getInstance()->showAlertDialog("未选定技能");
	}

}

void SkillScene::SureToLevUpSkill(CCObject *pSender)
{
	updown = 1;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1126, (void *)updown,(void *)curSkillId.c_str());
}
void SkillScene::SureToLevDownSkill(CCObject *pSender)
{
	updown = 2;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1126, (void *)updown,(void *)curSkillId.c_str());
}


//  无双按钮响应事件
void SkillScene::PreelessEvent( CCObject *pSender )
{
	ReqForPreeless * temp = new ReqForPreeless();
	temp->skillid = curSkillId;
	temp->grid = 4;
	temp->roleId = GameView::getInstance()->myplayer->getRoleId();
	temp->roleType = GameActor::type_player;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5075,temp);
	delete temp;

	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(pSender);
}

void SkillScene::RefreshSkillPoint()
{
	int allpoint = 0;
	if (GameView::getInstance()->myplayer->getActiveRole()->level()>=5)
	{
		int changeLevel = GameView::getInstance()->myplayer->getActiveRole()->level() - 4;
		if (changeLevel%2 == 0)
		{
			int temp = changeLevel/2;
			allpoint = temp*3;
		}
		else
		{
			int temp = changeLevel/2;
			allpoint = temp*3+1;
		}
	}
	else{}

	int usedPoint = allpoint - GameView::getInstance()->myplayer->player->skillpoint();
	if (usedPoint<0)
	{
		usedPoint = 0;
	}
	else{}

	int notUsedPoint = GameView::getInstance()->myplayer->player->skillpoint();
	if (notUsedPoint < 0)
	{
		notUsedPoint = 0;
	}
	else
	{

	}

	char s_usedPoint[10];
	sprintf(s_usedPoint,"%d",usedPoint);
	label_usedSkillPoint->setText(s_usedPoint);
	char s_notUsedPoint[10];
	sprintf(s_notUsedPoint,"%d",notUsedPoint);
	label_notUsedSkillPoint->setText(s_notUsedPoint);
}


void SkillScene::RefreshData()
{
	std::string tempStr = "SkillBtn_";
	if (panel_mengjiang->isVisible() == true)
	{
		skillpanel = panel_mengjiang;
		m_firstSkillName.append("AY1hxcq");
		tempStr.append("AY1hxcq");
	}
	else if (panel_guimou->isVisible() == true)
	{
		skillpanel = panel_guimou;
		m_firstSkillName.append("BY1tqms");
		tempStr.append("BY1tqms");
	}
	else if (panel_shenshe->isVisible() == true)
	{
		skillpanel = panel_shenshe;
		m_firstSkillName.append("DY1srpz");
		tempStr.append("DY1srpz");
	}
	else if (panel_haojie->isVisible() == true)
	{
		skillpanel = panel_haojie;
		m_firstSkillName.append("CY1tdwj");
		tempStr.append("CY1tdwj");
	}
	
	ccArray * childrenArray = skillpanel->getChildren()->data;
	int length = childrenArray->num;
	std::string commonString = "SkillBtn_";

	for (int i = 0;i<length;++i)
	{
		UIWidget * child = (UIWidget*)childrenArray->arr[i];
		std::string childName = child->getName();
		std::string skillid = childName.substr(9);

		if (strcmp(childName.c_str(),tempStr.c_str()) == 0)
		{
			m_firstAtkButton = (UIButton *)child;
		}

		if (childName.find(commonString)<childName.length())
		{
			GameFightSkill * gameFightSkill = new GameFightSkill();
			bool isHave = false;
			int curLevel;
			if (GameView::getInstance()->GameFightSkillList.size()>0)
			{
 				std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GameFightSkillList.begin();
				for ( ; it != GameView::getInstance()->GameFightSkillList.end(); ++ it )
				{
					//GameFightSkill * tempSkill = it->second;
					if (strcmp(it->second->getId().c_str(),skillid.c_str()) == 0)
					{
						curLevel = it->second->getLevel();
						curFightSkill->initSkill(skillid,skillid,it->second->getLevel(),GameActor::type_player);
						gameFightSkill->initSkill(skillid,skillid,it->second->getLevel(),GameActor::type_player);
						isHave = true;

// 						if (GameView::getInstance()->myplayer->getMusouSkill())
// 						{
// 							CCLOG("%d",GameView::getInstance()->GameFightSkillList.size());
// 							if (strcmp(GameView::getInstance()->myplayer->getMusouSkill()->getId().c_str(),it->second->getId().c_str()) == 0)
// 							{
// 								ImageView_musou->setVisible(true);
// 								ImageView_musou->setScale(1.05f);
// 								//ImageView_musou->setPosition(ccp(child->getPosition().x-41,child->getPosition().y+170));
// 								ImageView_musou->setPosition(ccp(child->getPosition().x-41,child->getPosition().y+(u_scrollView->getInnerContainerSize().height-70-u_scrollView->getSize().height)));
// 							}
// 						}

						break;
					}
				}

				if (isHave == false)
				{
					curLevel = 0;
					curFightSkill->initSkill(skillid,skillid,1,GameActor::type_player);
					gameFightSkill->initSkill(skillid,skillid,1,GameActor::type_player);
				}
			}
			else
			{
				curLevel = 0;
				gameFightSkill->initSkill(defaultAtkSkill,defaultAtkSkill,0,GameActor::type_player);
			}
			

			RefreshOneItem(curLevel,gameFightSkill);

			delete gameFightSkill;
		}	
	}
}

void SkillScene::RefreshFucBtn(std::string skillid)
{
	if (strcmp(defaultAtkSkill.c_str(),skillid.c_str()) == 0)
	{
		Button_study->setVisible(false);
		Button_study->setTouchEnable(false);
		Button_levUp->setVisible(true);
		Button_levUp->setNormalTexture(BUTTON_DISABLED_PATH);
		Button_levUp->setTouchEnable(false);
		//Button_levDown->setBright(false);
		Button_levDown->setNormalTexture(BUTTON_DISABLED_PATH);
		Button_levDown->setTouchEnable(false);
		//Button_convenient->setBright(false);
		Button_convenient->setNormalTexture(BUTTON_NORMAL_PATH);
		Button_convenient->setTouchEnable(true);
		return;
	}

	std::string temp = skillid.substr(0,1);
	GameFightSkill * gameFightSkill = new GameFightSkill();
	bool isHave = false;
	int curLevel;
	std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GameFightSkillList.begin();
	for ( ; it != GameView::getInstance()->GameFightSkillList.end(); ++ it )
	{
		GameFightSkill * tempSkill = it->second;
		if (strcmp(tempSkill->getId().c_str(),skillid.c_str()) == 0)
		{
			curLevel = tempSkill->getLevel();
			//curFightSkill = tempSkill;
			curFightSkill->initSkill(skillid,skillid,tempSkill->getLevel(),GameActor::type_player);
			gameFightSkill->initSkill(skillid,skillid,tempSkill->getLevel(),GameActor::type_player);
			isHave = true;
			break;
		}
	}

	if (isHave == false)    //没学会
	{
		curLevel = 0;
		gameFightSkill->initSkill(skillid,skillid,1,GameActor::type_player);
	}

	if (isHave == false)    //没学会
	{
		Button_study->setVisible(true);
		Button_study->setTouchEnable(true);
		Button_levUp->setVisible(false);
		Button_levUp->setTouchEnable(false);
		//Button_levDown->setBright(false);
		Button_levDown->setNormalTexture(BUTTON_DISABLED_PATH);
		Button_levDown->setTouchEnable(false);
		//Button_convenient->setBright(false);
		Button_convenient->setNormalTexture(BUTTON_DISABLED_PATH);
		Button_convenient->setTouchEnable(false);
	}
	else                //已学会
	{
		Button_study->setVisible(false);
		Button_study->setTouchEnable(false);
		Button_levUp->setVisible(true);
		Button_levUp->setTouchEnable(true);
		Button_levUp->setNormalTexture(BUTTON_NORMAL_PATH);
		//Button_levDown->setBright(true);
		Button_levDown->setNormalTexture(BUTTON_NORMAL_PATH);
		Button_levDown->setTouchEnable(true);
		if (strcmp(temp.c_str(),"P") == 0) //被动技能
		{
			//Button_convenient->setBright(false);
			Button_convenient->setNormalTexture(BUTTON_DISABLED_PATH);
			Button_convenient->setTouchEnable(false);
		}
		else
		{
			//Button_convenient->setBright(true);
			Button_convenient->setNormalTexture(BUTTON_NORMAL_PATH);
			Button_convenient->setTouchEnable(true);
		}
	}
}

void SkillScene::RefreshOneItem(int curLevel,GameFightSkill * gameFightSkill)
{
	std::string commonString = "SkillBtn_";
	commonString.append(gameFightSkill->getId());
	if (skillpanel->getChildByName(commonString.c_str()) != NULL)
	{
		UIButton *child = (UIButton *)skillpanel->getChildByName(commonString.c_str());
		child->setTouchEnable(true);
		child->addReleaseEvent(this,coco_releaseselector(SkillScene::SkillInfoEvent));
		
		if(((UILabelBMFont*)(child->getChildByName("LabelBMFont_dengji"))))
		{
			std::string lv;
			curSkillLevel = curLevel;
			char curlv[3];
			sprintf(curlv,"%d",curSkillLevel);
			lv = curlv;
			lv.append("/");
			char maxLV[3];
			sprintf(maxLV,"%d",gameFightSkill->getCBaseSkill()->maxlevel());
			lv.append(maxLV);

			((UILabelBMFont*)(child->getChildByName("LabelBMFont_dengji")))->setText(lv.c_str());
		}

		//skill variety icon
		if (gameFightSkill->getCBaseSkill()->usemodel() == 0) //主动
		{
			UIWidget * image_skillVariety = SkillScene::GetSkillVarirtyImageView(gameFightSkill->getId().c_str());
			if (image_skillVariety)
			{
				if (child->getChildByName("image_skillVariety"))
				{
					child->getChildByName("image_skillVariety")->removeFromParent();
				}

				image_skillVariety->setAnchorPoint(ccp(0.5f,0.5f));
				image_skillVariety->setPosition(ccp(41,27));
				image_skillVariety->setScale(0.9f);
				child->addChild(image_skillVariety);
				image_skillVariety->setName("image_skillVariety");
			}
		}

		CCArray* pArray = child->getChildren();
		CCObject* pObj = NULL;
		CCARRAY_FOREACH(pArray, pObj)
		{
			UIWidget* pWidget = (UIWidget*)pObj;
			if(pWidget != NULL)
			{
				std::string name = pWidget->getName();
				int index = name.find("Label_");   // 技能名，不参与变灰
				if(index >= 0)
					continue;

				index = name.find("ImageView_yuanquan");   // 被动技能，左上角的被动标识icon
				if(index >= 0)
					continue;

				if (curLevel == 0 && gameFightSkill->getCFightSkill()->get_required_level()>GameView::getInstance()->myplayer->getActiveRole()->level())
				{
					pWidget->setColor(ccc3(66,66,66));         //gray
				}
				else
				{
					pWidget->setColor(ccc3(255,255,255));   //white
				}
			}
		}
	}
}

void SkillScene::RefreshOneItemInfo(int curLevel,GameFightSkill * gameFightSkill)
{
	//create scrollerView
	UIScrollView * scrollView_skillInfo = (UIScrollView*)AllSkillLayer->getWidgetByTag(M_SCROLLVIEWTAG);
	if (scrollView_skillInfo == NULL)
	{
		scrollView_skillInfo = UIScrollView::create();
		scrollView_skillInfo->setTouchEnable(true);
		scrollView_skillInfo->setDirection(SCROLLVIEW_DIR_VERTICAL);
		scrollView_skillInfo->setSize(CCSizeMake(243,253));
		scrollView_skillInfo->setPosition(ccp(501,142));
		scrollView_skillInfo->setBounceEnabled(true);
		scrollView_skillInfo->setTag(M_SCROLLVIEWTAG);
		AllSkillLayer->addWidget(scrollView_skillInfo);
	}
	//create elements
	int scroll_height = 0;
	//技能名称
	UIImageView * image_nameFrame = (UIImageView *)scrollView_skillInfo->getChildByName("image_nameFrame");
	if (image_nameFrame == NULL)
	{
		image_nameFrame = UIImageView::create();
		image_nameFrame->setTexture("res_ui/moji_0.png");
		image_nameFrame->setScale(0.6f);
		image_nameFrame->setAnchorPoint(ccp(0,0));
		image_nameFrame->setName("image_nameFrame");
		scrollView_skillInfo->addChild(image_nameFrame);
	}
	
	UILabel * l_name = (UILabel *)scrollView_skillInfo->getChildByName("l_name");
	if (l_name == NULL)
	{
		l_name = UILabel::create();
		l_name->setText(gameFightSkill->getCBaseSkill()->name().c_str());
		l_name->setFontName(APP_FONT_NAME);
		l_name->setFontSize(18);
		l_name->setColor(ccc3(196,255,68));
		l_name->setAnchorPoint(ccp(0,0));
		l_name->setName("l_name");
		scrollView_skillInfo->addChild(l_name);
	}
	else
	{
		l_name->setText(gameFightSkill->getCBaseSkill()->name().c_str());
	}
	
	int zeroLineHeight = image_nameFrame->getContentSize().height;
	scroll_height = zeroLineHeight;

	//first line
	UIImageView * image_first_line = (UIImageView *)scrollView_skillInfo->getChildByName("image_first_line");
	if (image_first_line == NULL)
	{
		image_first_line = UIImageView::create();
		image_first_line->setTexture("res_ui/henggang.png");
		image_first_line->setScaleX(0.9f);
		image_first_line->setAnchorPoint(ccp(0,0));
		image_first_line->setName("image_first_line");
		scrollView_skillInfo->addChild(image_first_line);
	}
	int zeroLineHeight_1 = zeroLineHeight + image_first_line->getContentSize().height+6;
	scroll_height = zeroLineHeight_1;
	//主动/被动
	std::string str_type = "";
	if (gameFightSkill->getCBaseSkill()->usemodel() == 0)
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_zhudongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		str_type.append(skillinfo_zhudongjineng);
	}
	else if (gameFightSkill->getCBaseSkill()->usemodel() == 2)
	{
		const char *beidongjineng = StringDataManager::getString("skillinfo_beidongjineng");
		char* skillinfo_beidongjineng =const_cast<char*>(beidongjineng);
		str_type.append(skillinfo_beidongjineng);
	}
	else
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_zhudongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		str_type.append(skillinfo_zhudongjineng);
	}
	UILabel * l_type = (UILabel *)scrollView_skillInfo->getChildByName("l_type");
	if (l_type == NULL)
	{
		l_type = UILabel::create();
		l_type->setText(str_type.c_str());
		l_type->setFontName(APP_FONT_NAME);
		l_type->setFontSize(16);
		l_type->setColor(ccc3(212,255,151));
		l_type->setAnchorPoint(ccp(0,0));
		l_type->setName("l_type");
		scrollView_skillInfo->addChild(l_type);
	}
	else
	{
		l_type->setText(str_type.c_str());
	}
	//MP消耗
	UILabel * l_mp = (UILabel *)scrollView_skillInfo->getChildByName("l_mp");
	if (l_mp == NULL)
	{
		l_mp = UILabel::create();
		l_mp->setText(StringDataManager::getString("skillinfo_mofaxiaohao"));
		l_mp->setFontName(APP_FONT_NAME);
		l_mp->setFontSize(16);
		l_mp->setColor(ccc3(212,255,151));
		l_mp->setAnchorPoint(ccp(0,0));
		l_mp->setName("l_mp");
		scrollView_skillInfo->addChild(l_mp);
	}
	char s_mp[5];
	sprintf(s_mp,"%d",gameFightSkill->getCFightSkill()->mp());
	UILabel * l_mpValue = (UILabel *)scrollView_skillInfo->getChildByName("l_mpValue");
	if (l_mpValue == NULL)
	{
		l_mpValue = UILabel::create();
		l_mpValue->setText(s_mp);
		l_mpValue->setFontName(APP_FONT_NAME);
		l_mpValue->setFontSize(16);
		l_mpValue->setColor(ccc3(212,255,151));
		l_mpValue->setAnchorPoint(ccp(0,0));
		l_mpValue->setName("l_mpValue");
		scrollView_skillInfo->addChild(l_mpValue);
	}
	else
	{
		l_mpValue->setText(s_mp);
	}
	int firstLineHeight = zeroLineHeight_1 + l_type->getContentSize().height;
	scroll_height = firstLineHeight;
	//等级
	UILabel * l_lv = (UILabel *)scrollView_skillInfo->getChildByName("l_lv");
	if (l_lv == NULL)
	{
		l_lv = UILabel::create();
		l_lv->setText(StringDataManager::getString("skillinfo_dengji"));
		l_lv->setFontName(APP_FONT_NAME);
		l_lv->setFontSize(16);
		l_lv->setColor(ccc3(212,255,151));
		l_lv->setAnchorPoint(ccp(0,0));
		l_lv->setName("l_lv");
		scrollView_skillInfo->addChild(l_lv);
	}

	char s_curLevel[5];
	if (curLevel == 0)
	{
		sprintf(s_curLevel,"%d",0);
	}
	else
	{
		sprintf(s_curLevel,"%d",gameFightSkill->getCFightSkill()->level());
	}
	UILabel * l_lvValue = (UILabel *)scrollView_skillInfo->getChildByName("l_lvValue");
	if (l_lvValue == NULL)
	{
		l_lvValue = UILabel::create();
		l_lvValue->setText(s_curLevel);
		l_lvValue->setFontName(APP_FONT_NAME);
		l_lvValue->setFontSize(16);
		l_lvValue->setColor(ccc3(212,255,151));
		l_lvValue->setAnchorPoint(ccp(0,0));
		l_lvValue->setName("l_lvValue");
		scrollView_skillInfo->addChild(l_lvValue);
	}
	else
	{
		l_lvValue->setText(s_curLevel);
	}
	//CD时间 
	UILabel * l_cd = (UILabel *)scrollView_skillInfo->getChildByName("l_cd");
	if (l_cd == NULL)
	{
		l_cd = UILabel::create();
		l_cd->setText(StringDataManager::getString("skillinfo_lengqueshijian"));
		l_cd->setFontName(APP_FONT_NAME);
		l_cd->setFontSize(16);
		l_cd->setColor(ccc3(212,255,151));
		l_cd->setAnchorPoint(ccp(0,0));
		l_cd->setName("l_cd");
		scrollView_skillInfo->addChild(l_cd);
	}
	char s_intervaltime[5];
	sprintf(s_intervaltime,"%d",gameFightSkill->getCFightSkill()->intervaltime()/1000);
	UILabel * l_cdValue = (UILabel *)scrollView_skillInfo->getChildByName("l_cdValue");
	if (l_cdValue == NULL)
	{
		l_cdValue = UILabel::create();
		l_cdValue->setText(s_intervaltime);
		l_cdValue->setFontName(APP_FONT_NAME);
		l_cdValue->setFontSize(16);
		l_cdValue->setColor(ccc3(212,255,151));
		l_cdValue->setAnchorPoint(ccp(0,0));
		l_cdValue->setName("l_cdValue");
		scrollView_skillInfo->addChild(l_cdValue);
	}
	else
	{
		l_cdValue->setText(s_intervaltime);
	}
	int secondLineHeight = firstLineHeight + l_lv->getContentSize().height;
	scroll_height = secondLineHeight;
	//职业
	UILabel * l_pro = (UILabel *)scrollView_skillInfo->getChildByName("l_pro");
	if (l_pro == NULL)
	{
		l_pro = UILabel::create();
		l_pro->setText(StringDataManager::getString("skillinfo_zhiye"));
		l_pro->setFontName(APP_FONT_NAME);
		l_pro->setFontSize(16);
		l_pro->setColor(ccc3(212,255,151));
		l_pro->setAnchorPoint(ccp(0,0));
		l_pro->setName("l_pro");
		scrollView_skillInfo->addChild(l_pro);
	}
	UILabel * l_proValue = (UILabel *)scrollView_skillInfo->getChildByName("l_proValue");
	if (l_proValue == NULL)
	{
		l_proValue = UILabel::create();
		l_proValue->setText(GameView::getInstance()->myplayer->getActiveRole()->profession().c_str());
		l_proValue->setFontName(APP_FONT_NAME);
		l_proValue->setFontSize(16);
		l_proValue->setColor(ccc3(212,255,151));
		l_proValue->setAnchorPoint(ccp(0,0));
		l_proValue->setName("l_proValue");
		scrollView_skillInfo->addChild(l_proValue);
	}
	else
	{
		l_proValue->setText(GameView::getInstance()->myplayer->getActiveRole()->profession().c_str());
	}
	//施法距离
	UILabel * l_dis = (UILabel *)scrollView_skillInfo->getChildByName("l_dis");
	if (l_dis == NULL)
	{
		l_dis = UILabel::create();
		l_dis->setText(StringDataManager::getString("skillinfo_shifajuli"));
		l_dis->setFontName(APP_FONT_NAME);
		l_dis->setFontSize(16);
		l_dis->setColor(ccc3(212,255,151));
		l_dis->setAnchorPoint(ccp(0,0));
		l_dis->setName("l_dis");
		scrollView_skillInfo->addChild(l_dis);
	}
	char s_distance[5];
	sprintf(s_distance,"%d",gameFightSkill->getCFightSkill()->distance());
	UILabel * l_disValue = (UILabel *)scrollView_skillInfo->getChildByName("l_disValue");
	if (l_disValue == NULL)
	{
		l_disValue = UILabel::create();
		l_disValue->setText(s_distance);
		l_disValue->setFontName(APP_FONT_NAME);
		l_disValue->setFontSize(16);
		l_disValue->setColor(ccc3(212,255,151));
		l_disValue->setAnchorPoint(ccp(0,0));
		l_disValue->setName("l_disValue");
		scrollView_skillInfo->addChild(l_disValue);
	}
	else
	{
		l_disValue->setText(s_distance);
	}
	int thirdLineHeight = secondLineHeight + l_pro->getContentSize().height;
	scroll_height = thirdLineHeight;
	//简介
	UILabel * l_des = (UILabel *)scrollView_skillInfo->getChildByName("l_des");
	if (l_des == NULL)
	{
		l_des = UILabel::create();
		l_des->setText(gameFightSkill->getCFightSkill()->description().c_str());
		l_des->setFontName(APP_FONT_NAME);
		l_des->setFontSize(16);
		l_des->setTextAreaSize(CCSizeMake(230, 0 ));
		l_des->setColor(ccc3(30,255,0));
		l_des->setAnchorPoint(ccp(0,0));
		l_des->setName("l_des");
		scrollView_skillInfo->addChild(l_des);
	}
	else
	{
		l_des->setText(gameFightSkill->getCFightSkill()->description().c_str());
	}
	int fourthLineHeight = thirdLineHeight+l_des->getContentSize().height;
	scroll_height = fourthLineHeight;

	//next info
	if (scrollView_skillInfo->getChildByName("panel_nextInfo"))
	{
		scrollView_skillInfo->getChildByName("panel_nextInfo")->removeFromParent();
	}
	UIPanel * panel_nextInfo = UIPanel::create();
	panel_nextInfo->setName("panel_nextInfo");
	scrollView_skillInfo->addChild(panel_nextInfo);

	int fifthLineHeight = 0;
	int sixthLineHeight = 0;
	int seventhLineHeight = 0;
	int seventhLineHeight_1 = 0;
	int eighthLineHeight = 0;
	int ninthLineHeight = 0;
	int tenthLineHeight = 0;
	int eleventhLineHeight = 0;

	std::string m_str_next_des = "";
	int m_required_level = 0;
	int m_required_point = 0;
	std::string m_str_pre_skill = "";
	std::string m_pre_skill_id = "";
	int m_required_pre_skil_level = 0;
	int m_required_gold = 0;

	if (gameFightSkill->getLevel()<gameFightSkill->getCBaseSkill()->maxlevel())
	{
		//get data
		//下一级
		if (curLevel == 0)
		{
			//主角等级
			m_required_level = gameFightSkill->getCFightSkill()->get_required_level();
			//技能点
			m_required_point = gameFightSkill->getCFightSkill()->get_required_point();
			//XXX技能
			if (gameFightSkill->getCFightSkill()->get_pre_skill() != "")
			{
				CBaseSkill * temp = StaticDataBaseSkill::s_baseSkillData[gameFightSkill->getCFightSkill()->get_pre_skill()];
				m_str_pre_skill.append(temp->name().c_str());
				m_pre_skill_id.append(temp->id());

				m_required_pre_skil_level = gameFightSkill->getCFightSkill()->get_pre_skill_level();
			}
			//金币
			m_required_gold = gameFightSkill->getCFightSkill()->get_required_gold();
		}
		else
		{
			m_str_next_des.append(gameFightSkill->getCFightSkill()->get_next_description());
			//主角等级
			m_required_level = gameFightSkill->getCFightSkill()->get_next_required_level();
			//技能点
			m_required_point = gameFightSkill->getCFightSkill()->get_next_required_point();
			//XXX技能
			if (gameFightSkill->getCFightSkill()->get_next_pre_skill() != "")
			{
				CBaseSkill * temp = StaticDataBaseSkill::s_baseSkillData[gameFightSkill->getCFightSkill()->get_next_pre_skill()];
				m_str_pre_skill.append(temp->name().c_str());
				m_pre_skill_id.append(temp->id());

				m_required_pre_skil_level = gameFightSkill->getCFightSkill()->get_next_pre_skill_level();
			}
			//金币
			m_required_gold = gameFightSkill->getCFightSkill()->get_next_required_gold();
		}

		//create ui
		//下一级
		if (curLevel == 0)
		{
			fifthLineHeight = fourthLineHeight;
			scroll_height = fifthLineHeight;

			sixthLineHeight = fifthLineHeight;
			scroll_height = sixthLineHeight;
		}
		else
		{
			UILabel * l_nextLv = UILabel::create();
			l_nextLv->setText(StringDataManager::getString("skillinfo_xiayiji"));
			l_nextLv->setFontName(APP_FONT_NAME);
			l_nextLv->setFontSize(16);
			l_nextLv->setColor(ccc3(0,255,255));
			l_nextLv->setAnchorPoint(ccp(0,0));
			l_nextLv->setName("l_nextLv");
			panel_nextInfo->addChild(l_nextLv);

			fifthLineHeight = fourthLineHeight+l_nextLv->getContentSize().height*2;
			scroll_height = fifthLineHeight;
			/////////////////////////////////////////////////////
			UILabel * l_nextLvDes = UILabel::create();
			l_nextLvDes->setText(gameFightSkill->getCFightSkill()->get_next_description().c_str());
			l_nextLvDes->setFontName(APP_FONT_NAME);
			l_nextLvDes->setFontSize(16);
			l_nextLvDes->setTextAreaSize(CCSizeMake(230, 0 ));
			l_nextLvDes->setColor(ccc3(0,255,255));
			l_nextLvDes->setAnchorPoint(ccp(0,0));
			l_nextLvDes->setName("l_nextLvDes");
			panel_nextInfo->addChild(l_nextLvDes);

			sixthLineHeight = fifthLineHeight+l_nextLvDes->getContentSize().height;
			scroll_height = sixthLineHeight;
		}

		//升级需求
		UIImageView * image_upgradeNeed = UIImageView::create();
		image_upgradeNeed->setTexture("res_ui/moji_0.png");
		image_upgradeNeed->setScale(0.6f);
		image_upgradeNeed->setAnchorPoint(ccp(0,0));
		image_upgradeNeed->setName("image_upgradeNeed");
		panel_nextInfo->addChild(image_upgradeNeed);

		UILabel * l_upgradeNeed = UILabel::create();
		l_upgradeNeed->setText(StringDataManager::getString("skillinfo_xuexixuqiu"));
		l_upgradeNeed->setFontName(APP_FONT_NAME);
		l_upgradeNeed->setFontSize(18);
		l_upgradeNeed->setColor(ccc3(196,255,68));
		l_upgradeNeed->setAnchorPoint(ccp(0,0));
		l_upgradeNeed->setName("l_upgradeNeed");
		panel_nextInfo->addChild(l_upgradeNeed);

		seventhLineHeight = sixthLineHeight + image_upgradeNeed->getContentSize().height;
		scroll_height = seventhLineHeight;


		//////////////////////////////////////////////////////
		//
		UIImageView * image_second_line = UIImageView::create();
		image_second_line->setTexture("res_ui/henggang.png");
		image_second_line->setScale(0.9f);
		image_second_line->setAnchorPoint(ccp(0,0));
		image_second_line->setName("image_second_line");
		panel_nextInfo->addChild(image_second_line);

		seventhLineHeight_1 = seventhLineHeight + image_second_line->getContentSize().height+7;
		scroll_height = seventhLineHeight_1;

		//主角等级
		if (m_required_level > 0)
		{
			UILabel * l_playerLv = UILabel::create();
			l_playerLv->setText(StringDataManager::getString("skillinfo_zhujuedengji"));
			l_playerLv->setFontName(APP_FONT_NAME);
			l_playerLv->setFontSize(16);
			l_playerLv->setColor(ccc3(212,255,151));
			l_playerLv->setAnchorPoint(ccp(0,0));
			l_playerLv->setName("l_playerLv");
			panel_nextInfo->addChild(l_playerLv);

			UILabel * l_playerLv_colon = UILabel::create();
			l_playerLv_colon->setText(":");
			l_playerLv_colon->setFontName(APP_FONT_NAME);
			l_playerLv_colon->setFontSize(16);
			l_playerLv_colon->setColor(ccc3(212,255,151));
			l_playerLv_colon->setAnchorPoint(ccp(0,0));
			l_playerLv_colon->setName("l_playerLv_colon");
			panel_nextInfo->addChild(l_playerLv_colon);

			char s_required_level[5];
			sprintf(s_required_level,"%d",m_required_level);
			UILabel * l_playerLvValue = UILabel::create();
			l_playerLvValue->setText(s_required_level);
			l_playerLvValue->setFontName(APP_FONT_NAME);
			l_playerLvValue->setFontSize(16);
			l_playerLvValue->setTextAreaSize(CCSizeMake(230, 0 ));
			l_playerLvValue->setColor(ccc3(212,255,151));
			l_playerLvValue->setAnchorPoint(ccp(0,0));
			l_playerLvValue->setName("l_playerLvValue");
			panel_nextInfo->addChild(l_playerLvValue);

			eighthLineHeight = seventhLineHeight_1 + l_playerLv->getContentSize().height;
			scroll_height = eighthLineHeight;

			if (GameView::getInstance()->myplayer->getActiveRole()->level()<m_required_level)  //人物等级未达到技能升级要求
			{
				l_playerLv->setColor(ccc3(255,51,51));
				l_playerLv_colon->setColor(ccc3(255,51,51));
				l_playerLvValue->setColor(ccc3(255,51,51));
			}
		}
		else
		{
			eighthLineHeight = seventhLineHeight_1;
			scroll_height = eighthLineHeight;
		}
		//技能点
		if (m_required_point > 0)
		{
			UIWidget * widget_skillPoint = UIWidget::create();
			panel_nextInfo->addChild(widget_skillPoint);
			widget_skillPoint->setName("widget_skillPoint");

			std::string str_skillPoint = StringDataManager::getString("skillinfo_jinengdian");
			for(int i = 0;i<3;i++)
			{
				std::string str_name = "l_skillPoint";
				char s_index[5];
				sprintf(s_index,"%d",i);
				str_name.append(s_index);
				UILabel * l_skillPoint = UILabel::create();
				l_skillPoint->setText(str_skillPoint.substr(i*3,3).c_str());
				l_skillPoint->setFontName(APP_FONT_NAME);
				l_skillPoint->setFontSize(16);
				l_skillPoint->setColor(ccc3(212,255,151));
				l_skillPoint->setAnchorPoint(ccp(0,0));
				l_skillPoint->setName(str_name.c_str());
				widget_skillPoint->addChild(l_skillPoint);
				l_skillPoint->setPosition(ccp(24*i,0));
			}

			UILabel * l_skillPoint_colon = UILabel::create();
			l_skillPoint_colon->setText(":");
			l_skillPoint_colon->setFontName(APP_FONT_NAME);
			l_skillPoint_colon->setFontSize(16);
			l_skillPoint_colon->setColor(ccc3(212,255,151));
			l_skillPoint_colon->setAnchorPoint(ccp(0,0));
			l_skillPoint_colon->setName("l_skillPoint_colon");
			panel_nextInfo->addChild(l_skillPoint_colon);

			char s_required_point[10];
			sprintf(s_required_point,"%d",m_required_point);
			UILabel * l_skillPointValue = UILabel::create();
			l_skillPointValue->setText(s_required_point);
			l_skillPointValue->setFontName(APP_FONT_NAME);
			l_skillPointValue->setFontSize(16);
			l_skillPointValue->setTextAreaSize(CCSizeMake(230, 0 ));
			l_skillPointValue->setColor(ccc3(212,255,151));
			l_skillPointValue->setAnchorPoint(ccp(0,0));
			l_skillPointValue->setName("l_skillPointValue");
			panel_nextInfo->addChild(l_skillPointValue);

			ninthLineHeight = eighthLineHeight + l_skillPointValue->getContentSize().height;
			scroll_height = ninthLineHeight;

			if (GameView::getInstance()->myplayer->player->skillpoint()<m_required_point)  //人物技能点不足
			{
				for(int i = 0;i<3;i++)
				{
					std::string str_name = "l_skillPoint";
					char s_index[5];
					sprintf(s_index,"%d",i);
					str_name.append(s_index);
					UILabel * l_skillPoint = (UILabel*)widget_skillPoint->getChildByName(str_name.c_str());
					if (l_skillPoint)
					{
						l_skillPoint->setColor(ccc3(255,51,51));
					}
				}
				l_skillPoint_colon->setColor(ccc3(255,51,51));
				l_skillPointValue->setColor(ccc3(255,51,51));
			}
		}
		else
		{
			ninthLineHeight = eighthLineHeight;
			scroll_height = ninthLineHeight;
		}
		//XXX技能
		if (m_str_pre_skill != "")
		{
			UIWidget * widget_pre_skill = UIWidget::create();
			panel_nextInfo->addChild(widget_pre_skill);
			widget_pre_skill->setName("widget_pre_skill");
			if (m_str_pre_skill.size()/3 < 4)
			{
				for(int i = 0;i<m_str_pre_skill.size()/3;i++)
				{
					std::string str_name = "l_preSkill";
					char s_index[5];
					sprintf(s_index,"%d",i);
					str_name.append(s_index);

					UILabel * l_preSkill = UILabel::create();
					l_preSkill->setText(m_str_pre_skill.substr(i*3,3).c_str());
					l_preSkill->setFontName(APP_FONT_NAME);
					l_preSkill->setFontSize(16);
					l_preSkill->setColor(ccc3(212,255,151));
					l_preSkill->setAnchorPoint(ccp(0,0));
					widget_pre_skill->addChild(l_preSkill);
					l_preSkill->setName(str_name.c_str());
					if (m_str_pre_skill.size()/3 == 1)
					{
						l_preSkill->setPosition(ccp(0,0));
					}
					else if (m_str_pre_skill.size()/3 == 2)
					{
						l_preSkill->setPosition(ccp(48*i,0));
					}
					else if (m_str_pre_skill.size()/3 == 3)
					{
						l_preSkill->setPosition(ccp(24*i,0));
					}
				}
			}
			else
			{
				UILabel * l_preSkill = UILabel::create();
				l_preSkill->setText(m_str_pre_skill.c_str());
				l_preSkill->setFontName(APP_FONT_NAME);
				l_preSkill->setFontSize(16);
				l_preSkill->setColor(ccc3(212,255,151));
				l_preSkill->setAnchorPoint(ccp(0,0));
				l_preSkill->setPosition(ccp(0,0));
				widget_pre_skill->addChild(l_preSkill);
				l_preSkill->setName("l_preSkill");
			}

			UILabel * l_preSkill_colon = UILabel::create();
			l_preSkill_colon->setText(":");
			l_preSkill_colon->setFontName(APP_FONT_NAME);
			l_preSkill_colon->setFontSize(16);
			l_preSkill_colon->setColor(ccc3(212,255,151));
			l_preSkill_colon->setAnchorPoint(ccp(0,0));
			l_preSkill_colon->setName("l_preSkill_colon");
			panel_nextInfo->addChild(l_preSkill_colon);

			char s_pre_skill_level[5];
			sprintf(s_pre_skill_level,"%d",m_required_pre_skil_level);
			UILabel * l_preSkillValue = UILabel::create();
			l_preSkillValue->setText(s_pre_skill_level);
			l_preSkillValue->setFontName(APP_FONT_NAME);
			l_preSkillValue->setFontSize(16);
			l_preSkillValue->setTextAreaSize(CCSizeMake(230, 0 ));
			l_preSkillValue->setColor(ccc3(212,255,151));
			l_preSkillValue->setAnchorPoint(ccp(0,0));
			l_preSkillValue->setName("l_preSkillValue");
			panel_nextInfo->addChild(l_preSkillValue);

			tenthLineHeight = ninthLineHeight + l_preSkillValue->getContentSize().height;
			scroll_height = tenthLineHeight;

			std::map<std::string,GameFightSkill*>::const_iterator cIter;
			cIter = GameView::getInstance()->GameFightSkillList.find(m_pre_skill_id);
			if (cIter != GameView::getInstance()->GameFightSkillList.end()) // 没找到就是指向END了  
			{
				GameFightSkill * gameFightSkillStuded = GameView::getInstance()->GameFightSkillList[m_pre_skill_id];
				if (gameFightSkillStuded)
				{
					if (gameFightSkillStuded->getLevel() < m_required_pre_skil_level)
					{
						if (m_str_pre_skill.size()/3 < 4)
						{
							for(int i = 0;i<m_str_pre_skill.size()/3;i++)
							{
								std::string str_name = "l_preSkill";
								char s_index[5];
								sprintf(s_index,"%d",i);
								str_name.append(s_index);

								UILabel * l_preSkill = (UILabel*)widget_pre_skill->getChildByName(str_name.c_str());
								if (l_preSkill)
								{
									l_preSkill->setColor(ccc3(255,51,51));
								}
							}
						}
						else
						{
							UILabel * l_preSkill = (UILabel*)widget_pre_skill->getChildByName("l_preSkill");
							if (l_preSkill)
							{
								l_preSkill->setColor(ccc3(255,51,51));
							}
						}
						l_preSkill_colon->setColor(ccc3(255,51,51));
						l_preSkillValue->setColor(ccc3(255,51,51));
					}
				}
			}
		}
		else
		{
			tenthLineHeight = ninthLineHeight;
			scroll_height = tenthLineHeight;
		}
		//金币
		if (m_required_gold > 0)
		{
			UIWidget * widget_gold = UIWidget::create();
			panel_nextInfo->addChild(widget_gold);
			widget_gold->setName("widget_gold");

			std::string str_gold = StringDataManager::getString("skillinfo_jinbi");
			for(int i = 0;i<2;i++)
			{
				std::string str_name = "l_gold";
				char s_index[5];
				sprintf(s_index,"%d",i);
				str_name.append(s_index);
				UILabel * l_gold = UILabel::create();
				l_gold->setText(str_gold.substr(i*3,3).c_str());
				l_gold->setFontName(APP_FONT_NAME);
				l_gold->setFontSize(16);
				l_gold->setColor(ccc3(212,255,151));
				l_gold->setAnchorPoint(ccp(0,0));
				l_gold->setName(str_name.c_str());
				widget_gold->addChild(l_gold);
				l_gold->setPosition(ccp(48*i,0));
			}

			UILabel * l_gold_colon = UILabel::create();
			l_gold_colon->setText(":");
			l_gold_colon->setFontName(APP_FONT_NAME);
			l_gold_colon->setFontSize(16);
			l_gold_colon->setColor(ccc3(212,255,151));
			l_gold_colon->setAnchorPoint(ccp(0,0));
			l_gold_colon->setName("l_gold_colon");
			panel_nextInfo->addChild(l_gold_colon);

			char s_required_gold[20];
			sprintf(s_required_gold,"%d",m_required_gold);
			UILabel * l_goldValue = UILabel::create();
			l_goldValue->setText(s_required_gold);
			l_goldValue->setFontName(APP_FONT_NAME);
			l_goldValue->setFontSize(16);
			l_goldValue->setColor(ccc3(212,255,151));
			l_goldValue->setAnchorPoint(ccp(0,0));
			l_goldValue->setName("l_goldValue");
			panel_nextInfo->addChild(l_goldValue);

			UIImageView * image_gold = UIImageView::create();
			image_gold->setTexture("res_ui/coins.png");
			image_gold->setAnchorPoint(ccp(0,0));
			image_gold->setName("image_gold");
			image_gold->setScale(0.85f);
			panel_nextInfo->addChild(image_gold);

			eleventhLineHeight = tenthLineHeight + l_goldValue->getContentSize().height;
			scroll_height = eleventhLineHeight;

			if(GameView::getInstance()->getPlayerGold()<m_required_gold)//金币不足
			{
				for(int i = 0;i<2;i++)
				{
					std::string str_name = "l_gold";
					char s_index[5];
					sprintf(s_index,"%d",i);
					str_name.append(s_index);
					UILabel * l_gold = (UILabel*)widget_gold->getChildByName(str_name.c_str());
					if (l_gold)
					{
						l_gold->setColor(ccc3(255,51,51));
					}
				}
				l_gold_colon->setColor(ccc3(255,51,51));
				l_goldValue->setColor(ccc3(255,51,51));
			}
		}
		else
		{
			eleventhLineHeight = tenthLineHeight;
			scroll_height = eleventhLineHeight;
		}
	}

	int innerWidth = scrollView_skillInfo->getRect().size.width;
	int innerHeight = scroll_height;
	if (scroll_height < scrollView_skillInfo->getRect().size.height)
	{
		innerHeight = scrollView_skillInfo->getRect().size.height;
	}
	scrollView_skillInfo->setInnerContainerSize(CCSizeMake(innerWidth,innerHeight));

	image_nameFrame->setPosition(ccp(8,innerHeight - zeroLineHeight));
	l_name->setPosition(ccp(15,innerHeight - zeroLineHeight));
	image_first_line->setPosition(ccp(5,innerHeight - zeroLineHeight_1+5));
	l_type->setPosition(ccp(8,innerHeight - firstLineHeight));
	l_mp->setPosition(ccp(105,innerHeight - firstLineHeight));
	l_mpValue->setPosition(ccp(105+l_mp->getContentSize().width+2,innerHeight - firstLineHeight));
	l_lv->setPosition(ccp(8,innerHeight - secondLineHeight));
	l_lvValue->setPosition(ccp(8+l_lv->getContentSize().width+2,innerHeight - secondLineHeight));
	l_cd->setPosition(ccp(105,innerHeight - secondLineHeight));
	l_cdValue->setPosition(ccp(105+l_cd ->getContentSize().width+2,innerHeight - secondLineHeight));
	l_pro->setPosition(ccp(8,innerHeight - thirdLineHeight));
	l_proValue->setPosition(ccp(8+l_pro->getContentSize().width+2,innerHeight - thirdLineHeight));
	l_dis->setPosition(ccp(105,innerHeight - thirdLineHeight));
	l_disValue->setPosition(ccp(105+l_dis->getContentSize().width+2,innerHeight - thirdLineHeight));

	if (gameFightSkill->getCBaseSkill()->usemodel() == 2)
	{
		l_mp->setVisible(false);
		l_mpValue->setVisible(false);
		l_cd->setVisible(false);
		l_cdValue->setVisible(false);
		l_dis->setVisible(false);
		l_disValue->setVisible(false);
	}
	else
	{
		l_mp->setVisible(true);
		l_mpValue->setVisible(true);
		l_cd->setVisible(true);
		l_cdValue->setVisible(true);
		l_dis->setVisible(true);
		l_disValue->setVisible(true);
	}

	l_des->setPosition(ccp(8,innerHeight - fourthLineHeight));
	if (gameFightSkill->getLevel()<gameFightSkill->getCBaseSkill()->maxlevel())
	{
		if (panel_nextInfo->getChildByName("l_nextLv"))
		{
			panel_nextInfo->getChildByName("l_nextLv")->setPosition(ccp(8,innerHeight - fifthLineHeight));
		}

		if (panel_nextInfo->getChildByName("l_nextLvDes"))
		{
			panel_nextInfo->getChildByName("l_nextLvDes")->setPosition(ccp(8,innerHeight - sixthLineHeight));
		}

		if (panel_nextInfo->getChildByName("image_upgradeNeed"))
		{
			panel_nextInfo->getChildByName("image_upgradeNeed")->setPosition(ccp(8,innerHeight - seventhLineHeight));
		}

		if (panel_nextInfo->getChildByName("l_upgradeNeed"))
		{
			panel_nextInfo->getChildByName("l_upgradeNeed")->setPosition(ccp(15,innerHeight - seventhLineHeight));
		}

		if (panel_nextInfo->getChildByName("image_second_line"))
		{
			panel_nextInfo->getChildByName("image_second_line")->setPosition(ccp(5,innerHeight - seventhLineHeight_1+5));
		}

		int temp_width = 64;

		//主角等级
		if (m_required_level>0)
		{
			if (panel_nextInfo->getChildByName("l_playerLv"))
			{
				panel_nextInfo->getChildByName("l_playerLv")->setPosition(ccp(8,innerHeight - eighthLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_playerLv_colon"))
			{
				panel_nextInfo->getChildByName("l_playerLv_colon")->setPosition(ccp(76,innerHeight - eighthLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_playerLvValue"))
			{
				panel_nextInfo->getChildByName("l_playerLvValue")->setPosition(ccp(85,innerHeight - eighthLineHeight));
			}
		}
		//技能点
		if (m_required_point>0)
		{
			if (panel_nextInfo->getChildByName("widget_skillPoint"))
			{
				panel_nextInfo->getChildByName("widget_skillPoint")->setPosition(ccp(8,innerHeight - ninthLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_skillPoint_colon"))
			{
				panel_nextInfo->getChildByName("l_skillPoint_colon")->setPosition(ccp(76,innerHeight - ninthLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_skillPointValue"))
			{
				panel_nextInfo->getChildByName("l_skillPointValue")->setPosition(ccp(85,innerHeight - ninthLineHeight));
			}
		}
		//XXX技能
		if (m_str_pre_skill != "")
		{
			if (panel_nextInfo->getChildByName("widget_pre_skill"))
			{
				panel_nextInfo->getChildByName("widget_pre_skill")->setPosition(ccp(8,innerHeight - tenthLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_preSkill_colon"))
			{
				panel_nextInfo->getChildByName("l_preSkill_colon")->setPosition(ccp(76,innerHeight - tenthLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_preSkillValue"))
			{
				panel_nextInfo->getChildByName("l_preSkillValue")->setPosition(ccp(85,innerHeight - tenthLineHeight));
			}
		}
		//金币
		if (m_required_gold > 0)
		{
			if (panel_nextInfo->getChildByName("widget_gold"))
			{
				panel_nextInfo->getChildByName("widget_gold")->setPosition(ccp(8,innerHeight - eleventhLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_gold_colon"))
			{
				panel_nextInfo->getChildByName("l_gold_colon")->setPosition(ccp(76,innerHeight - eleventhLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_goldValue"))
			{
				panel_nextInfo->getChildByName("l_goldValue")->setPosition(ccp(85,innerHeight - eleventhLineHeight));
			}
			if (panel_nextInfo->getChildByName("image_gold"))
			{
				panel_nextInfo->getChildByName("image_gold")->setPosition(ccp(88+panel_nextInfo->getChildByName("l_goldValue")->getContentSize().width,innerHeight - eleventhLineHeight));
			}
		}
	}
	scrollView_skillInfo->jumpToTop();
}

void SkillScene::RefreshToDefault()
{
	this->RefreshSkillPoint();
	//////////////////////////////////////////////////////////////////
	//设置选中状态的图片位置
	//默认选中普通攻击技能
	ImageView_select->setTexture("res_ui/jineng/light_name.png");
	ImageView_select->setPosition(ccp(202,403));  //(62,490)
	ImageView_select->setVisible(true);
	GameFightSkill * gameFightSkill = new GameFightSkill();
	bool isHave = false;
	int curLevel;
	std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GameFightSkillList.begin();
	for ( ; it != GameView::getInstance()->GameFightSkillList.end(); ++ it )
	{
		GameFightSkill * tempSkill = it->second;
		if (strcmp(tempSkill->getId().c_str(),curSkillId.c_str()) == 0)
		{
			curLevel = tempSkill->getLevel();
			curFightSkill->initSkill(curSkillId,curSkillId,tempSkill->getLevel(),GameActor::type_player);
			gameFightSkill->initSkill(curSkillId,curSkillId,tempSkill->getLevel(),GameActor::type_player);
			isHave = true;
			break;
		}
	}

	if (isHave == false)    //没学会
	{
		curLevel = 0;
		curFightSkill->initSkill(curSkillId,curSkillId,1,GameActor::type_player);
		gameFightSkill->initSkill(curSkillId,curSkillId,1,GameActor::type_player);
	}

	//默认技能的功能按钮状态
// 		Button_study->setVisible(false);
// 		Button_study->setTouchEnable(false);
// 		Button_levUp->setVisible(true);
// 		Button_levUp->setNormalTexture(BUTTON_DISABLED_PATH);
// 		Button_levUp->setTouchEnable(false);
// 		//Button_levDown->setBright(false);
// 		Button_levDown->setNormalTexture(BUTTON_DISABLED_PATH);
// 		Button_levDown->setTouchEnable(false);
// 		//Button_convenient->setBright(false);
// 		Button_convenient->setNormalTexture(BUTTON_NORMAL_PATH);
// 		Button_convenient->setTouchEnable(true);

	if (isHave == false)    //没学会
	{
		Button_study->setVisible(true);
		Button_study->setTouchEnable(true);
		Button_levUp->setVisible(false);
		Button_levUp->setTouchEnable(false);
		//Button_levDown->setBright(false);
		Button_levDown->setNormalTexture(BUTTON_DISABLED_PATH);
		Button_levDown->setTouchEnable(false);
		//Button_convenient->setBright(false);
		Button_convenient->setNormalTexture(BUTTON_DISABLED_PATH);
		Button_convenient->setTouchEnable(false);
	}
	else                //已学会
	{
		Button_study->setVisible(false);
		Button_study->setTouchEnable(false);
		Button_levUp->setVisible(true);
		Button_levUp->setTouchEnable(true);
		Button_levUp->setNormalTexture(BUTTON_NORMAL_PATH);
		//Button_levDown->setBright(true);
		Button_levDown->setNormalTexture(BUTTON_NORMAL_PATH);
		Button_levDown->setTouchEnable(true);
		Button_convenient->setNormalTexture(BUTTON_NORMAL_PATH);
		Button_convenient->setTouchEnable(true);
	}

	RefreshOneItem(curLevel,gameFightSkill);
	RefreshOneItemInfo(curLevel,gameFightSkill);

	delete gameFightSkill;
	//////////////////////////////////////////////////////////////////
}

void SkillScene::RefreshMumouPos( CShortCut *shortcut )
{
	ccArray * childrenArray = skillpanel->getChildren()->data;
	int length = childrenArray->num;
	std::string commonString = "SkillBtn_";

	for (int i = 0;i<length;++i)
	{
		UIWidget * child = (UIWidget*)childrenArray->arr[i];
		std::string childName = child->getName();
		std::string skillid = childName.substr(9);

		if (childName.find(commonString)<childName.length())
		{
			if (strcmp(shortcut->skillpropid().c_str(),skillid.c_str()) == 0)
			{
// 				//ImageView_musou->setPosition(ccp(child->getPosition().x-41,child->getPosition().y+170));
// 				ImageView_musou->setPosition(ccp(child->getPosition().x-41,child->getPosition().y+(u_scrollView->getInnerContainerSize().height-70-u_scrollView->getSize().height)));
// 				ImageView_musou->setScale(3.0f);
// 				ImageView_musou->setVisible(true);
// 				CCFiniteTimeAction*  action = CCSequence::create(
// 					CCEaseElasticIn::create(CCScaleTo::create(0.7f,1.05f)),
// 					NULL);
// 				ImageView_musou->runAction(action);
			}
		}	
	}
}

void SkillScene::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void SkillScene::addCCTutorialIndicator1( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,30,60);
// 	tutorialIndicator->setPosition(ccp(pos.x+86+_w,pos.y+100+_h));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);
	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,52,52,true);
	tutorialIndicator->setDrawNodePos(ccp(pos.x+27+_w,pos.y+10+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void SkillScene::addCCTutorialIndicator2( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,60,65);
// 	tutorialIndicator->setPosition(ccp(pos.x-45+_w,pos.y+100+_h));
// 	//tutorialIndicator->setPosition(ccp(pos.x+60,pos.y+65));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);
	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,78,44,true);
	tutorialIndicator->setDrawNodePos(ccp(pos.x+_w,pos.y+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void SkillScene::addCCTutorialIndicator3( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,20,40);
// 	tutorialIndicator->setPosition(ccp(pos.x-60+_w,pos.y-75+_h));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);
	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,40,40,true);
	tutorialIndicator->setDrawNodePos(ccp(pos.x+_w,pos.y+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void SkillScene::addCCTutorialIndicator4( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,75,50);
	tutorialIndicator->setPosition(ccp(pos.x+240+_w,pos.y-70+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	this->addChild(tutorialIndicator);
}


void SkillScene::removeCCTutorialIndicator()
{
	CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		CCTeachingGuide* teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}

void SkillScene::autoEquipSkill(std::string skillId)
{
	ShortcutLayer * shortCutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
	if (!shortCutLayer)
		return;

	int index = -1;
	for (int i =0;i<MUSOUSKILL_INDEX;++i)
	{
		ShortcutSlot * shortCutSlot = (ShortcutSlot*)shortCutLayer->getChildByTag(i+100);
		if (!shortCutSlot)
			continue;

		if (!shortCutSlot->isVisible())
			continue;

		if (shortCutSlot->slotType != ShortcutSlot::T_Void)
			continue;

		index = i;
		break;
	}

	if (index == -1)
		return;

	bool isExist = false;
	for (int i =0;i<MUSOUSKILL_INDEX;++i)
	{
		ShortcutSlot * shortCutSlot = (ShortcutSlot*)shortCutLayer->getChildByTag(i+100);
		if (!shortCutSlot)
			continue;
			
		if (shortCutSlot->curShortCut->has_skillpropid())
		{
			if(shortCutSlot->curShortCut->skillpropid() == skillId)
			{
				isExist = true;
				break;
			}
		}
	}

	if (isExist)
		return;

	CShortCut * tempShortCut = new CShortCut();
	tempShortCut->set_index(index+100);
	tempShortCut->set_storedtype(1);
	tempShortCut->set_skillpropid(skillId);
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1122, tempShortCut);
	delete tempShortCut;
}

void SkillScene::createTalentAndMusou()
{
	//龙魂技能
	musouSkillLayer = MusouSkillScene::create();
	u_layer->addChild(musouSkillLayer);
	musouSkillLayer->setVisible(false);
	 
	//龙魂天赋
	musouSkillTalent = MusouSkillTalent::create();
	u_layer->addChild(musouSkillTalent);
	musouSkillTalent->setVisible(false);
}

void SkillScene::AddBonusSpecialEffect( std::string skillId )
{
	std::string commonString = "SkillBtn_";
	commonString.append(skillId);
	if (skillpanel->getChildByName(commonString.c_str()) != NULL)
	{
		UIButton *child = (UIButton *)skillpanel->getChildByName(commonString.c_str());
		if (child->getRenderer())
		{
			if (child->getRenderer()->getChildByTag(kTag_BonusSpecialEffect))
			{
				child->getRenderer()->getChildByTag(kTag_BonusSpecialEffect)->removeFromParent();
			}
		}

		// Bonus Special Effect
		CCNodeRGBA* pNode = BonusSpecialEffect::create();
		pNode->setScale(0.8f);
		pNode->setPosition(ccp(30,30));
		pNode->setTag(kTag_BonusSpecialEffect);
		child->addCCNode(pNode);
		pNode->setZOrder(5);
	}
}

void SkillScene::RemoveBonumSpecialEffect()
{
	ccArray * childrenArray = skillpanel->getChildren()->data;
	int length = childrenArray->num;
	std::string commonString = "SkillBtn_";

	for (int i = 0;i<length;++i)
	{
		UIWidget * child = (UIWidget*)childrenArray->arr[i];
		if (!child->getRenderer())
			continue;

		if (child->getRenderer()->getChildByTag(kTag_BonusSpecialEffect))
		{
			child->getRenderer()->getChildByTag(kTag_BonusSpecialEffect)->removeFromParent();
		}
	}
}

CCNode * SkillScene::GetSkillVarirtySprite( const  char * skillid )
{
	std::map<std::string,CBaseSkill*>::const_iterator cIter;
	cIter = StaticDataBaseSkill::s_baseSkillData.find(skillid);
	if (cIter == StaticDataBaseSkill::s_baseSkillData.end()) // 没找到就是指向END了  
	{
		return NULL;
	}

	CBaseSkill * baseSkill = StaticDataBaseSkill::s_baseSkillData[skillid];

	std::string str_skillVariety_path = "res_ui/font/";
	switch(baseSkill->get_skill_variety())
	{
	case 0:
		{
		}
		break;
	case 1:
		{
			str_skillVariety_path.append("dan.png");
		}
		break;
	case 2:
		{
			str_skillVariety_path.append("qun.png");
		}
		break;
	case 3:
		{
			str_skillVariety_path.append("kong.png");
		}
		break;
	case 4:
		{
			str_skillVariety_path.append("liao.png");
		}
		break;
	case 5:
		{
			str_skillVariety_path.append("te.png");
		}
		break;
	case 6:
		{
			str_skillVariety_path.append("fu.png");
		}
		break;
	}
	if (str_skillVariety_path.find(".png")!=string::npos)
	{
		CCSprite * sprite_skillVariety = CCSprite::create(str_skillVariety_path.c_str());
		return sprite_skillVariety;
	}
	return NULL;
}

UIWidget * SkillScene::GetSkillVarirtyImageView( const  char * skillid )
{
	std::map<std::string,CBaseSkill*>::const_iterator cIter;
	cIter = StaticDataBaseSkill::s_baseSkillData.find(skillid);
	if (cIter == StaticDataBaseSkill::s_baseSkillData.end()) // 没找到就是指向END了  
	{
		return NULL;
	}

	CBaseSkill * baseSkill = StaticDataBaseSkill::s_baseSkillData[skillid];

	std::string str_skillVariety_path = "res_ui/font/";
	switch(baseSkill->get_skill_variety())
	{
	case 0:
		{
		}
		break;
	case 1:
		{
			str_skillVariety_path.append("dan.png");
		}
		break;
	case 2:
		{
			str_skillVariety_path.append("qun.png");
		}
		break;
	case 3:
		{
			str_skillVariety_path.append("kong.png");
		}
		break;
	case 4:
		{
			str_skillVariety_path.append("liao.png");
		}
		break;
	case 5:
		{
			str_skillVariety_path.append("te.png");
		}
		break;
	case 6:
		{
			str_skillVariety_path.append("fu.png");
		}
		break;
	}
	if (str_skillVariety_path.find(".png")!=string::npos)
	{
		UIImageView * image_skillVariety = UIImageView::create();
		image_skillVariety->setTexture(str_skillVariety_path.c_str());
		return image_skillVariety;
	}
	return NULL;
}

bool SkillScene::IsExistSkillEnableToLevelUp()
{
	for(int i = 0;i<GameView::getInstance()->CBaseSkillList.size();i++)
	{
		bool isStuded = false;
		CBaseSkill * skillBase = GameView::getInstance()->CBaseSkillList.at(i);
		if(skillBase->get_owner() != 1)
			continue;

		std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GameFightSkillList.begin();
		for ( ; it != GameView::getInstance()->GameFightSkillList.end(); ++ it )
		{
			GameFightSkill * fightSkill = it->second;
			if (strcmp(fightSkill->getId().c_str(),skillBase->id().c_str()) == 0)
			{
				isStuded = true;
				if (IsEnableLevelUp(fightSkill->getLevel(),fightSkill))
				{
					return true;
				}
			}
		}
		if (!isStuded)
		{
			GameFightSkill * fightSkill = new GameFightSkill();
			fightSkill->initSkill(skillBase->id(),skillBase->id(),1,GameActor::type_player);
			if (IsEnableLevelUp(1,fightSkill))
			{
				return true;
			}
		}
	}

	return false;
}

bool SkillScene::IsEnableLevelUp( int curLevel,GameFightSkill * gameFightSkill )
{
	if (curLevel<gameFightSkill->getCBaseSkill()->maxlevel())
	{
		//下一级
		if (curLevel == 0)
		{
			//主角等级
			if (gameFightSkill->getCFightSkill()->get_required_level()>0)
			{
				if (GameView::getInstance()->myplayer->getActiveRole()->level()<gameFightSkill->getCFightSkill()->get_required_level())  //人物等级未达到技能升级要求
				{
					return false;
				}
			}
			//技能点
			if (gameFightSkill->getCFightSkill()->get_required_point()>0)
			{
				if(GameView::getInstance()->myplayer->player->skillpoint()<gameFightSkill->getCFightSkill()->get_required_point())//技能点不足
				{
					return false;
				}
			}
			//XXX技能
			if (gameFightSkill->getCFightSkill()->get_pre_skill() != "")
			{
				std::map<std::string,GameFightSkill*>::const_iterator cIter;
				cIter = GameView::getInstance()->GameFightSkillList.find(gameFightSkill->getCFightSkill()->get_pre_skill());
				if (cIter == GameView::getInstance()->GameFightSkillList.end()) // 没找到就是指向END了  
				{
					return false;
				}
				else
				{
					GameFightSkill * gameFightSkillStuded = GameView::getInstance()->GameFightSkillList[gameFightSkill->getCFightSkill()->get_pre_skill()];
					if (gameFightSkillStuded)
					{
						if (gameFightSkillStuded->getLevel() < gameFightSkill->getCFightSkill()->get_pre_skill_level())
						{
							return false;
						}
					}
				}
			}
			//金币
			if (gameFightSkill->getCFightSkill()->get_required_gold()>0)
			{
				if(GameView::getInstance()->getPlayerGold()<gameFightSkill->getCFightSkill()->get_required_gold())//金币不足
				{
					return false;
				}
			}
		}
		else
		{
			/////////////////////////////////////////////////////
			//主角等级
			if (gameFightSkill->getCFightSkill()->get_next_required_level()>0)
			{
				if (GameView::getInstance()->myplayer->getActiveRole()->level()<gameFightSkill->getCFightSkill()->get_next_required_level())  //人物等级未达到技能升级要求
				{
					return false;
				}
			}
			//技能点
			if (gameFightSkill->getCFightSkill()->get_next_required_point()>0)
			{
				if(GameView::getInstance()->myplayer->player->skillpoint()<gameFightSkill->getCFightSkill()->get_next_required_point())//技能点不足
				{
					return false;
				}
			}
			//XXX技能
			if (gameFightSkill->getCFightSkill()->get_next_pre_skill() != "")
			{
				std::map<std::string,GameFightSkill*>::const_iterator cIter;
				cIter = GameView::getInstance()->GameFightSkillList.find(gameFightSkill->getCFightSkill()->get_next_pre_skill());
				if (cIter == GameView::getInstance()->GameFightSkillList.end()) // 没找到就是指向END了  
				{
					return false;
				}
				else
				{
					GameFightSkill * gameFightSkillStuded = GameView::getInstance()->GameFightSkillList[gameFightSkill->getCFightSkill()->get_next_pre_skill()];
					if (gameFightSkillStuded)
					{
						if (gameFightSkillStuded->getLevel() < gameFightSkill->getCFightSkill()->get_next_pre_skill_level())
						{
							return false;
						}
					}
				}
			}
			//金币
			if (gameFightSkill->getCFightSkill()->get_next_required_gold()>0)
			{
				if(GameView::getInstance()->getPlayerGold()<gameFightSkill->getCFightSkill()->get_next_required_gold())//金币不足
				{
					return false;
				}
			}
		}

		return true;
	}

	return false;
}




