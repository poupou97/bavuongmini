
#ifndef _SKILLSCENE_SKILLSCENE_H_
#define _SKILLSCENE_SKILLSCENE_H_


#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "../../legend_script/ScriptHandlerProtocol.h"
#include "../../legend_script/CCTutorialIndicator.h"

USING_NS_CC;
USING_NS_CC_EXT;

class GameFightSkill;
class CShortCut;
class MusouSkillScene;
class UITab;
class MusouSkillTalent;

class SkillScene : public UIScene, public ScriptHandlerProtocol
{
public:
	SkillScene();
	~SkillScene();

	struct ReqForPreeless{
		std::string skillid;
		int grid;
		long long roleId;
		int roleType;
	};

	static SkillScene* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	void CloseEvent(CCObject *pSender);
	void SkillTypeTabIndexChangedEvent(CCObject *pSender);

	void createTalentAndMusou();

// 	virtual void scrollViewDidScroll(CCScrollView* view);
// 	virtual void scrollViewDidZoom(CCScrollView* view);

	void SkillInfoEvent(CCObject *pSender);
	void ResetEvent(CCObject *pSender); /*******重置*******/
	void ConvenientEvent(CCObject *pSender); /*******便捷*******/
	void SureResetEvent(CCObject *pSender);/*******确定重置*******/

	void StudyEvent(CCObject *pSender);     /********学习*******/
	void SureToStudyEvent(CCObject *pSender);

	void LevelUpEvent(CCObject *pSender);   /********升级*******/
	void LevelDownEvent(CCObject *pSender);  /********降级*******/
	void SureToLevUpSkill(CCObject *pSender);   
	void SureToLevDownSkill(CCObject *pSender);

	void PreelessEvent(CCObject *pSender);

	void RefreshToDefault();

	void RefreshSkillPoint();
	void RefreshFucBtn(std::string skillid);/**********某个技能更新后刷新按钮状态*********/
	void RefreshOneItem(int curLevel,GameFightSkill * gameFightSkill);/**********更新某个技能信息(等级)**********/
	void RefreshOneItemInfo(int curLevel,GameFightSkill * gameFightSkill);/**********更新某个技能信息(等级)**********/
	void RefreshData();

	//设置无双标识的位置（应该有个动画,因为刚进入该页面时在RefreshData中已经设置好了位置，之后每次都是变更所有应该有动画）
	void RefreshMumouPos(CShortCut *shortcut);

	//教学
	virtual void registerScriptCommand(int scriptId);
	//选技能
	void addCCTutorialIndicator1(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction  );
	//升级或装配按钮
	void addCCTutorialIndicator2(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction  );
	//关闭按钮
	void addCCTutorialIndicator3(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction  );
	//龙魂标签
	void addCCTutorialIndicator4(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction  );
	void removeCCTutorialIndicator();

	static void autoEquipSkill(std::string skillId);

	void AddBonusSpecialEffect(std::string skillId);
	void RemoveBonumSpecialEffect();

	UILabel * labelBmfont;

	static CCNode * GetSkillVarirtySprite(const char * skillid);
	static UIWidget * GetSkillVarirtyImageView(const char * skillid);

	static bool IsExistSkillEnableToLevelUp();
	static bool IsEnableLevelUp(int curLevel,GameFightSkill * gameFightSkill);
public:
	UIPanel * roleSkillPanel;
	UIPanel * skillpanel;

	UILayer * u_layer;
	UILayer * AllSkillLayer;

	MusouSkillScene * musouSkillLayer;

	MusouSkillTalent * musouSkillTalent;

	UITab *SkillTypeTab;

	UIScrollView * u_scrollView;
	//第一个技能
	UIButton * m_firstAtkButton;
	std::string m_firstSkillName ;
	//已用技能点
	UILabel * label_usedSkillPoint;
	//剩余技能点
	UILabel * label_notUsedSkillPoint;
	//剩余技能点底框（方便加小N）
	UIImageView * image_remainFightpPointFrame;
	//升级需要
	UITextArea * textArea_lvUp;
	//关闭按钮
	UIButton * Button_close;
	//升级按钮
	UIButton * Button_levUp;
	//降级按钮
	UIButton * Button_levDown;
	//快捷按钮
	UIButton * Button_convenient;
	//学习按钮
	UIButton * Button_study;
	//无双按钮
	UIButton * Button_preeless;
	//选中图标
	UIImageView * ImageView_select;
	//无双标识
	UIImageView * ImageView_musou;
	
	UILabelBMFont * Label_skillName;
	//UITextArea * textArea_lv;
	//UITextArea * textArea_des;

	//下一级描述
	CCLabelTTF * l_nextLv;
	//下一级
	CCLabelTTF * t_nextLvDes;
	//升级需要
	CCSprite * s_upgradeNeed;
	CCLabelTTF * l_upgradeNeed;
	CCSprite * s_second_line;
	//人物等级
	CCLabelTTF * l_playerLv;
	CCLabelTTF * l_playerLvValue;
	//技能点
	CCLabelTTF * l_skillPoint;
	CCLabelTTF * l_skillPointValue;
	//前置技能
	CCLabelTTF * l_preSkill;
	CCLabelTTF * l_preSkillValue;
	//金币
	CCLabelTTF * l_gold;
	CCLabelTTF * l_goldValue;
	//道具
	CCLabelTTF * l_prop;
	CCLabelTTF * l_propValue;

	
	int curSkillLevel;

	//教学
	int mTutorialScriptInstanceId;


public: 
	std::string defaultAtkSkill ;
	std::string curSkillId;
	UIPanel *ppanel;

	UIPanel * panel_mengjiang;
	UIPanel * panel_guimou;
	UIPanel * panel_shenshe;
	UIPanel * panel_haojie;

	GameFightSkill * curFightSkill;
	int updown;
};
#endif;

