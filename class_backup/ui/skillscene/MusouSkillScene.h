#ifndef _SKILLUI_MUSOUSKILLSCENE_H_
#define _SKILLUI_MUSOUSKILLSCENE_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "../../legend_script/CCTutorialIndicator.h"

USING_NS_CC;
USING_NS_CC_EXT;

class GameFightSkill;
class GeneralMusouSkillListUI;

class MusouSkillScene : public UIScene
{
public:
	MusouSkillScene();
	~MusouSkillScene();

	static MusouSkillScene* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual void setVisible(bool visible);

	void SetEvent(CCObject *pSender);
	void SuggestSetEvent(CCObject *pSender);

	void RefreshSkillInfo(int curLevel,GameFightSkill * gameFightSkill);/**********更新某个技能信息(等级)**********/
	//添加龙魂标示
	void AddMusouFlagWithAnm(std::string skillId);

	//教学
	virtual void registerScriptCommand(int scriptId);
	//选技能
	void addCCTutorialIndicator1(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction  );
	void removeCCTutorialIndicator();

public: 
	GeneralMusouSkillListUI *generalMusouSkillListUI;
	//选择的角色技能
	std::string curSelectRoleSkillSkillId;
	//本次选择的是主角还是武将
	bool isRoleOrGeneral;

	UIButton * btn_set;
	UIButton * btn_suggestSet;
private:
	UIPanel * panel_musouSkill;
	UILayer * layer_musouSkill;
	UILayer * layer_upper;


	//主角头像
	UIImageView * ImageView_roleheat;
	//主角名字
	UILabel * L_roleName;

	//下一级描述
	CCLabelTTF * l_nextLv;
	//下一级
	CCLabelTTF * t_nextLvDes;
	//升级需要
	CCSprite * s_upgradeNeed;
	CCLabelTTF * l_upgradeNeed;
	CCSprite * s_second_line;
	//人物等级
	CCLabelTTF * l_playerLv;
	CCLabelTTF * l_playerLvValue;
	//技能点
	CCLabelTTF * l_skillPoint;
	CCLabelTTF * l_skillPointValue;
	//前置技能
	CCLabelTTF * l_preSkill;
	CCLabelTTF * l_preSkillValue;
	//金币
	CCLabelTTF * l_gold;
	CCLabelTTF * l_goldValue;
	//道具
	CCLabelTTF * l_prop;
	CCLabelTTF * l_propValue;

//教学
	int mTutorialScriptInstanceId;
};

#endif

