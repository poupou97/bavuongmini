#ifndef _UI_ACTIVE_ACTIVEICON_H_
#define _UI_ACTIVE_ACTIVEICON_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

/////////////////////////////////
/**
 * 活跃度Icon
 * @author liuliang
 * @version 0.1.0
 * @date 2014.03.21
 */

class ActiveIcon:public UIScene
{
public:
	ActiveIcon(void);
	~ActiveIcon(void);

public:
	static ActiveIcon* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

public:
	UIButton * m_btn_activeIcon;						// 可点击的ICON
	UILayer * m_layer_active;								// 需要用到的layer
	UILabel * m_label_active_text;						// 	ICON下方显示的文字	
	CCScale9Sprite * m_spirte_fontBg;				// 文字显示的底图

public:
	void callBackActive(CCObject *obj);

};

#endif

