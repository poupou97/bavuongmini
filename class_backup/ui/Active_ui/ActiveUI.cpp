#include "ActiveUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "SimpleAudioEngine.h"
#include "../../GameAudio.h"
#include "../../utils/StaticDataManager.h"
#include "ActiveData.h"
#include "../../messageclient/element/CActiveLabel.h"
#include "../../messageclient/element/CActiveNote.h"
#include "../../AppMacros.h"
#include "GameView.h"
#include "../extensions/CCMoveableMenu.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../SignDaily_ui/SignDailyUI.h"
#include "../FivePersonInstance/InstanceDetailUI.h"
#include "../FivePersonInstance/FivePersonInstance.h"
#include "../../messageclient/element/MissionInfo.h"
#include "../missionscene/MissionManager.h"
#include "ActiveNoteData.h"
#include "ActiveLabelData.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "ActiveShopData.h"
#include "ActiveShopUI.h"
#include "../offlinearena_ui/OffLineArenaUI.h"
#include "ActiveMissonManager.h"
#include "../../utils/GameUtils.h"
#include "../../ui/Question_ui/QuestionUI.h"
#include "../../ui/Question_ui/QuestionData.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../ui/challengeRound/ChallengeRoundUi.h"
#include "ActiveTodayTableView.h"
#include "../../messageclient/element/CActiveLabelToday.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../ui/backpackscene/GoodsItemInfoBase.h"
#include "../family_ui/FamilyUI.h"
#include "../../messageclient/element/CTargetPosition.h"
#include "../../gamescene_state/role/MyPlayerOwnedCommand.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/element/CMapInfo.h"
#include "ActiveManager.h"
#include "../../messageclient/element/CTargetNpc.h"
#include "cocoa/CCGeometry.h"
#include "../FivePersonInstance/InstanceMapUI.h"
#include "WorldBoss_ui/WorldBossUI.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/CCTeachingGuide.h"
#include "../../gamescene_state/GameSceneEffects.h"

using namespace CocosDenshion;

UIPanel * ActiveUI::s_pPanel;

#define  TAG_TUTORIALPARTICLE 500
#define  TAG_SCROLLVIEW 50

const CCSize CCSizeCellSize = CCSizeMake(466, 55);

const CCSize CCSizeTableView = CCSizeMake(466, 275);
const CCPoint CCPointTableView = CCPointMake(71, 138);

const CCSize CCSizeContentScrollView = CCSizeMake(180, 255);
const CCPoint CCPointContentScrollView = CCPointMake(553, 179);

const CCSize CCSizeLeftTableView = CCSizeMake(198, 243);


ActiveUI::ActiveUI(void)
	:m_tableView(NULL)
	,m_contentScrollView(NULL)
	,m_label_content_name(NULL)
	,m_label_content_time(NULL)
	,m_label_content_gift(NULL)
	,m_label_content_describe(NULL)
	,m_label_activeAll(NULL)
	,m_labelTTF_activeAll(NULL)
	,m_nTableViewSize(0)
	,m_base_layer(NULL)
	,m_tab(NULL)
	,m_panel_huoyuedu(NULL)
	,m_leftTableView(NULL)
	,m_label_activeToday(NULL)
	,m_labelBMF_activeToday(NULL)
	,m_label_gift_num_1(NULL)
	,m_label_gift_num_2(NULL)
	,m_label_gift_num_3(NULL)
	,m_label_gift_num_4(NULL)
	,m_label_gift_num_5(NULL)
	,m_btn_gift_frame_1(NULL)
	,m_btn_gift_frame_2(NULL)
	,m_btn_gift_frame_3(NULL)
	,m_btn_gift_frame_4(NULL)
	,m_btn_gift_frame_5(NULL)
	,m_imageView_gift_icon_1(NULL)
	,m_imageView_gift_icon_2(NULL)
	,m_imageView_gift_icon_3(NULL)
	,m_imageView_gift_icon_4(NULL)
	,m_imageView_gift_icon_5(NULL)
	,m_labelBMF_gift_1(NULL)
	,m_labelBMF_gift_2(NULL)
	,m_labelBMF_gift_3(NULL)
	,m_labelBMF_gift_4(NULL)
	,m_labelBMF_gift_5(NULL)
	,m_imageView_progress_1(NULL)
	,m_imageView_progress_2(NULL)
	,m_imageView_progress_3(NULL)
	,m_imageView_progress_4(NULL)
	,m_imageView_progress_5(NULL)
{

}


ActiveUI::~ActiveUI(void)
{
	std::vector<ActiveNoteData *>::iterator iterActiveNote;
	for (iterActiveNote = m_vector_activeNote.begin(); iterActiveNote != m_vector_activeNote.end(); iterActiveNote++)
	{
		delete *iterActiveNote;
	}
	m_vector_activeNote.clear();

	std::vector<ActiveLabelData *>::iterator iterActiveLabel;
	for (iterActiveLabel = m_vector_activeLabel.begin(); iterActiveLabel != m_vector_activeLabel.end(); iterActiveLabel++)
	{
		delete *iterActiveLabel;
	}
	m_vector_activeLabel.clear();
}

ActiveUI* ActiveUI::create()
{
	ActiveUI * activeUI = new ActiveUI();
	if (activeUI && activeUI->init())
	{
		activeUI->autorelease();
		return activeUI;
	}
	CC_SAFE_DELETE(activeUI);
	return NULL;
}

bool ActiveUI::init()
{
	if (UIScene::init())
	{
		winSize = CCDirector::sharedDirector()->getVisibleSize();

		m_base_layer = UILayer::create();
		m_base_layer->ignoreAnchorPointForPosition(false);
		m_base_layer->setAnchorPoint(ccp(0.5f, 0.5f));
		m_base_layer->setContentSize(CCSizeMake(800, 480));
		m_base_layer->setPosition(ccp(winSize.width / 2, winSize.height / 2));
		this->addChild(m_base_layer);	

		m_active_layer = UILayer::create();
		m_active_layer->ignoreAnchorPointForPosition(false);
		m_active_layer->setAnchorPoint(ccp(0.5f, 0.5f));
		m_active_layer->setContentSize(CCSizeMake(800, 480));
		m_active_layer->setPosition(ccp(winSize.width / 2, winSize.height / 2));
		this->addChild(m_active_layer);
		m_active_layer->setVisible(true);

		m_boss_layer = UILayer::create();
		m_boss_layer->ignoreAnchorPointForPosition(false);
		m_boss_layer->setAnchorPoint(ccp(0.5f, 0.5f));
		m_boss_layer->setContentSize(CCSizeMake(800, 480));
		m_boss_layer->setPosition(ccp(winSize.width / 2, winSize.height / 2));
		this->addChild(m_boss_layer);
		m_boss_layer->setVisible(false);

		m_challenge_layer  = UILayer::create();
		m_challenge_layer->ignoreAnchorPointForPosition(false);
		m_challenge_layer->setAnchorPoint(ccp(0.5f, 0.5f));
		m_challenge_layer->setContentSize(CCSizeMake(800, 480));
		m_challenge_layer->setPosition(ccp(winSize.width / 2, winSize.height / 2));
		this->addChild(m_challenge_layer);
		this->initChallengeUi();
		m_challenge_layer->setVisible(false);
		
		m_up_layer = UILayer::create();
		m_up_layer->ignoreAnchorPointForPosition(false);
		m_up_layer->setAnchorPoint(ccp(0.5f, 0.5f));
		m_up_layer->setContentSize(CCSizeMake(800, 480));
		m_up_layer->setPosition(ccp(winSize.width / 2, winSize.height / 2));
		this->addChild(m_up_layer);	

		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winSize);
		mengban->setAnchorPoint(CCPointZero);
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);

		if(LoadSceneLayer::activeUIPanel->getWidgetParent() != NULL)
		{
			LoadSceneLayer::activeUIPanel->removeFromParentAndCleanup(false);
		}

		s_pPanel = LoadSceneLayer::activeUIPanel;
		s_pPanel->setAnchorPoint(ccp(0.5f, 0.5f));
		s_pPanel->getValidNode()->setContentSize(CCSizeMake(800, 480));
		s_pPanel->setPosition(ccp(winSize.width / 2, winSize.height / 2));
		s_pPanel->setScale(1.0f);
		s_pPanel->setTouchEnable(true);
		m_pUiLayer->addWidget(s_pPanel);

		s_panel_activeUI = (UIPanel*)UIHelper::seekWidgetByName(s_pPanel,"Panel_huoyuedu");
		s_panel_activeUI->setVisible(true);
		s_panel_worldBossUI = (UIPanel*)UIHelper::seekWidgetByName(s_pPanel,"Panel_worldBoss");
		s_panel_worldBossUI->setVisible(false);
		s_panel_singCopyUI =  (UIPanel*)UIHelper::seekWidgetByName(s_pPanel,"Panel_guoguanzhanjiang");
		s_panel_singCopyUI->setVisible(false);

		// 初始化稻穗
		const char * firstStr = StringDataManager::getString("UIName_active_huo");
		const char * secondStr = StringDataManager::getString("UIName_active_yue");
		const char * thirdStr = StringDataManager::getString("UIName_active_du");
		CCArmature * atmature = MainScene::createPendantAnm(firstStr, secondStr, thirdStr,"");
		atmature->setPosition(ccp(30,240));
		m_base_layer->addChild(atmature);

		initUI();

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(winSize);

		return true;
	}
	return false;
}

void ActiveUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
	GameUtils::playGameSound(SCENE_OPEN, 2, false);
}

void ActiveUI::onExit()
{
	UIScene::onExit();
	SimpleAudioEngine::sharedEngine()->playEffect(SCENE_CLOSE, false);
}

void ActiveUI::callBackBtnClolse( CCObject * obj )
{
	this->closeAnim();
}

void ActiveUI::initUI()
{
	// close btn
	UIButton* pBtnClose = (UIButton*)UIHelper::seekWidgetByName(s_pPanel, "Button_close");
	pBtnClose->setTouchEnable(true);
	pBtnClose->setPressedActionEnabled(true);
	pBtnClose->addReleaseEvent(this, coco_releaseselector(ActiveUI::callBackBtnClolse));

	// shop btn
	UIButton * pShopBtn = (UIButton *)UIHelper::seekWidgetByName(s_pPanel, "Button_shop");
	pShopBtn->setTouchEnable(true);
	pShopBtn->setPressedActionEnabled(true);
	pShopBtn->addReleaseEvent(this, coco_releaseselector(ActiveUI::callBackBtnAcitveShop));


	// 我的活跃度 label(不用编辑器里的 我的活跃度 几个字，要保持与 详情里的字体一样)
	UILabel * pLabel_myActive = (UILabel *)UIHelper::seekWidgetByName(s_pPanel, "Label_myActive"); 
	pLabel_myActive->setVisible(false);

	UIImageView * pImageView_frame_details = (UIImageView *)UIHelper::seekWidgetByName(s_pPanel, "ImageView_1073_0");
	
	const char * str_myActive = StringDataManager::getString("activy_myActive");
	CCLabelTTF * pLabelTTF_myActive = CCLabelTTF::create(str_myActive, APP_FONT_NAME, 15, CCSizeMake(120, 0), kCCTextAlignmentLeft);
	pLabelTTF_myActive->setColor(ccc3(255, 246, 0));

	pLabelTTF_myActive->setAnchorPoint(ccp(0, 0));
	pLabelTTF_myActive->ignoreAnchorPointForPosition(false);
	pLabelTTF_myActive->setPosition(ccp(pImageView_frame_details->getPosition().x + 36, pImageView_frame_details->getPosition().y + 15));

	this->m_active_layer->addChild(pLabelTTF_myActive);
	

	// 用户总的活跃度
	int nUserActive = ActiveData::instance()->getUserActive();
	char str_userActive[20];
	sprintf(str_userActive, "%d", nUserActive);

	m_label_activeAll = (UILabel *)UIHelper::seekWidgetByName(s_pPanel, "Label_activityValue");
	m_label_activeAll->setText(str_userActive);
	m_label_activeAll->setVisible(false);


	m_labelTTF_activeAll = CCLabelTTF::create(str_userActive, APP_FONT_NAME, 15, CCSizeMake(120, 0), kCCTextAlignmentLeft);
	m_labelTTF_activeAll->setAnchorPoint(ccp(0, 0));
	m_labelTTF_activeAll->ignoreAnchorPointForPosition(false);
	m_labelTTF_activeAll->setPosition(ccp(pImageView_frame_details->getPosition().x + 125, pImageView_frame_details->getPosition().y + 15));

	this->m_active_layer->addChild(m_labelTTF_activeAll);


	// 初始化 今日活跃度相关
	this->initTodayUI();

	// tab(此为之前的UITab，现在不需要了，暂且保留)
	//const char * normalImage = "res_ui/tab_3_off.png";
	//const char * selectImage = "res_ui/tab_3_on.png";
	//const char * finalImage = "";
	//const char * highLightImage = "res_ui/tab_3_on.png";
	//const char *str1 = StringDataManager::getString("activy_chartInfo2");
	//const char *str2 = StringDataManager::getString("WorldBossUI_uitab_boss");
	////const char *str3 = StringDataManager::getString("ActiveUi_ChallengeRoundTab");
	//char* p1 = const_cast<char*>(str1);
	//char* p2 = const_cast<char*>(str2);
	////char* p3 = const_cast<char*>(str3);
	//char* pHightLightImage = const_cast<char*>(highLightImage);
	//char * mainNames[] = {p1,p2};
	//m_tab = UITab::createWithBMFont(2, normalImage, selectImage, finalImage, mainNames,"res_ui/font/ziti_1.fnt",HORIZONTAL,5);
	//m_tab->setAnchorPoint(ccp(0,0));
	//m_tab->setPosition(ccp(74, 410));
	//m_tab->setHighLightImage(pHightLightImage);
	//m_tab->setDefaultPanelByIndex(0);
	//m_tab->addIndexChangedEvent(this, coco_indexchangedselector(ActiveUI::tabIndexChangedEvent));
	//m_tab->setPressedActionEnabled(true);
	//m_base_layer->addWidget(m_tab);
	

	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

	// 主tableView
	m_tableView = CCTableView::create(this, CCSizeTableView);
	m_tableView ->setSelectedEnable(true);   // 支持选中状态的显示
	m_tableView ->setSelectedScale9Texture("res_ui/highlight.png", CCRectMake(25, 25, 1, 1), ccp(0,0));   // 设置九宫格图片
	m_tableView->setDirection(kCCScrollViewDirectionVertical);
	m_tableView->setAnchorPoint(ccp(0, 0));
	m_tableView->setPosition(CCPointTableView);
	m_tableView->setDelegate(this);
	m_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
	m_tableView->setPressedActionEnabled(false);

	m_active_layer->addChild(m_tableView);


	//// 左侧的今日活跃度 tabelView
	//m_leftTableView = CCTableView::create(ActiveTodayTableView::instance(), CCSizeLeftTableView);
	//m_leftTableView->setSelectedEnable(false);
	//m_leftTableView->setSelectedScale9Texture("res_ui/mengban_green.png", CCRectMake(7, 7, 1, 1), ccp(0,0));   // 设置九宫格图片
	//m_leftTableView->setDirection(kCCScrollViewDirectionVertical);
	//m_leftTableView->setAnchorPoint(ccp(0, 0));
	//m_leftTableView->setPosition(CCPointMake(73, 130));
	//m_leftTableView->setDelegate(ActiveTodayTableView::instance());
	//m_leftTableView->setVerticalFillOrder(kCCTableViewFillTopDown);
	//m_leftTableView->setPressedActionEnabled(false);

	//m_active_layer->addChild(m_leftTableView);

	// 添加补丁（tableView的蒙版框）
	//UIImageView* pImageView_frame_main = (UIImageView*)UIHelper::seekWidgetByName(s_pPanel, "");
	// 

	/*UIImageView * imageViewLight = UIImageView::create();
	imageViewLight->setTexture("res_ui/Prepaid/light_round.png");
	imageViewLight->setScale9Enable(true);
	imageViewLight->setScale9Size(CCSizeMake(73,74));
	imageViewLight->setAnchorPoint(ccp(0.5f,0.5f));
	imageViewLight->setPosition(ccp(-84,-2));
	btn_buy->addChild(imageViewLight);*/

	/*UIImageView* pImageView_frame_main_copy = UIImageView::create();
	pImageView_frame_main_copy->loadTexture("res_ui/LV5_dikuang_miaobian1.png");
	pImageView_frame_main_copy->setAnchorPoint(ccp(0, 0));
	pImageView_frame_main_copy->setPosition(ccp(64, 138));
	pImageView_frame_main_copy->setScale9Enable(true);
	pImageView_frame_main_copy->setCapInsets(CCRectMake(32, 32, 1, 1));
	pImageView_frame_main_copy->setScale9Size(CCSizeMake(481, 300));
	m_active_layer->addWidget(pImageView_frame_main_copy);*/
	
}


bool ActiveUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return resignFirstResponder(touch, this, false);
}

void ActiveUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void ActiveUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void ActiveUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void ActiveUI::scrollViewDidScroll( CCScrollView* view )
{

}

void ActiveUI::scrollViewDidZoom( CCScrollView* view )
{

}

void ActiveUI::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{

	m_curActiveNote = m_vector_activeNote.at(cell->getIdx());

	this->reloatActiveInfoScrollView();
}

cocos2d::CCSize ActiveUI::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeCellSize;
}

cocos2d::extension::CCTableViewCell* ActiveUI::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCString *string = CCString::createWithFormat("%d", idx);
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell = new CCTableViewCell();
	cell->autorelease();

	if (idx < m_vector_activeLabel.size())
	{
		initCell(cell, idx);
	}

	return cell;
}

unsigned int ActiveUI::numberOfCellsInTableView( CCTableView *table )
{
	return this->getTableViewSize();
}

void ActiveUI::reloadActiveInfoData()
{
	if (m_vector_activeNote.empty())
	{
		return;
	}

	if (m_active_layer->getChildByTag(TAG_SCROLLVIEW) != NULL)
	{
		m_active_layer->getChildByTag(TAG_SCROLLVIEW)->removeFromParent();
	}

	m_contentScrollView = CCScrollView::create(CCSizeContentScrollView);
	m_contentScrollView->setViewSize(CCSizeContentScrollView);
	m_contentScrollView->ignoreAnchorPointForPosition(false);
	m_contentScrollView->setTouchEnabled(true);
	m_contentScrollView->setDirection(kCCScrollViewDirectionVertical);
	m_contentScrollView->setAnchorPoint(ccp(0,0));
	m_contentScrollView->setPosition(CCPointContentScrollView);
	m_contentScrollView->setBounceable(true);
	m_contentScrollView->setClippingToBounds(true);
	m_contentScrollView->setTag(TAG_SCROLLVIEW);

	m_active_layer->addChild(m_contentScrollView);


	int nScrollHeight = 0;
	int nSpace = 5;

	// 详情-名称
	const char * str_name = StringDataManager::getString("activy_name");
	m_label_content_name = CCLabelTTF::create(str_name, APP_FONT_NAME, 18, CCSizeMake(60, 0), kCCTextAlignmentLeft);
	m_label_content_name->setColor(ccc3(255, 246, 0));
    ccColor3B color = ccc3(0, 0, 0);
	m_label_content_name->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);

	// 详情-名称-text
	std::string str_contentName = m_curActiveNote->m_activeNote->notename();
	m_curActiveNote->m_label_content_name = CCLabelTTF::create(str_contentName.c_str(), APP_FONT_NAME, 18, CCSizeMake(396, 0), kCCTextAlignmentLeft);
	m_curActiveNote->m_label_content_name->setColor(ccc3(255, 255, 255));
	m_curActiveNote->m_label_content_name->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);

	int nZeroLineHeight = m_curActiveNote->m_label_content_name->getContentSize().height + nSpace;
	int nZeroLabelHeight = m_label_content_name->getContentSize().height + nSpace;

	// 详情-时间
	const char * str_time = StringDataManager::getString("activy_time");
	m_label_content_time = CCLabelTTF::create(str_time, APP_FONT_NAME, 18, CCSizeMake(60, 0), kCCTextAlignmentLeft);
	m_label_content_time->setColor(ccc3(255, 246, 0));
	m_label_content_time->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);


	// 详情-时间-text
	std::string str_contentTime = m_curActiveNote->m_activeNote->notetime();
	m_curActiveNote->m_label_content_time = CCLabelTTF::create(str_contentTime.c_str(), APP_FONT_NAME, 18, CCSizeMake(396, 0), kCCTextAlignmentLeft);
	m_curActiveNote->m_label_content_time->setColor(ccc3(255, 255, 255));
	m_curActiveNote->m_label_content_time->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);

	int nFirstLineHeight = nZeroLineHeight + m_curActiveNote->m_label_content_time->getContentSize().height + nSpace;
	int nFirstLabelHeight = nZeroLineHeight + m_label_content_time->getContentSize().height + nSpace;

	// 详情-奖励
	const char * str_gift = StringDataManager::getString("activy_gift");
	m_label_content_gift = CCLabelTTF::create(str_gift, APP_FONT_NAME, 18, CCSizeMake(60, 0), kCCTextAlignmentLeft);
	m_label_content_gift->setColor(ccc3(255, 246, 0));
	m_label_content_gift->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);

	// 详情-奖励-text
	std::string str_contentGift = m_curActiveNote->m_activeNote->notereward();
	m_curActiveNote->m_label_content_gift = CCLabelTTF::create(str_contentGift.c_str(), APP_FONT_NAME, 18, CCSizeMake(396, 0), kCCTextAlignmentLeft);
	m_curActiveNote->m_label_content_gift->setColor(ccc3(255, 255, 255));
	m_curActiveNote->m_label_content_gift->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);

	int nSecondLineHeight = nFirstLineHeight + m_curActiveNote->m_label_content_gift->getContentSize().height + nSpace;
	int nSecondLabelHeight = nFirstLineHeight + m_label_content_gift->getContentSize().height + nSpace;

	// 详情-限制
	const char * str_limit = StringDataManager::getString("activy_limit");
	m_label_content_limit = CCLabelTTF::create(str_limit, APP_FONT_NAME, 18, CCSizeMake(60, 0), kCCTextAlignmentLeft);
	m_label_content_limit->setColor(ccc3(255, 246, 0));
	m_label_content_limit->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);

	// 详情-限制-text
	std::string str_contentLimit = m_curActiveNote->m_activeNote->notelimit();
	m_curActiveNote->m_label_content_limit = CCLabelTTF::create(str_contentLimit.c_str(), APP_FONT_NAME, 18, CCSizeMake(396, 0), kCCTextAlignmentLeft);
	m_curActiveNote->m_label_content_limit->setColor(ccc3(255, 255, 255));
	m_curActiveNote->m_label_content_limit->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);

	int nThirdLineHeight = nSecondLineHeight + m_curActiveNote->m_label_content_limit->getContentSize().height + nSpace;
	int nThirdLabelHeight = nSecondLineHeight + m_label_content_limit->getContentSize().height + nSpace;

	// 详情-描述
	const char * str_describe = StringDataManager::getString("activy_describe");
	m_label_content_describe = CCLabelTTF::create(str_describe, APP_FONT_NAME, 18, CCSizeMake(60, 0), kCCTextAlignmentLeft);
	m_label_content_describe->setColor(ccc3(255, 246, 0));
	m_label_content_describe->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);

	// 详情-描述-text
	std::string str_contentDescribe = m_curActiveNote->m_activeNote->notecontent();
	m_curActiveNote->m_label_content_describe= CCLabelTTF::create(str_contentDescribe.c_str(), APP_FONT_NAME, 18, CCSizeMake(100, 0), kCCTextAlignmentLeft);
	m_curActiveNote->m_label_content_describe->setColor(ccc3(255, 255, 255));
	m_curActiveNote->m_label_content_describe->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);

	int nFourthLineHeight = nThirdLineHeight + m_curActiveNote->m_label_content_describe->getContentSize().height + nSpace;
	int nFourthLabelHeight = nThirdLineHeight + m_label_content_describe->getContentSize().height + nSpace;

	nScrollHeight = nFourthLineHeight + nSpace;
	if (nScrollHeight > CCSizeContentScrollView.height)
	{
		m_contentScrollView->setContentSize(ccp(CCSizeContentScrollView.width, nScrollHeight));
		m_contentScrollView->setContentOffset(ccp(0, CCSizeContentScrollView.height - nScrollHeight));
	}
	else 
	{
		m_contentScrollView->setContentSize(CCSizeContentScrollView);
	}

	// name
	m_label_content_name->setAnchorPoint(ccp(0, 1.0f));
	m_label_content_name->setPosition(ccp(15, m_contentScrollView->getContentSize().height - nZeroLabelHeight));

	m_curActiveNote->m_label_content_name->setAnchorPoint(ccp(0, 1.0f));
	m_curActiveNote->m_label_content_name->setPosition(ccp(m_label_content_name->getPosition().x + m_label_content_name->getContentSize().width - 10, m_contentScrollView->getContentSize().height - nZeroLineHeight));

	m_contentScrollView->addChild(m_label_content_name);
	m_contentScrollView->addChild(m_curActiveNote->m_label_content_name);

	// time
	m_label_content_time->setAnchorPoint(ccp(0, 1.0f));
	m_label_content_time->setPosition(ccp(15, m_contentScrollView->getContentSize().height - nFirstLabelHeight));

	m_curActiveNote->m_label_content_time->setAnchorPoint(ccp(0, 1.0f));
	m_curActiveNote->m_label_content_time->setPosition(ccp(m_label_content_time->getPosition().x + m_label_content_time->getContentSize().width - 10, m_contentScrollView->getContentSize().height - nFirstLineHeight));

	m_contentScrollView->addChild(m_label_content_time);
	m_contentScrollView->addChild(m_curActiveNote->m_label_content_time);

	// gift
	m_label_content_gift->setAnchorPoint(ccp(0, 1.0f));
	m_label_content_gift->setPosition(ccp(15, m_contentScrollView->getContentSize().height - nSecondLabelHeight));

	m_curActiveNote->m_label_content_gift->setAnchorPoint(ccp(0, 1.0f));
	m_curActiveNote->m_label_content_gift->setPosition(ccp(m_label_content_gift->getPosition().x + m_label_content_gift->getContentSize().width - 10, m_contentScrollView->getContentSize().height - nSecondLineHeight));

	m_contentScrollView->addChild(m_label_content_gift);
	m_contentScrollView->addChild(m_curActiveNote->m_label_content_gift);

	// limit
	m_label_content_limit->setAnchorPoint(ccp(0, 1.0f));
	m_label_content_limit->setPosition(ccp(15, m_contentScrollView->getContentSize().height - nThirdLabelHeight));

	m_curActiveNote->m_label_content_limit->setAnchorPoint(ccp(0, 1.0f));
	m_curActiveNote->m_label_content_limit->setPosition(ccp(m_label_content_limit->getPosition().x + m_label_content_limit->getContentSize().width - 10, m_contentScrollView->getContentSize().height - nThirdLineHeight));

	m_contentScrollView->addChild(m_label_content_limit);
	m_contentScrollView->addChild(m_curActiveNote->m_label_content_limit);

	// describe
	m_label_content_describe->setAnchorPoint(ccp(0, 1.0f));
	m_label_content_describe->setPosition(ccp(15, m_contentScrollView->getContentSize().height - nFourthLabelHeight));

	m_curActiveNote->m_label_content_describe->setAnchorPoint(ccp(0, 1.0f));
	m_curActiveNote->m_label_content_describe->setPosition(ccp(m_label_content_describe->getPosition().x + m_label_content_describe->getContentSize().width - 10, m_contentScrollView->getContentSize().height - nFourthLineHeight));

	m_contentScrollView->addChild(m_label_content_describe);
	m_contentScrollView->addChild(m_curActiveNote->m_label_content_describe);

}

void ActiveUI::initCell( CCTableViewCell * cell , unsigned int nIndex)
{
	// 底图（normal）
	CCScale9Sprite * sprite_frame_normal = CCScale9Sprite::create("res_ui/kuang0_new.png");
	sprite_frame_normal->setContentSize(CCSizeMake(CCSizeCellSize.width - 6, CCSizeCellSize.height - 2));
	sprite_frame_normal->setCapInsets(CCRectMake(18, 9, 2, 23));
	sprite_frame_normal->setAnchorPoint(CCPointZero);
	sprite_frame_normal->setPosition(ccp(3, 1));
	//cell->addChild(sprite_frame_normal);

	// 底图（特殊）
	CCScale9Sprite * sprite_frame_special = CCScale9Sprite::create("res_ui/kuang000_new.png");
	sprite_frame_special->setContentSize(CCSizeMake(CCSizeCellSize.width - 6, CCSizeCellSize.height - 2));
	sprite_frame_special->setCapInsets(CCRectMake(18, 9, 2, 23));
	sprite_frame_special->setAnchorPoint(CCPointZero);
	sprite_frame_special->setPosition(ccp(3, 1));

	// 底图颜色 正常0，活动1
	int nColorBg =  m_vector_activeLabel.at(nIndex)->m_activeLabel->color();
	if (0 == nColorBg)
	{
		cell->addChild(sprite_frame_normal);
	}
	else if (1 == nColorBg)
	{
		cell->addChild(sprite_frame_special);
	}


	// 活动内容名称
	std::string str_name = m_vector_activeLabel.at(nIndex)->m_activeLabel->name();
	CCLabelTTF * m_label_name = CCLabelTTF::create(str_name.c_str(), APP_FONT_NAME, 18, CCSizeMake(200, 0), kCCTextAlignmentLeft);
	m_label_name->setColor(ccc3(255, 255, 255));
    ccColor3B color = ccc3(0, 0, 0);
	m_label_name->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
	m_label_name->setAnchorPoint(ccp(0.5f, 0.5f));
	m_label_name->setHorizontalAlignment(kCCTextAlignmentCenter);
	m_label_name->setPosition(ccp(65, sprite_frame_normal->getContentSize().height / 2));
	cell->addChild(m_label_name);

	// 参与次数（数据）
	int m_nJoinCountAlready = m_vector_activeLabel.at(nIndex)->m_activeLabel->partakenumberalready();
	int m_nJoinCountAll = m_vector_activeLabel.at(nIndex)->m_activeLabel->partakenumberall();

	char str_joinCountAlready[10];
	sprintf(str_joinCountAlready, "%d", m_nJoinCountAlready);

	std::string str_joinCount = "";
	str_joinCount.append(str_joinCountAlready);
	str_joinCount.append("/");
	str_joinCount.append(getActiveJoinAll(m_nJoinCountAll).c_str());

	CCLabelTTF * m_label_joinCountShow = CCLabelTTF::create(str_joinCount.c_str(), APP_FONT_NAME, 18, CCSizeMake(200, 0), kCCTextAlignmentLeft);
	m_label_joinCountShow->setColor(ccc3(255, 255, 255));
	m_label_joinCountShow->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
	m_label_joinCountShow->setAnchorPoint(ccp(0.5f, 0.5f));
	m_label_joinCountShow->setHorizontalAlignment(kCCTextAlignmentCenter);
	m_label_joinCountShow->setPosition(ccp(175, sprite_frame_normal->getContentSize().height / 2));
	cell->addChild(m_label_joinCountShow);

	// 活跃度(数据)
	int m_nActiveAlready = m_vector_activeLabel.at(nIndex)->m_activeLabel->partakeactivealready();
	int m_nActiveAll = m_vector_activeLabel.at(nIndex)->m_activeLabel->partakeactiveall();

	char str_activeAlready[10];
	sprintf(str_activeAlready, "%d", m_nActiveAlready);

	std::string str_active = "";
	str_active.append(str_activeAlready);
	str_active.append("/");
	str_active.append(getActiveCanGetAll(m_nActiveAll).c_str());
	
	CCLabelTTF * m_label_active = CCLabelTTF::create(str_active.c_str(), APP_FONT_NAME, 18, CCSizeMake(150, 0), kCCTextAlignmentLeft);
	m_label_active->setColor(ccc3(255, 255, 255));
	m_label_active->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
	m_label_active->setAnchorPoint(ccp(0.5f, 0.5f));
	m_label_active->setHorizontalAlignment(kCCTextAlignmentCenter);
	m_label_active->setPosition(ccp(260, sprite_frame_normal->getContentSize().height / 2));
	cell->addChild(m_label_active);

	// 能否参加活动(不需要显示)
	int m_nIsCanJoinActivity = m_vector_activeLabel.at(nIndex)->m_activeLabel->canpartake();

	char str_status[10];
	sprintf(str_status, "%d", m_nIsCanJoinActivity);

	CCLabelTTF * m_label_status = CCLabelTTF::create(str_status, APP_FONT_NAME, 18, CCSizeMake(200, 0), kCCTextAlignmentLeft);
	m_label_status->setColor(ccc3(255, 255, 255));
	m_label_status->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
	m_label_status->setAnchorPoint(ccp(0, 0.5f));
	m_label_status->setPosition(ccp(300, sprite_frame_normal->getContentSize().height / 2));
	//cell->addChild(m_label_status);

	// 参加活动的时间
	/*std::string str_joinActivityTime = m_vector_activeLabel.at(nIndex)->m_activeLabel->notetime();*/
	std::string str_disableReason = m_vector_activeLabel.at(nIndex)->m_activeLabel->notcanpartakeshow();
	CCLabelTTF* m_label_joinActivityTime = CCLabelTTF::create(str_disableReason.c_str(), APP_FONT_NAME, 18, CCSizeMake(200, 0), kCCTextAlignmentLeft);
	m_label_joinActivityTime->setColor(ccc3(255, 255, 255));
	m_label_joinActivityTime->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
	m_label_joinActivityTime->setAnchorPoint(ccp(0.5f, 0.5f));
	m_label_joinActivityTime->setHorizontalAlignment(kCCTextAlignmentCenter);
	m_label_joinActivityTime->setPosition(ccp(375, sprite_frame_normal->getContentSize().height / 2));
	//cell->addChild(m_label_joinActivityTime);

	// MenuBtn
	int m_nId = m_vector_activeLabel.at(nIndex)->m_activeLabel->id();
	CCScale9Sprite * sprite_btn = CCScale9Sprite::create("res_ui/new_button_2.png");
	sprite_btn->setCapInsets(CCRectMake(18, 9, 2, 23));
	sprite_btn->setContentSize(CCSizeMake(103, 38));

	CCMenuItemSprite *item = CCMenuItemSprite::create(sprite_btn, sprite_btn, sprite_btn, this, menu_selector(ActiveUI::callBackBtnJoin));
	item->setZoomScale(1.2f);
	item->setTag(m_nId);

	CCMoveableMenu *selectMenu = CCMoveableMenu::create(item, NULL);
	selectMenu->setAnchorPoint(ccp(0,0));
	selectMenu->setPosition(ccp(380, sprite_frame_normal->getContentSize().height / 2));
	//cell->addChild(selectMenu);

	// 参与的text
	const char* str1 = StringDataManager::getString("activy_canyu");
	char* str_btnText = const_cast<char*>(str1);
	CCLabelTTF * m_label_btnText = CCLabelTTF::create(str_btnText, APP_FONT_NAME, 18, CCSizeMake(200, 0), kCCTextAlignmentLeft);
	m_label_btnText->setColor(ccc3(255, 255, 255));
	m_label_btnText->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
	m_label_btnText->setAnchorPoint(ccp(0, 0));
	/*m_label_btnText->setPosition(ccp(365, sprite_frame->getContentSize().height / 2));*/
	m_label_btnText->setPosition(ccp((m_label_btnText->getContentSize().width - sprite_btn->getContentSize().width) / 2 - 14, (sprite_btn->getContentSize().height - m_label_btnText->getContentSize().height) / 2));

	// 查看的text
	const char* strLook = StringDataManager::getString("activy_chakan");
	char * str_btnLook = const_cast<char*>(strLook);
	CCLabelTTF * m_label_btnTextLook = CCLabelTTF::create(str_btnLook, APP_FONT_NAME, 18, CCSizeMake(200, 0), kCCTextAlignmentLeft);
	m_label_btnTextLook->setColor(ccc3(255, 255, 255));
	m_label_btnTextLook->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
	m_label_btnTextLook->setAnchorPoint(ccp(0, 0));
	/*m_label_btnText->setPosition(ccp(365, sprite_frame->getContentSize().height / 2));*/
	m_label_btnTextLook->setPosition(ccp((m_label_btnTextLook->getContentSize().width - sprite_btn->getContentSize().width) / 2 - 14, (sprite_btn->getContentSize().height - m_label_btnTextLook->getContentSize().height) / 2));

	

	//能否参加活动 0不能、1能
	if (1 == m_nIsCanJoinActivity)
	{
		// 如果 可参与 和 已经参与 相等，则 参与 按钮不显示；否则判断，如果用户等级大于等于 预告等级，则 显示即将显示；再如果 还大于等于等级限制，则 显示 按钮
		if (m_nActiveAll == m_nActiveAlready)
		{
			//活跃度类型：1.任务、2.签到、3.副本、4.参与竞技场、5.答题、6.宝箱、7.家族战、8.过关斩将、9.世界BOSS
			//悬赏任务(activeType:1,activeTypeId:12)
			int nActiveType = m_vector_activeLabel.at(nIndex)->m_activeLabel->activetype();
			int nActiveTypeId = m_vector_activeLabel.at(nIndex)->m_activeLabel->activetypeid();

			if (2 == nActiveType || 3 == nActiveType || 4 == nActiveType || 7 == nActiveType || 8 == nActiveType || 9 == nActiveType || (1 == nActiveType && 12 == nActiveTypeId))
			{
				// 显示查看按钮
				item->addChild(m_label_btnTextLook);
				cell->addChild(selectMenu);
			}
			else
			{
				// 显示已达成
				const char * str_show = StringDataManager::getString("activy_yidacheng");
				m_label_joinActivityTime->setString(str_show);
				cell->addChild(m_label_joinActivityTime);
			}
		}
		else
		{
			// 当前用户等级
			int nPlayLevel = GameView::getInstance()->myplayer->getActiveRole()->level();

			// 大于等级预告等级
			if (nPlayLevel >= m_vector_activeLabel.at(nIndex)->m_activeLabel->levelforeshow())
			{
				if (nPlayLevel >= m_vector_activeLabel.at(nIndex)->m_activeLabel->levellimit())
				{
					// 添加按钮
					item->addChild(m_label_btnText);
					cell->addChild(selectMenu);
				}
				else
				{
					//// 显示 即将开放
					//const char * str_comeSoon = StringDataManager::getString("activy_comesoon");
					//m_label_joinActivityTime->setString(str_comeSoon);
					//cell->addChild(m_label_joinActivityTime);
					
					const char * str_level = StringDataManager::getString("activy_open");

					// XX级开放
					int nLevelLimit = m_vector_activeLabel.at(nIndex)->m_activeLabel->levellimit();

					char charLevelLimit[20];
					sprintf(charLevelLimit, "%d", nLevelLimit);

					std::string str_openLevel = "";
					str_openLevel.append(charLevelLimit);
					str_openLevel.append(str_level);
					m_label_joinActivityTime->setString(str_openLevel.c_str());
					cell->addChild(m_label_joinActivityTime);
				}
			}
		}
	}
	else if(0 == m_nIsCanJoinActivity)
	{
		// 不能参与
		cell->addChild(m_label_joinActivityTime);
	}

}

void ActiveUI::callBackBtnJoin( CCObject * obj )
{
	CCMenuItemSprite* pMenuItem = (CCMenuItemSprite *)obj;
	int nId = pMenuItem->getTag();
	for (unsigned int i = 0; i < m_vector_activeLabel.size(); i++)
	{
		if (nId == m_vector_activeLabel.at(i)->m_activeLabel->id())
		{
			// 找到 点击的哪个活动的按钮
			// 活跃度类型：1.任务、2.签到、3.副本、4.参与竞技场、5.答题、6.开宝箱、7.家族战、8.过关斩将、9.世界BOSS
			int nActiveType = m_vector_activeLabel.at(i)->m_activeLabel->activetype();
			switch (nActiveType)
			{
			case 1:
				{
					int nActiveTypeId = m_vector_activeLabel.at(i)->m_activeLabel->activetypeid();

					// 12 代表 悬赏任务
					if (12 == nActiveTypeId)
					{
						activeToRewardMisson();
					}
					else
					{
						activeToMisson(nActiveTypeId);
					}

					this->closeAnim();

					//GameView::getInstance()->showAlertDialog("renwu");
				}
				break;
			case 2:
				{
					activeToSign();
					this->closeAnim();
					//GameView::getInstance()->showAlertDialog("qiandao");
				}
				break;
			case 3:
				{
					activeToEctype();
					this->closeAnim();
					//GameView::getInstance()->showAlertDialog("fuben");
				}
				break;
			case 4:
				{
					activeToPK();
					this->closeAnim();
					//GameView::getInstance()->showAlertDialog("jingjichang");
				}
				break;
			case 5:
				{
					activeToAnswerQuestion();
					this->closeAnim();
					//GameView::getInstance()->showAlertDialog("dati");
				}
				break;
			case 6:
				{
					activeToOpenPresentBox();
					//this->closeAnim();
					//GameView::getInstance()->showAlertDialog("kaibaoxiang");
				}
				break;
			case 7:
				{
					activeToFamilyFight();
					this->closeAnim();
					//GameView::getInstance()->showAlertDialog("kaibaoxiang");
				}
				break;
			case 8:
				{
					activeToSingleFight();
					this->closeAnim();
					//GameView::getInstance()->showAlertDialog("kaibaoxiang");
				}
				break;
			case 9:
				{
					// 过关斩将 和 世界BOSS是一个逻辑
					activeToWorldBoss();
					this->closeAnim();
					//GameView::getInstance()->showAlertDialog("kaibaoxiang");
				}
				break;

			}
		}
	}
}

void ActiveUI::openActiveWindow(UIScene * activeScene, int tag, int nActiveType)
{
	MainScene* mainScene = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
	if(mainScene->getChildByTag(tag) != NULL)
	{
		const char * str_tishi1 = StringDataManager::getString("activy_tishi1");
		const char * str_tishi2 = StringDataManager::getString("activy_tishi2");

		switch (nActiveType)
		{
			case 1:
			{
				const char *str_fuben = StringDataManager::getString("activy_fuben");

				std::string str_info = "";
				str_info.append(str_tishi1);
				str_info.append(str_fuben);
				str_info.append(str_tishi2);

				GameView::getInstance()->showAlertDialog(str_info);
			}
			break;
		}

		return;
	}

	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	activeScene->ignoreAnchorPointForPosition(false);
	activeScene->setAnchorPoint(ccp(0.5f, 0.5f));
	activeScene->setPosition(ccp(winSize.width / 2, winSize.height / 2));
	activeScene->setTag(tag);

	mainScene->addChild(activeScene);

}

void ActiveUI::activeToSign()
{
	MainScene* mainScene = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
	if(mainScene->getChildByTag(kTagSignDailyUI) != NULL)
	{
		mainScene->getChildByTag(kTagSignDailyUI)->removeFromParent();
	}

	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	SignDailyUI * pSignDailyUI = SignDailyUI::create();
	pSignDailyUI->ignoreAnchorPointForPosition(false);
	pSignDailyUI->setAnchorPoint(ccp(0.5f, 0.5f));
	pSignDailyUI->setPosition(ccp(winSize.width / 2, winSize.height / 2));
	pSignDailyUI->setTag(kTagSignDailyUI);

	mainScene->addChild(pSignDailyUI);
	pSignDailyUI->refreshUI();
}

void ActiveUI::activeToEctype()
{
	// the player has already been in the instance map(角色已经在 副本 中)
	if(FivePersonInstance::getStatus() == FivePersonInstance::status_in_instance)
	{
		const char * str_tishi1 = StringDataManager::getString("activy_tishi1");
		const char * str_tishi2 = StringDataManager::getString("activy_tishi2");
		const char *str_fuben = StringDataManager::getString("activy_fuben");

		std::string str_info = "";
		str_info.append(str_tishi1);
		str_info.append(str_fuben);
		str_info.append(str_tishi2);

		GameView::getInstance()->showAlertDialog(str_info);
	}
	else
	{
// 		MainScene* mainUILayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
// 		if(mainUILayer->getChildByTag(kTagInstanceMainUI) != NULL)
// 		{
// 			mainUILayer->getChildByTag(kTagInstanceMainUI)->removeFromParent();
// 		}
// 
// 		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
// 
// 		InstanceDetailUI * pDetailUI = InstanceDetailUI::create();
// 		pDetailUI->ignoreAnchorPointForPosition(false);
// 		pDetailUI->setAnchorPoint(ccp(0.5f, 0.5f));
// 		pDetailUI->setPosition(ccp(winSize.width / 2, winSize.height / 2));
// 		pDetailUI->setTag(kTagSignDailyUI);
// 
// 		mainUILayer->addChild(pDetailUI);

		MainScene* mainUILayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
		if(mainUILayer->getChildByTag(kTagInstanceMapUI) != NULL)
		{
		 	mainUILayer->getChildByTag(kTagInstanceMapUI)->removeFromParent();
		}
		 
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		 
		InstanceMapUI * iMapUI = InstanceMapUI::create();
		iMapUI->ignoreAnchorPointForPosition(false);
		iMapUI->setAnchorPoint(ccp(0.5f, 0.5f));
		iMapUI->setPosition(ccp(winSize.width / 2, winSize.height / 2));
		iMapUI->setTag(kTagInstanceMapUI);
		 
		mainUILayer->addChild(iMapUI);
	}
}

void ActiveUI::activeToMisson(int nActiveTypeId)
{
	int nMissionCount = 0;
	int nIndex = -1;

	unsigned int nMissonSize = MissionManager::getInstance()->MissionList.size();
	std::vector<MissionInfo *> missonList = MissionManager::getInstance()->MissionList;
	for (unsigned int i = 0; i < nMissonSize; i++)
	{
		char str_tmpTypeId[4];
		sprintf(str_tmpTypeId, "%d", nActiveTypeId);
		std::string str_activeTypeId = "";
		str_activeTypeId.append(str_tmpTypeId);

		if (nActiveTypeId == missonList.at(i)->missionpackagetype())
		{
			nMissionCount++;
			nIndex = i;

			// 如果 大于1，说明 此任务有多个可选项；否则，按照 杨俊 的addMission接口（跑任务系统）
			// 暂且注释掉此处，当 张晶 服务器写好再打开
			/*if (nMissionCount > 1)
			{
			ActiveMissonManager::instance()->addAcitveMisson(missonList.at(i));

			return;
			}*/
		}
	}

	if (-1 != nIndex)
	{
		MissionManager::getInstance()->curMissionInfo->CopyFrom(*missonList.at(nIndex));
		MissionManager::getInstance()->addMission(missonList.at(nIndex));

		return;
	}
}

void ActiveUI::initLabelFromInternet()
{
	// 清空数据
	std::vector<ActiveLabelData *>::iterator iterActiveLabel;
	for (iterActiveLabel = m_vector_activeLabel.begin(); iterActiveLabel != m_vector_activeLabel.end(); iterActiveLabel++)
	{
		delete *iterActiveLabel;
	}
	m_vector_activeLabel.clear();

	// 使m_nTableViewSize大小为0
	this->setTableViewSize(0);

	// 当前用户等级
	int nPlayLevel = GameView::getInstance()->myplayer->getActiveRole()->level();

	ActiveRole * activeRole = GameView::getInstance()->myplayer->getActiveRole();
	long long nFactionId = activeRole->playerbaseinfo().factionid();

	unsigned int nActiveLabelSize = ActiveData::instance()->m_vector_native_activeLabel.size();
	for (unsigned int i = 0; i < nActiveLabelSize; i++)
	{
		// 判断如果该用户没有家族，则不把 家族相关任务加到vector中
		if (!IsCanPushVector(i))
		{
			continue;
		}


		// 只有 用户等级 大于等于 预告等级时，活动内容才装入vector
		if (nPlayLevel >= ActiveData::instance()->m_vector_native_activeLabel.at(i)->m_activeLabel->levelforeshow())
		{
			this->setTableViewSize(this->getTableViewSize() + 1);

			ActiveLabelData * data_ = new ActiveLabelData();
			data_->m_activeLabel = new CActiveLabel();
			data_->m_activeLabel->CopyFrom(*(ActiveData::instance()->m_vector_native_activeLabel.at(i)->m_activeLabel));
			m_vector_activeLabel.push_back(data_);

		}
	}
}

void ActiveUI::initNoteFromInternet()
{
	// 清空数据
	std::vector<ActiveNoteData *>::iterator iterActiveNote;
	for (iterActiveNote = m_vector_activeNote.begin(); iterActiveNote != m_vector_activeNote.end(); iterActiveNote++)
	{

		delete *iterActiveNote;
	}
	m_vector_activeNote.clear();

	// 当前用户等级
	int nPlayLevel = GameView::getInstance()->myplayer->getActiveRole()->level();

	// 注：必须确保 labelSize 和 noteSize大小一致
	unsigned int nActiveLabelSize = ActiveData::instance()->m_vector_native_activeLabel.size();
	unsigned int nActiveNoteSize = ActiveData::instance()->m_vector_native_activeNote.size();

	if (nActiveNoteSize == nActiveLabelSize)
	{
		for (unsigned int i = 0; i < nActiveNoteSize; i++)
		{
			// 判断如果该用户没有家族，则不把 家族相关任务加到vector中
			if (!IsCanPushVector(i))
			{
				continue;
			}

			// 只有 用户等级 大于等于 预告等级时，活动内容才装入vector
			if (nPlayLevel >= ActiveData::instance()->m_vector_native_activeLabel.at(i)->m_activeLabel->levelforeshow())
			{
				ActiveNoteData * data_ = new ActiveNoteData();
				data_->m_activeNote = new CActiveNote();
				data_->m_activeNote->CopyFrom(*ActiveData::instance()->m_vector_native_activeNote.at(i)->m_activeNote);
				m_vector_activeNote.push_back(data_);
			}
		}
	}
}

void ActiveUI::refrehTableView()
{
	m_tableView->reloadData();
}

void ActiveUI::initUserActiveFromInternet()
{
	// 用户总的活跃度
	int nUserActive = ActiveData::instance()->getUserActive();
	char str_userActive[10];
	sprintf(str_userActive, "%d", nUserActive);

	m_label_activeAll = (UILabel *)UIHelper::seekWidgetByName(s_pPanel, "Label_activityValue");
	m_label_activeAll->setText(str_userActive);
	m_label_activeAll->setVisible(false);

	if (NULL == m_labelTTF_activeAll)
	{
		UIImageView * pImageView_frame_details = (UIImageView *)UIHelper::seekWidgetByName(s_pPanel, "ImageView_details");

		m_labelTTF_activeAll = CCLabelTTF::create(str_userActive, APP_FONT_NAME, 15, CCSizeMake(120, 0), kCCTextAlignmentLeft);
		m_labelTTF_activeAll->setAnchorPoint(ccp(0, 0));
		m_labelTTF_activeAll->ignoreAnchorPointForPosition(false);
		m_labelTTF_activeAll->setPosition(ccp(pImageView_frame_details->getPosition().x + 125, pImageView_frame_details->getPosition().y + 15));

		this->m_active_layer->addChild(m_labelTTF_activeAll);
	}
	else
	{
		m_labelTTF_activeAll->setString(str_userActive);
	}
}

std::string ActiveUI::getActiveJoinAll( int nJoinCountAll )
{
	std::string str_count = "";

	// 如果nJoinCountAll小于0，说明为参与次数无限制；否则为具体的限制次数
	if (nJoinCountAll < 0)
	{
		const char * str_tmp = StringDataManager::getString("activy_none");
		str_count = str_tmp;
	}
	else
	{
		char str_joinCountAll[10];
		sprintf(str_joinCountAll, "%d", nJoinCountAll);
		str_count = str_joinCountAll;
	}

	return str_count;
}

std::string ActiveUI::getActiveCanGetAll( int nActiveCanGetAll )
{
	std::string str_active = "";

	// 如果nActiveCanGetAll小于0，说明可获得的活跃度无限制；否则为具体的限制次数
	if (nActiveCanGetAll < 0)
	{
		const char * str_tmp = StringDataManager::getString("activy_none");
		str_active = str_tmp;
	}
	else
	{
		char str_activeCanGet[10];
		sprintf(str_activeCanGet, "%d", nActiveCanGetAll);
		str_active = str_activeCanGet;
	}

	return str_active;
}

void ActiveUI::setTableViewSize( int nTableViewSize )
{
	this->m_nTableViewSize = nTableViewSize;
}

int ActiveUI::getTableViewSize()
{
	return m_nTableViewSize;
}

void ActiveUI::callBackBtnAcitveShop( CCObject * obj )
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveShop) == NULL)
	{
		CCSize winSize=CCDirector::sharedDirector()->getVisibleSize();
		ActiveShopUI * activeShopUI = ActiveShopUI::create(ActiveShopData::instance()->m_vector_activeShopSource);
		activeShopUI->ignoreAnchorPointForPosition(false);
		activeShopUI->setAnchorPoint(ccp(0.5f,0.5f));
		activeShopUI->setPosition(ccp(winSize.width/2,winSize.height/2));
		activeShopUI->setTag(kTagActiveShop);
		GameView::getInstance()->getMainUIScene()->addChild(activeShopUI);
	}
}

void ActiveUI::activeToPK()
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOffLineArenaUI) == NULL)
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		OffLineArenaUI * offLineArenaUI = OffLineArenaUI::create();
		offLineArenaUI->ignoreAnchorPointForPosition(false);
		offLineArenaUI->setAnchorPoint(ccp(0.5f, 0.5f));
		offLineArenaUI->setPosition(ccp(winSize.width / 2, winSize.height / 2));
		offLineArenaUI->setTag(kTagOffLineArenaUI);
		GameView::getInstance()->getMainUIScene()->addChild(offLineArenaUI);
	}
}

void ActiveUI::activeToAnswerQuestion()
{
	if (this->mTutorialIndex == 1)
	{
		Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if(sc != NULL)
			sc->endCommand(this);
	}

	if (QuestionData::instance()->get_hasAnswerQuestion() >= QuestionData::instance()->get_allQuestion())
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("QuestionUI_hasNoNumber"));
	}
	else
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		QuestionUI * pTmpQuestionUI = (QuestionUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagQuestionUI);
		if (NULL == pTmpQuestionUI)
		{
			QuestionUI * pQuestionUI = QuestionUI::create();
			pQuestionUI->ignoreAnchorPointForPosition(false);
			pQuestionUI->setAnchorPoint(ccp(0.5f, 0.5f));
			pQuestionUI->setPosition(ccp(winSize.width / 2, winSize.height / 2));
			pQuestionUI->setTag(kTagQuestionUI);
			pQuestionUI->requestQuestion();				// 向服务器请求题目

			GameView::getInstance()->getMainUIScene()->addChild(pQuestionUI);
		}
	}
}

void ActiveUI::tabIndexChangedEvent( CCObject *pSender )
{
	switch(((UITab*)pSender)->getCurrentIndex())
	{
	case 0:
		{
			m_active_layer->setVisible(true);
			m_boss_layer->setVisible(false);
			m_challenge_layer->setVisible(false);

			s_panel_activeUI->setVisible(true);
			s_panel_worldBossUI->setVisible(false);
			s_panel_singCopyUI->setVisible(false);
		}
		break;
	case 1:
		{
			m_active_layer->setVisible(false);
			m_boss_layer->setVisible(true);
			m_challenge_layer->setVisible(false);

			s_panel_activeUI->setVisible(false);
			s_panel_worldBossUI->setVisible(true);
			s_panel_singCopyUI->setVisible(false);
		}
		break;
	case 2:
		{
// 			m_active_layer->setVisible(false);
// 			m_boss_layer->setVisible(false);
// 			m_challenge_layer->setVisible(true);
// 			
// 			s_panel_singCopyUI->setVisible(true);
// 			s_panel_activeUI->setVisible(false);
// 			s_panel_worldBossUI->setVisible(false);
			
		}break;
	}
}

void ActiveUI::reloatActiveInfoScrollView()
{
	if (m_vector_activeNote.empty())
	{
		return;
	}

	if (m_active_layer->getChildByTag(TAG_SCROLLVIEW) != NULL)
	{
		m_active_layer->getChildByTag(TAG_SCROLLVIEW)->removeFromParent();
	}

	m_contentScrollView = CCScrollView::create(CCSizeContentScrollView);
	m_contentScrollView->setViewSize(CCSizeContentScrollView);
	m_contentScrollView->ignoreAnchorPointForPosition(false);
	m_contentScrollView->setTouchEnabled(true);
	m_contentScrollView->setDirection(kCCScrollViewDirectionVertical);
	m_contentScrollView->setAnchorPoint(ccp(0,0));
	m_contentScrollView->setPosition(CCPointContentScrollView);
	m_contentScrollView->setBounceable(true);
	m_contentScrollView->setClippingToBounds(true);
	m_contentScrollView->setTag(TAG_SCROLLVIEW);

	m_active_layer->addChild(m_contentScrollView);

	int nScrollHeight = 0;
	int nSpace = 2;

	ccColor3B color = ccc3(0, 0, 0);

	// 详情-奖励
	const char * str_gift = StringDataManager::getString("activy_gift");
	m_label_content_gift = CCLabelTTF::create(str_gift, APP_FONT_NAME, 15, CCSizeMake(60, 0), kCCTextAlignmentLeft);
	m_label_content_gift->setColor(ccc3(255, 246, 0));
	m_label_content_gift->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);

	// 详情-奖励-text
	std::string str_contentGift = m_curActiveNote->m_activeNote->notereward();
	m_curActiveNote->m_label_content_gift = CCLabelTTF::create(str_contentGift.c_str(), APP_FONT_NAME, 15, CCSizeMake(120, 0), kCCTextAlignmentLeft);
	m_curActiveNote->m_label_content_gift->setColor(ccc3(255, 255, 255));
	m_curActiveNote->m_label_content_gift->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);

	int nZeroLineHeight = m_curActiveNote->m_label_content_gift->getContentSize().height + nSpace;
	int nZeroLabelHeight = m_label_content_gift->getContentSize().height + nSpace;

	// 详情-限制
	const char * str_limit = StringDataManager::getString("activy_limit");
	m_label_content_limit = CCLabelTTF::create(str_limit, APP_FONT_NAME, 15, CCSizeMake(60, 0), kCCTextAlignmentLeft);
	m_label_content_limit->setColor(ccc3(255, 246, 0));
	m_label_content_limit->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);

	// 详情-限制-text
	std::string str_contentLimit = m_curActiveNote->m_activeNote->notelimit();
	m_curActiveNote->m_label_content_limit = CCLabelTTF::create(str_contentLimit.c_str(), APP_FONT_NAME, 15, CCSizeMake(120, 0), kCCTextAlignmentLeft);
	m_curActiveNote->m_label_content_limit->setColor(ccc3(255, 255, 255));
	m_curActiveNote->m_label_content_limit->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);

	int nFirstLineHeight = nZeroLineHeight + m_curActiveNote->m_label_content_limit->getContentSize().height + nSpace;
	int nFirstLabelHeight = nZeroLineHeight + m_label_content_limit->getContentSize().height + nSpace;

	// 详情-描述
	const char * str_describe = StringDataManager::getString("activy_describe");
	m_label_content_describe = CCLabelTTF::create(str_describe, APP_FONT_NAME, 15, CCSizeMake(60, 0), kCCTextAlignmentLeft);
	m_label_content_describe->setColor(ccc3(255, 246, 0));
	m_label_content_describe->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);

	// 详情-描述-text
	std::string str_contentDescribe = m_curActiveNote->m_activeNote->notecontent();
	m_curActiveNote->m_label_content_describe= CCLabelTTF::create(str_contentDescribe.c_str(), APP_FONT_NAME, 15, CCSizeMake(120, 0), kCCTextAlignmentLeft);
	m_curActiveNote->m_label_content_describe->setColor(ccc3(255, 255, 255));
	m_curActiveNote->m_label_content_describe->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);

	int nSecondLineHeight = nFirstLineHeight + m_curActiveNote->m_label_content_describe->getContentSize().height + nSpace;
	int nSecondLabelHeight = nFirstLineHeight + m_label_content_describe->getContentSize().height + nSpace;

	nScrollHeight = nSecondLineHeight + nSpace;
	if (nScrollHeight > CCSizeContentScrollView.height)
	{
		m_contentScrollView->setContentSize(ccp(CCSizeContentScrollView.width, nScrollHeight));
		m_contentScrollView->setContentOffset(ccp(0, CCSizeContentScrollView.height - nScrollHeight));
	}
	else 
	{
		m_contentScrollView->setContentSize(CCSizeContentScrollView);
	}


	// gift
	m_label_content_gift->setAnchorPoint(ccp(0, 1.0f));
	m_label_content_gift->setPosition(ccp(12, m_contentScrollView->getContentSize().height - nZeroLabelHeight));

	m_curActiveNote->m_label_content_gift->setAnchorPoint(ccp(0, 1.0f));
	m_curActiveNote->m_label_content_gift->setPosition(ccp(m_label_content_gift->getPosition().x + m_label_content_gift->getContentSize().width - 15, m_contentScrollView->getContentSize().height - nZeroLineHeight));

	m_contentScrollView->addChild(m_label_content_gift);
	m_contentScrollView->addChild(m_curActiveNote->m_label_content_gift);

	// limit
	m_label_content_limit->setAnchorPoint(ccp(0, 1.0f));
	m_label_content_limit->setPosition(ccp(12, m_contentScrollView->getContentSize().height - nFirstLabelHeight));

	m_curActiveNote->m_label_content_limit->setAnchorPoint(ccp(0, 1.0f));
	m_curActiveNote->m_label_content_limit->setPosition(ccp(m_label_content_limit->getPosition().x + m_label_content_limit->getContentSize().width - 15, m_contentScrollView->getContentSize().height - nFirstLineHeight));

	m_contentScrollView->addChild(m_label_content_limit);
	m_contentScrollView->addChild(m_curActiveNote->m_label_content_limit);

	// describe
	m_label_content_describe->setAnchorPoint(ccp(0, 1.0f));
	m_label_content_describe->setPosition(ccp(12, m_contentScrollView->getContentSize().height - nSecondLabelHeight));

	m_curActiveNote->m_label_content_describe->setAnchorPoint(ccp(0, 1.0f));
	m_curActiveNote->m_label_content_describe->setPosition(ccp(m_label_content_describe->getPosition().x + m_label_content_describe->getContentSize().width - 15, m_contentScrollView->getContentSize().height - nSecondLineHeight));

	m_contentScrollView->addChild(m_label_content_describe);
	m_contentScrollView->addChild(m_curActiveNote->m_label_content_describe);

}

void ActiveUI::initActiveUIData()
{
	initLabelFromInternet();
	initNoteFromInternet();
	initUserActiveFromInternet();
	initTodayActiveFromInternet();


	// 更新左侧今日活跃度数据
	//ActiveTodayTableView::instance()->initDataFromInternet();

	if (!m_vector_activeNote.empty())
	{
		m_curActiveNote = m_vector_activeNote.at(0);
	}

	// 更新tableView数据
	m_tableView->reloadData();
	// 详情scrollView更新
	this->reloatActiveInfoScrollView();
	// 更新LeftTableView数据
	//m_leftTableView->reloadData();
}

void ActiveUI::initChallengeUi()
{
// 	ChallengeRoundUi * challengeui = ChallengeRoundUi::create();
// 	challengeui->ignoreAnchorPointForPosition(false);
// 	challengeui->setAnchorPoint(ccp(0,0));
// 	challengeui->setPosition(ccp(75,40));
// 	challengeui->setTag(ktagChallengeRoundUi);
// 	m_challenge_layer->addChild(challengeui);
}

void ActiveUI::callBackGoodIcon( CCObject* obj )
{
	UIImageView* pBtn = (UIImageView *)obj;
	// 此处用 所需的活跃度作为 tag
	int nTag = pBtn->getTag();

	int nSize = m_vector_labelToday.size();
	for (int i = 0; i < nSize; i++)
	{
		CActiveLabelToday * activeLabelToday = m_vector_labelToday.at(i);
		if (nTag == activeLabelToday->needactive())
		{
			
			// 判断当前奖励是否可领取
			
			// 今日活跃度
			int nTodayActive = ActiveData::instance()->get_todayActive();
			if (nTodayActive >= nTag)
			{
				/** 奖励物品状态 ：0.未领取奖励，1.已领取奖励完毕*/
				int nStatus = activeLabelToday->status();
				if (0 == nStatus)
				{
					// 向服务器请求领取奖励
					GameMessageProcessor::sharedMsgProcessor()->sendReq(5130, (void *)nTag);
				}
				else
				{
					GoodsInfo * pGoodsInfo = new GoodsInfo();
					pGoodsInfo->CopyFrom(activeLabelToday->goods(0));

					GoodsItemInfoBase * goodsItemInfoBase = GoodsItemInfoBase::create(pGoodsInfo,GameView::getInstance()->EquipListItem,0);
					goodsItemInfoBase->ignoreAnchorPointForPosition(false);
					goodsItemInfoBase->setAnchorPoint(ccp(0.5f, 0.5f));
					//goodsItemInfoBase->setPosition(ccp(size.width / 2, size.height / 2));

					GameView::getInstance()->getMainUIScene()->addChild(goodsItemInfoBase);
				}
			}
			else
			{
				GoodsInfo * pGoodsInfo = new GoodsInfo();
				pGoodsInfo->CopyFrom(activeLabelToday->goods(0));

				GoodsItemInfoBase * goodsItemInfoBase = GoodsItemInfoBase::create(pGoodsInfo,GameView::getInstance()->EquipListItem,0);
				goodsItemInfoBase->ignoreAnchorPointForPosition(false);
				goodsItemInfoBase->setAnchorPoint(ccp(0.5f, 0.5f));
				//goodsItemInfoBase->setPosition(ccp(size.width / 2, size.height / 2));

				GameView::getInstance()->getMainUIScene()->addChild(goodsItemInfoBase);
			}
		}
	}

}

void ActiveUI::callBackActiveTodayBtn( CCObject* obj )
{
	GameView::getInstance()->showAlertDialog("btn");
}

void ActiveUI::initTodayActiveFromInternet()
{
	// 今日活跃度
	int nTodayActive = ActiveData::instance()->get_todayActive();
	char str_todayActive[10];
	sprintf(str_todayActive, "%d", nTodayActive);

	/*m_labelBMF_activeToday->setText(str_todayActive);*/

	// 今日活跃度value添加一个动画（逐渐增长）
	PhysicalBarEffect* pLabelPhyBarEffect = PhysicalBarEffect::create(nTodayActive, 700, 3.5f, m_labelBMF_activeToday);
	this->addChild(pLabelPhyBarEffect);

	// 清空数据
	std::vector<CActiveLabelToday *>::iterator iter;
	for (iter = m_vector_labelToday.begin(); iter != m_vector_labelToday.end(); iter++)
	{
		delete *iter;
	}
	m_vector_labelToday.clear();

	for (unsigned int i = 0; i < ActiveData::instance()->m_vector_internet_labelToday.size(); i++)
	{
		CActiveLabelToday * activeLabelToday = new CActiveLabelToday();
		activeLabelToday->CopyFrom(*ActiveData::instance()->m_vector_internet_labelToday.at(i));

		m_vector_labelToday.push_back(activeLabelToday);
	}

	refreshTodayAcitveData();
}

void ActiveUI::activeToOpenPresentBox()
{
	// 请求服务器领取奖品
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5015);
}

void ActiveUI::activeToFamilyFight()
{
	MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	CCLayer *familyUILayer = (CCLayer*)mainscene->getChildByTag(kTagFamilyUI);
	if(familyUILayer == NULL)
	{
		FamilyUI * familyui =FamilyUI::create();
		familyui->ignoreAnchorPointForPosition(false);
		familyui->setAnchorPoint(ccp(0.5f,0.5f));
		familyui->setPosition(ccp(winSize.width/2,winSize.height/2));
		familyui->setTag(kTagFamilyUI);
		GameView::getInstance()->getMainUIScene()->addChild(familyui);
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1500,NULL);

		mainscene->remindFamilyApply();

		// 打开家族战UI
		familyui->openFamilyFightUI();
	}
}

void ActiveUI::activeToSingleFight()
{

	if (this->mTutorialIndex == 2)
	{
		Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if(sc != NULL)
			sc->endCommand(this);
	}

	// 过关斩将的activeType为8
	unsigned int nActiveLabelSize = ActiveData::instance()->m_vector_native_activeLabel.size();
	for (unsigned int i = 0; i < nActiveLabelSize; i++)
	{
		int nActiveType = ActiveData::instance()->m_vector_native_activeLabel.at(i)->m_activeLabel->activetype();
		if (8 == nActiveType)
		{
			CTargetPosition* pTargetPosition = new CTargetPosition();
			pTargetPosition->CopyFrom(ActiveData::instance()->m_vector_native_activeLabel.at(i)->m_activeLabel->npcpostion());

			CTargetNpc * pTargetNpc = new CTargetNpc();
			pTargetNpc->CopyFrom(pTargetPosition->targetnpc());

			long long targetNpcId = pTargetNpc->npcid();

			std::string targetMapId = pTargetPosition->mapid();
			int xPos = pTargetPosition->x();
			int yPos = pTargetPosition->y();

			int posX = GameView::getInstance()->getGameScene()->tileToPositionX(xPos);
			int posY = GameView::getInstance()->getGameScene()->tileToPositionY(yPos);

			CCPoint targetPos = CCPointMake(posX, posY);

			// 监听自动寻路
			// 判断是否加到mainUIScene
			ActiveManager * pActiveManager = (ActiveManager *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveLayer);
			if (NULL == pActiveManager)
			{
				ActiveManager* activeManager = ActiveManager::create();
				activeManager->set_targetMapId(targetMapId);
				activeManager->set_targetPos(targetPos);
				activeManager->set_targetNpcId(targetNpcId);
				activeManager->setLayerUserful(true);
				activeManager->setTag(kTagActiveLayer);

				GameView::getInstance()->getMainUIScene()->addChild(activeManager);
			}
			else 
			{
				pActiveManager->set_targetMapId(targetMapId);
				pActiveManager->set_targetPos(targetPos);
				pActiveManager->set_targetNpcId(targetNpcId);
				pActiveManager->setLayerUserful(true);
			}

			// 寻路逻辑
			if (strcmp(GameView::getInstance()->getMapInfo()->mapid().c_str(),targetMapId.c_str()) == 0)
			{
				//same map
				CCPoint cp_role = GameView::getInstance()->myplayer->getWorldPosition();
				CCPoint cp_target = targetPos;
				if (ccpDistance(cp_role,cp_target) < 96)
				{
					return;
				}
				else
				{
					if (targetPos.x == 0 && targetPos.y == 0)
					{
						return;
					}

					MyPlayerCommandMove* cmd = new MyPlayerCommandMove();
					// NOT reachable, so find the near one

					cmd->targetPosition = targetPos;
					cmd->method = MyPlayerCommandMove::method_searchpath;
					bool bSwitchCommandImmediately = true;
					if(GameView::getInstance()->myplayer->isAttacking())
						bSwitchCommandImmediately = false;
					GameView::getInstance()->myplayer->setNextCommand(cmd, bSwitchCommandImmediately);	
				}
			}
			else
			{
				// 不在同一地图，直接飞到目的地
				ActiveManager::AcrossMapTransport(targetMapId, targetPos);

				// 判断是否加到mainUIScene
				ActiveManager * pActiveLayer = (ActiveManager *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveLayer);
				if (NULL == pActiveLayer)
				{
					ActiveManager* activeLayer = ActiveManager::create();
					activeLayer->set_transportFinish(false);
					activeLayer->setTag(kTagActiveLayer);

					GameView::getInstance()->getMainUIScene()->addChild(activeLayer);
				}
				else 
				{
					pActiveLayer->set_transportFinish(false);
				}
			}

			CC_SAFE_DELETE(pTargetPosition);
			CC_SAFE_DELETE(pTargetNpc);
		}
		else 
		{
			continue;
		}
	}
}

bool ActiveUI::IsCanPushVector(int nId)
{
	bool bFlag = true;

	int nActiveTypeId = ActiveData::instance()->m_vector_native_activeLabel.at(nId)->m_activeLabel->activetypeid();
	int nActiveType = ActiveData::instance()->m_vector_native_activeLabel.at(nId)->m_activeLabel->activetype();
	if ((1 == nActiveType && 7 == nActiveTypeId) || (7 == nActiveType))
	{
		ActiveRole * activeRole = GameView::getInstance()->myplayer->getActiveRole();
		long long nFactionId = activeRole->playerbaseinfo().factionid();
		// 等于0说明没有家族
		if (0 == nFactionId)
		{
			bFlag = false;
		}
		else
		{
			bFlag = true;
		}
	}

	return bFlag;
}

void ActiveUI::initTodayUI()
{
	// 今日活跃度
	m_labelBMF_activeToday = (UILabelBMFont *)UIHelper::seekWidgetByName(s_pPanel, "LabelBMFont_todayActive");

	m_btn_gift_frame_1 = (UIButton *)UIHelper::seekWidgetByName(s_pPanel, "Button_1");
	m_btn_gift_frame_2 = (UIButton *)UIHelper::seekWidgetByName(s_pPanel, "Button_2");
	m_btn_gift_frame_3 = (UIButton *)UIHelper::seekWidgetByName(s_pPanel, "Button_3");
	m_btn_gift_frame_4 = (UIButton *)UIHelper::seekWidgetByName(s_pPanel, "Button_4");
	m_btn_gift_frame_5 = (UIButton *)UIHelper::seekWidgetByName(s_pPanel, "Button_5");

	m_imageView_gift_icon_1 = (UIImageView *)UIHelper::seekWidgetByName(s_pPanel, "ImageView_gift_icon_1");
	m_imageView_gift_icon_2 = (UIImageView *)UIHelper::seekWidgetByName(s_pPanel, "ImageView_gift_icon_2");
	m_imageView_gift_icon_3 = (UIImageView *)UIHelper::seekWidgetByName(s_pPanel, "ImageView_gift_icon_3");
	m_imageView_gift_icon_4 = (UIImageView *)UIHelper::seekWidgetByName(s_pPanel, "ImageView_gift_icon_4");
	m_imageView_gift_icon_5 = (UIImageView *)UIHelper::seekWidgetByName(s_pPanel, "ImageView_gift_icon_5");

	m_btn_gift_frame_1->setTouchEnable(true);
	m_btn_gift_frame_2->setTouchEnable(true);
	m_btn_gift_frame_3->setTouchEnable(true);
	m_btn_gift_frame_4->setTouchEnable(true);
	m_btn_gift_frame_5->setTouchEnable(true);

	m_btn_gift_frame_1->setPressedActionEnabled(true);
	m_btn_gift_frame_2->setPressedActionEnabled(true);
	m_btn_gift_frame_3->setPressedActionEnabled(true);
	m_btn_gift_frame_4->setPressedActionEnabled(true);
	m_btn_gift_frame_5->setPressedActionEnabled(true);

	m_btn_gift_frame_1->setScale(0.8f);
	m_btn_gift_frame_2->setScale(0.8f);
	m_btn_gift_frame_3->setScale(0.8f);
	m_btn_gift_frame_4->setScale(0.8f);
	m_btn_gift_frame_5->setScale(0.8f);


	m_label_gift_num_1 = (UILabel *)UIHelper::seekWidgetByName(s_pPanel, "Label_gift_num_1");
	m_label_gift_num_2 = (UILabel *)UIHelper::seekWidgetByName(s_pPanel, "Label_gift_num_2");
	m_label_gift_num_3 = (UILabel *)UIHelper::seekWidgetByName(s_pPanel, "Label_gift_num_3");
	m_label_gift_num_4 = (UILabel *)UIHelper::seekWidgetByName(s_pPanel, "Label_gift_num_4");
	m_label_gift_num_5 = (UILabel *)UIHelper::seekWidgetByName(s_pPanel, "Label_gift_num_5");

	m_labelBMF_gift_1 = (UILabelBMFont *)UIHelper::seekWidgetByName(s_pPanel, "LabelBMFont_gift_value_1");
	m_labelBMF_gift_2 = (UILabelBMFont *)UIHelper::seekWidgetByName(s_pPanel, "LabelBMFont_gift_value_2");
	m_labelBMF_gift_3 = (UILabelBMFont *)UIHelper::seekWidgetByName(s_pPanel, "LabelBMFont_gift_value_3");
	m_labelBMF_gift_4 = (UILabelBMFont *)UIHelper::seekWidgetByName(s_pPanel, "LabelBMFont_gift_value_4");
	m_labelBMF_gift_5 = (UILabelBMFont *)UIHelper::seekWidgetByName(s_pPanel, "LabelBMFont_gift_value_5");

	m_imageView_progress_1 = (UIImageView *)UIHelper::seekWidgetByName(s_pPanel, "ImageView_process_1");
	m_imageView_progress_2 = (UIImageView *)UIHelper::seekWidgetByName(s_pPanel, "ImageView_process_2");
	m_imageView_progress_3 = (UIImageView *)UIHelper::seekWidgetByName(s_pPanel, "ImageView_process_3");
	m_imageView_progress_4 = (UIImageView *)UIHelper::seekWidgetByName(s_pPanel, "ImageView_process_4");
	m_imageView_progress_5 = (UIImageView *)UIHelper::seekWidgetByName(s_pPanel, "ImageView_process_5");


}

void ActiveUI::refreshTodayAcitveData()
{
	int nSize = m_vector_labelToday.size();
	if (5 == nSize)
	{
		for (int i = 0; i < nSize; i++)
		{
			CActiveLabelToday * activeLabelToday = m_vector_labelToday.at(i);

			std::vector<GoodsInfo *> vector_goods;
			int nGoodSize = m_vector_labelToday.at(i)->goods_size();	
			for(int j = 0; j < nGoodSize; j ++)
			{
				GoodsInfo* pGoodsInfo = new GoodsInfo();
				pGoodsInfo->CopyFrom(m_vector_labelToday.at(i)->goods(j));

				vector_goods.push_back(pGoodsInfo);
			}

			// icon_frame
			if (0 == nGoodSize)
			{
				return ;
			}

			GoodsInfo* tmpGoodsInfo = vector_goods.at(0);

			std::string goods_icon = vector_goods.at(0)->icon();

			std::string goodsIconName = "res_ui/props_icon/";
			goodsIconName.append(goods_icon);
			goodsIconName.append(".png");

			// 根据goods品阶选择底图
			int nQuality = vector_goods.at(0)->quality();
			std::string strQuality = getEquipmentQualityByIndex(nQuality);

			initGoodsFrame(i, strQuality, goodsIconName, activeLabelToday);
		}
	}
}

std::string ActiveUI::getEquipmentQualityByIndex( int quality )
{
	std::string frameColorPath = "res_ui/";
	switch(quality)
	{
	case 1:
		{
			frameColorPath.append("sdi_white");
		}break;
	case 2:
		{
			frameColorPath.append("sdi_green");
		}break;
	case 3:
		{
			frameColorPath.append("sdi_bule");
		}break;
	case 4:
		{
			frameColorPath.append("sdi_purple");
		}break;
	case 5:
		{
			frameColorPath.append("sdi_orange");
		}break;
	}
	frameColorPath.append(".png");
	return frameColorPath;
}

void ActiveUI::initGoodsFrame( int nIndex, std::string strFrame, std::string strGoodIconName, CActiveLabelToday* pActiveLabelToday)
{
	// good 数量
	std::vector<int > vector_goodsNum;
	int nGoodNumSize = m_vector_labelToday.at(nIndex)->goodsnumber_size();
	for(int i = 0; i < nGoodNumSize; i ++)
	{
		vector_goodsNum.push_back(m_vector_labelToday.at(nIndex)->goodsnumber(i));
	}

	if (0 == nGoodNumSize)
	{
		return ;
	}

	// good's num
	int nNum = vector_goodsNum.at(0);

	char charGoodNum[20];
	sprintf(charGoodNum, "%d", nNum);

	// 单个item的活跃度
	int nActiveItem = pActiveLabelToday->needactive();
	
	char charActiveItem[20];
	sprintf(charActiveItem, "%d", nActiveItem);

	// 今日活跃度
	int nTodayActive = ActiveData::instance()->get_todayActive();

	// 计算进度条
	float progress_scale = (nTodayActive * 1.0f) / (nActiveItem * 1.0f);
	if (progress_scale > 1.0f)
	{
		progress_scale = 1.0f;
	}

	const float fActionTime = 0.5f;
	const float fActionTimeDelay = 0.55f;

	if (0 == nIndex)
	{
		m_btn_gift_frame_1->loadTextures(strFrame.c_str(), strFrame.c_str(), strFrame.c_str());
		m_btn_gift_frame_1->setTouchEnable(true);
		m_btn_gift_frame_1->addReleaseEvent(this, coco_releaseselector(ActiveUI::callBackGoodIcon));
		m_btn_gift_frame_1->setTag(pActiveLabelToday->needactive());

		m_imageView_gift_icon_1->setTexture(strGoodIconName.c_str());
		m_imageView_gift_icon_1->setTouchEnable(false);

		m_label_gift_num_1->setText(charGoodNum);

		m_labelBMF_gift_1->setText(charActiveItem);

		if (nTodayActive >= nActiveItem)
		{
			// 判断奖励是否领取，若没领取，则加动画效果
			/** 奖励物品状态 ：0.未领取奖励，1.已领取奖励完毕*/
			int nStatus = pActiveLabelToday->status();
			if (0 == nStatus)
			{
				m_imageView_gift_icon_1->runAction(initActionForGoods());
				this->addParticleForGift(nIndex + 1);
			}
			else
			{
				m_imageView_gift_icon_1->stopAllActions();
				m_imageView_gift_icon_1->setScale(0.8f);
				this->removeParticleForGift(nIndex + 1);
			}
		}

		// 计算进度条
		float progress_scale = (nTodayActive * 1.0f) / (nActiveItem * 1.0f);
		if (progress_scale > 1.0f)
		{
			progress_scale = 1.0f;
		}

		//m_imageView_progress_1->setTextureRect(CCRectMake(0, 0, m_imageView_progress_1->getContentSize().width * progress_scale, m_imageView_progress_1->getContentSize().height));

		m_imageView_progress_1->setTextureRect(CCRectMake(0, 0, 
			m_imageView_progress_1->getContentSize().width * 0.0f,
			m_imageView_progress_1->getContentSize().height));

		PhysicalBarEffect* pPhyBarEffect = PhysicalBarEffect::create(nTodayActive, nActiveItem, fActionTime, m_imageView_progress_1, NULL);
		this->addChild(pPhyBarEffect);
	}
	else if (1 == nIndex)
	{
		m_btn_gift_frame_2->loadTextures(strFrame.c_str(), strFrame.c_str(), strFrame.c_str());
		m_btn_gift_frame_2->setTouchEnable(true);
		m_btn_gift_frame_2->addReleaseEvent(this, coco_releaseselector(ActiveUI::callBackGoodIcon));
		m_btn_gift_frame_2->setTag(pActiveLabelToday->needactive());

		m_imageView_gift_icon_2->setTexture(strGoodIconName.c_str());
		m_imageView_gift_icon_2->setTouchEnable(false);

		m_label_gift_num_2->setText(charGoodNum);

		m_labelBMF_gift_2->setText(charActiveItem);

		int nPreActive = m_vector_labelToday.at(nIndex - 1)->needactive();

		if (nTodayActive >= nPreActive)
		{
			int nTrueActive = nActiveItem - nPreActive;

			int nTrueActiveOff = nTodayActive - nPreActive;

			// 计算进度条
			float progress_scale = (nTrueActiveOff * 1.0f) / (nTrueActive * 1.0f);
			if (progress_scale > 1.0f)
			{
				progress_scale = 1.0f;
			}

			//m_imageView_progress_2->setTextureRect(CCRectMake(0, 0, m_imageView_progress_2->getContentSize().width * progress_scale, m_imageView_progress_2->getContentSize().height));

			m_imageView_progress_2->setTextureRect(CCRectMake(0, 0, 
				m_imageView_progress_2->getContentSize().width * 0.0f,
				m_imageView_progress_2->getContentSize().height));

			PhysicalBarEffect* pPhyBarEffect = PhysicalBarEffect::create(nTrueActiveOff, nTrueActive, fActionTime, m_imageView_progress_2, NULL, fActionTimeDelay);
			this->addChild(pPhyBarEffect);
		}
		else
		{
			m_imageView_progress_2->setTextureRect(CCRectMake(0, 0, 0, m_imageView_progress_2->getContentSize().height));
		}

		if (nTodayActive >= nActiveItem)
		{
			// 判断奖励是否领取，若没领取，则加动画效果
			/** 奖励物品状态 ：0.未领取奖励，1.已领取奖励完毕*/
			int nStatus = pActiveLabelToday->status();
			if (0 == nStatus)
			{
				m_imageView_gift_icon_2->runAction(initActionForGoods());
				this->addParticleForGift(nIndex + 1);
			}
			else
			{
				m_imageView_gift_icon_2->stopAllActions();
				m_imageView_gift_icon_2->setScale(0.8f);
				this->removeParticleForGift(nIndex + 1);
			}
		}
	}
	else if (2 == nIndex)
	{
		m_btn_gift_frame_3->loadTextures(strFrame.c_str(), strFrame.c_str(), strFrame.c_str());
		m_btn_gift_frame_3->setTouchEnable(true);
		m_btn_gift_frame_3->addReleaseEvent(this, coco_releaseselector(ActiveUI::callBackGoodIcon));
		m_btn_gift_frame_3->setTag(pActiveLabelToday->needactive());

		m_imageView_gift_icon_3->setTexture(strGoodIconName.c_str());
		m_imageView_gift_icon_3->setTouchEnable(false);

		m_label_gift_num_3->setText(charGoodNum);

		m_labelBMF_gift_3->setText(charActiveItem);

		int nPreActive = m_vector_labelToday.at(nIndex - 1)->needactive();

		if (nTodayActive >= nPreActive)
		{
			int nTrueActive = nActiveItem - nPreActive;

			int nTrueActiveOff = nTodayActive - nPreActive;

			// 计算进度条
			float progress_scale = (nTrueActiveOff * 1.0f) / (nTrueActive * 1.0f);
			if (progress_scale > 1.0f)
			{
				progress_scale = 1.0f;
			}

			//m_imageView_progress_3->setTextureRect(CCRectMake(0, 0, m_imageView_progress_3->getContentSize().width * progress_scale, m_imageView_progress_3->getContentSize().height));

			m_imageView_progress_3->setTextureRect(CCRectMake(0, 0, 
				m_imageView_progress_3->getContentSize().width * 0.0f,
				m_imageView_progress_3->getContentSize().height));

			PhysicalBarEffect* pPhyBarEffect = PhysicalBarEffect::create(nTrueActiveOff, nTrueActive, fActionTime, m_imageView_progress_3, NULL, fActionTimeDelay * 2);
			this->addChild(pPhyBarEffect);
		}
		else
		{
			m_imageView_progress_3->setTextureRect(CCRectMake(0, 0, 0, m_imageView_progress_3->getContentSize().height));
		}

		if (nTodayActive >= nActiveItem)
		{
			// 判断奖励是否领取，若没领取，则加动画效果
			/** 奖励物品状态 ：0.未领取奖励，1.已领取奖励完毕*/
			int nStatus = pActiveLabelToday->status();
			if (0 == nStatus)
			{
				m_imageView_gift_icon_3->runAction(initActionForGoods());
				this->addParticleForGift(nIndex + 1);
			}
			else
			{
				m_imageView_gift_icon_3->stopAllActions();
				m_imageView_gift_icon_3->setScale(0.8f);
				this->removeParticleForGift(nIndex + 1);
			}
		}
	}
	else if (3 == nIndex)
	{
		m_btn_gift_frame_4->loadTextures(strFrame.c_str(), strFrame.c_str(), strFrame.c_str());
		m_btn_gift_frame_4->setTouchEnable(true);
		m_btn_gift_frame_4->addReleaseEvent(this, coco_releaseselector(ActiveUI::callBackGoodIcon));
		m_btn_gift_frame_4->setTag(pActiveLabelToday->needactive());

		m_imageView_gift_icon_4->setTexture(strGoodIconName.c_str());
		m_imageView_gift_icon_4->setTouchEnable(false);


		m_label_gift_num_4->setText(charGoodNum);

		m_labelBMF_gift_4->setText(charActiveItem);

		int nPreActive = m_vector_labelToday.at(nIndex - 1)->needactive();

		if (nTodayActive >= nPreActive)
		{
			int nTrueActive = nActiveItem - nPreActive;

			int nTrueActiveOff = nTodayActive - nPreActive;

			// 计算进度条
			float progress_scale = (nTrueActiveOff * 1.0f) / (nTrueActive * 1.0f);
			if (progress_scale > 1.0f)
			{
				progress_scale = 1.0f;
			}

			//m_imageView_progress_4->setTextureRect(CCRectMake(0, 0, m_imageView_progress_4->getContentSize().width * progress_scale, m_imageView_progress_4->getContentSize().height));

			m_imageView_progress_4->setTextureRect(CCRectMake(0, 0, 
				m_imageView_progress_4->getContentSize().width * 0.0f,
				m_imageView_progress_4->getContentSize().height));

			PhysicalBarEffect* pPhyBarEffect = PhysicalBarEffect::create(nTrueActiveOff, nTrueActive, fActionTime, m_imageView_progress_4, NULL, fActionTimeDelay * 3);
			this->addChild(pPhyBarEffect);
		}
		else
		{
			m_imageView_progress_4->setTextureRect(CCRectMake(0, 0, 0, m_imageView_progress_4->getContentSize().height));
		}

		if (nTodayActive >= nActiveItem)
		{
			// 判断奖励是否领取，若没领取，则加动画效果
			/** 奖励物品状态 ：0.未领取奖励，1.已领取奖励完毕*/
			int nStatus = pActiveLabelToday->status();
			if (0 == nStatus)
			{
				m_imageView_gift_icon_4->runAction(initActionForGoods());
				this->addParticleForGift(nIndex + 1);
			}
			else
			{
				m_imageView_gift_icon_4->stopAllActions();
				m_imageView_gift_icon_4->setScale(0.8f);
				this->removeParticleForGift(nIndex + 1);
			}
		}
	}
	else if (4 == nIndex)
	{
		m_btn_gift_frame_5->loadTextures(strFrame.c_str(), strFrame.c_str(), strFrame.c_str());
		m_btn_gift_frame_5->setTouchEnable(true);
		m_btn_gift_frame_5->addReleaseEvent(this, coco_releaseselector(ActiveUI::callBackGoodIcon));
		m_btn_gift_frame_5->setTag(pActiveLabelToday->needactive());

		m_imageView_gift_icon_5->setTexture(strGoodIconName.c_str());
		m_imageView_gift_icon_5->setTouchEnable(false);

		m_label_gift_num_5->setText(charGoodNum);

		m_labelBMF_gift_5->setText(charActiveItem);

		int nPreActive = m_vector_labelToday.at(nIndex - 1)->needactive();

		if (nTodayActive >= nPreActive)
		{
			int nTrueActive = nActiveItem - nPreActive;

			int nTrueActiveOff = nTodayActive - nPreActive;

			// 计算进度条
			float progress_scale = (nTrueActiveOff * 1.0f) / (nTrueActive * 1.0f);
			if (progress_scale > 1.0f)
			{
				progress_scale = 1.0f;
			}

			//m_imageView_progress_5->setTextureRect(CCRectMake(0, 0, m_imageView_progress_5->getContentSize().width * progress_scale, m_imageView_progress_5->getContentSize().height));

			m_imageView_progress_5->setTextureRect(CCRectMake(0, 0, 
				m_imageView_progress_5->getContentSize().width * 0.0f,
				m_imageView_progress_5->getContentSize().height));

			PhysicalBarEffect* pPhyBarEffect = PhysicalBarEffect::create(nTrueActiveOff, nTrueActive, fActionTime, m_imageView_progress_5, NULL, fActionTimeDelay * 4);
			this->addChild(pPhyBarEffect);
		}
		else
		{
			m_imageView_progress_5->setTextureRect(CCRectMake(0, 0, 0, m_imageView_progress_5->getContentSize().height));
		}

		if (nTodayActive >= nActiveItem)
		{
			// 判断奖励是否领取，若没领取，则加动画效果
			/** 奖励物品状态 ：0.未领取奖励，1.已领取奖励完毕*/
			int nStatus = pActiveLabelToday->status();
			if (0 == nStatus)
			{
				m_imageView_gift_icon_5->runAction(initActionForGoods());
				this->addParticleForGift(nIndex + 1);
			}
			else
			{
				m_imageView_gift_icon_5->stopAllActions();
				m_imageView_gift_icon_5->setScale(0.8f);
				this->removeParticleForGift(nIndex + 1);
			}
		}
	}
}

CCActionInterval * ActiveUI::initActionForGoods()
{
	// 放大缩小的 效果
	CCScaleTo * pActionScaleToSmall = CCScaleTo::create(0.75f, 0.8f);
	CCScaleTo * pActionScaleToBig = CCScaleTo::create(0.75f, 1.2f);
	CCSequence * pActionSequence = CCSequence::createWithTwoActions(pActionScaleToSmall, pActionScaleToBig);
	CCRepeatForever *pActionRepeat = CCRepeatForever::create(pActionSequence);

	return (CCActionInterval*)pActionRepeat;
}

void ActiveUI::activeToWorldBoss()
{
	//// 世界BOSS的activeType为9
	//unsigned int nActiveLabelSize = ActiveData::instance()->m_vector_native_activeLabel.size();
	//for (unsigned int i = 0; i < nActiveLabelSize; i++)
	//{
	//	int nActiveType = ActiveData::instance()->m_vector_native_activeLabel.at(i)->m_activeLabel->activetype();
	//	if (9 == nActiveType)
	//	{
	//		CTargetPosition* pTargetPosition = new CTargetPosition();
	//		pTargetPosition->CopyFrom(ActiveData::instance()->m_vector_native_activeLabel.at(i)->m_activeLabel->npcpostion());

	//		CTargetNpc * pTargetNpc = new CTargetNpc();
	//		pTargetNpc->CopyFrom(pTargetPosition->targetnpc());

	//		long long targetNpcId = pTargetNpc->npcid();

	//		std::string targetMapId = pTargetPosition->mapid();
	//		int xPos = pTargetPosition->x();
	//		int yPos = pTargetPosition->y();

	//		int posX = GameView::getInstance()->getGameScene()->tileToPositionX(xPos);
	//		int posY = GameView::getInstance()->getGameScene()->tileToPositionY(yPos);

	//		CCPoint targetPos = CCPointMake(posX, posY);

	//		// 监听自动寻路
	//		// 判断是否加到mainUIScene
	//		ActiveManager * pActiveLayer = (ActiveManager *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveLayer);
	//		if (NULL == pActiveLayer)
	//		{
	//			ActiveManager* activeLayer = ActiveManager::create();
	//			activeLayer->set_targetMapId(targetMapId);
	//			activeLayer->set_targetPos(targetPos);
	//			activeLayer->set_targetNpcId(targetNpcId);
	//			activeLayer->setLayerUserful(true);
	//			activeLayer->setTag(kTagActiveLayer);

	//			GameView::getInstance()->getMainUIScene()->addChild(activeLayer);
	//		}
	//		else 
	//		{
	//			pActiveLayer->set_targetMapId(targetMapId);
	//			pActiveLayer->set_targetPos(targetPos);
	//			pActiveLayer->set_targetNpcId(targetNpcId);
	//			pActiveLayer->setLayerUserful(true);
	//		}

	//		// 寻路逻辑
	//		if (strcmp(GameView::getInstance()->getMapInfo()->mapid().c_str(),targetMapId.c_str()) == 0)
	//		{
	//			//same map
	//			CCPoint cp_role = GameView::getInstance()->myplayer->getWorldPosition();
	//			CCPoint cp_target = targetPos;
	//			if (ccpDistance(cp_role,cp_target) < 64)
	//			{
	//				return;
	//			}
	//			else
	//			{
	//				if (targetPos.x == 0 && targetPos.y == 0)
	//				{
	//					return;
	//				}

	//				MyPlayerCommandMove* cmd = new MyPlayerCommandMove();
	//				// NOT reachable, so find the near one

	//				cmd->targetPosition = targetPos;
	//				cmd->method = MyPlayerCommandMove::method_searchpath;
	//				bool bSwitchCommandImmediately = true;
	//				if(GameView::getInstance()->myplayer->isAttacking())
	//					bSwitchCommandImmediately = false;
	//				GameView::getInstance()->myplayer->setNextCommand(cmd, bSwitchCommandImmediately);	
	//			}
	//		}
	//		else
	//		{
	//			// 不在同一地图，直接飞到目的地
	//			ActiveManager::AcrossMapTransport(targetMapId, targetPos);

	//			// 判断是否加到mainUIScene
	//			ActiveManager * pActiveLayer = (ActiveManager *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveLayer);
	//			if (NULL == pActiveLayer)
	//			{
	//				ActiveManager* activeLayer = ActiveManager::create();
	//				activeLayer->set_transportFinish(false);
	//				activeLayer->setTag(kTagActiveLayer);

	//				GameView::getInstance()->getMainUIScene()->addChild(activeLayer);
	//			}
	//			else 
	//			{
	//				pActiveLayer->set_transportFinish(false);
	//			}
	//		}

	//		CC_SAFE_DELETE(pTargetPosition);
	//		CC_SAFE_DELETE(pTargetNpc);
	//	}
	//	else 
	//	{
	//		continue;
	//	}
	//}


	// 以上逻辑是 寻路到世界BOSS npc处（npc挂着世界BOSS的任务）
	// 此处逻辑是 直接打开世界BOSS的面板
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	CCLayer *worldBossUI = (CCLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagWorldBossUI);
	if(worldBossUI == NULL)
	{
		worldBossUI = WorldBossUI::create();
		GameView::getInstance()->getMainUIScene()->addChild(worldBossUI,0,kTagWorldBossUI);
		worldBossUI->ignoreAnchorPointForPosition(false);
		worldBossUI->setAnchorPoint(ccp(0.5f, 0.5f));
		worldBossUI->setPosition(ccp(winSize.width/2, winSize.height/2));

		GameMessageProcessor::sharedMsgProcessor()->sendReq(5301);
	}
}

void ActiveUI::activeToRewardMisson()
{
	unsigned int nActiveLabelSize = ActiveData::instance()->m_vector_native_activeLabel.size();
	for (unsigned int i = 0; i < nActiveLabelSize; i++)
	{
		int nActiveType = ActiveData::instance()->m_vector_native_activeLabel.at(i)->m_activeLabel->activetype();
		int nActiveTypeId = ActiveData::instance()->m_vector_native_activeLabel.at(i)->m_activeLabel->activetypeid();
		if (1 == nActiveType && 12 == nActiveTypeId)
		{
			CTargetPosition* pTargetPosition = new CTargetPosition();
			pTargetPosition->CopyFrom(ActiveData::instance()->m_vector_native_activeLabel.at(i)->m_activeLabel->npcpostion());

			CTargetNpc * pTargetNpc = new CTargetNpc();
			pTargetNpc->CopyFrom(pTargetPosition->targetnpc());

			long long targetNpcId = pTargetNpc->npcid();

			std::string targetMapId = pTargetPosition->mapid();
			int xPos = pTargetPosition->x();
			int yPos = pTargetPosition->y();

			int posX = GameView::getInstance()->getGameScene()->tileToPositionX(xPos);
			int posY = GameView::getInstance()->getGameScene()->tileToPositionY(yPos);

			CCPoint targetPos = CCPointMake(posX, posY);

			// 监听自动寻路
			// 判断是否加到mainUIScene
			ActiveManager * pActiveLayer = (ActiveManager *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveLayer);
			if (NULL == pActiveLayer)
			{
				ActiveManager* activeLayer = ActiveManager::create();
				activeLayer->set_targetMapId(targetMapId);
				activeLayer->set_targetPos(targetPos);
				activeLayer->set_targetNpcId(targetNpcId);
				activeLayer->setLayerUserful(true);
				activeLayer->setTag(kTagActiveLayer);

				GameView::getInstance()->getMainUIScene()->addChild(activeLayer);
			}
			else 
			{
				pActiveLayer->set_targetMapId(targetMapId);
				pActiveLayer->set_targetPos(targetPos);
				pActiveLayer->set_targetNpcId(targetNpcId);
				pActiveLayer->setLayerUserful(true);
			}

			// 寻路逻辑
			if (strcmp(GameView::getInstance()->getMapInfo()->mapid().c_str(),targetMapId.c_str()) == 0)
			{
				//same map
				CCPoint cp_role = GameView::getInstance()->myplayer->getWorldPosition();
				CCPoint cp_target = targetPos;
				if (ccpDistance(cp_role,cp_target) < 64)
				{
					return;
				}
				else
				{
					if (targetPos.x == 0 && targetPos.y == 0)
					{
						return;
					}

					MyPlayerCommandMove* cmd = new MyPlayerCommandMove();
					// NOT reachable, so find the near one

					cmd->targetPosition = targetPos;
					cmd->method = MyPlayerCommandMove::method_searchpath;
					bool bSwitchCommandImmediately = true;
					if(GameView::getInstance()->myplayer->isAttacking())
						bSwitchCommandImmediately = false;
					GameView::getInstance()->myplayer->setNextCommand(cmd, bSwitchCommandImmediately);	
				}
			}
			else
			{
				// 不在同一地图，直接飞到目的地
				ActiveManager::AcrossMapTransport(targetMapId, targetPos);

				// 判断是否加到mainUIScene
				ActiveManager * pActiveLayer = (ActiveManager *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveLayer);
				if (NULL == pActiveLayer)
				{
					ActiveManager* activeLayer = ActiveManager::create();
					activeLayer->set_transportFinish(false);
					activeLayer->setTag(kTagActiveLayer);

					GameView::getInstance()->getMainUIScene()->addChild(activeLayer);
				}
				else 
				{
					pActiveLayer->set_transportFinish(false);
				}
			}

			CC_SAFE_DELETE(pTargetPosition);
			CC_SAFE_DELETE(pTargetNpc);
		}
		else 
		{
			continue;
		}
	}
}

void ActiveUI::addParticleForGift( int nIndex )
{
	float width = m_btn_gift_frame_1->getContentSize().width * 0.8f;
	float height = m_btn_gift_frame_1->getContentSize().height * 0.8f;

	float xPos = m_btn_gift_frame_1->getPosition().x - width / 2.0f;
	float yPos = m_btn_gift_frame_1->getPosition().y - height / 2.0f;

	// 根据Cocostudio 读取的json文件，一共有5个奖励
	int nGiftSize = 5;
	for (int i = 0; i < nGiftSize; i ++)
	{
		if(nIndex == i + 1)
		{
			// 设置粒子特效tag值，用于删除时使用
			int nParticleTag_1 = nIndex * 100 + 1;
			int nParticleTag_2 = nIndex * 100 + 2;

			// 判断粒子效果是否存在，存在的话则不创建
			CCNode * pNode_1 = (CCNode*)this->m_active_layer->getChildByTag(nParticleTag_1);
			CCNode * pNode_2 = (CCNode*)this->m_active_layer->getChildByTag(nParticleTag_2);
			if (NULL != pNode_1 || NULL != pNode_2)
			{
				return;
			}

			float speed = 100.f;

			float nOff = 93.0f;

			std::string _path = "animation/texiao/particledesigner/";
			_path.append("tuowei0.plist");

			CCParticleSystemQuad* particleEffect_1 = CCParticleSystemQuad::create(_path.c_str());   //tuowei0.plist
			particleEffect_1->setPosition(ccp(xPos + i * nOff, yPos));
			//particleEffect_1->setStartSize(30);
			particleEffect_1->setPositionType(kCCPositionTypeFree);
			particleEffect_1->setVisible(true);
			particleEffect_1->setScale(1.0f);
			//particleEffect_1->setLife(1.5f);
			particleEffect_1->setTag(nParticleTag_1);
			//particleEffect_1->setStartColor(color);
			//particleEffect_1->setEndColor(color);
			this->m_active_layer->addChild(particleEffect_1);

			CCSequence * sequence_1 = CCSequence::create(
				CCMoveTo::create(height*1.0f/speed,ccp(xPos + i * nOff, yPos + height)),
				CCMoveTo::create(width*1.0f/speed,ccp(xPos + i * nOff + width, yPos + height)),
				CCMoveTo::create(height*1.0f/speed,ccp(xPos + i * nOff + width, yPos)),
				CCMoveTo::create(width*1.0f/speed,ccp(xPos + i * nOff, yPos)),
				NULL
				);
			CCRepeatForever * repeatForeverAnm_1 = CCRepeatForever::create(sequence_1);
			particleEffect_1->runAction(repeatForeverAnm_1);


			CCParticleSystemQuad* particleEffect_2 = CCParticleSystemQuad::create(_path.c_str());   //tuowei0.plist
			particleEffect_2->setPosition(ccp(xPos + i * nOff + 2 + width,yPos + 2 + height));
			//particleEffect_2->setStartSize(30);
			particleEffect_2->setPositionType(kCCPositionTypeFree);
			particleEffect_2->setVisible(true);
			particleEffect_2->setScale(1.0f);
			//particleEffect_2->setLife(1.5f);
			particleEffect_2->setTag(nParticleTag_2);
			//particleEffect_2->setStartColor(color);
			//particleEffect_2->setEndColor(color);
			this->m_active_layer->addChild(particleEffect_2);

			CCSequence * sequence_2 = CCSequence::create(
				CCMoveTo::create(height*1.0f/speed,ccp(xPos + i * nOff + width, yPos)),
				CCMoveTo::create(width*1.0f/speed,ccp(xPos + i * nOff, yPos)),
				CCMoveTo::create(height*1.0f/speed,ccp(xPos + i * nOff, yPos  + height)),
				CCMoveTo::create(width*1.0f/speed,ccp(xPos + i * nOff + width, yPos  + height)),
				NULL
				);
			CCRepeatForever * repeatForeverAnm_2 = CCRepeatForever::create(sequence_2);
			particleEffect_2->runAction(repeatForeverAnm_2);

			break;
		}
	}
}

void ActiveUI::removeParticleForGift( int nIndex )
{
	// 取得粒子特效tag值，删除使用
	int nParticleTag_1 = nIndex * 100 + 1;
	int nParticleTag_2 = nIndex * 100 + 2;

	CCNode * pNode_1 = (CCNode*)this->m_active_layer->getChildByTag(nParticleTag_1);
	if (NULL != pNode_1)
	{
		this->m_active_layer->removeChildByTag(nParticleTag_1);
	}

	CCNode * pNode_2 = (CCNode*)this->m_active_layer->getChildByTag(nParticleTag_2);
	if (NULL != pNode_2)
	{
		this->m_active_layer->removeChildByTag(nParticleTag_2);
	}
}




//void ActiveUI::AcrossMapTransport( std::string mapId,CCPoint pos )
//{
//	setStatus(ActiveUI::status_waitForTransport);
//	//req acrossMap
//	CCPoint * t_point = new CCPoint(MissionManager::getInstance()->getNearestReachablePos(pos));
//	//CCPoint * t_point = new CCPoint(pos);
//	GameMessageProcessor::sharedMsgProcessor()->sendReq(1114,(void *)mapId.c_str(),t_point);
//	delete t_point;
//}



void ActiveUI::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void ActiveUI::addCCTutorialIndicator( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction,int tutorialIndex )
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height-480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,65,50);
// 	tutorialIndicator->setPosition(ccp(pos.x+505,pos.y+350));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	m_up_layer->addChild(tutorialIndicator);

	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,115,50,true);
	tutorialIndicator->setDrawNodePos(ccp(pos.x+452+_w,pos.y+219+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}

	mTutorialIndex = tutorialIndex;
}

void ActiveUI::addCCTutorialIndicatorSingCopy( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction,int tutorialIndex )
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height-480)/2;

	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,115,50,true);
	tutorialIndicator->setDrawNodePos(ccp(pos.x+452+_w,pos.y+330+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}

	mTutorialIndex = tutorialIndex;
}


void ActiveUI::removeCCTutorialIndicator()
{
	CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(m_up_layer->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		CCTeachingGuide* teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}

	mTutorialIndex = -1;
}
