#include "ActiveIconWidget.h"
#include "../../GameView.h"
#include "ActiveUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../missionscene/MissionManager.h"
#include "../../messageclient/element/MissionInfo.h"
#include "../../utils/StaticDataManager.h"
#include "ActiveShopData.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "ActiveData.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../messageclient/element/CActiveLabel.h"
#include "ActiveLabelData.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/ScriptManager.h"
#include "../../gamescene_state/sceneelement/GuideMap.h"

#define  TAG_TUTORIALPARTICLE_ACTIVEICON 500

ActiveIconWidget::ActiveIconWidget(void)
	:m_btn_activeIcon(NULL)
	,m_layer_active(NULL)
	,m_label_active_text(NULL)
	,m_spirte_fontBg(NULL)
	,m_tutorialParticle(NULL)
{

}


ActiveIconWidget::~ActiveIconWidget(void)
{
	
}

ActiveIconWidget* ActiveIconWidget::create()
{
	ActiveIconWidget * activeIcon = new ActiveIconWidget();
	if (activeIcon && activeIcon->init())
	{
		activeIcon->autorelease();
		return activeIcon;
	}
	CC_SAFE_DELETE(activeIcon);
	return NULL;
}

bool ActiveIconWidget::init()
{
	if (UIWidget::init())
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();


		// icon
		m_btn_activeIcon = UIButton::create();
		m_btn_activeIcon->setTouchEnable(true);
		m_btn_activeIcon->setPressedActionEnabled(true);

		m_btn_activeIcon->setTextures("gamescene_state/zhujiemian3/tubiao/active.png", "gamescene_state/zhujiemian3/tubiao/active.png","");
		
		m_btn_activeIcon->setAnchorPoint(ccp(0.5f, 0.5f));
		//m_btn_activeIcon->setPosition(ccp( winSize.width - 70 - 159 - 80, winSize.height-70 - 80));
		m_btn_activeIcon->setPosition(ccp(0, 0));
		m_btn_activeIcon->addReleaseEvent(this, coco_cancelselector(ActiveIconWidget::callBackActive));

		// label
		const char * str_active = StringDataManager::getString("activy_chartInfo2");

		m_label_active_text = UILabel::create();
		m_label_active_text->setText(str_active);
		m_label_active_text->setColor(ccc3(255, 246, 0));
		m_label_active_text->setFontSize(14);
		m_label_active_text->setAnchorPoint(ccp(0.5f, 0.5f));
		m_label_active_text->setPosition(ccp(m_btn_activeIcon->getPosition().x, m_btn_activeIcon->getPosition().y - m_btn_activeIcon->getContentSize().height / 2 - 5));


		// text_bg
		m_spirte_fontBg = CCScale9Sprite::create(CCRectMake(5, 10, 1, 4) , "res_ui/zhezhao70.png");
		m_spirte_fontBg->setPreferredSize(CCSize(47, 14));
		m_spirte_fontBg->setAnchorPoint(ccp(0.5f, 0.5f));
		m_spirte_fontBg->setPosition(m_label_active_text->getPosition());


		// 临时CCLayer，用于使用其定时器
		CCLayer * tmpLayer = CCLayer::create();
		tmpLayer->setTouchEnabled(true);
		tmpLayer->setPosition(ccp(0, 0));
		tmpLayer->setContentSize(winSize);
		tmpLayer->schedule(schedule_selector(ActiveIconWidget::update), 1.0f);

		this->addCCNode(tmpLayer);
		this->addChild(m_btn_activeIcon);

		this->setUpdateEnabled(true);
		this->setTouchEnable(true);
		this->setSize(m_btn_activeIcon->getContentSize());

		

		return true;
	}
	return false;
}

void ActiveIconWidget::callBackActive( CCObject *obj )
{
	//add by yangjun 2014.10.13
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		if (mainScene->isHeadMenuOn)
		{
			mainScene->ButtonHeadEvent(NULL);
		}
	}

	// 初始化 UI WINDOW数据
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

	ActiveUI* pTmpActiveUI = (ActiveUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveUI);
	if (NULL == pTmpActiveUI)
	{
		ActiveUI * pActiveUI = ActiveUI::create();
		pActiveUI->ignoreAnchorPointForPosition(false);
		pActiveUI->setAnchorPoint(ccp(0.5f, 0.5f));
		pActiveUI->setPosition(ccp(winSize.width / 2, winSize.height / 2));
		pActiveUI->setTag(kTagActiveUI);

		GameView::getInstance()->getMainUIScene()->addChild(pActiveUI);

		pActiveUI->initActiveUIData();

		// 请求 活跃度商店数据（当活跃度商店数据不为空时，说明已经请求过服务器数据，那么不再发送请求）
		if (ActiveShopData::instance()->m_vector_activeShopSource.empty())
		{
			// 请求服务器领取奖品
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5114, NULL);
		}
	}

	GuideMap * guideMap =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
	if (guideMap)
	{
		Script* sc = ScriptManager::getInstance()->getScriptById(guideMap->mTutorialScriptInstanceId);
		if(sc != NULL)
			sc->endCommand(this);
	}
}

bool ActiveIconWidget::IsNeedParticle()
{
	bool bFlag = false;

	for (unsigned int i = 0; i < ActiveData::instance()->m_vector_internet_label.size(); i++)
	{
		// 当前用户等级
		int nPlayLevel = GameView::getInstance()->myplayer->getActiveRole()->level();

		if (nPlayLevel >= ActiveData::instance()->m_vector_native_activeLabel.at(i)->m_activeLabel->levelforeshow())
		{
			CActiveLabel * activeLabel = ActiveData::instance()->m_vector_internet_label.at(i);

			int nHasGetActivity = activeLabel->partakeactivealready();
			int nAllActivity = activeLabel->partakeactiveall();

			int nColor = activeLabel->color();
			//颜色 正常0，活动1
			if (1 == nColor && nHasGetActivity != nAllActivity)
			{
				bFlag = true;

				return bFlag;
			}
		}
	}

	return bFlag;
}

void ActiveIconWidget::update( float dt )
{
	if (this->IsNeedParticle())
	{
		CCTutorialParticle* tutorialParticle = (CCTutorialParticle*)this->getRenderer()->getChildByTag(TAG_TUTORIALPARTICLE_ACTIVEICON);
		if (NULL == tutorialParticle)
		{
			//m_tutorialParticle = CCTutorialParticle::create("tuowei0.plist", m_btn_onlineGiftIcon->getContentSize().width, m_btn_onlineGiftIcon->getContentSize().height + 10);
			//m_tutorialParticle->setPosition(ccp(m_btn_onlineGiftIcon->getPosition().x, m_btn_onlineGiftIcon->getPosition().y - 35));
			m_tutorialParticle = CCTutorialParticle::create("tuowei0.plist", 30, 46);
			m_tutorialParticle->setPosition(ccp(m_btn_activeIcon->getPosition().x, m_btn_activeIcon->getPosition().y - 25));
			m_tutorialParticle->setAnchorPoint(ccp(0.5f, 0.5f));
			m_tutorialParticle->setTag(TAG_TUTORIALPARTICLE_ACTIVEICON);
			this->addCCNode(m_tutorialParticle);
		}
	}
	else
	{
		CCTutorialParticle* tutorialParticle = (CCTutorialParticle*)this->getRenderer()->getChildByTag(TAG_TUTORIALPARTICLE_ACTIVEICON);
		if (NULL != tutorialParticle)
		{
			m_tutorialParticle->removeFromParent();
		}
	}
}
