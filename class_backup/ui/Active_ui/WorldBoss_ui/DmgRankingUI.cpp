#include "DmgRankingUI.h"
#include "WorldBossData.h"
#include "../../../messageclient/element/CBoss.h"
#include "../../../messageclient/element/CRankingPrize.h"
#include "AppMacros.h"
#include "../../../messageclient/element/GoodsInfo.h"
#include "../../../ui/vip_ui/VipRewardCellItem.h"
#include "../../../messageclient/element/CDeadHistory.h"
#include "../../../gamescene_state/sceneelement/TargetInfo.h"
#include "../../../GameView.h"
#include "../../../gamescene_state/GameSceneState.h"
#include "../../../utils/StaticDataManager.h"
#include "../../../gamescene_state/MainScene.h"
#include "../../../utils/GameConfig.h"
#include "../../../gamescene_state/role/BasePlayer.h"

#define  SelectImageTag 458

DmgRankingUI::DmgRankingUI():
curBossIdx(0),
curRankIdx(0),
lastSelectCellId(0),
selectCellId(0)
{
}


DmgRankingUI::~DmgRankingUI()
{
}

DmgRankingUI * DmgRankingUI::create(int bossIdx,int rankIdx)
{
	DmgRankingUI * dmgRankingUI = new DmgRankingUI();
	if (dmgRankingUI && dmgRankingUI->init(bossIdx,rankIdx))
	{
		dmgRankingUI->autorelease();
		return dmgRankingUI;
	}
	CC_SAFE_DELETE(dmgRankingUI);
	return NULL;
}

bool DmgRankingUI::init(int bossIdx,int rankIdx)
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		this->scheduleUpdate();

		curBossIdx = bossIdx;
		curRankIdx = rankIdx;

		UILayer * u_layer = UILayer::create();
		addChild(u_layer);

		//加载UI
		UIPanel * ppanel = (UIPanel *)GUIReader::shareReader()->widgetFromJsonFile("res_ui/worldboss_1.json");
		ppanel->setAnchorPoint(ccp(0.0f,0.0f));
		ppanel->getValidNode()->setContentSize(CCSizeMake(608, 417));
		ppanel->setPosition(CCPointZero);	
		ppanel->setTouchEnable(true);
		m_pUiLayer->addWidget(ppanel);

		UIButton * btn_close = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		btn_close->setTouchEnable( true );
		btn_close->setPressedActionEnabled(true);
		btn_close->addReleaseEvent(this,coco_releaseselector(DmgRankingUI::CloseEvent));

		UIPanel * panel_rewardDes = (UIPanel*)UIHelper::seekWidgetByName(ppanel,"Panel_introduction");
		panel_rewardDes->setVisible(false);
		UIPanel * panel_getReward = (UIPanel*)UIHelper::seekWidgetByName(ppanel,"Panel_introduction_0");
		panel_getReward->setVisible(false);
		UIPanel * panel_ranking = (UIPanel*)UIHelper::seekWidgetByName(ppanel,"Panel_ranking");
		panel_ranking->setVisible(true);

		UILabel * l_name = (UILabel*)UIHelper::seekWidgetByName(panel_ranking,"Label_bossNameValue");
		l_name->setText(WorldBossData::getInstance()->m_worldBossDataList.at(bossIdx)->name().c_str());
		UILabel * l_lv = (UILabel*)UIHelper::seekWidgetByName(panel_ranking,"Label_bossLvValue");
		std::string str_lv = "LV";
		char s_lv[10];
		sprintf(s_lv,"%d",WorldBossData::getInstance()->m_worldBossDataList.at(bossIdx)->level());
		str_lv.append(s_lv);
		l_lv->setText(str_lv.c_str());
		UILabel * l_date = (UILabel*)UIHelper::seekWidgetByName(panel_ranking,"Label_dateValue");
		l_date->setText(WorldBossData::getInstance()->m_worldBossDataList.at(bossIdx)->deadhistory(rankIdx).data().c_str());
		UILabel * l_time = (UILabel*)UIHelper::seekWidgetByName(panel_ranking,"Label_dateValue_0");
		l_time->setText(WorldBossData::getInstance()->m_worldBossDataList.at(bossIdx)->deadhistory(rankIdx).time().c_str());

		UILabel * l_roleRanking = (UILabel*)UIHelper::seekWidgetByName(panel_ranking,"Label_roleRank");
		if (WorldBossData::getInstance()->m_worldBossDataList.at(bossIdx)->deadhistory(rankIdx).playerranking() > 0)
		{
			char s_roleRanking[10];
			sprintf(s_roleRanking,"%d",WorldBossData::getInstance()->m_worldBossDataList.at(bossIdx)->deadhistory(rankIdx).playerranking());
			l_roleRanking->setText(s_roleRanking);
		}
		else
		{
			l_roleRanking->setText(StringDataManager::getString("WorldBossUI_ranking_none"));
		}

		CCTableView *m_tableView = CCTableView::create(this, CCSizeMake(501,205));
		//m_tableView ->setSelectedEnable(true);   // 支持选中状态的显示
		//m_tableView ->setSelectedScale9Texture("res_ui/highlight.png", CCRectMake(25, 25, 1, 1), ccp(0,0));   // 设置九宫格图片
		m_tableView->setDirection(kCCScrollViewDirectionVertical);
		m_tableView->setAnchorPoint(ccp(0, 0));
		m_tableView->setPosition(ccp(52,69));
		m_tableView->setDelegate(this);
		m_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		m_tableView->setPressedActionEnabled(false);
		u_layer->addChild(m_tableView);

		this->setContentSize(CCSizeMake(608,417));
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);

		return true;
	}
	return false;
}

void DmgRankingUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void DmgRankingUI::onExit()
{
	UIScene::onExit();
}

bool DmgRankingUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return true;
}

void DmgRankingUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void DmgRankingUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void DmgRankingUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void DmgRankingUI::CloseEvent( CCObject * pSender )
{
	this->closeAnim();
}

void DmgRankingUI::scrollViewDidScroll( CCScrollView* view )
{

}

void DmgRankingUI::scrollViewDidZoom( CCScrollView* view )
{

}

void DmgRankingUI::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

	selectCellId = cell->getIdx();
	//取出上次选中状态，更新本次选中状态
	if(table->cellAtIndex(lastSelectCellId))
	{
		if (table->cellAtIndex(lastSelectCellId)->getChildByTag(SelectImageTag))
		{
			table->cellAtIndex(lastSelectCellId)->getChildByTag(SelectImageTag)->setVisible(false);
		}
	}

	if (cell->getChildByTag(SelectImageTag))
	{
		cell->getChildByTag(SelectImageTag)->setVisible(true);
	}

	lastSelectCellId = cell->getIdx();

	CDeadHistory * deadHistory = new CDeadHistory();
	deadHistory->CopyFrom(WorldBossData::getInstance()->m_worldBossDataList.at(curBossIdx)->deadhistory(curRankIdx));

	TargetInfoList * list = (TargetInfoList*)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagMainSceneTargetInfo);
	if (list != NULL)
	{
		list->removeFromParentAndCleanup(true);
	}

	std::string pressionStr = BasePlayer::getProfessionNameIdxByIndex(deadHistory->ranking(cell->getIdx()).player().profession());
	int pressionId = BasePlayer::getProfessionIdxByName(pressionStr.c_str());

	TargetInfoList * tarlist = TargetInfoList::create(deadHistory->ranking(cell->getIdx()).player().roleid(), 
										deadHistory->ranking(cell->getIdx()).player().name().c_str(),
										deadHistory->ranking(cell->getIdx()).player().countryid(),
										deadHistory->ranking(cell->getIdx()).player().viplevel(),
										deadHistory->ranking(cell->getIdx()).player().level(),
										pressionId);
	tarlist->ignoreAnchorPointForPosition(false);
	tarlist->setAnchorPoint(ccp(0, 0));
	tarlist->setTag(ktagMainSceneTargetInfo);
	//tarlist->setPosition(ccp(finalPositionX, finalPositionY));
	tarlist->setPosition(ccp(winSize.width / 2 - tarlist->getContentSize().width/2, winSize.height / 2 + tarlist->getContentSize().height/2));
	GameView::getInstance()->getMainUIScene()->addChild(tarlist);

	delete deadHistory;
}

cocos2d::CCSize DmgRankingUI::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeMake(499,47);
}

cocos2d::extension::CCTableViewCell* DmgRankingUI::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell = new CCTableViewCell();
	cell->autorelease();

	CCScale9Sprite* pHighlightSpr = CCScale9Sprite::create(CCRectMake(5, 5, 1, 1) , "res_ui/mengban_green.png");
	pHighlightSpr->setPreferredSize(CCSize(499,45));
	pHighlightSpr->setAnchorPoint(CCPointZero);
	pHighlightSpr->setPosition(ccp(0,0));
	pHighlightSpr->setTag(SelectImageTag);
	cell->addChild(pHighlightSpr);
	pHighlightSpr->setVisible(false);

	CDeadHistory * deadHistory = new CDeadHistory();
	deadHistory->CopyFrom(WorldBossData::getInstance()->m_worldBossDataList.at(curBossIdx)->deadhistory(curRankIdx));
	ccColor3B color_green = ccc3(47,93,13);

	// 底图
	CCScale9Sprite * sprite_bigFrame = CCScale9Sprite::create("res_ui/LV3_dikuang.png");
	sprite_bigFrame->setAnchorPoint(ccp(0, 0));
	sprite_bigFrame->setPosition(ccp(0, 0));
	sprite_bigFrame->setCapInsets(CCRectMake(9,14,1,1));
	sprite_bigFrame->setPreferredSize(CCSizeMake(499,45));
	cell->addChild(sprite_bigFrame);

	if (deadHistory->ranking(idx).ranking() <= 3 )
	{
		std::string icon_path = "res_ui/rank";
		char s_ranking[10];
		if (deadHistory->ranking(idx).ranking() <= 0)
		{
			sprintf(s_ranking,"%d",1);
		}
		else
		{
			sprintf(s_ranking,"%d",deadHistory->ranking(idx).ranking());
		}
		icon_path.append(s_ranking);
		icon_path.append(".png");
		CCSprite * sp_ranking = CCSprite::create(icon_path.c_str());
		sp_ranking->setAnchorPoint(ccp(0.5f,0.5f));
		sp_ranking->setPosition(ccp(41,24));
		sp_ranking->setScale(0.95f);
		cell->addChild(sp_ranking);
	}
	else if (deadHistory->ranking(idx).ranking() > 3  && deadHistory->ranking(idx).ranking() <= 10)
	{
		char s_ranking[10];
		sprintf(s_ranking,"%d",deadHistory->ranking(idx).ranking());
		CCLabelBMFont * l_ranking = CCLabelBMFont::create(s_ranking,"res_ui/font/ziti_3.fnt");
		l_ranking->setAnchorPoint(ccp(0.5f,0.5f));
		l_ranking->setPosition(ccp(41,24));
		cell->addChild(l_ranking);
	}
	else
	{
		char s_ranking[10];
		sprintf(s_ranking,"%d",deadHistory->ranking(idx).ranking());
		CCLabelTTF * l_ranking = CCLabelTTF::create(s_ranking,APP_FONT_NAME,18);
		l_ranking->setAnchorPoint(ccp(0.5f,0.5f));
		l_ranking->setPosition(ccp(41,24));
		l_ranking->setColor(color_green);
		cell->addChild(l_ranking);
	}

	//Name
	CCLabelTTF * l_name = CCLabelTTF::create(deadHistory->ranking(idx).player().name().c_str(),APP_FONT_NAME,18);
	l_name->setAnchorPoint(ccp(0.0f,0.5f));
	l_name->setPosition(ccp(116,24));
	l_name->setColor(color_green);
	cell->addChild(l_name);

	int countryId = deadHistory->ranking(idx).player().countryid();
	if (countryId > 0 && countryId <6)
	{
		std::string countryIdName = "country_";
		char id[2];
		sprintf(id, "%d", countryId);
		countryIdName.append(id);
		std::string iconPathName = "res_ui/country_icon/";
		iconPathName.append(countryIdName);
		iconPathName.append(".png");
		CCSprite * countrySp_ = CCSprite::create(iconPathName.c_str());
		countrySp_->setAnchorPoint(ccp(1.0,0.5f));
		//countrySp_->setPosition(ccp(l_name->getPositionX() - l_name->getContentSize().width/2-1,l_name->getPositionY()));
		countrySp_->setPosition(ccp(l_name->getPositionX()-1,l_name->getPositionY()));
		countrySp_->setScale(0.7f);
		cell->addChild(countrySp_);
	}

	//vipInfo
	// vip开关控制
	bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
	if (vipEnabled)
	{
		if (deadHistory->ranking(idx).player().viplevel() > 0)
		{
			CCNode *pNode = MainScene::addVipInfoByLevelForNode(deadHistory->ranking(idx).player().viplevel());
			if (pNode)
			{
				cell->addChild(pNode);
				//pNode->setPosition(ccp(l_name->getPosition().x+l_name->getContentSize().width/2+13,l_name->getPosition().y));
				pNode->setPosition(ccp(l_name->getPosition().x+l_name->getContentSize().width+13,l_name->getPosition().y));
			}
		}
	}

	//Level
	char s_lv[20];
	sprintf(s_lv,"%d",deadHistory->ranking(idx).player().level());
	CCLabelTTF * l_level = CCLabelTTF::create(s_lv,APP_FONT_NAME,18);
	l_level->setAnchorPoint(ccp(0.5f,0.5f));
	l_level->setPosition(ccp(267,24));
	l_level->setColor(color_green);
	cell->addChild(l_level);
	//profession
	CCLabelTTF * l_profession = CCLabelTTF::create(BasePlayer::getProfessionNameIdxByIndex(deadHistory->ranking(idx).player().profession()).c_str(),APP_FONT_NAME,18);
	l_profession->setAnchorPoint(ccp(0.5f,0.5f));
	l_profession->setPosition(ccp(345,24));
	l_profession->setColor(color_green);
	cell->addChild(l_profession);
	//Dmg
	char s_dmg[20];
	sprintf(s_dmg,"%d",deadHistory->ranking(idx).hurt());
	CCLabelTTF * l_dmg = CCLabelTTF::create(s_dmg,APP_FONT_NAME,18);
	l_dmg->setAnchorPoint(ccp(0.5f,0.5f));
	l_dmg->setPosition(ccp(440,24));
	l_dmg->setColor(color_green);
	cell->addChild(l_dmg);

	if (this->selectCellId == idx)
	{
		pHighlightSpr->setVisible(true);
	}

	delete deadHistory;
	return cell;
}

unsigned int DmgRankingUI::numberOfCellsInTableView( CCTableView *table )
{
	if (WorldBossData::getInstance()->m_worldBossDataList.at(curBossIdx)->deadhistory(curRankIdx).ranking_size() <= 10)
	{
		return WorldBossData::getInstance()->m_worldBossDataList.at(curBossIdx)->deadhistory(curRankIdx).ranking_size();
	}
	else
	{
		return 10;
	}
}
