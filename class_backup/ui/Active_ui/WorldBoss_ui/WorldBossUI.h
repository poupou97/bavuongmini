#ifndef _UI_ACTIVE_WORLDBOSSUI_WORLDBOSSUI_H_
#define _UI_ACTIVE_WORLDBOSSUI_WORLDBOSSUI_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../../extensions/UIScene.h"
#include "KillNotesList.h"

USING_NS_CC;
USING_NS_CC_EXT;

/////////////////////////////////
/**
 * 世界BosssUI
 * @author yangjun 
 * @version 0.1.0
 * @date 2014.05.26
 */

class KillNotesList;
class CCRichLabel;

class WorldBossUI:public UIScene, public CCTableViewDataSource, public CCTableViewDelegate
{
public:
	WorldBossUI(void);
	~WorldBossUI(void);

public:
	static WorldBossUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);
private:
	UILayer * m_base_layer;																							// 基调layer
	CCTableView * m_tableView;																						// 主tableView
	CCScrollView * m_contentScrollView;																		// 活动详情ScrollView
	CCRichLabel * l_des;

	int m_nTableViewSize;

	UILabel * l_name;
	UILabel * l_lv;
	UILabel * l_state; 
	UILabel * l_pos;

	UIButton * btn_challenge;
	UIImageView * imageView_wait;

private:
	//int curSelectBossIdx;
	KillNotesList * s_killNotesList;
private:
	void initUI();

public:
	void CloseEvent(CCObject *pSender);
	void getRewardEvent(CCObject * pSender);
	void rewardDesEvent(CCObject * pSender);
	void challengeEvent(CCObject * pSender);

	void tabIndexChangedEvent(CCObject *pSender);

	void RefreshTableView();
	void RefreshBossInfo(int idx);

	void RefreshDesScrollView(int idx);

	void setToDefault();
};

#endif

