#include "WorldBossUI.h"
#include "../ActiveUI.h"
#include "../../../loadscene_state/LoadSceneState.h"
#include "SimpleAudioEngine.h"
#include "../../../GameAudio.h"
#include "../../../utils/StaticDataManager.h"
#include "../../../AppMacros.h"
#include "GameView.h"
#include "../../extensions/CCMoveableMenu.h"
#include "../../../gamescene_state/GameSceneState.h"
#include "../../../gamescene_state/MainScene.h"
#include "../../../gamescene_state/role/MyPlayer.h"
#include "../../../utils/GameUtils.h"
#include "../../../messageclient/element/CBoss.h"
#include "WorldBossData.h"
#include "RewardDesUI.h"
#include "GetRewardUI.h"
#include "../../extensions/CCRichLabel.h"
#include "KillNotesList.h"
#include "../../../gamescene_state/role/MyPlayerOwnedCommand.h"
#include "../../../messageclient/element/CMapInfo.h"
#include "../../extensions/UITab.h"

#define  TAG_CONTENTSSCROLLVIEW 50

WorldBossUI::WorldBossUI(void)
{
	WorldBossData::getInstance()->m_nCurSelectBossIdx = -1;
}


WorldBossUI::~WorldBossUI(void)
{
}

WorldBossUI* WorldBossUI::create()
{
	WorldBossUI * worldBossUI = new WorldBossUI();
	if (worldBossUI && worldBossUI->init())
	{
		worldBossUI->autorelease();
		return worldBossUI;
	}
	CC_SAFE_DELETE(worldBossUI);
	return NULL;
}

bool WorldBossUI::init()
{
	if (UIScene::init())
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

		m_base_layer = UILayer::create();
		m_base_layer->ignoreAnchorPointForPosition(false);
		m_base_layer->setAnchorPoint(ccp(.5f,.5f));
		m_base_layer->setPosition(ccp(winSize.width/2,winSize.height/2));
		m_base_layer->setContentSize(CCSizeMake(800,480));
		this->addChild(m_base_layer);	

		initUI();

// 		this->setTouchEnabled(false);
// 		this->setTouchMode(kCCTouchesOneByOne);
// 		this->setContentSize(winSize);
		return true;
	}
	return false;
}

void WorldBossUI::onEnter()
{
	UIScene::onEnter();
}

void WorldBossUI::onExit()
{
	UIScene::onExit();
}

void WorldBossUI::initUI()
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

	if (!GameView::getInstance()->getMainUIScene())
		return;

	UIImageView *mengban = UIImageView::create();
	mengban->setTexture("res_ui/zhezhao80.png");
	mengban->setScale9Enable(true);
	mengban->setScale9Size(winSize);
	mengban->setAnchorPoint(CCPointZero);
	mengban->setPosition(ccp(0,0));
	m_pUiLayer->addWidget(mengban);

	UIPanel *s_panel_worldBossUI = (UIPanel*)GUIReader::shareReader()->widgetFromJsonFile("res_ui/boss_1.json");
	s_panel_worldBossUI->setAnchorPoint(ccp(0.5f, 0.5f));
	s_panel_worldBossUI->setPosition(ccp(winSize.width / 2, winSize.height / 2));
	s_panel_worldBossUI->setScale(1.0f);
	s_panel_worldBossUI->setTouchEnable(true);
	m_pUiLayer->addWidget(s_panel_worldBossUI);

	UIButton * btn_close = (UIButton*)UIHelper::seekWidgetByName(s_panel_worldBossUI,"Button_close");
	btn_close->setTouchEnable(true);
	btn_close->setPressedActionEnabled(true);
	btn_close->addReleaseEvent(this, coco_releaseselector(WorldBossUI::CloseEvent));

	UIButton * btn_getReward = (UIButton*)UIHelper::seekWidgetByName(s_panel_worldBossUI,"Button_getReward");
	btn_getReward->setTouchEnable(true);
	btn_getReward->setPressedActionEnabled(true);
	btn_getReward->addReleaseEvent(this, coco_releaseselector(WorldBossUI::getRewardEvent));

	UIButton * btn_rewardDes = (UIButton*)UIHelper::seekWidgetByName(s_panel_worldBossUI,"Button_introduction");
	btn_rewardDes->setTouchEnable(true);
	btn_rewardDes->setPressedActionEnabled(true);
	btn_rewardDes->addReleaseEvent(this, coco_releaseselector(WorldBossUI::rewardDesEvent));

	btn_challenge = (UIButton*)UIHelper::seekWidgetByName(s_panel_worldBossUI,"Button_challenge");
	btn_challenge->setTouchEnable(true);
	btn_challenge->setPressedActionEnabled(true);
	btn_challenge->addReleaseEvent(this, coco_releaseselector(WorldBossUI::challengeEvent));
	btn_challenge->setVisible(false);

	imageView_wait = (UIImageView*)UIHelper::seekWidgetByName(s_panel_worldBossUI,"ImageView_waitForRefresh");
	imageView_wait->setVisible(false);

	l_name = (UILabel*)UIHelper::seekWidgetByName(s_panel_worldBossUI,"Label_bossNameValue");
	l_lv = (UILabel*)UIHelper::seekWidgetByName(s_panel_worldBossUI,"Label_bossLvValue");
	l_state = (UILabel*)UIHelper::seekWidgetByName(s_panel_worldBossUI,"Label_bossValue");
	l_pos = (UILabel*)UIHelper::seekWidgetByName(s_panel_worldBossUI,"Label_bossPlaceValue");

	// tab
// 	const char * normalImage = "res_ui/tab_3_off.png";
// 	const char * selectImage = "res_ui/tab_3_on.png";
// 	const char * finalImage = "";
// 	const char * highLightImage = "res_ui/tab_3_on.png";
// 	const char *str1 = StringDataManager::getString("WorldBossUI_uitab_boss");
// 	char* p1 = const_cast<char*>(str1);
// 	char* pHightLightImage = const_cast<char*>(highLightImage);
// 	char * mainNames[] = {p1};
// 	UITab *m_tab = UITab::createWithBMFont(1, normalImage, selectImage, finalImage, mainNames,"res_ui/font/ziti_1.fnt",HORIZONTAL,5);
// 	m_tab->setAnchorPoint(ccp(0,0));
// 	m_tab->setPosition(ccp(74, 410));
// 	m_tab->setHighLightImage(pHightLightImage);
// 	m_tab->setDefaultPanelByIndex(0);
// 	m_tab->addIndexChangedEvent(this, coco_indexchangedselector(WorldBossUI::tabIndexChangedEvent));
// 	m_tab->setPressedActionEnabled(true);
// 	m_base_layer->addWidget(m_tab);
	
	// ��tableView
	m_tableView = CCTableView::create(this, CCSizeMake(245,289));
	m_tableView ->setSelectedEnable(true);   // ֧��ѡ��״̬����ʾ
	m_tableView ->setSelectedScale9Texture("res_ui/highlight.png", CCRectMake(25, 25, 1, 1), ccp(0,0));   // ���þŹ���ͼƬ
	m_tableView->setDirection(kCCScrollViewDirectionVertical);
	m_tableView->setAnchorPoint(ccp(0, 0));
	m_tableView->setPosition(ccp(71,118));
	m_tableView->setDelegate(this);
	m_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
	m_tableView->setPressedActionEnabled(false);
	m_base_layer->addChild(m_tableView);

	int scroll_height = 0;
	//����
	l_des = CCRichLabel::createWithString("",CCSizeMake(387,0),this,0,0,18);
	m_contentScrollView = CCScrollView::create(CCSizeMake(389,114));
	m_contentScrollView->setViewSize(CCSizeMake(389, 114));
	m_contentScrollView->ignoreAnchorPointForPosition(false);
	m_contentScrollView->setTouchEnabled(true);
	m_contentScrollView->setDirection(kCCScrollViewDirectionVertical);
	m_contentScrollView->setAnchorPoint(ccp(0,0));
	m_contentScrollView->setPosition(ccp(350,223));
	m_contentScrollView->setBounceable(true);
	m_contentScrollView->setClippingToBounds(true);
	m_base_layer->addChild(m_contentScrollView);
	m_contentScrollView->addChild(l_des);

	s_killNotesList = KillNotesList::create();
	m_base_layer->addChild(s_killNotesList);

	if (WorldBossData::getInstance()->m_worldBossDataList.size()>0)
	{
		WorldBossData::getInstance()->m_nCurSelectBossIdx = 0;
		RefreshBossInfo(0);
		RefreshDesScrollView(0);
		s_killNotesList->refreshDataSourceByIdx(0);
		s_killNotesList->refreshUI();
	}

	this->setTouchEnabled(false);
	this->setTouchMode(kCCTouchesOneByOne);
	this->setContentSize(winSize);
}


bool WorldBossUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return false;
}

void WorldBossUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void WorldBossUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void WorldBossUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void WorldBossUI::scrollViewDidScroll( CCScrollView* view )
{

}

void WorldBossUI::scrollViewDidZoom( CCScrollView* view )
{

}

void WorldBossUI::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{
	WorldBossData::getInstance()->m_nCurSelectBossIdx = cell->getIdx();
	RefreshBossInfo(WorldBossData::getInstance()->m_nCurSelectBossIdx);
	RefreshDesScrollView(WorldBossData::getInstance()->m_nCurSelectBossIdx);
	s_killNotesList->refreshDataSourceByIdx(WorldBossData::getInstance()->m_nCurSelectBossIdx);
	s_killNotesList->refreshUI();
}

cocos2d::CCSize WorldBossUI::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeMake(238,53);
}

cocos2d::extension::CCTableViewCell* WorldBossUI::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCString *string = CCString::createWithFormat("%d", idx);
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell = new CCTableViewCell();
	cell->autorelease();

	CBoss * bossData =	WorldBossData::getInstance()->m_worldBossDataList.at(idx);

	// ��ͼ
	CCScale9Sprite * sprite_frame = CCScale9Sprite::create("res_ui/kuang0_new.png");
	sprite_frame->setContentSize(CCSizeMake(233, 51));
	sprite_frame->setCapInsets(CCRectMake(18, 9, 2, 23));
	sprite_frame->setAnchorPoint(CCPointZero);
	sprite_frame->setPosition(ccp(2, 0));
	cell->addChild(sprite_frame);

	// ����
	CCLabelTTF * l_name = CCLabelTTF::create(bossData->name().c_str(), APP_FONT_NAME, 18, CCSizeMake(200, 0), kCCTextAlignmentLeft);
	l_name->setColor(ccc3(255, 255, 255));
	ccColor3B color = ccc3(0, 0, 0);
	l_name->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
	l_name->setAnchorPoint(ccp(0.5f, 0.5f));
	l_name->setHorizontalAlignment(kCCTextAlignmentCenter);
	l_name->setPosition(ccp(61, 26));
	cell->addChild(l_name);

	// �ȼ�
	char str_lv[10];
	//test
	//sprintf(str_lv, "%d", 20);
	sprintf(str_lv, "%d", bossData->level());
	CCLabelTTF * l_lv = CCLabelTTF::create(str_lv, APP_FONT_NAME, 18, CCSizeMake(200, 0), kCCTextAlignmentLeft);
	l_lv->setColor(ccc3(255, 255, 255));
	l_lv->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
	l_lv->setAnchorPoint(ccp(0.5f, 0.5f));
	l_lv->setHorizontalAlignment(kCCTextAlignmentCenter);
	l_lv->setPosition(ccp(140, 26));
	cell->addChild(l_lv);

	//״̬
	CCLabelTTF * l_status = CCLabelTTF::create("", APP_FONT_NAME, 18, CCSizeMake(200,0), kCCTextAlignmentLeft);
	l_status->setColor(ccc3(255, 255, 255));
	l_status->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
	l_status->setAnchorPoint(ccp(0.5f, 0.5f));
	l_status->setHorizontalAlignment(kCCTextAlignmentCenter);
	l_status->setPosition(ccp(200, 26));
	cell->addChild(l_status);

	switch(bossData->state())
	{
	case DEAD:
		{
			l_status->setString(StringDataManager::getString("WorldBossUI_State_dead"));
		}
		break;
	case ALIVE:
		{
			l_status->setString(StringDataManager::getString("WorldBossUI_State_alive"));
		}
		break;
	}

	return cell;
}

unsigned int WorldBossUI::numberOfCellsInTableView( CCTableView *table )
{
	return WorldBossData::getInstance()->m_worldBossDataList.size();
}

void WorldBossUI::getRewardEvent( CCObject * pSender )
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGetRewardUI) == NULL)
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		//GetRewardUI * getRewardUI = GetRewardUI::create(WorldBossData::getInstance()->m_nCurSelectBossIdx);
		GetRewardUI * getRewardUI = GetRewardUI::create();
		getRewardUI->ignoreAnchorPointForPosition(false);
		getRewardUI->setAnchorPoint(ccp(.5f,.5f));
		getRewardUI->setPosition(ccp(winSize.width/2,winSize.height/2));
		getRewardUI->setTag(kTagGetRewardUI);
		GameView::getInstance()->getMainUIScene()->addChild(getRewardUI);
	}
}

void WorldBossUI::rewardDesEvent( CCObject * pSender )
{
	if (WorldBossData::getInstance()->m_nCurSelectBossIdx < 0)
		return;

	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRewardDesUI) == NULL)
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		RewardDesUI * rewardDesUI = RewardDesUI::create(WorldBossData::getInstance()->m_nCurSelectBossIdx);
		rewardDesUI->ignoreAnchorPointForPosition(false);
		rewardDesUI->setAnchorPoint(ccp(.5f,.5f));
		rewardDesUI->setPosition(ccp(winSize.width/2,winSize.height/2));
		rewardDesUI->setTag(kTagRewardDesUI);
		GameView::getInstance()->getMainUIScene()->addChild(rewardDesUI);
	}
}

void WorldBossUI::challengeEvent( CCObject * pSender )
{
	if (WorldBossData::getInstance()->m_nCurSelectBossIdx < 0)
		return;

	CBoss * tempData = WorldBossData::getInstance()->m_worldBossDataList.at(WorldBossData::getInstance()->m_nCurSelectBossIdx);

	CCPoint targetPos = ccp(tempData->pos().x(),tempData->pos().y());
 	std::string targetMapId = tempData->pos().mapid();
// 	CCPoint targetPos;
// 	if (pos.x == 0 && pos.y == 0)
// 	{
// 		targetPos = ccp(0,0);
// 	}
// 	else
// 	{
// 		float x = GameView::getInstance()->getGameScene()->tileToPositionX(pos.x);
// 		float y = GameView::getInstance()->getGameScene()->tileToPositionY(pos.y);
// 		targetPos = ccp(x,y);
// 	}


	if (strcmp(GameView::getInstance()->getMapInfo()->mapid().c_str(),targetMapId.c_str()) == 0)
	{
		//same map
		CCPoint cp_role = GameView::getInstance()->myplayer->getWorldPosition();
		CCPoint cp_target = targetPos;
		if (ccpDistance(cp_role,cp_target)< 64)
		{
			return;
		}
		else
		{
			if (targetPos.x == 0 && targetPos.y == 0)
			{
				return;
			}

			MyPlayerCommandMove* cmd = new MyPlayerCommandMove();
			// NOT reachable, so find the near one
			short tileX = GameView::getInstance()->getGameScene()->positionToTileX(targetPos.x);
			short tileY = GameView::getInstance()->getGameScene()->positionToTileY(targetPos.y);
			cmd->targetPosition = targetPos;
			cmd->method = MyPlayerCommandMove::method_searchpath;
			bool bSwitchCommandImmediately = true;
			if(GameView::getInstance()->myplayer->isAttacking())
				bSwitchCommandImmediately = false;
			GameView::getInstance()->myplayer->setNextCommand(cmd, bSwitchCommandImmediately);	
		}
	}
	else
	{
		MyPlayerCommandMove* cmd = new MyPlayerCommandMove();
		cmd->targetMapId = targetMapId;
		if (targetPos.x == 0 && targetPos.y == 0)
		{
		}
		else
		{
			cmd->targetPosition = targetPos;
		}
		cmd->method = MyPlayerCommandMove::method_searchpath;
		bool bSwitchCommandImmediately = true;
		if(GameView::getInstance()->myplayer->isAttacking())
			bSwitchCommandImmediately = false;
		GameView::getInstance()->myplayer->setNextCommand(cmd, bSwitchCommandImmediately);	
	}

	this->closeAnim();
}

void WorldBossUI::RefreshTableView()
{
	this->m_tableView->reloadData();
	this->setToDefault();
}

void WorldBossUI::RefreshBossInfo( int idx )
{
	CBoss * tempData = WorldBossData::getInstance()->m_worldBossDataList.at(idx);

	l_name->setText(tempData->name().c_str());
	std::string str_lv = "LV";
	char s_lv[10];
	sprintf(s_lv,"%d",tempData->level());
	str_lv.append(s_lv);
	l_lv->setText(str_lv.c_str());

	switch(tempData->state())
	{
		case ALIVE:
			{
				l_state->setText(StringDataManager::getString("WorldBossUI_State_alive"));
				
				int countryId = tempData->country();
				if(countryId == 0)   // common map
				{
					l_pos->setText(tempData->pos().mapname().c_str());
				}
				else
				{
					std::string full_map_name = CMapInfo::getCountryName(countryId);
					full_map_name.append(StringDataManager::getString("country_separator"));
					full_map_name.append(tempData->pos().mapname().c_str());
					l_pos->setText(full_map_name.c_str());
				}

				btn_challenge->setVisible(true);
				imageView_wait->setVisible(false);
			}
			break;
		case DEAD:
			{
				l_state->setText(StringDataManager::getString("WorldBossUI_State_dead"));
				l_pos->setText(StringDataManager::getString("activy_none"));
				btn_challenge->setVisible(false);
				imageView_wait->setVisible(true);
			}
			break;
	}
	
	
}

void WorldBossUI::RefreshDesScrollView( int idx )
{
	if (idx >= WorldBossData::getInstance()->m_worldBossDataList.size())
		return;

	CBoss * tempData = WorldBossData::getInstance()->m_worldBossDataList.at(idx);
	
	l_des->setString(tempData->descr().c_str());
	int zeroLineHeight = l_des->getContentSize().height+10;
	int scroll_height = zeroLineHeight;
	 
	if (scroll_height > 114)
	{
	 	m_contentScrollView->setContentSize(CCSizeMake(389,scroll_height));
	 	m_contentScrollView->setContentOffset(ccp(0,114-scroll_height));  
	}
	else
	{
	 	m_contentScrollView->setContentSize(ccp(389,114));
	}
	l_des->setPosition(ccp(2,m_contentScrollView->getContentSize().height - zeroLineHeight)); 	
}

void WorldBossUI::setToDefault()
{
	if (WorldBossData::getInstance()->m_worldBossDataList.size() > 0)
	{
		m_tableView->selectCell(0);
		RefreshBossInfo(0);
		RefreshDesScrollView(0);
	}
}

void WorldBossUI::CloseEvent( CCObject *pSender )
{
	this->closeAnim();
}

void WorldBossUI::tabIndexChangedEvent( CCObject *pSender )
{
/*	this->setToDefault();*/
}
