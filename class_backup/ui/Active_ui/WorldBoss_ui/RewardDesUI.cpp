#include "RewardDesUI.h"
#include "WorldBossData.h"
#include "../../../messageclient/element/CBoss.h"
#include "../../../messageclient/element/CRankingPrize.h"
#include "AppMacros.h"
#include "../../../messageclient/element/GoodsInfo.h"
#include "../../../ui/vip_ui/VipRewardCellItem.h"

RewardDesUI::RewardDesUI()
{
}


RewardDesUI::~RewardDesUI()
{
	std::vector<CRankingPrize*>::iterator _iter;
	for (_iter = m_rewardDesList.begin(); _iter != m_rewardDesList.end(); ++_iter)
	{
		delete *_iter;
	}
	m_rewardDesList.clear();
}

RewardDesUI * RewardDesUI::create(int idx)
{
	RewardDesUI * rewardDesUI = new RewardDesUI();
	if (rewardDesUI && rewardDesUI->init(idx))
	{
		rewardDesUI->autorelease();
		return rewardDesUI;
	}
	CC_SAFE_DELETE(rewardDesUI);
	return NULL;
}

bool RewardDesUI::init(int idx)
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		this->scheduleUpdate();

		UILayer * u_layer = UILayer::create();
		addChild(u_layer);

		//加载UI
		UIPanel * ppanel = (UIPanel *)GUIReader::shareReader()->widgetFromJsonFile("res_ui/worldboss_1.json");
		ppanel->setAnchorPoint(ccp(0.0f,0.0f));
		ppanel->getValidNode()->setContentSize(CCSizeMake(608, 417));
		ppanel->setPosition(CCPointZero);	
		ppanel->setTouchEnable(true);
		m_pUiLayer->addWidget(ppanel);

		UIButton * btn_close = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		btn_close->setTouchEnable( true );
		btn_close->setPressedActionEnabled(true);
		btn_close->addReleaseEvent(this,coco_releaseselector(RewardDesUI::CloseEvent));

		UIPanel * panel_rewardDes = (UIPanel*)UIHelper::seekWidgetByName(ppanel,"Panel_introduction");
		panel_rewardDes->setVisible(true);
		UIPanel * panel_getReward = (UIPanel*)UIHelper::seekWidgetByName(ppanel,"Panel_introduction_0");
		panel_getReward->setVisible(false);
		UIPanel * panel_ranking = (UIPanel*)UIHelper::seekWidgetByName(ppanel,"Panel_ranking");
		panel_ranking->setVisible(false);

		UILabel * l_name = (UILabel*)UIHelper::seekWidgetByName(panel_rewardDes,"Label_bossNameValue_1");
		l_name->setText(WorldBossData::getInstance()->m_worldBossDataList.at(idx)->name().c_str());
		UILabel * l_lv = (UILabel*)UIHelper::seekWidgetByName(panel_rewardDes,"Label_bossLvValue_1");
		std::string str_lv = "LV";
		char s_lv[10];
		sprintf(s_lv,"%d",WorldBossData::getInstance()->m_worldBossDataList.at(idx)->level());
		str_lv.append(s_lv);
		l_lv->setText(str_lv.c_str());

		for (int i = 0;i<WorldBossData::getInstance()->m_worldBossDataList.at(idx)->prizes_size();++i)
		{
			CRankingPrize * temp = new CRankingPrize();
			temp->CopyFrom(WorldBossData::getInstance()->m_worldBossDataList.at(idx)->prizes(i));
			m_rewardDesList.push_back(temp);
		}

		CCTableView *m_tableView = CCTableView::create(this, CCSizeMake(501,225));
		//m_tableView ->setSelectedEnable(true);   // 支持选中状态的显示
		//m_tableView ->setSelectedScale9Texture("res_ui/highlight.png", CCRectMake(25, 25, 1, 1), ccp(0,0));   // 设置九宫格图片
		m_tableView->setDirection(kCCScrollViewDirectionVertical);
		m_tableView->setAnchorPoint(ccp(0, 0));
		m_tableView->setPosition(ccp(53,42));
		m_tableView->setDelegate(this);
		m_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		m_tableView->setPressedActionEnabled(false);
		u_layer->addChild(m_tableView);

		this->setContentSize(CCSizeMake(608,417));
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);

		return true;
	}
	return false;
}

void RewardDesUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void RewardDesUI::onExit()
{
	UIScene::onExit();
}

bool RewardDesUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return true;
}

void RewardDesUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void RewardDesUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void RewardDesUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void RewardDesUI::CloseEvent( CCObject * pSender )
{
	this->closeAnim();
}

void RewardDesUI::scrollViewDidScroll( CCScrollView* view )
{

}

void RewardDesUI::scrollViewDidZoom( CCScrollView* view )
{

}

void RewardDesUI::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{

}

cocos2d::CCSize RewardDesUI::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeMake(499,76);
}

cocos2d::extension::CCTableViewCell* RewardDesUI::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell = new CCTableViewCell();
	cell->autorelease();

	CRankingPrize * rankingPrize = m_rewardDesList.at(idx);
	ccColor3B color_green = ccc3(47,93,13);

	// 底图
	CCScale9Sprite * sprite_bigFrame = CCScale9Sprite::create("res_ui/LV3_dikuang.png");
	sprite_bigFrame->setAnchorPoint(ccp(0, 0));
	sprite_bigFrame->setPosition(ccp(0, 0));
	sprite_bigFrame->setCapInsets(CCRectMake(9,14,1,1));
	sprite_bigFrame->setPreferredSize(CCSizeMake(499,74));
	cell->addChild(sprite_bigFrame);

	if (rankingPrize->end() - rankingPrize->start() == 0)
	{
		if (rankingPrize->start() <= 3 )
		{
			std::string icon_path = "res_ui/rank";
			char s_ranking[10];
			sprintf(s_ranking,"%d",rankingPrize->start());
			icon_path.append(s_ranking);
			icon_path.append(".png");
			CCSprite * sp_ranking = CCSprite::create(icon_path.c_str());
			sp_ranking->setAnchorPoint(ccp(0.5f,0.5f));
			sp_ranking->setPosition(ccp(55,38));
			sp_ranking->setScale(0.95f);
			cell->addChild(sp_ranking);
		}
		else
		{
			char s_ranking[10];
			sprintf(s_ranking,"%d",rankingPrize->start());
			CCLabelTTF * l_ranking = CCLabelTTF::create(s_ranking,APP_FONT_NAME,18);
			l_ranking->setAnchorPoint(ccp(0.5f,0.5f));
			l_ranking->setPosition(ccp(55,38));
			l_ranking->setColor(color_green);
			cell->addChild(l_ranking);
		}
	}
	else
	{
		std::string str_ranking_des ;
		char s_ranking_start[10];
		sprintf(s_ranking_start,"%d",rankingPrize->start());
		char s_ranking_end[10];
		sprintf(s_ranking_end,"%d",rankingPrize->end());
		str_ranking_des.append(s_ranking_start);
		str_ranking_des.append("-");
		str_ranking_des.append(s_ranking_end);
		CCLabelTTF * l_ranking = CCLabelTTF::create(str_ranking_des.c_str(),APP_FONT_NAME,18);
		l_ranking->setAnchorPoint(ccp(0.5f,0.5f));
		l_ranking->setPosition(ccp(55,38));
		l_ranking->setColor(color_green);
		cell->addChild(l_ranking);
	}

	//EXP
	CCSprite * sp_exp_icon = CCSprite::create("res_ui/exp.png");
	sp_exp_icon->setAnchorPoint(ccp(0.5f,0.5f));
	sp_exp_icon->setPosition(ccp(140,49));
	cell->addChild(sp_exp_icon);
	char s_expValue[20];
	sprintf(s_expValue,"%d",rankingPrize->prize().exp());
	CCLabelTTF * l_expValue = CCLabelTTF::create(s_expValue,APP_FONT_NAME,14);
	l_expValue->setAnchorPoint(ccp(0.5f,0.5f));
	l_expValue->setPosition(ccp(203,49));
	l_expValue->setColor(color_green);
	cell->addChild(l_expValue);

	//Gold
	CCSprite * sp_gold_icon = CCSprite::create("res_ui/coins.png");
	sp_gold_icon->setAnchorPoint(ccp(0.5f,0.5f));
	sp_gold_icon->setPosition(ccp(140,22));
	cell->addChild(sp_gold_icon);
	char s_goldValue[20];
	sprintf(s_goldValue,"%d",rankingPrize->prize().gold());
	CCLabelTTF * l_goldValue = CCLabelTTF::create(s_goldValue,APP_FONT_NAME,14);
	l_goldValue->setAnchorPoint(ccp(0.5f,0.5f));
	l_goldValue->setPosition(ccp(203,22));
	l_goldValue->setColor(color_green);
	cell->addChild(l_goldValue);

	for(int j = 0;j<rankingPrize->prize().goods_size();++j)
	{
		if (j >= 3)
			continue;

		GoodsInfo * goodInfo = new GoodsInfo();
		goodInfo->CopyFrom(rankingPrize->prize().goods(j).goods());
		int num = 0;
		num = rankingPrize->prize().goods(j).quantity();

		VipRewardCellItem * goodsItem = VipRewardCellItem::create(goodInfo,num);
		goodsItem->ignoreAnchorPointForPosition(false);
		goodsItem->setAnchorPoint(ccp(0.5f,0.5f));
		goodsItem->setPosition(ccp(290+85*j,38));
		cell->addChild(goodsItem);

		delete goodInfo;
	}

	return cell;
}

unsigned int RewardDesUI::numberOfCellsInTableView( CCTableView *table )
{
	return m_rewardDesList.size();
}
