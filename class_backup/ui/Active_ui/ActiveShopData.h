#ifndef _UI_ACTIVE_ACTIVESHOPDATA_H_
#define _UI_ACTIVE_ACTIVESHOPDATA_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

/////////////////////////////////
/**
 * 活跃度商店模块（用于保存服务器数据）
 * @author liuliang
 * @version 0.1.0
 * @date 2014.03.28
 */
class CHonorCommodity;

class ActiveShopData
{
public:
	static ActiveShopData * s_activeShopData;
	static ActiveShopData * instance();

private:
	ActiveShopData(void);
	~ActiveShopData(void);

public:
	std::vector<CHonorCommodity *> m_vector_activeShopSource;
};

#endif

