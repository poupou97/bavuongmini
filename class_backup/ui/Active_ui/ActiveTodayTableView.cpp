#include "ActiveTodayTableView.h"
#include "../../messageclient/element/CActiveLabelToday.h"
#include "ActiveData.h"
#include "AppMacros.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "ActiveUI.h"
#include "../../gamescene_state/MainScene.h"
#include "GameView.h"
#include "../../ui/extensions/CCMoveableMenu.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../ui/backpackscene/GoodsItemInfoBase.h"


const CCSize CCSizeDataCellSize = CCSizeMake(510, 37);

const CCSize CCSizeCellSize = CCSizeMake(183, 57);

#define PAGE_NUM 10

ActiveTodayTableView * ActiveTodayTableView::s_activeToadyTableView = NULL;

ActiveTodayTableView * ActiveTodayTableView::instance()
{
	if (NULL == s_activeToadyTableView)
	{
		s_activeToadyTableView = new ActiveTodayTableView();
	}

	return s_activeToadyTableView;
}

ActiveTodayTableView::ActiveTodayTableView( void )
{

}

ActiveTodayTableView::~ActiveTodayTableView( void )
{

}

void ActiveTodayTableView::scrollViewDidScroll( CCScrollView* view )
{

}

void ActiveTodayTableView::scrollViewDidZoom( CCScrollView* view )
{

}

void ActiveTodayTableView::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{

}

cocos2d::CCSize ActiveTodayTableView::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeCellSize;
}

cocos2d::extension::CCTableViewCell* ActiveTodayTableView::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell = new CCTableViewCell();
	cell->autorelease();

	initCell(cell, idx);

	return cell;
}

unsigned int ActiveTodayTableView::numberOfCellsInTableView( CCTableView *table )
{
	return m_vector_labelToday.size();
}

void ActiveTodayTableView::initDataFromInternet()
{
	// 清空数据
	std::vector<CActiveLabelToday *>::iterator iter;
	for (iter = m_vector_labelToday.begin(); iter != m_vector_labelToday.end(); iter++)
	{
		delete *iter;
	}
	m_vector_labelToday.clear();

	for (unsigned int i = 0; i < ActiveData::instance()->m_vector_internet_labelToday.size(); i ++)
	{
		CActiveLabelToday * activeLabelToday = new CActiveLabelToday();
		activeLabelToday->CopyFrom(*ActiveData::instance()->m_vector_internet_labelToday.at(i));

		m_vector_labelToday.push_back(activeLabelToday);
	}
}

void ActiveTodayTableView::initCell( CCTableViewCell * cell, unsigned int nIndex )
{
	// 底图
	CCScale9Sprite * sprite_frame = CCScale9Sprite::create("res_ui/kuang0_new.png");
	sprite_frame->setContentSize(CCSizeMake(CCSizeCellSize.width - 6, CCSizeCellSize.height - 2));
	sprite_frame->setCapInsets(CCRectMake(18, 9, 2, 23));
	sprite_frame->setAnchorPoint(CCPointZero);
	sprite_frame->setPosition(ccp(3, 1));
	cell->addChild(sprite_frame);

	CCSprite * sprite_activeIcon = CCSprite::create("res_ui/huoyuedu.png");
	sprite_activeIcon->setAnchorPoint(ccp(0, 0));
	sprite_activeIcon->ignoreAnchorPointForPosition(false);
	sprite_activeIcon->setPosition(ccp(5, (sprite_frame->getContentSize().height - sprite_activeIcon->getContentSize().height) / 2));
	cell->addChild(sprite_activeIcon);


	// 所需活跃度
	int nActiveNeed = m_vector_labelToday.at(nIndex)->needactive();

	char charActiveNeed[20];
	sprintf(charActiveNeed, "%d", nActiveNeed);

	/*CCLabelTTF * pLabel_active = CCLabelTTF::create(charActiveNeed, APP_FONT_NAME, 18, CCSizeMake(150, 0), kCCTextAlignmentLeft);
	pLabel_active->setColor(ccc3(255, 255, 255));
	pLabel_active->setAnchorPoint(ccp(0.5f, 0.5f));
	pLabel_active->setHorizontalAlignment(kCCTextAlignmentCenter);
	pLabel_active->setPosition(ccp(20, sprite_frame->getContentSize().height / 2));
	cell->addChild(pLabel_active);*/

	UILabelBMFont * pLabel_active = UILabelBMFont::create();
	pLabel_active->setFntFile("res_ui/font/ziti_12.fnt");
	pLabel_active->setText(charActiveNeed);
	pLabel_active->setAnchorPoint(ccp(0.5f, 0.5f));
	pLabel_active->setPosition(ccp(sprite_activeIcon->getPositionX() + sprite_activeIcon->getContentSize().width / 2,
		sprite_activeIcon->getPositionY() + sprite_activeIcon->getContentSize().height / 2 - 10));
	
	UILayer * layer = UILayer::create();
	cell->addChild(layer);

	layer->addWidget(pLabel_active);

	// 物品底框
	initGoodsInfo(layer, nIndex, sprite_frame);
}

void ActiveTodayTableView::initGoodsInfo(UILayer * ui_layer, unsigned int nIndex, CCScale9Sprite* sprite_bg)
{
	std::vector<GoodsInfo *> vector_goods;
	int nGoodSize = m_vector_labelToday.at(nIndex)->goods_size();	
	for(int i = 0; i < nGoodSize; i ++)
	{
		GoodsInfo* pGoodsInfo = new GoodsInfo();
		pGoodsInfo->CopyFrom(m_vector_labelToday.at(nIndex)->goods(i));

		vector_goods.push_back(pGoodsInfo);
	}

	// icon_frame
	if (0 == nGoodSize)
	{
		return ;
	}

	GoodsInfo* tmpGoodsInfo = vector_goods.at(0);

	std::string goods_icon = vector_goods.at(0)->icon();

	std::string goodsIconName = "res_ui/props_icon/";
	goodsIconName.append(goods_icon);
	goodsIconName.append(".png");

	// 根据goods品阶选择底图
	int nQuality = vector_goods.at(0)->quality();
	std::string strQuality = getEquipmentQualityByIndex(nQuality);

	ActiveUI * activeUI = (ActiveUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveUI);
	if (NULL == activeUI)
	{
		return;
	}

	// frame
	UIImageView* pImageView_frame = UIImageView::create();
	pImageView_frame->loadTexture(strQuality.c_str());
	pImageView_frame->setAnchorPoint(ccp(0.5f, 0.5f));
	pImageView_frame->setPosition(ccp(70, sprite_bg->getContentSize().height / 2));
	pImageView_frame->setTouchEnable(true);
	pImageView_frame->setScale(0.8f);
	ui_layer->addWidget(pImageView_frame);

	// icon
	UIButton* pImageView_icon = UIButton::create();
	pImageView_icon->loadTextures(goodsIconName.c_str(), goodsIconName.c_str(), goodsIconName.c_str());
	pImageView_icon->setAnchorPoint(ccp(0.5f, 0.5f));
	pImageView_icon->setPosition(ccp(70, sprite_bg->getContentSize().height / 2));
	pImageView_icon->addReleaseEvent(activeUI, coco_releaseselector(ActiveUI::callBackGoodIcon));
	pImageView_icon->setTouchEnable(true);
	pImageView_icon->setScale(0.8f);
	pImageView_icon->setTag(m_vector_labelToday.at(nIndex)->needactive());
	ui_layer->addWidget(pImageView_icon);

	// good 数量
	std::vector<int > vector_goodsNum;
	int nGoodNumSize = m_vector_labelToday.at(nIndex)->goodsnumber_size();
	for(int i = 0; i < nGoodNumSize; i ++)
	{
		vector_goodsNum.push_back(m_vector_labelToday.at(nIndex)->goodsnumber(i));
	}

	if (0 == nGoodNumSize)
	{
		return ;
	}

	int nNum = vector_goodsNum.at(0);

	char charGoodNum[20];
	sprintf(charGoodNum, "%d", nNum);

	CCLabelTTF * pLabel_goodNum = CCLabelTTF::create(charGoodNum, APP_FONT_NAME, 12, CCSizeMake(150, 0), kCCTextAlignmentLeft);
	pLabel_goodNum->setColor(ccc3(255, 255, 255));
	pLabel_goodNum->setAnchorPoint(ccp(0.5f, 0.5f));
	pLabel_goodNum->setHorizontalAlignment(kCCTextAlignmentCenter);
	//pLabel_goodNum->setPosition(ccp(110, sprite_bg->getContentSize().height / 2));
	pLabel_goodNum->setPosition(ccp(pImageView_frame->getPosition().x + 15, sprite_bg->getContentSize().height / 2 - 15));
	ui_layer->addChild(pLabel_goodNum);

	// btn
	UIButton * sprite_btn_normal = UIButton::create();
	sprite_btn_normal->setTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","res_ui/new_button_001.png");
	sprite_btn_normal->setAnchorPoint(ccp(0.5f,0.5f));
	sprite_btn_normal->setScale9Enable(true);
	sprite_btn_normal->setCapInsets(CCRectMake(18,9,2,23));
	sprite_btn_normal->setScale9Size(CCSizeMake(75,36));
	sprite_btn_normal->setTouchEnable(true);
	sprite_btn_normal->addReleaseEvent(this,coco_releaseselector(ActiveTodayTableView::callBackActiveTodayBtn));
	sprite_btn_normal->setPosition(ccp(135, sprite_bg->getContentSize().height / 2));
	

	// 所需活跃度
	int nActiveNeed = m_vector_labelToday.at(nIndex)->needactive();
	sprite_btn_normal->setTag(nActiveNeed);


	// 判断 btn状态 0.未领取奖励，1.已领取奖励完毕
	std::string strBtnText = "";
	int nBtnStatus = m_vector_labelToday.at(nIndex)->status();
	if (0 == nBtnStatus)
	{
		// 所需活跃度
		int nActiveNeed = m_vector_labelToday.at(nIndex)->needactive();
		int nTodayActive = ActiveData::instance()->get_todayActive();

		if (nTodayActive >= nActiveNeed)
		{
			const char * char_get = StringDataManager::getString("activy_lingqu");
			char * str_get = const_cast<char *>(char_get);
			strBtnText = str_get;

			sprite_btn_normal->setPressedActionEnabled(true);

		}
		else
		{
			const char * char_disable = StringDataManager::getString("activy_weidacheng");
			char * str_disable = const_cast<char *>(char_disable);
			strBtnText = str_disable;
			
			sprite_btn_normal->setTextures("res_ui/zhezhao2_btn.png","res_ui/zhezhao2_btn.png","res_ui/zhezhao2_btn.png");
			sprite_btn_normal->setScale9Enable(false);
			sprite_btn_normal->setTouchEnable(false);
			sprite_btn_normal->setPressedActionEnabled(false);
		}
	}
	else if (1 == nBtnStatus)
	{
		const char * char_hasGet = StringDataManager::getString("activy_yilingqu");
		char * str_hasGet = const_cast<char *>(char_hasGet);
		strBtnText = str_hasGet;

		sprite_btn_normal->setTextures("res_ui/new_button_001.png","res_ui/new_button_001.png","res_ui/new_button_001.png");
		sprite_btn_normal->setScale9Enable(true);
		sprite_btn_normal->setCapInsets(CCRectMake(18,9,2,23));
		sprite_btn_normal->setScale9Size(CCSizeMake(75,36));
		sprite_btn_normal->setTouchEnable(false);
		sprite_btn_normal->setPressedActionEnabled(false);
	}
	ui_layer->addWidget(sprite_btn_normal);

	// btn label
	UILabel * label_btn = UILabel::create();
	label_btn->setAnchorPoint(ccp(0.5f, 0.5f));
	label_btn->setFontSize(16);
	label_btn->setText(strBtnText.c_str());
	label_btn->setPosition(ccp(0, 0));
	sprite_btn_normal->addChild(label_btn);
}

std::string ActiveTodayTableView::getEquipmentQualityByIndex( int quality )
{
	std::string frameColorPath = "res_ui/";
	switch(quality)
	{
	case 1:
		{
			frameColorPath.append("sdi_white");
		}break;
	case 2:
		{
			frameColorPath.append("sdi_green");
		}break;
	case 3:
		{
			frameColorPath.append("sdi_bule");
		}break;
	case 4:
		{
			frameColorPath.append("sdi_purple");
		}break;
	case 5:
		{
			frameColorPath.append("sdi_orange");
		}break;
	}
	frameColorPath.append(".png");
	return frameColorPath;
}

void ActiveTodayTableView::callBackGoodIcon( CCObject* obj )
{
	UIButton* pBtn = (UIButton *)obj;
	// 此处用 所需的活跃度作为 tag
	int nTag = pBtn->getTag();

	//initDataFromInternet();

	int nSize = m_vector_labelToday.size();
	for (int i = 0; i < nSize; i++)
	{
		CActiveLabelToday * activeLabelToday = m_vector_labelToday.at(i);
		if (nTag == activeLabelToday->needactive())
		{
			GoodsInfo * pGoodsInfo = new GoodsInfo();
			pGoodsInfo->CopyFrom(activeLabelToday->goods(0));

			GoodsItemInfoBase * goodsItemInfoBase = GoodsItemInfoBase::create(pGoodsInfo,GameView::getInstance()->EquipListItem,0);
			goodsItemInfoBase->ignoreAnchorPointForPosition(false);
			goodsItemInfoBase->setAnchorPoint(ccp(0.5f, 0.5f));
			//goodsItemInfoBase->setPosition(ccp(size.width / 2, size.height / 2));

			GameView::getInstance()->getMainUIScene()->addChild(goodsItemInfoBase);
		}
	}
}

void ActiveTodayTableView::callBackActiveTodayBtn( CCObject* obj )
{
	/*CCMenuItemSprite* pMenuItem = (CCMenuItemSprite *)obj;*/
	UIButton * pUIBtn = (UIButton*)obj;
	int nId = pUIBtn->getTag();

	//char charTmp[20];
	//sprintf(charTmp, "%d", nId);

	//GameView::getInstance()->showAlertDialog(charTmp);

	GameMessageProcessor::sharedMsgProcessor()->sendReq(5130, (void *)nId);
}






