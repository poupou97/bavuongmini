#ifndef _SIGNMONTHLYUI_SIGNMONTHLYUI_H_
#define _SIGNMONTHLYUI_SIGNMONTHLYUI_H_

#include "../../ui/extensions/UIScene.h"

class UITab;
class GoodsInfo;

class SignMonthlyUI : public UIScene
{
public:
	SignMonthlyUI();
	~SignMonthlyUI();

	static SignMonthlyUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	void CloseEvent(CCObject *pSender);

	virtual void update(float dt);

	void GetGeneralEvent(CCObject *pSender);

	void createGenralHead();

	void RefreshByDays(int curDay);
private:
	UILayer * u_layer;
	CCScrollView * scrollView_info ;
};

///////////////////////////////////////////////////////////////

class GoodsInfo;
class COneMouthSignGift;

class SignMonthlyItem : public UIScene
{
public:
	SignMonthlyItem();
	~SignMonthlyItem();

	static SignMonthlyItem* create(COneMouthSignGift * oneMoushSignGift);
	bool init(COneMouthSignGift * oneMoushSignGift);

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	void GoodItemEvent(CCObject *pSender);

	void RefreshUIByStatus(int status);

public:
	COneMouthSignGift * curOneMonthSighGift;
private:
	
	GoodsInfo * curGoods;
	UIImageView* image_Frame;
};

#endif

