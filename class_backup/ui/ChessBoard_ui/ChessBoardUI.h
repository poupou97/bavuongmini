#ifndef _UI_CHESSBOARD_CHESSBOARDUI_H_
#define _UI_CHESSBOARD_CHESSBOARDUI_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

/////////////////////////////////
/**
 * 华容道 界面
 * @author liuliang
 * @version 0.1.0
 * @date 2014.07.11
 */

class ChessBoardLayer;
class CCTutorialParticle;

class ChessBoardUI : public UIScene
{
public:
	ChessBoardUI();
	virtual ~ChessBoardUI();

	static UIPanel * s_pPanel;

	static ChessBoardUI * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual void update(float dt);

	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);

public:
	void callBackBtnClolse(CCObject * obj);																	// 关闭按钮的响应
	void callBackBtnRest(CCObject* obj);																	// 重置 按钮的响应
	void callBackBtnFinish(CCObject* obj);																	// 完成 按钮的响应
	void callBackBtnSkip(CCObject* obj);

	void setFinishBtnEnable();																				// 设置 完成 按钮 为可用
	void setResetBtnDisable();																				// 设置 重置 按钮 为不可用
	
	void initFinishParticle();																				// 初始化 完成 按钮的特效
	void removeFinishParticle();																			// 移除 完成 按钮的特效
	
	void initExitSpriteAction();																			// 初始化 出口图片的 动画效果
	void removeExitSprite();																				// 移除 出口图片（及动画效果）
	
	void initChessBoardData();																				// 对外接口，用于初始化棋盘（调用此接口前，务必调用 set_difficulty 接口）

private:
	void initUI();
	void initChessBoard(float xPos, float yPos);
	void initLocationFrame();																				// 此方法用于 当 跳过按钮不可用时，重新布局
	void initExitSprite();																					// 初始化 出口图片


private:
	UILayer * m_base_layer;																							// 基调layer

	ChessBoardLayer * m_layer_chessBoardLayer;
	UIImageView * m_imageView_frame;

	UIButton* m_pBtnFinish_enable;
	UIButton* m_pBtnFinish_disable;
	UIButton* m_pBtnSkip;
	UIButton* m_pBtnReset_enable;
	UIButton* m_pBtnReset_disable;

	UILabelBMFont * m_labelBMF_stepValue;

	CCTutorialParticle * m_particle_finish;																//  完成按钮 特效
	CCSprite * m_sprite_arrowExit;

	float m_xOff_chessBoard;
	float m_yOff_chessBoard;

	int m_nDifficulty;																						// 难易程度（1：简单；2：中等；3：困难）
public:
	void set_difficulty(int nDifficulty);
	int get_difficulty();
};

#endif