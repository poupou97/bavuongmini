#ifndef __LAYER_BLOCKLAYER_H__
#define __LAYER_BLOCKLAYER_H__

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

#include <string>

using namespace std;

class ChessBoardLayer;

class BlockLayer :
	public cocos2d::CCLayer
{
public:
	BlockLayer(void);
	~BlockLayer(void);

public:
	static BlockLayer* create(ChessBoardLayer * pChessBoardLayer);

	virtual bool init(ChessBoardLayer * pChessBoardLayer);

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

public:
	void initBlock(int nType, int nBlockInRow, int nBlockInColumn, float cellWidth, float cellHeight, std::string strImageName);			// 初始化Block
	
	bool tryMoveUp(int &nRow, int &nColumn, int nType);															// 是否可以向 上 移动
	bool tryMoveDown(int &nRow, int &nColumn, int nType);															// 是否可以向 下 移动
	bool tryMoveLeft(int &nRow, int &nColumn, int nType);															// 是否可以向 左 移动
	bool tryMoveRight(int &nRow, int &nColumn, int nType);															// 是否可以向 右 移动

	void blockRunMoveAction(int nDirection);																		// block向指定的方向移动
	void blockMove(float xOff, float yOff, int nDirection);
	void blockRunMoveAction(float xOff, float yOff, int nDirection);

	void refreshChessBoard(int nRow, int nColumn, int nDirection, int nBlockType);								
	void refreshChessBoardForSmall(int nRow, int nColumn, int nDirection);
	void refreshChessBoardForVer(int nRow, int nColumn, int nDirection);
	void refreshChessBoardForHor(int nRow, int nColumn, int nDirection);
	void refreshChessBoardForBig(int nRow, int nColumn, int nDirection);

	void refreshBlockState(int nRow, int nColumn);																	// 更新blcokState
	void refreshBlockState(int nRow, int nColumn, int nDirection, int nBlockType);

	void refreshBeginTouch(CCPoint& pointBegin, CCPoint& pointMove);

public:
	typedef enum BlockType
	{
		BLOCK_TYPE_SMALL = 1,
		BLOCK_TYPE_VER,
		BLOCK_TYPE_HOR,
		BLOCK_TYPE_BIG,
		BLOCK_TYPE_NONE,
	};

	typedef enum BlockDirection
	{
		DIRECTION_UP = 1,
		DIRECTION_DOWN,
		DIRECTION_LEFT,
		DIRECTION_RIGHT,
		DIRECTION_NONE,
	};

private:
	ChessBoardLayer * m_chessBoardLayer;

	int m_blockType;										// blockType
	CCSprite * m_pBlockSprite;								// 图形
		
private:
	CCPoint m_point_begin;
	CCPoint m_point_end; 
	CCPoint m_point_move;
	CCPoint m_point_preMove;

	int m_state_direction;						

	int m_BlockInRow;										// 中心点所在row
	int m_BlockInColumn;									// 中心店所在column
	
	int m_point_touchRow;
	int m_point_touchColumn;
	
	float m_xPos;
	float m_yPos;

	bool m_bIsWin;

public:
	void set_blockInRow(int nRow);
	void set_blockInColumn(int nColumn);
	void set_blockType(int nType);

	void set_xPos(float xPos);
	float get_xPos();

	void set_yPos(float yPos);
	float get_yPos();

private:
	int get_row(CCPoint point);																			// 通过点击的point获取所在的row
	int get_column(CCPoint point);																		// 通过点击的point获取所在的column
	
	bool checkIsInTouch(int nRow, int nColumn, int nBlockType);										// 检测BeginTouch中是否在block上
	bool checkIsCanMove(int &nBeginRow, int &nBeginColumn, int nDirection,int nBlockType);			// 检测是否可以移动
	bool checkIsCanMove(int &nBeginRow, int &nBeginColumn, int nDirection,int nBlockType, int nMoveRow, int nMoveColumn);			// 检测是否可以移动

	bool checkPointIsEmpty(int nRow, int nColumn);

	void initTouchMove(CCTouch *pTouch);																// 初始化touchMove事件逻辑
	void initTouchMoveForSmall(CCTouch *pTouch);
	void initTouchMoveForVer(CCTouch *pTouch);
	void initTouchMoveForHor(CCTouch *pTouch);
	void initTouchMoveForBig(CCTouch *pTouch);

	void initTouchEnd(CCTouch *pTouch);																	// 初始化touchEnd事件逻辑
	void initTouchEndForSmall(CCTouch *pTouch);
	void initTouchEndForVer(CCTouch *pTouch);
	void initTouchEndForHor(CCTouch *pTouch);
	void initTouchEndForBig(CCTouch *pTouch);

	bool checkDirectionIsUserful(int nDirection);						// 判断移动方向是否可用（例如在起始方向为水平方向，则在TouchEnd前，竖直方向的移动无效）

	void changeRowToCenter(int &nRow);																	// 将row设置到中心点
	void changeColumnToCenter(int &nColumn);															// 将column设置到中心点

	bool checkIsWin();																					// 判断是否胜利
	void BigBlockRunWinnerAction();

	void setNegativeDirection(int nPositiveDirection);												// 通过正方向来设置反方向

	int checkEndMoveDirection(int nBeginRow, int nBeginColumn, int nEndRow, int nEndColumn);

	void refreshEndMoveState(int nBeginRow, int nBeginColumn, int nEndRow, int nEndColumn);
	void refreshEndMoveStateForSmall(int nBeginRow, int nBeginColumn, int nEndRow, int nEndColumn);
	void refreshEndMoveStateForVer(int nBeginRow, int nBeginColumn, int nEndRow, int nEndColumn);
	void refreshEndMoveStateForHor(int nBeginRow, int nBeginColumn, int nEndRow, int nEndColumn);
	void refreshEndMoveStateForBig(int nBeginRow, int nBeginColumn, int nEndRow, int nEndColumn);

	void refreshStepMove(int nStepMove);
	void calculateStepMove(int nBeginRow, int nBeginColumn, int nEndRow, int nEndColumn);
	void calculateStepMoveForSmall(int nBeginRow, int nBeginColumn, int nEndRow, int nEndColumn);
	void calculateStepMoveForVer(int nBeginRow, int nBeginColumn, int nEndRow, int nEndColumn);
	void calculateStepMoveForHor(int nBeginRow, int nBeginColumn, int nEndRow, int nEndColumn);
	void calculateStepMoveForBig(int nBeginRow, int nBeginColumn, int nEndRow, int nEndColumn);

	void calculateSpentTimeForHor(CCTouch *pTouch);
	void calculateSpentTimeForVer(CCTouch *pTouch);

	std::string getImagePath(std::string strImageName);												// 获取XML imageName
	
private:
	void callBackWinnerActionFinish( CCObject * obj );

private:
	float m_xBeginPos;
	float m_yBeginPos;

	float m_xMinPos;
	float m_xMaxPos;
	float m_yMinPos;
	float m_yMaxPos;

	bool bIsMoveing;

	int m_direction_positive;
	int m_direcion_negative;

	long double m_dStartTime;
	long double m_dEndedTime;

	float m_time_runAction;
	float m_velocity;
};

#endif // __LAYER_CHESSBOARDLAYER_H__