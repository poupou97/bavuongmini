#include "ChessBlockItem.h"

ChessBlockItem::ChessBlockItem()
	:m_nId(0)
	,m_nType(0)
	,m_nRow(0)
	,m_nColumn(0)
{

}

ChessBlockItem::~ChessBlockItem()
{

}

void ChessBlockItem::set_id( int nId )
{
	this->m_nId = nId;
}

int ChessBlockItem::get_id()
{
	return this->m_nId;
}

void ChessBlockItem::set_type( int nType )
{
	this->m_nType = nType;
}

int ChessBlockItem::get_type()
{
	return this->m_nType;
}

void ChessBlockItem::set_row( int nRow )
{
	this->m_nRow = nRow;
}

int ChessBlockItem::get_row()
{
	return this->m_nRow;
}

void ChessBlockItem::set_column( int nColumn )
{
	this->m_nColumn = nColumn;
}

int ChessBlockItem::get_column()
{
	return this->m_nColumn;
}

void ChessBlockItem::set_imageName( std::string strImageName )
{
	this->m_strImageName = strImageName;
}

std::string ChessBlockItem::get_imageName()
{
	return this->m_strImageName;
}
