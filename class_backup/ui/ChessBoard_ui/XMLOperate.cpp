#include "XMLOperate.h"

#include <iostream>
#include "../../../cocos2dx/support/tinyxml2/tinyxml2.h"

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ChessBlockItem.h"
#include "GameView.h"

USING_NS_CC;
USING_NS_CC_EXT;

std::vector<ChessBlockItem *> XMLOperate::s_vector_data;
int XMLOperate::s_randPassNum = 0;									// 随机选出来的关卡

XMLOperate::XMLOperate()
{

}

XMLOperate::~XMLOperate()
{

}

void XMLOperate::readXML( const char * charFileName )
{
	// win32的方法
	/*tinyxml2::XMLDocument * pDoc = new tinyxml2::XMLDocument();
	pDoc->LoadFile(charFileName);*/

	unsigned long size;
	char *pFileContent = (char*)CCFileUtils::sharedFileUtils()->getFileData(charFileName, "r", &size);
	tinyxml2::XMLDocument *pDoc = new tinyxml2::XMLDocument();
	pDoc->Parse(pFileContent, size);
	
	// 读取根节点
	tinyxml2::XMLElement * rootElement = pDoc->RootElement();
	if (NULL == rootElement)
	{
		CCLOG("xml file is error!!!");

		return;
	}

	// 关卡数量
	int nPassNum = 0;

	const tinyxml2::XMLElement * firstElement = rootElement->FirstChildElement();
	while(firstElement)
	{
		nPassNum++;
		firstElement = firstElement->NextSiblingElement();
	}

	// 从1 - nPassNum随机选择一个关卡
	// n=rand()%(Y-X+1)+X; /*n为X~Y之间的随机数*/
	int nRandom = rand() % (nPassNum - 1 + 1) + 1;

	if (0 == s_randPassNum)
	{
		s_randPassNum = nRandom;
	}

	const tinyxml2::XMLElement * firstElement2 = rootElement->FirstChildElement();
	while(firstElement2)
	{
		const tinyxml2::XMLAttribute * pass_attribute = firstElement2->FindAttribute("pass");
		int nPass = pass_attribute->IntValue();

		if (nPass == s_randPassNum)
		{
			const tinyxml2::XMLElement * dataElement = firstElement2->FirstChildElement();
			while(dataElement)
			{
				const tinyxml2::XMLAttribute * id_attribute = dataElement->FindAttribute("id");
				int nId = id_attribute->IntValue();

				const tinyxml2::XMLAttribute * type_attribute = dataElement->FindAttribute("type");
				int nType = type_attribute->IntValue();

				const tinyxml2::XMLAttribute * row_attribute = dataElement->FindAttribute("row");
				int nRow = row_attribute->IntValue();

				const tinyxml2::XMLAttribute * column_attribute = dataElement->FindAttribute("column");
				int nColumn = column_attribute->IntValue();

				const tinyxml2::XMLAttribute * imageName_attribute = dataElement->FindAttribute("imageName");
				const char * charImageName = imageName_attribute->Value();

				ChessBlockItem * pChessBlockItem = new ChessBlockItem();
				pChessBlockItem->set_id(nId);
				pChessBlockItem->set_type(nType);
				pChessBlockItem->set_row(nRow);
				pChessBlockItem->set_column(nColumn);
				pChessBlockItem->set_imageName(charImageName);

				s_vector_data.push_back(pChessBlockItem);

				dataElement = dataElement->NextSiblingElement();
			}

			break;
		}

		firstElement2 = firstElement2->NextSiblingElement();
	}
}

void XMLOperate::clearVector()
{
	std::vector<ChessBlockItem *>::iterator iter;
	for (iter = s_vector_data.begin(); iter != s_vector_data.end(); iter++)
	{
		delete *iter;
	}
	s_vector_data.clear();
}

void XMLOperate::clearPassNum()
{
	// 设置 随机关卡为0（清除之前保存的信息）
	s_randPassNum = 0;
}
