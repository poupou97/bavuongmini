#ifndef _UI_CHESSBOARD_XMLOPERATE_H_
#define _UI_CHESSBOARD_XMLOPERATE_H_

#include <vector>

class ChessBlockItem;

class XMLOperate
{
public:
	XMLOperate();
	~XMLOperate();

public:
	static void readXML(const char * charFileName);

	static void clearVector();

	static int s_randPassNum;									// 随机选出来的关卡
	
	static void clearPassNum();

public:
	static std::vector<ChessBlockItem *> s_vector_data;
};

#endif