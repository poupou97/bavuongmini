
#ifndef _SHOPUI_SHOPITEMINFO_H_
#define _SHOPUI_SHOPITEMINFO_H_

#include "../backpackscene/GoodsItemInfoBase.h"

class CSalableCommodity;

class ShopItemInfo : public GoodsItemInfoBase
{
public:
	ShopItemInfo();
	~ShopItemInfo();

	static ShopItemInfo * create(CSalableCommodity * salableCommodity);
	bool init(CSalableCommodity * salableCommodity);

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);
};

#endif
