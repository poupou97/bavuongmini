#include "ShopItemInfo.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../messageclient/element/CSalableCommodity.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"


ShopItemInfo::ShopItemInfo()
{
}


ShopItemInfo::~ShopItemInfo()
{
}

ShopItemInfo * ShopItemInfo::create( CSalableCommodity * salableCommodity )
{
	ShopItemInfo * shopItemInfo = new ShopItemInfo();
	if (shopItemInfo && shopItemInfo->init(salableCommodity))
	{
		shopItemInfo->autorelease();
		return shopItemInfo;
	}
	CC_SAFE_DELETE(shopItemInfo);
	return NULL;
}

bool ShopItemInfo::init( CSalableCommodity * salableCommodity )
{
	GoodsInfo * goodsInfo = new GoodsInfo();
	goodsInfo->CopyFrom(salableCommodity->goods());
	if (GoodsItemInfoBase::init(goodsInfo,GameView::getInstance()->EquipListItem,0))
	{
		delete goodsInfo;

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setAnchorPoint(ccp(0,0));
		return true;
	}
	return false;
}

void ShopItemInfo::onEnter()
{
	GoodsItemInfoBase::onEnter();
	this->openAnim();
}

void ShopItemInfo::onExit()
{
	GoodsItemInfoBase::onExit();
}

bool ShopItemInfo::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	return resignFirstResponder(pTouch,this,true);
}

void ShopItemInfo::ccTouchMoved( CCTouch *pTouch, CCEvent *pEvent )
{

}

void ShopItemInfo::ccTouchEnded( CCTouch *pTouch, CCEvent *pEvent )
{

}

void ShopItemInfo::ccTouchCancelled( CCTouch *pTouch, CCEvent *pEvent )
{

}

