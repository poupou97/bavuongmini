
#ifndef _SHOPUI_SHOPUI_
#define _SHOPUI_SHOPUI_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class UITab;
class PacPageView;
class CSalableCommodity;

class ShopUI : public UIScene, public CCTableViewDataSource, public CCTableViewDelegate
{
public:
	ShopUI();
	~ShopUI();

	static ShopUI * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);

	void CloseEvent(CCObject* pSender);
	void SortEvent(CCObject *pSender);

	void SellInfoEvent(CCObject * pSender);
	void BuyBackInfoEvent(CCObject * pSender);
	void BuyEvent(CCObject * pSender);
	void BuyBackEvent(CCObject * pSender);

	void ReloadData();
	void ReloadDataWithOutChangeOffSet();

	virtual void update(float dt);

	void PageScrollToDefault();

private:
	UIPanel * ppanel;
	UILayer * u_layer;
	UILabel *Label_gold;
	UILabel *Label_goldIngot;
	
	CCTableView * m_tableView;

	PacPageView * m_pacPageView;
	//右边pageview的指示点
	UIImageView * pointRight;

	//当前选择的商店物品项
	CSalableCommodity * curSalableCommodity;
	//当前选择的回购项
	CSalableCommodity * curBuyBackCommodity;

	float m_remainArrangePac;
	
public: 
	UITab * mainTab ;
	bool isBuyOrSell;
	//是否点击了购买/回购按钮
	bool isSelectBtn;

	struct sellGoodsStruct
	{
		int folderIndex;
		int amount;
		int autoflag;
	};

private:
	void MainTabIndexChangedEvent(CCObject * pSender);
	void RightPageViewChanged(CCObject* pSender);
};

#endif

