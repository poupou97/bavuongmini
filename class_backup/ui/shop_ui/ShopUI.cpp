#include "ShopUI.h"
#include "../extensions/UITab.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "GameView.h"
#include "../extensions/CCMoveableMenu.h"
#include "../backpackscene/PacPageView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/CSalableCommodity.h"
#include "../extensions/Counter.h"
#include "ShopItemInfo.h"
#include "ShopItemBuyInfo.h"
#include "../../utils/StaticDataManager.h"
#include "AppMacros.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "../../messageclient/element/CShortCut.h"
#include "../../messageclient/element/CDrug.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutSlot.h"
#include "../backpackscene/PacPageView.h"

#define CellBase_Buy_Tag 100
#define CellBase_Info_Tag 1000

ShopUI::ShopUI():
isBuyOrSell(true),
isSelectBtn(false)
{
}


ShopUI::~ShopUI()
{
	delete curSalableCommodity;
	delete curBuyBackCommodity;
}

ShopUI * ShopUI::create()
{
	ShopUI * shopUI = new ShopUI();
	if(shopUI && shopUI->init())
	{
		shopUI->autorelease();
		return shopUI;
	}
	CC_SAFE_DELETE(shopUI);
	return NULL;
}

bool ShopUI::init()
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();

		this->scheduleUpdate();

		curSalableCommodity = new CSalableCommodity();
		curBuyBackCommodity = new CSalableCommodity();

		u_layer = UILayer::create();
		u_layer->ignoreAnchorPointForPosition(false);
		u_layer->setAnchorPoint(ccp(0.5f,0.5f));
		u_layer->setContentSize(CCSizeMake(800, 480));
		u_layer->setPosition(CCPointZero);
		u_layer->setPosition(ccp(winsize.width/2,winsize.height/2));
		addChild(u_layer);

		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winsize);
		mengban->setAnchorPoint(CCPointZero);
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);

		//加载UI
		if(LoadSceneLayer::ShopLayer->getWidgetParent() != NULL)
		{
			LoadSceneLayer::ShopLayer->removeFromParentAndCleanup(false);
		}
		ppanel = LoadSceneLayer::ShopLayer;
		ppanel->setAnchorPoint(ccp(0.5f,0.5f));
		ppanel->getValidNode()->setContentSize(CCSizeMake(800, 480));
		ppanel->setPosition(CCPointZero);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnable(true);
		ppanel->setPosition(ccp(winsize.width/2,winsize.height/2));
		m_pUiLayer->addWidget(ppanel);

		const char * secondStr = StringDataManager::getString("UIName_shang");
		const char * thirdStr = StringDataManager::getString("UIName_dian");
		CCArmature * atmature = MainScene::createPendantAnm("",secondStr,thirdStr,"");
		atmature->setPosition(ccp(30,240));
		u_layer->addChild(atmature);


		//关闭按钮
		UIButton * Button_close = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnable(true);
		Button_close->setPressedActionEnabled(true);
		Button_close->addReleaseEvent(this, coco_releaseselector(ShopUI::CloseEvent));
		//整理钮
		UIButton * Button_sort = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_ArrangeBackpack");
		Button_sort->setTouchEnable(true);
		Button_sort->setPressedActionEnabled(true);
		Button_sort->addReleaseEvent(this, coco_releaseselector(ShopUI::SortEvent));

		pointRight = (UIImageView *)UIHelper::seekWidgetByName(ppanel,"dian_on_right");

		//jinbi
		int m_goldValue = GameView::getInstance()->getPlayerGold();
		char GoldStr_[20];
		sprintf(GoldStr_,"%d",m_goldValue);
		Label_gold = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_gold");
		Label_gold->setText(GoldStr_);
		//yuanbao
		int m_goldIngot =  GameView::getInstance()->getPlayerGoldIngot();
		char GoldIngotStr_[20];
		sprintf(GoldIngotStr_,"%d",m_goldIngot);
		Label_goldIngot = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_goldIngold");
		Label_goldIngot->setText(GoldIngotStr_);

		//maintab
		const char * normalImage = "res_ui/tab_1_off.png";
		const char * selectImage = "res_ui/tab_1_on.png";
		const char * finalImage = "";
		const char * highLightImage = "res_ui/tab_1_on.png";
		//CCDictionary *strings = CCDictionary::createWithContentsOfFile("res_ui/font/strings.xml");
		//const char *str1  = ((CCString*)strings->objectForKey("goods_auction_buy"))->m_sString.c_str();
		const char *str1 = StringDataManager::getString("goods_auction_buy");
		char* p1 =const_cast<char*>(str1);
		//const char *str2  = ((CCString*)strings->objectForKey("goods_shop_buyBack"))->m_sString.c_str();
		const char *str2 = StringDataManager::getString("goods_shop_buyBack");
		char* p2 =const_cast<char*>(str2);

		char * mainNames[] ={p1,p2};
		mainTab = UITab::createWithText(2,normalImage,selectImage,finalImage,mainNames,HORIZONTAL,5);
		mainTab->setAnchorPoint(ccp(0,0));
		mainTab->setPosition(ccp(69,415));
		mainTab->setHighLightImage((char * )highLightImage);
		mainTab->setDefaultPanelByIndex(0);
		mainTab->addIndexChangedEvent(this,coco_indexchangedselector(ShopUI::MainTabIndexChangedEvent));
		mainTab->setPressedActionEnabled(true);
		u_layer->addWidget(mainTab);

		m_tableView = CCTableView::create(this,CCSizeMake(301,362));
		m_tableView ->setSelectedEnable(true);   // 支持选中状态的显示
		m_tableView ->setSelectedScale9Texture("res_ui/highlight.png", CCRectMake(26, 26, 1, 1), ccp(0,-1)); 
		m_tableView->setPressedActionEnabled(true);
		m_tableView->setDirection(kCCScrollViewDirectionVertical);
		m_tableView->setAnchorPoint(ccp(0,0));
		m_tableView->setPosition(ccp(76,45));
		m_tableView->setDelegate(this);
		m_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		u_layer->addChild(m_tableView);

		//右侧背包
		m_pacPageView = GameView::getInstance()->pacPageView;
		m_pacPageView->getPageView()->addPageTurningEvent(this,coco_PageView_PageTurning_selector(ShopUI::RightPageViewChanged));
		u_layer->addChild(m_pacPageView);
		m_pacPageView->setPosition(ccp(399,100));
		m_pacPageView->setCurUITag(kTagShopUI);
		m_pacPageView->setVisible(true);
		m_pacPageView->checkCDOnBegan();
		CCSequence* seq = CCSequence::create(CCDelayTime::create(0.05f),CCCallFunc::create(this,callfunc_selector(ShopUI::PageScrollToDefault)),NULL);
		m_pacPageView->runAction(seq);

		//refresh cd
		ShortcutLayer * shortCutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
		if (shortCutLayer)
		{
			for (int i = 0;i<7;++i)  //一共7个快捷栏
			{
				if (i < GameView::getInstance()->shortCutList.size())
				{
					CShortCut * c_shortcut = GameView::getInstance()->shortCutList.at(i);
					if (c_shortcut->storedtype() == 2)    //快捷栏中是物品
					{
						if(CDrug::isDrug(c_shortcut->skillpropid()))
						{
							if (shortCutLayer->getChildByTag(shortCutLayer->shortcurTag[c_shortcut->index()]+100))
							{
								ShortcutSlot * shortcutSlot = (ShortcutSlot*)shortCutLayer->getChildByTag(shortCutLayer->shortcurTag[c_shortcut->index()]+100);
								this->m_pacPageView->RefreshCD(c_shortcut->skillpropid());
							}
						}
					}
				}
			}
		}

		UILayer * u_pageFrameLayer = UILayer::create();
		u_pageFrameLayer->ignoreAnchorPointForPosition(false);
		u_pageFrameLayer->setAnchorPoint(ccp(0.5f,0.5f));
		u_pageFrameLayer->setContentSize(CCSizeMake(800, 480));
		u_pageFrameLayer->setPosition(ccp(winsize.width/2,winsize.height/2));
		this->addChild(u_pageFrameLayer);

// 		UIImageView * m_pacPageView_kuang = UIImageView::create();
// 		m_pacPageView_kuang->setTexture("res_ui/LV5_dikuang_miaobian1_o.png");
// 		m_pacPageView_kuang->setScale9Enable(true);
// 		m_pacPageView_kuang->setScale9Size(CCSizeMake(368,35));
// 		m_pacPageView_kuang->setCapInsets(CCRectMake(30,0,1,1));
// 		m_pacPageView_kuang->setAnchorPoint(ccp(0.5f,0.5f));
// 		m_pacPageView_kuang->setPosition(ccp(571,396));
// 		u_pageFrameLayer->addWidget(m_pacPageView_kuang);
// 		UIImageView * m_tableView_kuang = UIImageView::create();
// 		m_tableView_kuang->setTexture("res_ui/LV5_dikuang_miaobian1.png");
// 		m_tableView_kuang->setScale9Enable(true);
// 		m_tableView_kuang->setScale9Size(CCSizeMake(320,377));
// 		m_tableView_kuang->setCapInsets(CCRectMake(30,30,1,1));
// 		m_tableView_kuang->setAnchorPoint(ccp(0,0));
// 		m_tableView_kuang->setPosition(ccp(62,35));
// 		u_pageFrameLayer->addWidget(m_tableView_kuang

		this->setContentSize(winsize);
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);

		return true;
	}
	return false;
}

void ShopUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void ShopUI::onExit()
{
	UIScene::onExit();
}


bool ShopUI::ccTouchBegan(CCTouch *touch, CCEvent * pEvent)
{
	return this->resignFirstResponder(touch,this,false);
}

void ShopUI::ccTouchEnded(CCTouch *touch, CCEvent * pEvent)
{
}

void ShopUI::ccTouchCancelled(CCTouch *touch, CCEvent * pEvent)
{
}

void ShopUI::ccTouchMoved(CCTouch *touch, CCEvent * pEvent)
{
}


void ShopUI::CloseEvent( CCObject* pSender )
{
	this->closeAnim();
}

CCSize ShopUI::tableCellSizeForIndex(CCTableView *table, unsigned int idx)
{
// 	if (mainTab->getCurrentIndex() <= 1)
// 	{
// 		if (idx == 0)
// 		{
// 			return CCSizeMake(298,70);
// 		}
// 		else
// 		{
// 			return CCSizeMake(298, 63);
// 		}
// 	}
// 	else
// 	{
		return CCSizeMake(295, 69);
//	}


}

CCTableViewCell* ShopUI::tableCellAtIndex(CCTableView *table, unsigned int idx)
{
	CCString *string = CCString::createWithFormat("%d", idx);
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell=new CCTableViewCell();
	cell->autorelease();

	
	if (isBuyOrSell)
	{
		CSalableCommodity * temp = GameView::getInstance()->shopItemList.at(idx);

		CCScale9Sprite * pCellBg = CCScale9Sprite::create("res_ui/kuang0_new.png");
		pCellBg->setPreferredSize((CCSizeMake(295,66)));
		pCellBg->setAnchorPoint(ccp(0, 0));
		pCellBg->setPosition(ccp(0, 0));
		cell->addChild(pCellBg);

		//物品Icon底框
		std::string iconFramePath;
		switch(temp->goods().quality())
		{
		case 1 :
			{
				iconFramePath = "res_ui/smdi_white.png";
			}
			break;
		case 2 :
			{
				iconFramePath = "res_ui/smdi_green.png";
			}
			break;
		case 3 :
			{
				iconFramePath = "res_ui/smdi_bule.png";
			}
			break;
		case 4 :
			{
				iconFramePath = "res_ui/smdi_purple.png";
			}
			break;
		case 5 :
			{
				iconFramePath = "res_ui/smdi_orange.png";
			}
			break;
		}
// 		CCSprite *  smaillFrame = CCSprite::create(iconFramePath.c_str());
// 		smaillFrame->setAnchorPoint(ccp(0, 0));
// 		smaillFrame->setPosition(ccp(15, 10));
// 		cell->addChild(smaillFrame);
// 		
		CCSprite * spFrameBg = CCSprite::create(iconFramePath.c_str());

		CCMenuItemSprite *smaillFrame = CCMenuItemSprite::create(spFrameBg, spFrameBg, spFrameBg, this, menu_selector(ShopUI::SellInfoEvent));
		smaillFrame->setTag(CellBase_Info_Tag+idx);
		smaillFrame->setZoomScale(1.0f);
		CCMoveableMenu *btn_smaillFrame = CCMoveableMenu::create(smaillFrame,NULL);
		btn_smaillFrame->setAnchorPoint(ccp(0.5f,0.5f));
		btn_smaillFrame->setPosition(ccp(38,33));
		cell->addChild(btn_smaillFrame);

		std::string pheadPath = "res_ui/props_icon/";
		pheadPath.append(temp->goods().icon().c_str());
		pheadPath.append(".png");
		CCSprite * phead = CCSprite::create(pheadPath.c_str());
		phead->ignoreAnchorPointForPosition(false);
		phead->setAnchorPoint(ccp(0, 0.5f));
		phead->setPosition(ccp(17, pCellBg->getContentSize().height/2+1));
		phead->setScale(0.85f);
		cell->addChild(phead);

		CCLabelTTF * pName = CCLabelTTF::create(temp->goods().name().c_str(),APP_FONT_NAME,18);
		pName->setAnchorPoint(ccp(0.5f,0.5f));
		pName->setPosition(ccp(140,42));
		pName->setColor(GameView::getInstance()->getGoodsColorByQuality(temp->goods().quality()));
		cell->addChild(pName);

		if(temp->pricetype() == 1)
		{
			CCSprite * sprite_price_gold = CCSprite::create("res_ui/coins.png");
			sprite_price_gold->setAnchorPoint(ccp(0.5f,0.5f));
			sprite_price_gold->setPosition(ccp(106,23));
			cell->addChild(sprite_price_gold);
		}
		else
		{
			CCSprite * sprite_price_goldIngold = CCSprite::create("res_ui/ingot.png");
			sprite_price_goldIngold->setAnchorPoint(ccp(0.5f,0.5f));
			sprite_price_goldIngold->setPosition(ccp(106,23));
			cell->addChild(sprite_price_goldIngold);
		}

		char s_price[20];
		sprintf(s_price,"%d",temp->price());
		CCLabelTTF * pPrice = CCLabelTTF::create(s_price,APP_FONT_NAME,16);
		pPrice->setAnchorPoint(ccp(0.5f,0.5f));
		pPrice->setPosition(ccp(155,23));
		cell->addChild(pPrice);


		CCScale9Sprite * spBg = CCScale9Sprite::create("res_ui/new_button_2.png");
		spBg->setPreferredSize(CCSizeMake(71,39));

		CCMenuItemSprite *itemSprite = CCMenuItemSprite::create(spBg, spBg, spBg, this, menu_selector(ShopUI::BuyEvent));
		itemSprite->setTag(CellBase_Buy_Tag+idx);
		itemSprite->setZoomScale(1.3f);
		CCMoveableMenu *btn_buy = CCMoveableMenu::create(itemSprite,NULL);
		btn_buy->setAnchorPoint(ccp(0.5f,0.5f));
		btn_buy->setPosition(ccp(246,34));
		cell->addChild(btn_buy);

		//CCDictionary *strings = CCDictionary::createWithContentsOfFile("res_ui/font/strings.xml");
		//const char *str1  = ((CCString*)strings->objectForKey("goods_auction_buy"))->m_sString.c_str();
		const char *str1 = StringDataManager::getString("goods_auction_buy");
		char* p1 =const_cast<char*>(str1);
		CCLabelTTF *l_buy=CCLabelTTF::create(p1,APP_FONT_NAME,20);
		l_buy->setAnchorPoint(ccp(0.5f,0.5f));
		l_buy->setPosition(ccp(71/2,39/2));
		itemSprite->addChild(l_buy);
	}
	else
	{
		CSalableCommodity * temp = GameView::getInstance()->buyBackList.at(idx);

		CCScale9Sprite * pCellBg = CCScale9Sprite::create("res_ui/kuang0_new.png");
		pCellBg->setPreferredSize((CCSizeMake(295,66)));
		pCellBg->setAnchorPoint(ccp(0, 0));
		pCellBg->setPosition(ccp(0, 0));
		cell->addChild(pCellBg);

		//物品Icon底框
		std::string iconFramePath;
		switch(temp->goods().quality())
		{
		case 1 :
			{
				iconFramePath = "res_ui/smdi_white.png";
			}
			break;
		case 2 :
			{
				iconFramePath = "res_ui/smdi_green.png";
			}
			break;
		case 3 :
			{
				iconFramePath = "res_ui/smdi_bule.png";
			}
			break;
		case 4 :
			{
				iconFramePath = "res_ui/smdi_purple.png";
			}
			break;
		case 5 :
			{
				iconFramePath = "res_ui/smdi_orange.png";
			}
			break;
		default:
			iconFramePath = "res_ui/smdi_white.png";
		}
// 		CCSprite *  smaillFrame = CCSprite::create(iconFramePath.c_str());
// 		smaillFrame->setAnchorPoint(ccp(0, 0));
// 		smaillFrame->setPosition(ccp(15, 10));
// 		cell->addChild(smaillFrame);

		CCSprite * spFrameBg = CCSprite::create(iconFramePath.c_str());

		CCMenuItemSprite *smaillFrame = CCMenuItemSprite::create(spFrameBg, spFrameBg, spFrameBg, this, menu_selector(ShopUI::BuyBackInfoEvent));
		smaillFrame->setTag(CellBase_Info_Tag+idx);
		smaillFrame->setZoomScale(1.0f);
		CCMoveableMenu *btn_smaillFrame = CCMoveableMenu::create(smaillFrame,NULL);
		btn_smaillFrame->setAnchorPoint(ccp(0.5f,0.5f));
		btn_smaillFrame->setPosition(ccp(38,33));
		cell->addChild(btn_smaillFrame);

		std::string pheadPath = "res_ui/props_icon/";
		pheadPath.append(temp->goods().icon().c_str());
		pheadPath.append(".png");
		CCSprite * phead = CCSprite::create(pheadPath.c_str());
		phead->ignoreAnchorPointForPosition(false);
		phead->setAnchorPoint(ccp(0, 0.5f));
		phead->setPosition(ccp(17, pCellBg->getContentSize().height/2+1));
		phead->setScale(0.85f);
		cell->addChild(phead);

		CCLabelTTF * pName = CCLabelTTF::create(temp->goods().name().c_str(),APP_FONT_NAME,18);
		pName->setAnchorPoint(ccp(0.5f,0.5f));
		pName->setPosition(ccp(130,42));
		pName->setColor(GameView::getInstance()->getGoodsColorByQuality(temp->goods().quality()));
		cell->addChild(pName);

		std::string str_num = "*";
		char s_num[20];
		sprintf(s_num,"%d",temp->quantity());
		str_num.append(s_num);
		CCLabelTTF * pNum = CCLabelTTF::create(str_num.c_str(),APP_FONT_NAME,18);
		pNum->setAnchorPoint(ccp(0.5f,0.5f));
		pNum->setPosition(ccp(pName->getPositionX()+pName->getContentSize().width/2+10,42));
		cell->addChild(pNum);

		if(temp->pricetype() == 1)
		{
			CCSprite * sprite_price_gold = CCSprite::create("res_ui/coins.png");
			sprite_price_gold->setAnchorPoint(ccp(0.5f,0.5f));
			sprite_price_gold->setPosition(ccp(106,23));
			cell->addChild(sprite_price_gold);
		}
		else
		{
			CCSprite * sprite_price_goldIngold = CCSprite::create("res_ui/ingot.png");
			sprite_price_goldIngold->setAnchorPoint(ccp(0.5f,0.5f));
			sprite_price_goldIngold->setPosition(ccp(106,23));
			cell->addChild(sprite_price_goldIngold);
		}


		char s_price[20];
		sprintf(s_price,"%d",temp->goods().sellprice()*temp->quantity());
		CCLabelTTF * pPrice = CCLabelTTF::create(s_price,APP_FONT_NAME,16);
		pPrice->setAnchorPoint(ccp(0.5f,0.5f));
		pPrice->setPosition(ccp(155,23));
		cell->addChild(pPrice);

		CCScale9Sprite * spBg = CCScale9Sprite::create("res_ui/new_button_2.png");
		spBg->setPreferredSize(CCSizeMake(71,39));

		CCMenuItemSprite *itemSprite = CCMenuItemSprite::create(spBg, spBg, spBg, this, menu_selector(ShopUI::BuyBackEvent));
		itemSprite->setTag(CellBase_Buy_Tag+idx);
		itemSprite->setZoomScale(1.3f);
		CCMoveableMenu *btn_buy = CCMoveableMenu::create(itemSprite,NULL);
		btn_buy->setAnchorPoint(ccp(0.5f,0.5f));
		btn_buy->setPosition(ccp(246,34));
		cell->addChild(btn_buy);

		//CCDictionary *strings = CCDictionary::createWithContentsOfFile("res_ui/font/strings.xml");
		//const char *str1  = ((CCString*)strings->objectForKey("goods_shop_buyBack"))->m_sString.c_str();
		const char *str1 = StringDataManager::getString("goods_shop_buyBack");
		char* p1 =const_cast<char*>(str1);
		CCLabelTTF *l_buy=CCLabelTTF::create(p1,APP_FONT_NAME,20);
		l_buy->setAnchorPoint(ccp(0.5f,0.5f));
		l_buy->setPosition(ccp(71/2,39/2));
		itemSprite->addChild(l_buy);

	}

	return cell;

}
unsigned int ShopUI::numberOfCellsInTableView(CCTableView *table)
{
		if (isBuyOrSell)
		{
			return GameView::getInstance()->shopItemList.size();
		}
		else
		{
			int buyBackItemNum = 0;
			for(int i =0;i<GameView::getInstance()->buyBackList.size();++i)
			{
				if (GameView::getInstance()->buyBackList.at(i)->has_goods())
				{
					buyBackItemNum++;
				}
			}
			return buyBackItemNum;
		}
}

void ShopUI::tableCellTouched(CCTableView* table, CCTableViewCell* cell)
{
// 	if (!isSelectBtn)
// 	{
// 		if (isBuyOrSell)
// 		{
// 			int idx = cell->getIdx();
// 			//curSalableCommodity = GameView::getInstance()->shopItemList.at(idx);
// 			curSalableCommodity->CopyFrom(*GameView::getInstance()->shopItemList.at(idx));
// 
// 			CCSize size= CCDirector::sharedDirector()->getVisibleSize();
// 			ShopItemInfo * shopItemInfo = ShopItemInfo::create(curSalableCommodity);
// 			shopItemInfo->ignoreAnchorPointForPosition(false);
// 			shopItemInfo->setAnchorPoint(ccp(0.5f,0.5f));
// 			//shopItemInfo->setPosition(ccp(size.width/2,size.height/2));
// 			GameView::getInstance()->getMainUIScene()->addChild(shopItemInfo);
// 		}
// 		else
// 		{
// 			int idx = cell->getIdx();
// 			//curBuyBackCommodity = GameView::getInstance()->buyBackList.at(idx);
// 			curBuyBackCommodity->CopyFrom(*GameView::getInstance()->buyBackList.at(idx));
// 
// 			CCSize size= CCDirector::sharedDirector()->getVisibleSize();
// 			ShopItemInfo * shopItemInfo = ShopItemInfo::create(curBuyBackCommodity);
// 			shopItemInfo->ignoreAnchorPointForPosition(false);
// 			shopItemInfo->setAnchorPoint(ccp(0.5f,0.5f));
// 			//shopItemInfo->setPosition(ccp(size.width/2,size.height/2));
// 			GameView::getInstance()->getMainUIScene()->addChild(shopItemInfo);
// 		}
// 	}
}
void ShopUI::scrollViewDidScroll(CCScrollView* view )
{
}

void ShopUI::scrollViewDidZoom(CCScrollView* view )
{
}

void ShopUI::MainTabIndexChangedEvent(CCObject* pSender)
{
	switch((int)mainTab->getCurrentIndex())
	{
	case 0 :
		isBuyOrSell = true;
		break;   
	case 1 :
		isBuyOrSell = false;
		break;
	}

	this->m_tableView->reloadData();
}

void ShopUI::BuyEvent( CCObject * pSender )
{
	CCMenuItemSprite * ps = (CCMenuItemSprite*)pSender;
	curSalableCommodity->CopyFrom(*GameView::getInstance()->shopItemList.at(ps->getTag()-CellBase_Buy_Tag));
	
	CCSize size= CCDirector::sharedDirector()->getVisibleSize();
	ShopItemBuyInfo * shopItemBuyInfo = ShopItemBuyInfo::create(curSalableCommodity);
	shopItemBuyInfo->ignoreAnchorPointForPosition(false);
	shopItemBuyInfo->setAnchorPoint(ccp(0.5f,0.5f));
	shopItemBuyInfo->setPosition(ccp(size.width/2,size.height/2));
	shopItemBuyInfo->setTag(kTagShopItemBuyInfo);
	GameView::getInstance()->getMainUIScene()->addChild(shopItemBuyInfo);

	isSelectBtn = true;
}

void ShopUI::BuyBackEvent( CCObject * pSender )
{
	CCMenuItemSprite * ps = (CCMenuItemSprite*)pSender;
	curSalableCommodity->CopyFrom(*GameView::getInstance()->buyBackList.at(ps->getTag()-CellBase_Buy_Tag));

// 	CCSize size= CCDirector::sharedDirector()->getVisibleSize();
// 	ShopItemBuyInfo * shopItemBuyInfo = ShopItemBuyInfo::create(curSalableCommodity);
// 	shopItemBuyInfo->ignoreAnchorPointForPosition(false);
// 	shopItemBuyInfo->setAnchorPoint(ccp(0.5f,0.5f));
// 	shopItemBuyInfo->setPosition(ccp(size.width/2,size.height/2));
// 	shopItemBuyInfo->setTag(kTagShopItemBuyInfo);
// 	GameView::getInstance()->getMainUIScene()->addChild(shopItemBuyInfo);

	GameMessageProcessor::sharedMsgProcessor()->sendReq(1134,(void *)curSalableCommodity->id(),(void *)curSalableCommodity->quantity());

	isSelectBtn = true;
}

void ShopUI::RightPageViewChanged( CCObject* pSender )
{
	switch((int)(m_pacPageView->getPageView()->getCurPageIndex()))
	{
	case 0:
		pointRight->setPosition(ccp(525,77));
		break;
	case 1:
		pointRight->setPosition(ccp(546,77));
		break;
	case 2:
		pointRight->setPosition(ccp(565,77));
		break;
	case 3:
		pointRight->setPosition(ccp(584,77));
		break;
	case 4:
		pointRight->setPosition(ccp(603,77));
		break;
	}
}

void ShopUI::ReloadData()
{
	this->m_tableView->reloadData();
}

void ShopUI::ReloadDataWithOutChangeOffSet()
{
	CCPoint _s = this->m_tableView->getContentOffset();
	int _h = this->m_tableView->getContentSize().height + _s.y;
	this->m_tableView->reloadData();
	CCPoint temp = ccp(_s.x,_h - this->m_tableView->getContentSize().height);
	this->m_tableView->setContentOffset(temp); 
}

void ShopUI::SortEvent( CCObject *pSender )
{
	if (m_remainArrangePac > 0.f)
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("package_arrange_canNot"));
		return;
	}

	m_remainArrangePac = 15.0f;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1308, (void *)1);
}


void ShopUI::update(float dt)
{
	char str_gold[20];
	sprintf(str_gold,"%d",GameView::getInstance()->getPlayerGold());
	Label_gold->setText(str_gold);
	char str_ingot[20];
	sprintf(str_ingot,"%d",GameView::getInstance()->getPlayerGoldIngot());
	Label_goldIngot->setText(str_ingot);

	m_remainArrangePac -= 1.0f/60;
	if (m_remainArrangePac < 0)
		m_remainArrangePac = 0.f;
}

void ShopUI::PageScrollToDefault()
{
	m_pacPageView->getPageView()->scrollToPage(0);
	RightPageViewChanged(m_pacPageView);
}

void ShopUI::SellInfoEvent( CCObject * pSender )
{
	CCMenuItemSprite * ps = (CCMenuItemSprite*)pSender;
	curSalableCommodity->CopyFrom(*GameView::getInstance()->shopItemList.at(ps->getTag()-CellBase_Info_Tag));

	CCSize size= CCDirector::sharedDirector()->getVisibleSize();
	ShopItemInfo * shopItemInfo = ShopItemInfo::create(curSalableCommodity);
	shopItemInfo->ignoreAnchorPointForPosition(false);
	shopItemInfo->setAnchorPoint(ccp(0.5f,0.5f));
	GameView::getInstance()->getMainUIScene()->addChild(shopItemInfo);
}

void ShopUI::BuyBackInfoEvent( CCObject * pSender )
{
	CCMenuItemSprite * ps = (CCMenuItemSprite*)pSender;
	curBuyBackCommodity->CopyFrom(*GameView::getInstance()->buyBackList.at(ps->getTag()-CellBase_Info_Tag));

	CCSize size= CCDirector::sharedDirector()->getVisibleSize();
	ShopItemInfo * shopItemInfo = ShopItemInfo::create(curBuyBackCommodity);
	shopItemInfo->ignoreAnchorPointForPosition(false);
	shopItemInfo->setAnchorPoint(ccp(0.5f,0.5f));
	GameView::getInstance()->getMainUIScene()->addChild(shopItemInfo);
}
