#include "UITest.h"
#include "AppMacros.h"

UITest::UITest()
{
	setTouchEnabled(true);
	setTouchMode(kCCTouchesOneByOne);

	CCSize s = CCDirector::sharedDirector()->getVisibleSize();

	// 只是简单获取一下图形大小
    CCSprite* tmp = CCSprite::create("zsg_ui/diban1.png");
    CCSize size = tmp->getContentSize();
    CCRect fullRect = CCRectMake(0,0, size.width, size.height);
    //CCRect insetRect = CCRectMake(2,19,268,29);
	CCRect insetRect = CCRectMake(103,42,107,29);
    CCLog("wh:%f,%f", size.width, size.height);
    tmp->release();
	// 调用CCScale9Sprite
    CCScale9Sprite* backGround = CCScale9Sprite::create("zsg_ui/diban1.png", fullRect, insetRect ); 
	backGround->setPreferredSize(CCSizeMake(designResolutionSize.width, designResolutionSize.height));
    //backGround->setPosition(ccp(10, 230));
	//backGround->setPosition(ccp(s.width/2, s.height/2));
	backGround->setPosition(ccp(0, 0));
	//backGround->setPosition(ccp(0, s.height));
    backGround->setAnchorPoint(CCPointZero);
	//backGround->setAnchorPoint(ccp(0.5f, 0.5f));
	//backGround->setAnchorPoint(ccp(0, 1));
    addChild(backGround);

	//this->setAnchorPoint(ccp(0.f, 0.f));
	setContentSize(CCSizeMake(designResolutionSize.width, designResolutionSize.height));

	// content
	CCSprite* pSprite = CCSprite::create("zsg_ui/huoyuedu.png");
	pSprite->setAnchorPoint(ccp(0, 0));
    pSprite->setPosition(ccp(5, 5));
    addChild(pSprite);

	pSprite = CCSprite::create("zsg_ui/tab.png");
	pSprite->setAnchorPoint(ccp(0, 1));
	pSprite->setPosition(ccp(25, designResolutionSize.height - 5));
    addChild(pSprite);

	// close button
	CCMenuItemImage *pButtonItem = CCMenuItemImage::create(
                                "zsg_ui/close.png",
                                "zsg_ui/close.png",
                                this,
                                menu_selector(UITest::menuCloseCallback));
    // create menu, it's an autorelease object
    CCMenu* pMenu = CCMenu::create(pButtonItem, NULL);
	//pMenu->ignoreAnchorPointForPosition(false);
	//pMenu->setAnchorPoint(ccp(0, 0));
	pMenu->setPosition(ccp(designResolutionSize.width - 28, designResolutionSize.height - 28));
    this->addChild(pMenu);
}

UITest::~UITest()
{

}

void UITest::onEnter()
{
    CCLayer::onEnter();

	// CCSpawn
	CCFiniteTimeAction*  action = CCSequence::create(
		CCScaleTo::create(0.15f*1/2,1.1f),
		CCScaleTo::create(0.15f*1/3,1.0f),
		NULL);

	runAction(action);
}

void UITest::onExit()
{
    CCLayer::onExit();
}

bool UITest::ccTouchBegan(CCTouch *touch, CCEvent * pEvent)
{
	// convert the touch point to OpenGL coordinates
	CCPoint location = touch->getLocation();

	CCPoint pos = this->getPosition();
	CCSize size = this->getContentSize();
	CCPoint anchorPoint = this->getAnchorPointInPoints();
	pos = ccpSub(pos, anchorPoint);
	CCRect rect(pos.x, pos.y, size.width, size.height);
	if(rect.containsPoint(location))
	{
		return true;
	}
	//this->removeFromParent();
	menuCloseCallback(NULL);
    return false;
}

void UITest::ccTouchEnded(CCTouch *touch, CCEvent * pEvent)
{
}

void UITest::ccTouchCancelled(CCTouch *touch, CCEvent * pEvent)
{
}

void UITest::ccTouchMoved(CCTouch *touch, CCEvent * pEvent)
{
}

void UITest::menuCloseCallback(CCObject* pSender)
{
	//this->removeFromParent();

	CCFiniteTimeAction*  action = CCSequence::create(
		CCScaleTo::create(0.15f*1/3,1.1f),
		CCScaleTo::create(0.15f*1/2,1.0f),
		CCRemoveSelf::create(),
		NULL);

	runAction(action);
}

void UITest::menuButtonCallback(CCObject* pSender)
{

}

void UITest::menuSkillCallback(CCObject* pSender)
{

}
