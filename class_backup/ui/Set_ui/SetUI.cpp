#include "SetUI.h"
#include "GameStateBasic.h"
#include "../../login_state/LoginState.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../extensions/UITab.h"
#include "SimpleAudioEngine.h"
#include "../../login_state/Login.h"
#include "GameAudio.h"
#include "../../GameUserDefault.h"
#include "../../utils/StaticDataManager.h"
#include "../../GameView.h"
#include "../../utils/GameUtils.h"

using namespace CocosDenshion;

SetUI::SetUI(void)
{
}


SetUI::~SetUI(void)
{
}

SetUI * SetUI::create()
{
	SetUI * setui=new SetUI();
	if (setui && setui->init())
	{
		setui->autorelease();
		return setui;
	}
	CC_SAFE_DELETE(setui);
	return NULL;
}

bool SetUI::init()
{
	if (UIScene::init())
	{
		CCSize size=CCDirector::sharedDirector()->getVisibleSize();
		//get userdefault.xml by:liutao 
		SetUI::getUserDefault();

		//SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(m_nMusic);
		//SimpleAudioEngine::sharedEngine()->playBackgroundMusic("music0.mid", true);

		UILayer * layer=UILayer::create();
		layer->ignoreAnchorPointForPosition(false);
		layer->setAnchorPoint(ccp(0.5f,0.5f));
		layer->setPosition(ccp(size.width/2,size.height/2));
		layer->setContentSize(CCSizeMake(UI_DESIGN_RESOLUTION_WIDTH,UI_DESIGN_RESOLUTION_HEIGHT));
		addChild(layer);

		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(size);
		mengban->setAnchorPoint(ccp(0,0));
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);

		if(LoadSceneLayer::setUiPanel->getWidgetParent() != NULL)
		{
			LoadSceneLayer::setUiPanel->removeFromParentAndCleanup(false);
		}

		UIPanel *setPanel=LoadSceneLayer::setUiPanel;
		setPanel->setAnchorPoint(ccp(0.5f,0.5f));
		setPanel->setPosition(ccp(size.width/2,size.height/2));
		m_pUiLayer->addWidget(setPanel);

		UICheckBox* showNearbyPlayer =(UICheckBox *)UIHelper::seekWidgetByName(setPanel,"CheckBox_seeOther");
		showNearbyPlayer->setTouchEnable(true);
		showNearbyPlayer->addEventListenerCheckBox(this,checkboxselectedeventselector(SetUI::callBackShowNearbyPlayer));
		showNearbyPlayer->setSelectedState(CCUserDefault::sharedUserDefault()->getBoolForKey("showNearbyPlayer", true));

		UIButton * btn_reload= (UIButton *)UIHelper::seekWidgetByName(setPanel,"Button_return_landing");
		btn_reload->setTouchEnable(true);
		btn_reload->setPressedActionEnabled(true);
		btn_reload->addReleaseEvent(this,coco_releaseselector(SetUI::reLoading));


		UIButton * btn_Exit= (UIButton *)UIHelper::seekWidgetByName(setPanel,"Button_close");
		btn_Exit->setTouchEnable(true);
		btn_Exit->setPressedActionEnabled(true);
		btn_Exit->addReleaseEvent(this,coco_releaseselector(SetUI::callBackExit));
	
		//setting interface	by:liutao
		/*
		//close app
		UIButton * btn_ExitWindow = (UIButton *)UIHelper::seekWidgetByName(setPanel,"Button_exit");
		btn_ExitWindow->setTouchEnable(true);
		btn_ExitWindow->setPressedActionEnabled(true);
		btn_ExitWindow->addReleaseEvent(this,coco_releaseselector(SetUI::closeWindow));
		*/
		UIButton * btn_Set = (UIButton *)UIHelper::seekWidgetByName(setPanel,"Button_set");
		btn_Set->setTextures("res_ui/new_button_001.png", "res_ui/new_button_001.png", "");
		btn_Set->setTouchEnable(true);
		btn_Set->setVisible(false);
		//btn_Set->setPressedActionEnabled(true);
		btn_Set->addReleaseEvent(this,coco_releaseselector(SetUI::callBackError));

		UIButton * btn_ContactUs = (UIButton *)UIHelper::seekWidgetByName(setPanel,"Button_ContactUs");
		btn_ContactUs->setTextures("res_ui/new_button_001.png", "res_ui/new_button_001.png", "");
		btn_ContactUs->setTouchEnable(true);
		btn_ContactUs->setVisible(false);
		//btn_ContactUs->setPressedActionEnabled(true);
		btn_ContactUs->addReleaseEvent(this,coco_releaseselector(SetUI::callBackError));

		UISlider * slider_Music = (UISlider *)UIHelper::seekWidgetByName(setPanel,"Slider_music");
		slider_Music->setTouchEnable(true);
		slider_Music->setPercent(m_nMusic);
		slider_Music->addEventListenerSlider(this, sliderpercentchangedselector(SetUI::sliderMusicEvent));

		UISlider * slider_Sound = (UISlider *)UIHelper::seekWidgetByName(setPanel,"Slider_sound");
		slider_Sound->setTouchEnable(true);
		slider_Sound->setPercent(m_nSound);
		slider_Sound->addEventListenerSlider(this, sliderpercentchangedselector(SetUI::sliderSoundEvent));

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(size);
		return true;
	}
	return false;
}

void SetUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
	GameUtils::playGameSound(SCENE_OPEN, 2, false);
}

void SetUI::onExit()
{
	UIScene::onExit();
}

void SetUI::callBackShowNearbyPlayer( CCObject * obj ,CheckBoxEventType type )
{
	UICheckBox * checkBox =dynamic_cast<UICheckBox *>(obj);
	int select_ =checkBox->getSelectedState();
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5231, (void*)select_);
	//CCUserDefault::sharedUserDefault()->setBoolForKey("showNearbyPlayer", select_);
}

 void SetUI::tabIndexChangedEvent(CCObject* pSender)
 {

 }

void SetUI::callBackError(CCObject * obj)
{
	GameView::getInstance()->showAlertDialog(StringDataManager::getString("skillui_nothavefuction"));
}

void SetUI::callBackExit(CCObject * obj)
{
	float volume = (float)m_nMusic/100.0f;
	SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(volume);
	CCUserDefault::sharedUserDefault()->setIntegerForKey(MUSIC, m_nMusic);

	float effectVolume = (float)m_nSound/100.0f;
	SimpleAudioEngine::sharedEngine()->setEffectsVolume(effectVolume);
	CCUserDefault::sharedUserDefault()->setIntegerForKey(SOUND, m_nSound);

	CCUserDefault::sharedUserDefault()->flush();

	if(volume <= 0)
		SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
	
	this->closeAnim();
	SimpleAudioEngine::sharedEngine()->playEffect(SCENE_CLOSE, false);
}

void SetUI::reLoading( CCObject * obj )
{
	GameView::getInstance()->showPopupWindow(StringDataManager::getString("con_sureToChangeId"),2,this,coco_selectselector(SetUI::SureToReLoading),NULL);
}
/*
void SetUI::closeWindow(CCObject * obj)
{
	CCUserDefault::sharedUserDefault()->flush();
	//SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
	CCDirector::sharedDirector()->end();
}
*/

void SetUI::sliderMusicEvent(CCObject *pSender, SliderEventType type)
{
    switch (type)
    {
        case cocos2d::extension::SLIDER_PERCENTCHANGED:
        {
            UISlider* slider = dynamic_cast<UISlider*>(pSender);
            int percent = slider->getPercent();
			m_nMusic = percent;
			float volume = (float)percent/100.0f;
			SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(volume);
			//CCLOG("%d", percent);
			//CCUserDefault::sharedUserDefault()->setIntegerForKey(MUSIC, percent);
			//music regulate
        }
            break;
            
        default:
            break;
    }
}

void SetUI::sliderSoundEvent(CCObject *pSender, SliderEventType type)
{
    switch (type)
    {
        case cocos2d::extension::SLIDER_PERCENTCHANGED:
        {
            UISlider* slider = dynamic_cast<UISlider*>(pSender);
            int percent = slider->getPercent();
			m_nSound = percent;
			float volume = (float)percent/100.0f;
			SimpleAudioEngine::sharedEngine()->setEffectsVolume(volume);
			//SimpleAudioEngine::sharedEngine()->playEffect("event.mid", true);

			//CCLog("%d", percent);
			//CCUserDefault::sharedUserDefault()->setIntegerForKey(SOUND, percent);
			//sound regulate
        }
            break;
            
        default:
            break;
    }
}

void SetUI::getUserDefault()
{
	m_nMusic = CCUserDefault::sharedUserDefault()->getIntegerForKey(MUSIC,50);
    CCLog("m_nMusic is %d", m_nMusic);

	m_nSound = CCUserDefault::sharedUserDefault()->getIntegerForKey(SOUND,70);
    CCLog("m_nSound is %d", m_nSound);
}

void SetUI::SureToReLoading( CCObject *obj )
{
	float volume = (float)m_nMusic/100.0f;
	SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(volume);
	CCUserDefault::sharedUserDefault()->setIntegerForKey(MUSIC, m_nMusic);

	float effectVolume = (float)m_nSound/100.0f;
	SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(effectVolume);
	CCUserDefault::sharedUserDefault()->setIntegerForKey(SOUND, m_nSound);

	CCUserDefault::sharedUserDefault()->flush();
	//SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1002);   // request logout


	//GameState* pScene = new LoginState();
	//if (pScene)
	//{
	//	pScene->runThisState();
	//	pScene->release();
	//}
}
