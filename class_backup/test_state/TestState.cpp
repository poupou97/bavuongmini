#include "TestState.h"

#include "../gamescene_state/GameSceneState.h"
#include "../loadscene_state/LoadSceneState.h"
#include "../login_state/LoginState.h"
#include "../legend_engine/CCLegendAnimation.h"
#include "../legend_engine/LegendLevel.h"
#include "../ui/extensions/CCRichLabel.h"
#include "../ui/extensions/RichElement.h"

#include "../utils/GameUtils.h"

#include "../messageclient/GameMessageProcessor.h"
#include "../messageclient/ClientNetEngine.h"
#include "../ui/extensions/RichTextInput.h"
#include "../gamescene_state/role/GameActorAnimation.h"
#include "../GameView.h"
#include "../legend_engine/GameWorld.h"
#include "../messageclient/element/CMapInfo.h"
#include "../ui/UITest.h"
#include "../ui/UISceneTest.h"
#include "../ui/extensions/UIScene.h"
#include "../legend_engine/CCLegendTiledMap.h"
#include "../utils/pathfinder/AStarTiledMap.h"

#include <cstdio> 
#include "../utils/StaticDataManager.h"
#include "../gamescene_state/role/ActorUtils.h"
#include <sqlite3.h>
#include "../legend_script/ScriptManager.h"
#include "../legend_script/Script.h"
#include "../ui/story/StoryBlackBand.h"
#include "../ui/story/StoryDialog.h"
#include "../gamescene_state/SimpleEffectManager.h"
#include "AppMacros.h"
#include "../utils/StrUtils.h"
#include "../gamescene_state/role/BasePlayer.h"
#include "../gamescene_state/GameSceneEffects.h"
#include "../utils/GameConfig.h"
#include "../ui/searchgeneral/SearchGeneral.h"

#define TEST_MOTIONSTREAK_TAG 2003
#define TEST_MOTIONSTREAK_PARTICLE_TAG 2004

//创建表  
void createTable() 
{ 
	//记录返回结果是否成功  
    int result; 

    //获取保存路径  + 保存文件名  
    std::string path=CCFileUtils::sharedFileUtils()->getWritablePath() +"save.db"; 
     
    //数据库对象  
    sqlite3 *pdb;  

    std::string sql;  
     
    result=sqlite3_open(path.c_str(),&pdb); 
    if(result!=SQLITE_OK) 
        CCLog("open database failed,  number%d",result); 
     
    //创建表的方法  
    result=sqlite3_exec(pdb,"create table student(ID integer primary key autoincrement,name text,sex text)",NULL,NULL,NULL); 
    if(result!=SQLITE_OK) 
        CCLog("create table failed1"); 

	// 插入数据
	sql="insert into student values(1,'zhycheng','male')";
	result=sqlite3_exec(pdb,sql.c_str(),NULL,NULL,NULL);
	if(result!=SQLITE_OK)
		CCLog("insert data failed!");

	sql="insert into student values(2,'liuyali','female')";
	result=sqlite3_exec(pdb,sql.c_str(),NULL,NULL,NULL);
	if(result!=SQLITE_OK)
		CCLog("insert data failed!");

	sql="insert into student values(3,'zhy_cheng','male')";
	result=sqlite3_exec(pdb,sql.c_str(),NULL,NULL,NULL);
	if(result!=SQLITE_OK)
		CCLog("insert data failed!");

	// 更新记录
	//sqlstr="update MyTable_1 set name='威震天' where ID = 3"; 
	//sqlite3_exec( pDB, sqlstr.c_str() , NULL, NULL, &errMsg ); 

	// 删除数据
	sql="delete from student where ID=1";  
	result=sqlite3_exec(pdb,sql.c_str(),NULL,NULL,NULL);  
	if(result!=SQLITE_OK)  
		CCLog("delete data failed!");  

	// 查询数据
	char **re;  
	int r,c;  
	sqlite3_get_table(pdb,"select * from student",&re,&r,&c,NULL);  
	CCLog("row is %d,column is %d",r,c);  
	//std::string value = re[2*c+1];
	//CCLog(re[2*c+1]);  

	sqlite3_free_table(re);  

    sqlite3_close(pdb); 
} 

int loadRecord( void * para, int n_column, char ** column_value, char ** column_name ) 
{ 
    CCLog("ID=%s,name=%s",column_value[0],column_value[1]); 
    return 0; 
}

std::string chineseSkillName = "no data";

//创建表  
void queryTable() 
{ 
	//记录返回结果是否成功  
    int result; 

    //获取保存路径  + 保存文件名  
    std::string path=CCFileUtils::sharedFileUtils()->getWritablePath() +"newappstore1.db";
	//std::string path="newappstore1.db";
	//std::string path=CCFileUtils::sharedFileUtils()->fullPathForFilename("newappstore1.db");

    //数据库对象  
    sqlite3 *pdb;  

    std::string sql;  
     
    result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	//result=sqlite3_open_v2(path.c_str(), &pdb, SQLITE_OPEN_READONLY, NULL);
    if(result!=SQLITE_OK) 
        CCLog("open database failed,  number%d",result); 

	// 查询数据 - 方法1
	sql="select * from t_skill where id=3"; 
	sqlite3_exec( pdb, sql.c_str(), loadRecord, NULL, NULL ); 

	// 查询数据 - 方法2
	char **re;  
	int r,c;  
	sqlite3_get_table(pdb,"select * from t_skill",&re,&r,&c,NULL);  
	CCLog("row is %d,column is %d",r,c);  
	std::string value = re[1*3+0];
	for(int row = 0; row < r; row++) {
		std::string onerecord = "";
		for(int col = 0; col < c; col++) {
			//CCLog(re[(row+1)*c+col]);  
			onerecord.append(re[(row+1)*c+col]);
			onerecord.append(", ");
		}
		//CCLog(onerecord.c_str());
	}
	sqlite3_free_table(re);  

	// 查询数据 - 方法3
	const char *query_stmt = "SELECT id,description,attack from t_skill_effect";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) {
		//通过列来读取数据，获得结果集 statement结果集
		while (sqlite3_step(statement) == SQLITE_ROW) {
			int id =sqlite3_column_double(statement, 0);
			char* descriptionField=(char*)sqlite3_column_text(statement, 1);
			double attackField =sqlite3_column_double(statement, 2);
			//char* nameField=(char*)sqlite3_column_text(statement, 0);
			chineseSkillName = descriptionField;
			//int professionField=sqlite3_column_int(statement, 1);
			//CCLOG("skill name: %s, profession: %d", nameField, professionField);
			CCLOG("id: %d, description: %s, attack: %f", id, descriptionField, attackField);
		}
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

    sqlite3_close(pdb); 
} 

TestLayer::TestLayer()
: m_state(-1)
, m_dStartTime(0)
, m_curState(State_perfect)
, m_tableviewCellNumber(5)
{
	setTouchEnabled(true);
	scheduleUpdate();

	GameConfig::load();

	CCSize s = CCDirector::sharedDirector()->getVisibleSize();

	//testSqlite();

	//testLightningEffect();

	//testLabelColorByMark();

	//testGameActorAnimation();

	//testGraySprite();

	//testScript();

	//testStory();
    
    //testParticle();
	//testMotionStreak();

	//testLegendAnimation();

	//testLegendTiledMap();

	//testTableView();

	//testCocosStudioScene();

	//testAPI();
    
    testCCRichLabel();

	//testOpenUrl();

	//testClippingNode();
	//testClippingNode_Spark();

	//testSearchGeneral();

	return;
	
	CCLayerColor *background = CCLayerColor::create(ccc4(255,255,255,255));
	background->setContentSize(s);
	addChild(background, -10);

	// test CCTextFieldTTF
	RichTextInputBox* pTestLayer = new RichTextInputBox();
	pTestLayer->setCharLimit(25);
	pTestLayer->setInputBoxWidth(300);

	pTestLayer->setPosition(ccp(10,100));
	this->addChild(pTestLayer);
	pTestLayer->release();

	return;
}

TestLayer::~TestLayer()
{
}

void TestLayer::testSearchGeneral()
{
	StringDataManager::load();
	CCSize s = CCDirector::sharedDirector()->getVisibleSize();

	SearchGeneral * searchGeneral = SearchGeneral::create(1);
	searchGeneral->ignoreAnchorPointForPosition(false);
	searchGeneral->setAnchorPoint(ccp(0.5f,0.5f));
	searchGeneral->setPosition(ccp(s.width/2,s.height/2));
	addChild(searchGeneral);
}

void TestLayer::registerWithTouchDispatcher()
{
    CCDirector* pDirector = CCDirector::sharedDirector();
    pDirector->getTouchDispatcher()->addTargetedDelegate(this, 0, false);
}

void TestLayer::setEffectPosition(CCNode* effectNode, CCPoint startPoint, CCPoint endPoint)
{
	float distance = ccpDistance(startPoint, endPoint);
	float degree = GameUtils::getDegree(startPoint, endPoint);

	effectNode->setPosition(startPoint);
	effectNode->setRotation(degree);
	effectNode->setScaleY(distance / 170);
}

void TestLayer::testLightningEffect()
{
	CCLegendAnimation* pRoleAnim = CCLegendAnimation::create("animation/player/zsgr_npj/stand.anm", 2);
	pRoleAnim->setPlayLoop(true);
	pRoleAnim->setReleaseWhenStop(false);   // only play once, then release
	pRoleAnim->setPosition(ccp(100,100-0));
	addChild(pRoleAnim, 2);

	pRoleAnim = CCLegendAnimation::create("animation/player/zsgr_npj/stand.anm", 1);
	pRoleAnim->setPlayLoop(true);
	pRoleAnim->setReleaseWhenStop(false);   // only play once, then release
	pRoleAnim->setPosition(ccp(400,100-0));
	addChild(pRoleAnim, 2);

	pRoleAnim = CCLegendAnimation::create("animation/player/zsgr_npj/stand.anm", 3);
	pRoleAnim->setPlayLoop(true);
	pRoleAnim->setReleaseWhenStop(false);   // only play once, then release
	pRoleAnim->setPosition(ccp(300, 250-0));
	addChild(pRoleAnim, 2);

	pRoleAnim = CCLegendAnimation::create("animation/player/zsgr_npj/stand.anm", 2);
	pRoleAnim->setPlayLoop(true);
	pRoleAnim->setReleaseWhenStop(false);   // only play once, then release
	pRoleAnim->setPosition(ccp(200,80-0));
	addChild(pRoleAnim, 2);

	// lightning 1
	CCLegendAnimation* pAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/lightning.anm");
	pAnim->setPlayLoop(true);
	pAnim->setReleaseWhenStop(false);   // only play once, then release
	//pAnim->setColor(ccc3(200, 230, 255));
	//pAnim->setColor(ccc3(255, 200, 200));
	//pAnim->setColor(ccc3(200, 255, 200));
	addChild(pAnim, 0);
	setEffectPosition(pAnim, ccp(100, 100), ccp(400,100));

	// lightning 2
	pAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/lightning.anm");
	pAnim->setPlayLoop(true);
	pAnim->setReleaseWhenStop(false);   // only play once, then release
	//pAnim->setColor(ccc3(255, 200, 200));
	addChild(pAnim, 0);
	setEffectPosition(pAnim, ccp(400, 100), ccp(300,250));

	// lightning 3
	pAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/lightning.anm");
	pAnim->setPlayLoop(true);
	pAnim->setReleaseWhenStop(false);   // only play once, then release
	//pAnim->setColor(ccc3(255, 200, 200));
	addChild(pAnim, 0);
	setEffectPosition(pAnim, ccp(300, 250), ccp(100,100));
}

void TestLayer::testGameActorAnimation()
{
	GameActorAnimation* pAnim = ActorUtils::createActorAnimation("zsgr_npj", "");
	addChild(pAnim);

	pAnim->setPosition(ccp(300,300));
	pAnim->setAnimName(0);
	pAnim->setAction(2);
	pAnim->play();
}

void TestLayer::testSqlite()
{
	//createTable();
	queryTable();

	CCSize s = CCDirector::sharedDirector()->getVisibleSize();

	CCLabelTTF *pLabel = CCLabelTTF::create(chineseSkillName.c_str(), "Arial", 30);
	pLabel->setAnchorPoint(ccp(0,0));
	pLabel->setPosition(ccp(100,100));
	addChild(pLabel);
}

void TestLayer::testLabelColorByMark()
{
	CCRichLabel* pRichLabel = CCRichLabel::createWithString("/#ff0000,hello//n world, it's very /#00ff00,good/!", CCSizeMake(600, 250), NULL, NULL);
	pRichLabel->setPosition(ccp(100, 400));
	addChild(pRichLabel);

	pRichLabel = CCRichLabel::createWithString("what/nhello hello/n/nworld/nhaha", CCSizeMake(300, 150), NULL, NULL);
	pRichLabel->setPosition(ccp(200, 100));
	addChild(pRichLabel);
}

void TestLayer::testGraySprite()
{
	#define TEST_PNG_NAME "res_ui/protagonist_hero/haojieNV_hero.png"

	CCSprite* testSpr = CCSprite::create(TEST_PNG_NAME);
	testSpr->setAnchorPoint(ccp(0,0));
	testSpr->setPosition(ccp(20, 200));
	testSpr->setScale(0.6f);
	GameUtils::addGray(testSpr);
	addChild(testSpr);

	testSpr = CCSprite::create(TEST_PNG_NAME);
	testSpr->setAnchorPoint(ccp(0,0));
	testSpr->setPosition(ccp(270, 200));
	testSpr->setScale(0.6f);
	testSpr->setColor(ccc3(89,89,89));
	//testSpr1->setColor(ccc3(168,168,168));
	addChild(testSpr);

	testSpr = CCSprite::create(TEST_PNG_NAME);
	testSpr->setAnchorPoint(ccp(0,0));
	testSpr->setPosition(ccp(520, 200));
	testSpr->setScale(0.6f);
	addChild(testSpr);
	
}

void TestLayer::testScript()
{
	CCSize s = CCDirector::sharedDirector()->getVisibleSize();
	// background
	CCLayerColor *background = CCLayerColor::create(ccc4(0,128,0,255), s.width, s.height);
	background->setPosition(ccp(0,0));
	addChild(background);

	CCMenuItemFont* item = CCMenuItemFont::create("Run Script", this, menu_selector(TestLayer::runScriptCallback));
	CCMenu* menu = CCMenu::create( item, NULL);
	menu->setPosition(ccp(115,250));
	addChild(menu);

	item = CCMenuItemFont::create("Skip Script", this, menu_selector(TestLayer::skipScriptCallback));
	menu = CCMenu::create( item, NULL);
	menu->setPosition(ccp(315,250));
	addChild(menu);
}

#define CCTABLEVIEW_TEST_TAG 901
void TestLayer::testTableView()
{
	CCTableView* pTableView = CCTableView::create(this,CCSizeMake(256,363));
	pTableView->setSelectedEnable(true);
	pTableView->setSelectedScale9Texture("res_ui/highlight.png", CCRectMake(26, 26, 1, 1), ccp(0,2));
	pTableView->setDirection(kCCScrollViewDirectionVertical);
	//pTableView->setDirection(kCCScrollViewDirectionHorizontal);
	pTableView->setAnchorPoint(ccp(0,0));
	pTableView->setPosition(ccp(71,38));
	pTableView->setDelegate(this);
	pTableView->setVerticalFillOrder(kCCTableViewFillTopDown);
	pTableView->setTag(CCTABLEVIEW_TEST_TAG);
	addChild(pTableView);

	pTableView->setPosition(ccp(0,0));

	CCMenuItemFont* item = CCMenuItemFont::create("Select Cell", this, menu_selector(TestLayer::menuCallback));
	CCMenu* menu = CCMenu::create( item, NULL);
	menu->setPosition(ccp(400,250));
	addChild(menu);
}

void TestLayer::testStory()
{
	CCSize s = CCDirector::sharedDirector()->getVisibleSize();
	// background
	CCLayerColor *background = CCLayerColor::create(ccc4(255,255,255,255), s.width, s.height);
	background->setPosition(ccp(0,0));
	addChild(background);

	StoryBlackBand* pBand = StoryBlackBand::create();
	addChild(pBand);

	StoryDialog* pDialog = StoryDialog::create("/#ff0000,hello//n world, it's very /#00ff00,good/!",
		"res_ui/npc/BlackMarket.png", 1, "Caocao");
	addChild(pDialog);
}

void TestLayer::addTutorialIndicator(const char* content)
{
	CCLabelTTF* label = CCLabelTTF::create(content, APP_FONT_NAME, 32);
	label->setPosition(ccp(100, 100));
	this->addChild(label, 0, 2);
}
void TestLayer::setTutorialIndicatorPosition(CCPoint pos)
{
	CCLabelTTF* label = dynamic_cast<CCLabelTTF*>(this->getChildByTag(2));
	if(label != NULL)
		label->setPosition(pos);
}
void TestLayer::removeTutorialIndicator()
{
	CCLabelTTF* label = dynamic_cast<CCLabelTTF*>(this->getChildByTag(2));
	if(label != NULL)
		label->removeFromParent();
}

void TestLayer::registerScriptCommand(int scriptId)
{
	mTutorialScriptInstanceId = scriptId;
}

void TestLayer::testParticle()
{
    CCSize s = CCDirector::sharedDirector()->getVisibleSize();
    
	CCParticleSystem* particleEffect = CCParticleSystemQuad::create("animation/texiao/particledesigner/tuowei1.plist");   // caiji1.plist
	//ccColor4F startColor = {0.5f, 0.5f, 0.5f, 1.0f};
	//particleEffect->setStartColor(startColor);
	//ccColor4F endColor = {0.1f, 0.1f, 0.1f, 0.2f};
    //   particleEffect->setEndColor(endColor);
	//particleEffect->setStartSize(10.0f);
	//particleEffect->setEndSize(10.0f);
	//particleEffect->setTexture(CCTextureCache::sharedTextureCache()->addImage("images/stars-grayscale.png"));
    particleEffect->setPosition(ccp(300,200));
	particleEffect->setPositionType(kCCPositionTypeFree);
    particleEffect->setLife(0.7f);
    particleEffect->setLifeVar(0);
	particleEffect->setScale(1.0f);
	particleEffect->setTag(11);
	particleEffect->setVisible(true);
	addChild(particleEffect);
    
	//CCFiniteTimeAction*  action = CCSequence::create(
 //                                                    CCMoveTo::create(3.0f, ccp(800,480)),
 //                                                    NULL);
    //particleEffect->runAction(action);
    
	CCRepeatForever* path2 = SimpleEffectManager::RoundRectPathAction(70, 100, 0 );
	particleEffect->setPosition(ccp(s.width/2,s.height/2));
	particleEffect->runAction(path2);
    
	CCParticleSystem* centerParticleEffect = CCParticleSystemQuad::create("animation/texiao/particledesigner/yunyu.plist");
	centerParticleEffect->setPosition(ccp(s.width*3/4,s.height/2));
	CCFiniteTimeAction* removeAction = CCSequence::create(
		//CCShow::create(),
		//CCDelayTime::create(2.0f),
		CCFadeOut::create(3.0f),
		CCRemoveSelf::create(),
		NULL);
	centerParticleEffect->runAction(removeAction);
	//this->addChild(centerParticleEffect);

	// Bonus Special Effect
	CCNodeRGBA* pNode = BonusSpecialEffect::create();
	pNode->setCascadeOpacityEnabled(true);
	pNode->setPosition(ccp(s.width/4,s.height/2));
	pNode->setOpacity(0.0f);
	addChild(pNode);

	CCMenuItemFont* item = CCMenuItemFont::create("show effect", this, menu_selector(TestLayer::showEffectCallback));
	CCMenu* menu = CCMenu::create( item, NULL);
	menu->setPosition(ccp(100, 100));
	addChild(menu);
}
void TestLayer::showEffectCallback(CCObject* pSender)
{
	CCSize s = CCDirector::sharedDirector()->getVisibleSize();

	// Bonus Special Effect
	CCNodeRGBA* pNode = BonusSpecialEffect::create();
	pNode->setPosition(ccp(s.width/4,s.height/2));
	pNode->setScale(1.0f);
	addChild(pNode);
}

void TestLayer::testMotionStreak()
{
	CCPoint initPos = ccp(240, 160);

	ccColor3B color = ccc3(255,255,0);
	CCMotionStreak* strike=CCMotionStreak::create(
		0.5f,
		16.0f,
		32.0f,
		color,
		"images/streak.png"
		);
	addChild(strike,1);
	strike->setPosition(initPos);
	strike->setTag(TEST_MOTIONSTREAK_TAG);

    //最后的颜色会跟背景色一致，这样就达到逐渐消失的效果
    CCActionInterval *colorAction = CCRepeatForever::create((CCActionInterval *)CCSequence::create(
        CCTintTo::create(0.2f, 255, 0, 0),
        CCTintTo::create(0.2f, 0, 255, 0),
        CCTintTo::create(0.2f, 0, 0, 255),
        CCTintTo::create(0.2f, 0, 255, 255),
        CCTintTo::create(0.2f, 255, 255, 0),
        CCTintTo::create(0.2f, 255, 0, 255),
        CCTintTo::create(0.2f, 255, 255, 255),
        NULL));
	strike->runAction(colorAction);

	cocos2d::CCParticleSystemQuad *mSystem=CCParticleSystemQuad::create("images/Particle.plist");
	//mSystem->initWithFile("Particle.plist");//plist文件可以通过例子编辑器获得
	mSystem->setTextureWithRect(CCTextureCache::sharedTextureCache()->addImage("images/Particle.png")
				,CCRectMake(0,0,32,32));//加载图片，第一个参数是纹理，第二个参数是选择图片上的位置
	mSystem->setBlendAdditive(true);//这个调用必不可少
	mSystem->setPosition(initPos);//设置位置
	//mSystem->setDuration(0.3f);
	mSystem->setLife(0.3f);
	mSystem->setTag(TEST_MOTIONSTREAK_PARTICLE_TAG);
	mSystem->setPositionType(kCCPositionTypeFree);
	addChild(mSystem);
	//mSystem->release();
	//delete mSystem;
	//CC_SAFE_DELETE(mSystem);
	//mSystem->setAutoRemoveOnFinish(true);
}

void TestLayer::testLegendAnimation()
{
	CCSize s = CCDirector::sharedDirector()->getVisibleSize();

	//CCLegendAnimation* pAnim = CCLegendAnimation::create("animation/npc/zsgn_LvBu/stand.anm");
	//CCLegendAnimation* pAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/AY3/ay3.anm");
	CCLegendAnimation* pAnim = CCLegendAnimation::create("animation/monster/zsgr_bha/stand.anm");
	//pAnim->setPlaySpeed(4.0f);
	pAnim->setAction(1);
	pAnim->setVisible(true);
	pAnim->play();
	pAnim->setPlayLoop(true);
	pAnim->setPosition(ccp(s.width/2, s.height/2));
	//pAnim->setPosition(ccp(0, 0));
	//this->addChild(pAnim, 0, 1);

	// test special amodule
	pAnim = CCLegendAnimation::create("animation/player/zsgr_tjw/stand.anm");
	
	// add special amodule
	int imgIdx = BasePlayer::getWeaponImgIdx(pAnim);
	CCLegendAModule* pAModule = pAnim->addSpecialAModule(imgIdx, 0);
	//pAModule->runAction(repeapAction);
	// extra effect for the special amodule
	const int particleNum = 2;
	const float particleStartSize = 30.f;
	CCParticleSystem* particleEffect = CCParticleSystemQuad::create("animation/texiao/particledesigner/longhundi.plist");   // caiji1.plist
	particleEffect->setPosition(ccp(32/2, 40));
	particleEffect->setScale(1.0f);
	particleEffect->setTotalParticles(particleNum);
	//particleEffect->setScaleX(0.5f);
	//particleEffect->setScaleY(2.0f);
	particleEffect->setStartSize(particleStartSize);
	//particleEffect->setLife(0.5f);
	//particleEffect->setPositionType(kCCPositionTypeFree);
	particleEffect->setPositionType(kCCPositionTypeRelative);
	pAModule->addChild(particleEffect, 0);

	// add special amodule
	CCLegendAModule* pAModuleWeapon = pAnim->addSpecialAModule(imgIdx, 0);
	CCSprite* pHalo = CCSprite::create("animation/weapon/fan02guang.png");
	pHalo->setPosition(ccp(32/2, 35));
	//pHalo->setAnchorPoint(ccp(0.5f, 0.5f));
	pHalo->setFlipY(true);
	pHalo->setColor(ccc3(255, 0, 255));
	//pHalo->setColor(ccc3(255, 128, 0));
	//pHalo->setColor(ccc3(128, 255, 128));
	//pHalo->setColor(ccc3(255, 0, 0));
	pHalo->setOpacity(0);
	//pHalo->setScale(2.5f);
	CCFadeTo*  action1 = CCFadeTo::create(0.5f,0);
	CCFadeTo*  action2 = CCFadeTo::create(0.5f,64);
	CCFadeTo*  action3 = CCFadeTo::create(1.5f,128);
	CCFadeTo*  action4 = CCFadeTo::create(0.5f,64);
	CCFadeTo*  action5 = CCFadeTo::create(0.5f,0);
	CCDelayTime*  action6 = CCDelayTime::create(3.0f);
	CCRepeatForever * repeapAction = CCRepeatForever::create(
		CCSequence::create(action1,action2,action3,action4,action5,action6,NULL));
	pHalo->runAction(repeapAction);
	//pAModule->addChild(pHalo, 101);
	//pAModule->sortAllChildren();

	pAnim->setPlaySpeed(1.0f);
	pAnim->setAction(2);
	pAnim->setVisible(true);
	pAnim->play();
	pAnim->setPlayLoop(true);
	pAnim->setPosition(ccp(s.width/2, s.height/2));
	//pAnim->setPosition(ccp(0, 0));
	this->addChild(pAnim, 0, 1);

	CCMoveTo*  action10 = CCMoveTo::create(1.5f, ccp(100, 100));
	CCMoveTo*  action11 = CCMoveTo::create(1.5f, ccp(100, 200));
	CCRepeatForever * repeapAction1 = CCRepeatForever::create(CCSequence::create(action10,action11,NULL));
	//pAnim->runAction(repeapAction1);

	//this->setPosition(ccp(-100,-100));
	CCMoveTo*  action20 = CCMoveTo::create(1.5f, ccp(100, 100));
	CCMoveTo*  action21 = CCMoveTo::create(1.5f, ccp(300, 100));
	CCRepeatForever * repeapAction2 = CCRepeatForever::create(CCSequence::create(action20,action21,NULL));
	//this->runAction(repeapAction2);
}

#define CCSCROLLVIEW_MAP_TAG 900
void TestLayer::testLegendTiledMap()
{
	// load level
	LegendLevel* pLevelData = new LegendLevel();

	std::string levelFile = "level/";
	levelFile.append("xsc_1.level");
	pLevelData->load(levelFile.c_str());   // rom_huanggong.level

	CCLegendTiledMap* pTiledmap = new CCLegendTiledMap();
	pTiledmap->setStatic(true);
	//m_tiledmap->initWithTiledmapFilename("background/xsc/xsc.map");
	//std::string mapName = "";
	//mapName.append("background/");
	//mapName.append(m_levelData->tiledmapName);
	//m_tiledmap->initWithTiledmapFilename(mapName.c_str());
	pTiledmap->initWithTiledmapFilename(pLevelData->tiledmapName.c_str());
	CCSize mapSize = pTiledmap->getTiledMapData()->getMapSize();

	pTiledmap->setPosition(CCPointZero);

	delete pLevelData;

	////////////////////////////////////////////////////////////////

	//new CCScrollView 
	CCScrollView * scrollView_areaMap = CCScrollView::create(CCSizeMake(600, 350));
	//scrollView_areaMap->setContentSize(CCSizeMake(512,512));
	//scrollView_areaMap->setViewSize(CCSizeMake(600, 350));
	scrollView_areaMap->setAnchorPoint(CCPointZero);
	scrollView_areaMap->setContentOffset(CCPointZero);
	scrollView_areaMap->setTouchEnabled(true);
	scrollView_areaMap->setDirection(kCCScrollViewDirectionBoth);
	scrollView_areaMap->setPosition(ccp(20,25));
	//can not use setContainer,will cause error 
	//scrollView_areaMap->setContainer(areaMap);
	scrollView_areaMap->setDelegate(this);
	scrollView_areaMap->setBounceable(true);
	scrollView_areaMap->setClippingToBounds(true);
	addChild(scrollView_areaMap, 0, CCSCROLLVIEW_MAP_TAG);

	const float scale = 0.4f;
	pTiledmap->setScale(scale);
	mapSize = mapSize * scale;

	scrollView_areaMap->addChild(pTiledmap);

	// adjust the scroll view's contentsize
	scrollView_areaMap->setContentSize(mapSize);

	//scrollView_areaMap->setContentOffset(ccp(-800, -400));
}

void TestLayer::testCocosStudioScene()
{
	//CCNode *pNode = SceneReader::sharedSceneReader()->createNodeWithSceneFile("scenetest/FishJoy2.json");
	CCNode *pNode = SceneReader::sharedSceneReader()->createNodeWithSceneFile("scenetest1/TestScene.json");
	if (pNode == NULL)
	{
		
	}
	this->addChild(pNode);
}

void TestLayer::testAPI()
{
	std::string result = StrUtils::applyColor("hello World", ccc3(255, 0, 0));

	float degree = GameUtils::getDegree(ccp(0,0), ccp(10,10));
	degree = GameUtils::getDegree(ccp(0,0), ccp(0,10));
	degree = GameUtils::getDegree(ccp(0,0), ccp(-10,10));
	degree = GameUtils::getDegree(ccp(0,0), ccp(-10,0));
	degree = GameUtils::getDegree(ccp(0,0), ccp(-10,-10));
	degree = GameUtils::getDegree(ccp(0,0), ccp(0,-10));
	degree = GameUtils::getDegree(ccp(0,0), ccp(10,-10));
	degree = GameUtils::getDegree(ccp(0,0), ccp(0,0));

	long long currentSecond = GameUtils::getDateSecond();
	GameUtils::getDateNow(0);

	std::string id = GameUtils::getAppUniqueID();
	CCLOG("app unique id: %s", id.c_str());
}

void TestLayer::testCCRichLabel()
{
	std::string str;
	/*
    std::string testString = "propid(potion01,1001yuu)";
    structPropIds ids = RichElementButton::parseLink(testString);
    
    //std::string str = RichElementButton::makeButton("potion", "potion01", 1001);
    std::string str = RichElementButton::makeButton("potion", "potion01", 1001, 0x00ff00);
    CCLOG("new button string: %s", str.c_str());
	*/
    
    ///////////////////////////////////////////////////////////////////////////////////
    
    // test case: CCRichLabel
	CCRichLabel* richLabel = NULL;
	/*
    CCDictionary *strings = CCDictionary::createWithContentsOfFile("res_ui/font/strings.xml");
    //const char *chat  = ((CCString*)strings->objectForKey("test_str"))->m_sString.c_str();
    //const char* button_str = "/tbuttonName,propid(potion01;1001),color(00ff00)/";
	const char* chat = "/#00ff00 mission one - 4/20/ task/#00ff00 finished/ hello";
	CCRichLabel* richLabel = CCRichLabel::createWithString(chat, CCSizeMake(480, 100),
                                                           this, NULL, 0);
 	richLabel->setPosition(ccp(100, 100));
 	addChild(richLabel);
	*/

	const char* chat1 = "hello world /#ff0000 hello world/ hello world";
	RichLabelDefinition _def;
	_def.fontSize = 20;
	_def.fontColor = ccc3(0, 255, 0);
	CCRichLabel* richLabel1 = CCRichLabel::create(chat1, CCSizeMake(480, 100), _def);
 	richLabel1->setPosition(ccp(100, 100));
 	addChild(richLabel1);

	return;

	str = "/tbuttonName屠龙刀,propid(potion01;1001),color(00ff00)/";
 	richLabel = CCRichLabel::createWithString(str.c_str(), CCSizeMake(480, 100), NULL, NULL);
	int charCount = richLabel->getCharCount();

	str = "/e01中国/e01test";
 	richLabel = CCRichLabel::createWithString(str.c_str(), CCSizeMake(480, 100), NULL, NULL);
	charCount = richLabel->getCharCount();

	str = "test中国";
 	richLabel = CCRichLabel::createWithString(str.c_str(), CCSizeMake(480, 100), NULL, NULL);
	charCount = richLabel->getCharCount();

	StringDataManager::load();
 	const char* test_str = StringDataManager::getString("test_str");
	richLabel = CCRichLabel::createWithString(test_str, CCSizeMake(480, 250),
                                                           this, NULL, 0);
	richLabel->setAnchorPoint(ccp(0,1));	

 	richLabel->setPosition(ccp(50, 300));
 	addChild(richLabel);
}


void TestLayer::testOpenUrl()
{
	CCMenuItemFont* item = CCMenuItemFont::create("open URL", this, menu_selector(TestLayer::openURLCallback));
	CCMenu* menu = CCMenu::create( item, NULL);
	menu->setPosition(ccp(115,250));
	addChild(menu);
}


void TestLayer::testClippingNode()
{
    CCClippingNode *clip = CCClippingNode::create();
    //clip->setInverted(true);
	this->addChild(clip);

 //   CCSprite *back = CCSprite::create("images/loding1.jpg");
	//back->setAnchorPoint(ccp(0,0));
	//back->setPosition(ccp(0,0));
 //   clip->addChild(back);

	CCLegendAnimation* pAnim = CCLegendAnimation::create("animation/monster/zsgr_bha/stand.anm");
	pAnim->setAction(1);
	pAnim->setVisible(true);
	pAnim->play();
	pAnim->setPlayLoop(true);
	pAnim->setPosition(ccp(100, 100));
	clip->addChild(pAnim);
    
    //CCSprite *holeStencil = CCSprite::create("Images/hole_stencil.png");

	CCDrawNode* front=CCDrawNode::create();  
	ccColor4F yellow = {1, 1, 0, 1};  
	CCPoint rect[4]={ccp(-30,30),ccp(30,30),ccp(30,-30),ccp(-30,-30)};  
	front->drawPolygon(rect, 4, yellow, 0, yellow);  
	//front->setPosition(ccp(420, 300));  
	front->setPosition(ccp(100, 100));  
	clip->setStencil(front);
}
void TestLayer::testClippingNode_Spark()
{
	CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();

	CCClippingNode* clip = CCClippingNode::create();//创建裁剪节点
    CCSprite* gameTitle = CCSprite::create("res_ui/select_the_sercer/gamelogo_text_mask.png");
    clip->setStencil(gameTitle);//设置裁剪模板
    clip->setAlphaThreshold(0.0f);//设置透明度阈值
	//clip->setInverted(true);
    clip->setContentSize(CCSize(gameTitle->getContentSize().width,gameTitle->getContentSize().height));//设置裁剪节点大小    
    CCSize clipSize = clip->getContentSize();//获取裁剪节点大小
    clip->setPosition(ccp(visibleSize.width/2,visibleSize.height/2));//设置裁剪节点位置
   
//     CCSprite* gameTitle_show = CCSprite::create("res_ui/select_the_sercer/gamelogo.png");//创建要显示的对象
//     CCSprite* spark = CCSprite::create("images/guangtiao.png");//创建闪亮精灵
//     clip->addChild(gameTitle_show,1);//把要显示的内容放在裁剪节点中，其实可以直接clip->addChild(gameTitle,1);此处为了说明更改显示内容
//     spark->setPosition(ccp(-visibleSize.width/2, 0));//设置闪亮精灵位置
//     clip->addChild(spark,2);//添加闪亮精灵到裁剪节点
//     addChild(clip,4);//添加裁剪节点
//      
//     CCMoveTo* moveAction = CCMoveTo::create(2.6f, ccp(clipSize.width, 0));//创建精灵节点的动作
//     CCMoveTo* moveBack = CCMoveTo::create(2.6f, ccp(-clipSize.width, 0));
//     CCSequence* seq = CCSequence::create(moveAction,moveBack, NULL);
//     CCRepeatForever* repreatAction = CCRepeatForever::create(seq);
//     spark->runAction(repreatAction);//精灵节点重复执行动作
}


void TestLayer::scrollViewDidScroll( CCScrollView* view )
{
}
void TestLayer::scrollViewDidZoom( CCScrollView* view )
{
}

void TestLayer::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{
	int idx = cell->getIdx();
	CCLOG("table cell idx: %d", idx);
}
cocos2d::CCSize TestLayer::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeMake(80,50);
}
cocos2d::extension::CCTableViewCell* TestLayer::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCString *string = CCString::createWithFormat("idx %d", idx);
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell=new CCTableViewCell();
	cell->autorelease();

	CCLabelTTF* pLabel = CCLabelTTF::create(string->getCString(), APP_FONT_NAME, 24);
	pLabel->setAnchorPoint(ccp(0.5f,0.5f));
	pLabel->setPosition(ccp(tableCellSizeForIndex(table, idx).width/2, tableCellSizeForIndex(table, idx).height/2));
	cell->addChild(pLabel);

	return cell;
}
unsigned int TestLayer::numberOfCellsInTableView( CCTableView *table )
{
	return m_tableviewCellNumber;
}

bool TestLayer::ccTouchBegan(CCTouch* pTouch, CCEvent* event)
{
    return true;
    
	CCNode* pStrike = this->getChildByTag(TEST_MOTIONSTREAK_TAG);
	pStrike->setPosition(pTouch->getLocation());

	CCNode* pParticle = this->getChildByTag(TEST_MOTIONSTREAK_PARTICLE_TAG);
	pParticle->setPosition(pTouch->getLocation());

    return true;
}

void TestLayer::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
{
    return;
    
	CCNode* pStrike = this->getChildByTag(TEST_MOTIONSTREAK_TAG);
	pStrike->setPosition(pTouch->getLocation());

	CCNode* pParticle = this->getChildByTag(TEST_MOTIONSTREAK_PARTICLE_TAG);
	pParticle->setPosition(pTouch->getLocation());
}

void TestLayer::ccTouchEnded(CCTouch* pTouch, CCEvent* event)
{
	return;
	CCScrollView* pScrollView = (CCScrollView*)this->getChildByTag(CCSCROLLVIEW_MAP_TAG);
	CCPoint targetPos = pScrollView->convertTouchToNodeSpace(pTouch);

	CCPoint contentOffset = pScrollView->getContentOffset();
	CCPoint mapOffset = targetPos - contentOffset;
}

//void TestLayer::ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent)
//{
//	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
//	if(sc != NULL)
//		sc->endCommand(this);
//
//	if (m_curState==State_touchEnable)
//	{
//		GameState* pScene = new LoginState();
//		if (pScene)
//		{
//			pScene->runThisState();
//			pScene->release();
//		}
//	}
//
//
//	//layer->setVisible(false);
//	//return;
//
//// 	GameView::getInstance()->showPopupWindow("Attachments extracted successfully",2,callfuncO_selector(LogoLayer::menuSendCallback));
//// 	return;
//
//	// exit app
//	//CCDirector::sharedDirector()->end();
//
//	//CCDirector::sharedDirector()->purgeCachedData();
//
//	// create the next scene and run it
//    //GameState* pScene = new GameSceneState();
//    //if (pScene)
//    //{
//    //    pScene->runThisState();
//    //    pScene->release();
//    //}
//
//	//// create the next scene and run it
//	//add by yangjun 2013.9.29 am
//	//test for art
//	/*GameView::getInstance()->getMapInfo()->set_mapid("gd.level");
//    GameState* pScene = new LoadSceneState();
//    if (pScene)
//    {
//        pScene->runThisState();
//        pScene->release();
//    }*/
//	
//	// create the next scene and run it
//
//	
//	//CCLegendAnimation* anim = (CCLegendAnimation*)this->getChildByTag(1);
//	//static int actionId = 0;
//	//actionId++;
//	//if(actionId >= 7)
//	//	actionId = 0;
//	//anim->setAction(actionId);
//}

void TestLayer::update(float dt)
{
	ScriptManager::getInstance()->update();

	//GameMessageProcessor::sharedMsgProcessor()->socketUpdate();
	static int tick = 0;
	if(m_state == 0)
	{
		//GameMessageProcessor::sharedMsgProcessor()->sendReq(1001, this);

		//ClientNetEngine::sharedSocketEngine()->connect();

		//string data;
		//com::future::nettytest::protocol::CommonMessage comMessage;
		//comMessage.set_cmdid(1001);

		//com::future::nettytest::protocol::MessageFight message2;
		//message2.set_item("item");
		//message2.set_player("player");
		//message2.set_action(1002);
		//string msgData;
		//message2.SerializeToString(&msgData);
		//comMessage.set_data(msgData);

		//comMessage.SerializeToString(&data);
  //  
		//clent->send(comMessage);
		////Sleep(1*1000);
		//clent->send(comMessage);
		////Sleep(1*1000);
		//clent->getSendMsgs()->push(comMessage);
  //  
		////Sleep(1*1000);

		//clent->getSendMsgs()->push(comMessage);

		tick = 0;
		m_state = 1;
	}
	else if(m_state == 1)
	{
		tick++;
		if(tick < 60)
			return;
		//Sleep(5*1000);
		int i = 0;
		//while (i < 10) 
		{
			//com::future::nettytest::protocol::CommonMessage comMessage = ClientNetEngine::sharedSocketEngine()->getMsg();
			//if(comMessage.cmdid() == 0)
			//	return;

			//char name[5];
			//sprintf(name, "%d", comMessage.cmdid());   // message id
			//std::string pushhandlerClassName = "PushHandler";
			//PushHandler1001 *pVar = (PushHandler1001*)CKClassFactory::sharedClassFactory().getClassByName(pushhandlerClassName.append(name));
			//pVar->set(comMessage);
			//delete pVar;

			//cout << i << endl;
			//cout << "CommonMessage11: " << endl;
			//cout << "cmdid: " << comMessage.cmdid() << endl;
			//cout << "data: " << comMessage.data() << endl;
			//com::future::nettytest::protocol::MessagePlayer test;
			//test.ParseFromString(comMessage.data());
			//CCLog("msg: %d, %s", comMessage.cmdid(), comMessage.data().c_str());

			//cout << "message11: " << endl;
			//cout << "from: " << test.from() << endl;
			//cout << "to: " << test.to() << endl;
			//cout << "keycode: " << test.keycode() << endl;
			//CCLog("key: %s, %s, %d", test.from().c_str(), test.to().c_str(), test.keycode());
   //     
			////Sleep(1*1000);
			//i++;

			//char msg[100];
			//sprintf(msg, "msg: %d, %s; key: %s, %s, %d", comMessage.cmdid(), comMessage.data().c_str(), test.from().c_str(), test.to().c_str(), test.keycode());
			//CCLabelTTF *pLabel = CCLabelTTF::create(msg, "Marker Felt", 15);
			//static int index = 0;
			//pLabel->setPosition(ccp(10, 480 - index*30));
			//pLabel->setAnchorPoint(CCPointZero);
			//addChild(pLabel);

			//index++;
		}

		m_state = 5;

    
		//sleep(100000);
	//    return 0;
	}
	else if(m_state == 5)
	{
		// do nothing
	}
}

void TestLayer::skipScriptCallback(CCObject* pSender)
{
	ScriptManager::getInstance()->skipScript();
}
void TestLayer::runScriptCallback(CCObject* pSender)
{
	//ScriptManager::getInstance()->runScript("script/story.sc");
	ScriptManager::getInstance()->runScript("script/story9.sc");
	//ScriptManager::getInstance()->runScript("script/demo-story.sc");

	//CCTableView* pTableView = (CCTableView*)this->getChildByTag(CCTABLEVIEW_TEST_TAG);
	//pTableView->selectCell(1);
}
void TestLayer::openURLCallback(CCObject* pSender)
{
	CCApplication::sharedApplication()->openURL("http://124.202.137.33:8089/packages/ios/91/91.html");
   // CCApplication::sharedApplication()->openURL("http://blog.s135.com/demo/ios/");
}

void TestLayer::menuCallback(CCObject* pSender)
{
	CCTableView* pTableView = (CCTableView*)this->getChildByTag(CCTABLEVIEW_TEST_TAG);
	if(pTableView != NULL)
	{
		if(m_tableviewCellNumber > 0)
			m_tableviewCellNumber--;

		pTableView->removeCellAtIndex(0);
	}
}

void TestLayer::menuSendCallback(CCObject* pSender)
{
	testScene();
	return;
	//testBytes = new char[10485760];   // 10485760 = 10M

	m_dStartTime = 1000;
	double test = m_dStartTime;
	//static bool sConnected = false;
	//if(!sConnected)
	//{
	//	ClientNetEngine::sharedSocketEngine()->connect();
	//	sConnected = true;
	//}
	//GameMessageProcessor::sharedMsgProcessor()->sendReq(1001, this);

	//UITest* ui = new UITest();
	//addChild(ui);
	//ui->release();

	//GameView::getInstance()->showAlertDialog("haha");
	//GameView::getInstance()->showPopupWindow("test", 2, this, NULL);

	if(LoadSceneLayer::friendPanelBg == NULL)
	{
		//LoadSceneLayer::friendPanelBg = (UIPanel*)GUIReader::shareReader()->widgetFromJsonFile("res_ui/haoyou_1.json");
		//LoadSceneLayer::friendPanelBg->retain();
	}

	UISceneTest* uiSceneTest = UISceneTest::create();
	addChild(uiSceneTest);
	CCLOG("add UISceneTest");
}

void TestLayer::menuSendCallback1(CCObject* pSender)
{
	delete[] testBytes;   // 10485760 = 10M
}

void TestLayer::menuRichLabelButtonCallback(CCObject* pSender)
{
	CCMenuItemFont* button = (CCMenuItemFont*)pSender;
	RichElementButton* btnElement = (RichElementButton*)button->getUserData();
	//CCLog("richlabel clicked, %s", btnElement->linkContent.c_str());

	std::string item1_Id = "10";
	const char* item1_Info = "This is a weapon!";

	std::string item2_Id = "20";
	const char* item2_Info = "This is a potion!";

	if(btnElement->linkContent == item1_Id)
	{
		//CCLog(item1_Info);
	}
	else if(btnElement->linkContent == item2_Id)
	{
		//CCLog(item2_Info);
	}
}

// 创建地图人物...
void TestLayer::testScene()
{
	//char* test = new char[1024*1024];
	//delete[] test;
	//return;

	// load level
	LegendLevel* pLevelData = new LegendLevel();
	//delete pLevelData;
	//return;

	std::string levelFile = "level/";
	levelFile.append("xsc.level");
	pLevelData->load(levelFile.c_str());   // rom_huanggong.level
	//delete pLevelData;
	//return;

	//initDoors(GameView::getInstance()->getMapInfo()->mapid().c_str());

	CCLegendTiledMap* m_tiledmap = new CCLegendTiledMap();
	//std::string mapName = "";
	//mapName.append("background/");
	//mapName.append(m_levelData->tiledmapName);
	//m_tiledmap->initWithTiledmapFilename(mapName.c_str());
	m_tiledmap->initWithTiledmapFilename(pLevelData->tiledmapName.c_str());
	//m_tiledmap->initWithTiledmapFilename("background/xsc/xsc.map");
	m_tiledmap->release();

	AStarTiledMap* searchMap = new AStarTiledMap(500, 500);
	delete searchMap;

	CCDirector::sharedDirector()->purgeCachedData();

	// delete level data at last
	delete pLevelData;
}


void TestLayer::setCurrentState( CCObject * obj )
{
	spLog->removeFromParentAndCleanup(true);
	CCSize size=CCDirector::sharedDirector()->getVisibleSize();
	CCSprite * workRoomLog=CCSprite::create("images/quzhonglogo.jpg");
	workRoomLog->setAnchorPoint(ccp(0.5f,0.5f));
	workRoomLog->setPosition(ccp(size.width/2,size.height/2));
	addChild(workRoomLog,1);
	m_curState=State_touchEnable;
}




/////////////////////////////////////////////////////////////////////////

TestState::~TestState()
{
}

void TestState::runThisState()
{
    CCLayer* pLayer = new TestLayer();
    addChild(pLayer, 0, 1);

	if(CCDirector::sharedDirector()->getRunningScene() != NULL)
		CCDirector::sharedDirector()->replaceScene(this);
	else
		CCDirector::sharedDirector()->runWithScene(this);
    pLayer->release();
}

