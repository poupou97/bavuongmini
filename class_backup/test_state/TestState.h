#ifndef _GAMESTATE_TEST_STATE_H_
#define _GAMESTATE_TEST_STATE_H_

#include "../GameStateBasic.h"
#include "../legend_script/ScriptHandlerProtocol.h"

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class Script;

/**
 * test class, 
 * demonstrate some code samples, run some testcase, etc.
 */

class TestLayer : public CCLayer, public ScriptHandlerProtocol, public CCTableViewDataSource, public CCTableViewDelegate
{
public:
    TestLayer();
	~TestLayer();

	enum State
	{
		State_perfect = 0,
		State_workRoom = 1,
		State_touchEnable = 2,

	};

	virtual void update(float dt);

	void scrollViewDidScroll(CCScrollView* view);
	void scrollViewDidZoom(CCScrollView* view);

	void registerWithTouchDispatcher();

	//virtual void ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent);

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch* pTouch, CCEvent* event);

	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);

	virtual void registerScriptCommand(int scriptId);
	void addTutorialIndicator(const char* content);
	void removeTutorialIndicator();
	void setTutorialIndicatorPosition(CCPoint pos);

private:
	void testScene();

	void testSqlite();

	void setEffectPosition(CCNode* effectNode, CCPoint startPoint, CCPoint endPoint);
	void testLightningEffect();

	void testLabelColorByMark();

	void testGameActorAnimation();

	void testGraySprite();

	void testScript();

	void testStory();
    
	// special effect
    void testParticle();
	void showEffectCallback(CCObject* pSender);
	void testMotionStreak();

	void testLegendAnimation();
	void testLegendTiledMap();

	void testTableView();

	void testCocosStudioScene();

	void testAPI();
    
    void testCCRichLabel();

	void testOpenUrl();

	void testClippingNode();
	void testClippingNode_Spark();

	void testSearchGeneral();
private:
	int m_state;

	struct timeval m_sStartTime;
	double m_dStartTime;

	void menuCallback(CCObject* pSender);
	void menuSendCallback(CCObject* pSender);
	void menuSendCallback1(CCObject* pSender);
	void menuRichLabelButtonCallback(CCObject* pSender);
	void skipScriptCallback(CCObject* pSender);
	void runScriptCallback(CCObject* pSender);
	void openURLCallback(CCObject* pSender);

	CCSprite * spLog;
	void setCurrentState(CCObject * obj);
	char* testBytes;
	State m_curState;

	int mTutorialScriptInstanceId;

	int m_tableviewCellNumber;
};

class TestState : public GameState
{
public:
	~TestState();

    virtual void runThisState();
	
};

#endif // _GAMESTATE_LOGO_STATE_H_
