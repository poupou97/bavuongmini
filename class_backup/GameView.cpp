
#include "GameView.h"

#include "gamescene_state/GameSceneState.h"
#include "gamescene_state/MainScene.h"
#include "gamescene_state/role/GameActor.h"
#include "gamescene_state/role/MyPlayer.h"

#include "messageclient/element/CMapInfo.h"
#include "ui/extensions/PopupWindow.h"
#include "ui/extensions/AlertDialog.h"
#include "ui/missionscene/MissionManager.h"
#include "ui/backpackscene/PacPageView.h"
#include "ui/extensions/Counter.h"
#include "ui/Chat_ui/ChatCell.h"
// #include "messageclient\protobuf\InteractMessage.pb.h"
// #include "messageclient\protobuf\AuctionMessage.pb.h"
/*#include "messageclient\protobuf\MailMessage.pb.h"*/
#include "messageclient/element/CMailInfo.h"
#include "messageclient/element/FolderInfo.h"
#include "messageclient/element/MissionInfo.h"
#include "messageclient/element/CActiveRole.h"
#include "messageclient/element/CNpcDialog.h"
#include "ui/Chat_ui/ChatCell.h"
#include "gamescene_state/skill/GameFightSkill.h"
/*#include "messageclient/protobuf/ModelMessage.pb.h"*/
#include "messageclient/element/CShortCut.h"
#include "messageclient/element/CAuctionInfo.h"
#include "messageclient/element/CTeamMember.h"
#include "messageclient/element/CMapTeam.h"
#include "messageclient/element/CAroundPlayer.h"
#include "messageclient/element/CActionDetail.h"
#include "messageclient/element/CEquipment.h"
#include "messageclient/element/CRelationPlayer.h"
#include "messageclient/element/CGeneralBaseMsg.h"
#include "messageclient/element/CSalableCommodity.h"
#include "login_state/Login.h"
#include "messageclient/element/CGuildRecord.h"
#include "messageclient/element/CFivePersonInstanceEndInfo.h"
#include "messageclient/element/CLable.h"
#include "messageclient/element/CGeneralDetail.h"
#include "utils/StaticDataManager.h"
#include "gamescene_state/role/MyPlayerAIConfig.h"
#include "messageclient/GameMessageProcessor.h"
#include "ui/storehouse_ui/StoreHousePageView.h"
#include "GameAudio.h"
#include "utils/GameUtils.h"
#include "messageclient/element/COneVipGift.h"
#include "messageclient/element/COfflineExpPuf.h"
#include "gamescene_state/sceneelement/ChatWindows.h"
#include "ui/Chat_ui/PrivateCell.h"
#include "gamescene_state/role/MyPlayerOwnedCommand.h"
#include "ui/family_ui/familyFight_ui/FamilyFightData.h"
#include "messageclient/element/CRewardBase.h"
#include "ui/FivePersonInstance/FivePersonInstance.h"
#include "newcomerstory/NewCommerStoryManager.h"
#include "gamescene_state/role/ShowBaseGeneral.h"
#include "ui/GameUIUtils.h"
#include "gamescene_state/MainAnimationScene.h"
#include "login_state/LoginState.h"
#include "ui/extensions/ShortCutItem.h"
#include "gamescene_state/EffectDispatch.h"
#include "gamescene_state/LoadScene.h"

static GameView *s_SharedGameView = NULL;

#define POPUP_WINDOW_ZORDER 100
#define ALERT_DIALOG_ZORDER 200
#define POPUP_COUNTER_TAG 300 

GameView* GameView::getInstance(void)
{
    if (!s_SharedGameView)
    {
		s_SharedGameView = new GameView();
    }

    return s_SharedGameView;
}

GameView::GameView() 
{
	m_nAlertDialogCreatedId = kTagAlertDialogBaseId + 1;
	m_goodsColor = ccc3(255,255,255);
	m_generalsColor = ccc3(255,255,255);
	isShortCutLayerHidden = false;

	applyPlayersize = 0;
	m_playerGoldNUm = 0;
	m_playerGoldIngotNUm = 0;
	m_playerBindGoldIngotNUm = 0;
	hasPrivatePlaer = false;
	isShowPrivateChatBtn = false;
	registerNoticeIsShow = false;

	m_nRequiredGoldToGetExp = 0;
	m_nRequiredGoldInGotToGetExp = 0;
	isShowRepairEquip = false;

	m_checkRemind = true;
	m_remainTimes = 0;

	showSocketCloseInfo = true;
	reconnectGameServer = true;

	m_flowerParticleTime = 0;
	m_flowerNum = 0;
}
GameView::~GameView() 
{
    CC_SAFE_DELETE(myplayer);
	//CC_SAFE_DELETE(scene);

	CC_SAFE_DELETE(m_pMapInfo);
	CC_SAFE_DELETE(missionManager);

	CC_SAFE_DELETE(pacPageView);

	CC_SAFE_DELETE(storeHousePageView);

	std::vector<FolderInfo*>::iterator iter;

	for (iter = AllPacItem.begin(); iter != AllPacItem.end(); ++iter)
	{
		delete *iter;
	}
	AllPacItem.clear();
	for (iter = AllEquipItem.begin(); iter != AllEquipItem.end(); ++iter)
	{
		delete *iter;
	}
	AllEquipItem.clear();
	for (iter = AllGoodsItem.begin(); iter != AllGoodsItem.end(); ++iter)
	{
		delete *iter;
	}
	AllGoodsItem.clear();

	std::vector<CEquipment*>::iterator _iter;
	for (_iter = EquipListItem.begin(); _iter != EquipListItem.end(); ++_iter)
	{
		delete *_iter;
	}
	EquipListItem.clear();
	//chat
	std::vector<CRelationPlayer *>::iterator iter_relation;
	for (iter_relation=relationSourceVector.begin();iter_relation !=relationSourceVector.end();++iter_relation)
	{
		delete *iter_relation;
	}
	relationSourceVector.clear();
	for (iter_relation=relationFriendVector.begin();iter_relation !=relationFriendVector.end();++iter_relation)
	{
		delete *iter_relation;
	}
	relationFriendVector.clear();
	for (iter_relation=relationBlackVector.begin();iter_relation !=relationBlackVector.end();++iter_relation)
	{
		delete *iter_relation;
	}
	relationBlackVector.clear();
	for (iter_relation=relationEnemyVector.begin();iter_relation !=relationEnemyVector.end();++iter_relation)
	{
		delete *iter_relation;
	}
	relationEnemyVector.clear();
	for (iter_relation=relationTempVector.begin();iter_relation !=relationTempVector.end();++iter_relation)
	{
		delete *iter_relation;
	}
	relationTempVector.clear();
	
	std::vector<CMapTeam *>::iterator iter_mapTeam;
	for (iter_mapTeam= mapteamVector.begin();iter_mapTeam!=mapteamVector.end();++iter_mapTeam)
	{
		delete *iter_mapTeam;
	}
	mapteamVector.clear();
	std::vector<CAroundPlayer *>::iterator iter_aroundPlayer;
	for (iter_aroundPlayer= aroundPlayerVector.begin();iter_aroundPlayer!=aroundPlayerVector.end();++iter_mapTeam)
	{
		delete *iter_aroundPlayer;
	}
	aroundPlayerVector.clear();

	std::vector<CTeamMember *>::iterator iter_teamMember;
	for (iter_teamMember= teamMemberVector.begin();iter_teamMember!=teamMemberVector.end();++iter_teamMember)
	{
		delete *iter_aroundPlayer;
	}
	teamMemberVector.clear();
	std::vector<CAuctionInfo *>::iterator iter_auction;
	for (iter_auction=auctionConsignVector.begin();iter_auction !=auctionConsignVector.end();++iter_auction)
	{
		delete *iter_auction;
	}
	auctionConsignVector.clear();
	for (iter_auction=auctionSourceVector.begin();iter_auction !=auctionSourceVector.end();++iter_auction)
	{
		delete *iter_auction;
	}
	auctionSourceVector.clear();

	std::vector<CActionDetail*>::iterator iter_actionDetail;
	for (iter_actionDetail = actionDetailList.begin(); iter_actionDetail != actionDetailList.end(); ++iter_actionDetail)
	{
		delete *iter_actionDetail;
	}
	actionDetailList.clear();

	std::vector<CGeneralBaseMsg*>::iterator iter_generalBaseMsg;
	for (iter_generalBaseMsg = generalsInLineList.begin(); iter_generalBaseMsg != generalsInLineList.end(); ++iter_generalBaseMsg)
	{
		delete *iter_generalBaseMsg;
	}
	generalsInLineList.clear();

	std::vector<CGeneralDetail*>::iterator iter_generalDetail;
	for (iter_generalDetail = generalsInLineDetailList.begin(); iter_generalDetail != generalsInLineDetailList.end(); ++iter_generalDetail)
	{
		delete *iter_generalDetail;
	}
	generalsInLineDetailList.clear();

	std::vector<CSalableCommodity*>::iterator iter_shopItemList;
	for (iter_shopItemList = shopItemList.begin(); iter_shopItemList != shopItemList.end(); ++iter_shopItemList)
	{
		delete *iter_shopItemList;
	}
	shopItemList.clear();

	std::vector<CSalableCommodity*>::iterator iter_buyBackList;
	for (iter_buyBackList = buyBackList.begin(); iter_buyBackList != buyBackList.end(); ++iter_buyBackList)
	{
		delete *iter_buyBackList;
	}
	buyBackList.clear();

	std::vector<CGuildRecord*>::iterator iter_guildRecord;
	for (iter_guildRecord = guildRecordVector.begin(); iter_guildRecord != guildRecordVector.end(); ++iter_guildRecord)
	{
		delete *iter_guildRecord;
	}
	guildRecordVector.clear();

	std::vector<CLable*>::iterator iter_goldStoreList;
	for (iter_goldStoreList = goldStoreList.begin(); iter_goldStoreList != goldStoreList.end(); ++iter_goldStoreList)
	{
		delete *iter_goldStoreList;
	}
	goldStoreList.clear();

	std::vector<CLable*>::iterator iter_bingGoldStoreList;
	for (iter_bingGoldStoreList = bingGoldStoreList.begin(); iter_bingGoldStoreList != bingGoldStoreList.end(); ++iter_bingGoldStoreList)
	{
		delete *iter_bingGoldStoreList;
	}
	bingGoldStoreList.clear();

	std::vector<CMailInfo*>::iterator iter_systemMail;
	for (iter_systemMail = mailVectorOfSystem.begin(); iter_systemMail != mailVectorOfSystem.end(); ++iter_systemMail)
	{
		delete *iter_systemMail;
	}
	mailVectorOfSystem.clear();

	std::vector<CMailInfo*>::iterator iter_playerMail;
	for (iter_playerMail = mailVectorOfPlayer.begin(); iter_playerMail != mailVectorOfPlayer.end(); ++iter_playerMail)
	{
		delete *iter_playerMail;
	}
	mailVectorOfPlayer.clear();


	std::map<int,CGeneralBaseMsg*>::const_iterator cIter;
	for (std::map<int,CGeneralBaseMsg*>::iterator it = m_aliveGeneralBaseList.begin(); it != m_aliveGeneralBaseList.end(); ++it) 
	{
		CC_SAFE_DELETE(it->second);
	}
	m_aliveGeneralBaseList.clear();

	std::vector<COneVipGift*>::iterator iter_oneVipGift;
	for (iter_oneVipGift = m_vipEveryDayRewardsList.begin(); iter_oneVipGift != m_vipEveryDayRewardsList.end(); ++iter_oneVipGift)
	{
		delete *iter_oneVipGift;
	}
	m_vipEveryDayRewardsList.clear();

	std::vector<COfflineExpPuf*>::iterator iter_offLineExp;
	for (iter_offLineExp = m_offLineExpPuf.begin(); iter_offLineExp != m_offLineExpPuf.end(); ++iter_offLineExp)
	{
		delete *iter_offLineExp;
	}
	m_offLineExpPuf.clear();
}

void GameView::init() 
{
    myplayer = new MyPlayer();
	myplayer->retain();

	m_pMapInfo = new CMapInfo();

	//维持一个MissionManager的实例
	missionManager = MissionManager::getInstance();
	//missionManager->init();

	pacPageView = PacPageView::create();
	pacPageView->retain();

	storeHousePageView = StoreHousePageView::create();
	storeHousePageView->retain();
}

void GameView::onSocketOpen(ClientNetEngine* socketEngine)
{
	GameState* pScene;
	if(!reconnectGameServer)
	{
		pScene = new LoginState();
		CCLayer* pLayer = new LoginLayer();
		pScene->addChild(pLayer, 0, 1);
		pLayer->release();
		reconnectGameServer = true;
	}
	else
	{
		pScene = (GameState*)CCDirector::sharedDirector()->getRunningScene();
	}
	pScene->onSocketOpen(socketEngine);
}
void GameView::onSocketClose(ClientNetEngine* socketEngine)
{
	CCScene* runningScene = CCDirector::sharedDirector()->getRunningScene();
	if(showSocketCloseInfo)
	{
		reconnectGameServer = false;
		if(runningScene->getTag() == GameView::STATE_GAME)
		{
			SceneLoadLayer* pLoadSceneNode = (SceneLoadLayer*)runningScene->getChildByTag(GameSceneState::TAG_LOADSCENE);
			if(pLoadSceneNode != NULL)
			{
				showAlertDialog(StringDataManager::getString("con_relogin"));
			}
			else
			{
				showReconnectButton();
				//LoginLayer::reconnectServer();
				return;
			}
		}
		else
		{
			showAlertDialog(StringDataManager::getString("con_relogin"));
		}
		//showAlertDialog(StringDataManager::getString("con_relogin"));
	}
	showSocketCloseInfo = true;

	GameState* pGameState = (GameState*)CCDirector::sharedDirector()->getRunningScene();
	pGameState->onSocketClose(socketEngine);
}
void GameView::onSocketError(ClientNetEngine* socketEngines, const ClientNetEngine::ErrorCode& error)
{
}

bool GameView::isOwn(long long roleId){
	return roleId == myplayer->getRoleId();
}
bool GameView::isOwn(GameActor* a) {
	return isOwn(a->getRoleId());
}

CMapInfo* GameView::getMapInfo()
{
	return m_pMapInfo;
}

void GameView::setPlayerGold( int _value )
{
	m_playerGoldNUm = _value;
}

void GameView::setPlayerGoldIngot( int _value )
{
	m_playerGoldIngotNUm = _value;
}

void GameView::setPlayerBindGoldIngot( int _value )
{
	m_playerBindGoldIngotNUm = _value;
}

int GameView::getPlayerBindGoldIngot()
{
	return m_playerBindGoldIngotNUm;
}

int GameView::getPlayerGoldIngot()
{
	return m_playerGoldIngotNUm;
}


int GameView::getPlayerGold()
{
	return m_playerGoldNUm ;
}

GameSceneLayer* GameView::getGameScene()
{
	GameSceneLayer* scene = (GameSceneLayer*)CCDirector::sharedDirector()->getRunningScene()->getChildByTag(GameSceneState::TAG_GAMESCENE);
	return scene;
}
MainScene* GameView::getMainUIScene()
{
	MainScene* scene = (MainScene*)CCDirector::sharedDirector()->getRunningScene()->getChildByTag(GameSceneState::TAG_MAINSCENE);
	return scene;
}

MainAnimationScene * GameView::getMainAnimationScene()
{
	MainAnimationScene* scene = (MainAnimationScene*)CCDirector::sharedDirector()->getRunningScene()->getChildByTag(GameSceneState::TAG_ANIMATIONSCENE);
	return scene;
}

void GameView::showReconnectButton()
{
	CCSize winSize=CCDirector::sharedDirector()->getVisibleSize();
	CCScene* runningScene = CCDirector::sharedDirector()->getRunningScene();
	
	PopupWindow * dialog =PopupWindow::create(StringDataManager::getString("con_reconnection_introduction"),1,PopupWindow::KtypeNeverRemove,0);
	dialog->ignoreAnchorPointForPosition(false);
	dialog->setAnchorPoint(ccp(0.5f,0.5f));
	dialog->setPosition(ccp(winSize.width/2,winSize.height/2));
	dialog->AddcallBackEvent(runningScene,callfuncO_selector(GameView::callBackReconnect),NULL);
	GameView::getInstance()->getMainUIScene()->addChild(dialog,ZOrder_PopUpWindow);
}

void GameView::callBackReconnect(cocos2d::CCObject *sender)
{
	LoginLayer::reconnectServer();
}

void GameView::showAlertDialog(std::string string)
{
	CCSize winSize=CCDirector::sharedDirector()->getVisibleSize();
	CCScene* runningScene = CCDirector::sharedDirector()->getRunningScene();

	AlertDialog * dialog = AlertDialog::create(string.c_str());	
	dialog->setTag(m_nAlertDialogCreatedId++);
	int alert_height = dialog->getContentSize().height;   // should use alert height
	dialog->setPosition(ccp(winSize.width/2,winSize.height/2-alert_height));
	//dialog->runAction(CCMoveBy::create(0.3f,ccp(0,height)));
	runningScene->addChild(dialog, ALERT_DIALOG_ZORDER);

	int lastestTag = m_nAlertDialogCreatedId-1;
	if (runningScene->getChildByTag(lastestTag) !=NULL )
	{
		for (int i = kTagAlertDialogBaseId; i < m_nAlertDialogCreatedId; i++)
		{			
			if (runningScene->getChildByTag(i)!=NULL)
			{
				runningScene->getChildByTag(i)->runAction(CCMoveBy::create(0.3f,ccp(0,alert_height)));
			}
		}
	}

	//GameUtils::playGameSound(ERROR_TIPS, 2, false);
}

void GameView::showPopupWindow( std::string string,int buttonNum,CCObject * pSender,SEL_CallFuncO pSelectorYes,SEL_CallFuncO pSelectorNo,PopupWindow::PopWindowType winType, int remainTime)
{
	if(!this->getMainUIScene())
		return;

	CCSize winSize=CCDirector::sharedDirector()->getVisibleSize();
	PopupWindow * dialog =PopupWindow::create(string,buttonNum,winType,remainTime);
	dialog->ignoreAnchorPointForPosition(false);
	dialog->setAnchorPoint(ccp(0.5f,0.5f));
	dialog->setPosition(ccp(winSize.width/2,winSize.height/2));
	dialog->AddcallBackEvent(pSender,pSelectorYes,pSelectorNo);
	this->getMainUIScene()->addChild(dialog,ZOrder_PopUpWindow);
	//CCDirector::sharedDirector()->getRunningScene()->addChild(dialog, POPUP_WINDOW_ZORDER);
}

void GameView::showCounter( CCObject * pSender,SEL_CallFuncO pSelector,int defaultNum /*=0*/ )
{
	CCSize winSize=CCDirector::sharedDirector()->getVisibleSize();

	Counter* jisuanqi1 = (Counter *)CCDirector::sharedDirector()->getRunningScene()->getChildByTag(POPUP_COUNTER_TAG);
	if (jisuanqi1 == NULL)
	{
		jisuanqi1 = Counter::create();
		jisuanqi1->ignoreAnchorPointForPosition(false);
		jisuanqi1->setAnchorPoint(ccp(0.5f,0.5f));
		jisuanqi1->setPosition(ccp(winSize.width/2,winSize.height/2));
		jisuanqi1->callBackSendNum(pSender,pSelector);
		jisuanqi1->setShowNumDefault(defaultNum);
		CCDirector::sharedDirector()->getRunningScene()->addChild(jisuanqi1, POPUP_WINDOW_ZORDER,POPUP_COUNTER_TAG);
	}
}

void GameView::ResetData()
{
	if (this->missionManager)
	{
		std::vector<MissionInfo*>::iterator _iter;
		for (_iter = this->missionManager->MissionList.begin(); _iter != this->missionManager->MissionList.end(); ++_iter)
		{
			delete *_iter;
		}
		this->missionManager->MissionList.clear();

		for (_iter = this->missionManager->MissionList_MainScene.begin(); _iter != this->missionManager->MissionList_MainScene.end(); ++_iter)
		{
			delete *_iter;
		}
		this->missionManager->MissionList_MainScene.clear();

		for (_iter = this->missionManager->curMissionList.begin(); _iter != this->missionManager->curMissionList.end(); ++_iter)
		{
			delete *_iter;
		}
		this->missionManager->curMissionList.clear();

		//设置是首次创建人物面板
		this->missionManager->setIsFirstCreateMissionAndTeam(true);
	}

	//上阵武将列表
	std::vector<CGeneralBaseMsg*>::iterator iter_generalBaseMsg;
	for (iter_generalBaseMsg = generalsInLineList.begin(); iter_generalBaseMsg != generalsInLineList.end(); ++iter_generalBaseMsg)
	{
		delete *iter_generalBaseMsg;
	}
	generalsInLineList.clear();
	//上阵武将详细信息
	std::vector<CGeneralDetail*>::iterator iter_generalDetail;
	for (iter_generalDetail = generalsInLineDetailList.begin(); iter_generalDetail != generalsInLineDetailList.end(); ++iter_generalDetail)
	{
		delete *iter_generalDetail;
	}
	generalsInLineDetailList.clear();
	//武将技能列表
	GeneralsGameFightSkillList.erase(GeneralsGameFightSkillList.begin(),GeneralsGameFightSkillList.end());
	std::map<std::string, GameFightSkill*>::iterator it = GeneralsGameFightSkillList.begin();
	for ( ; it != GeneralsGameFightSkillList.end(); ++ it )
	{
		GameFightSkill * tempSkill = it->second;
		delete tempSkill;
	}
	GeneralsGameFightSkillList.clear();

	if (myplayer)
	{
		myplayer->removeAllExStatus();
        
        myplayer->setAnimDir(ANIM_DIR_DOWN);

		myplayer->setActiveStrategyId(-1);

		myplayer->setAngerValue(0);

		myplayer->setPKMode(BasePlayer::PKMode_peace);
	}

	MyPlayerCommandAttack::s_latestSkillId = "";
	MyPlayerCommandAttack::s_latestSkillStartTime = 0;

	std::map<int,CGeneralBaseMsg*>::const_iterator cIter;
	for (std::map<int,CGeneralBaseMsg*>::iterator it = m_aliveGeneralBaseList.begin(); it != m_aliveGeneralBaseList.end(); ++it) 
	{
		CC_SAFE_DELETE(it->second);
	}
	m_aliveGeneralBaseList.clear();


	MyPlayerAIConfig::setAutomaticSkill(0);
	MyPlayerAIConfig::enableRobot(false);

	std::vector<ChatCell * >::iterator iter;
	for (iter =chatCell_Vdata.begin(); iter != chatCell_Vdata.end(); ++ iter)
	{
		ChatCell * cellv_=* iter;
		cellv_->release();
	}
	chatCell_Vdata.clear();

	std::vector<ChatCell *>::iterator iterCell;
	for (iterCell = ChatWindows::s_chatCellsDataVector.begin();iterCell != ChatWindows::s_chatCellsDataVector.end();++iterCell)
	{
		ChatCell * cellv_=* iterCell;
		cellv_->release();
	}
	ChatWindows::s_chatCellsDataVector.clear();

	setIsCheckRemind(true);

	repairEquipInstanceIdVector.clear();
	hasPrivatePlaer = false;
	isShowPrivateChatBtn = false;
	//
	std::vector<PrivateCell *>::iterator iterContent;
	for (iterContent = chatPrivateContentVector.begin();iterContent != chatPrivateContentVector.end();++iterContent)
	{
		PrivateCell * cell_=* iterContent;
		cell_->release();
	}
	chatPrivateContentVector.clear();

	std::vector<CRelationPlayer *>::iterator iteSpeaker_;
	for (iteSpeaker_ = chatPrivateSpeakerVector.begin();iteSpeaker_ != chatPrivateSpeakerVector.end();++iteSpeaker_)
	{
		delete * iteSpeaker_;
	}
	chatPrivateSpeakerVector.clear();


	chatPrivateContentSourceVector.clear();

	//team

	std::vector<CTeamMember *>::iterator iterSelfTeamMember_;
	for (iterSelfTeamMember_ = teamMemberVector.begin();iterSelfTeamMember_!= teamMemberVector.end();++iterSelfTeamMember_)
	{
		delete *iterSelfTeamMember_;
	}
	teamMemberVector.clear();

	//goldStore
	std::vector<CLable*>::iterator iter_goldStoreList;
	for (iter_goldStoreList = goldStoreList.begin(); iter_goldStoreList != goldStoreList.end(); ++iter_goldStoreList)
	{
		delete *iter_goldStoreList;
	}
	goldStoreList.clear();

	std::vector<CLable*>::iterator iter_bingGoldStoreList;
	for (iter_bingGoldStoreList = bingGoldStoreList.begin(); iter_bingGoldStoreList != bingGoldStoreList.end(); ++iter_bingGoldStoreList)
	{
		delete *iter_bingGoldStoreList;
	}
	bingGoldStoreList.clear();

	//familyFightInfo
	FamilyFightData::getInstance()->setAllToDefault();

	//
	remindvector.clear();
	//

	std::vector<CRewardBase*>::iterator iter_rewardBase;
	for (iter_rewardBase = rewardvector.begin(); iter_rewardBase != rewardvector.end(); ++iter_rewardBase)
	{
		delete * iter_rewardBase;
	}
	rewardvector.clear();

	//fiveInstanceStar
	FivePersonInstance::getInstance()->s_m_copyStar.clear();

	//add by yangjun 2014.9.16
	NewCommerStoryManager::getInstance()->clear();

	//set flower particle time is 0
	setFlowerParticleTime(0);

	//add by LiuLiang 2014.11.7
	ShowMyGeneral::getInstance()->clearAllData();

	GameUIUtils::s_bPreloaded = false;

	//set robot data save xml
	MyPlayerAIConfig::savePushConfig();

	//
	std::vector<FolderInfo*>::iterator iter_shortFolder;
	for (iter_shortFolder = EffectEquipItem::folderItemVector.begin(); iter_shortFolder != EffectEquipItem::folderItemVector.end(); ++iter_shortFolder)
	{
		delete * iter_shortFolder;
	}
	EffectEquipItem::folderItemVector.clear();
}

cocos2d::ccColor3B GameView::getGoodsColorByQuality( int goodsQuality )
{
	switch(goodsQuality)
	{
	case 1 :
		{
			m_goodsColor = ccc3(255,255,255); 
		}
		break;
	case 2 :
		{
			m_goodsColor = ccc3(0,255,0);
		}
		break;
	case 3 :
		{
			m_goodsColor = ccc3(0,255,255);
		}
		break;
	case 4 :
		{
			m_goodsColor = ccc3(255,0,228);
		}
		break;
	case 5 :
		{
			m_goodsColor = ccc3(254,124,0);
		}
		break;
	}
	return m_goodsColor;
}

cocos2d::ccColor3B GameView::getGeneralsColorByQuality( int generalsQuality )
{
	if (generalsQuality < 11)                                                                //白色
	{
		m_generalsColor = ccc3(255,255,255); 
	}
	else if (generalsQuality>10 && generalsQuality < 21)                              //绿色
	{
		m_generalsColor = ccc3(0,255,0);
	}
	else if (generalsQuality>20 && generalsQuality < 31)                              //蓝色
	{
		m_generalsColor = ccc3(0,255,255);
	}
	else if (generalsQuality>30 && generalsQuality < 41)                              //紫色
	{
		m_generalsColor = ccc3(255,0,228);
	}
	else if (generalsQuality>40 && generalsQuality < 51)                              //橙色
	{
		m_generalsColor = ccc3(254,124,0);
	}
	else
	{
		m_generalsColor = ccc3(255,255,255); 
	}
	return m_generalsColor;
}

void GameView::setIsCheckRemind(bool _value)
{
	m_checkRemind = _value;
}

bool GameView::getIsCheckRemind()
{
	return m_checkRemind;
}

void GameView::setRemainTimesOfArena( int times )
{
	m_remainTimes = times;
}

int GameView::getRemainTimesOfArena()
{
	return m_remainTimes;
}

void GameView::setFlowerParticleTime(long long time_)
{
	m_flowerParticleTime = time_;
}

long long GameView::getFlowerParticleTime()
{
	return m_flowerParticleTime;
}

void GameView::setFlowerNum( int num_ )
{
	m_flowerNum = num_;
}

int GameView::getFlowerNum()
{
	return m_flowerNum;
}








