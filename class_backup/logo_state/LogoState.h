#ifndef _GAMESTATE_LOGO_STATE_H_
#define _GAMESTATE_LOGO_STATE_H_


//1:wanmei		0:test
#define THREEKINGDOMS_SERVERLIST_MODE 1

#include "cocos2d.h"
#include "../GameStateBasic.h"


/**
 Display the game logo
 */


class LogoLayer : public CCLayer
{
public:
    LogoLayer();
	~LogoLayer();

	enum State
	{
		State_perfect_logo = 0,
		State_studio_logo = 1,
		State_touchEnable = 2,
	};

	virtual void update(float dt);

	virtual void ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent);

private:
	void loadUIWidgetRes();

private:
	int m_state;

	struct timeval m_sStartTime;
	double m_dStartTime;

	void getUserDefault();

	CCSprite * spLog;
	void publisherLogoStateCallBack(CCObject * obj);
	void studioLogoStateCallBack( CCObject * obj );

	State m_curState;
};

class LogoState : public GameState
{
public:
	~LogoState();

    virtual void runThisState();
	
};

#endif // _GAMESTATE_LOGO_STATE_H_
