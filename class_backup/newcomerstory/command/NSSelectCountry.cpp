#include "NSSelectCountry.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../AppMacros.h"
#include "../../gamescene_state/MainScene.h"
#include "../../legend_script/ScriptManager.h"
#include "../NewCommerStoryManager.h"
#include "../../messageclient/element/CMapInfo.h"
#include "../../ui/country_ui/CountryUI.h"
#include "../../messageclient/GameMessageProcessor.h"

NSSelectCountry::NSSelectCountry(void)
{
}


NSSelectCountry::~NSSelectCountry(void)
{
}

void* NSSelectCountry::createInstance()
{
	return new NSSelectCountry() ;
}

void NSSelectCountry::init()
{
	setState(STATE_INITED);
}

void NSSelectCountry::update() {

	CCScene* scene = CCDirector::sharedDirector()->getRunningScene();

	switch (mState) {
	case STATE_INITED:
		{
			setState(STATE_START);
		}
	case STATE_START:
		{
			CCLOG("start NSSelectCountry");

			//打开选择国家界面
			CCSize winSize  = CCDirector::sharedDirector()->getVisibleSize();
			CountryUI * countryUI = CountryUI::create();
			countryUI->ignoreAnchorPointForPosition(false);
			countryUI->setAnchorPoint(ccp(.5f,.5f));
			countryUI->setPosition(ccp(winSize.width/2,winSize.height/2));
			countryUI->setTag(kTag_NewLayer);
			scene->addChild(countryUI,10000);

			setState(STATE_UPDATE);
		}
		break;
	case STATE_UPDATE:
		{
			//检测是否完成了选择国家
			if (NewCommerStoryManager::getInstance()->IsSelectCountryFinished())
			{
				//close selectCountryUI
				if (scene->getChildByTag(kTag_NewLayer))
				{
					scene->getChildByTag(kTag_NewLayer)->removeFromParent();
				}
				//set state
				setState(STATE_END);
			}
		}
		break;
	case STATE_END:
		break;
	default:
		CCLOG("please init data");
		break;
	}
}

