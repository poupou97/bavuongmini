#ifndef _NEWCOMMERSTORY_COMMAND_NSMOVE_H_
#define _NEWCOMMERSTORY_COMMAND_NSMOVE_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include <string>
#include <vector>
#include "../NewCommerStory.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace std;

/**
 * 新手剧情-移动逻辑
 * 
 * @author yangjun
 * @date 2014-9-12
 */
class NSMove : public NewCommerStory
{
public:
	NSMove(void);
	~NSMove(void);

	static void* createInstance() ;
	void init();

	virtual void update();

private:
};


/**
 * 新手剧情-移动到楼梯下
 * 
 * @author yangjun
 * @date 2014-11-06
 */
class NSMoveToStairs : public NewCommerStory
{
public:
	NSMoveToStairs(void);
	~NSMoveToStairs(void);

	static void* createInstance() ;
	void init();

	virtual void update();
};
#endif
