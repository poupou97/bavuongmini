//
//  CKBaseClass.cpp
//  Blog_C++_Reflection
//
//  Created by 晓龙 张 on 12-2-28.
//  Copyright (c) 2012年 chukong-inc. All rights reserved.
//

#include <iostream>

#include "CKBaseClass.h"

IMPLEMENT_CLASS(CKBaseClass)

CKBaseClass::CKBaseClass() 
{
    
}
CKBaseClass::~CKBaseClass() 
{
    
}
void* CKBaseClass::createInstance() 
{
    return new CKBaseClass() ;
}
void CKBaseClass::registProperty() 
{
    //overwrite method at subclass
}
void CKBaseClass::display() 
{
    
}