//
//  CKClassFactory.cpp
//  Blog_C++_Reflection
//
//  Created by 晓龙 张 on 12-2-28.
//  Copyright (c) 2012年 chukong-inc. All rights reserved.
//


#include "CKClassFactory.h"

CKClassFactory::CKClassFactory() 
{
}

CKClassFactory::~CKClassFactory() 
{
}

void* CKClassFactory::getClassByName(string className) 
{
    map<string, createClass>::const_iterator iter ;
    
    iter = m_classMap.find(className) ; 
    if ( iter == m_classMap.end() )
        return NULL ;
    else 
        return iter->second() ;
}

void CKClassFactory::registClass(string name, createClass method) 
{
    m_classMap.insert(pair<string, createClass>(name, method)) ;
}

CKClassFactory& CKClassFactory::sharedClassFactory() 
{
    static CKClassFactory _sharedClassFactory ;
    return _sharedClassFactory ;
}