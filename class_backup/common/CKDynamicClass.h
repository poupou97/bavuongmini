//
//  CKDynamicClass.h
//  Blog_C++_Reflection
//
//  Created by 晓龙 张 on 12-2-28.
//  Copyright (c) 2012年 chukong-inc. All rights reserved.
//

#ifndef Blog_C___Reflection_CKDynamicClass_h
#define Blog_C___Reflection_CKDynamicClass_h

#include"CKClassFactory.h"

class CKDynamicClass
{
public:
    CKDynamicClass(string name, createClass method) ;
} ;

#define DECLARE_CLASS(className)\
string className##Name ;        \
static CKDynamicClass* m_className##dc ;

#define IMPLEMENT_CLASS(className)  \
CKDynamicClass* className::m_className##dc = \
new CKDynamicClass(#className, className::createInstance) ;

#endif
