#include "GameResourceManager.h"

#include <string>
#include "GameUserDefault.h"

GameResourceManager::GameResourceManager()
{
}

GameResourceManager::~GameResourceManager()
{
}

void GameResourceManager::prepareStaticData()
{
	// prepare resource
	#define GAME_STATICDATA_DB "game.db"
	std::string dbPath = CCFileUtils::sharedFileUtils()->getWritablePath();  
	dbPath += GAME_STATICDATA_DB;

    bool bNewVersion = false;
    int resVersion = CCUserDefault::sharedUserDefault()->getIntegerForKey(KEY_OF_RES_VERSION);
    if(GAME_RES_VERSION > resVersion)
    {
        bNewVersion = true;
		CCUserDefault::sharedUserDefault()->setIntegerForKey(KEY_OF_RES_VERSION, GAME_RES_VERSION);
    }
    
	if(!CCFileUtils::sharedFileUtils()->isFileExist(dbPath))
	{
		copyFile(GAME_STATICDATA_DB);
	}
    else
    {
        if(bNewVersion)
        {
            copyFile(GAME_STATICDATA_DB);
        }
    }

	////////////////////////////////////////////

	char* game_static_data_db_file = "gamestaticdata.db";
	dbPath = CCFileUtils::sharedFileUtils()->getWritablePath();  
	dbPath += game_static_data_db_file;

	if(!CCFileUtils::sharedFileUtils()->isFileExist(dbPath))
	{
		copyFile(game_static_data_db_file);
	}
    else
    {
        if(bNewVersion)
        {
            copyFile(game_static_data_db_file);
        }
    }
	////////////////////////////////////////////

	// LiuLiang add 2014.4.8(排行榜数据库)
	#define GAME_RANK_DB "rank.db"
	std::string rankDbPath = CCFileUtils::sharedFileUtils()->getWritablePath();  
	rankDbPath += GAME_RANK_DB;

	if (!CCFileUtils::sharedFileUtils()->isFileExist(rankDbPath))
	{
		copyFile(GAME_RANK_DB); //要使用的sqlite库文件
	}
    else
    {
        if(bNewVersion)
        {
            copyFile(GAME_RANK_DB);
        }
    }
}

void GameResourceManager::copyFile(const char* pFileName) {  

	std::string strPath = CCFileUtils::sharedFileUtils()->fullResourcePathForFilename(pFileName);  

	unsigned long len = 0;  
	unsigned char* data = NULL;  
	// must use "rb", otherwise you maybe get wrong the len of filedata
	data = CCFileUtils::sharedFileUtils()->getFileData(strPath.c_str(), "rb", &len);  

	std::string destPath = CCFileUtils::sharedFileUtils()->getWritablePath();
	destPath += pFileName;

	// must use "wb", otherwise you maybe write a wrong file
	FILE *pFp = fopen(destPath.c_str(), "wb");  
	fwrite(data, sizeof(unsigned char), len, pFp);  
	fclose(pFp);  
	delete[] data;  
	data = NULL;  
}  
