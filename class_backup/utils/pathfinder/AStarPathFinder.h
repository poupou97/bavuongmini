/*
 *  AStarPathFinder.h
 *
 *  Created by Zhao Gang on 07-31-2013.
 *  Copyright 2013 Perfect Future. All rights reserved.
 *
 */

#ifndef PATHFINDER_ASTARPATHFINDER_H
#define PATHFINDER_ASTARPATHFINDER_H

#include "cocos2d.h"

USING_NS_CC;

class FinderContext;
class AStarTiledMap;
class AStarNode;

class AStarPathFinder {
public:
	AStarPathFinder();
	virtual ~AStarPathFinder();
	
	/**
	 * 地图格子信息，第一维Y轴，第二维X轴，值为1表示可通过
	 */
	void setMap(char** mapData, int hNum, int vNum);

	AStarTiledMap* getMap();

	CCArray* getOpenList();

	std::vector<short*>* searchPath(short* startPos, short* objectPos);

	void setSearchTimes(short maxTimes);

	void resetList();

	void setLimit(char* limit);

	void release();

	void clear();

	bool positionEquals(short* pos1, short* pos2);

	/**
	 * 搜索算法
	 */
	CCArray* findPath(short* srcPos, short* targetPos);

	/**
	 * 判断从from结点到to结点是否不可行
	 * 
	 * @param from
	 * @param to
	 * @return
	 */
	bool isCannotGo(AStarTiledMap* tiledMap, AStarNode* from, int toX, int toY);

	bool isValidX(int x);
	bool isValidY(int y);

	CCArray* buildPath(FinderContext* ctx, AStarNode* current);

	/**
	 * 比较指定结点是否是目标结点
	 * 
	 * @param current
	 * @return
	 */
	bool isAchieve(AStarNode* current, AStarNode* target);

	long getMaxCost();
	long getMinCost();

	int getCount();
	long getAvgCost();

	long getTotCost();

private:
	FinderContext* ctx;
	long maxCost;
	long minCost;
	int count;
	long avgCost;
	long totCost;
};

#endif