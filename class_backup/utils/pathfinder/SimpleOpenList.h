/*
 *  SimpleOpenList.h
 *
 *  Created by Zhao Gang on 07-31-2013.
 *  Copyright 2013 Perfect Future. All rights reserved.
 *
 */

#ifndef __PATHFINDER_SIMPLEOPENLIST_H__
#define __PATHFINDER_SIMPLEOPENLIST_H__

#include "OpenList.h"

class Table;

class SimpleOpenList : public OpenList {
public:
	SimpleOpenList();
	virtual ~SimpleOpenList();

	virtual void add(AStarNode* node);

	virtual AStarNode* poll();

	virtual CCArray* values();

	virtual AStarNode* get(int x, int y);

	virtual int size();

	virtual void clear();

	virtual void touchNeighbor(AStarNode* neighbor, AStarNode* current,
			AStarNode* target);

private:
	Table *table;
    CCArray *list; // List
};

#endif