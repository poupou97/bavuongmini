#include "NextAdjNode.h"

#include "cocos2d.h"

NextAdjNode::NextAdjNode(int node, int weight) {
	nextNode = NULL;

	nodeNum = node;
	edgeWeight = weight;
}

int NextAdjNode::getNodeNum() {
	return nodeNum;
}

void NextAdjNode::setNodeNum(int nodeNum) {
	this->nodeNum = nodeNum;
}

int NextAdjNode::getEdgeWeight() {
	return edgeWeight;
}

void NextAdjNode::setEdgeWeight(int edgeWeight) {
	this->edgeWeight = edgeWeight;
}

NextAdjNode* NextAdjNode::getNextNode() {
	return nextNode;
}

void NextAdjNode::setNextNode(NextAdjNode* nextNode) {
	this->nextNode = nextNode;
}